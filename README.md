# Makefile

The Makefile does all the heavy lifting. The main commands I've been using:
- `make build`, which makes the Docker image
- `make run`, which runs the Docker image

Once in Docker, you can:
- `make DESIGN=picorv32 glitch`, which performs a glitch run
- `make DESIGN=picorv32 repro`, which performs a glitch run, finds the most 1337 FI setting, and re-runs that on the target
- `make DESIGN=picorv32 single`, which just does a single run without glitching

See the makefile for more poorly documented details/options.
