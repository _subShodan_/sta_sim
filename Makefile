# Examples:
# make DESIGN=aes DESIGN_TB=tb_aes sim
# make DESIGN=picorv32_axi DESIGN_TB=picorv32_wrapper sim
# make DESIGN=andgate DESIGN_TB=andgate_tb sim
# make DESIGN=picorv32         GLITCH_TESTCASES=1000   GLITCH_CLOCKS=2100 GLITCH_START=0 GLITCH_END=2038 GLITCH_MODE=random results
# make DESIGN=otaes_2020-01-22 GLITCH_TESTCASES=100000 GLITCH_CLOCKS=100  GLITCH_START=0 GLITCH_END=100  GLITCH_MODE=random results
# make DESIGN=otaes                                    GLITCH_CLOCKS=1000 GLITCH_START=0 GLITCH_END=1000 GLITCH_MODE=bruteforce FIGREP_TOPN=10000 results

# Platform inputs
export PLATFORM := sky130
export OPENROAD := /OpenROAD-flow-public
export LIBRARY := $(OPENROAD)/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib

# Various
export BE_NICE := nice -n 19 chrt --idle 0
export NPROC := `nproc`
export OUTDIR := $(realpath ./output)
export DESIGNS := $(realpath ./designs)
export PLATFORMS := $(realpath ./platform)
export SHELL := /bin/bash
export TOOLDIR := $(realpath ./tools)
export GLITCHER_TOOL := $(TOOLDIR)/glitcher.py

# Design inputs
export DESIGN := picorv32
include $(DESIGNS)/$(PLATFORM)/$(DESIGN)/config.mk
export DESIGN_TOP := $(DESIGN_NAME)# Sourced from abve config file

# Instrument all gates or only FFs? Empty => ffonly, anything else => all
export INSTRUMENT_ALL :=
export INSTRUMENT_OPTS := $(if $(INSTRUMENT_ALL),--all,--ff-only)
export INST_MODIFIER := $(subst -,,$(INSTRUMENT_OPTS))
export DESIGN_OUT := $(OUTDIR)/$(PLATFORM)/$(DESIGN)/$(INST_MODIFIER)

# Glitching args
export GLITCH_TESTCASES := 100000
export GLITCH_PARALLEL := $(NPROC)
export GLITCH_PROB := 0.01
export GLITCH_CLOCKS := 100
export GLITCH_START := 0
export GLITCH_END := 100
export GLITCH_WIDTH := 1
export GLITCH_MACHINES := 256
export GLITCH_OPTS := # e.g. -v
export GLITCH_RNG := `date +%-N`
export GLITCH_MODE := bruteforce# bruteforce or random
ifeq ($(GLITCH_MODE),bruteforce)
export GLITCH_BRUTEFORCE := -b
else 
export GLITCH_BRUTEFORCE :=
endif
export GLITCH_OUTPUT := $(DESIGN_OUT)/figrep_$(GLITCH_MODE).res
$(info ${GLITCH_OUTPUT})
export FIGREP_TOPN := 100
export GLITCH_REPRO := $(DESIGN_OUT)/repro
export GLITCH_REPRO_DIR := $(DESIGN_OUT)/rep
export GLITCH_VCD_FAULT := $(DESIGN_OUT)/fault.vcd
export GLITCH_VCD_NORMAL := $(DESIGN_OUT)/normal.vcd
export GLITCH_VCD_DIFF := $(DESIGN_OUT)/diff.vcd

export TB_FILE := test.bin
export VCD_SINGLE_FILE := trace.vcd
export SINGLE_NUM := 1
export SINGLE_CLOCKS := $(GLITCH_CLOCKS)
export SINGLE_VERBOSE := vvv


# Hardened output
export HARDEN_OUT := $(DESIGN_OUT)/harden_$(GLITCH_MODE)$(FIGREP_TOPN)
export HARDEN_PINS := $(HARDEN_OUT)/harden_pins.txt
export HARDEN_OUTV := $(HARDEN_OUT)/hardened.v

# Files and folders
export TBDIR := $(DESIGNS)/tb/$(DESIGN)
export DESIGNCONFIGDIR := $(DESIGNS)/$(PLATFORM)/$(DESIGN)
export LIB2V := $(TOOLDIR)/lib2v.py
export PEWTRACE := $(TOOLDIR)/pew_trace.py
export LIB2V_OUT := $(OUTDIR)/$(PLATFORM)/lib2v.v
export FI_GREP := $(TOOLDIR)/fi_grep.py
export FI_DIFF := $(TOOLDIR)/fi_diff.py
export HARDEN := $(TOOLDIR)/harden.py
export TB_VERI := $(DESIGN_OUT)/tb/testbench_veri
export GL_MAP := $(DESIGN_OUT)/instrumented.v.map
export SOURCE_V := $(OPENROAD)/flow/results/$(PLATFORM)/$(DESIGN)/6_final.v
export INITIAL_V := $(DESIGN_OUT)/final.v
export GLITCH_PINS := $(DESIGN_OUT)/pins.txt
export PEW_OUT := $(DESIGN_OUT)/pins_trace.txt

# Verilator args
export CXX := clang
export LINK := clang
export CXXOPT := -O2 -mavx2
export CXXFLAGS := $(CXXOPT) -DMACHINES=$(GLITCH_MACHINES)

# Cloud stuff
export GCP_REGIONS := northamerica-northeast1 us-central1 us-east1 us-east4 #us-west1 us-west2 us-west3
export GCP_MACHINE_TYPE := c2-standard-8 #c2-standard-60
export GCP_NETWORK_TIER := standard
export GCP_IMAGE := ubuntu-2004-focal-v20210510
export GCP_IMAGE_PROJECT := ubuntu-os-cloud

cloudcreate:
	for GCP_REGION in $(GCP_REGIONS); do (gcloud compute instances bulk create --name-pattern=vm-$$GCP_REGION-'###' --region=$$GCP_REGION --count=1 --machine-type=$(GCP_MACHINE_TYPE) --network-tier=$(GCP_NETWORK_TIER) --image=$(GCP_IMAGE) --image-project=$(GCP_IMAGE_PROJECT) &); done

clouddelete:
	gcloud compute instances list | grep ^vm- | awk '{ print "gcloud compute instances delete " $$1 " --quiet --zone " $$2 }' | sh

cloudstatus:
	gcloud compute instances list | grep ^vm- | awk '{ print "echo " $$1 " `gcloud compute ssh " $$1 " --zone " $$2 " --command=\"uptime; tail -n4 ~/stdout\"` &" }' | sh

# XXX we should make the files to be copied dependencies in makefile, but that means gcloud has to be in docker as well
export PACKAGE=/tmp/package.tgz
export SCRIPT=cloudstart.sh
export CLOUDSTART=0
export CLOUDSIZE=10
cloudrun: 
	tar cvzhf $(PACKAGE) Makefile tools/ output/$(PLATFORM)/$(DESIGN)/$(INST_MODIFIER)/tb/testbench_veri  output/$(PLATFORM)/$(DESIGN)/$(INST_MODIFIER)/tb/$(TB_FILE) output/$(PLATFORM)/$(DESIGN)/$(INST_MODIFIER)/instrumented.v.map 
	$(TOOLDIR)/cloudrun.py $(SCRIPT) $(PACKAGE) $(CLOUDSTART) $(CLOUDSIZE)

 
# Go rtl2gds to create final design files
gds: $(INITIAL_V)

# Add glitch bus
instrument: $(DESIGN_OUT)/instrumented.v

# Run iverilog on design
sim: $(DESIGN_OUT)/tb/testbench.vvp $(TBDIR)
	cd $(DESIGN_OUT)/tb && vvp -N $< +vcd +trace +noerror

# Run verilator on design
glitch: $(GLITCH_OUTPUT)
.PHONY: glitch

.PHONY: clean_glitch
clean_glitch:
	rm -f $(GLITCH_OUTPUT)

$(GLITCH_OUTPUT): $(TB_VERI) $(GL_MAP)
	cd $(DESIGN_OUT)/tb && $(BE_NICE) $(GLITCHER_TOOL) $(GLITCH_OPTS) --glitchprob $(GLITCH_PROB) -r $(GLITCH_RNG) -s $(GLITCH_START) -e $(GLITCH_END) -t $(GLITCH_TESTCASES) -n $(GLITCH_PARALLEL) --clocks $(GLITCH_CLOCKS) -g $(GLITCH_WIDTH) -m $(GLITCH_MACHINES) $(GLITCH_BRUTEFORCE) "$(TB_VERI) -i $(TB_FILE)" $(GL_MAP) $@ | tee $@.log

.PHONY: results
results: $(GLITCH_REPRO)
$(GLITCH_REPRO) $(GLITCH_PINS): $(GLITCH_OUTPUT)
	$(FI_GREP) -v -n $(FIGREP_TOPN) -g $(GL_MAP) -r $(GLITCH_REPRO) -o $(GLITCH_PINS) $(GLITCH_OUTPUT) | tee $@.repro.log
$(GLITCH_PINS): $(GLITCH_REPRO)

.PHONY: harden_pins
harden_pins: $(HARDEN_PINS)
$(HARDEN_PINS): $(GLITCH_OUTPUT)
	mkdir -p $(HARDEN_OUT)
	$(FI_GREP) -n $(FIGREP_TOPN) -g $(GL_MAP) -w $@ $^ | tee $@.pins.log

.PHONY: tbveri
tbveri: $(TB_VERI)

.PHONY: vcdtrace
vcdtrace: repro
	cd $(DESIGN_OUT)/tb && $(MAKE) vcdtrace
	$(FI_DIFF) -v $(GLITCH_VCD_NORMAL) $(GLITCH_VCD_FAULT) $(GLITCH_VCD_DIFF)

.PHONY: repro
repro: $(GLITCH_REPRO)
	cd $(DESIGN_OUT)/tb && $(MAKE) repro

.PHONY: reprorun
reprorun: $(GLITCH_OUTPUT)
	$(BE_NICE) $(FI_GREP) -g $(GL_MAP) -a $(GLITCH_REPRO_DIR) -vv -R "$(TB_VERI) -vvv -s1 -i $(TBDIR)/$(TB_FILE)" $^ | tee $@.log

.PHONY: single
single: $(TB_VERI)
	cd $(DESIGN_OUT)/tb && $(MAKE) single

.PHONY: pewtrace
pewtrace: $(PEW_OUT)
$(PEW_OUT): $(GLITCH_PINS)
	$(PEWTRACE) $(INITIAL_V) $(LIB2V_OUT) -o $@ -f $^ -g $@.dot -t otaes

.PHONY: harden
harden: $(HARDEN_OUTV)

$(HARDEN_OUTV): $(HARDEN_PINS) $(INITIAL_V)
	mkdir -p $(HARDEN_OUT)
	$(HARDEN) -v $(INITIAL_V) $(HARDEN_OUTV) -r $(HARDEN_PINS) | tee $(HARDEN_OUTV).log

.PHONY: exp1 exp1cp exp1hardenresults exp1hardentb
exp1hardentb: $(HARDEN_OUTV)
	$(MAKE) DESIGN_OUT=$(HARDEN_OUT) SOURCE_V=$(HARDEN_OUTV) tbveri

exp1hardenresults: $(HARDEN_OUTV)
	$(MAKE) DESIGN_OUT=$(HARDEN_OUT) SOURCE_V=$(HARDEN_OUTV) GLITCH_MODE=random GLITCH_TESTCASES=10000 results

exp1cp:
	export NEW_HARDEN_OUT=$(HARDEN_OUT)/../`basename $(HARDEN_OUT)`$(HARDEN_MOD)/ && \
	export NEW_SOURCE_V=$$NEW_HARDEN_OUT/hardened.v && \
	mkdir -p $$NEW_HARDEN_OUT && \
	cp -a $(HARDEN_OUTV) $(HARDEN_PINS) $$NEW_HARDEN_OUT && \
	$(MAKE) DESIGN_OUT=$$NEW_HARDEN_OUT SOURCE_V=$$NEW_SOURCE_V INSTRUMENT_ALL=$$NEW_INSTRUMENT_ALL GLITCH_MODE=random GLITCH_TESTCASES=10000 results

exp1:
	# First, build the base case
	$(MAKE) INSTRUMENT_ALL= GLITCH_MODE=random glitch
	# Do the building, which is single threaded, in parallel for all experiments
	# Do the simulation, which is multithreaded, one experiment at a time
	echo `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000` | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) FIGREP_TOPN={0} NPROC=1 INSTRUMENT_ALL= exp1hardentb
	for TOPN in `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000`; do $(MAKE) FIGREP_TOPN=$$TOPN INSTRUMENT_ALL= exp1hardenresults; done
	# Now repeat for instrument_all = 1
	$(MAKE) INSTRUMENT_ALL=1 GLITCH_MODE=random glitch
	echo `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000` | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) FIGREP_TOPN={0} NPROC=1 INSTRUMENT_ALL=1 exp1hardentb
	for TOPN in `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000`; do $(MAKE) FIGREP_TOPN=$$TOPN INSTRUMENT_ALL=1 exp1hardenresults; done
	# Harden all, test ff-only
	echo `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000` | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) FIGREP_TOPN={0} NPROC=1 INSTRUMENT_ALL= NEW_INSTRUMENT_ALL=1 HARDEN_MOD=all exp1cp
	# Harden ff-only, test all => need the 'all' testbenches, but harden only ffs
	echo `seq 1 1 10` `seq 20 20 100` `seq 200 200 1000` `seq 2000 2000 10000` | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) FIGREP_TOPN={0} NPROC=1 INSTRUMENT_ALL=1 NEW_INSTRUMENT_ALL= HARDEN_MOD=ffonly exp1cp
	$(MAKE) exp1_plot INSTRUMENT_ALL= HARDEN_MOD=
	$(MAKE) exp1_plot INSTRUMENT_ALL=1 HARDEN_MOD=
	$(MAKE) exp1_plot INSTRUMENT_ALL= HARDEN_MOD=all
	$(MAKE) exp1_plot INSTRUMENT_ALL=1 HARDEN_MOD=ffonly
	gnuplot  -e "set terminal dumb 150 40; set title 'exp1_$(GLITCH_MODE)'; set xrange [1:6000]; set xlabel 'Hardened gates'; set style data lines; plot '$(DESIGN_OUT)/../ffonly/exp1_$(GLITCH_MODE)_.tsv' title columnheader, '$(DESIGN_OUT)/../ffonly/exp1_$(GLITCH_MODE)_all.tsv' title columnheader, '$(DESIGN_OUT)/../all/exp1_$(GLITCH_MODE)_ffonly.tsv' title columnheader, '$(DESIGN_OUT)/../all/exp1_$(GLITCH_MODE)_.tsv' title columnheader"
	gnuplot  -e "set output 'exp1_$(GLITCH_MODE).svg'; set terminal svg size 600,400 dynamic enhanced font 'arial,10' mousing name 'simple_1' butt dashlength 1.0; set xrange [1:6000]; set xlabel 'Hardened gates'; set style data lines; plot '$(DESIGN_OUT)/../ffonly/exp1_$(GLITCH_MODE)_.tsv' title columnheader, '$(DESIGN_OUT)/../ffonly/exp1_$(GLITCH_MODE)_all.tsv' title columnheader, '$(DESIGN_OUT)/../all/exp1_$(GLITCH_MODE)_ffonly.tsv' title columnheader, '$(DESIGN_OUT)/../all/exp1_$(GLITCH_MODE)_.tsv' title columnheader"

exp1_plot:
	export PLOT_TSV=$(DESIGN_OUT)/exp1_$(GLITCH_MODE)_$(HARDEN_MOD).tsv && \
	cd $(DESIGN_OUT) && \
	echo -e "Hardened-gates\tFaults-$(INST_MODIFIER)-$(HARDEN_MOD)" > $$PLOT_TSV && \
	export INPUTS=`ls |grep -P '^harden_$(GLITCH_MODE)\d+$(HARDEN_MOD)$$'` && \
	for pinfile in $$INPUTS; do echo `cat $$pinfile/harden_pins.txt | awk -F'(' '{print $$1}' | sort -u | wc -l` `grep Jobs $$pinfile/figrep_random.res.log | sed 's/[^0-9 ]*//g' | awk '{print $$3}'`; done | sort -n >> $$PLOT_TSV

.PHONY: exp_perf
exp_perf:
	NPROC=$(NPROC); for NP in `seq 2 2 $$NPROC`; do $(MAKE) NPROC=$$NP GLITCH_MODE=random INSTRUMENT_ALL=1 GLITCH_TESTCASES=200 GLITCH_OUTPUT=exp_perf_all_$$NP.res    glitch; done
	NPROC=$(NPROC); for NP in `seq 2 2 $$NPROC`; do $(MAKE) NPROC=$$NP GLITCH_MODE=random INSTRUMENT_ALL=  GLITCH_TESTCASES=1000 GLITCH_OUTPUT=exp_perf_ffonly_$$NP.res glitch; done

.PHONY: exp2
exp2:
	# Use randomized glitching as base case for hardening, as opposed to exp1 where we bruteforce

test:
	cd $(TOOLDIR) && for t in "*_test*"; do ./$$t; done

.PHONY: exp3
exp3:
	# Build all in parallel
	echo otaes-496fd343659e686a8b97123409d13e6796666b82 otaes-1056d3d7967ae4d651e524bda8f6d8745ccf9235 otaes-275dd04aca5fb00b3fff1e10bef849af547e2a0d otaes-4d1697aa4e2f78d46b099542120b971a4f4c72e8 otaes-a8cabbb13a6881ce4b657e8a2ddf5973d58e29fe otaes-b50dc9d7c6148ad94cd77ab24d59da027567f1d2 otaes-fd3708aa4d674ce7ed871e4f41ad7fc7abfa2f69 otaes_2020-01-22 | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) DESIGN={0} NPROC=1        GLITCH_CLOCKS=300 GLITCH_START=0 GLITCH_END=300 GLITCH_MODE=bruteforce FIGREP_TOPN=100000 GLITCH_MACHINES=32 INSTRUMENT_ALL=1 tbveri
	# Run experiments one by one
	echo otaes-496fd343659e686a8b97123409d13e6796666b82 otaes-1056d3d7967ae4d651e524bda8f6d8745ccf9235 otaes-275dd04aca5fb00b3fff1e10bef849af547e2a0d otaes-4d1697aa4e2f78d46b099542120b971a4f4c72e8 otaes-a8cabbb13a6881ce4b657e8a2ddf5973d58e29fe otaes-b50dc9d7c6148ad94cd77ab24d59da027567f1d2 otaes-fd3708aa4d674ce7ed871e4f41ad7fc7abfa2f69 otaes_2020-01-22 | tr ' ' '\n' | parallel --ungroup --jobs 1        $(MAKE) DESIGN={0} NPROC=$(NPROC) GLITCH_CLOCKS=300 GLITCH_START=0 GLITCH_END=300 GLITCH_MODE=bruteforce FIGREP_TOPN=100000 GLITCH_MACHINES=32 INSTRUMENT_ALL=1 glitch
	# Run results in parallel again
	echo otaes-496fd343659e686a8b97123409d13e6796666b82 otaes-1056d3d7967ae4d651e524bda8f6d8745ccf9235 otaes-275dd04aca5fb00b3fff1e10bef849af547e2a0d otaes-4d1697aa4e2f78d46b099542120b971a4f4c72e8 otaes-a8cabbb13a6881ce4b657e8a2ddf5973d58e29fe otaes-b50dc9d7c6148ad94cd77ab24d59da027567f1d2 otaes-fd3708aa4d674ce7ed871e4f41ad7fc7abfa2f69 otaes_2020-01-22 | tr ' ' '\n' | parallel --ungroup --jobs $(NPROC) $(MAKE) DESIGN={0} NPROC=1        GLITCH_CLOCKS=300 GLITCH_START=0 GLITCH_END=300 GLITCH_MODE=bruteforce FIGREP_TOPN=100000 GLITCH_MACHINES=32 INSTRUMENT_ALL=1 pewtrace


# Build docker image when dockerfile changed
build: docker.build
docker.build: Dockerfile
	sudo docker build --build-arg http_proxy="$(http_proxy)" --build-arg https_proxy="$(https_proxy)" -t nlsim:v1 .
	touch docker.build

# Run a built docker image
run: build
	sudo docker run --env http_proxy="$(http_proxy)" --env https_proxy="$(https_proxy)" -v "`pwd`":/nlsim -it nlsim:v1

# Export built docker image
nlsim.tgz: build
	sudo docker save nlsim:v1 | pigz > $@

export: nlsim.tgz

import:
	cat nlsim.tgz | gunzip | sudo docker load

test_openroad:
	cd $(OPENROAD)/flow && source ../setup_env.sh && make clean_all && make

test_picorv32:
	cd /picorv32 && make test_verilator

clean:
	rm -rf $(OUTDIR)

# Final design synth/pnr/etc
$(INITIAL_V): $(SOURCE_V) 
	mkdir -p $(DESIGN_OUT)
	cp -a $< $@

$(SOURCE_V): $(DESIGNCONFIGDIR)/*
	cp -arLf $(DESIGNS)/$(PLATFORM)/$(DESIGN) $(OPENROAD)/flow/designs/$(PLATFORM)/$(DESIGN)
	cp -arLf $(DESIGNS)/src/$(DESIGN) $(OPENROAD)/flow/designs/src/$(DESIGN)
	cd $(OPENROAD)/flow && source ../setup_env.sh && export DESIGN_CONFIG=./designs/$(PLATFORM)/$(DESIGN)/config.mk && make clean_all && make

$(DESIGN_OUT)/final.def: $(INITIAL_V)
	cp $(OPENROAD)/flow/results/$(PLATFORM)/$(DESIGN)/6_final.def $@

# Instrument based on final design
$(DESIGN_OUT)/instrumented.v: $(INITIAL_V) 
	mkdir -p $(DESIGN_OUT)
	$(TOOLDIR)/instrument.py -v $(INSTRUMENT_OPTS) $(LIBRARY) $< $@ $(GL_MAP) | tee $@.log

# Split instrumented version into bits
$(DESIGN_OUT)/bitlified.v: $(DESIGN_OUT)/instrumented.v
	$(TOOLDIR)/bitlify.py -v $< $@ $(DESIGN_OUT)/bitlified.h $(LIBRARY) -w $(GLITCH_MACHINES) | tee $@.log

# Platform .v file
$(LIB2V_OUT): $(LIBRARY) $(LIB2V) 
	mkdir -p $(DESIGN_OUT)
	$(LIB2V) $< $@ -w $(GLITCH_MACHINES)
	cp -ar $(PLATFORMS)/$(PLATFORM)/* `dirname "$@"`

# Iverilog vvp file:
$(DESIGN_OUT)/tb/testbench.vvp: $(INITIAL_V) $(OUTDIR)/$(PLATFORM)/lib2v.v
	mkdir -p $(DESIGN_OUT)/tb
	cp -srf $(TBDIR)/* $(DESIGN_OUT)/tb
	cd $(DESIGN_OUT)/tb && $(MAKE) sim

# Verilator binary
$(TB_VERI): $(DESIGN_OUT)/bitlified.v $(DESIGN_OUT)/bitlified.h $(OUTDIR)/$(PLATFORM)/lib2v.v
	mkdir -p $(DESIGN_OUT)/tb
	cp -srf $(TBDIR)/* $(DESIGN_OUT)/tb
	cd $(DESIGN_OUT)/tb && $(BE_NICE) $(MAKE) veri
	#verilator --threads $(NPROC) --cc --exe -Wno-lint -trace --top-module $(DESIGN_TB) $^ $(DEFINES_TB) --Mdir $(DESIGN_OUT)/verilator
