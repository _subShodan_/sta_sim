export DESIGN_NICKNAME = cortexm0
# DESIGN_NAME is the module top name
export DESIGN_NAME = nlsim_wrapper
export PLATFORM    = sky130

export VERILOG_FILES = $(sort $(wildcard ./designs/src/$(DESIGN_NICKNAME)/cm0_arm.v))
export SDC_FILE      = ./designs/$(PLATFORM)/$(DESIGN_NICKNAME)/constraint.sdc

# These values must be multiples of placement site
export DIE_AREA    = 0 0 1120 1020.8
export CORE_AREA   = 10 12 1110 1011.2
