export DESIGN_NICKNAME = aes
# DESIGN_NAME is the module top name
export DESIGN_NAME = aes
export PLATFORM    = sky130

export VERILOG_FILES = $(sort $(wildcard ./designs/src/$(DESIGN_NICKNAME)/src/rtl/*.v))
export SDC_FILE      = ./designs/$(PLATFORM)/$(DESIGN_NICKNAME)/constraint.sdc

# These values must be multiples of placement site
export DIE_AREA    = 0 0 40000 40000
export CORE_AREA   = 10 10 39990 39990
