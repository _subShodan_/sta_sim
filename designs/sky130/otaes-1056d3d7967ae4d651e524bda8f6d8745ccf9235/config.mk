export DESIGN_NICKNAME = otaes-1056d3d7967ae4d651e524bda8f6d8745ccf9235
# DESIGN_NAME is the module top name
export DESIGN_NAME = otaes
export PLATFORM    = sky130

export VERILOG_FILES = $(sort $(wildcard ./designs/src/$(DESIGN_NICKNAME)/opentitan.v))
export SDC_FILE      = ./designs/$(PLATFORM)/$(DESIGN_NICKNAME)/constraint.sdc

# These values must be multiples of placement site
export DIE_AREA    = 0 0 11200 10208
export CORE_AREA   = 10 12 11100 10112
export PLACE_DENSITY = 0.99 # Jasper added this, not sure if it's sane

