export DESIGN_NICKNAME = andgate
# DESIGN_NAME is the module top name
export DESIGN_NAME = andgate
export PLATFORM    = sky130

export VERILOG_FILES = $(sort $(wildcard ./designs/src/$(DESIGN_NICKNAME)/andgate.v))
export SDC_FILE      = ./designs/$(PLATFORM)/$(DESIGN_NICKNAME)/constraint.sdc

# These values must be multiples of placement site
export DIE_AREA    = 0 0 112.0 102.08
export CORE_AREA   = 1.0 1.2 111.0 101.12
