`timescale 1ns/1ns

module sim_tb;
    reg clk = 1;
    reg rst_n = 0;
    integer clock_counter = 0;
    always #200 clk = ~clk;

    always @(posedge clk) begin
        clock_counter <= clock_counter + 1;
    end

    reg [31:0] memory [0:'h100000];
    reg [31:0] read_temp;
    integer r, f, f_bin, f_random;

    initial begin
        $dumpfile("sim_tb.vcd");
        $dumpvars(0, sim_tb);
        $readmemh("rv32i.hex", memory);

        repeat (10) @(posedge clk);
        rst_n <= 1;
        repeat (3050) @(posedge clk);
        $finish();
    end

    wire mem_valid;
    wire mem_instr;
    reg mem_ready;
    wire [31:0] mem_addr;
    wire [31:0] mem_wdata;
    wire [3:0] mem_wstrb;
    reg  [31:0] mem_rdata;

    // display some information
    always @(posedge clk) begin
        if (mem_valid && mem_ready) begin
            if (mem_instr)
                $display("%d | ifetch 0x%08x: 0x%08x", clock_counter, mem_addr, mem_rdata);
            else if (mem_wstrb)
                $display("%d | write  0x%08x: 0x%08x (wstrb=%b)", clock_counter, mem_addr, mem_wdata, mem_wstrb);
            else
                $display("%d | read   0x%08x: 0x%08x", clock_counter, mem_addr, mem_rdata);
        end
    end

    //wire [20816:0] glitches = 20817'b0;

    picorv32 #(
    ) cpu0 (
        .clk(clk),
        .resetn(rst_n),
        .mem_valid(mem_valid),
        .mem_instr(mem_instr),
        .mem_ready(mem_ready),
        .mem_addr(mem_addr),
        .mem_wdata(mem_wdata),
        .mem_wstrb(mem_wstrb),
        .mem_rdata(mem_rdata)//,
        //.glitches(glitches)
    );

    // logic for testbench memory
    integer test_status;
    always @(posedge clk) begin
        mem_ready <= 0;
        if (mem_valid && !mem_ready) begin
            if (mem_addr < 'h100000 * 4) begin
                mem_ready <= 1;
                mem_rdata <= memory[mem_addr >> 2];
                if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
                if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
                if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
                if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
	end else if (mem_addr == 'hb0000000) begin
		mem_ready <= 1;
	        test_status = mem_wdata[7:0];
		$display("Test status set to %d", test_status);
		if ((test_status == 1) || (test_status == 2)) begin
			$display("End of test");
			$finish();
		end
	end else begin
                //$display("unmapped memory read 0x%08x", mem_addr);
		// Just set all to 0
		mem_ready <= 1;
		mem_rdata <= 0;
            end
        end
    end

    // glitching logic
    always @(posedge clk) begin
	    if (clock_counter == 2780) begin
		    force cpu0.mem_wdata[7:0] = 'hff; // pin 697
		    $display("Faulting %d", cpu0.mem_wdata[1]);
		    repeat (1) @(posedge clk);
		    $display("Stopping fault");
		    release cpu0.mem_wdata[1];
            end
    end
endmodule
