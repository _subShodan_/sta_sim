#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <assert.h>

#include "Vpicorv32.h"
#include "verilated.h"
#include "../bitlified.h"

#define COMMAND_VERSION 0xABCD0000
#define COMMAND_GLITCH 0xABCD0001
#define COMMAND_EXIT 0xABCD0002

#define UART0_BASE_ADDRESS          0xA0000000
#define UART0_STATUS                0x00000000
#define UART0_TX_DATA               0x00000004
#define UART0_RX_DATA               0x00000008

#define TEST_STATUS 0xB0000000

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define MSG_TEST_STATUS 0x01
#define MSG_CRASHED 0x02
#define MSG_PID 0x03
#define MSG_NUM_MACHINES 0x04
#define MSG_STATUS 0x05
#define MSG_CMD 0x06
#define MSG_GLITCHBUF 0x07
#define MSG_ARGS 0x08


#define TEST_NO_FAULT (0 << 6)
#define TEST_EXPLOITABLE (1 << 6)
#define TEST_UNEXPLOITABLE (2 << 6)
#define TEST_UNKNOWN_FAULT (3 << 6)

#define TEST_START (TEST_UNEXPLOITABLE | 0)
#define TEST_FI_SUCCESS (TEST_EXPLOITABLE | 0)
#define TEST_FI_RUNNING (TEST_NO_FAULT | 0)
#define TEST_FI_FAIL (TEST_NO_FAULT | 1)
#define TEST_TRIGGER_UP (TEST_UNEXPLOITABLE | 2)
#define TEST_TRIGGER_DOWN (TEST_UNEXPLOITABLE | 3)
#define TEST_CRASHED (TEST_UNEXPLOITABLE | 4)

#define TEST_DISABLED (TEST_UNKNOWN_FAULT)

#include <errno.h>
#include <string.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[%d][ERROR] (%s:%d: errno: %s) " M "\n", getpid(), __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) { log_error(M, ##__VA_ARGS__); assert(A); }


uint32_t memory[0x100000][MACHINES];
unsigned int clock_counter = 0;
uint8_t test_status[MACHINES] = {TEST_FI_RUNNING};
uint8_t prev_status[MACHINES] = {TEST_FI_RUNNING};
int verbose = 0;
bool logpid = true;

typedef uint8_t glitchbuf[MACHINES][bytes_in(glitches)];

void log_verbose(int level, const char* fmt, ...) {
	if (verbose >= level) {
		if (logpid)
			printf("[%d] ", getpid());
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}

void readmemh(const char *filename) {
	FILE *fp;
	unsigned int i;

	fp = fopen(filename, "rb");
	assertf(fp != NULL, "Unable to open hexfile %s", filename);

	i = 0;

	while(!feof(fp)) {
		uint32_t word;
		fscanf(fp, "%08X\n", &word);
		for (int vm = 0; vm < MACHINES; vm++)
			memory[i][vm] = word;
		i++;
	}

	log_verbose(1, "Read %x words\n", i);

	fclose(fp);
}

void manage_memory(Vpicorv32 *top) {
	for (int vm = 0; vm < MACHINES; vm++) {
		if (test_status[vm] == TEST_FI_FAIL || 
            test_status[vm] == TEST_FI_SUCCESS ||
            test_status[vm] == TEST_CRASHED ||
            test_status[vm] == TEST_DISABLED) {
            log_verbose(4, "Machine %d disabled because of status %d\n", vm, test_status[vm]);
        } else {
			uint32_t mem_addr = get_mem_addr(top, vm);
			uint32_t mem_wstrb = get_mem_wstrb(top, vm);
			uint32_t mem_wdata = get_mem_wdata(top, vm);
			uint32_t mem_rdata = get_mem_rdata(top, vm);
            bool mem_instr; 
			bool mem_valid = get_mem_valid(top, vm);
			bool mem_ready = get_mem_ready(top, vm);

			if(mem_valid && !mem_ready) {
			    log_verbose(4, "in (%d):  mem_addr %08x, mem_wstrb %08x, mem_wdata %08x, mem_rdata %08x, mem_valid %d, mem_ready %d\n", vm, mem_addr, mem_wstrb, mem_wdata, mem_rdata, mem_valid, mem_ready);
				if(mem_addr < 0x100000 * 4) {
					set_mem_ready(top, vm, 1);
					set_mem_rdata(top, vm, memory[mem_addr >> 2][vm]);
					//	    printf("   vm %d, memory read is %08x\n", vm, memory[mem_addr >> 2][vm]);
					if(mem_wstrb & 1) {
						memory[mem_addr >> 2][vm] = (memory[mem_addr >> 2][vm] & 0x00FFFFFF) | (mem_wdata & 0xFF000000);
					}
					if (mem_wstrb & 2) {
						memory[mem_addr >> 2][vm] = (memory[mem_addr >> 2][vm] & 0xFF00FFFF) | (mem_wdata & 0x00FF0000);
					}
					if (mem_wstrb & 4) {
						memory[mem_addr >> 2][vm] = (memory[mem_addr >> 2][vm] & 0xFFFF00FF) | (mem_wdata & 0x0000FF00);
					}
					if (mem_wstrb & 8) {
						memory[mem_addr >> 2][vm] = (memory[mem_addr >> 2][vm] & 0xFFFFFF00) | (mem_wdata & 0x000000FF);
					}
				} else if(mem_addr == UART0_BASE_ADDRESS + UART0_TX_DATA) {
					// TODO: fix memory mapping and loading in the sample to make data section work and have string printed properly
					set_mem_ready(top, vm, 1);
					if (verbose > 1) {
						printf(ANSI_COLOR_BLUE);
						putchar(mem_wdata & 0x000000FF);
						printf(ANSI_COLOR_RESET);
					}
				} else if(mem_addr == UART0_BASE_ADDRESS + UART0_RX_DATA) {
					set_mem_ready(top, vm, 1);
					set_mem_rdata(top, vm, 0); // Read always returns 0 from UART
				} else if (mem_addr == TEST_STATUS) {
					set_mem_ready(top, vm, 1);
					uint8_t status = mem_wdata & 0x000000FF;
					if (status == prev_status[vm]) {
						test_status[vm] = status;
						log_verbose(2, "vm %d: setting test status to %d at clock %d\n", vm, status, clock_counter);
					} else {
						prev_status[vm] = ~status; // It's first written bitflipped, to avoid FI on this code
						log_verbose(3, "vm %d: trigger for test status to %d is set at clock %d\n", vm, ~status, clock_counter);
					}
				} else {
					// TODO: add if it was read or read/write
					log_verbose(2, "vm %d: unmapped memory 0x%08x [strb %i%i%i%i]\n", vm, mem_addr, (mem_wstrb & 1) ? 1 : 0, (mem_wstrb & 2) ? 1 : 0, (mem_wstrb & 4) ? 1 : 0, (mem_wstrb & 8) ? 1 : 0);
					// return 1; XXX only return 1 if all VMs crashed
					test_status[vm] = TEST_CRASHED;
				}
    			if (verbose > 3){
	    			mem_addr = get_mem_addr(top, vm);
		    		mem_wstrb = get_mem_wstrb(top, vm);
			    	mem_wdata = get_mem_wdata(top, vm);
				    mem_rdata = get_mem_rdata(top, vm);
    				mem_valid = get_mem_valid(top, vm);
	    			mem_ready = get_mem_ready(top, vm);
                    mem_instr = get_mem_instr(top, vm);
		    		log_verbose(4, "out(%d):  mem_addr %08x, mem_wstrb %08x, mem_wdata %08x, mem_rdata %08x, mem_valid %d, mem_ready %d, mem_instr %d\n", vm, mem_addr, mem_wstrb, mem_wdata, mem_rdata, mem_valid, mem_ready, mem_instr);
		    	}
			} else {
				set_mem_ready(top, vm, 0);
			}

		}
	}
}

int connect(char *address) {
	struct sockaddr_un addr;
	int fd;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	assertf(fd >= 0, "Unable to connect to %s", address);

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, address, sizeof(addr.sun_path) - 1);

	int res = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	assertf(res >= 0, "Unable to connect to pipe");

	return fd;
}

Vpicorv32 *freshcore() {
	Vpicorv32 *top = new Vpicorv32;

	// reset
	top->clk = 0;
	set_resetn_all(top, 0);
	for(int i = 0; i < 10; i++) {
		top->eval();
		top->clk ^= 1;
	}
	set_resetn_all(top, 1);

	top->eval();
	top->clk ^= 1;

	return top;
}

void injectfaults(Vpicorv32 *top, glitchbuf glitches) {
	// XXX the order of bits needs to be checked
	log_verbose(2, "Inserting glitches at cycle %d\n", clock_counter);
	for (int vm = 0; vm < MACHINES; vm++) {
		set_glitches_buf(top, vm, &(glitches[vm][0]));
		if (verbose > 3) {
			int total = 0;
			for (int glitch = 0; glitch < bytes_in(glitches); glitch++) 
				total += __builtin_popcount(glitches[vm][glitch]);

			log_verbose(4, "Glitches for vm %d = %d, pins: ", vm, total);
			loop_glitches(top, vm, print_if_bit_1, NULL);
			printf("\n"); // B/c log_verbose doesn't
		}
	}
}

void runcore(Vpicorv32 *top, uint32_t machines, uint32_t clocks, uint32_t start, uint32_t width, glitchbuf glitches) {
	// Disable not used machines
	for (int i = machines; i < MACHINES; i++) {
		test_status[i] = TEST_DISABLED;
	}

	// tick for a finite number of cycles
	for(int i = 0; i < clocks * 2; i++) {
		log_verbose(4, "clock %d\n", clock_counter);
		if(top->clk) {
			clock_counter++;

			if (width > 0) {
				if(clock_counter == start) {
					injectfaults(top, glitches);
				} else if(clock_counter == start + width) {
					set_glitches_zero(top);
				}
			}
		}

		manage_memory(top);

		top->eval();
		top->clk ^= 1;
	}

	delete top;
}

void recvwait(int cli, void* buffer, size_t size) {
	char *buf = (char*)buffer;
	while (size > 0) {
		ssize_t num = read(cli, buf, size);
		assertf(num > 0, "%d unexpected disconnect", cli);

		buf += num;
		size -= num;
	}
}

void receivebuf(int cli, uint8_t id, void* buffer, size_t size) {
	log_verbose(3, "Waiting for %ld bytes from %d, expecting id %02x\n", size, cli, id);
	uint8_t recv_id;
    uint32_t recv_size;

	recvwait(cli, &recv_id, sizeof(recv_id));
	assertf(id == recv_id, "Received wrong ID, is %02x, should be %02x; cli=%d", recv_id, id, cli);

	recvwait(cli, &recv_size, sizeof(recv_size));
	assertf(size == recv_size, "Received wrong number of bytes, is %d, should be %zu; cli=%d, msg_id=%02x", recv_size, size, cli, id);

	recvwait(cli, buffer, size);
	log_verbose(3, "Done receiving bytes from %d\n", cli);
}

void sendbuf(int cli, uint8_t id, void* buffer, size_t size) {
	log_verbose(3, "Sending %ld bytes to %d, with id %02x\n", size, cli, id);

	ssize_t bytes = write(cli, &id, sizeof(id));
	assertf(bytes == sizeof(id), "Unexpected number of bytes written. %zd should be %lu", bytes, sizeof(id));

    bytes = write(cli, &size, 4); // XXX implicit truncate, but should never send this much data
	assertf(bytes == 4, "Unexpected number of bytes written. %zd should be %d", bytes, 4);

	bytes = write(cli, buffer, size);
	assertf(bytes == size, "Unexpected number of bytes written. %zd should be %zu", bytes, size);

	log_verbose(3, "Done %ld bytes to %d\n", size, cli);
}

void send_results(int cli) {
	log_verbose(2, "Client sending results to parent\n");

	sendbuf(cli, MSG_TEST_STATUS, test_status, sizeof(test_status));

	log_verbose(2, "Done sending results to parent\n");
}

void results_to_parent(int child, int parent) {
	log_verbose(2, "Sending results to orchestrator\n");

	uint8_t tmpbuf[MACHINES];
	// test_status
	receivebuf(child, MSG_TEST_STATUS, tmpbuf, sizeof(tmpbuf));
	sendbuf(parent, MSG_TEST_STATUS, tmpbuf, sizeof(tmpbuf));

	log_verbose(2, "Done sending results to orchestrator\n");
}

uint32_t do_glitch(Vpicorv32 *top, int fcmd, int fout, int clocks, uint32_t machines) {
	uint32_t status;
	
	// Allocate glitches buffer
	glitchbuf glitches = {0};
	assertf(sizeof(glitches[0])*MACHINES == sizeof(glitches), "glitchbuf somehow not packed...") // Just double-check it's packed

	// receive arguments
	struct {
		uint32_t clocks;
		uint32_t start;
		uint32_t width;
	} __attribute__((packed)) arguments;
	if (clocks < 0) {
		receivebuf(fcmd, MSG_ARGS, &arguments, sizeof(arguments));

		// TODO: make this a shared memory region
		// receive glitches
		receivebuf(fcmd, MSG_GLITCHBUF, glitches, machines * sizeof(glitches[0]));
	} else {
		arguments.clocks = clocks;
		arguments.start = 0;
		arguments.width = 0;
	}

	// Create pipe for child comms
	int pipe_fds[2];
	pipe2(pipe_fds, O_CLOEXEC); // Close on exit, so we can detect a child when it closes and we're stuck on a recv()

	// We fork here, so the parent stays in the 'just initialized' state.
	// Jasper did an experiment removing fork() and just recalculating state; about the same speed. Leaving fork() in so later we can use it to do multiple faults in a row from one process
	int fd = fork();
	if(fd == 0) {
		close(pipe_fds[0]);
		// child process
		// TODO: redirect stdio/stderr to somewhere
		log_verbose(1, "glitch | machines: %i clocks: %i start: %i width: %i\n", machines, arguments.clocks, arguments.start, arguments.width);
		runcore(top, machines, arguments.clocks, arguments.start, arguments.width, glitches);
		send_results(pipe_fds[1]);
		exit(0);
	}
    log_verbose(1, "Spawned child %d\n", fd);

	close(pipe_fds[1]);

	// Read results from child
	results_to_parent(pipe_fds[0], fout);

	// wait for child to exit
	wait((int *)&status);

	return status;
}


int main(int argc, char **argv, char **env) {
	Verilated::commandArgs(argc, argv);

	verbose = 0;
	char *hexfile = (char*)"rv32i.hex";
	char *address = NULL;
	char *infile = NULL;
	uint32_t machines = MACHINES;
	int c;
	bool nofault = false;
	int clocks = -1;
	while ((c = getopt(argc, argv, "n:vs:m:f:c:")) != -1) {
		log_verbose(4, "Argument: %c -> %s\n", c, optarg);
		switch (c) {
			case 's':
				machines = atoi(optarg);
				break;
			case 'v':
				verbose++;
				break;
			case 'f':
				infile = optarg;
				break;
			case 'm':
				hexfile = optarg;
				break;
			case 'c':
				address = optarg;
				break;
			case 'n':
				clocks = atoi(optarg);
				break;
			default:
				assertf(false, "Unknown command line option %c", c);
		}
	}
	
	// Check arguments
	if (clocks >= 0)
		assertf(infile == NULL && address == NULL, "Must specify only one of address, infile or clocks");
	if (infile != NULL)
		assertf(clocks < 0 && address == NULL, "Must specify only one of address, infile or clocks");
	if (address != NULL)
		assertf(infile == NULL && clocks < 0, "Must specify only one of address, infile or clocks");
	assertf(machines <= MACHINES, "Cannot simulate more than machines (%d <= %d)", machines, MACHINES);

	// Read memory
	readmemh(hexfile);
	
	// Create fresh core
	Vpicorv32 *top = freshcore();

	// connect to server
	int fcmd, fout;
	if (address) {
		log_verbose(1, "Connecting to %s\n", address);
		fcmd = connect(address);
		fout = fcmd;
	} else if (infile) {
		log_verbose(1, "Reading from %s\n", infile);
		fcmd = open(infile, O_RDONLY);
		fout = open("/dev/null", O_WRONLY); // We don't care about output when reading from file
		logpid = false;
	} else { // clocks
		fcmd = open("/dev/zero", O_RDONLY); // don't care
		fout = open("/dev/null", O_WRONLY); // don't care
		logpid = false;
		uint32_t status = do_glitch(top, fcmd, fout, clocks, machines);
		return status; // We're done
	}
	assertf(fcmd >= 0, "Can't open command input");
	assertf(fout >= 0, "Can't open output");


	log_verbose(1, "Sending serving ping\n");

	// tell server our pid as a ping
	uint32_t pid = getpid();
	sendbuf(fout, MSG_PID, &pid, sizeof(pid));

	// Send number of machines
	sendbuf(fout, MSG_NUM_MACHINES, &machines, sizeof(machines));

	while(1) {
		uint32_t status = 0xFFFFFFFF;

		// wait for command
		unsigned int cmd = 0;
		receivebuf(fcmd, MSG_CMD, &cmd, sizeof(cmd));

		if(cmd == COMMAND_VERSION) {
			status = 1337;
		} else if(cmd == COMMAND_GLITCH) {
			status = do_glitch(top, fcmd, fout, clocks, machines);
		} else if(cmd == COMMAND_EXIT) {
			break;
		}

		sendbuf(fout, MSG_STATUS, &status, sizeof(status));
	}

	close(fout);
	close(fcmd);
	exit(0);
}
