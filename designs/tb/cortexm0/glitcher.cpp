/* Copyright 2021 Riscure */

#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <assert.h>

#include "verilated.h"
#include "verilated_vcd_c.h"
#include "../bitlified.h"

#define COMMAND_VERSION 0xABCD0000
#define COMMAND_GLITCH 0xABCD0001
#define COMMAND_EXIT 0xABCD0002

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define MSG_TEST_STATUS 0x01
#define MSG_PID 0x03
#define MSG_NUM_MACHINES 0x04
#define MSG_STATUS 0x05
#define MSG_CMD 0x06
#define MSG_GLITCHBUF 0x07
#define MSG_ARGS 0x08
#define MSG_EXTRA_DATA_SIZE 0x09
#define MSG_EXTRA_DATA 0x10

#define TEST_NO_FAULT (0 << 6)
#define TEST_EXPLOITABLE (1 << 6)
#define TEST_UNEXPLOITABLE (2 << 6)
#define TEST_UNKNOWN_FAULT (3 << 6)
#define TEST_DONE (0xfe)
#define TEST_RUNNING (0xff)

#include <errno.h>
#include <string.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[%d][ERROR] (%s:%d: errno: %s) " M "\n", getpid(), __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) { log_error(M, ##__VA_ARGS__); assert(A); }

int verbose = 0;
bool logpid = true;

void log_verbose(int level, const char* fmt, ...) {
	if (verbose >= level) {
		if (logpid)
			printf("[%d] ", getpid());
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}

// Hack....
#include "target_m0.cpp"

unsigned int clock_counter = 0;
uint8_t test_status[MACHINES] = {TEST_NO_FAULT};
typedef uint8_t glitchbuf[MACHINES][bytes_in(glitches)];
uint8_t extra_data[MACHINES][EXTRA_DATA_SIZE] = {0};

int connect(char *address) {
	struct sockaddr_un addr;
	int fd;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	assertf(fd >= 0, "Unable to connect to %s", address);

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, address, sizeof(addr.sun_path) - 1);

	int res = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	assertf(res >= 0, "Unable to connect to pipe");

	return fd;
}

void injectfaults(Vtop *top, glitchbuf glitches) {
	log_verbose(2, "Inserting glitches at cycle %d\n", clock_counter);
	for (int vm = 0; vm < MACHINES; vm++) {
		//set_glitches_buf(top, vm, &(glitches[vm][0]));
        handle_fault(top, vm, &(glitches[vm][0]));
		if (verbose > 3) {
			int total = 0;
			for (int glitch = 0; glitch < bytes_in(glitches); glitch++) 
				total += __builtin_popcount(glitches[vm][glitch]);

			log_verbose(4, "Glitches for vm %d = %d, pins: ", vm, total);
			loop_glitches(top, vm, print_if_bit_1, NULL);
			printf("\n"); // B/c log_verbose doesn't
		}
	}
}

void runcore(Vtop *top, char* tracefile, uint32_t machines, uint32_t clocks, uint32_t start, uint32_t width, glitchbuf glitches) {
	// Disable not used machines
	for (int i = 0; i < machines; i++) {
        test_status[i] = TEST_RUNNING;
    }
	for (int i = machines; i < MACHINES; i++) {
		test_status[i] = TEST_DONE;
	}

	// Open trace file
	VerilatedVcdC* tfp = NULL;
	if (tracefile != NULL) {
		Verilated::traceEverOn(true);
		tfp = new VerilatedVcdC();
		top->trace(tfp, 99); // Trace 99 levels of hierarchy
		tfp->open(tracefile);
	}

	// tick for a finite number of cycles
	int num_done = 0;
	int i; 
    log_verbose(4, "Total clocks: %d\n", clocks);
	for(i = 0; (i < clocks * 2) && (num_done < machines); i++) {
		log_verbose(4, "clock %d\n", clock_counter);

		// Inject faults
		if(top->clk_i) {
			clock_counter++;

			if (width > 0) {
				if(clock_counter == start) {
					injectfaults(top, glitches);
				} else if(clock_counter == start + width) {
					set_glitches_zero(top);
				}
			}
		}

		// Evaluate clock cycle
		top->eval();

		// Check test is done, handle tick and update status
		num_done = 0;
		for (int vm = 0; vm < machines; vm++) {
            if (test_status[vm] == TEST_RUNNING) 
                test_status[vm] = handle_tick(top, vm);
            else
                log_verbose(4, "Machine %d not handling ticks because of status %x", vm, test_status[vm]);

	        if (test_status[vm] != TEST_RUNNING)
	            num_done++;
        }

        // Next clock
		top->clk_i ^= 1;

		// Dump if we need to
		if (tfp != NULL)
		    tfp->dump(i);
	}  
	log_verbose(2, "Test done after %d cycles\n", i/2);

	if (tfp != NULL)
		tfp->close();
	
	for (int vm = 0; vm < machines; vm++) {
        test_status[vm] = obtain_test_status(top, vm, test_status[vm], (uint8_t*)&extra_data[vm]);
		assertf(test_status[vm] != TEST_RUNNING && test_status[vm] != TEST_DONE, "obtain_test_status(top, %d) didn't set correct test status", vm);
	}

	delete top;
}

void recvwait(int cli, void* buffer, size_t size) {
	char *buf = (char*)buffer;
	while (size > 0) {
		ssize_t num = read(cli, buf, size);
		assertf(num > 0, "%d unexpected disconnect", cli);

		buf += num;
		size -= num;
	}
}

void receivebuf(int cli, uint8_t id, void* buffer, size_t size) {
	log_verbose(3, "Waiting for %ld bytes from %d, expecting id %02x\n", size, cli, id);
	uint8_t recv_id;
	uint32_t recv_size;

	recvwait(cli, &recv_id, sizeof(recv_id));
	assertf(id == recv_id, "Received wrong ID, is %02x, should be %02x; cli=%d", recv_id, id, cli);

	recvwait(cli, &recv_size, sizeof(recv_size));
	assertf(size == recv_size, "Received wrong number of bytes, is %d, should be %zu; cli=%d, msg_id=%02x", recv_size, size, cli, id);

	recvwait(cli, buffer, size);
	log_verbose(3, "Done receiving bytes from %d\n", cli);
}

void sendbuf(int cli, uint8_t id, void* buffer, size_t size) {
	log_verbose(3, "Sending %ld bytes to %d, with id %02x\n", size, cli, id);

	ssize_t bytes = write(cli, &id, sizeof(id));
	assertf(bytes == sizeof(id), "Unexpected number of bytes written. %zd should be %lu", bytes, sizeof(id));

    assertf(size <= 0xffffffff, "Size of %zu truncated", size);
	bytes = write(cli, &size, 4); 
	assertf(bytes == 4, "Unexpected number of bytes written. %zd should be %d", bytes, 4);

	bytes = write(cli, buffer, size);
	assertf(bytes == size, "Unexpected number of bytes written. %zd should be %zu", bytes, size);

	log_verbose(3, "Done %ld bytes to %d\n", size, cli);
}

void send_results(int cli) {
	log_verbose(2, "Client sending results to parent\n");

	sendbuf(cli, MSG_TEST_STATUS, test_status, sizeof(test_status));
    sendbuf(cli, MSG_EXTRA_DATA, extra_data, sizeof(extra_data));

	log_verbose(2, "Done sending results to parent\n");
}

void results_to_parent(int child, int parent) {
	log_verbose(2, "Sending results to orchestrator\n");

	uint8_t tmpbuf[MACHINES];
	uint8_t tmpbuf2[MACHINES][EXTRA_DATA_SIZE];
	// test_status
	receivebuf(child, MSG_TEST_STATUS, tmpbuf, sizeof(tmpbuf));
	sendbuf(parent, MSG_TEST_STATUS, tmpbuf, sizeof(tmpbuf));
    receivebuf(child, MSG_EXTRA_DATA, tmpbuf2, sizeof(tmpbuf2));
    sendbuf(parent, MSG_EXTRA_DATA, tmpbuf2, sizeof(tmpbuf2));

	log_verbose(2, "Done sending results to orchestrator\n");
}

uint32_t do_glitch(Vtop *top, char* tracefile, int fcmd, int fout, int clocks, uint32_t machines) {
	uint32_t status;
	
	// Allocate glitches buffer
	glitchbuf glitches = {0};
	assertf(sizeof(glitches[0])*MACHINES == sizeof(glitches), "glitchbuf somehow not packed...") // Just double-check it's packed

	// receive arguments
	struct {
		uint32_t clocks;
		uint32_t start;
		uint32_t width;
	} __attribute__((packed)) arguments;
	if (clocks < 0) {
		receivebuf(fcmd, MSG_ARGS, &arguments, sizeof(arguments));

		// TODO: make this a shared memory region
		// receive glitches
		receivebuf(fcmd, MSG_GLITCHBUF, glitches, machines * sizeof(glitches[0]));
	} else {
		arguments.clocks = clocks;
		arguments.start = 0;
		arguments.width = 0;
	}

	// Create pipe for child comms
	int pipe_fds[2];
	pipe2(pipe_fds, O_CLOEXEC); // Close on exit, so we can detect a child when it closes and we're stuck on a recv()

	// We fork here, so the parent stays in the 'just initialized' state.
	// Jasper did an experiment removing fork() and just recalculating state; about the same speed. Leaving fork() in so later we can use it to do multiple faults in a row from one process
	int fd = fork();
	if(fd == 0) {
		close(pipe_fds[0]);
		// child process
		// TODO: redirect stdio/stderr to somewhere
		log_verbose(1, "glitch | machines: %i clocks: %i start: %i width: %i\n", machines, arguments.clocks, arguments.start, arguments.width);
		runcore(top, tracefile, machines, arguments.clocks, arguments.start, arguments.width, glitches);
		send_results(pipe_fds[1]);

		exit(0);
	}
		log_verbose(1, "Spawned child %d\n", fd);

	close(pipe_fds[1]);

	// Read results from child
	results_to_parent(pipe_fds[0], fout);

	// wait for child to exit
	wait((int *)&status);

	return status;
}


int main(int argc, char **argv, char **env) {
	Verilated::commandArgs(argc, argv);

	verbose = 0;
	char *address = NULL;
	char *infile = NULL;
	char *tracefile = NULL;
    char *tbinput = NULL;
	uint32_t machines = MACHINES;
	int c;
	bool nofault = false;
	int clocks = -1;
	while ((c = getopt(argc, argv, "n:vs:m:f:i:c:t:")) != -1) {
		log_verbose(4, "Argument: %c -> %s\n", c, optarg);
		switch (c) {
			case 's':
				machines = atoi(optarg);
				break;
			case 'v':
				verbose++;
				break;
			case 'f':
				infile = optarg;
				break;
            case 'i':
                tbinput = optarg;
                break;
			case 'c':
				address = optarg;
				break;
			case 'n':
				clocks = atoi(optarg);
				break;
			case 't':
				tracefile = optarg;
				break;
			default:
				assertf(false, "Unknown command line option %c", c);
		}
	}
	
	// Check arguments
	if (clocks >= 0)
		assertf(infile == NULL && address == NULL, "Must specify only one of address, infile or clocks");
	if (infile != NULL)
		assertf(clocks < 0 && address == NULL, "Must specify only one of address, infile or clocks");
	if (address != NULL)
		assertf(infile == NULL && clocks < 0, "Must specify only one of address, infile or clocks");
	assertf(!(tracefile != NULL && address != NULL), "Cannot trace when using socket address mode");
	assertf(machines <= MACHINES, "Cannot simulate more than machines (%d <= %d)", machines, MACHINES);

	// Create fresh core
	Vtop *top = freshcore(tbinput);
	int fcmd, fout;
	if (address) {
		// connect to server
		log_verbose(1, "Connecting to %s\n", address);
		fcmd = connect(address);
		fout = fcmd;
	} else if (infile) {
		log_verbose(1, "Reading from %s\n", infile);
		fcmd = open(infile, O_RDONLY);
		fout = open("/dev/null", O_WRONLY); // We don't care about output when reading from file
		logpid = false;
	} else { // clocks
		fcmd = open("/dev/zero", O_RDONLY); // don't care
		fout = open("/dev/null", O_WRONLY); // don't care
		logpid = false;
		uint32_t status = do_glitch(top, tracefile, fcmd, fout, clocks, machines);
		return status; // We're done
	}
	assertf(fcmd >= 0, "Can't open command input");
	assertf(fout >= 0, "Can't open output");


	log_verbose(1, "Sending serving ping\n");

	// tell server our pid as a ping
	uint32_t pid = getpid();
	sendbuf(fout, MSG_PID, &pid, sizeof(pid));

	// Send number of machines
	sendbuf(fout, MSG_NUM_MACHINES, &machines, sizeof(machines));

    // Send extra data size
    uint32_t extra_data_size = EXTRA_DATA_SIZE;
    sendbuf(fout, MSG_EXTRA_DATA_SIZE, &extra_data_size, sizeof(extra_data_size));

	while(1) {
		uint32_t status = 0xFFFFFFFF;

		// wait for command
		unsigned int cmd = 0;
		receivebuf(fcmd, MSG_CMD, &cmd, sizeof(cmd));

		if(cmd == COMMAND_VERSION) {
			status = 1337;
		} else if(cmd == COMMAND_GLITCH) {
			status = do_glitch(top, tracefile, fcmd, fout, clocks, machines);
		} else if(cmd == COMMAND_EXIT) {
			break;
		}

		sendbuf(fout, MSG_STATUS, &status, sizeof(status));
	}

	close(fout);
	close(fcmd);

	exit(0);
}
