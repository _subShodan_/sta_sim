/*  Copyright 2021 Riscure */

#include "Vnlsim_wrapper.h"

#define UART0_BASE_ADDRESS          0xA0000000
#define UART0_STATUS                0x00000000
#define UART0_TX_DATA               0x00000004
#define UART0_RX_DATA               0x00000008

#define TEST_STATUS 0xB0000000


/* HSIZE constants as per AMBA 3 AHB-Lite Protocol Specification. Dont't change these values,
 * as they are coming from the hardware! */
enum SIM_HSIZE {
    HSIZE_8_BITS = 0,
    HSIZE_16_BITS = 1,
    HSIZE_32_BITS = 2,
};

typedef Vnlsim_wrapper Vtop;
#define MEMORY_SIZE 0x05000000 
#define MEM_BLOCK_SHIFT 20
#define NUM_MEM_BLOCK ((0xffffffff >> MEM_BLOCK_SHIFT) + 1)
#define MEM_BLOCK_SIZE (1 << MEM_BLOCK_SHIFT)
#define BLOCK_ADDR_MASK (MEM_BLOCK_SIZE - 1)
#define MEM_ALLOC_SIZE ((1 << MEM_BLOCK_SHIFT) * MACHINES)
uint32_t* memory_block[NUM_MEM_BLOCK] = {NULL};

#define TEST_START (TEST_UNEXPLOITABLE | 0)
#define TEST_FI_SUCCESS (TEST_EXPLOITABLE | 0)
#define TEST_FI_RUNNING (TEST_NO_FAULT | 0)
#define TEST_FI_FAIL (TEST_NO_FAULT | 1)
#define TEST_FI_TIMEOUT (TEST_NO_FAULT | 2)
#define TEST_TRIGGER_UP (TEST_UNEXPLOITABLE | 2)
#define TEST_TRIGGER_DOWN (TEST_UNEXPLOITABLE | 3)
#define TEST_CRASHED (TEST_UNEXPLOITABLE | 4)

#define EXTRA_DATA_SIZE 4 // Just the PC

uint32_t pc_at_fault;

uint32_t* memory_ptr(uint32_t address, int vm) {
    if (address >= MEMORY_SIZE)
        return NULL;

    // Dynamically allocate memory if needed
    uint32_t block = address >> MEM_BLOCK_SHIFT; // Calc block number
    uint32_t* memory = memory_block[block]; // Get pointer. Can't overflow buffer.
    if (memory == NULL) {
        log_verbose(2, "Allocating 0x%x bytes of memory for block 0x%x. Addr = 0x%08x\n", MEM_ALLOC_SIZE, block, address);

        memory = (uint32_t*)calloc(MEM_ALLOC_SIZE, 1);
        assertf(memory != NULL, "Couldn't allocate machine memory of %d size", MEM_ALLOC_SIZE);

        memory_block[block] = memory;
    }

    // Return right memory ptr 
    uint32_t block_addr = address & BLOCK_ADDR_MASK;
    return &(memory[((size_t)block_addr >> 2)*MACHINES + (size_t)vm]); // Note this is layed out such that address x for all vms is consecutive in memory. Should help with caches. Each vm has 4 bytes
}

void readmemh(const char *filename) {
	FILE *fp;
	size_t i;

	fp = fopen(filename, "rb");
	assertf(fp != NULL, "Unable to open hexfile %s", filename);

	i = 0;
    size_t nr = 1;
    // XXX BUG: doesn't read entire memory image if the size is not full words
	while(!feof(fp) && nr > 0) {
		uint32_t word;
//		fscanf( fp, "%08X\n", &word); // Hex file
        nr = fread(&word, sizeof(word), 1, fp);
        if (nr > 0) {
    		for (int vm = 0; vm < MACHINES; vm++) {
                uint32_t *addr = memory_ptr(i*4, vm);
                *addr = word;
            }
		    i++;
        }
	}

	log_verbose(1, "Read %zu words, %zu bytes from %s\n", i, i*4, filename);

    // Dump memory and check
    for (int j = 0; j < i; j++) {
        uint32_t value;
        int vm = 0;
        uint32_t *addr = memory_ptr(j*4, vm);
        value = *addr;
 
        // Every 16 bytes, 4 dwords print header
        if (j % 4 == 0) {
            log_verbose(4, "%08x: ", j*4); 
        }
        bool old_log = logpid; // Avoid printing pid in line
        logpid = false;
        log_verbose(4, "%08x ", value);
        if (j % 4 == 3) {
            log_verbose(4, "\n");
        }
        logpid = old_log;

        // Check rest is the same
   		for (vm = 1; vm < MACHINES; vm++) {
            uint32_t *addr = memory_ptr(j*4, vm);
            assertf(*addr == value, "VM %d at address %08x has value %08x and not value %08x\n", vm, j*4, *addr, value);
        }
    }

	fclose(fp);
}


Vtop *freshcore(const char *inputfile) {
	Vtop *top = new Vtop;

    // load code into memory
    readmemh(inputfile);

	// reset
	top->clk_i = 0;
	set_rst_ni_all(top, 0);

    set_NLSIM_READ_DATA_all(top, 0);
    set_NLSIM_READY_all(top, 1);

    // Run for 10 cycles to reset it
	for(int i = 0; i < 10; i++) {
		top->eval();
        top->clk_i ^= 1;
	}

    // Release reset
	set_rst_ni_all(top, 1);
	top->eval();
    top->clk_i ^= 1;

   	return top;
}


uint8_t test_prev_status[MACHINES] = {TEST_RUNNING};
uint8_t test_cur_status[MACHINES] = {TEST_RUNNING};
#define CONSOLE_LINE_SIZE 256
uint8_t console_buffer[MACHINES][CONSOLE_LINE_SIZE+1] = {0}; // One for for trailing 0
size_t console_index[MACHINES] = {0};

void handle_char(uint8_t output, int vm) {
    if (console_index[vm] < CONSOLE_LINE_SIZE && output != '\n') { // EOL or out of space?
        console_buffer[vm][console_index[vm]] = output;
        console_index[vm]++;
    } else {
        console_buffer[vm][console_index[vm]] = 0; // Add null terminator
        console_index[vm] = 0; // Reset to 0
        log_verbose(1, "Console: %s%s%s\n", ANSI_COLOR_BLUE, console_buffer[vm], ANSI_COLOR_RESET);

        // XXX hacked test status check
        if (strstr((const char*)console_buffer[vm], "Entry point address =")) {
            log_verbose(2, "Found boot string, FI success\n");
            test_cur_status[vm] = TEST_FI_SUCCESS;
        }
    }
}

bool handle_write(int vm, uint32_t write_addr, SIM_HSIZE write_hsize, uint32_t write_data) {
    bool ok = false;

    uint32_t *base_addr = memory_ptr(write_addr, vm);
    if (base_addr != NULL) {
        ok = true;
        if (write_hsize == HSIZE_32_BITS) {
            uint32_t *dest = (uint32_t*)base_addr;
            *dest = (uint32_t)write_data;
        } else if (write_hsize == HSIZE_16_BITS) {
            uint16_t* dest = (uint16_t*)base_addr;
            dest += (write_addr >> 1) & 1;
            *dest = (uint16_t)(write_data & 0xFFFF);
        } else if (write_hsize == HSIZE_8_BITS) {
            uint8_t* dest = (uint8_t*)base_addr;
            dest += write_addr & 3;
            *dest = (uint8_t)(write_data & 0xFF);
        } else {
            log_verbose(2, "Unexpected HSIZE %x. Simulating crash", write_hsize);
            ok = false;
        }
        log_verbose(4, "write (%d): addr %08x, hsize %08x, write_data %08x\n", vm, write_addr, write_hsize, write_data);

    } else if (write_addr == UART0_BASE_ADDRESS + UART0_TX_DATA) {
        // Buffer incoming data, print when we get a newline
        ok = true;
        handle_char(write_data & 0xff, vm);

	} else if (write_addr == TEST_STATUS) {
        ok = true;
		uint8_t status = write_data & 0xFF;
        if (status == test_prev_status[vm]) {
            test_cur_status[vm] = status;
            log_verbose(2, "vm %d: setting test status to %02x\n", vm, (uint8_t)status);
        } else {
            test_prev_status[vm] = ~status; // It's first written bitflipped, to avoid FI on this code
            log_verbose(3, "vm %d: trigger for test status to %02x is set\n", vm, (uint8_t)~status);
        }

    } else {
	    ok = false;
        log_verbose(2, "write (%d): addr %08x, hsize %08x, write_data %08x, OUT OF BOUNDS\n", vm, write_addr, write_hsize, write_data);
    }

    return ok;
}

bool handle_read(int vm, uint32_t read_addr, SIM_HSIZE read_hsize, uint8_t read_hprot, uint32_t* result) {
    uint32_t read_data;
    bool ok = false;

    uint32_t *base_addr = memory_ptr(read_addr, vm);
    if (base_addr != NULL) {
        ok = true;
        if (read_hsize == HSIZE_32_BITS) {
            uint32_t *dest = (uint32_t*)base_addr;
            read_data = *dest;
        } else if (read_hsize == HSIZE_16_BITS) {
            uint16_t* dest = (uint16_t*)base_addr;
            dest += (read_addr >> 1) & 1;
            read_data = (*dest << 16) | (*dest);
        } else if (read_hsize == HSIZE_8_BITS) {
            uint8_t* dest = (uint8_t*)base_addr;
            dest += read_addr & 3;
            read_data = (*dest << 24) | (*dest << 16) | (*dest<< 8) | *dest;
        } else {
            log_verbose(2, "Unexpected HSIZE %x. Simulating crash", read_hsize);
            ok = false;
        }
        log_verbose(4, "read  (%d): addr %08x, hsize %08x, hprot %08x, read_data %08x\n", vm, read_addr, read_hsize, read_hprot, read_data);
    } else if (read_addr == UART0_BASE_ADDRESS + UART0_RX_DATA || read_addr == UART0_BASE_ADDRESS + UART0_TX_DATA) {
        // Always return 0 on RX/TX read
        log_verbose(4, "read  (%d): addr %08x, hsize %08x, hprot %08x, read_data %08x\n", vm, read_addr, read_hsize, read_hprot, read_data);
        ok = true;
        read_data = 0;
	} else if (read_addr == TEST_STATUS) {
        ok = true;
		uint8_t status = test_prev_status[vm];
        read_data = (status << 24) | (status << 16) | (status<< 8) | status;
        log_verbose(4, "read  (%d): addr %08x, hsize %08x, hprot %08x, read_data %08x\n", vm, read_addr, read_hsize, read_hprot, read_data);
    } else {
        read_data = 0;
        ok = false;
        log_verbose(2, "read  (%d): addr %08x, hsize %08x, hprot %08x, read_data %08x, OUT OF BOUNDS\n", vm, read_addr, read_hsize, read_hprot, read_data);
    }

    *result = read_data;
    return ok;
}

uint8_t handle_tick(Vtop* top, int vm) {
    // Handle memory access
    log_verbose(4, "Handle tick for vm %d, status %d\n", vm, test_cur_status[vm]);
    bool ok = true;
    bool wfi;
    uint8_t newstatus;

    if (top->clk_i == 0) {
        if (get_NLSIM_WRITE(top, vm)) {
            uint32_t addr = get_NLSIM_WRITE_ADDR(top, vm);
            SIM_HSIZE hsize = (SIM_HSIZE)get_NLSIM_WRITE_HSIZE(top, vm);
            uint32_t data = get_NLSIM_WRITE_DATA(top, vm);
            ok = handle_write(vm, addr, hsize, data);
        }
        if (get_NLSIM_READ(top, vm)) {
            uint32_t addr = get_NLSIM_READ_ADDR(top, vm);
            SIM_HSIZE hsize = (SIM_HSIZE)get_NLSIM_READ_HSIZE(top, vm);
            uint32_t hprot = get_HPROT(top, vm);
            uint32_t read_data;
            ok = handle_read(vm, addr, hsize, hprot, &read_data);
            set_NLSIM_READ_DATA(top, vm, read_data);
        }

        uint32_t pc = get_program_counter(top, vm);
        uint32_t* instruction_ptr = memory_ptr(pc, vm);
        wfi = (instruction_ptr == NULL) ? false : ((*instruction_ptr & 0xffff) == 0xbf30); // PC is at WFI instruction
        // Debug print PC
        log_verbose(4, "pc    (%d): %08x (%d)\n", vm, pc, wfi);
    }
        
    if (!ok) {
        // Handle out of bounds access
        newstatus = TEST_CRASHED;
        log_verbose(4, "Handle tick done for vm %d, crashed; newstatus %d\n", vm, newstatus);
    } else if (test_cur_status[vm] == TEST_FI_SUCCESS || test_cur_status[vm] == TEST_FI_FAIL) {
        // If software indicates test over, return that
        newstatus = test_cur_status[vm];
        log_verbose(4, "Handle tick done for vm %d, test done from sw; newstatus %d\n", vm, newstatus);
    } else if (get_test_done_o(top, vm) || wfi) { // XXX somehow test_done_o doesn't go true when WFI instruction is executed, so added the 'manual' detection
        // We never got a status but are done, so calling it fail        
        newstatus = TEST_FI_FAIL; 
        log_verbose(4, "Handle tick done for vm %d, test done or wfi from netlist; newstatus %d\n", vm, newstatus);
    } else {
        newstatus = TEST_RUNNING; 
        log_verbose(4, "Handle tick done for vm %d, test running; newstatus %d\n", vm, newstatus);
    }
    return newstatus;
}

uint8_t obtain_test_status(Vtop* top, int vm, uint8_t curstatus, uint8_t* extra_data) {
    // Get PC
    assertf(EXTRA_DATA_SIZE == sizeof(pc_at_fault), "Extra data size is %d but should be %lu", EXTRA_DATA_SIZE, sizeof(pc_at_fault));
    *((uint32_t*)extra_data) = pc_at_fault; 

    // Dump console by printing eol
    if (console_index[vm] > 0)
        handle_char('\n', vm);

    // Just return from own buffer
    if (curstatus == TEST_RUNNING)
	    return TEST_FI_TIMEOUT;
    return curstatus; 
}

void handle_fault(Vtop *top, int vm, void* glitches) {
    // Inject fault
    set_glitches_buf(top, vm, glitches);

    // Record pc
    pc_at_fault = get_program_counter(top, vm);
    log_verbose(2, "PC at fault is %08x\n", pc_at_fault);
}
