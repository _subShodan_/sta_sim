// John Fitzgerald

// 0x100000
//
#include <stdint.h> 

#define UART0_BASE_ADDRESS          0xA0000000
#define UART0_STATUS                0x00000000
#define UART0_TX_DATA               0x00000004
#define UART0_RX_DATA               0x00000008

#define TEST_STATUS                 0xB0000000

#define TEST_NO_FAULT (0 << 6)
#define TEST_EXPLOITABLE (1 << 6)
#define TEST_UNEXPLOITABLE (2 << 6)
#define TEST_UNKNOWN_FAULT (3 << 6)

#define TEST_START (TEST_UNEXPLOITABLE | 0)
#define TEST_FI_SUCCESS (TEST_EXPLOITABLE | 0)
#define TEST_FI_RUNNING (TEST_NO_FAULT | 0)
#define TEST_FI_FAIL (TEST_NO_FAULT | 1)
#define TEST_TRIGGER_UP (TEST_UNEXPLOITABLE | 2)
#define TEST_TRIGGER_DOWN (TEST_UNEXPLOITABLE | 3)
#define TEST_CRASHED (TEST_UNEXPLOITABLE | 4)

#define TEST_DISABLED (TEST_UNKNOWN_FAULT)


inline void set_test_status(uint8_t status) {
	// Prevent FI on setting test status itself
	volatile uint8_t* test_status;
	test_status = (uint8_t*)TEST_STATUS;
	*test_status = ~status;
	*test_status = status;
}

void debug(char *string);

void mem_test() {
    void* val = (void*)(0x04000000);
    volatile uint32_t* valptr32 = (uint32_t*)val;
    volatile uint16_t* valptr16 = (uint16_t*)val;
    volatile uint8_t* valptr8 = (uint8_t*)val;

    *(valptr32) = 0xdeadbeef;
    if (*valptr32 != 0xdeadbeef)
        debug("Read 32 failed\n");
    if (*valptr16 != 0xbeef)
        debug("Read 16.1 failed\n");
    if (*(valptr16+1) != 0xdead)
        debug("Read 16.2 failed\n");
    if (*(valptr8+3) != 0xde)
        debug("Read 8.1 failed\n");
    if (*(valptr8+2) != 0xad)
        debug("Read 8.2 failed\n");
    if (*(valptr8+1) != 0xbe)
        debug("Read 8.3 failed\n");
    if (*valptr8 != 0xef)
        debug("Read 8.4 failed\n");

    *(valptr8+3) = 0xca;
    if (*valptr32 != 0xcaadbeef)
        debug("Write 8.3 failed\n");
    *(valptr8+2) = 0xfe;
    if (*valptr32 != 0xcafebeef)
        debug("Write 8.3 failed\n");
    *(valptr8+1) = 0xba;
    if (*valptr32 != 0xcafebaef)
        debug("Write 8.3 failed\n");
    *valptr8 = 0xbe;
    if (*valptr32 != 0xcafebabe)
        debug("Write 8.4 failed\n");

    *(valptr16+1) = 0xdead;
    if (*valptr32 != 0xdeadbabe)
        debug("Write 16.1 failed\n");
    *(valptr16) = 0xbeef;
    if (*valptr32 != 0xdeadbeef)
        debug("Write 16.2 failed\n");

}

void _start(void) {
    set_test_status(TEST_START);
    debug("secureboot 2019\n");

    mem_test();

    char b = *(volatile unsigned char *)(UART0_BASE_ADDRESS + UART0_RX_DATA);
    for(int i = 0; i < 200; i++) {
        b++;
    }

    if(b == 0) {
        debug("b is zero\n");
    }

    set_test_status(TEST_TRIGGER_UP);
    char test = *(volatile unsigned char *)(UART0_BASE_ADDRESS + UART0_RX_DATA);
    if(test) {
    	set_test_status(TEST_TRIGGER_DOWN);
    	set_test_status(TEST_FI_SUCCESS);
        debug("success\n");
    } else {
    	set_test_status(TEST_TRIGGER_DOWN);
    	set_test_status(TEST_FI_FAIL);
        debug("failure\n");
    }
}

void debug(char *string) {
    char c;
    while(c = *string++) {
        *(volatile unsigned char *)(UART0_BASE_ADDRESS + UART0_TX_DATA) = c;
    }
}
