/*
 * Copyright (c) 2016-2021, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

	/* -----------------------------------------------------
	 * Setup the vector table to support SVC & MON mode.
	 * -----------------------------------------------------
	 */
/* Vector Table; Jasper stole this from Dennis boot_start.S who stole it from CMSDK */

    .align 2
    .globl __isr_vector
__isr_vector:
    .long   __StackTop                 /* Top of Stack                  */
    .long   bl1_entrypoint_thumb        /* Reset Handler                 */
    .long   NMI_Handler                 /* NMI Handler                   */
    .long   HardFault_Handler           /* Hard Fault Handler            */
    //.long   0                           /* Reserved                      */
    //.long   0                           /* Reserved                      */
    // This replaces two entry in the m0 vector table by a jump to the right place, so it actually works on the qemu which starts in "arm" mode
    // Long live polyglots!
    ldr r4, __my_entry_hack
    bx r4
__my_entry_hack:
    .long   bl1_entrypoint_thumb
    .long   0                           /* Reserved                      */
    .long   0                           /* Reserved                      */
    .long   0                           /* Reserved                      */
    .long   0                           /* Reserved                      */
    .long   0                           /* Reserved                      */
    .long   SVC_Handler                 /* SVCall Handler                */
    .long   0                           /* Debug Monitor Handler         */
    .long   0                           /* Reserved                      */
    .long   PendSV_Handler              /* PendSV Handler                */
    .long   SysTick_Handler             /* SysTick Handler               */

    /* External Interrupts */
    .long   UARTRX0_Handler             /* 16+ 0: UART 0 RX Handler          */
    .long   UARTTX0_Handler             /* 16+ 1: UART 0 TX Handler          */
    .long   UARTRX1_Handler             /* 16+ 2: UART 1 RX Handler          */
    .long   UARTTX1_Handler             /* 16+ 3: UART 1 TX Handler          */
    .long   UARTRX2_Handler             /* 16+ 4: UART 2 RX Handler          */
    .long   UARTTX2_Handler             /* 16+ 5: UART 2 TX Handler          */
    .long   PORT0_COMB_Handler          /* 16+ 6: GPIO Port 0 Combined Handler */
    .long   PORT1_COMB_Handler          /* 16+ 7: GPIO Port 1 Combined Handler */
    .long   TIMER0_Handler              /* 16+ 8: TIMER 0 handler            */
    .long   TIMER1_Handler              /* 16+ 9: TIMER 1 handler            */
    .long   DUALTIMER_HANDLER           /* 16+10: Dual timer 2 handler       */
    .long   SPI_ALL_Handler             /* 16+11: Reserved                   */
    .long   UARTOVF_Handler             /* 16+12: UART Combined Overflow Handler    */
    .long   ETHERNET_Handler            /* 16+13: Ethernet Handler           */
    .long   I2S_Handler                 /* 16+14: Audio I2S Handler          */
    .long   DMA_Handler                 /* 16+15: DMA done + error Handler   */
    .long   PORT0_0_Handler             /* 16+16: GPIO Port 0 pin 0 Handler  */
    .long   PORT0_1_Handler             /* 16+17: GPIO Port 0 pin 1 Handler  */
    .long   PORT0_2_Handler             /* 16+18: GPIO Port 0 pin 2 Handler  */
    .long   PORT0_3_Handler             /* 16+19: GPIO Port 0 pin 3 Handler  */
    .long   PORT0_4_Handler             /* 16+20: GPIO Port 0 pin 4 Handler  */
    .long   PORT0_5_Handler             /* 16+21: GPIO Port 0 pin 5 Handler  */
    .long   PORT0_6_Handler             /* 16+22: GPIO Port 0 pin 6 Handler  */
    .long   PORT0_7_Handler             /* 16+23: GPIO Port 0 pin 7 Handler  */
    .long   PORT0_8_Handler             /* 16+24: GPIO Port 0 pin 8 Handler  */
    .long   PORT0_9_Handler             /* 16+25: GPIO Port 0 pin 9 Handler  */
    .long   PORT0_10_Handler            /* 16+26: GPIO Port 0 pin 10 Handler */
    .long   PORT0_11_Handler            /* 16+27: GPIO Port 0 pin 11 Handler */
    .long   PORT0_12_Handler            /* 16+28: GPIO Port 0 pin 12 Handler */
    .long   PORT0_13_Handler            /* 16+29: GPIO Port 0 pin 13 Handler */
    .long   PORT0_14_Handler            /* 16+30: GPIO Port 0 pin 14 Handler */
    .long   PORT0_15_Handler            /* 16+31: GPIO Port 0 pin 15 Handler */

/*    Macro to define default handlers. Default handler
 *    will be weak symbol and just dead loops. They can be
 *    overwritten by other handlers */
    .macro    def_default_handler    handler_name
    .align 1
    .thumb_func
    .weak    \handler_name
    .type    \handler_name, %function
\handler_name :
    wfi
    b    .
    .size    \handler_name, . - \handler_name
    .endm

/* System Exception Handlers */

    def_default_handler    NMI_Handler
    def_default_handler    HardFault_Handler
    def_default_handler    MemManage_Handler
    def_default_handler    BusFault_Handler
    def_default_handler    UsageFault_Handler
    def_default_handler    SVC_Handler
    def_default_handler    DebugMon_Handler
    def_default_handler    PendSV_Handler
    def_default_handler    SysTick_Handler

/* IRQ Handlers */

    def_default_handler    UARTRX0_Handler
    def_default_handler    UARTTX0_Handler
    def_default_handler    UARTRX1_Handler
    def_default_handler    UARTTX1_Handler
    def_default_handler    UARTRX2_Handler
    def_default_handler    UARTTX2_Handler
    def_default_handler    PORT0_COMB_Handler
    def_default_handler    PORT1_COMB_Handler
    def_default_handler    TIMER0_Handler
    def_default_handler    TIMER1_Handler
    def_default_handler    DUALTIMER_HANDLER
    def_default_handler    SPI_ALL_Handler
    def_default_handler    UARTOVF_Handler
    def_default_handler    ETHERNET_Handler
    def_default_handler    I2S_Handler
    def_default_handler    DMA_Handler
    def_default_handler    PORT0_0_Handler
    def_default_handler    PORT0_1_Handler
    def_default_handler    PORT0_2_Handler
    def_default_handler    PORT0_3_Handler
    def_default_handler    PORT0_4_Handler
    def_default_handler    PORT0_5_Handler
    def_default_handler    PORT0_6_Handler
    def_default_handler    PORT0_7_Handler
    def_default_handler    PORT0_8_Handler
    def_default_handler    PORT0_9_Handler
    def_default_handler    PORT0_10_Handler
    def_default_handler    PORT0_11_Handler
    def_default_handler    PORT0_12_Handler
    def_default_handler    PORT0_13_Handler
    def_default_handler    PORT0_14_Handler
    def_default_handler    PORT0_15_Handler


.thumb
.thumb_func
.align 2
.global bl1_entrypoint_thumb
bl1_entrypoint_thumb:
    ldr r4, addr_stacktop
    mov sp,r4
    ldr r4, addr_bl1_main
	blx	r4
    wfi
    b .
    .align 4
addr_bl1_main:
    .word _start
addr_stacktop:
    .word __StackTop  

/*
// <h> Stack Configuration
//   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
// </h>
*/

    .section .stack
    .align 3
//#ifdef __STACK_SIZE
//    .equ    Stack_Size, __STACK_SIZE
//#else
    .equ    Stack_Size, 0x1000
//#endif
    .globl    __StackTop
    .globl    __StackLimit
__StackLimit:
    .space    Stack_Size
    .size __StackLimit, . - __StackLimit
__StackTop:
    .size __StackTop, . - __StackTop
