#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "Vcounter.h"
#include "verilated.h"
#include "../bitlified.h"

#define COMMAND_VERSION 0xABCD0000
#define COMMAND_GLITCH 0xABCD0001
#define COMMAND_EXIT 0xABCD0002

#define UART0_BASE_ADDRESS          0xA0000000
#define UART0_STATUS                0x00000000
#define UART0_TX_DATA               0x00000004
#define UART0_RX_DATA               0x00000008

#define WIDTH 64

unsigned int clock_counter = 0;

int connect(char *address) {
    struct sockaddr_un addr;
    int fd;

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd == -1) {
        printf("error: could not create socket\n");
        exit(1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, address, sizeof(addr.sun_path) - 1);

    if(connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        printf("error: could not connect socket\n");
        exit(1);
    }

    return fd;
}

Vcounter *freshcore() {
    Vcounter *top = new Vcounter;

    // reset
    top->clk = 0;
    set_rst_all(top, 1);
    for(int i = 0; i < 10; i++) {
        top->eval();
    	top->clk ^= 1;
    }
    set_rst_all(top, 0);

    top->eval();
    top->clk ^= 1;

    return top;
}

void injectfaults(Vcounter *top, char *words) {
    set_glitches_buf(top, words);
}

void runcore(Vcounter *top, uint32_t clocks, uint32_t start, uint32_t width, char *glitches) {
    // tick for a finite number of cycles
    for(int i = 0; i < clocks * 2; i++) {
        if(top->clk) {
            clock_counter++;

            if(clock_counter == start) {
                injectfaults(top, glitches);
            } else if(clock_counter == start + width) {
		set_glitches_zero(top);
            }
        }

        top->eval();
	top->clk ^= 1;
    }

    printf("\n\n");
    
    delete top;
}

void receive(int cli, void* buffer, size_t size) {
    //printf("[**] Receiving %ld bytes\n", size);
    char *buf = (char*)buffer;
    while (size > 0) {
        size_t num = recv(cli, buf, size, MSG_WAITALL);

        if (num == 0) {
   	    printf("No message received, aborting!");
	    exit(1);
       	}

	buf += num;
	size -= num;
    }
}

int main(int argc, char **argv, char **env) {
    Verilated::commandArgs(argc, argv);
    
    if(argc < 3) {
        printf("usage: %s [hex] [domain socket]\n", argv[0]);
        exit(1);
    }

    char *address = argv[2];

    Vcounter *top = freshcore();

    // connect to server
    int cli = connect(address);

    // tell server our pid as a ping
    uint32_t pid = getpid();
    send(cli, &pid, sizeof(pid), 0);

    size_t glitch_bytes = ((bits_in_glitches+7) / 8); 
    char *glitches = (char*)malloc(glitch_bytes); 

    while(1) {
        uint32_t status = 0xFFFFFFFF;

        // wait for command
        unsigned int cmd = 0;
        receive(cli, &cmd, sizeof(cmd));
            
        if(cmd == COMMAND_VERSION) {
            status = 1337;
        } else if(cmd == COMMAND_GLITCH) {
            // receive arguments
            struct {
                uint32_t clocks;
                uint32_t start;
                uint32_t width;
            } __attribute__((packed)) arguments;
            receive(cli, &arguments, sizeof(arguments));

            // TODO: make this a shared memory region
            // receive glitches
            receive(cli, glitches, glitch_bytes);

            int pipe_fds[2];
            pipe(pipe_fds);

	    // We fork here, so the parent stays in the 'just initialized' state.
	    // Jasper did an experiment removing fork() and just recalculating state; about the same speed. Leaving fork() in so later we can use it to do multiple faults in a row from one process
            int fd = fork();
            if(fd == 0) {
                // child process
                // TODO: redirect stdio/stderr to somewhere
                printf("glitch | clocks: %i start: %i width: %i\n", arguments.clocks, arguments.start, arguments.width);
                runcore(top, arguments.clocks, arguments.start, arguments.width, glitches);
                exit(0);
            }

            // wait for child to exit
            wait((int *)&status);
        } else if(cmd == COMMAND_EXIT) {
            break;
        }
        
        send(cli, &status, sizeof(status), 0);
    }

    free(glitches);
    
    exit(0);
}
