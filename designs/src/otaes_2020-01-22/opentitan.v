module xbar_main (
	clk_main_i,
	clk_fixed_i,
	rst_main_ni,
	rst_fixed_ni,
	tl_corei_i,
	tl_corei_o,
	tl_cored_i,
	tl_cored_o,
	tl_dm_sba_i,
	tl_dm_sba_o,
	tl_rom_o,
	tl_rom_i,
	tl_debug_mem_o,
	tl_debug_mem_i,
	tl_ram_main_o,
	tl_ram_main_i,
	tl_eflash_o,
	tl_eflash_i,
	tl_peri_o,
	tl_peri_i,
	tl_flash_ctrl_o,
	tl_flash_ctrl_i,
	tl_hmac_o,
	tl_hmac_i,
	tl_aes_o,
	tl_aes_i,
	tl_rv_plic_o,
	tl_rv_plic_i,
	tl_pinmux_o,
	tl_pinmux_i,
	tl_alert_handler_o,
	tl_alert_handler_i,
	tl_nmi_gen_o,
	tl_nmi_gen_i,
	scanmode_i
);
	input clk_main_i;
	input clk_fixed_i;
	input rst_main_ni;
	input rst_fixed_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_corei_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_corei_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_cored_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_cored_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_dm_sba_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_dm_sba_o;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_rom_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_rom_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_debug_mem_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_debug_mem_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_ram_main_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_ram_main_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_eflash_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_eflash_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_peri_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_peri_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_flash_ctrl_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_flash_ctrl_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_hmac_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_hmac_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_aes_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_aes_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_rv_plic_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_rv_plic_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_pinmux_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_pinmux_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_alert_handler_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_alert_handler_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_nmi_gen_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_nmi_gen_i;
	input scanmode_i;
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	localparam [31:0] ADDR_SPACE_ROM = 32'h00008000;
	localparam [31:0] ADDR_SPACE_DEBUG_MEM = 32'h1a110000;
	localparam [31:0] ADDR_SPACE_RAM_MAIN = 32'h10000000;
	localparam [31:0] ADDR_SPACE_EFLASH = 32'h20000000;
	localparam [63:0] ADDR_SPACE_PERI = {32'h40000000, 32'h40080000};
	localparam [31:0] ADDR_SPACE_FLASH_CTRL = 32'h40030000;
	localparam [31:0] ADDR_SPACE_HMAC = 32'h40120000;
	localparam [31:0] ADDR_SPACE_AES = 32'h40110000;
	localparam [31:0] ADDR_SPACE_RV_PLIC = 32'h40090000;
	localparam [31:0] ADDR_SPACE_PINMUX = 32'h40070000;
	localparam [31:0] ADDR_SPACE_ALERT_HANDLER = 32'h40130000;
	localparam [31:0] ADDR_SPACE_NMI_GEN = 32'h40140000;
	localparam [31:0] ADDR_MASK_ROM = 32'h00001fff;
	localparam [31:0] ADDR_MASK_DEBUG_MEM = 32'h00000fff;
	localparam [31:0] ADDR_MASK_RAM_MAIN = 32'h0000ffff;
	localparam [31:0] ADDR_MASK_EFLASH = 32'h0007ffff;
	localparam [63:0] ADDR_MASK_PERI = {32'h00020fff, 32'h00000fff};
	localparam [31:0] ADDR_MASK_FLASH_CTRL = 32'h00000fff;
	localparam [31:0] ADDR_MASK_HMAC = 32'h00000fff;
	localparam [31:0] ADDR_MASK_AES = 32'h00000fff;
	localparam [31:0] ADDR_MASK_RV_PLIC = 32'h00000fff;
	localparam [31:0] ADDR_MASK_PINMUX = 32'h00000fff;
	localparam [31:0] ADDR_MASK_ALERT_HANDLER = 32'h00000fff;
	localparam [31:0] ADDR_MASK_NMI_GEN = 32'h00000fff;
	localparam signed [31:0] N_HOST = 3;
	localparam signed [31:0] N_DEVICE = 12;
	localparam signed [31:0] TlRom = 0;
	localparam signed [31:0] TlDebugMem = 1;
	localparam signed [31:0] TlRamMain = 2;
	localparam signed [31:0] TlEflash = 3;
	localparam signed [31:0] TlPeri = 4;
	localparam signed [31:0] TlFlashCtrl = 5;
	localparam signed [31:0] TlHmac = 6;
	localparam signed [31:0] TlAes = 7;
	localparam signed [31:0] TlRvPlic = 8;
	localparam signed [31:0] TlPinmux = 9;
	localparam signed [31:0] TlAlertHandler = 10;
	localparam signed [31:0] TlNmiGen = 11;
	localparam signed [31:0] TlCorei = 0;
	localparam signed [31:0] TlCored = 1;
	localparam signed [31:0] TlDmSba = 2;
	wire unused_scanmode;
	assign unused_scanmode = scanmode_i;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_s1n_15_us_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_s1n_15_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (4 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (4 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_s1n_15_ds_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (4 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (4 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_s1n_15_ds_d2h;
	reg [2:0] dev_sel_s1n_15;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_16_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_16_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_16_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_16_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_17_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_17_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_17_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_17_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_18_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_18_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_18_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_18_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_19_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (3 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (3 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_19_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_19_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_19_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_s1n_20_us_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_s1n_20_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (12 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (12 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_s1n_20_ds_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (12 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (12 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_s1n_20_ds_d2h;
	reg [3:0] dev_sel_s1n_20;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_asf_21_us_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_asf_21_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_asf_21_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_asf_21_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_22_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_22_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_22_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_22_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_23_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_23_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_23_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_23_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_24_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_24_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_24_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_24_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_25_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_25_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_25_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_25_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_26_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_26_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_26_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_26_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_27_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_27_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_27_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_27_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_28_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_28_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_28_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_28_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_sm1_29_us_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_sm1_29_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_sm1_29_ds_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_sm1_29_ds_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_s1n_30_us_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_s1n_30_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (11 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (11 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_s1n_30_ds_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (11 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (11 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_s1n_30_ds_d2h;
	reg [3:0] dev_sel_s1n_30;
	assign tl_sm1_16_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_15_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (3 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_15_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (3 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_16_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_17_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_15_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_15_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_17_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_18_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_15_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_15_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_18_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_19_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_15_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_15_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_19_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_16_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (11 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (11 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_16_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_17_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (10 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (10 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_17_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_18_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (9 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (9 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_18_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_19_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (8 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (8 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_19_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_22_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (7 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (7 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_22_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_23_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (6 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (6 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_23_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_24_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (5 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (5 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_24_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_25_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (4 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (4 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_25_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_26_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (3 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (3 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_26_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_27_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_27_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_28_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_28_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_29_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_20_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_20_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_29_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_16_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (10 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (10 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_16_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_18_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (9 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (9 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_18_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_19_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (8 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (8 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_19_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_22_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (7 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (7 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_22_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_23_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (6 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (6 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_23_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_24_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (5 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (5 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_24_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_25_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (4 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (4 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_25_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_26_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (3 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (3 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_26_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_27_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_27_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_28_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_28_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_sm1_29_us_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_s1n_30_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_30_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_sm1_29_us_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	assign tl_s1n_15_us_h2d = tl_corei_i;
	assign tl_corei_o = tl_s1n_15_us_d2h;
	assign tl_rom_o = tl_sm1_16_ds_h2d;
	assign tl_sm1_16_ds_d2h = tl_rom_i;
	assign tl_debug_mem_o = tl_sm1_17_ds_h2d;
	assign tl_sm1_17_ds_d2h = tl_debug_mem_i;
	assign tl_ram_main_o = tl_sm1_18_ds_h2d;
	assign tl_sm1_18_ds_d2h = tl_ram_main_i;
	assign tl_eflash_o = tl_sm1_19_ds_h2d;
	assign tl_sm1_19_ds_d2h = tl_eflash_i;
	assign tl_s1n_20_us_h2d = tl_cored_i;
	assign tl_cored_o = tl_s1n_20_us_d2h;
	assign tl_peri_o = tl_asf_21_ds_h2d;
	assign tl_asf_21_ds_d2h = tl_peri_i;
	assign tl_asf_21_us_h2d = tl_sm1_22_ds_h2d;
	assign tl_sm1_22_ds_d2h = tl_asf_21_us_d2h;
	assign tl_flash_ctrl_o = tl_sm1_23_ds_h2d;
	assign tl_sm1_23_ds_d2h = tl_flash_ctrl_i;
	assign tl_aes_o = tl_sm1_24_ds_h2d;
	assign tl_sm1_24_ds_d2h = tl_aes_i;
	assign tl_hmac_o = tl_sm1_25_ds_h2d;
	assign tl_sm1_25_ds_d2h = tl_hmac_i;
	assign tl_rv_plic_o = tl_sm1_26_ds_h2d;
	assign tl_sm1_26_ds_d2h = tl_rv_plic_i;
	assign tl_pinmux_o = tl_sm1_27_ds_h2d;
	assign tl_sm1_27_ds_d2h = tl_pinmux_i;
	assign tl_alert_handler_o = tl_sm1_28_ds_h2d;
	assign tl_sm1_28_ds_d2h = tl_alert_handler_i;
	assign tl_nmi_gen_o = tl_sm1_29_ds_h2d;
	assign tl_sm1_29_ds_d2h = tl_nmi_gen_i;
	assign tl_s1n_30_us_h2d = tl_dm_sba_i;
	assign tl_dm_sba_o = tl_s1n_30_us_d2h;
	always @(*) begin
		dev_sel_s1n_15 = 3'd4;
		if ((tl_s1n_15_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_ROM) == ADDR_SPACE_ROM)
			dev_sel_s1n_15 = 3'd0;
		else if ((tl_s1n_15_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_DEBUG_MEM) == ADDR_SPACE_DEBUG_MEM)
			dev_sel_s1n_15 = 3'd1;
		else if ((tl_s1n_15_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RAM_MAIN) == ADDR_SPACE_RAM_MAIN)
			dev_sel_s1n_15 = 3'd2;
		else if ((tl_s1n_15_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_EFLASH) == ADDR_SPACE_EFLASH)
			dev_sel_s1n_15 = 3'd3;
	end
	always @(*) begin
		dev_sel_s1n_20 = 4'd12;
		if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_ROM) == ADDR_SPACE_ROM)
			dev_sel_s1n_20 = 4'd0;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_DEBUG_MEM) == ADDR_SPACE_DEBUG_MEM)
			dev_sel_s1n_20 = 4'd1;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RAM_MAIN) == ADDR_SPACE_RAM_MAIN)
			dev_sel_s1n_20 = 4'd2;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_EFLASH) == ADDR_SPACE_EFLASH)
			dev_sel_s1n_20 = 4'd3;
		else if (((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= (ADDR_MASK_PERI[0+:32] + ADDR_SPACE_PERI[0+:32])) && (tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] >= ADDR_SPACE_PERI[0+:32])) || ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= (ADDR_MASK_PERI[32+:32] + ADDR_SPACE_PERI[32+:32])) && (tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] >= ADDR_SPACE_PERI[32+:32])))
			dev_sel_s1n_20 = 4'd4;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_FLASH_CTRL) == ADDR_SPACE_FLASH_CTRL)
			dev_sel_s1n_20 = 4'd5;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_AES) == ADDR_SPACE_AES)
			dev_sel_s1n_20 = 4'd6;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_HMAC) == ADDR_SPACE_HMAC)
			dev_sel_s1n_20 = 4'd7;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RV_PLIC) == ADDR_SPACE_RV_PLIC)
			dev_sel_s1n_20 = 4'd8;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_PINMUX) == ADDR_SPACE_PINMUX)
			dev_sel_s1n_20 = 4'd9;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_ALERT_HANDLER) == ADDR_SPACE_ALERT_HANDLER)
			dev_sel_s1n_20 = 4'd10;
		else if ((tl_s1n_20_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_NMI_GEN) == ADDR_SPACE_NMI_GEN)
			dev_sel_s1n_20 = 4'd11;
	end
	always @(*) begin
		dev_sel_s1n_30 = 4'd11;
		if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_ROM) == ADDR_SPACE_ROM)
			dev_sel_s1n_30 = 4'd0;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RAM_MAIN) == ADDR_SPACE_RAM_MAIN)
			dev_sel_s1n_30 = 4'd1;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_EFLASH) == ADDR_SPACE_EFLASH)
			dev_sel_s1n_30 = 4'd2;
		else if (((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= (ADDR_MASK_PERI[0+:32] + ADDR_SPACE_PERI[0+:32])) && (tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] >= ADDR_SPACE_PERI[0+:32])) || ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= (ADDR_MASK_PERI[32+:32] + ADDR_SPACE_PERI[32+:32])) && (tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] >= ADDR_SPACE_PERI[32+:32])))
			dev_sel_s1n_30 = 4'd3;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_FLASH_CTRL) == ADDR_SPACE_FLASH_CTRL)
			dev_sel_s1n_30 = 4'd4;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_AES) == ADDR_SPACE_AES)
			dev_sel_s1n_30 = 4'd5;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_HMAC) == ADDR_SPACE_HMAC)
			dev_sel_s1n_30 = 4'd6;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RV_PLIC) == ADDR_SPACE_RV_PLIC)
			dev_sel_s1n_30 = 4'd7;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_PINMUX) == ADDR_SPACE_PINMUX)
			dev_sel_s1n_30 = 4'd8;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_ALERT_HANDLER) == ADDR_SPACE_ALERT_HANDLER)
			dev_sel_s1n_30 = 4'd9;
		else if ((tl_s1n_30_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_NMI_GEN) == ADDR_SPACE_NMI_GEN)
			dev_sel_s1n_30 = 4'd10;
	end
	tlul_socket_1n #(
		.HReqDepth(4'h0),
		.HRspDepth(4'h0),
		.DReqDepth({4 {4'h0}}),
		.DRspDepth({4 {4'h0}}),
		.N(4)
	) u_s1n_15(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_s1n_15_us_h2d),
		.tl_h_o(tl_s1n_15_us_d2h),
		.tl_d_o(tl_s1n_15_ds_h2d),
		.tl_d_i(tl_s1n_15_ds_d2h),
		.dev_select(dev_sel_s1n_15)
	);
	tlul_socket_m1 #(
		.HReqDepth({3 {4'h0}}),
		.HRspDepth({3 {4'h0}}),
		.DReqDepth(4'h0),
		.DRspDepth(4'h0),
		.M(3)
	) u_sm1_16(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_16_us_h2d),
		.tl_h_o(tl_sm1_16_us_d2h),
		.tl_d_o(tl_sm1_16_ds_h2d),
		.tl_d_i(tl_sm1_16_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_17(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_17_us_h2d),
		.tl_h_o(tl_sm1_17_us_d2h),
		.tl_d_o(tl_sm1_17_ds_h2d),
		.tl_d_i(tl_sm1_17_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqDepth({3 {4'h0}}),
		.HRspDepth({3 {4'h0}}),
		.DReqDepth(4'h0),
		.DRspDepth(4'h0),
		.M(3)
	) u_sm1_18(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_18_us_h2d),
		.tl_h_o(tl_sm1_18_us_d2h),
		.tl_d_o(tl_sm1_18_ds_h2d),
		.tl_d_i(tl_sm1_18_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqDepth({3 {4'h0}}),
		.HRspDepth({3 {4'h0}}),
		.DReqDepth(4'h0),
		.DRspDepth(4'h0),
		.M(3)
	) u_sm1_19(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_19_us_h2d),
		.tl_h_o(tl_sm1_19_us_d2h),
		.tl_d_o(tl_sm1_19_ds_h2d),
		.tl_d_i(tl_sm1_19_ds_d2h)
	);
	tlul_socket_1n #(
		.HReqDepth(4'h0),
		.HRspDepth(4'h0),
		.DReqDepth({12 {4'h0}}),
		.DRspDepth({12 {4'h0}}),
		.N(12)
	) u_s1n_20(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_s1n_20_us_h2d),
		.tl_h_o(tl_s1n_20_us_d2h),
		.tl_d_o(tl_s1n_20_ds_h2d),
		.tl_d_i(tl_s1n_20_ds_d2h),
		.dev_select(dev_sel_s1n_20)
	);
	tlul_fifo_async #(
		.ReqDepth(3),
		.RspDepth(3)
	) u_asf_21(
		.clk_h_i(clk_main_i),
		.rst_h_ni(rst_main_ni),
		.clk_d_i(clk_fixed_i),
		.rst_d_ni(rst_fixed_ni),
		.tl_h_i(tl_asf_21_us_h2d),
		.tl_h_o(tl_asf_21_us_d2h),
		.tl_d_o(tl_asf_21_ds_h2d),
		.tl_d_i(tl_asf_21_ds_d2h)
	);
	tlul_socket_m1 #(.M(2)) u_sm1_22(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_22_us_h2d),
		.tl_h_o(tl_sm1_22_us_d2h),
		.tl_d_o(tl_sm1_22_ds_h2d),
		.tl_d_i(tl_sm1_22_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_23(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_23_us_h2d),
		.tl_h_o(tl_sm1_23_us_d2h),
		.tl_d_o(tl_sm1_23_ds_h2d),
		.tl_d_i(tl_sm1_23_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_24(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_24_us_h2d),
		.tl_h_o(tl_sm1_24_us_d2h),
		.tl_d_o(tl_sm1_24_ds_h2d),
		.tl_d_i(tl_sm1_24_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_25(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_25_us_h2d),
		.tl_h_o(tl_sm1_25_us_d2h),
		.tl_d_o(tl_sm1_25_ds_h2d),
		.tl_d_i(tl_sm1_25_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_26(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_26_us_h2d),
		.tl_h_o(tl_sm1_26_us_d2h),
		.tl_d_o(tl_sm1_26_ds_h2d),
		.tl_d_i(tl_sm1_26_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_27(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_27_us_h2d),
		.tl_h_o(tl_sm1_27_us_d2h),
		.tl_d_o(tl_sm1_27_ds_h2d),
		.tl_d_i(tl_sm1_27_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_28(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_28_us_h2d),
		.tl_h_o(tl_sm1_28_us_d2h),
		.tl_d_o(tl_sm1_28_ds_h2d),
		.tl_d_i(tl_sm1_28_ds_d2h)
	);
	tlul_socket_m1 #(
		.HReqPass(2'h0),
		.HRspPass(2'h0),
		.DReqPass(1'b0),
		.DRspPass(1'b0),
		.M(2)
	) u_sm1_29(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_sm1_29_us_h2d),
		.tl_h_o(tl_sm1_29_us_d2h),
		.tl_d_o(tl_sm1_29_ds_h2d),
		.tl_d_i(tl_sm1_29_ds_d2h)
	);
	tlul_socket_1n #(
		.HReqPass(1'b0),
		.HRspPass(1'b0),
		.DReqPass(11'h000),
		.DRspPass(11'h000),
		.N(11)
	) u_s1n_30(
		.clk_i(clk_main_i),
		.rst_ni(rst_main_ni),
		.tl_h_i(tl_s1n_30_us_h2d),
		.tl_h_o(tl_s1n_30_us_d2h),
		.tl_d_o(tl_s1n_30_ds_h2d),
		.tl_d_i(tl_s1n_30_ds_d2h),
		.dev_select(dev_sel_s1n_30)
	);
endmodule
module xbar_peri (
	clk_peri_i,
	rst_peri_ni,
	tl_main_i,
	tl_main_o,
	tl_uart_o,
	tl_uart_i,
	tl_gpio_o,
	tl_gpio_i,
	tl_spi_device_o,
	tl_spi_device_i,
	tl_rv_timer_o,
	tl_rv_timer_i,
	scanmode_i
);
	input clk_peri_i;
	input rst_peri_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_main_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_main_o;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_uart_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_uart_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_gpio_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_gpio_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_spi_device_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_spi_device_i;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_rv_timer_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_rv_timer_i;
	input scanmode_i;
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	localparam [31:0] ADDR_SPACE_UART = 32'h40000000;
	localparam [31:0] ADDR_SPACE_GPIO = 32'h40010000;
	localparam [31:0] ADDR_SPACE_SPI_DEVICE = 32'h40020000;
	localparam [31:0] ADDR_SPACE_RV_TIMER = 32'h40080000;
	localparam [31:0] ADDR_MASK_UART = 32'h00000fff;
	localparam [31:0] ADDR_MASK_GPIO = 32'h00000fff;
	localparam [31:0] ADDR_MASK_SPI_DEVICE = 32'h00000fff;
	localparam [31:0] ADDR_MASK_RV_TIMER = 32'h00000fff;
	localparam signed [31:0] N_HOST = 1;
	localparam signed [31:0] N_DEVICE = 4;
	localparam signed [31:0] TlUart = 0;
	localparam signed [31:0] TlGpio = 1;
	localparam signed [31:0] TlSpiDevice = 2;
	localparam signed [31:0] TlRvTimer = 3;
	localparam signed [31:0] TlMain = 0;
	wire unused_scanmode;
	assign unused_scanmode = scanmode_i;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_s1n_5_us_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_s1n_5_us_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (4 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (4 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_s1n_5_ds_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (4 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (4 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_s1n_5_ds_d2h;
	reg [2:0] dev_sel_s1n_5;
	assign tl_uart_o = tl_s1n_5_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (3 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_5_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (3 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_uart_i;
	assign tl_gpio_o = tl_s1n_5_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (2 * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_5_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (2 * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_gpio_i;
	assign tl_spi_device_o = tl_s1n_5_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_5_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_spi_device_i;
	assign tl_rv_timer_o = tl_s1n_5_ds_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_s1n_5_ds_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_rv_timer_i;
	assign tl_s1n_5_us_h2d = tl_main_i;
	assign tl_main_o = tl_s1n_5_us_d2h;
	always @(*) begin
		dev_sel_s1n_5 = 3'd4;
		if ((tl_s1n_5_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_UART) == ADDR_SPACE_UART)
			dev_sel_s1n_5 = 3'd0;
		else if ((tl_s1n_5_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_GPIO) == ADDR_SPACE_GPIO)
			dev_sel_s1n_5 = 3'd1;
		else if ((tl_s1n_5_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_SPI_DEVICE) == ADDR_SPACE_SPI_DEVICE)
			dev_sel_s1n_5 = 3'd2;
		else if ((tl_s1n_5_us_h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] & ~ADDR_MASK_RV_TIMER) == ADDR_SPACE_RV_TIMER)
			dev_sel_s1n_5 = 3'd3;
	end
	tlul_socket_1n #(
		.HReqDepth(4'h0),
		.HRspDepth(4'h0),
		.DReqDepth({4 {4'h0}}),
		.DRspDepth({4 {4'h0}}),
		.N(4)
	) u_s1n_5(
		.clk_i(clk_peri_i),
		.rst_ni(rst_peri_ni),
		.tl_h_i(tl_s1n_5_us_h2d),
		.tl_h_o(tl_s1n_5_us_d2h),
		.tl_d_o(tl_s1n_5_ds_h2d),
		.tl_d_i(tl_s1n_5_ds_d2h),
		.dev_select(dev_sel_s1n_5)
	);
endmodule
module rv_plic_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [262:0] reg2hw;
	input wire [131:0] hw2reg;
	input devmode_i;
	localparam signed [31:0] NumSrc = 63;
	localparam signed [31:0] NumTarget = 1;
	localparam RV_PLIC_IP0_OFFSET = 10'h000;
	localparam RV_PLIC_IP1_OFFSET = 10'h004;
	localparam RV_PLIC_LE0_OFFSET = 10'h008;
	localparam RV_PLIC_LE1_OFFSET = 10'h00c;
	localparam RV_PLIC_PRIO0_OFFSET = 10'h010;
	localparam RV_PLIC_PRIO1_OFFSET = 10'h014;
	localparam RV_PLIC_PRIO2_OFFSET = 10'h018;
	localparam RV_PLIC_PRIO3_OFFSET = 10'h01c;
	localparam RV_PLIC_PRIO4_OFFSET = 10'h020;
	localparam RV_PLIC_PRIO5_OFFSET = 10'h024;
	localparam RV_PLIC_PRIO6_OFFSET = 10'h028;
	localparam RV_PLIC_PRIO7_OFFSET = 10'h02c;
	localparam RV_PLIC_PRIO8_OFFSET = 10'h030;
	localparam RV_PLIC_PRIO9_OFFSET = 10'h034;
	localparam RV_PLIC_PRIO10_OFFSET = 10'h038;
	localparam RV_PLIC_PRIO11_OFFSET = 10'h03c;
	localparam RV_PLIC_PRIO12_OFFSET = 10'h040;
	localparam RV_PLIC_PRIO13_OFFSET = 10'h044;
	localparam RV_PLIC_PRIO14_OFFSET = 10'h048;
	localparam RV_PLIC_PRIO15_OFFSET = 10'h04c;
	localparam RV_PLIC_PRIO16_OFFSET = 10'h050;
	localparam RV_PLIC_PRIO17_OFFSET = 10'h054;
	localparam RV_PLIC_PRIO18_OFFSET = 10'h058;
	localparam RV_PLIC_PRIO19_OFFSET = 10'h05c;
	localparam RV_PLIC_PRIO20_OFFSET = 10'h060;
	localparam RV_PLIC_PRIO21_OFFSET = 10'h064;
	localparam RV_PLIC_PRIO22_OFFSET = 10'h068;
	localparam RV_PLIC_PRIO23_OFFSET = 10'h06c;
	localparam RV_PLIC_PRIO24_OFFSET = 10'h070;
	localparam RV_PLIC_PRIO25_OFFSET = 10'h074;
	localparam RV_PLIC_PRIO26_OFFSET = 10'h078;
	localparam RV_PLIC_PRIO27_OFFSET = 10'h07c;
	localparam RV_PLIC_PRIO28_OFFSET = 10'h080;
	localparam RV_PLIC_PRIO29_OFFSET = 10'h084;
	localparam RV_PLIC_PRIO30_OFFSET = 10'h088;
	localparam RV_PLIC_PRIO31_OFFSET = 10'h08c;
	localparam RV_PLIC_PRIO32_OFFSET = 10'h090;
	localparam RV_PLIC_PRIO33_OFFSET = 10'h094;
	localparam RV_PLIC_PRIO34_OFFSET = 10'h098;
	localparam RV_PLIC_PRIO35_OFFSET = 10'h09c;
	localparam RV_PLIC_PRIO36_OFFSET = 10'h0a0;
	localparam RV_PLIC_PRIO37_OFFSET = 10'h0a4;
	localparam RV_PLIC_PRIO38_OFFSET = 10'h0a8;
	localparam RV_PLIC_PRIO39_OFFSET = 10'h0ac;
	localparam RV_PLIC_PRIO40_OFFSET = 10'h0b0;
	localparam RV_PLIC_PRIO41_OFFSET = 10'h0b4;
	localparam RV_PLIC_PRIO42_OFFSET = 10'h0b8;
	localparam RV_PLIC_PRIO43_OFFSET = 10'h0bc;
	localparam RV_PLIC_PRIO44_OFFSET = 10'h0c0;
	localparam RV_PLIC_PRIO45_OFFSET = 10'h0c4;
	localparam RV_PLIC_PRIO46_OFFSET = 10'h0c8;
	localparam RV_PLIC_PRIO47_OFFSET = 10'h0cc;
	localparam RV_PLIC_PRIO48_OFFSET = 10'h0d0;
	localparam RV_PLIC_PRIO49_OFFSET = 10'h0d4;
	localparam RV_PLIC_PRIO50_OFFSET = 10'h0d8;
	localparam RV_PLIC_PRIO51_OFFSET = 10'h0dc;
	localparam RV_PLIC_PRIO52_OFFSET = 10'h0e0;
	localparam RV_PLIC_PRIO53_OFFSET = 10'h0e4;
	localparam RV_PLIC_PRIO54_OFFSET = 10'h0e8;
	localparam RV_PLIC_PRIO55_OFFSET = 10'h0ec;
	localparam RV_PLIC_PRIO56_OFFSET = 10'h0f0;
	localparam RV_PLIC_PRIO57_OFFSET = 10'h0f4;
	localparam RV_PLIC_PRIO58_OFFSET = 10'h0f8;
	localparam RV_PLIC_PRIO59_OFFSET = 10'h0fc;
	localparam RV_PLIC_PRIO60_OFFSET = 10'h100;
	localparam RV_PLIC_PRIO61_OFFSET = 10'h104;
	localparam RV_PLIC_PRIO62_OFFSET = 10'h108;
	localparam RV_PLIC_IE00_OFFSET = 10'h200;
	localparam RV_PLIC_IE01_OFFSET = 10'h204;
	localparam RV_PLIC_THRESHOLD0_OFFSET = 10'h208;
	localparam RV_PLIC_CC0_OFFSET = 10'h20c;
	localparam RV_PLIC_MSIP0_OFFSET = 10'h210;
	localparam signed [31:0] RV_PLIC_IP0 = 0;
	localparam signed [31:0] RV_PLIC_IP1 = 1;
	localparam signed [31:0] RV_PLIC_LE0 = 2;
	localparam signed [31:0] RV_PLIC_LE1 = 3;
	localparam signed [31:0] RV_PLIC_PRIO0 = 4;
	localparam signed [31:0] RV_PLIC_PRIO1 = 5;
	localparam signed [31:0] RV_PLIC_PRIO2 = 6;
	localparam signed [31:0] RV_PLIC_PRIO3 = 7;
	localparam signed [31:0] RV_PLIC_PRIO4 = 8;
	localparam signed [31:0] RV_PLIC_PRIO5 = 9;
	localparam signed [31:0] RV_PLIC_PRIO6 = 10;
	localparam signed [31:0] RV_PLIC_PRIO7 = 11;
	localparam signed [31:0] RV_PLIC_PRIO8 = 12;
	localparam signed [31:0] RV_PLIC_PRIO9 = 13;
	localparam signed [31:0] RV_PLIC_PRIO10 = 14;
	localparam signed [31:0] RV_PLIC_PRIO11 = 15;
	localparam signed [31:0] RV_PLIC_PRIO12 = 16;
	localparam signed [31:0] RV_PLIC_PRIO13 = 17;
	localparam signed [31:0] RV_PLIC_PRIO14 = 18;
	localparam signed [31:0] RV_PLIC_PRIO15 = 19;
	localparam signed [31:0] RV_PLIC_PRIO16 = 20;
	localparam signed [31:0] RV_PLIC_PRIO17 = 21;
	localparam signed [31:0] RV_PLIC_PRIO18 = 22;
	localparam signed [31:0] RV_PLIC_PRIO19 = 23;
	localparam signed [31:0] RV_PLIC_PRIO20 = 24;
	localparam signed [31:0] RV_PLIC_PRIO21 = 25;
	localparam signed [31:0] RV_PLIC_PRIO22 = 26;
	localparam signed [31:0] RV_PLIC_PRIO23 = 27;
	localparam signed [31:0] RV_PLIC_PRIO24 = 28;
	localparam signed [31:0] RV_PLIC_PRIO25 = 29;
	localparam signed [31:0] RV_PLIC_PRIO26 = 30;
	localparam signed [31:0] RV_PLIC_PRIO27 = 31;
	localparam signed [31:0] RV_PLIC_PRIO28 = 32;
	localparam signed [31:0] RV_PLIC_PRIO29 = 33;
	localparam signed [31:0] RV_PLIC_PRIO30 = 34;
	localparam signed [31:0] RV_PLIC_PRIO31 = 35;
	localparam signed [31:0] RV_PLIC_PRIO32 = 36;
	localparam signed [31:0] RV_PLIC_PRIO33 = 37;
	localparam signed [31:0] RV_PLIC_PRIO34 = 38;
	localparam signed [31:0] RV_PLIC_PRIO35 = 39;
	localparam signed [31:0] RV_PLIC_PRIO36 = 40;
	localparam signed [31:0] RV_PLIC_PRIO37 = 41;
	localparam signed [31:0] RV_PLIC_PRIO38 = 42;
	localparam signed [31:0] RV_PLIC_PRIO39 = 43;
	localparam signed [31:0] RV_PLIC_PRIO40 = 44;
	localparam signed [31:0] RV_PLIC_PRIO41 = 45;
	localparam signed [31:0] RV_PLIC_PRIO42 = 46;
	localparam signed [31:0] RV_PLIC_PRIO43 = 47;
	localparam signed [31:0] RV_PLIC_PRIO44 = 48;
	localparam signed [31:0] RV_PLIC_PRIO45 = 49;
	localparam signed [31:0] RV_PLIC_PRIO46 = 50;
	localparam signed [31:0] RV_PLIC_PRIO47 = 51;
	localparam signed [31:0] RV_PLIC_PRIO48 = 52;
	localparam signed [31:0] RV_PLIC_PRIO49 = 53;
	localparam signed [31:0] RV_PLIC_PRIO50 = 54;
	localparam signed [31:0] RV_PLIC_PRIO51 = 55;
	localparam signed [31:0] RV_PLIC_PRIO52 = 56;
	localparam signed [31:0] RV_PLIC_PRIO53 = 57;
	localparam signed [31:0] RV_PLIC_PRIO54 = 58;
	localparam signed [31:0] RV_PLIC_PRIO55 = 59;
	localparam signed [31:0] RV_PLIC_PRIO56 = 60;
	localparam signed [31:0] RV_PLIC_PRIO57 = 61;
	localparam signed [31:0] RV_PLIC_PRIO58 = 62;
	localparam signed [31:0] RV_PLIC_PRIO59 = 63;
	localparam signed [31:0] RV_PLIC_PRIO60 = 64;
	localparam signed [31:0] RV_PLIC_PRIO61 = 65;
	localparam signed [31:0] RV_PLIC_PRIO62 = 66;
	localparam signed [31:0] RV_PLIC_IE00 = 67;
	localparam signed [31:0] RV_PLIC_IE01 = 68;
	localparam signed [31:0] RV_PLIC_THRESHOLD0 = 69;
	localparam signed [31:0] RV_PLIC_CC0 = 70;
	localparam signed [31:0] RV_PLIC_MSIP0 = 71;
	localparam [287:0] RV_PLIC_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam AW = 10;
	localparam DW = 32;
	localparam DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire ip0_p0_qs;
	wire ip0_p1_qs;
	wire ip0_p2_qs;
	wire ip0_p3_qs;
	wire ip0_p4_qs;
	wire ip0_p5_qs;
	wire ip0_p6_qs;
	wire ip0_p7_qs;
	wire ip0_p8_qs;
	wire ip0_p9_qs;
	wire ip0_p10_qs;
	wire ip0_p11_qs;
	wire ip0_p12_qs;
	wire ip0_p13_qs;
	wire ip0_p14_qs;
	wire ip0_p15_qs;
	wire ip0_p16_qs;
	wire ip0_p17_qs;
	wire ip0_p18_qs;
	wire ip0_p19_qs;
	wire ip0_p20_qs;
	wire ip0_p21_qs;
	wire ip0_p22_qs;
	wire ip0_p23_qs;
	wire ip0_p24_qs;
	wire ip0_p25_qs;
	wire ip0_p26_qs;
	wire ip0_p27_qs;
	wire ip0_p28_qs;
	wire ip0_p29_qs;
	wire ip0_p30_qs;
	wire ip0_p31_qs;
	wire ip1_p32_qs;
	wire ip1_p33_qs;
	wire ip1_p34_qs;
	wire ip1_p35_qs;
	wire ip1_p36_qs;
	wire ip1_p37_qs;
	wire ip1_p38_qs;
	wire ip1_p39_qs;
	wire ip1_p40_qs;
	wire ip1_p41_qs;
	wire ip1_p42_qs;
	wire ip1_p43_qs;
	wire ip1_p44_qs;
	wire ip1_p45_qs;
	wire ip1_p46_qs;
	wire ip1_p47_qs;
	wire ip1_p48_qs;
	wire ip1_p49_qs;
	wire ip1_p50_qs;
	wire ip1_p51_qs;
	wire ip1_p52_qs;
	wire ip1_p53_qs;
	wire ip1_p54_qs;
	wire ip1_p55_qs;
	wire ip1_p56_qs;
	wire ip1_p57_qs;
	wire ip1_p58_qs;
	wire ip1_p59_qs;
	wire ip1_p60_qs;
	wire ip1_p61_qs;
	wire ip1_p62_qs;
	wire le0_le0_qs;
	wire le0_le0_wd;
	wire le0_le0_we;
	wire le0_le1_qs;
	wire le0_le1_wd;
	wire le0_le1_we;
	wire le0_le2_qs;
	wire le0_le2_wd;
	wire le0_le2_we;
	wire le0_le3_qs;
	wire le0_le3_wd;
	wire le0_le3_we;
	wire le0_le4_qs;
	wire le0_le4_wd;
	wire le0_le4_we;
	wire le0_le5_qs;
	wire le0_le5_wd;
	wire le0_le5_we;
	wire le0_le6_qs;
	wire le0_le6_wd;
	wire le0_le6_we;
	wire le0_le7_qs;
	wire le0_le7_wd;
	wire le0_le7_we;
	wire le0_le8_qs;
	wire le0_le8_wd;
	wire le0_le8_we;
	wire le0_le9_qs;
	wire le0_le9_wd;
	wire le0_le9_we;
	wire le0_le10_qs;
	wire le0_le10_wd;
	wire le0_le10_we;
	wire le0_le11_qs;
	wire le0_le11_wd;
	wire le0_le11_we;
	wire le0_le12_qs;
	wire le0_le12_wd;
	wire le0_le12_we;
	wire le0_le13_qs;
	wire le0_le13_wd;
	wire le0_le13_we;
	wire le0_le14_qs;
	wire le0_le14_wd;
	wire le0_le14_we;
	wire le0_le15_qs;
	wire le0_le15_wd;
	wire le0_le15_we;
	wire le0_le16_qs;
	wire le0_le16_wd;
	wire le0_le16_we;
	wire le0_le17_qs;
	wire le0_le17_wd;
	wire le0_le17_we;
	wire le0_le18_qs;
	wire le0_le18_wd;
	wire le0_le18_we;
	wire le0_le19_qs;
	wire le0_le19_wd;
	wire le0_le19_we;
	wire le0_le20_qs;
	wire le0_le20_wd;
	wire le0_le20_we;
	wire le0_le21_qs;
	wire le0_le21_wd;
	wire le0_le21_we;
	wire le0_le22_qs;
	wire le0_le22_wd;
	wire le0_le22_we;
	wire le0_le23_qs;
	wire le0_le23_wd;
	wire le0_le23_we;
	wire le0_le24_qs;
	wire le0_le24_wd;
	wire le0_le24_we;
	wire le0_le25_qs;
	wire le0_le25_wd;
	wire le0_le25_we;
	wire le0_le26_qs;
	wire le0_le26_wd;
	wire le0_le26_we;
	wire le0_le27_qs;
	wire le0_le27_wd;
	wire le0_le27_we;
	wire le0_le28_qs;
	wire le0_le28_wd;
	wire le0_le28_we;
	wire le0_le29_qs;
	wire le0_le29_wd;
	wire le0_le29_we;
	wire le0_le30_qs;
	wire le0_le30_wd;
	wire le0_le30_we;
	wire le0_le31_qs;
	wire le0_le31_wd;
	wire le0_le31_we;
	wire le1_le32_qs;
	wire le1_le32_wd;
	wire le1_le32_we;
	wire le1_le33_qs;
	wire le1_le33_wd;
	wire le1_le33_we;
	wire le1_le34_qs;
	wire le1_le34_wd;
	wire le1_le34_we;
	wire le1_le35_qs;
	wire le1_le35_wd;
	wire le1_le35_we;
	wire le1_le36_qs;
	wire le1_le36_wd;
	wire le1_le36_we;
	wire le1_le37_qs;
	wire le1_le37_wd;
	wire le1_le37_we;
	wire le1_le38_qs;
	wire le1_le38_wd;
	wire le1_le38_we;
	wire le1_le39_qs;
	wire le1_le39_wd;
	wire le1_le39_we;
	wire le1_le40_qs;
	wire le1_le40_wd;
	wire le1_le40_we;
	wire le1_le41_qs;
	wire le1_le41_wd;
	wire le1_le41_we;
	wire le1_le42_qs;
	wire le1_le42_wd;
	wire le1_le42_we;
	wire le1_le43_qs;
	wire le1_le43_wd;
	wire le1_le43_we;
	wire le1_le44_qs;
	wire le1_le44_wd;
	wire le1_le44_we;
	wire le1_le45_qs;
	wire le1_le45_wd;
	wire le1_le45_we;
	wire le1_le46_qs;
	wire le1_le46_wd;
	wire le1_le46_we;
	wire le1_le47_qs;
	wire le1_le47_wd;
	wire le1_le47_we;
	wire le1_le48_qs;
	wire le1_le48_wd;
	wire le1_le48_we;
	wire le1_le49_qs;
	wire le1_le49_wd;
	wire le1_le49_we;
	wire le1_le50_qs;
	wire le1_le50_wd;
	wire le1_le50_we;
	wire le1_le51_qs;
	wire le1_le51_wd;
	wire le1_le51_we;
	wire le1_le52_qs;
	wire le1_le52_wd;
	wire le1_le52_we;
	wire le1_le53_qs;
	wire le1_le53_wd;
	wire le1_le53_we;
	wire le1_le54_qs;
	wire le1_le54_wd;
	wire le1_le54_we;
	wire le1_le55_qs;
	wire le1_le55_wd;
	wire le1_le55_we;
	wire le1_le56_qs;
	wire le1_le56_wd;
	wire le1_le56_we;
	wire le1_le57_qs;
	wire le1_le57_wd;
	wire le1_le57_we;
	wire le1_le58_qs;
	wire le1_le58_wd;
	wire le1_le58_we;
	wire le1_le59_qs;
	wire le1_le59_wd;
	wire le1_le59_we;
	wire le1_le60_qs;
	wire le1_le60_wd;
	wire le1_le60_we;
	wire le1_le61_qs;
	wire le1_le61_wd;
	wire le1_le61_we;
	wire le1_le62_qs;
	wire le1_le62_wd;
	wire le1_le62_we;
	wire [1:0] prio0_qs;
	wire [1:0] prio0_wd;
	wire prio0_we;
	wire [1:0] prio1_qs;
	wire [1:0] prio1_wd;
	wire prio1_we;
	wire [1:0] prio2_qs;
	wire [1:0] prio2_wd;
	wire prio2_we;
	wire [1:0] prio3_qs;
	wire [1:0] prio3_wd;
	wire prio3_we;
	wire [1:0] prio4_qs;
	wire [1:0] prio4_wd;
	wire prio4_we;
	wire [1:0] prio5_qs;
	wire [1:0] prio5_wd;
	wire prio5_we;
	wire [1:0] prio6_qs;
	wire [1:0] prio6_wd;
	wire prio6_we;
	wire [1:0] prio7_qs;
	wire [1:0] prio7_wd;
	wire prio7_we;
	wire [1:0] prio8_qs;
	wire [1:0] prio8_wd;
	wire prio8_we;
	wire [1:0] prio9_qs;
	wire [1:0] prio9_wd;
	wire prio9_we;
	wire [1:0] prio10_qs;
	wire [1:0] prio10_wd;
	wire prio10_we;
	wire [1:0] prio11_qs;
	wire [1:0] prio11_wd;
	wire prio11_we;
	wire [1:0] prio12_qs;
	wire [1:0] prio12_wd;
	wire prio12_we;
	wire [1:0] prio13_qs;
	wire [1:0] prio13_wd;
	wire prio13_we;
	wire [1:0] prio14_qs;
	wire [1:0] prio14_wd;
	wire prio14_we;
	wire [1:0] prio15_qs;
	wire [1:0] prio15_wd;
	wire prio15_we;
	wire [1:0] prio16_qs;
	wire [1:0] prio16_wd;
	wire prio16_we;
	wire [1:0] prio17_qs;
	wire [1:0] prio17_wd;
	wire prio17_we;
	wire [1:0] prio18_qs;
	wire [1:0] prio18_wd;
	wire prio18_we;
	wire [1:0] prio19_qs;
	wire [1:0] prio19_wd;
	wire prio19_we;
	wire [1:0] prio20_qs;
	wire [1:0] prio20_wd;
	wire prio20_we;
	wire [1:0] prio21_qs;
	wire [1:0] prio21_wd;
	wire prio21_we;
	wire [1:0] prio22_qs;
	wire [1:0] prio22_wd;
	wire prio22_we;
	wire [1:0] prio23_qs;
	wire [1:0] prio23_wd;
	wire prio23_we;
	wire [1:0] prio24_qs;
	wire [1:0] prio24_wd;
	wire prio24_we;
	wire [1:0] prio25_qs;
	wire [1:0] prio25_wd;
	wire prio25_we;
	wire [1:0] prio26_qs;
	wire [1:0] prio26_wd;
	wire prio26_we;
	wire [1:0] prio27_qs;
	wire [1:0] prio27_wd;
	wire prio27_we;
	wire [1:0] prio28_qs;
	wire [1:0] prio28_wd;
	wire prio28_we;
	wire [1:0] prio29_qs;
	wire [1:0] prio29_wd;
	wire prio29_we;
	wire [1:0] prio30_qs;
	wire [1:0] prio30_wd;
	wire prio30_we;
	wire [1:0] prio31_qs;
	wire [1:0] prio31_wd;
	wire prio31_we;
	wire [1:0] prio32_qs;
	wire [1:0] prio32_wd;
	wire prio32_we;
	wire [1:0] prio33_qs;
	wire [1:0] prio33_wd;
	wire prio33_we;
	wire [1:0] prio34_qs;
	wire [1:0] prio34_wd;
	wire prio34_we;
	wire [1:0] prio35_qs;
	wire [1:0] prio35_wd;
	wire prio35_we;
	wire [1:0] prio36_qs;
	wire [1:0] prio36_wd;
	wire prio36_we;
	wire [1:0] prio37_qs;
	wire [1:0] prio37_wd;
	wire prio37_we;
	wire [1:0] prio38_qs;
	wire [1:0] prio38_wd;
	wire prio38_we;
	wire [1:0] prio39_qs;
	wire [1:0] prio39_wd;
	wire prio39_we;
	wire [1:0] prio40_qs;
	wire [1:0] prio40_wd;
	wire prio40_we;
	wire [1:0] prio41_qs;
	wire [1:0] prio41_wd;
	wire prio41_we;
	wire [1:0] prio42_qs;
	wire [1:0] prio42_wd;
	wire prio42_we;
	wire [1:0] prio43_qs;
	wire [1:0] prio43_wd;
	wire prio43_we;
	wire [1:0] prio44_qs;
	wire [1:0] prio44_wd;
	wire prio44_we;
	wire [1:0] prio45_qs;
	wire [1:0] prio45_wd;
	wire prio45_we;
	wire [1:0] prio46_qs;
	wire [1:0] prio46_wd;
	wire prio46_we;
	wire [1:0] prio47_qs;
	wire [1:0] prio47_wd;
	wire prio47_we;
	wire [1:0] prio48_qs;
	wire [1:0] prio48_wd;
	wire prio48_we;
	wire [1:0] prio49_qs;
	wire [1:0] prio49_wd;
	wire prio49_we;
	wire [1:0] prio50_qs;
	wire [1:0] prio50_wd;
	wire prio50_we;
	wire [1:0] prio51_qs;
	wire [1:0] prio51_wd;
	wire prio51_we;
	wire [1:0] prio52_qs;
	wire [1:0] prio52_wd;
	wire prio52_we;
	wire [1:0] prio53_qs;
	wire [1:0] prio53_wd;
	wire prio53_we;
	wire [1:0] prio54_qs;
	wire [1:0] prio54_wd;
	wire prio54_we;
	wire [1:0] prio55_qs;
	wire [1:0] prio55_wd;
	wire prio55_we;
	wire [1:0] prio56_qs;
	wire [1:0] prio56_wd;
	wire prio56_we;
	wire [1:0] prio57_qs;
	wire [1:0] prio57_wd;
	wire prio57_we;
	wire [1:0] prio58_qs;
	wire [1:0] prio58_wd;
	wire prio58_we;
	wire [1:0] prio59_qs;
	wire [1:0] prio59_wd;
	wire prio59_we;
	wire [1:0] prio60_qs;
	wire [1:0] prio60_wd;
	wire prio60_we;
	wire [1:0] prio61_qs;
	wire [1:0] prio61_wd;
	wire prio61_we;
	wire [1:0] prio62_qs;
	wire [1:0] prio62_wd;
	wire prio62_we;
	wire ie00_e0_qs;
	wire ie00_e0_wd;
	wire ie00_e0_we;
	wire ie00_e1_qs;
	wire ie00_e1_wd;
	wire ie00_e1_we;
	wire ie00_e2_qs;
	wire ie00_e2_wd;
	wire ie00_e2_we;
	wire ie00_e3_qs;
	wire ie00_e3_wd;
	wire ie00_e3_we;
	wire ie00_e4_qs;
	wire ie00_e4_wd;
	wire ie00_e4_we;
	wire ie00_e5_qs;
	wire ie00_e5_wd;
	wire ie00_e5_we;
	wire ie00_e6_qs;
	wire ie00_e6_wd;
	wire ie00_e6_we;
	wire ie00_e7_qs;
	wire ie00_e7_wd;
	wire ie00_e7_we;
	wire ie00_e8_qs;
	wire ie00_e8_wd;
	wire ie00_e8_we;
	wire ie00_e9_qs;
	wire ie00_e9_wd;
	wire ie00_e9_we;
	wire ie00_e10_qs;
	wire ie00_e10_wd;
	wire ie00_e10_we;
	wire ie00_e11_qs;
	wire ie00_e11_wd;
	wire ie00_e11_we;
	wire ie00_e12_qs;
	wire ie00_e12_wd;
	wire ie00_e12_we;
	wire ie00_e13_qs;
	wire ie00_e13_wd;
	wire ie00_e13_we;
	wire ie00_e14_qs;
	wire ie00_e14_wd;
	wire ie00_e14_we;
	wire ie00_e15_qs;
	wire ie00_e15_wd;
	wire ie00_e15_we;
	wire ie00_e16_qs;
	wire ie00_e16_wd;
	wire ie00_e16_we;
	wire ie00_e17_qs;
	wire ie00_e17_wd;
	wire ie00_e17_we;
	wire ie00_e18_qs;
	wire ie00_e18_wd;
	wire ie00_e18_we;
	wire ie00_e19_qs;
	wire ie00_e19_wd;
	wire ie00_e19_we;
	wire ie00_e20_qs;
	wire ie00_e20_wd;
	wire ie00_e20_we;
	wire ie00_e21_qs;
	wire ie00_e21_wd;
	wire ie00_e21_we;
	wire ie00_e22_qs;
	wire ie00_e22_wd;
	wire ie00_e22_we;
	wire ie00_e23_qs;
	wire ie00_e23_wd;
	wire ie00_e23_we;
	wire ie00_e24_qs;
	wire ie00_e24_wd;
	wire ie00_e24_we;
	wire ie00_e25_qs;
	wire ie00_e25_wd;
	wire ie00_e25_we;
	wire ie00_e26_qs;
	wire ie00_e26_wd;
	wire ie00_e26_we;
	wire ie00_e27_qs;
	wire ie00_e27_wd;
	wire ie00_e27_we;
	wire ie00_e28_qs;
	wire ie00_e28_wd;
	wire ie00_e28_we;
	wire ie00_e29_qs;
	wire ie00_e29_wd;
	wire ie00_e29_we;
	wire ie00_e30_qs;
	wire ie00_e30_wd;
	wire ie00_e30_we;
	wire ie00_e31_qs;
	wire ie00_e31_wd;
	wire ie00_e31_we;
	wire ie01_e32_qs;
	wire ie01_e32_wd;
	wire ie01_e32_we;
	wire ie01_e33_qs;
	wire ie01_e33_wd;
	wire ie01_e33_we;
	wire ie01_e34_qs;
	wire ie01_e34_wd;
	wire ie01_e34_we;
	wire ie01_e35_qs;
	wire ie01_e35_wd;
	wire ie01_e35_we;
	wire ie01_e36_qs;
	wire ie01_e36_wd;
	wire ie01_e36_we;
	wire ie01_e37_qs;
	wire ie01_e37_wd;
	wire ie01_e37_we;
	wire ie01_e38_qs;
	wire ie01_e38_wd;
	wire ie01_e38_we;
	wire ie01_e39_qs;
	wire ie01_e39_wd;
	wire ie01_e39_we;
	wire ie01_e40_qs;
	wire ie01_e40_wd;
	wire ie01_e40_we;
	wire ie01_e41_qs;
	wire ie01_e41_wd;
	wire ie01_e41_we;
	wire ie01_e42_qs;
	wire ie01_e42_wd;
	wire ie01_e42_we;
	wire ie01_e43_qs;
	wire ie01_e43_wd;
	wire ie01_e43_we;
	wire ie01_e44_qs;
	wire ie01_e44_wd;
	wire ie01_e44_we;
	wire ie01_e45_qs;
	wire ie01_e45_wd;
	wire ie01_e45_we;
	wire ie01_e46_qs;
	wire ie01_e46_wd;
	wire ie01_e46_we;
	wire ie01_e47_qs;
	wire ie01_e47_wd;
	wire ie01_e47_we;
	wire ie01_e48_qs;
	wire ie01_e48_wd;
	wire ie01_e48_we;
	wire ie01_e49_qs;
	wire ie01_e49_wd;
	wire ie01_e49_we;
	wire ie01_e50_qs;
	wire ie01_e50_wd;
	wire ie01_e50_we;
	wire ie01_e51_qs;
	wire ie01_e51_wd;
	wire ie01_e51_we;
	wire ie01_e52_qs;
	wire ie01_e52_wd;
	wire ie01_e52_we;
	wire ie01_e53_qs;
	wire ie01_e53_wd;
	wire ie01_e53_we;
	wire ie01_e54_qs;
	wire ie01_e54_wd;
	wire ie01_e54_we;
	wire ie01_e55_qs;
	wire ie01_e55_wd;
	wire ie01_e55_we;
	wire ie01_e56_qs;
	wire ie01_e56_wd;
	wire ie01_e56_we;
	wire ie01_e57_qs;
	wire ie01_e57_wd;
	wire ie01_e57_we;
	wire ie01_e58_qs;
	wire ie01_e58_wd;
	wire ie01_e58_we;
	wire ie01_e59_qs;
	wire ie01_e59_wd;
	wire ie01_e59_we;
	wire ie01_e60_qs;
	wire ie01_e60_wd;
	wire ie01_e60_we;
	wire ie01_e61_qs;
	wire ie01_e61_wd;
	wire ie01_e61_we;
	wire ie01_e62_qs;
	wire ie01_e62_wd;
	wire ie01_e62_we;
	wire [1:0] threshold0_qs;
	wire [1:0] threshold0_wd;
	wire threshold0_we;
	wire [5:0] cc0_qs;
	wire [5:0] cc0_wd;
	wire cc0_we;
	wire cc0_re;
	wire msip0_qs;
	wire msip0_wd;
	wire msip0_we;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[6]),
		.d(hw2reg[7]),
		.qe(),
		.q(),
		.qs(ip0_p0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[8]),
		.d(hw2reg[9]),
		.qe(),
		.q(),
		.qs(ip0_p1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[10]),
		.d(hw2reg[11]),
		.qe(),
		.q(),
		.qs(ip0_p2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[12]),
		.d(hw2reg[13]),
		.qe(),
		.q(),
		.qs(ip0_p3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[14]),
		.d(hw2reg[15]),
		.qe(),
		.q(),
		.qs(ip0_p4_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[16]),
		.d(hw2reg[17]),
		.qe(),
		.q(),
		.qs(ip0_p5_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[18]),
		.d(hw2reg[19]),
		.qe(),
		.q(),
		.qs(ip0_p6_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[20]),
		.d(hw2reg[21]),
		.qe(),
		.q(),
		.qs(ip0_p7_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[22]),
		.d(hw2reg[23]),
		.qe(),
		.q(),
		.qs(ip0_p8_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[24]),
		.d(hw2reg[25]),
		.qe(),
		.q(),
		.qs(ip0_p9_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[26]),
		.d(hw2reg[27]),
		.qe(),
		.q(),
		.qs(ip0_p10_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[28]),
		.d(hw2reg[29]),
		.qe(),
		.q(),
		.qs(ip0_p11_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[30]),
		.d(hw2reg[31]),
		.qe(),
		.q(),
		.qs(ip0_p12_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[32]),
		.d(hw2reg[33]),
		.qe(),
		.q(),
		.qs(ip0_p13_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[34]),
		.d(hw2reg[35]),
		.qe(),
		.q(),
		.qs(ip0_p14_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[36]),
		.d(hw2reg[37]),
		.qe(),
		.q(),
		.qs(ip0_p15_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[38]),
		.d(hw2reg[39]),
		.qe(),
		.q(),
		.qs(ip0_p16_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[40]),
		.d(hw2reg[41]),
		.qe(),
		.q(),
		.qs(ip0_p17_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[42]),
		.d(hw2reg[43]),
		.qe(),
		.q(),
		.qs(ip0_p18_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[44]),
		.d(hw2reg[45]),
		.qe(),
		.q(),
		.qs(ip0_p19_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[46]),
		.d(hw2reg[47]),
		.qe(),
		.q(),
		.qs(ip0_p20_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[48]),
		.d(hw2reg[49]),
		.qe(),
		.q(),
		.qs(ip0_p21_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[50]),
		.d(hw2reg[51]),
		.qe(),
		.q(),
		.qs(ip0_p22_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[52]),
		.d(hw2reg[53]),
		.qe(),
		.q(),
		.qs(ip0_p23_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[54]),
		.d(hw2reg[55]),
		.qe(),
		.q(),
		.qs(ip0_p24_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[56]),
		.d(hw2reg[57]),
		.qe(),
		.q(),
		.qs(ip0_p25_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[58]),
		.d(hw2reg[59]),
		.qe(),
		.q(),
		.qs(ip0_p26_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[60]),
		.d(hw2reg[61]),
		.qe(),
		.q(),
		.qs(ip0_p27_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[62]),
		.d(hw2reg[63]),
		.qe(),
		.q(),
		.qs(ip0_p28_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[64]),
		.d(hw2reg[65]),
		.qe(),
		.q(),
		.qs(ip0_p29_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[66]),
		.d(hw2reg[67]),
		.qe(),
		.q(),
		.qs(ip0_p30_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip0_p31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[68]),
		.d(hw2reg[69]),
		.qe(),
		.q(),
		.qs(ip0_p31_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p32(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[70]),
		.d(hw2reg[71]),
		.qe(),
		.q(),
		.qs(ip1_p32_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p33(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[72]),
		.d(hw2reg[73]),
		.qe(),
		.q(),
		.qs(ip1_p33_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p34(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[74]),
		.d(hw2reg[75]),
		.qe(),
		.q(),
		.qs(ip1_p34_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p35(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[76]),
		.d(hw2reg[77]),
		.qe(),
		.q(),
		.qs(ip1_p35_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p36(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[78]),
		.d(hw2reg[79]),
		.qe(),
		.q(),
		.qs(ip1_p36_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p37(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[80]),
		.d(hw2reg[81]),
		.qe(),
		.q(),
		.qs(ip1_p37_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p38(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[82]),
		.d(hw2reg[83]),
		.qe(),
		.q(),
		.qs(ip1_p38_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p39(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[84]),
		.d(hw2reg[85]),
		.qe(),
		.q(),
		.qs(ip1_p39_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p40(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[86]),
		.d(hw2reg[87]),
		.qe(),
		.q(),
		.qs(ip1_p40_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p41(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[88]),
		.d(hw2reg[89]),
		.qe(),
		.q(),
		.qs(ip1_p41_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p42(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[90]),
		.d(hw2reg[91]),
		.qe(),
		.q(),
		.qs(ip1_p42_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p43(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[92]),
		.d(hw2reg[93]),
		.qe(),
		.q(),
		.qs(ip1_p43_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p44(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[94]),
		.d(hw2reg[95]),
		.qe(),
		.q(),
		.qs(ip1_p44_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p45(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[96]),
		.d(hw2reg[97]),
		.qe(),
		.q(),
		.qs(ip1_p45_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p46(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[98]),
		.d(hw2reg[99]),
		.qe(),
		.q(),
		.qs(ip1_p46_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p47(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[100]),
		.d(hw2reg[101]),
		.qe(),
		.q(),
		.qs(ip1_p47_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p48(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[102]),
		.d(hw2reg[103]),
		.qe(),
		.q(),
		.qs(ip1_p48_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p49(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[104]),
		.d(hw2reg[105]),
		.qe(),
		.q(),
		.qs(ip1_p49_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p50(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[106]),
		.d(hw2reg[107]),
		.qe(),
		.q(),
		.qs(ip1_p50_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p51(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[108]),
		.d(hw2reg[109]),
		.qe(),
		.q(),
		.qs(ip1_p51_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p52(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[110]),
		.d(hw2reg[111]),
		.qe(),
		.q(),
		.qs(ip1_p52_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p53(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[112]),
		.d(hw2reg[113]),
		.qe(),
		.q(),
		.qs(ip1_p53_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p54(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[114]),
		.d(hw2reg[115]),
		.qe(),
		.q(),
		.qs(ip1_p54_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p55(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[116]),
		.d(hw2reg[117]),
		.qe(),
		.q(),
		.qs(ip1_p55_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p56(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[118]),
		.d(hw2reg[119]),
		.qe(),
		.q(),
		.qs(ip1_p56_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p57(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[120]),
		.d(hw2reg[121]),
		.qe(),
		.q(),
		.qs(ip1_p57_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p58(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[122]),
		.d(hw2reg[123]),
		.qe(),
		.q(),
		.qs(ip1_p58_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p59(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[124]),
		.d(hw2reg[125]),
		.qe(),
		.q(),
		.qs(ip1_p59_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p60(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[126]),
		.d(hw2reg[127]),
		.qe(),
		.q(),
		.qs(ip1_p60_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p61(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[128]),
		.d(hw2reg[129]),
		.qe(),
		.q(),
		.qs(ip1_p61_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_ip1_p62(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[130]),
		.d(hw2reg[131]),
		.qe(),
		.q(),
		.qs(ip1_p62_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le0_we),
		.wd(le0_le0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[200]),
		.qs(le0_le0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le1_we),
		.wd(le0_le1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[201]),
		.qs(le0_le1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le2_we),
		.wd(le0_le2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[202]),
		.qs(le0_le2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le3_we),
		.wd(le0_le3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[203]),
		.qs(le0_le3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le4_we),
		.wd(le0_le4_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[204]),
		.qs(le0_le4_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le5_we),
		.wd(le0_le5_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[205]),
		.qs(le0_le5_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le6_we),
		.wd(le0_le6_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[206]),
		.qs(le0_le6_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le7_we),
		.wd(le0_le7_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[207]),
		.qs(le0_le7_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le8_we),
		.wd(le0_le8_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[208]),
		.qs(le0_le8_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le9_we),
		.wd(le0_le9_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[209]),
		.qs(le0_le9_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le10_we),
		.wd(le0_le10_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[210]),
		.qs(le0_le10_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le11_we),
		.wd(le0_le11_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[211]),
		.qs(le0_le11_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le12_we),
		.wd(le0_le12_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[212]),
		.qs(le0_le12_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le13_we),
		.wd(le0_le13_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[213]),
		.qs(le0_le13_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le14_we),
		.wd(le0_le14_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[214]),
		.qs(le0_le14_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le15_we),
		.wd(le0_le15_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[215]),
		.qs(le0_le15_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le16_we),
		.wd(le0_le16_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[216]),
		.qs(le0_le16_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le17_we),
		.wd(le0_le17_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[217]),
		.qs(le0_le17_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le18_we),
		.wd(le0_le18_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[218]),
		.qs(le0_le18_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le19_we),
		.wd(le0_le19_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[219]),
		.qs(le0_le19_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le20_we),
		.wd(le0_le20_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[220]),
		.qs(le0_le20_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le21_we),
		.wd(le0_le21_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[221]),
		.qs(le0_le21_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le22_we),
		.wd(le0_le22_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[222]),
		.qs(le0_le22_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le23_we),
		.wd(le0_le23_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[223]),
		.qs(le0_le23_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le24_we),
		.wd(le0_le24_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[224]),
		.qs(le0_le24_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le25_we),
		.wd(le0_le25_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[225]),
		.qs(le0_le25_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le26_we),
		.wd(le0_le26_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[226]),
		.qs(le0_le26_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le27_we),
		.wd(le0_le27_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[227]),
		.qs(le0_le27_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le28_we),
		.wd(le0_le28_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[228]),
		.qs(le0_le28_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le29_we),
		.wd(le0_le29_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[229]),
		.qs(le0_le29_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le30_we),
		.wd(le0_le30_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[230]),
		.qs(le0_le30_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le0_le31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le0_le31_we),
		.wd(le0_le31_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[231]),
		.qs(le0_le31_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le32(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le32_we),
		.wd(le1_le32_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[232]),
		.qs(le1_le32_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le33(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le33_we),
		.wd(le1_le33_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[233]),
		.qs(le1_le33_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le34(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le34_we),
		.wd(le1_le34_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[234]),
		.qs(le1_le34_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le35(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le35_we),
		.wd(le1_le35_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[235]),
		.qs(le1_le35_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le36(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le36_we),
		.wd(le1_le36_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[236]),
		.qs(le1_le36_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le37(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le37_we),
		.wd(le1_le37_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[237]),
		.qs(le1_le37_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le38(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le38_we),
		.wd(le1_le38_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[238]),
		.qs(le1_le38_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le39(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le39_we),
		.wd(le1_le39_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[239]),
		.qs(le1_le39_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le40(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le40_we),
		.wd(le1_le40_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[240]),
		.qs(le1_le40_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le41(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le41_we),
		.wd(le1_le41_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[241]),
		.qs(le1_le41_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le42(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le42_we),
		.wd(le1_le42_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[242]),
		.qs(le1_le42_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le43(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le43_we),
		.wd(le1_le43_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[243]),
		.qs(le1_le43_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le44(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le44_we),
		.wd(le1_le44_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[244]),
		.qs(le1_le44_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le45(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le45_we),
		.wd(le1_le45_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[245]),
		.qs(le1_le45_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le46(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le46_we),
		.wd(le1_le46_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[246]),
		.qs(le1_le46_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le47(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le47_we),
		.wd(le1_le47_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[247]),
		.qs(le1_le47_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le48(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le48_we),
		.wd(le1_le48_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[248]),
		.qs(le1_le48_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le49(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le49_we),
		.wd(le1_le49_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[249]),
		.qs(le1_le49_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le50(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le50_we),
		.wd(le1_le50_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[250]),
		.qs(le1_le50_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le51(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le51_we),
		.wd(le1_le51_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[251]),
		.qs(le1_le51_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le52(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le52_we),
		.wd(le1_le52_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[252]),
		.qs(le1_le52_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le53(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le53_we),
		.wd(le1_le53_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[253]),
		.qs(le1_le53_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le54(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le54_we),
		.wd(le1_le54_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[254]),
		.qs(le1_le54_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le55(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le55_we),
		.wd(le1_le55_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[255]),
		.qs(le1_le55_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le56(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le56_we),
		.wd(le1_le56_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[256]),
		.qs(le1_le56_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le57(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le57_we),
		.wd(le1_le57_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[257]),
		.qs(le1_le57_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le58(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le58_we),
		.wd(le1_le58_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[258]),
		.qs(le1_le58_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le59(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le59_we),
		.wd(le1_le59_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[259]),
		.qs(le1_le59_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le60(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le60_we),
		.wd(le1_le60_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[260]),
		.qs(le1_le60_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le61(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le61_we),
		.wd(le1_le61_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[261]),
		.qs(le1_le61_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_le1_le62(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(le1_le62_we),
		.wd(le1_le62_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[262]),
		.qs(le1_le62_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio0_we),
		.wd(prio0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[199-:2]),
		.qs(prio0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio1_we),
		.wd(prio1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[197-:2]),
		.qs(prio1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio2_we),
		.wd(prio2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[195-:2]),
		.qs(prio2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio3_we),
		.wd(prio3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[193-:2]),
		.qs(prio3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio4_we),
		.wd(prio4_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[191-:2]),
		.qs(prio4_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio5_we),
		.wd(prio5_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[189-:2]),
		.qs(prio5_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio6_we),
		.wd(prio6_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[187-:2]),
		.qs(prio6_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio7_we),
		.wd(prio7_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[185-:2]),
		.qs(prio7_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio8_we),
		.wd(prio8_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[183-:2]),
		.qs(prio8_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio9_we),
		.wd(prio9_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[181-:2]),
		.qs(prio9_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio10_we),
		.wd(prio10_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[179-:2]),
		.qs(prio10_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio11_we),
		.wd(prio11_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[177-:2]),
		.qs(prio11_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio12_we),
		.wd(prio12_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[175-:2]),
		.qs(prio12_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio13_we),
		.wd(prio13_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[173-:2]),
		.qs(prio13_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio14_we),
		.wd(prio14_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[171-:2]),
		.qs(prio14_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio15_we),
		.wd(prio15_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[169-:2]),
		.qs(prio15_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio16_we),
		.wd(prio16_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[167-:2]),
		.qs(prio16_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio17_we),
		.wd(prio17_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[165-:2]),
		.qs(prio17_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio18_we),
		.wd(prio18_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[163-:2]),
		.qs(prio18_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio19_we),
		.wd(prio19_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[161-:2]),
		.qs(prio19_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio20_we),
		.wd(prio20_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[159-:2]),
		.qs(prio20_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio21_we),
		.wd(prio21_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[157-:2]),
		.qs(prio21_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio22_we),
		.wd(prio22_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[155-:2]),
		.qs(prio22_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio23_we),
		.wd(prio23_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[153-:2]),
		.qs(prio23_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio24_we),
		.wd(prio24_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[151-:2]),
		.qs(prio24_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio25_we),
		.wd(prio25_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[149-:2]),
		.qs(prio25_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio26_we),
		.wd(prio26_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[147-:2]),
		.qs(prio26_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio27_we),
		.wd(prio27_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[145-:2]),
		.qs(prio27_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio28_we),
		.wd(prio28_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[143-:2]),
		.qs(prio28_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio29_we),
		.wd(prio29_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[141-:2]),
		.qs(prio29_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio30_we),
		.wd(prio30_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[139-:2]),
		.qs(prio30_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio31_we),
		.wd(prio31_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[137-:2]),
		.qs(prio31_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio32(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio32_we),
		.wd(prio32_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[135-:2]),
		.qs(prio32_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio33(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio33_we),
		.wd(prio33_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[133-:2]),
		.qs(prio33_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio34(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio34_we),
		.wd(prio34_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[131-:2]),
		.qs(prio34_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio35(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio35_we),
		.wd(prio35_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[129-:2]),
		.qs(prio35_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio36(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio36_we),
		.wd(prio36_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[127-:2]),
		.qs(prio36_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio37(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio37_we),
		.wd(prio37_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[125-:2]),
		.qs(prio37_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio38(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio38_we),
		.wd(prio38_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[123-:2]),
		.qs(prio38_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio39(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio39_we),
		.wd(prio39_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[121-:2]),
		.qs(prio39_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio40(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio40_we),
		.wd(prio40_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[119-:2]),
		.qs(prio40_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio41(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio41_we),
		.wd(prio41_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[117-:2]),
		.qs(prio41_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio42(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio42_we),
		.wd(prio42_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[115-:2]),
		.qs(prio42_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio43(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio43_we),
		.wd(prio43_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[113-:2]),
		.qs(prio43_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio44(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio44_we),
		.wd(prio44_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[111-:2]),
		.qs(prio44_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio45(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio45_we),
		.wd(prio45_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[109-:2]),
		.qs(prio45_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio46(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio46_we),
		.wd(prio46_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[107-:2]),
		.qs(prio46_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio47(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio47_we),
		.wd(prio47_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[105-:2]),
		.qs(prio47_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio48(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio48_we),
		.wd(prio48_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[103-:2]),
		.qs(prio48_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio49(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio49_we),
		.wd(prio49_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[101-:2]),
		.qs(prio49_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio50(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio50_we),
		.wd(prio50_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[99-:2]),
		.qs(prio50_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio51(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio51_we),
		.wd(prio51_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[97-:2]),
		.qs(prio51_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio52(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio52_we),
		.wd(prio52_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[95-:2]),
		.qs(prio52_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio53(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio53_we),
		.wd(prio53_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[93-:2]),
		.qs(prio53_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio54(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio54_we),
		.wd(prio54_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[91-:2]),
		.qs(prio54_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio55(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio55_we),
		.wd(prio55_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[89-:2]),
		.qs(prio55_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio56(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio56_we),
		.wd(prio56_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[87-:2]),
		.qs(prio56_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio57(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio57_we),
		.wd(prio57_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[85-:2]),
		.qs(prio57_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio58(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio58_we),
		.wd(prio58_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[83-:2]),
		.qs(prio58_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio59(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio59_we),
		.wd(prio59_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[81-:2]),
		.qs(prio59_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio60(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio60_we),
		.wd(prio60_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[79-:2]),
		.qs(prio60_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio61(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio61_we),
		.wd(prio61_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[77-:2]),
		.qs(prio61_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_prio62(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(prio62_we),
		.wd(prio62_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[75-:2]),
		.qs(prio62_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e0_we),
		.wd(ie00_e0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[11]),
		.qs(ie00_e0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e1_we),
		.wd(ie00_e1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[12]),
		.qs(ie00_e1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e2_we),
		.wd(ie00_e2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[13]),
		.qs(ie00_e2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e3_we),
		.wd(ie00_e3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[14]),
		.qs(ie00_e3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e4_we),
		.wd(ie00_e4_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[15]),
		.qs(ie00_e4_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e5_we),
		.wd(ie00_e5_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[16]),
		.qs(ie00_e5_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e6_we),
		.wd(ie00_e6_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[17]),
		.qs(ie00_e6_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e7_we),
		.wd(ie00_e7_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[18]),
		.qs(ie00_e7_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e8_we),
		.wd(ie00_e8_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[19]),
		.qs(ie00_e8_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e9_we),
		.wd(ie00_e9_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[20]),
		.qs(ie00_e9_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e10_we),
		.wd(ie00_e10_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[21]),
		.qs(ie00_e10_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e11_we),
		.wd(ie00_e11_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[22]),
		.qs(ie00_e11_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e12_we),
		.wd(ie00_e12_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[23]),
		.qs(ie00_e12_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e13_we),
		.wd(ie00_e13_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[24]),
		.qs(ie00_e13_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e14_we),
		.wd(ie00_e14_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[25]),
		.qs(ie00_e14_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e15_we),
		.wd(ie00_e15_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[26]),
		.qs(ie00_e15_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e16_we),
		.wd(ie00_e16_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[27]),
		.qs(ie00_e16_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e17_we),
		.wd(ie00_e17_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[28]),
		.qs(ie00_e17_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e18_we),
		.wd(ie00_e18_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[29]),
		.qs(ie00_e18_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e19_we),
		.wd(ie00_e19_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[30]),
		.qs(ie00_e19_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e20_we),
		.wd(ie00_e20_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[31]),
		.qs(ie00_e20_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e21_we),
		.wd(ie00_e21_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[32]),
		.qs(ie00_e21_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e22_we),
		.wd(ie00_e22_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[33]),
		.qs(ie00_e22_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e23_we),
		.wd(ie00_e23_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[34]),
		.qs(ie00_e23_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e24_we),
		.wd(ie00_e24_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[35]),
		.qs(ie00_e24_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e25_we),
		.wd(ie00_e25_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[36]),
		.qs(ie00_e25_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e26_we),
		.wd(ie00_e26_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[37]),
		.qs(ie00_e26_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e27_we),
		.wd(ie00_e27_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[38]),
		.qs(ie00_e27_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e28_we),
		.wd(ie00_e28_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[39]),
		.qs(ie00_e28_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e29_we),
		.wd(ie00_e29_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[40]),
		.qs(ie00_e29_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e30_we),
		.wd(ie00_e30_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[41]),
		.qs(ie00_e30_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie00_e31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie00_e31_we),
		.wd(ie00_e31_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[42]),
		.qs(ie00_e31_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e32(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e32_we),
		.wd(ie01_e32_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[43]),
		.qs(ie01_e32_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e33(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e33_we),
		.wd(ie01_e33_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[44]),
		.qs(ie01_e33_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e34(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e34_we),
		.wd(ie01_e34_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[45]),
		.qs(ie01_e34_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e35(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e35_we),
		.wd(ie01_e35_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[46]),
		.qs(ie01_e35_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e36(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e36_we),
		.wd(ie01_e36_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[47]),
		.qs(ie01_e36_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e37(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e37_we),
		.wd(ie01_e37_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[48]),
		.qs(ie01_e37_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e38(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e38_we),
		.wd(ie01_e38_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[49]),
		.qs(ie01_e38_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e39(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e39_we),
		.wd(ie01_e39_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[50]),
		.qs(ie01_e39_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e40(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e40_we),
		.wd(ie01_e40_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[51]),
		.qs(ie01_e40_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e41(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e41_we),
		.wd(ie01_e41_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[52]),
		.qs(ie01_e41_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e42(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e42_we),
		.wd(ie01_e42_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[53]),
		.qs(ie01_e42_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e43(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e43_we),
		.wd(ie01_e43_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[54]),
		.qs(ie01_e43_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e44(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e44_we),
		.wd(ie01_e44_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[55]),
		.qs(ie01_e44_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e45(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e45_we),
		.wd(ie01_e45_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[56]),
		.qs(ie01_e45_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e46(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e46_we),
		.wd(ie01_e46_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[57]),
		.qs(ie01_e46_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e47(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e47_we),
		.wd(ie01_e47_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[58]),
		.qs(ie01_e47_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e48(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e48_we),
		.wd(ie01_e48_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[59]),
		.qs(ie01_e48_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e49(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e49_we),
		.wd(ie01_e49_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[60]),
		.qs(ie01_e49_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e50(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e50_we),
		.wd(ie01_e50_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[61]),
		.qs(ie01_e50_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e51(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e51_we),
		.wd(ie01_e51_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[62]),
		.qs(ie01_e51_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e52(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e52_we),
		.wd(ie01_e52_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[63]),
		.qs(ie01_e52_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e53(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e53_we),
		.wd(ie01_e53_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[64]),
		.qs(ie01_e53_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e54(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e54_we),
		.wd(ie01_e54_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[65]),
		.qs(ie01_e54_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e55(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e55_we),
		.wd(ie01_e55_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[66]),
		.qs(ie01_e55_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e56(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e56_we),
		.wd(ie01_e56_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[67]),
		.qs(ie01_e56_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e57(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e57_we),
		.wd(ie01_e57_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[68]),
		.qs(ie01_e57_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e58(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e58_we),
		.wd(ie01_e58_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[69]),
		.qs(ie01_e58_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e59(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e59_we),
		.wd(ie01_e59_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[70]),
		.qs(ie01_e59_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e60(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e60_we),
		.wd(ie01_e60_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[71]),
		.qs(ie01_e60_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e61(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e61_we),
		.wd(ie01_e61_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[72]),
		.qs(ie01_e61_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ie01_e62(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ie01_e62_we),
		.wd(ie01_e62_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[73]),
		.qs(ie01_e62_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_threshold0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(threshold0_we),
		.wd(threshold0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[10-:2]),
		.qs(threshold0_qs)
	);
	prim_subreg_ext #(.DW(6)) u_cc0(
		.re(cc0_re),
		.we(cc0_we),
		.wd(cc0_wd),
		.d(hw2reg[5-:6]),
		.qre(reg2hw[1]),
		.qe(reg2hw[2]),
		.q(reg2hw[8-:6]),
		.qs(cc0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_msip0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(msip0_we),
		.wd(msip0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[-0]),
		.qs(msip0_qs)
	);
	reg [71:0] addr_hit;
	always @(*) begin
		addr_hit = {72 {1'sb0}};
		addr_hit[0] = reg_addr == RV_PLIC_IP0_OFFSET;
		addr_hit[1] = reg_addr == RV_PLIC_IP1_OFFSET;
		addr_hit[2] = reg_addr == RV_PLIC_LE0_OFFSET;
		addr_hit[3] = reg_addr == RV_PLIC_LE1_OFFSET;
		addr_hit[4] = reg_addr == RV_PLIC_PRIO0_OFFSET;
		addr_hit[5] = reg_addr == RV_PLIC_PRIO1_OFFSET;
		addr_hit[6] = reg_addr == RV_PLIC_PRIO2_OFFSET;
		addr_hit[7] = reg_addr == RV_PLIC_PRIO3_OFFSET;
		addr_hit[8] = reg_addr == RV_PLIC_PRIO4_OFFSET;
		addr_hit[9] = reg_addr == RV_PLIC_PRIO5_OFFSET;
		addr_hit[10] = reg_addr == RV_PLIC_PRIO6_OFFSET;
		addr_hit[11] = reg_addr == RV_PLIC_PRIO7_OFFSET;
		addr_hit[12] = reg_addr == RV_PLIC_PRIO8_OFFSET;
		addr_hit[13] = reg_addr == RV_PLIC_PRIO9_OFFSET;
		addr_hit[14] = reg_addr == RV_PLIC_PRIO10_OFFSET;
		addr_hit[15] = reg_addr == RV_PLIC_PRIO11_OFFSET;
		addr_hit[16] = reg_addr == RV_PLIC_PRIO12_OFFSET;
		addr_hit[17] = reg_addr == RV_PLIC_PRIO13_OFFSET;
		addr_hit[18] = reg_addr == RV_PLIC_PRIO14_OFFSET;
		addr_hit[19] = reg_addr == RV_PLIC_PRIO15_OFFSET;
		addr_hit[20] = reg_addr == RV_PLIC_PRIO16_OFFSET;
		addr_hit[21] = reg_addr == RV_PLIC_PRIO17_OFFSET;
		addr_hit[22] = reg_addr == RV_PLIC_PRIO18_OFFSET;
		addr_hit[23] = reg_addr == RV_PLIC_PRIO19_OFFSET;
		addr_hit[24] = reg_addr == RV_PLIC_PRIO20_OFFSET;
		addr_hit[25] = reg_addr == RV_PLIC_PRIO21_OFFSET;
		addr_hit[26] = reg_addr == RV_PLIC_PRIO22_OFFSET;
		addr_hit[27] = reg_addr == RV_PLIC_PRIO23_OFFSET;
		addr_hit[28] = reg_addr == RV_PLIC_PRIO24_OFFSET;
		addr_hit[29] = reg_addr == RV_PLIC_PRIO25_OFFSET;
		addr_hit[30] = reg_addr == RV_PLIC_PRIO26_OFFSET;
		addr_hit[31] = reg_addr == RV_PLIC_PRIO27_OFFSET;
		addr_hit[32] = reg_addr == RV_PLIC_PRIO28_OFFSET;
		addr_hit[33] = reg_addr == RV_PLIC_PRIO29_OFFSET;
		addr_hit[34] = reg_addr == RV_PLIC_PRIO30_OFFSET;
		addr_hit[35] = reg_addr == RV_PLIC_PRIO31_OFFSET;
		addr_hit[36] = reg_addr == RV_PLIC_PRIO32_OFFSET;
		addr_hit[37] = reg_addr == RV_PLIC_PRIO33_OFFSET;
		addr_hit[38] = reg_addr == RV_PLIC_PRIO34_OFFSET;
		addr_hit[39] = reg_addr == RV_PLIC_PRIO35_OFFSET;
		addr_hit[40] = reg_addr == RV_PLIC_PRIO36_OFFSET;
		addr_hit[41] = reg_addr == RV_PLIC_PRIO37_OFFSET;
		addr_hit[42] = reg_addr == RV_PLIC_PRIO38_OFFSET;
		addr_hit[43] = reg_addr == RV_PLIC_PRIO39_OFFSET;
		addr_hit[44] = reg_addr == RV_PLIC_PRIO40_OFFSET;
		addr_hit[45] = reg_addr == RV_PLIC_PRIO41_OFFSET;
		addr_hit[46] = reg_addr == RV_PLIC_PRIO42_OFFSET;
		addr_hit[47] = reg_addr == RV_PLIC_PRIO43_OFFSET;
		addr_hit[48] = reg_addr == RV_PLIC_PRIO44_OFFSET;
		addr_hit[49] = reg_addr == RV_PLIC_PRIO45_OFFSET;
		addr_hit[50] = reg_addr == RV_PLIC_PRIO46_OFFSET;
		addr_hit[51] = reg_addr == RV_PLIC_PRIO47_OFFSET;
		addr_hit[52] = reg_addr == RV_PLIC_PRIO48_OFFSET;
		addr_hit[53] = reg_addr == RV_PLIC_PRIO49_OFFSET;
		addr_hit[54] = reg_addr == RV_PLIC_PRIO50_OFFSET;
		addr_hit[55] = reg_addr == RV_PLIC_PRIO51_OFFSET;
		addr_hit[56] = reg_addr == RV_PLIC_PRIO52_OFFSET;
		addr_hit[57] = reg_addr == RV_PLIC_PRIO53_OFFSET;
		addr_hit[58] = reg_addr == RV_PLIC_PRIO54_OFFSET;
		addr_hit[59] = reg_addr == RV_PLIC_PRIO55_OFFSET;
		addr_hit[60] = reg_addr == RV_PLIC_PRIO56_OFFSET;
		addr_hit[61] = reg_addr == RV_PLIC_PRIO57_OFFSET;
		addr_hit[62] = reg_addr == RV_PLIC_PRIO58_OFFSET;
		addr_hit[63] = reg_addr == RV_PLIC_PRIO59_OFFSET;
		addr_hit[64] = reg_addr == RV_PLIC_PRIO60_OFFSET;
		addr_hit[65] = reg_addr == RV_PLIC_PRIO61_OFFSET;
		addr_hit[66] = reg_addr == RV_PLIC_PRIO62_OFFSET;
		addr_hit[67] = reg_addr == RV_PLIC_IE00_OFFSET;
		addr_hit[68] = reg_addr == RV_PLIC_IE01_OFFSET;
		addr_hit[69] = reg_addr == RV_PLIC_THRESHOLD0_OFFSET;
		addr_hit[70] = reg_addr == RV_PLIC_CC0_OFFSET;
		addr_hit[71] = reg_addr == RV_PLIC_MSIP0_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (RV_PLIC_PERMIT[284+:4] != (RV_PLIC_PERMIT[284+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (RV_PLIC_PERMIT[280+:4] != (RV_PLIC_PERMIT[280+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (RV_PLIC_PERMIT[276+:4] != (RV_PLIC_PERMIT[276+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (RV_PLIC_PERMIT[272+:4] != (RV_PLIC_PERMIT[272+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (RV_PLIC_PERMIT[268+:4] != (RV_PLIC_PERMIT[268+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (RV_PLIC_PERMIT[264+:4] != (RV_PLIC_PERMIT[264+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (RV_PLIC_PERMIT[260+:4] != (RV_PLIC_PERMIT[260+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (RV_PLIC_PERMIT[256+:4] != (RV_PLIC_PERMIT[256+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (RV_PLIC_PERMIT[252+:4] != (RV_PLIC_PERMIT[252+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (RV_PLIC_PERMIT[248+:4] != (RV_PLIC_PERMIT[248+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (RV_PLIC_PERMIT[244+:4] != (RV_PLIC_PERMIT[244+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (RV_PLIC_PERMIT[240+:4] != (RV_PLIC_PERMIT[240+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (RV_PLIC_PERMIT[236+:4] != (RV_PLIC_PERMIT[236+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (RV_PLIC_PERMIT[232+:4] != (RV_PLIC_PERMIT[232+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (RV_PLIC_PERMIT[228+:4] != (RV_PLIC_PERMIT[228+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[15] && reg_we) && (RV_PLIC_PERMIT[224+:4] != (RV_PLIC_PERMIT[224+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[16] && reg_we) && (RV_PLIC_PERMIT[220+:4] != (RV_PLIC_PERMIT[220+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[17] && reg_we) && (RV_PLIC_PERMIT[216+:4] != (RV_PLIC_PERMIT[216+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[18] && reg_we) && (RV_PLIC_PERMIT[212+:4] != (RV_PLIC_PERMIT[212+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[19] && reg_we) && (RV_PLIC_PERMIT[208+:4] != (RV_PLIC_PERMIT[208+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[20] && reg_we) && (RV_PLIC_PERMIT[204+:4] != (RV_PLIC_PERMIT[204+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[21] && reg_we) && (RV_PLIC_PERMIT[200+:4] != (RV_PLIC_PERMIT[200+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[22] && reg_we) && (RV_PLIC_PERMIT[196+:4] != (RV_PLIC_PERMIT[196+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[23] && reg_we) && (RV_PLIC_PERMIT[192+:4] != (RV_PLIC_PERMIT[192+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[24] && reg_we) && (RV_PLIC_PERMIT[188+:4] != (RV_PLIC_PERMIT[188+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[25] && reg_we) && (RV_PLIC_PERMIT[184+:4] != (RV_PLIC_PERMIT[184+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[26] && reg_we) && (RV_PLIC_PERMIT[180+:4] != (RV_PLIC_PERMIT[180+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[27] && reg_we) && (RV_PLIC_PERMIT[176+:4] != (RV_PLIC_PERMIT[176+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[28] && reg_we) && (RV_PLIC_PERMIT[172+:4] != (RV_PLIC_PERMIT[172+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[29] && reg_we) && (RV_PLIC_PERMIT[168+:4] != (RV_PLIC_PERMIT[168+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[30] && reg_we) && (RV_PLIC_PERMIT[164+:4] != (RV_PLIC_PERMIT[164+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[31] && reg_we) && (RV_PLIC_PERMIT[160+:4] != (RV_PLIC_PERMIT[160+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[32] && reg_we) && (RV_PLIC_PERMIT[156+:4] != (RV_PLIC_PERMIT[156+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[33] && reg_we) && (RV_PLIC_PERMIT[152+:4] != (RV_PLIC_PERMIT[152+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[34] && reg_we) && (RV_PLIC_PERMIT[148+:4] != (RV_PLIC_PERMIT[148+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[35] && reg_we) && (RV_PLIC_PERMIT[144+:4] != (RV_PLIC_PERMIT[144+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[36] && reg_we) && (RV_PLIC_PERMIT[140+:4] != (RV_PLIC_PERMIT[140+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[37] && reg_we) && (RV_PLIC_PERMIT[136+:4] != (RV_PLIC_PERMIT[136+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[38] && reg_we) && (RV_PLIC_PERMIT[132+:4] != (RV_PLIC_PERMIT[132+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[39] && reg_we) && (RV_PLIC_PERMIT[128+:4] != (RV_PLIC_PERMIT[128+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[40] && reg_we) && (RV_PLIC_PERMIT[124+:4] != (RV_PLIC_PERMIT[124+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[41] && reg_we) && (RV_PLIC_PERMIT[120+:4] != (RV_PLIC_PERMIT[120+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[42] && reg_we) && (RV_PLIC_PERMIT[116+:4] != (RV_PLIC_PERMIT[116+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[43] && reg_we) && (RV_PLIC_PERMIT[112+:4] != (RV_PLIC_PERMIT[112+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[44] && reg_we) && (RV_PLIC_PERMIT[108+:4] != (RV_PLIC_PERMIT[108+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[45] && reg_we) && (RV_PLIC_PERMIT[104+:4] != (RV_PLIC_PERMIT[104+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[46] && reg_we) && (RV_PLIC_PERMIT[100+:4] != (RV_PLIC_PERMIT[100+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[47] && reg_we) && (RV_PLIC_PERMIT[96+:4] != (RV_PLIC_PERMIT[96+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[48] && reg_we) && (RV_PLIC_PERMIT[92+:4] != (RV_PLIC_PERMIT[92+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[49] && reg_we) && (RV_PLIC_PERMIT[88+:4] != (RV_PLIC_PERMIT[88+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[50] && reg_we) && (RV_PLIC_PERMIT[84+:4] != (RV_PLIC_PERMIT[84+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[51] && reg_we) && (RV_PLIC_PERMIT[80+:4] != (RV_PLIC_PERMIT[80+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[52] && reg_we) && (RV_PLIC_PERMIT[76+:4] != (RV_PLIC_PERMIT[76+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[53] && reg_we) && (RV_PLIC_PERMIT[72+:4] != (RV_PLIC_PERMIT[72+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[54] && reg_we) && (RV_PLIC_PERMIT[68+:4] != (RV_PLIC_PERMIT[68+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[55] && reg_we) && (RV_PLIC_PERMIT[64+:4] != (RV_PLIC_PERMIT[64+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[56] && reg_we) && (RV_PLIC_PERMIT[60+:4] != (RV_PLIC_PERMIT[60+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[57] && reg_we) && (RV_PLIC_PERMIT[56+:4] != (RV_PLIC_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[58] && reg_we) && (RV_PLIC_PERMIT[52+:4] != (RV_PLIC_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[59] && reg_we) && (RV_PLIC_PERMIT[48+:4] != (RV_PLIC_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[60] && reg_we) && (RV_PLIC_PERMIT[44+:4] != (RV_PLIC_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[61] && reg_we) && (RV_PLIC_PERMIT[40+:4] != (RV_PLIC_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[62] && reg_we) && (RV_PLIC_PERMIT[36+:4] != (RV_PLIC_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[63] && reg_we) && (RV_PLIC_PERMIT[32+:4] != (RV_PLIC_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[64] && reg_we) && (RV_PLIC_PERMIT[28+:4] != (RV_PLIC_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[65] && reg_we) && (RV_PLIC_PERMIT[24+:4] != (RV_PLIC_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[66] && reg_we) && (RV_PLIC_PERMIT[20+:4] != (RV_PLIC_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[67] && reg_we) && (RV_PLIC_PERMIT[16+:4] != (RV_PLIC_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[68] && reg_we) && (RV_PLIC_PERMIT[12+:4] != (RV_PLIC_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[69] && reg_we) && (RV_PLIC_PERMIT[8+:4] != (RV_PLIC_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[70] && reg_we) && (RV_PLIC_PERMIT[4+:4] != (RV_PLIC_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[71] && reg_we) && (RV_PLIC_PERMIT[0+:4] != (RV_PLIC_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign le0_le0_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le0_wd = reg_wdata[0];
	assign le0_le1_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le1_wd = reg_wdata[1];
	assign le0_le2_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le2_wd = reg_wdata[2];
	assign le0_le3_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le3_wd = reg_wdata[3];
	assign le0_le4_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le4_wd = reg_wdata[4];
	assign le0_le5_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le5_wd = reg_wdata[5];
	assign le0_le6_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le6_wd = reg_wdata[6];
	assign le0_le7_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le7_wd = reg_wdata[7];
	assign le0_le8_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le8_wd = reg_wdata[8];
	assign le0_le9_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le9_wd = reg_wdata[9];
	assign le0_le10_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le10_wd = reg_wdata[10];
	assign le0_le11_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le11_wd = reg_wdata[11];
	assign le0_le12_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le12_wd = reg_wdata[12];
	assign le0_le13_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le13_wd = reg_wdata[13];
	assign le0_le14_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le14_wd = reg_wdata[14];
	assign le0_le15_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le15_wd = reg_wdata[15];
	assign le0_le16_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le16_wd = reg_wdata[16];
	assign le0_le17_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le17_wd = reg_wdata[17];
	assign le0_le18_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le18_wd = reg_wdata[18];
	assign le0_le19_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le19_wd = reg_wdata[19];
	assign le0_le20_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le20_wd = reg_wdata[20];
	assign le0_le21_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le21_wd = reg_wdata[21];
	assign le0_le22_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le22_wd = reg_wdata[22];
	assign le0_le23_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le23_wd = reg_wdata[23];
	assign le0_le24_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le24_wd = reg_wdata[24];
	assign le0_le25_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le25_wd = reg_wdata[25];
	assign le0_le26_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le26_wd = reg_wdata[26];
	assign le0_le27_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le27_wd = reg_wdata[27];
	assign le0_le28_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le28_wd = reg_wdata[28];
	assign le0_le29_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le29_wd = reg_wdata[29];
	assign le0_le30_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le30_wd = reg_wdata[30];
	assign le0_le31_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign le0_le31_wd = reg_wdata[31];
	assign le1_le32_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le32_wd = reg_wdata[0];
	assign le1_le33_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le33_wd = reg_wdata[1];
	assign le1_le34_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le34_wd = reg_wdata[2];
	assign le1_le35_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le35_wd = reg_wdata[3];
	assign le1_le36_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le36_wd = reg_wdata[4];
	assign le1_le37_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le37_wd = reg_wdata[5];
	assign le1_le38_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le38_wd = reg_wdata[6];
	assign le1_le39_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le39_wd = reg_wdata[7];
	assign le1_le40_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le40_wd = reg_wdata[8];
	assign le1_le41_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le41_wd = reg_wdata[9];
	assign le1_le42_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le42_wd = reg_wdata[10];
	assign le1_le43_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le43_wd = reg_wdata[11];
	assign le1_le44_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le44_wd = reg_wdata[12];
	assign le1_le45_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le45_wd = reg_wdata[13];
	assign le1_le46_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le46_wd = reg_wdata[14];
	assign le1_le47_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le47_wd = reg_wdata[15];
	assign le1_le48_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le48_wd = reg_wdata[16];
	assign le1_le49_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le49_wd = reg_wdata[17];
	assign le1_le50_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le50_wd = reg_wdata[18];
	assign le1_le51_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le51_wd = reg_wdata[19];
	assign le1_le52_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le52_wd = reg_wdata[20];
	assign le1_le53_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le53_wd = reg_wdata[21];
	assign le1_le54_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le54_wd = reg_wdata[22];
	assign le1_le55_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le55_wd = reg_wdata[23];
	assign le1_le56_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le56_wd = reg_wdata[24];
	assign le1_le57_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le57_wd = reg_wdata[25];
	assign le1_le58_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le58_wd = reg_wdata[26];
	assign le1_le59_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le59_wd = reg_wdata[27];
	assign le1_le60_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le60_wd = reg_wdata[28];
	assign le1_le61_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le61_wd = reg_wdata[29];
	assign le1_le62_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign le1_le62_wd = reg_wdata[30];
	assign prio0_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign prio0_wd = reg_wdata[1:0];
	assign prio1_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign prio1_wd = reg_wdata[1:0];
	assign prio2_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign prio2_wd = reg_wdata[1:0];
	assign prio3_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign prio3_wd = reg_wdata[1:0];
	assign prio4_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign prio4_wd = reg_wdata[1:0];
	assign prio5_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign prio5_wd = reg_wdata[1:0];
	assign prio6_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign prio6_wd = reg_wdata[1:0];
	assign prio7_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign prio7_wd = reg_wdata[1:0];
	assign prio8_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign prio8_wd = reg_wdata[1:0];
	assign prio9_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign prio9_wd = reg_wdata[1:0];
	assign prio10_we = (addr_hit[14] & reg_we) & ~wr_err;
	assign prio10_wd = reg_wdata[1:0];
	assign prio11_we = (addr_hit[15] & reg_we) & ~wr_err;
	assign prio11_wd = reg_wdata[1:0];
	assign prio12_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign prio12_wd = reg_wdata[1:0];
	assign prio13_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign prio13_wd = reg_wdata[1:0];
	assign prio14_we = (addr_hit[18] & reg_we) & ~wr_err;
	assign prio14_wd = reg_wdata[1:0];
	assign prio15_we = (addr_hit[19] & reg_we) & ~wr_err;
	assign prio15_wd = reg_wdata[1:0];
	assign prio16_we = (addr_hit[20] & reg_we) & ~wr_err;
	assign prio16_wd = reg_wdata[1:0];
	assign prio17_we = (addr_hit[21] & reg_we) & ~wr_err;
	assign prio17_wd = reg_wdata[1:0];
	assign prio18_we = (addr_hit[22] & reg_we) & ~wr_err;
	assign prio18_wd = reg_wdata[1:0];
	assign prio19_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign prio19_wd = reg_wdata[1:0];
	assign prio20_we = (addr_hit[24] & reg_we) & ~wr_err;
	assign prio20_wd = reg_wdata[1:0];
	assign prio21_we = (addr_hit[25] & reg_we) & ~wr_err;
	assign prio21_wd = reg_wdata[1:0];
	assign prio22_we = (addr_hit[26] & reg_we) & ~wr_err;
	assign prio22_wd = reg_wdata[1:0];
	assign prio23_we = (addr_hit[27] & reg_we) & ~wr_err;
	assign prio23_wd = reg_wdata[1:0];
	assign prio24_we = (addr_hit[28] & reg_we) & ~wr_err;
	assign prio24_wd = reg_wdata[1:0];
	assign prio25_we = (addr_hit[29] & reg_we) & ~wr_err;
	assign prio25_wd = reg_wdata[1:0];
	assign prio26_we = (addr_hit[30] & reg_we) & ~wr_err;
	assign prio26_wd = reg_wdata[1:0];
	assign prio27_we = (addr_hit[31] & reg_we) & ~wr_err;
	assign prio27_wd = reg_wdata[1:0];
	assign prio28_we = (addr_hit[32] & reg_we) & ~wr_err;
	assign prio28_wd = reg_wdata[1:0];
	assign prio29_we = (addr_hit[33] & reg_we) & ~wr_err;
	assign prio29_wd = reg_wdata[1:0];
	assign prio30_we = (addr_hit[34] & reg_we) & ~wr_err;
	assign prio30_wd = reg_wdata[1:0];
	assign prio31_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign prio31_wd = reg_wdata[1:0];
	assign prio32_we = (addr_hit[36] & reg_we) & ~wr_err;
	assign prio32_wd = reg_wdata[1:0];
	assign prio33_we = (addr_hit[37] & reg_we) & ~wr_err;
	assign prio33_wd = reg_wdata[1:0];
	assign prio34_we = (addr_hit[38] & reg_we) & ~wr_err;
	assign prio34_wd = reg_wdata[1:0];
	assign prio35_we = (addr_hit[39] & reg_we) & ~wr_err;
	assign prio35_wd = reg_wdata[1:0];
	assign prio36_we = (addr_hit[40] & reg_we) & ~wr_err;
	assign prio36_wd = reg_wdata[1:0];
	assign prio37_we = (addr_hit[41] & reg_we) & ~wr_err;
	assign prio37_wd = reg_wdata[1:0];
	assign prio38_we = (addr_hit[42] & reg_we) & ~wr_err;
	assign prio38_wd = reg_wdata[1:0];
	assign prio39_we = (addr_hit[43] & reg_we) & ~wr_err;
	assign prio39_wd = reg_wdata[1:0];
	assign prio40_we = (addr_hit[44] & reg_we) & ~wr_err;
	assign prio40_wd = reg_wdata[1:0];
	assign prio41_we = (addr_hit[45] & reg_we) & ~wr_err;
	assign prio41_wd = reg_wdata[1:0];
	assign prio42_we = (addr_hit[46] & reg_we) & ~wr_err;
	assign prio42_wd = reg_wdata[1:0];
	assign prio43_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign prio43_wd = reg_wdata[1:0];
	assign prio44_we = (addr_hit[48] & reg_we) & ~wr_err;
	assign prio44_wd = reg_wdata[1:0];
	assign prio45_we = (addr_hit[49] & reg_we) & ~wr_err;
	assign prio45_wd = reg_wdata[1:0];
	assign prio46_we = (addr_hit[50] & reg_we) & ~wr_err;
	assign prio46_wd = reg_wdata[1:0];
	assign prio47_we = (addr_hit[51] & reg_we) & ~wr_err;
	assign prio47_wd = reg_wdata[1:0];
	assign prio48_we = (addr_hit[52] & reg_we) & ~wr_err;
	assign prio48_wd = reg_wdata[1:0];
	assign prio49_we = (addr_hit[53] & reg_we) & ~wr_err;
	assign prio49_wd = reg_wdata[1:0];
	assign prio50_we = (addr_hit[54] & reg_we) & ~wr_err;
	assign prio50_wd = reg_wdata[1:0];
	assign prio51_we = (addr_hit[55] & reg_we) & ~wr_err;
	assign prio51_wd = reg_wdata[1:0];
	assign prio52_we = (addr_hit[56] & reg_we) & ~wr_err;
	assign prio52_wd = reg_wdata[1:0];
	assign prio53_we = (addr_hit[57] & reg_we) & ~wr_err;
	assign prio53_wd = reg_wdata[1:0];
	assign prio54_we = (addr_hit[58] & reg_we) & ~wr_err;
	assign prio54_wd = reg_wdata[1:0];
	assign prio55_we = (addr_hit[59] & reg_we) & ~wr_err;
	assign prio55_wd = reg_wdata[1:0];
	assign prio56_we = (addr_hit[60] & reg_we) & ~wr_err;
	assign prio56_wd = reg_wdata[1:0];
	assign prio57_we = (addr_hit[61] & reg_we) & ~wr_err;
	assign prio57_wd = reg_wdata[1:0];
	assign prio58_we = (addr_hit[62] & reg_we) & ~wr_err;
	assign prio58_wd = reg_wdata[1:0];
	assign prio59_we = (addr_hit[63] & reg_we) & ~wr_err;
	assign prio59_wd = reg_wdata[1:0];
	assign prio60_we = (addr_hit[64] & reg_we) & ~wr_err;
	assign prio60_wd = reg_wdata[1:0];
	assign prio61_we = (addr_hit[65] & reg_we) & ~wr_err;
	assign prio61_wd = reg_wdata[1:0];
	assign prio62_we = (addr_hit[66] & reg_we) & ~wr_err;
	assign prio62_wd = reg_wdata[1:0];
	assign ie00_e0_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e0_wd = reg_wdata[0];
	assign ie00_e1_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e1_wd = reg_wdata[1];
	assign ie00_e2_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e2_wd = reg_wdata[2];
	assign ie00_e3_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e3_wd = reg_wdata[3];
	assign ie00_e4_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e4_wd = reg_wdata[4];
	assign ie00_e5_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e5_wd = reg_wdata[5];
	assign ie00_e6_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e6_wd = reg_wdata[6];
	assign ie00_e7_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e7_wd = reg_wdata[7];
	assign ie00_e8_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e8_wd = reg_wdata[8];
	assign ie00_e9_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e9_wd = reg_wdata[9];
	assign ie00_e10_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e10_wd = reg_wdata[10];
	assign ie00_e11_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e11_wd = reg_wdata[11];
	assign ie00_e12_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e12_wd = reg_wdata[12];
	assign ie00_e13_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e13_wd = reg_wdata[13];
	assign ie00_e14_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e14_wd = reg_wdata[14];
	assign ie00_e15_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e15_wd = reg_wdata[15];
	assign ie00_e16_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e16_wd = reg_wdata[16];
	assign ie00_e17_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e17_wd = reg_wdata[17];
	assign ie00_e18_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e18_wd = reg_wdata[18];
	assign ie00_e19_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e19_wd = reg_wdata[19];
	assign ie00_e20_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e20_wd = reg_wdata[20];
	assign ie00_e21_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e21_wd = reg_wdata[21];
	assign ie00_e22_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e22_wd = reg_wdata[22];
	assign ie00_e23_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e23_wd = reg_wdata[23];
	assign ie00_e24_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e24_wd = reg_wdata[24];
	assign ie00_e25_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e25_wd = reg_wdata[25];
	assign ie00_e26_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e26_wd = reg_wdata[26];
	assign ie00_e27_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e27_wd = reg_wdata[27];
	assign ie00_e28_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e28_wd = reg_wdata[28];
	assign ie00_e29_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e29_wd = reg_wdata[29];
	assign ie00_e30_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e30_wd = reg_wdata[30];
	assign ie00_e31_we = (addr_hit[67] & reg_we) & ~wr_err;
	assign ie00_e31_wd = reg_wdata[31];
	assign ie01_e32_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e32_wd = reg_wdata[0];
	assign ie01_e33_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e33_wd = reg_wdata[1];
	assign ie01_e34_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e34_wd = reg_wdata[2];
	assign ie01_e35_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e35_wd = reg_wdata[3];
	assign ie01_e36_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e36_wd = reg_wdata[4];
	assign ie01_e37_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e37_wd = reg_wdata[5];
	assign ie01_e38_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e38_wd = reg_wdata[6];
	assign ie01_e39_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e39_wd = reg_wdata[7];
	assign ie01_e40_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e40_wd = reg_wdata[8];
	assign ie01_e41_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e41_wd = reg_wdata[9];
	assign ie01_e42_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e42_wd = reg_wdata[10];
	assign ie01_e43_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e43_wd = reg_wdata[11];
	assign ie01_e44_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e44_wd = reg_wdata[12];
	assign ie01_e45_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e45_wd = reg_wdata[13];
	assign ie01_e46_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e46_wd = reg_wdata[14];
	assign ie01_e47_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e47_wd = reg_wdata[15];
	assign ie01_e48_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e48_wd = reg_wdata[16];
	assign ie01_e49_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e49_wd = reg_wdata[17];
	assign ie01_e50_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e50_wd = reg_wdata[18];
	assign ie01_e51_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e51_wd = reg_wdata[19];
	assign ie01_e52_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e52_wd = reg_wdata[20];
	assign ie01_e53_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e53_wd = reg_wdata[21];
	assign ie01_e54_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e54_wd = reg_wdata[22];
	assign ie01_e55_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e55_wd = reg_wdata[23];
	assign ie01_e56_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e56_wd = reg_wdata[24];
	assign ie01_e57_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e57_wd = reg_wdata[25];
	assign ie01_e58_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e58_wd = reg_wdata[26];
	assign ie01_e59_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e59_wd = reg_wdata[27];
	assign ie01_e60_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e60_wd = reg_wdata[28];
	assign ie01_e61_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e61_wd = reg_wdata[29];
	assign ie01_e62_we = (addr_hit[68] & reg_we) & ~wr_err;
	assign ie01_e62_wd = reg_wdata[30];
	assign threshold0_we = (addr_hit[69] & reg_we) & ~wr_err;
	assign threshold0_wd = reg_wdata[1:0];
	assign cc0_we = (addr_hit[70] & reg_we) & ~wr_err;
	assign cc0_wd = reg_wdata[5:0];
	assign cc0_re = addr_hit[70] && reg_re;
	assign msip0_we = (addr_hit[71] & reg_we) & ~wr_err;
	assign msip0_wd = reg_wdata[0];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: begin
				reg_rdata_next[0] = ip0_p0_qs;
				reg_rdata_next[1] = ip0_p1_qs;
				reg_rdata_next[2] = ip0_p2_qs;
				reg_rdata_next[3] = ip0_p3_qs;
				reg_rdata_next[4] = ip0_p4_qs;
				reg_rdata_next[5] = ip0_p5_qs;
				reg_rdata_next[6] = ip0_p6_qs;
				reg_rdata_next[7] = ip0_p7_qs;
				reg_rdata_next[8] = ip0_p8_qs;
				reg_rdata_next[9] = ip0_p9_qs;
				reg_rdata_next[10] = ip0_p10_qs;
				reg_rdata_next[11] = ip0_p11_qs;
				reg_rdata_next[12] = ip0_p12_qs;
				reg_rdata_next[13] = ip0_p13_qs;
				reg_rdata_next[14] = ip0_p14_qs;
				reg_rdata_next[15] = ip0_p15_qs;
				reg_rdata_next[16] = ip0_p16_qs;
				reg_rdata_next[17] = ip0_p17_qs;
				reg_rdata_next[18] = ip0_p18_qs;
				reg_rdata_next[19] = ip0_p19_qs;
				reg_rdata_next[20] = ip0_p20_qs;
				reg_rdata_next[21] = ip0_p21_qs;
				reg_rdata_next[22] = ip0_p22_qs;
				reg_rdata_next[23] = ip0_p23_qs;
				reg_rdata_next[24] = ip0_p24_qs;
				reg_rdata_next[25] = ip0_p25_qs;
				reg_rdata_next[26] = ip0_p26_qs;
				reg_rdata_next[27] = ip0_p27_qs;
				reg_rdata_next[28] = ip0_p28_qs;
				reg_rdata_next[29] = ip0_p29_qs;
				reg_rdata_next[30] = ip0_p30_qs;
				reg_rdata_next[31] = ip0_p31_qs;
			end
			addr_hit[1]: begin
				reg_rdata_next[0] = ip1_p32_qs;
				reg_rdata_next[1] = ip1_p33_qs;
				reg_rdata_next[2] = ip1_p34_qs;
				reg_rdata_next[3] = ip1_p35_qs;
				reg_rdata_next[4] = ip1_p36_qs;
				reg_rdata_next[5] = ip1_p37_qs;
				reg_rdata_next[6] = ip1_p38_qs;
				reg_rdata_next[7] = ip1_p39_qs;
				reg_rdata_next[8] = ip1_p40_qs;
				reg_rdata_next[9] = ip1_p41_qs;
				reg_rdata_next[10] = ip1_p42_qs;
				reg_rdata_next[11] = ip1_p43_qs;
				reg_rdata_next[12] = ip1_p44_qs;
				reg_rdata_next[13] = ip1_p45_qs;
				reg_rdata_next[14] = ip1_p46_qs;
				reg_rdata_next[15] = ip1_p47_qs;
				reg_rdata_next[16] = ip1_p48_qs;
				reg_rdata_next[17] = ip1_p49_qs;
				reg_rdata_next[18] = ip1_p50_qs;
				reg_rdata_next[19] = ip1_p51_qs;
				reg_rdata_next[20] = ip1_p52_qs;
				reg_rdata_next[21] = ip1_p53_qs;
				reg_rdata_next[22] = ip1_p54_qs;
				reg_rdata_next[23] = ip1_p55_qs;
				reg_rdata_next[24] = ip1_p56_qs;
				reg_rdata_next[25] = ip1_p57_qs;
				reg_rdata_next[26] = ip1_p58_qs;
				reg_rdata_next[27] = ip1_p59_qs;
				reg_rdata_next[28] = ip1_p60_qs;
				reg_rdata_next[29] = ip1_p61_qs;
				reg_rdata_next[30] = ip1_p62_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[0] = le0_le0_qs;
				reg_rdata_next[1] = le0_le1_qs;
				reg_rdata_next[2] = le0_le2_qs;
				reg_rdata_next[3] = le0_le3_qs;
				reg_rdata_next[4] = le0_le4_qs;
				reg_rdata_next[5] = le0_le5_qs;
				reg_rdata_next[6] = le0_le6_qs;
				reg_rdata_next[7] = le0_le7_qs;
				reg_rdata_next[8] = le0_le8_qs;
				reg_rdata_next[9] = le0_le9_qs;
				reg_rdata_next[10] = le0_le10_qs;
				reg_rdata_next[11] = le0_le11_qs;
				reg_rdata_next[12] = le0_le12_qs;
				reg_rdata_next[13] = le0_le13_qs;
				reg_rdata_next[14] = le0_le14_qs;
				reg_rdata_next[15] = le0_le15_qs;
				reg_rdata_next[16] = le0_le16_qs;
				reg_rdata_next[17] = le0_le17_qs;
				reg_rdata_next[18] = le0_le18_qs;
				reg_rdata_next[19] = le0_le19_qs;
				reg_rdata_next[20] = le0_le20_qs;
				reg_rdata_next[21] = le0_le21_qs;
				reg_rdata_next[22] = le0_le22_qs;
				reg_rdata_next[23] = le0_le23_qs;
				reg_rdata_next[24] = le0_le24_qs;
				reg_rdata_next[25] = le0_le25_qs;
				reg_rdata_next[26] = le0_le26_qs;
				reg_rdata_next[27] = le0_le27_qs;
				reg_rdata_next[28] = le0_le28_qs;
				reg_rdata_next[29] = le0_le29_qs;
				reg_rdata_next[30] = le0_le30_qs;
				reg_rdata_next[31] = le0_le31_qs;
			end
			addr_hit[3]: begin
				reg_rdata_next[0] = le1_le32_qs;
				reg_rdata_next[1] = le1_le33_qs;
				reg_rdata_next[2] = le1_le34_qs;
				reg_rdata_next[3] = le1_le35_qs;
				reg_rdata_next[4] = le1_le36_qs;
				reg_rdata_next[5] = le1_le37_qs;
				reg_rdata_next[6] = le1_le38_qs;
				reg_rdata_next[7] = le1_le39_qs;
				reg_rdata_next[8] = le1_le40_qs;
				reg_rdata_next[9] = le1_le41_qs;
				reg_rdata_next[10] = le1_le42_qs;
				reg_rdata_next[11] = le1_le43_qs;
				reg_rdata_next[12] = le1_le44_qs;
				reg_rdata_next[13] = le1_le45_qs;
				reg_rdata_next[14] = le1_le46_qs;
				reg_rdata_next[15] = le1_le47_qs;
				reg_rdata_next[16] = le1_le48_qs;
				reg_rdata_next[17] = le1_le49_qs;
				reg_rdata_next[18] = le1_le50_qs;
				reg_rdata_next[19] = le1_le51_qs;
				reg_rdata_next[20] = le1_le52_qs;
				reg_rdata_next[21] = le1_le53_qs;
				reg_rdata_next[22] = le1_le54_qs;
				reg_rdata_next[23] = le1_le55_qs;
				reg_rdata_next[24] = le1_le56_qs;
				reg_rdata_next[25] = le1_le57_qs;
				reg_rdata_next[26] = le1_le58_qs;
				reg_rdata_next[27] = le1_le59_qs;
				reg_rdata_next[28] = le1_le60_qs;
				reg_rdata_next[29] = le1_le61_qs;
				reg_rdata_next[30] = le1_le62_qs;
			end
			addr_hit[4]: reg_rdata_next[1:0] = prio0_qs;
			addr_hit[5]: reg_rdata_next[1:0] = prio1_qs;
			addr_hit[6]: reg_rdata_next[1:0] = prio2_qs;
			addr_hit[7]: reg_rdata_next[1:0] = prio3_qs;
			addr_hit[8]: reg_rdata_next[1:0] = prio4_qs;
			addr_hit[9]: reg_rdata_next[1:0] = prio5_qs;
			addr_hit[10]: reg_rdata_next[1:0] = prio6_qs;
			addr_hit[11]: reg_rdata_next[1:0] = prio7_qs;
			addr_hit[12]: reg_rdata_next[1:0] = prio8_qs;
			addr_hit[13]: reg_rdata_next[1:0] = prio9_qs;
			addr_hit[14]: reg_rdata_next[1:0] = prio10_qs;
			addr_hit[15]: reg_rdata_next[1:0] = prio11_qs;
			addr_hit[16]: reg_rdata_next[1:0] = prio12_qs;
			addr_hit[17]: reg_rdata_next[1:0] = prio13_qs;
			addr_hit[18]: reg_rdata_next[1:0] = prio14_qs;
			addr_hit[19]: reg_rdata_next[1:0] = prio15_qs;
			addr_hit[20]: reg_rdata_next[1:0] = prio16_qs;
			addr_hit[21]: reg_rdata_next[1:0] = prio17_qs;
			addr_hit[22]: reg_rdata_next[1:0] = prio18_qs;
			addr_hit[23]: reg_rdata_next[1:0] = prio19_qs;
			addr_hit[24]: reg_rdata_next[1:0] = prio20_qs;
			addr_hit[25]: reg_rdata_next[1:0] = prio21_qs;
			addr_hit[26]: reg_rdata_next[1:0] = prio22_qs;
			addr_hit[27]: reg_rdata_next[1:0] = prio23_qs;
			addr_hit[28]: reg_rdata_next[1:0] = prio24_qs;
			addr_hit[29]: reg_rdata_next[1:0] = prio25_qs;
			addr_hit[30]: reg_rdata_next[1:0] = prio26_qs;
			addr_hit[31]: reg_rdata_next[1:0] = prio27_qs;
			addr_hit[32]: reg_rdata_next[1:0] = prio28_qs;
			addr_hit[33]: reg_rdata_next[1:0] = prio29_qs;
			addr_hit[34]: reg_rdata_next[1:0] = prio30_qs;
			addr_hit[35]: reg_rdata_next[1:0] = prio31_qs;
			addr_hit[36]: reg_rdata_next[1:0] = prio32_qs;
			addr_hit[37]: reg_rdata_next[1:0] = prio33_qs;
			addr_hit[38]: reg_rdata_next[1:0] = prio34_qs;
			addr_hit[39]: reg_rdata_next[1:0] = prio35_qs;
			addr_hit[40]: reg_rdata_next[1:0] = prio36_qs;
			addr_hit[41]: reg_rdata_next[1:0] = prio37_qs;
			addr_hit[42]: reg_rdata_next[1:0] = prio38_qs;
			addr_hit[43]: reg_rdata_next[1:0] = prio39_qs;
			addr_hit[44]: reg_rdata_next[1:0] = prio40_qs;
			addr_hit[45]: reg_rdata_next[1:0] = prio41_qs;
			addr_hit[46]: reg_rdata_next[1:0] = prio42_qs;
			addr_hit[47]: reg_rdata_next[1:0] = prio43_qs;
			addr_hit[48]: reg_rdata_next[1:0] = prio44_qs;
			addr_hit[49]: reg_rdata_next[1:0] = prio45_qs;
			addr_hit[50]: reg_rdata_next[1:0] = prio46_qs;
			addr_hit[51]: reg_rdata_next[1:0] = prio47_qs;
			addr_hit[52]: reg_rdata_next[1:0] = prio48_qs;
			addr_hit[53]: reg_rdata_next[1:0] = prio49_qs;
			addr_hit[54]: reg_rdata_next[1:0] = prio50_qs;
			addr_hit[55]: reg_rdata_next[1:0] = prio51_qs;
			addr_hit[56]: reg_rdata_next[1:0] = prio52_qs;
			addr_hit[57]: reg_rdata_next[1:0] = prio53_qs;
			addr_hit[58]: reg_rdata_next[1:0] = prio54_qs;
			addr_hit[59]: reg_rdata_next[1:0] = prio55_qs;
			addr_hit[60]: reg_rdata_next[1:0] = prio56_qs;
			addr_hit[61]: reg_rdata_next[1:0] = prio57_qs;
			addr_hit[62]: reg_rdata_next[1:0] = prio58_qs;
			addr_hit[63]: reg_rdata_next[1:0] = prio59_qs;
			addr_hit[64]: reg_rdata_next[1:0] = prio60_qs;
			addr_hit[65]: reg_rdata_next[1:0] = prio61_qs;
			addr_hit[66]: reg_rdata_next[1:0] = prio62_qs;
			addr_hit[67]: begin
				reg_rdata_next[0] = ie00_e0_qs;
				reg_rdata_next[1] = ie00_e1_qs;
				reg_rdata_next[2] = ie00_e2_qs;
				reg_rdata_next[3] = ie00_e3_qs;
				reg_rdata_next[4] = ie00_e4_qs;
				reg_rdata_next[5] = ie00_e5_qs;
				reg_rdata_next[6] = ie00_e6_qs;
				reg_rdata_next[7] = ie00_e7_qs;
				reg_rdata_next[8] = ie00_e8_qs;
				reg_rdata_next[9] = ie00_e9_qs;
				reg_rdata_next[10] = ie00_e10_qs;
				reg_rdata_next[11] = ie00_e11_qs;
				reg_rdata_next[12] = ie00_e12_qs;
				reg_rdata_next[13] = ie00_e13_qs;
				reg_rdata_next[14] = ie00_e14_qs;
				reg_rdata_next[15] = ie00_e15_qs;
				reg_rdata_next[16] = ie00_e16_qs;
				reg_rdata_next[17] = ie00_e17_qs;
				reg_rdata_next[18] = ie00_e18_qs;
				reg_rdata_next[19] = ie00_e19_qs;
				reg_rdata_next[20] = ie00_e20_qs;
				reg_rdata_next[21] = ie00_e21_qs;
				reg_rdata_next[22] = ie00_e22_qs;
				reg_rdata_next[23] = ie00_e23_qs;
				reg_rdata_next[24] = ie00_e24_qs;
				reg_rdata_next[25] = ie00_e25_qs;
				reg_rdata_next[26] = ie00_e26_qs;
				reg_rdata_next[27] = ie00_e27_qs;
				reg_rdata_next[28] = ie00_e28_qs;
				reg_rdata_next[29] = ie00_e29_qs;
				reg_rdata_next[30] = ie00_e30_qs;
				reg_rdata_next[31] = ie00_e31_qs;
			end
			addr_hit[68]: begin
				reg_rdata_next[0] = ie01_e32_qs;
				reg_rdata_next[1] = ie01_e33_qs;
				reg_rdata_next[2] = ie01_e34_qs;
				reg_rdata_next[3] = ie01_e35_qs;
				reg_rdata_next[4] = ie01_e36_qs;
				reg_rdata_next[5] = ie01_e37_qs;
				reg_rdata_next[6] = ie01_e38_qs;
				reg_rdata_next[7] = ie01_e39_qs;
				reg_rdata_next[8] = ie01_e40_qs;
				reg_rdata_next[9] = ie01_e41_qs;
				reg_rdata_next[10] = ie01_e42_qs;
				reg_rdata_next[11] = ie01_e43_qs;
				reg_rdata_next[12] = ie01_e44_qs;
				reg_rdata_next[13] = ie01_e45_qs;
				reg_rdata_next[14] = ie01_e46_qs;
				reg_rdata_next[15] = ie01_e47_qs;
				reg_rdata_next[16] = ie01_e48_qs;
				reg_rdata_next[17] = ie01_e49_qs;
				reg_rdata_next[18] = ie01_e50_qs;
				reg_rdata_next[19] = ie01_e51_qs;
				reg_rdata_next[20] = ie01_e52_qs;
				reg_rdata_next[21] = ie01_e53_qs;
				reg_rdata_next[22] = ie01_e54_qs;
				reg_rdata_next[23] = ie01_e55_qs;
				reg_rdata_next[24] = ie01_e56_qs;
				reg_rdata_next[25] = ie01_e57_qs;
				reg_rdata_next[26] = ie01_e58_qs;
				reg_rdata_next[27] = ie01_e59_qs;
				reg_rdata_next[28] = ie01_e60_qs;
				reg_rdata_next[29] = ie01_e61_qs;
				reg_rdata_next[30] = ie01_e62_qs;
			end
			addr_hit[69]: reg_rdata_next[1:0] = threshold0_qs;
			addr_hit[70]: reg_rdata_next[5:0] = cc0_qs;
			addr_hit[71]: reg_rdata_next[0] = msip0_qs;
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module rv_plic (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	intr_src_i,
	irq_o,
	irq_id_o,
	msip_o
);
	localparam signed [31:0] NumSrc = 63;
	localparam signed [31:0] NumTarget = 1;
	localparam RV_PLIC_IP0_OFFSET = 10'h000;
	localparam RV_PLIC_IP1_OFFSET = 10'h004;
	localparam RV_PLIC_LE0_OFFSET = 10'h008;
	localparam RV_PLIC_LE1_OFFSET = 10'h00c;
	localparam RV_PLIC_PRIO0_OFFSET = 10'h010;
	localparam RV_PLIC_PRIO1_OFFSET = 10'h014;
	localparam RV_PLIC_PRIO2_OFFSET = 10'h018;
	localparam RV_PLIC_PRIO3_OFFSET = 10'h01c;
	localparam RV_PLIC_PRIO4_OFFSET = 10'h020;
	localparam RV_PLIC_PRIO5_OFFSET = 10'h024;
	localparam RV_PLIC_PRIO6_OFFSET = 10'h028;
	localparam RV_PLIC_PRIO7_OFFSET = 10'h02c;
	localparam RV_PLIC_PRIO8_OFFSET = 10'h030;
	localparam RV_PLIC_PRIO9_OFFSET = 10'h034;
	localparam RV_PLIC_PRIO10_OFFSET = 10'h038;
	localparam RV_PLIC_PRIO11_OFFSET = 10'h03c;
	localparam RV_PLIC_PRIO12_OFFSET = 10'h040;
	localparam RV_PLIC_PRIO13_OFFSET = 10'h044;
	localparam RV_PLIC_PRIO14_OFFSET = 10'h048;
	localparam RV_PLIC_PRIO15_OFFSET = 10'h04c;
	localparam RV_PLIC_PRIO16_OFFSET = 10'h050;
	localparam RV_PLIC_PRIO17_OFFSET = 10'h054;
	localparam RV_PLIC_PRIO18_OFFSET = 10'h058;
	localparam RV_PLIC_PRIO19_OFFSET = 10'h05c;
	localparam RV_PLIC_PRIO20_OFFSET = 10'h060;
	localparam RV_PLIC_PRIO21_OFFSET = 10'h064;
	localparam RV_PLIC_PRIO22_OFFSET = 10'h068;
	localparam RV_PLIC_PRIO23_OFFSET = 10'h06c;
	localparam RV_PLIC_PRIO24_OFFSET = 10'h070;
	localparam RV_PLIC_PRIO25_OFFSET = 10'h074;
	localparam RV_PLIC_PRIO26_OFFSET = 10'h078;
	localparam RV_PLIC_PRIO27_OFFSET = 10'h07c;
	localparam RV_PLIC_PRIO28_OFFSET = 10'h080;
	localparam RV_PLIC_PRIO29_OFFSET = 10'h084;
	localparam RV_PLIC_PRIO30_OFFSET = 10'h088;
	localparam RV_PLIC_PRIO31_OFFSET = 10'h08c;
	localparam RV_PLIC_PRIO32_OFFSET = 10'h090;
	localparam RV_PLIC_PRIO33_OFFSET = 10'h094;
	localparam RV_PLIC_PRIO34_OFFSET = 10'h098;
	localparam RV_PLIC_PRIO35_OFFSET = 10'h09c;
	localparam RV_PLIC_PRIO36_OFFSET = 10'h0a0;
	localparam RV_PLIC_PRIO37_OFFSET = 10'h0a4;
	localparam RV_PLIC_PRIO38_OFFSET = 10'h0a8;
	localparam RV_PLIC_PRIO39_OFFSET = 10'h0ac;
	localparam RV_PLIC_PRIO40_OFFSET = 10'h0b0;
	localparam RV_PLIC_PRIO41_OFFSET = 10'h0b4;
	localparam RV_PLIC_PRIO42_OFFSET = 10'h0b8;
	localparam RV_PLIC_PRIO43_OFFSET = 10'h0bc;
	localparam RV_PLIC_PRIO44_OFFSET = 10'h0c0;
	localparam RV_PLIC_PRIO45_OFFSET = 10'h0c4;
	localparam RV_PLIC_PRIO46_OFFSET = 10'h0c8;
	localparam RV_PLIC_PRIO47_OFFSET = 10'h0cc;
	localparam RV_PLIC_PRIO48_OFFSET = 10'h0d0;
	localparam RV_PLIC_PRIO49_OFFSET = 10'h0d4;
	localparam RV_PLIC_PRIO50_OFFSET = 10'h0d8;
	localparam RV_PLIC_PRIO51_OFFSET = 10'h0dc;
	localparam RV_PLIC_PRIO52_OFFSET = 10'h0e0;
	localparam RV_PLIC_PRIO53_OFFSET = 10'h0e4;
	localparam RV_PLIC_PRIO54_OFFSET = 10'h0e8;
	localparam RV_PLIC_PRIO55_OFFSET = 10'h0ec;
	localparam RV_PLIC_PRIO56_OFFSET = 10'h0f0;
	localparam RV_PLIC_PRIO57_OFFSET = 10'h0f4;
	localparam RV_PLIC_PRIO58_OFFSET = 10'h0f8;
	localparam RV_PLIC_PRIO59_OFFSET = 10'h0fc;
	localparam RV_PLIC_PRIO60_OFFSET = 10'h100;
	localparam RV_PLIC_PRIO61_OFFSET = 10'h104;
	localparam RV_PLIC_PRIO62_OFFSET = 10'h108;
	localparam RV_PLIC_IE00_OFFSET = 10'h200;
	localparam RV_PLIC_IE01_OFFSET = 10'h204;
	localparam RV_PLIC_THRESHOLD0_OFFSET = 10'h208;
	localparam RV_PLIC_CC0_OFFSET = 10'h20c;
	localparam RV_PLIC_MSIP0_OFFSET = 10'h210;
	localparam signed [31:0] RV_PLIC_IP0 = 0;
	localparam signed [31:0] RV_PLIC_IP1 = 1;
	localparam signed [31:0] RV_PLIC_LE0 = 2;
	localparam signed [31:0] RV_PLIC_LE1 = 3;
	localparam signed [31:0] RV_PLIC_PRIO0 = 4;
	localparam signed [31:0] RV_PLIC_PRIO1 = 5;
	localparam signed [31:0] RV_PLIC_PRIO2 = 6;
	localparam signed [31:0] RV_PLIC_PRIO3 = 7;
	localparam signed [31:0] RV_PLIC_PRIO4 = 8;
	localparam signed [31:0] RV_PLIC_PRIO5 = 9;
	localparam signed [31:0] RV_PLIC_PRIO6 = 10;
	localparam signed [31:0] RV_PLIC_PRIO7 = 11;
	localparam signed [31:0] RV_PLIC_PRIO8 = 12;
	localparam signed [31:0] RV_PLIC_PRIO9 = 13;
	localparam signed [31:0] RV_PLIC_PRIO10 = 14;
	localparam signed [31:0] RV_PLIC_PRIO11 = 15;
	localparam signed [31:0] RV_PLIC_PRIO12 = 16;
	localparam signed [31:0] RV_PLIC_PRIO13 = 17;
	localparam signed [31:0] RV_PLIC_PRIO14 = 18;
	localparam signed [31:0] RV_PLIC_PRIO15 = 19;
	localparam signed [31:0] RV_PLIC_PRIO16 = 20;
	localparam signed [31:0] RV_PLIC_PRIO17 = 21;
	localparam signed [31:0] RV_PLIC_PRIO18 = 22;
	localparam signed [31:0] RV_PLIC_PRIO19 = 23;
	localparam signed [31:0] RV_PLIC_PRIO20 = 24;
	localparam signed [31:0] RV_PLIC_PRIO21 = 25;
	localparam signed [31:0] RV_PLIC_PRIO22 = 26;
	localparam signed [31:0] RV_PLIC_PRIO23 = 27;
	localparam signed [31:0] RV_PLIC_PRIO24 = 28;
	localparam signed [31:0] RV_PLIC_PRIO25 = 29;
	localparam signed [31:0] RV_PLIC_PRIO26 = 30;
	localparam signed [31:0] RV_PLIC_PRIO27 = 31;
	localparam signed [31:0] RV_PLIC_PRIO28 = 32;
	localparam signed [31:0] RV_PLIC_PRIO29 = 33;
	localparam signed [31:0] RV_PLIC_PRIO30 = 34;
	localparam signed [31:0] RV_PLIC_PRIO31 = 35;
	localparam signed [31:0] RV_PLIC_PRIO32 = 36;
	localparam signed [31:0] RV_PLIC_PRIO33 = 37;
	localparam signed [31:0] RV_PLIC_PRIO34 = 38;
	localparam signed [31:0] RV_PLIC_PRIO35 = 39;
	localparam signed [31:0] RV_PLIC_PRIO36 = 40;
	localparam signed [31:0] RV_PLIC_PRIO37 = 41;
	localparam signed [31:0] RV_PLIC_PRIO38 = 42;
	localparam signed [31:0] RV_PLIC_PRIO39 = 43;
	localparam signed [31:0] RV_PLIC_PRIO40 = 44;
	localparam signed [31:0] RV_PLIC_PRIO41 = 45;
	localparam signed [31:0] RV_PLIC_PRIO42 = 46;
	localparam signed [31:0] RV_PLIC_PRIO43 = 47;
	localparam signed [31:0] RV_PLIC_PRIO44 = 48;
	localparam signed [31:0] RV_PLIC_PRIO45 = 49;
	localparam signed [31:0] RV_PLIC_PRIO46 = 50;
	localparam signed [31:0] RV_PLIC_PRIO47 = 51;
	localparam signed [31:0] RV_PLIC_PRIO48 = 52;
	localparam signed [31:0] RV_PLIC_PRIO49 = 53;
	localparam signed [31:0] RV_PLIC_PRIO50 = 54;
	localparam signed [31:0] RV_PLIC_PRIO51 = 55;
	localparam signed [31:0] RV_PLIC_PRIO52 = 56;
	localparam signed [31:0] RV_PLIC_PRIO53 = 57;
	localparam signed [31:0] RV_PLIC_PRIO54 = 58;
	localparam signed [31:0] RV_PLIC_PRIO55 = 59;
	localparam signed [31:0] RV_PLIC_PRIO56 = 60;
	localparam signed [31:0] RV_PLIC_PRIO57 = 61;
	localparam signed [31:0] RV_PLIC_PRIO58 = 62;
	localparam signed [31:0] RV_PLIC_PRIO59 = 63;
	localparam signed [31:0] RV_PLIC_PRIO60 = 64;
	localparam signed [31:0] RV_PLIC_PRIO61 = 65;
	localparam signed [31:0] RV_PLIC_PRIO62 = 66;
	localparam signed [31:0] RV_PLIC_IE00 = 67;
	localparam signed [31:0] RV_PLIC_IE01 = 68;
	localparam signed [31:0] RV_PLIC_THRESHOLD0 = 69;
	localparam signed [31:0] RV_PLIC_CC0 = 70;
	localparam signed [31:0] RV_PLIC_MSIP0 = 71;
	localparam [287:0] RV_PLIC_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam signed [31:0] SRCW = 6;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	input [NumSrc - 1:0] intr_src_i;
	output [NumTarget - 1:0] irq_o;
	output [((2 - NumTarget) * SRCW) + (((NumTarget - 1) * SRCW) - 1):(NumTarget - 1) * SRCW] irq_id_o;
	output wire [NumTarget - 1:0] msip_o;
	wire [262:0] reg2hw;
	wire [131:0] hw2reg;
	localparam signed [31:0] MAX_PRIO = 3;
	localparam signed [31:0] PRIOW = 2;
	wire [NumSrc - 1:0] le;
	wire [NumSrc - 1:0] ip;
	wire [NumSrc - 1:0] ie [0:NumTarget - 1];
	wire [NumTarget - 1:0] claim_re;
	wire [SRCW - 1:0] claim_id [0:NumTarget - 1];
	reg [NumSrc - 1:0] claim;
	wire [NumTarget - 1:0] complete_we;
	wire [SRCW - 1:0] complete_id [0:NumTarget - 1];
	reg [NumSrc - 1:0] complete;
	wire [((2 - NumTarget) * SRCW) + (((NumTarget - 1) * SRCW) - 1):(NumTarget - 1) * SRCW] cc_id;
	wire [(NumSrc * PRIOW) - 1:0] prio;
	wire [PRIOW - 1:0] threshold [0:NumTarget - 1];
	assign cc_id = irq_id_o;
	always @(*) begin
		claim = {NumSrc {1'sb0}};
		begin : sv2v_autoblock_151
			reg signed [31:0] i;
			for (i = 0; i < NumTarget; i = i + 1)
				if (claim_re[i])
					claim[claim_id[i] - 1] = 1'b1;
		end
	end
	always @(*) begin
		complete = {NumSrc {1'sb0}};
		begin : sv2v_autoblock_152
			reg signed [31:0] i;
			for (i = 0; i < NumTarget; i = i + 1)
				if (complete_we[i])
					complete[complete_id[i] - 1] = 1'b1;
		end
	end
	assign prio[(NumSrc - 1) * PRIOW+:PRIOW] = reg2hw[199-:2];
	assign prio[(NumSrc - 2) * PRIOW+:PRIOW] = reg2hw[197-:2];
	assign prio[(NumSrc - 3) * PRIOW+:PRIOW] = reg2hw[195-:2];
	assign prio[(NumSrc - 4) * PRIOW+:PRIOW] = reg2hw[193-:2];
	assign prio[(NumSrc - 5) * PRIOW+:PRIOW] = reg2hw[191-:2];
	assign prio[(NumSrc - 6) * PRIOW+:PRIOW] = reg2hw[189-:2];
	assign prio[(NumSrc - 7) * PRIOW+:PRIOW] = reg2hw[187-:2];
	assign prio[(NumSrc - 8) * PRIOW+:PRIOW] = reg2hw[185-:2];
	assign prio[(NumSrc - 9) * PRIOW+:PRIOW] = reg2hw[183-:2];
	assign prio[(NumSrc - 10) * PRIOW+:PRIOW] = reg2hw[181-:2];
	assign prio[(NumSrc - 11) * PRIOW+:PRIOW] = reg2hw[179-:2];
	assign prio[(NumSrc - 12) * PRIOW+:PRIOW] = reg2hw[177-:2];
	assign prio[(NumSrc - 13) * PRIOW+:PRIOW] = reg2hw[175-:2];
	assign prio[(NumSrc - 14) * PRIOW+:PRIOW] = reg2hw[173-:2];
	assign prio[(NumSrc - 15) * PRIOW+:PRIOW] = reg2hw[171-:2];
	assign prio[(NumSrc - 16) * PRIOW+:PRIOW] = reg2hw[169-:2];
	assign prio[(NumSrc - 17) * PRIOW+:PRIOW] = reg2hw[167-:2];
	assign prio[(NumSrc - 18) * PRIOW+:PRIOW] = reg2hw[165-:2];
	assign prio[(NumSrc - 19) * PRIOW+:PRIOW] = reg2hw[163-:2];
	assign prio[(NumSrc - 20) * PRIOW+:PRIOW] = reg2hw[161-:2];
	assign prio[(NumSrc - 21) * PRIOW+:PRIOW] = reg2hw[159-:2];
	assign prio[(NumSrc - 22) * PRIOW+:PRIOW] = reg2hw[157-:2];
	assign prio[(NumSrc - 23) * PRIOW+:PRIOW] = reg2hw[155-:2];
	assign prio[(NumSrc - 24) * PRIOW+:PRIOW] = reg2hw[153-:2];
	assign prio[(NumSrc - 25) * PRIOW+:PRIOW] = reg2hw[151-:2];
	assign prio[(NumSrc - 26) * PRIOW+:PRIOW] = reg2hw[149-:2];
	assign prio[(NumSrc - 27) * PRIOW+:PRIOW] = reg2hw[147-:2];
	assign prio[(NumSrc - 28) * PRIOW+:PRIOW] = reg2hw[145-:2];
	assign prio[(NumSrc - 29) * PRIOW+:PRIOW] = reg2hw[143-:2];
	assign prio[(NumSrc - 30) * PRIOW+:PRIOW] = reg2hw[141-:2];
	assign prio[(NumSrc - 31) * PRIOW+:PRIOW] = reg2hw[139-:2];
	assign prio[(NumSrc - 32) * PRIOW+:PRIOW] = reg2hw[137-:2];
	assign prio[(NumSrc - 33) * PRIOW+:PRIOW] = reg2hw[135-:2];
	assign prio[(NumSrc - 34) * PRIOW+:PRIOW] = reg2hw[133-:2];
	assign prio[(NumSrc - 35) * PRIOW+:PRIOW] = reg2hw[131-:2];
	assign prio[(NumSrc - 36) * PRIOW+:PRIOW] = reg2hw[129-:2];
	assign prio[(NumSrc - 37) * PRIOW+:PRIOW] = reg2hw[127-:2];
	assign prio[(NumSrc - 38) * PRIOW+:PRIOW] = reg2hw[125-:2];
	assign prio[(NumSrc - 39) * PRIOW+:PRIOW] = reg2hw[123-:2];
	assign prio[(NumSrc - 40) * PRIOW+:PRIOW] = reg2hw[121-:2];
	assign prio[(NumSrc - 41) * PRIOW+:PRIOW] = reg2hw[119-:2];
	assign prio[(NumSrc - 42) * PRIOW+:PRIOW] = reg2hw[117-:2];
	assign prio[(NumSrc - 43) * PRIOW+:PRIOW] = reg2hw[115-:2];
	assign prio[(NumSrc - 44) * PRIOW+:PRIOW] = reg2hw[113-:2];
	assign prio[(NumSrc - 45) * PRIOW+:PRIOW] = reg2hw[111-:2];
	assign prio[(NumSrc - 46) * PRIOW+:PRIOW] = reg2hw[109-:2];
	assign prio[(NumSrc - 47) * PRIOW+:PRIOW] = reg2hw[107-:2];
	assign prio[(NumSrc - 48) * PRIOW+:PRIOW] = reg2hw[105-:2];
	assign prio[(NumSrc - 49) * PRIOW+:PRIOW] = reg2hw[103-:2];
	assign prio[(NumSrc - 50) * PRIOW+:PRIOW] = reg2hw[101-:2];
	assign prio[(NumSrc - 51) * PRIOW+:PRIOW] = reg2hw[99-:2];
	assign prio[(NumSrc - 52) * PRIOW+:PRIOW] = reg2hw[97-:2];
	assign prio[(NumSrc - 53) * PRIOW+:PRIOW] = reg2hw[95-:2];
	assign prio[(NumSrc - 54) * PRIOW+:PRIOW] = reg2hw[93-:2];
	assign prio[(NumSrc - 55) * PRIOW+:PRIOW] = reg2hw[91-:2];
	assign prio[(NumSrc - 56) * PRIOW+:PRIOW] = reg2hw[89-:2];
	assign prio[(NumSrc - 57) * PRIOW+:PRIOW] = reg2hw[87-:2];
	assign prio[(NumSrc - 58) * PRIOW+:PRIOW] = reg2hw[85-:2];
	assign prio[(NumSrc - 59) * PRIOW+:PRIOW] = reg2hw[83-:2];
	assign prio[(NumSrc - 60) * PRIOW+:PRIOW] = reg2hw[81-:2];
	assign prio[(NumSrc - 61) * PRIOW+:PRIOW] = reg2hw[79-:2];
	assign prio[(NumSrc - 62) * PRIOW+:PRIOW] = reg2hw[77-:2];
	assign prio[(NumSrc - 63) * PRIOW+:PRIOW] = reg2hw[75-:2];
	generate
		genvar s;
		for (s = 0; s < 63; s = s + 1) begin : gen_ie0
			assign ie[0][s] = reg2hw[11 + s];
		end
	endgenerate
	assign threshold[0] = reg2hw[10-:2];
	assign claim_re[0] = reg2hw[1];
	assign claim_id[0] = irq_id_o[0+:SRCW];
	assign complete_we[0] = reg2hw[2];
	assign complete_id[0] = reg2hw[8-:6];
	assign hw2reg[5-:6] = cc_id[0+:SRCW];
	assign msip_o[0] = reg2hw[-0];
	generate
		for (s = 0; s < 63; s = s + 1) begin : gen_ip
			assign hw2reg[6 + (s * 2)] = 1'b1;
			assign hw2reg[6 + ((s * 2) + 1)] = ip[s];
		end
	endgenerate
	generate
		for (s = 0; s < 63; s = s + 1) begin : gen_le
			assign le[s] = reg2hw[200 + s];
		end
	endgenerate
	rv_plic_gateway #(.N_SOURCE(NumSrc)) u_gateway(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.src(intr_src_i),
		.le(le),
		.claim(claim),
		.complete(complete),
		.ip(ip)
	);
	generate
		genvar i;
		for (i = 0; i < NumTarget; i = i + 1) begin : gen_target
			rv_plic_target #(
				.N_SOURCE(NumSrc),
				.MAX_PRIO(MAX_PRIO)
			) u_target(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.ip(ip),
				.ie(ie[i]),
				.prio(prio),
				.threshold(threshold[i]),
				.irq(irq_o[i]),
				.irq_id(irq_id_o[i * SRCW+:SRCW])
			);
		end
	endgenerate
	rv_plic_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	genvar k;
endmodule
module pinmux_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [383:0] reg2hw;
	input devmode_i;
	localparam signed [31:0] NPeriphIn = 32;
	localparam signed [31:0] NPeriphOut = 32;
	localparam signed [31:0] NMioPads = 32;
	localparam PINMUX_REGEN_OFFSET = 6'h00;
	localparam PINMUX_PERIPH_INSEL0_OFFSET = 6'h04;
	localparam PINMUX_PERIPH_INSEL1_OFFSET = 6'h08;
	localparam PINMUX_PERIPH_INSEL2_OFFSET = 6'h0c;
	localparam PINMUX_PERIPH_INSEL3_OFFSET = 6'h10;
	localparam PINMUX_PERIPH_INSEL4_OFFSET = 6'h14;
	localparam PINMUX_PERIPH_INSEL5_OFFSET = 6'h18;
	localparam PINMUX_PERIPH_INSEL6_OFFSET = 6'h1c;
	localparam PINMUX_MIO_OUTSEL0_OFFSET = 6'h20;
	localparam PINMUX_MIO_OUTSEL1_OFFSET = 6'h24;
	localparam PINMUX_MIO_OUTSEL2_OFFSET = 6'h28;
	localparam PINMUX_MIO_OUTSEL3_OFFSET = 6'h2c;
	localparam PINMUX_MIO_OUTSEL4_OFFSET = 6'h30;
	localparam PINMUX_MIO_OUTSEL5_OFFSET = 6'h34;
	localparam PINMUX_MIO_OUTSEL6_OFFSET = 6'h38;
	localparam signed [31:0] PINMUX_REGEN = 0;
	localparam signed [31:0] PINMUX_PERIPH_INSEL0 = 1;
	localparam signed [31:0] PINMUX_PERIPH_INSEL1 = 2;
	localparam signed [31:0] PINMUX_PERIPH_INSEL2 = 3;
	localparam signed [31:0] PINMUX_PERIPH_INSEL3 = 4;
	localparam signed [31:0] PINMUX_PERIPH_INSEL4 = 5;
	localparam signed [31:0] PINMUX_PERIPH_INSEL5 = 6;
	localparam signed [31:0] PINMUX_PERIPH_INSEL6 = 7;
	localparam signed [31:0] PINMUX_MIO_OUTSEL0 = 8;
	localparam signed [31:0] PINMUX_MIO_OUTSEL1 = 9;
	localparam signed [31:0] PINMUX_MIO_OUTSEL2 = 10;
	localparam signed [31:0] PINMUX_MIO_OUTSEL3 = 11;
	localparam signed [31:0] PINMUX_MIO_OUTSEL4 = 12;
	localparam signed [31:0] PINMUX_MIO_OUTSEL5 = 13;
	localparam signed [31:0] PINMUX_MIO_OUTSEL6 = 14;
	localparam [59:0] PINMUX_PERMIT = {4'b0001, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0011};
	localparam AW = 6;
	localparam DW = 32;
	localparam DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire regen_qs;
	wire regen_wd;
	wire regen_we;
	wire [5:0] periph_insel0_in0_qs;
	wire [5:0] periph_insel0_in0_wd;
	wire periph_insel0_in0_we;
	wire [5:0] periph_insel0_in1_qs;
	wire [5:0] periph_insel0_in1_wd;
	wire periph_insel0_in1_we;
	wire [5:0] periph_insel0_in2_qs;
	wire [5:0] periph_insel0_in2_wd;
	wire periph_insel0_in2_we;
	wire [5:0] periph_insel0_in3_qs;
	wire [5:0] periph_insel0_in3_wd;
	wire periph_insel0_in3_we;
	wire [5:0] periph_insel0_in4_qs;
	wire [5:0] periph_insel0_in4_wd;
	wire periph_insel0_in4_we;
	wire [5:0] periph_insel1_in5_qs;
	wire [5:0] periph_insel1_in5_wd;
	wire periph_insel1_in5_we;
	wire [5:0] periph_insel1_in6_qs;
	wire [5:0] periph_insel1_in6_wd;
	wire periph_insel1_in6_we;
	wire [5:0] periph_insel1_in7_qs;
	wire [5:0] periph_insel1_in7_wd;
	wire periph_insel1_in7_we;
	wire [5:0] periph_insel1_in8_qs;
	wire [5:0] periph_insel1_in8_wd;
	wire periph_insel1_in8_we;
	wire [5:0] periph_insel1_in9_qs;
	wire [5:0] periph_insel1_in9_wd;
	wire periph_insel1_in9_we;
	wire [5:0] periph_insel2_in10_qs;
	wire [5:0] periph_insel2_in10_wd;
	wire periph_insel2_in10_we;
	wire [5:0] periph_insel2_in11_qs;
	wire [5:0] periph_insel2_in11_wd;
	wire periph_insel2_in11_we;
	wire [5:0] periph_insel2_in12_qs;
	wire [5:0] periph_insel2_in12_wd;
	wire periph_insel2_in12_we;
	wire [5:0] periph_insel2_in13_qs;
	wire [5:0] periph_insel2_in13_wd;
	wire periph_insel2_in13_we;
	wire [5:0] periph_insel2_in14_qs;
	wire [5:0] periph_insel2_in14_wd;
	wire periph_insel2_in14_we;
	wire [5:0] periph_insel3_in15_qs;
	wire [5:0] periph_insel3_in15_wd;
	wire periph_insel3_in15_we;
	wire [5:0] periph_insel3_in16_qs;
	wire [5:0] periph_insel3_in16_wd;
	wire periph_insel3_in16_we;
	wire [5:0] periph_insel3_in17_qs;
	wire [5:0] periph_insel3_in17_wd;
	wire periph_insel3_in17_we;
	wire [5:0] periph_insel3_in18_qs;
	wire [5:0] periph_insel3_in18_wd;
	wire periph_insel3_in18_we;
	wire [5:0] periph_insel3_in19_qs;
	wire [5:0] periph_insel3_in19_wd;
	wire periph_insel3_in19_we;
	wire [5:0] periph_insel4_in20_qs;
	wire [5:0] periph_insel4_in20_wd;
	wire periph_insel4_in20_we;
	wire [5:0] periph_insel4_in21_qs;
	wire [5:0] periph_insel4_in21_wd;
	wire periph_insel4_in21_we;
	wire [5:0] periph_insel4_in22_qs;
	wire [5:0] periph_insel4_in22_wd;
	wire periph_insel4_in22_we;
	wire [5:0] periph_insel4_in23_qs;
	wire [5:0] periph_insel4_in23_wd;
	wire periph_insel4_in23_we;
	wire [5:0] periph_insel4_in24_qs;
	wire [5:0] periph_insel4_in24_wd;
	wire periph_insel4_in24_we;
	wire [5:0] periph_insel5_in25_qs;
	wire [5:0] periph_insel5_in25_wd;
	wire periph_insel5_in25_we;
	wire [5:0] periph_insel5_in26_qs;
	wire [5:0] periph_insel5_in26_wd;
	wire periph_insel5_in26_we;
	wire [5:0] periph_insel5_in27_qs;
	wire [5:0] periph_insel5_in27_wd;
	wire periph_insel5_in27_we;
	wire [5:0] periph_insel5_in28_qs;
	wire [5:0] periph_insel5_in28_wd;
	wire periph_insel5_in28_we;
	wire [5:0] periph_insel5_in29_qs;
	wire [5:0] periph_insel5_in29_wd;
	wire periph_insel5_in29_we;
	wire [5:0] periph_insel6_in30_qs;
	wire [5:0] periph_insel6_in30_wd;
	wire periph_insel6_in30_we;
	wire [5:0] periph_insel6_in31_qs;
	wire [5:0] periph_insel6_in31_wd;
	wire periph_insel6_in31_we;
	wire [5:0] mio_outsel0_out0_qs;
	wire [5:0] mio_outsel0_out0_wd;
	wire mio_outsel0_out0_we;
	wire [5:0] mio_outsel0_out1_qs;
	wire [5:0] mio_outsel0_out1_wd;
	wire mio_outsel0_out1_we;
	wire [5:0] mio_outsel0_out2_qs;
	wire [5:0] mio_outsel0_out2_wd;
	wire mio_outsel0_out2_we;
	wire [5:0] mio_outsel0_out3_qs;
	wire [5:0] mio_outsel0_out3_wd;
	wire mio_outsel0_out3_we;
	wire [5:0] mio_outsel0_out4_qs;
	wire [5:0] mio_outsel0_out4_wd;
	wire mio_outsel0_out4_we;
	wire [5:0] mio_outsel1_out5_qs;
	wire [5:0] mio_outsel1_out5_wd;
	wire mio_outsel1_out5_we;
	wire [5:0] mio_outsel1_out6_qs;
	wire [5:0] mio_outsel1_out6_wd;
	wire mio_outsel1_out6_we;
	wire [5:0] mio_outsel1_out7_qs;
	wire [5:0] mio_outsel1_out7_wd;
	wire mio_outsel1_out7_we;
	wire [5:0] mio_outsel1_out8_qs;
	wire [5:0] mio_outsel1_out8_wd;
	wire mio_outsel1_out8_we;
	wire [5:0] mio_outsel1_out9_qs;
	wire [5:0] mio_outsel1_out9_wd;
	wire mio_outsel1_out9_we;
	wire [5:0] mio_outsel2_out10_qs;
	wire [5:0] mio_outsel2_out10_wd;
	wire mio_outsel2_out10_we;
	wire [5:0] mio_outsel2_out11_qs;
	wire [5:0] mio_outsel2_out11_wd;
	wire mio_outsel2_out11_we;
	wire [5:0] mio_outsel2_out12_qs;
	wire [5:0] mio_outsel2_out12_wd;
	wire mio_outsel2_out12_we;
	wire [5:0] mio_outsel2_out13_qs;
	wire [5:0] mio_outsel2_out13_wd;
	wire mio_outsel2_out13_we;
	wire [5:0] mio_outsel2_out14_qs;
	wire [5:0] mio_outsel2_out14_wd;
	wire mio_outsel2_out14_we;
	wire [5:0] mio_outsel3_out15_qs;
	wire [5:0] mio_outsel3_out15_wd;
	wire mio_outsel3_out15_we;
	wire [5:0] mio_outsel3_out16_qs;
	wire [5:0] mio_outsel3_out16_wd;
	wire mio_outsel3_out16_we;
	wire [5:0] mio_outsel3_out17_qs;
	wire [5:0] mio_outsel3_out17_wd;
	wire mio_outsel3_out17_we;
	wire [5:0] mio_outsel3_out18_qs;
	wire [5:0] mio_outsel3_out18_wd;
	wire mio_outsel3_out18_we;
	wire [5:0] mio_outsel3_out19_qs;
	wire [5:0] mio_outsel3_out19_wd;
	wire mio_outsel3_out19_we;
	wire [5:0] mio_outsel4_out20_qs;
	wire [5:0] mio_outsel4_out20_wd;
	wire mio_outsel4_out20_we;
	wire [5:0] mio_outsel4_out21_qs;
	wire [5:0] mio_outsel4_out21_wd;
	wire mio_outsel4_out21_we;
	wire [5:0] mio_outsel4_out22_qs;
	wire [5:0] mio_outsel4_out22_wd;
	wire mio_outsel4_out22_we;
	wire [5:0] mio_outsel4_out23_qs;
	wire [5:0] mio_outsel4_out23_wd;
	wire mio_outsel4_out23_we;
	wire [5:0] mio_outsel4_out24_qs;
	wire [5:0] mio_outsel4_out24_wd;
	wire mio_outsel4_out24_we;
	wire [5:0] mio_outsel5_out25_qs;
	wire [5:0] mio_outsel5_out25_wd;
	wire mio_outsel5_out25_we;
	wire [5:0] mio_outsel5_out26_qs;
	wire [5:0] mio_outsel5_out26_wd;
	wire mio_outsel5_out26_we;
	wire [5:0] mio_outsel5_out27_qs;
	wire [5:0] mio_outsel5_out27_wd;
	wire mio_outsel5_out27_we;
	wire [5:0] mio_outsel5_out28_qs;
	wire [5:0] mio_outsel5_out28_wd;
	wire mio_outsel5_out28_we;
	wire [5:0] mio_outsel5_out29_qs;
	wire [5:0] mio_outsel5_out29_wd;
	wire mio_outsel5_out29_we;
	wire [5:0] mio_outsel6_out30_qs;
	wire [5:0] mio_outsel6_out30_wd;
	wire mio_outsel6_out30_we;
	wire [5:0] mio_outsel6_out31_qs;
	wire [5:0] mio_outsel6_out31_wd;
	wire mio_outsel6_out31_we;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W0C"),
		.RESVAL(1'h1)
	) u_regen(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(regen_we),
		.wd(regen_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(),
		.qs(regen_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel0_in0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel0_in0_we & regen_qs),
		.wd(periph_insel0_in0_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[197-:6]),
		.qs(periph_insel0_in0_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel0_in1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel0_in1_we & regen_qs),
		.wd(periph_insel0_in1_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[203-:6]),
		.qs(periph_insel0_in1_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel0_in2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel0_in2_we & regen_qs),
		.wd(periph_insel0_in2_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[209-:6]),
		.qs(periph_insel0_in2_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel0_in3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel0_in3_we & regen_qs),
		.wd(periph_insel0_in3_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[215-:6]),
		.qs(periph_insel0_in3_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel0_in4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel0_in4_we & regen_qs),
		.wd(periph_insel0_in4_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[221-:6]),
		.qs(periph_insel0_in4_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel1_in5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel1_in5_we & regen_qs),
		.wd(periph_insel1_in5_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[227-:6]),
		.qs(periph_insel1_in5_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel1_in6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel1_in6_we & regen_qs),
		.wd(periph_insel1_in6_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[233-:6]),
		.qs(periph_insel1_in6_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel1_in7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel1_in7_we & regen_qs),
		.wd(periph_insel1_in7_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[239-:6]),
		.qs(periph_insel1_in7_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel1_in8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel1_in8_we & regen_qs),
		.wd(periph_insel1_in8_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[245-:6]),
		.qs(periph_insel1_in8_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel1_in9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel1_in9_we & regen_qs),
		.wd(periph_insel1_in9_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[251-:6]),
		.qs(periph_insel1_in9_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel2_in10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel2_in10_we & regen_qs),
		.wd(periph_insel2_in10_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[257-:6]),
		.qs(periph_insel2_in10_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel2_in11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel2_in11_we & regen_qs),
		.wd(periph_insel2_in11_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[263-:6]),
		.qs(periph_insel2_in11_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel2_in12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel2_in12_we & regen_qs),
		.wd(periph_insel2_in12_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[269-:6]),
		.qs(periph_insel2_in12_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel2_in13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel2_in13_we & regen_qs),
		.wd(periph_insel2_in13_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[275-:6]),
		.qs(periph_insel2_in13_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel2_in14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel2_in14_we & regen_qs),
		.wd(periph_insel2_in14_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[281-:6]),
		.qs(periph_insel2_in14_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel3_in15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel3_in15_we & regen_qs),
		.wd(periph_insel3_in15_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[287-:6]),
		.qs(periph_insel3_in15_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel3_in16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel3_in16_we & regen_qs),
		.wd(periph_insel3_in16_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[293-:6]),
		.qs(periph_insel3_in16_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel3_in17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel3_in17_we & regen_qs),
		.wd(periph_insel3_in17_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[299-:6]),
		.qs(periph_insel3_in17_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel3_in18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel3_in18_we & regen_qs),
		.wd(periph_insel3_in18_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[305-:6]),
		.qs(periph_insel3_in18_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel3_in19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel3_in19_we & regen_qs),
		.wd(periph_insel3_in19_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[311-:6]),
		.qs(periph_insel3_in19_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel4_in20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel4_in20_we & regen_qs),
		.wd(periph_insel4_in20_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[317-:6]),
		.qs(periph_insel4_in20_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel4_in21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel4_in21_we & regen_qs),
		.wd(periph_insel4_in21_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[323-:6]),
		.qs(periph_insel4_in21_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel4_in22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel4_in22_we & regen_qs),
		.wd(periph_insel4_in22_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[329-:6]),
		.qs(periph_insel4_in22_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel4_in23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel4_in23_we & regen_qs),
		.wd(periph_insel4_in23_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[335-:6]),
		.qs(periph_insel4_in23_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel4_in24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel4_in24_we & regen_qs),
		.wd(periph_insel4_in24_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[341-:6]),
		.qs(periph_insel4_in24_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel5_in25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel5_in25_we & regen_qs),
		.wd(periph_insel5_in25_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[347-:6]),
		.qs(periph_insel5_in25_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel5_in26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel5_in26_we & regen_qs),
		.wd(periph_insel5_in26_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[353-:6]),
		.qs(periph_insel5_in26_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel5_in27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel5_in27_we & regen_qs),
		.wd(periph_insel5_in27_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[359-:6]),
		.qs(periph_insel5_in27_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel5_in28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel5_in28_we & regen_qs),
		.wd(periph_insel5_in28_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[365-:6]),
		.qs(periph_insel5_in28_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel5_in29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel5_in29_we & regen_qs),
		.wd(periph_insel5_in29_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[371-:6]),
		.qs(periph_insel5_in29_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel6_in30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel6_in30_we & regen_qs),
		.wd(periph_insel6_in30_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[377-:6]),
		.qs(periph_insel6_in30_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h00)
	) u_periph_insel6_in31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(periph_insel6_in31_we & regen_qs),
		.wd(periph_insel6_in31_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[383-:6]),
		.qs(periph_insel6_in31_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel0_out0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel0_out0_we & regen_qs),
		.wd(mio_outsel0_out0_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[5-:6]),
		.qs(mio_outsel0_out0_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel0_out1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel0_out1_we & regen_qs),
		.wd(mio_outsel0_out1_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[11-:6]),
		.qs(mio_outsel0_out1_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel0_out2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel0_out2_we & regen_qs),
		.wd(mio_outsel0_out2_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[17-:6]),
		.qs(mio_outsel0_out2_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel0_out3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel0_out3_we & regen_qs),
		.wd(mio_outsel0_out3_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[23-:6]),
		.qs(mio_outsel0_out3_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel0_out4(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel0_out4_we & regen_qs),
		.wd(mio_outsel0_out4_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[29-:6]),
		.qs(mio_outsel0_out4_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel1_out5(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel1_out5_we & regen_qs),
		.wd(mio_outsel1_out5_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[35-:6]),
		.qs(mio_outsel1_out5_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel1_out6(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel1_out6_we & regen_qs),
		.wd(mio_outsel1_out6_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[41-:6]),
		.qs(mio_outsel1_out6_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel1_out7(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel1_out7_we & regen_qs),
		.wd(mio_outsel1_out7_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[47-:6]),
		.qs(mio_outsel1_out7_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel1_out8(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel1_out8_we & regen_qs),
		.wd(mio_outsel1_out8_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[53-:6]),
		.qs(mio_outsel1_out8_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel1_out9(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel1_out9_we & regen_qs),
		.wd(mio_outsel1_out9_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[59-:6]),
		.qs(mio_outsel1_out9_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel2_out10(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel2_out10_we & regen_qs),
		.wd(mio_outsel2_out10_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[65-:6]),
		.qs(mio_outsel2_out10_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel2_out11(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel2_out11_we & regen_qs),
		.wd(mio_outsel2_out11_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[71-:6]),
		.qs(mio_outsel2_out11_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel2_out12(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel2_out12_we & regen_qs),
		.wd(mio_outsel2_out12_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[77-:6]),
		.qs(mio_outsel2_out12_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel2_out13(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel2_out13_we & regen_qs),
		.wd(mio_outsel2_out13_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[83-:6]),
		.qs(mio_outsel2_out13_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel2_out14(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel2_out14_we & regen_qs),
		.wd(mio_outsel2_out14_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[89-:6]),
		.qs(mio_outsel2_out14_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel3_out15(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel3_out15_we & regen_qs),
		.wd(mio_outsel3_out15_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[95-:6]),
		.qs(mio_outsel3_out15_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel3_out16(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel3_out16_we & regen_qs),
		.wd(mio_outsel3_out16_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[101-:6]),
		.qs(mio_outsel3_out16_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel3_out17(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel3_out17_we & regen_qs),
		.wd(mio_outsel3_out17_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[107-:6]),
		.qs(mio_outsel3_out17_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel3_out18(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel3_out18_we & regen_qs),
		.wd(mio_outsel3_out18_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[113-:6]),
		.qs(mio_outsel3_out18_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel3_out19(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel3_out19_we & regen_qs),
		.wd(mio_outsel3_out19_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[119-:6]),
		.qs(mio_outsel3_out19_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel4_out20(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel4_out20_we & regen_qs),
		.wd(mio_outsel4_out20_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[125-:6]),
		.qs(mio_outsel4_out20_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel4_out21(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel4_out21_we & regen_qs),
		.wd(mio_outsel4_out21_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[131-:6]),
		.qs(mio_outsel4_out21_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel4_out22(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel4_out22_we & regen_qs),
		.wd(mio_outsel4_out22_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[137-:6]),
		.qs(mio_outsel4_out22_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel4_out23(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel4_out23_we & regen_qs),
		.wd(mio_outsel4_out23_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[143-:6]),
		.qs(mio_outsel4_out23_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel4_out24(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel4_out24_we & regen_qs),
		.wd(mio_outsel4_out24_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[149-:6]),
		.qs(mio_outsel4_out24_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel5_out25(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel5_out25_we & regen_qs),
		.wd(mio_outsel5_out25_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[155-:6]),
		.qs(mio_outsel5_out25_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel5_out26(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel5_out26_we & regen_qs),
		.wd(mio_outsel5_out26_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[161-:6]),
		.qs(mio_outsel5_out26_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel5_out27(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel5_out27_we & regen_qs),
		.wd(mio_outsel5_out27_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[167-:6]),
		.qs(mio_outsel5_out27_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel5_out28(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel5_out28_we & regen_qs),
		.wd(mio_outsel5_out28_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[173-:6]),
		.qs(mio_outsel5_out28_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel5_out29(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel5_out29_we & regen_qs),
		.wd(mio_outsel5_out29_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[179-:6]),
		.qs(mio_outsel5_out29_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel6_out30(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel6_out30_we & regen_qs),
		.wd(mio_outsel6_out30_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[185-:6]),
		.qs(mio_outsel6_out30_qs)
	);
	prim_subreg #(
		.DW(6),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(6'h02)
	) u_mio_outsel6_out31(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(mio_outsel6_out31_we & regen_qs),
		.wd(mio_outsel6_out31_wd),
		.de(1'b0),
		.d({6 {1'sb0}}),
		.qe(),
		.q(reg2hw[191-:6]),
		.qs(mio_outsel6_out31_qs)
	);
	reg [14:0] addr_hit;
	always @(*) begin
		addr_hit = {15 {1'sb0}};
		addr_hit[0] = reg_addr == PINMUX_REGEN_OFFSET;
		addr_hit[1] = reg_addr == PINMUX_PERIPH_INSEL0_OFFSET;
		addr_hit[2] = reg_addr == PINMUX_PERIPH_INSEL1_OFFSET;
		addr_hit[3] = reg_addr == PINMUX_PERIPH_INSEL2_OFFSET;
		addr_hit[4] = reg_addr == PINMUX_PERIPH_INSEL3_OFFSET;
		addr_hit[5] = reg_addr == PINMUX_PERIPH_INSEL4_OFFSET;
		addr_hit[6] = reg_addr == PINMUX_PERIPH_INSEL5_OFFSET;
		addr_hit[7] = reg_addr == PINMUX_PERIPH_INSEL6_OFFSET;
		addr_hit[8] = reg_addr == PINMUX_MIO_OUTSEL0_OFFSET;
		addr_hit[9] = reg_addr == PINMUX_MIO_OUTSEL1_OFFSET;
		addr_hit[10] = reg_addr == PINMUX_MIO_OUTSEL2_OFFSET;
		addr_hit[11] = reg_addr == PINMUX_MIO_OUTSEL3_OFFSET;
		addr_hit[12] = reg_addr == PINMUX_MIO_OUTSEL4_OFFSET;
		addr_hit[13] = reg_addr == PINMUX_MIO_OUTSEL5_OFFSET;
		addr_hit[14] = reg_addr == PINMUX_MIO_OUTSEL6_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (PINMUX_PERMIT[56+:4] != (PINMUX_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (PINMUX_PERMIT[52+:4] != (PINMUX_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (PINMUX_PERMIT[48+:4] != (PINMUX_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (PINMUX_PERMIT[44+:4] != (PINMUX_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (PINMUX_PERMIT[40+:4] != (PINMUX_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (PINMUX_PERMIT[36+:4] != (PINMUX_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (PINMUX_PERMIT[32+:4] != (PINMUX_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (PINMUX_PERMIT[28+:4] != (PINMUX_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (PINMUX_PERMIT[24+:4] != (PINMUX_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (PINMUX_PERMIT[20+:4] != (PINMUX_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (PINMUX_PERMIT[16+:4] != (PINMUX_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (PINMUX_PERMIT[12+:4] != (PINMUX_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (PINMUX_PERMIT[8+:4] != (PINMUX_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (PINMUX_PERMIT[4+:4] != (PINMUX_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (PINMUX_PERMIT[0+:4] != (PINMUX_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign regen_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign regen_wd = reg_wdata[0];
	assign periph_insel0_in0_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign periph_insel0_in0_wd = reg_wdata[5:0];
	assign periph_insel0_in1_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign periph_insel0_in1_wd = reg_wdata[11:6];
	assign periph_insel0_in2_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign periph_insel0_in2_wd = reg_wdata[17:12];
	assign periph_insel0_in3_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign periph_insel0_in3_wd = reg_wdata[23:18];
	assign periph_insel0_in4_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign periph_insel0_in4_wd = reg_wdata[29:24];
	assign periph_insel1_in5_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign periph_insel1_in5_wd = reg_wdata[5:0];
	assign periph_insel1_in6_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign periph_insel1_in6_wd = reg_wdata[11:6];
	assign periph_insel1_in7_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign periph_insel1_in7_wd = reg_wdata[17:12];
	assign periph_insel1_in8_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign periph_insel1_in8_wd = reg_wdata[23:18];
	assign periph_insel1_in9_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign periph_insel1_in9_wd = reg_wdata[29:24];
	assign periph_insel2_in10_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign periph_insel2_in10_wd = reg_wdata[5:0];
	assign periph_insel2_in11_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign periph_insel2_in11_wd = reg_wdata[11:6];
	assign periph_insel2_in12_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign periph_insel2_in12_wd = reg_wdata[17:12];
	assign periph_insel2_in13_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign periph_insel2_in13_wd = reg_wdata[23:18];
	assign periph_insel2_in14_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign periph_insel2_in14_wd = reg_wdata[29:24];
	assign periph_insel3_in15_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign periph_insel3_in15_wd = reg_wdata[5:0];
	assign periph_insel3_in16_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign periph_insel3_in16_wd = reg_wdata[11:6];
	assign periph_insel3_in17_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign periph_insel3_in17_wd = reg_wdata[17:12];
	assign periph_insel3_in18_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign periph_insel3_in18_wd = reg_wdata[23:18];
	assign periph_insel3_in19_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign periph_insel3_in19_wd = reg_wdata[29:24];
	assign periph_insel4_in20_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign periph_insel4_in20_wd = reg_wdata[5:0];
	assign periph_insel4_in21_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign periph_insel4_in21_wd = reg_wdata[11:6];
	assign periph_insel4_in22_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign periph_insel4_in22_wd = reg_wdata[17:12];
	assign periph_insel4_in23_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign periph_insel4_in23_wd = reg_wdata[23:18];
	assign periph_insel4_in24_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign periph_insel4_in24_wd = reg_wdata[29:24];
	assign periph_insel5_in25_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign periph_insel5_in25_wd = reg_wdata[5:0];
	assign periph_insel5_in26_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign periph_insel5_in26_wd = reg_wdata[11:6];
	assign periph_insel5_in27_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign periph_insel5_in27_wd = reg_wdata[17:12];
	assign periph_insel5_in28_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign periph_insel5_in28_wd = reg_wdata[23:18];
	assign periph_insel5_in29_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign periph_insel5_in29_wd = reg_wdata[29:24];
	assign periph_insel6_in30_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign periph_insel6_in30_wd = reg_wdata[5:0];
	assign periph_insel6_in31_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign periph_insel6_in31_wd = reg_wdata[11:6];
	assign mio_outsel0_out0_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign mio_outsel0_out0_wd = reg_wdata[5:0];
	assign mio_outsel0_out1_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign mio_outsel0_out1_wd = reg_wdata[11:6];
	assign mio_outsel0_out2_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign mio_outsel0_out2_wd = reg_wdata[17:12];
	assign mio_outsel0_out3_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign mio_outsel0_out3_wd = reg_wdata[23:18];
	assign mio_outsel0_out4_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign mio_outsel0_out4_wd = reg_wdata[29:24];
	assign mio_outsel1_out5_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign mio_outsel1_out5_wd = reg_wdata[5:0];
	assign mio_outsel1_out6_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign mio_outsel1_out6_wd = reg_wdata[11:6];
	assign mio_outsel1_out7_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign mio_outsel1_out7_wd = reg_wdata[17:12];
	assign mio_outsel1_out8_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign mio_outsel1_out8_wd = reg_wdata[23:18];
	assign mio_outsel1_out9_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign mio_outsel1_out9_wd = reg_wdata[29:24];
	assign mio_outsel2_out10_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign mio_outsel2_out10_wd = reg_wdata[5:0];
	assign mio_outsel2_out11_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign mio_outsel2_out11_wd = reg_wdata[11:6];
	assign mio_outsel2_out12_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign mio_outsel2_out12_wd = reg_wdata[17:12];
	assign mio_outsel2_out13_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign mio_outsel2_out13_wd = reg_wdata[23:18];
	assign mio_outsel2_out14_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign mio_outsel2_out14_wd = reg_wdata[29:24];
	assign mio_outsel3_out15_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign mio_outsel3_out15_wd = reg_wdata[5:0];
	assign mio_outsel3_out16_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign mio_outsel3_out16_wd = reg_wdata[11:6];
	assign mio_outsel3_out17_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign mio_outsel3_out17_wd = reg_wdata[17:12];
	assign mio_outsel3_out18_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign mio_outsel3_out18_wd = reg_wdata[23:18];
	assign mio_outsel3_out19_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign mio_outsel3_out19_wd = reg_wdata[29:24];
	assign mio_outsel4_out20_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign mio_outsel4_out20_wd = reg_wdata[5:0];
	assign mio_outsel4_out21_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign mio_outsel4_out21_wd = reg_wdata[11:6];
	assign mio_outsel4_out22_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign mio_outsel4_out22_wd = reg_wdata[17:12];
	assign mio_outsel4_out23_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign mio_outsel4_out23_wd = reg_wdata[23:18];
	assign mio_outsel4_out24_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign mio_outsel4_out24_wd = reg_wdata[29:24];
	assign mio_outsel5_out25_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign mio_outsel5_out25_wd = reg_wdata[5:0];
	assign mio_outsel5_out26_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign mio_outsel5_out26_wd = reg_wdata[11:6];
	assign mio_outsel5_out27_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign mio_outsel5_out27_wd = reg_wdata[17:12];
	assign mio_outsel5_out28_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign mio_outsel5_out28_wd = reg_wdata[23:18];
	assign mio_outsel5_out29_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign mio_outsel5_out29_wd = reg_wdata[29:24];
	assign mio_outsel6_out30_we = (addr_hit[14] & reg_we) & ~wr_err;
	assign mio_outsel6_out30_wd = reg_wdata[5:0];
	assign mio_outsel6_out31_we = (addr_hit[14] & reg_we) & ~wr_err;
	assign mio_outsel6_out31_wd = reg_wdata[11:6];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: reg_rdata_next[0] = regen_qs;
			addr_hit[1]: begin
				reg_rdata_next[5:0] = periph_insel0_in0_qs;
				reg_rdata_next[11:6] = periph_insel0_in1_qs;
				reg_rdata_next[17:12] = periph_insel0_in2_qs;
				reg_rdata_next[23:18] = periph_insel0_in3_qs;
				reg_rdata_next[29:24] = periph_insel0_in4_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[5:0] = periph_insel1_in5_qs;
				reg_rdata_next[11:6] = periph_insel1_in6_qs;
				reg_rdata_next[17:12] = periph_insel1_in7_qs;
				reg_rdata_next[23:18] = periph_insel1_in8_qs;
				reg_rdata_next[29:24] = periph_insel1_in9_qs;
			end
			addr_hit[3]: begin
				reg_rdata_next[5:0] = periph_insel2_in10_qs;
				reg_rdata_next[11:6] = periph_insel2_in11_qs;
				reg_rdata_next[17:12] = periph_insel2_in12_qs;
				reg_rdata_next[23:18] = periph_insel2_in13_qs;
				reg_rdata_next[29:24] = periph_insel2_in14_qs;
			end
			addr_hit[4]: begin
				reg_rdata_next[5:0] = periph_insel3_in15_qs;
				reg_rdata_next[11:6] = periph_insel3_in16_qs;
				reg_rdata_next[17:12] = periph_insel3_in17_qs;
				reg_rdata_next[23:18] = periph_insel3_in18_qs;
				reg_rdata_next[29:24] = periph_insel3_in19_qs;
			end
			addr_hit[5]: begin
				reg_rdata_next[5:0] = periph_insel4_in20_qs;
				reg_rdata_next[11:6] = periph_insel4_in21_qs;
				reg_rdata_next[17:12] = periph_insel4_in22_qs;
				reg_rdata_next[23:18] = periph_insel4_in23_qs;
				reg_rdata_next[29:24] = periph_insel4_in24_qs;
			end
			addr_hit[6]: begin
				reg_rdata_next[5:0] = periph_insel5_in25_qs;
				reg_rdata_next[11:6] = periph_insel5_in26_qs;
				reg_rdata_next[17:12] = periph_insel5_in27_qs;
				reg_rdata_next[23:18] = periph_insel5_in28_qs;
				reg_rdata_next[29:24] = periph_insel5_in29_qs;
			end
			addr_hit[7]: begin
				reg_rdata_next[5:0] = periph_insel6_in30_qs;
				reg_rdata_next[11:6] = periph_insel6_in31_qs;
			end
			addr_hit[8]: begin
				reg_rdata_next[5:0] = mio_outsel0_out0_qs;
				reg_rdata_next[11:6] = mio_outsel0_out1_qs;
				reg_rdata_next[17:12] = mio_outsel0_out2_qs;
				reg_rdata_next[23:18] = mio_outsel0_out3_qs;
				reg_rdata_next[29:24] = mio_outsel0_out4_qs;
			end
			addr_hit[9]: begin
				reg_rdata_next[5:0] = mio_outsel1_out5_qs;
				reg_rdata_next[11:6] = mio_outsel1_out6_qs;
				reg_rdata_next[17:12] = mio_outsel1_out7_qs;
				reg_rdata_next[23:18] = mio_outsel1_out8_qs;
				reg_rdata_next[29:24] = mio_outsel1_out9_qs;
			end
			addr_hit[10]: begin
				reg_rdata_next[5:0] = mio_outsel2_out10_qs;
				reg_rdata_next[11:6] = mio_outsel2_out11_qs;
				reg_rdata_next[17:12] = mio_outsel2_out12_qs;
				reg_rdata_next[23:18] = mio_outsel2_out13_qs;
				reg_rdata_next[29:24] = mio_outsel2_out14_qs;
			end
			addr_hit[11]: begin
				reg_rdata_next[5:0] = mio_outsel3_out15_qs;
				reg_rdata_next[11:6] = mio_outsel3_out16_qs;
				reg_rdata_next[17:12] = mio_outsel3_out17_qs;
				reg_rdata_next[23:18] = mio_outsel3_out18_qs;
				reg_rdata_next[29:24] = mio_outsel3_out19_qs;
			end
			addr_hit[12]: begin
				reg_rdata_next[5:0] = mio_outsel4_out20_qs;
				reg_rdata_next[11:6] = mio_outsel4_out21_qs;
				reg_rdata_next[17:12] = mio_outsel4_out22_qs;
				reg_rdata_next[23:18] = mio_outsel4_out23_qs;
				reg_rdata_next[29:24] = mio_outsel4_out24_qs;
			end
			addr_hit[13]: begin
				reg_rdata_next[5:0] = mio_outsel5_out25_qs;
				reg_rdata_next[11:6] = mio_outsel5_out26_qs;
				reg_rdata_next[17:12] = mio_outsel5_out27_qs;
				reg_rdata_next[23:18] = mio_outsel5_out28_qs;
				reg_rdata_next[29:24] = mio_outsel5_out29_qs;
			end
			addr_hit[14]: begin
				reg_rdata_next[5:0] = mio_outsel6_out30_qs;
				reg_rdata_next[11:6] = mio_outsel6_out31_qs;
			end
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module pinmux (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	periph_to_mio_i,
	periph_to_mio_oe_i,
	mio_to_periph_o,
	mio_out_o,
	mio_oe_o,
	mio_in_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	localparam signed [31:0] pinmux_reg_pkg_NPeriphOut = 32;
	input [pinmux_reg_pkg_NPeriphOut - 1:0] periph_to_mio_i;
	input [pinmux_reg_pkg_NPeriphOut - 1:0] periph_to_mio_oe_i;
	localparam signed [31:0] pinmux_reg_pkg_NPeriphIn = 32;
	output wire [pinmux_reg_pkg_NPeriphIn - 1:0] mio_to_periph_o;
	localparam signed [31:0] pinmux_reg_pkg_NMioPads = 32;
	output wire [pinmux_reg_pkg_NMioPads - 1:0] mio_out_o;
	output wire [pinmux_reg_pkg_NMioPads - 1:0] mio_oe_o;
	input [pinmux_reg_pkg_NMioPads - 1:0] mio_in_i;
	wire [383:0] reg2hw;
	pinmux_reg_top i_reg_top(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.devmode_i(1'b1)
	);
	generate
		genvar k;
		for (k = 0; k < pinmux_reg_pkg_NPeriphIn; k = k + 1) begin : gen_periph_in
			wire [pinmux_reg_pkg_NMioPads + 1:0] data_mux;
			function automatic [33:0] sv2v_cast_34;
				input reg [33:0] inp;
				sv2v_cast_34 = inp;
			endfunction
			assign data_mux = sv2v_cast_34({mio_in_i, 1'b1, 1'b0});
			assign mio_to_periph_o[k] = data_mux[reg2hw[192 + ((k * 6) + 5)-:6]];
		end
	endgenerate
	generate
		for (k = 0; k < pinmux_reg_pkg_NMioPads; k = k + 1) begin : gen_mio_out
			wire [pinmux_reg_pkg_NPeriphOut + 2:0] data_mux;
			wire [pinmux_reg_pkg_NPeriphOut + 2:0] oe_mux;
			function automatic [34:0] sv2v_cast_35;
				input reg [34:0] inp;
				sv2v_cast_35 = inp;
			endfunction
			assign data_mux = sv2v_cast_35({periph_to_mio_i, 1'b0, 1'b1, 1'b0});
			assign oe_mux = sv2v_cast_35({periph_to_mio_oe_i, 1'b0, 1'b1, 1'b1});
			assign mio_out_o[k] = data_mux[reg2hw[(k * 6) + 5-:6]];
			assign mio_oe_o[k] = oe_mux[reg2hw[(k * 6) + 5-:6]];
		end
	endgenerate
endmodule
module alert_handler_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [828:0] reg2hw;
	input wire [229:0] hw2reg;
	input devmode_i;
	localparam signed [31:0] NAlerts = 1;
	localparam signed [31:0] EscCntDw = 32;
	localparam signed [31:0] AccuCntDw = 16;
	localparam signed [31:0] LfsrSeed = 2147483647;
	localparam [NAlerts - 1:0] AsyncOn = 1'b0;
	localparam signed [31:0] N_CLASSES = 4;
	localparam signed [31:0] N_ESC_SEV = 4;
	localparam signed [31:0] N_PHASES = 4;
	localparam signed [31:0] N_LOC_ALERT = 4;
	localparam signed [31:0] PING_CNT_DW = 24;
	localparam signed [31:0] PHASE_DW = 2;
	localparam signed [31:0] CLASS_DW = 2;
	localparam ALERT_HANDLER_INTR_STATE_OFFSET = 8'h00;
	localparam ALERT_HANDLER_INTR_ENABLE_OFFSET = 8'h04;
	localparam ALERT_HANDLER_INTR_TEST_OFFSET = 8'h08;
	localparam ALERT_HANDLER_REGEN_OFFSET = 8'h0c;
	localparam ALERT_HANDLER_PING_TIMEOUT_CYC_OFFSET = 8'h10;
	localparam ALERT_HANDLER_ALERT_EN_OFFSET = 8'h14;
	localparam ALERT_HANDLER_ALERT_CLASS_OFFSET = 8'h18;
	localparam ALERT_HANDLER_ALERT_CAUSE_OFFSET = 8'h1c;
	localparam ALERT_HANDLER_LOC_ALERT_EN_OFFSET = 8'h20;
	localparam ALERT_HANDLER_LOC_ALERT_CLASS_OFFSET = 8'h24;
	localparam ALERT_HANDLER_LOC_ALERT_CAUSE_OFFSET = 8'h28;
	localparam ALERT_HANDLER_CLASSA_CTRL_OFFSET = 8'h2c;
	localparam ALERT_HANDLER_CLASSA_CLREN_OFFSET = 8'h30;
	localparam ALERT_HANDLER_CLASSA_CLR_OFFSET = 8'h34;
	localparam ALERT_HANDLER_CLASSA_ACCUM_CNT_OFFSET = 8'h38;
	localparam ALERT_HANDLER_CLASSA_ACCUM_THRESH_OFFSET = 8'h3c;
	localparam ALERT_HANDLER_CLASSA_TIMEOUT_CYC_OFFSET = 8'h40;
	localparam ALERT_HANDLER_CLASSA_PHASE0_CYC_OFFSET = 8'h44;
	localparam ALERT_HANDLER_CLASSA_PHASE1_CYC_OFFSET = 8'h48;
	localparam ALERT_HANDLER_CLASSA_PHASE2_CYC_OFFSET = 8'h4c;
	localparam ALERT_HANDLER_CLASSA_PHASE3_CYC_OFFSET = 8'h50;
	localparam ALERT_HANDLER_CLASSA_ESC_CNT_OFFSET = 8'h54;
	localparam ALERT_HANDLER_CLASSA_STATE_OFFSET = 8'h58;
	localparam ALERT_HANDLER_CLASSB_CTRL_OFFSET = 8'h5c;
	localparam ALERT_HANDLER_CLASSB_CLREN_OFFSET = 8'h60;
	localparam ALERT_HANDLER_CLASSB_CLR_OFFSET = 8'h64;
	localparam ALERT_HANDLER_CLASSB_ACCUM_CNT_OFFSET = 8'h68;
	localparam ALERT_HANDLER_CLASSB_ACCUM_THRESH_OFFSET = 8'h6c;
	localparam ALERT_HANDLER_CLASSB_TIMEOUT_CYC_OFFSET = 8'h70;
	localparam ALERT_HANDLER_CLASSB_PHASE0_CYC_OFFSET = 8'h74;
	localparam ALERT_HANDLER_CLASSB_PHASE1_CYC_OFFSET = 8'h78;
	localparam ALERT_HANDLER_CLASSB_PHASE2_CYC_OFFSET = 8'h7c;
	localparam ALERT_HANDLER_CLASSB_PHASE3_CYC_OFFSET = 8'h80;
	localparam ALERT_HANDLER_CLASSB_ESC_CNT_OFFSET = 8'h84;
	localparam ALERT_HANDLER_CLASSB_STATE_OFFSET = 8'h88;
	localparam ALERT_HANDLER_CLASSC_CTRL_OFFSET = 8'h8c;
	localparam ALERT_HANDLER_CLASSC_CLREN_OFFSET = 8'h90;
	localparam ALERT_HANDLER_CLASSC_CLR_OFFSET = 8'h94;
	localparam ALERT_HANDLER_CLASSC_ACCUM_CNT_OFFSET = 8'h98;
	localparam ALERT_HANDLER_CLASSC_ACCUM_THRESH_OFFSET = 8'h9c;
	localparam ALERT_HANDLER_CLASSC_TIMEOUT_CYC_OFFSET = 8'ha0;
	localparam ALERT_HANDLER_CLASSC_PHASE0_CYC_OFFSET = 8'ha4;
	localparam ALERT_HANDLER_CLASSC_PHASE1_CYC_OFFSET = 8'ha8;
	localparam ALERT_HANDLER_CLASSC_PHASE2_CYC_OFFSET = 8'hac;
	localparam ALERT_HANDLER_CLASSC_PHASE3_CYC_OFFSET = 8'hb0;
	localparam ALERT_HANDLER_CLASSC_ESC_CNT_OFFSET = 8'hb4;
	localparam ALERT_HANDLER_CLASSC_STATE_OFFSET = 8'hb8;
	localparam ALERT_HANDLER_CLASSD_CTRL_OFFSET = 8'hbc;
	localparam ALERT_HANDLER_CLASSD_CLREN_OFFSET = 8'hc0;
	localparam ALERT_HANDLER_CLASSD_CLR_OFFSET = 8'hc4;
	localparam ALERT_HANDLER_CLASSD_ACCUM_CNT_OFFSET = 8'hc8;
	localparam ALERT_HANDLER_CLASSD_ACCUM_THRESH_OFFSET = 8'hcc;
	localparam ALERT_HANDLER_CLASSD_TIMEOUT_CYC_OFFSET = 8'hd0;
	localparam ALERT_HANDLER_CLASSD_PHASE0_CYC_OFFSET = 8'hd4;
	localparam ALERT_HANDLER_CLASSD_PHASE1_CYC_OFFSET = 8'hd8;
	localparam ALERT_HANDLER_CLASSD_PHASE2_CYC_OFFSET = 8'hdc;
	localparam ALERT_HANDLER_CLASSD_PHASE3_CYC_OFFSET = 8'he0;
	localparam ALERT_HANDLER_CLASSD_ESC_CNT_OFFSET = 8'he4;
	localparam ALERT_HANDLER_CLASSD_STATE_OFFSET = 8'he8;
	localparam signed [31:0] ALERT_HANDLER_INTR_STATE = 0;
	localparam signed [31:0] ALERT_HANDLER_INTR_ENABLE = 1;
	localparam signed [31:0] ALERT_HANDLER_INTR_TEST = 2;
	localparam signed [31:0] ALERT_HANDLER_REGEN = 3;
	localparam signed [31:0] ALERT_HANDLER_PING_TIMEOUT_CYC = 4;
	localparam signed [31:0] ALERT_HANDLER_ALERT_EN = 5;
	localparam signed [31:0] ALERT_HANDLER_ALERT_CLASS = 6;
	localparam signed [31:0] ALERT_HANDLER_ALERT_CAUSE = 7;
	localparam signed [31:0] ALERT_HANDLER_LOC_ALERT_EN = 8;
	localparam signed [31:0] ALERT_HANDLER_LOC_ALERT_CLASS = 9;
	localparam signed [31:0] ALERT_HANDLER_LOC_ALERT_CAUSE = 10;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_CTRL = 11;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_CLREN = 12;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_CLR = 13;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_ACCUM_CNT = 14;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_ACCUM_THRESH = 15;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_TIMEOUT_CYC = 16;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_PHASE0_CYC = 17;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_PHASE1_CYC = 18;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_PHASE2_CYC = 19;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_PHASE3_CYC = 20;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_ESC_CNT = 21;
	localparam signed [31:0] ALERT_HANDLER_CLASSA_STATE = 22;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_CTRL = 23;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_CLREN = 24;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_CLR = 25;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_ACCUM_CNT = 26;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_ACCUM_THRESH = 27;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_TIMEOUT_CYC = 28;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_PHASE0_CYC = 29;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_PHASE1_CYC = 30;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_PHASE2_CYC = 31;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_PHASE3_CYC = 32;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_ESC_CNT = 33;
	localparam signed [31:0] ALERT_HANDLER_CLASSB_STATE = 34;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_CTRL = 35;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_CLREN = 36;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_CLR = 37;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_ACCUM_CNT = 38;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_ACCUM_THRESH = 39;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_TIMEOUT_CYC = 40;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_PHASE0_CYC = 41;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_PHASE1_CYC = 42;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_PHASE2_CYC = 43;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_PHASE3_CYC = 44;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_ESC_CNT = 45;
	localparam signed [31:0] ALERT_HANDLER_CLASSC_STATE = 46;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_CTRL = 47;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_CLREN = 48;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_CLR = 49;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_ACCUM_CNT = 50;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_ACCUM_THRESH = 51;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_TIMEOUT_CYC = 52;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_PHASE0_CYC = 53;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_PHASE1_CYC = 54;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_PHASE2_CYC = 55;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_PHASE3_CYC = 56;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_ESC_CNT = 57;
	localparam signed [31:0] ALERT_HANDLER_CLASSD_STATE = 58;
	localparam [235:0] ALERT_HANDLER_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0011, 4'b0001, 4'b0001, 4'b0011, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0011, 4'b0001, 4'b0001, 4'b0011, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0011, 4'b0001, 4'b0001, 4'b0011, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0011, 4'b0001, 4'b0001, 4'b0011, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001};
	localparam AW = 8;
	localparam DW = 32;
	localparam DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire intr_state_classa_qs;
	wire intr_state_classa_wd;
	wire intr_state_classa_we;
	wire intr_state_classb_qs;
	wire intr_state_classb_wd;
	wire intr_state_classb_we;
	wire intr_state_classc_qs;
	wire intr_state_classc_wd;
	wire intr_state_classc_we;
	wire intr_state_classd_qs;
	wire intr_state_classd_wd;
	wire intr_state_classd_we;
	wire intr_enable_classa_qs;
	wire intr_enable_classa_wd;
	wire intr_enable_classa_we;
	wire intr_enable_classb_qs;
	wire intr_enable_classb_wd;
	wire intr_enable_classb_we;
	wire intr_enable_classc_qs;
	wire intr_enable_classc_wd;
	wire intr_enable_classc_we;
	wire intr_enable_classd_qs;
	wire intr_enable_classd_wd;
	wire intr_enable_classd_we;
	wire intr_test_classa_wd;
	wire intr_test_classa_we;
	wire intr_test_classb_wd;
	wire intr_test_classb_we;
	wire intr_test_classc_wd;
	wire intr_test_classc_we;
	wire intr_test_classd_wd;
	wire intr_test_classd_we;
	wire regen_qs;
	wire regen_wd;
	wire regen_we;
	wire [23:0] ping_timeout_cyc_qs;
	wire [23:0] ping_timeout_cyc_wd;
	wire ping_timeout_cyc_we;
	wire alert_en_qs;
	wire alert_en_wd;
	wire alert_en_we;
	wire [1:0] alert_class_qs;
	wire [1:0] alert_class_wd;
	wire alert_class_we;
	wire alert_cause_qs;
	wire alert_cause_wd;
	wire alert_cause_we;
	wire loc_alert_en_en_la0_qs;
	wire loc_alert_en_en_la0_wd;
	wire loc_alert_en_en_la0_we;
	wire loc_alert_en_en_la1_qs;
	wire loc_alert_en_en_la1_wd;
	wire loc_alert_en_en_la1_we;
	wire loc_alert_en_en_la2_qs;
	wire loc_alert_en_en_la2_wd;
	wire loc_alert_en_en_la2_we;
	wire loc_alert_en_en_la3_qs;
	wire loc_alert_en_en_la3_wd;
	wire loc_alert_en_en_la3_we;
	wire [1:0] loc_alert_class_class_la0_qs;
	wire [1:0] loc_alert_class_class_la0_wd;
	wire loc_alert_class_class_la0_we;
	wire [1:0] loc_alert_class_class_la1_qs;
	wire [1:0] loc_alert_class_class_la1_wd;
	wire loc_alert_class_class_la1_we;
	wire [1:0] loc_alert_class_class_la2_qs;
	wire [1:0] loc_alert_class_class_la2_wd;
	wire loc_alert_class_class_la2_we;
	wire [1:0] loc_alert_class_class_la3_qs;
	wire [1:0] loc_alert_class_class_la3_wd;
	wire loc_alert_class_class_la3_we;
	wire loc_alert_cause_la0_qs;
	wire loc_alert_cause_la0_wd;
	wire loc_alert_cause_la0_we;
	wire loc_alert_cause_la1_qs;
	wire loc_alert_cause_la1_wd;
	wire loc_alert_cause_la1_we;
	wire loc_alert_cause_la2_qs;
	wire loc_alert_cause_la2_wd;
	wire loc_alert_cause_la2_we;
	wire loc_alert_cause_la3_qs;
	wire loc_alert_cause_la3_wd;
	wire loc_alert_cause_la3_we;
	wire classa_ctrl_en_qs;
	wire classa_ctrl_en_wd;
	wire classa_ctrl_en_we;
	wire classa_ctrl_lock_qs;
	wire classa_ctrl_lock_wd;
	wire classa_ctrl_lock_we;
	wire classa_ctrl_en_e0_qs;
	wire classa_ctrl_en_e0_wd;
	wire classa_ctrl_en_e0_we;
	wire classa_ctrl_en_e1_qs;
	wire classa_ctrl_en_e1_wd;
	wire classa_ctrl_en_e1_we;
	wire classa_ctrl_en_e2_qs;
	wire classa_ctrl_en_e2_wd;
	wire classa_ctrl_en_e2_we;
	wire classa_ctrl_en_e3_qs;
	wire classa_ctrl_en_e3_wd;
	wire classa_ctrl_en_e3_we;
	wire [1:0] classa_ctrl_map_e0_qs;
	wire [1:0] classa_ctrl_map_e0_wd;
	wire classa_ctrl_map_e0_we;
	wire [1:0] classa_ctrl_map_e1_qs;
	wire [1:0] classa_ctrl_map_e1_wd;
	wire classa_ctrl_map_e1_we;
	wire [1:0] classa_ctrl_map_e2_qs;
	wire [1:0] classa_ctrl_map_e2_wd;
	wire classa_ctrl_map_e2_we;
	wire [1:0] classa_ctrl_map_e3_qs;
	wire [1:0] classa_ctrl_map_e3_wd;
	wire classa_ctrl_map_e3_we;
	wire classa_clren_qs;
	wire classa_clren_wd;
	wire classa_clren_we;
	wire classa_clr_wd;
	wire classa_clr_we;
	wire [15:0] classa_accum_cnt_qs;
	wire classa_accum_cnt_re;
	wire [15:0] classa_accum_thresh_qs;
	wire [15:0] classa_accum_thresh_wd;
	wire classa_accum_thresh_we;
	wire [31:0] classa_timeout_cyc_qs;
	wire [31:0] classa_timeout_cyc_wd;
	wire classa_timeout_cyc_we;
	wire [31:0] classa_phase0_cyc_qs;
	wire [31:0] classa_phase0_cyc_wd;
	wire classa_phase0_cyc_we;
	wire [31:0] classa_phase1_cyc_qs;
	wire [31:0] classa_phase1_cyc_wd;
	wire classa_phase1_cyc_we;
	wire [31:0] classa_phase2_cyc_qs;
	wire [31:0] classa_phase2_cyc_wd;
	wire classa_phase2_cyc_we;
	wire [31:0] classa_phase3_cyc_qs;
	wire [31:0] classa_phase3_cyc_wd;
	wire classa_phase3_cyc_we;
	wire [31:0] classa_esc_cnt_qs;
	wire classa_esc_cnt_re;
	wire [2:0] classa_state_qs;
	wire classa_state_re;
	wire classb_ctrl_en_qs;
	wire classb_ctrl_en_wd;
	wire classb_ctrl_en_we;
	wire classb_ctrl_lock_qs;
	wire classb_ctrl_lock_wd;
	wire classb_ctrl_lock_we;
	wire classb_ctrl_en_e0_qs;
	wire classb_ctrl_en_e0_wd;
	wire classb_ctrl_en_e0_we;
	wire classb_ctrl_en_e1_qs;
	wire classb_ctrl_en_e1_wd;
	wire classb_ctrl_en_e1_we;
	wire classb_ctrl_en_e2_qs;
	wire classb_ctrl_en_e2_wd;
	wire classb_ctrl_en_e2_we;
	wire classb_ctrl_en_e3_qs;
	wire classb_ctrl_en_e3_wd;
	wire classb_ctrl_en_e3_we;
	wire [1:0] classb_ctrl_map_e0_qs;
	wire [1:0] classb_ctrl_map_e0_wd;
	wire classb_ctrl_map_e0_we;
	wire [1:0] classb_ctrl_map_e1_qs;
	wire [1:0] classb_ctrl_map_e1_wd;
	wire classb_ctrl_map_e1_we;
	wire [1:0] classb_ctrl_map_e2_qs;
	wire [1:0] classb_ctrl_map_e2_wd;
	wire classb_ctrl_map_e2_we;
	wire [1:0] classb_ctrl_map_e3_qs;
	wire [1:0] classb_ctrl_map_e3_wd;
	wire classb_ctrl_map_e3_we;
	wire classb_clren_qs;
	wire classb_clren_wd;
	wire classb_clren_we;
	wire classb_clr_wd;
	wire classb_clr_we;
	wire [15:0] classb_accum_cnt_qs;
	wire classb_accum_cnt_re;
	wire [15:0] classb_accum_thresh_qs;
	wire [15:0] classb_accum_thresh_wd;
	wire classb_accum_thresh_we;
	wire [31:0] classb_timeout_cyc_qs;
	wire [31:0] classb_timeout_cyc_wd;
	wire classb_timeout_cyc_we;
	wire [31:0] classb_phase0_cyc_qs;
	wire [31:0] classb_phase0_cyc_wd;
	wire classb_phase0_cyc_we;
	wire [31:0] classb_phase1_cyc_qs;
	wire [31:0] classb_phase1_cyc_wd;
	wire classb_phase1_cyc_we;
	wire [31:0] classb_phase2_cyc_qs;
	wire [31:0] classb_phase2_cyc_wd;
	wire classb_phase2_cyc_we;
	wire [31:0] classb_phase3_cyc_qs;
	wire [31:0] classb_phase3_cyc_wd;
	wire classb_phase3_cyc_we;
	wire [31:0] classb_esc_cnt_qs;
	wire classb_esc_cnt_re;
	wire [2:0] classb_state_qs;
	wire classb_state_re;
	wire classc_ctrl_en_qs;
	wire classc_ctrl_en_wd;
	wire classc_ctrl_en_we;
	wire classc_ctrl_lock_qs;
	wire classc_ctrl_lock_wd;
	wire classc_ctrl_lock_we;
	wire classc_ctrl_en_e0_qs;
	wire classc_ctrl_en_e0_wd;
	wire classc_ctrl_en_e0_we;
	wire classc_ctrl_en_e1_qs;
	wire classc_ctrl_en_e1_wd;
	wire classc_ctrl_en_e1_we;
	wire classc_ctrl_en_e2_qs;
	wire classc_ctrl_en_e2_wd;
	wire classc_ctrl_en_e2_we;
	wire classc_ctrl_en_e3_qs;
	wire classc_ctrl_en_e3_wd;
	wire classc_ctrl_en_e3_we;
	wire [1:0] classc_ctrl_map_e0_qs;
	wire [1:0] classc_ctrl_map_e0_wd;
	wire classc_ctrl_map_e0_we;
	wire [1:0] classc_ctrl_map_e1_qs;
	wire [1:0] classc_ctrl_map_e1_wd;
	wire classc_ctrl_map_e1_we;
	wire [1:0] classc_ctrl_map_e2_qs;
	wire [1:0] classc_ctrl_map_e2_wd;
	wire classc_ctrl_map_e2_we;
	wire [1:0] classc_ctrl_map_e3_qs;
	wire [1:0] classc_ctrl_map_e3_wd;
	wire classc_ctrl_map_e3_we;
	wire classc_clren_qs;
	wire classc_clren_wd;
	wire classc_clren_we;
	wire classc_clr_wd;
	wire classc_clr_we;
	wire [15:0] classc_accum_cnt_qs;
	wire classc_accum_cnt_re;
	wire [15:0] classc_accum_thresh_qs;
	wire [15:0] classc_accum_thresh_wd;
	wire classc_accum_thresh_we;
	wire [31:0] classc_timeout_cyc_qs;
	wire [31:0] classc_timeout_cyc_wd;
	wire classc_timeout_cyc_we;
	wire [31:0] classc_phase0_cyc_qs;
	wire [31:0] classc_phase0_cyc_wd;
	wire classc_phase0_cyc_we;
	wire [31:0] classc_phase1_cyc_qs;
	wire [31:0] classc_phase1_cyc_wd;
	wire classc_phase1_cyc_we;
	wire [31:0] classc_phase2_cyc_qs;
	wire [31:0] classc_phase2_cyc_wd;
	wire classc_phase2_cyc_we;
	wire [31:0] classc_phase3_cyc_qs;
	wire [31:0] classc_phase3_cyc_wd;
	wire classc_phase3_cyc_we;
	wire [31:0] classc_esc_cnt_qs;
	wire classc_esc_cnt_re;
	wire [2:0] classc_state_qs;
	wire classc_state_re;
	wire classd_ctrl_en_qs;
	wire classd_ctrl_en_wd;
	wire classd_ctrl_en_we;
	wire classd_ctrl_lock_qs;
	wire classd_ctrl_lock_wd;
	wire classd_ctrl_lock_we;
	wire classd_ctrl_en_e0_qs;
	wire classd_ctrl_en_e0_wd;
	wire classd_ctrl_en_e0_we;
	wire classd_ctrl_en_e1_qs;
	wire classd_ctrl_en_e1_wd;
	wire classd_ctrl_en_e1_we;
	wire classd_ctrl_en_e2_qs;
	wire classd_ctrl_en_e2_wd;
	wire classd_ctrl_en_e2_we;
	wire classd_ctrl_en_e3_qs;
	wire classd_ctrl_en_e3_wd;
	wire classd_ctrl_en_e3_we;
	wire [1:0] classd_ctrl_map_e0_qs;
	wire [1:0] classd_ctrl_map_e0_wd;
	wire classd_ctrl_map_e0_we;
	wire [1:0] classd_ctrl_map_e1_qs;
	wire [1:0] classd_ctrl_map_e1_wd;
	wire classd_ctrl_map_e1_we;
	wire [1:0] classd_ctrl_map_e2_qs;
	wire [1:0] classd_ctrl_map_e2_wd;
	wire classd_ctrl_map_e2_we;
	wire [1:0] classd_ctrl_map_e3_qs;
	wire [1:0] classd_ctrl_map_e3_wd;
	wire classd_ctrl_map_e3_we;
	wire classd_clren_qs;
	wire classd_clren_wd;
	wire classd_clren_we;
	wire classd_clr_wd;
	wire classd_clr_we;
	wire [15:0] classd_accum_cnt_qs;
	wire classd_accum_cnt_re;
	wire [15:0] classd_accum_thresh_qs;
	wire [15:0] classd_accum_thresh_wd;
	wire classd_accum_thresh_we;
	wire [31:0] classd_timeout_cyc_qs;
	wire [31:0] classd_timeout_cyc_wd;
	wire classd_timeout_cyc_we;
	wire [31:0] classd_phase0_cyc_qs;
	wire [31:0] classd_phase0_cyc_wd;
	wire classd_phase0_cyc_we;
	wire [31:0] classd_phase1_cyc_qs;
	wire [31:0] classd_phase1_cyc_wd;
	wire classd_phase1_cyc_we;
	wire [31:0] classd_phase2_cyc_qs;
	wire [31:0] classd_phase2_cyc_wd;
	wire classd_phase2_cyc_we;
	wire [31:0] classd_phase3_cyc_qs;
	wire [31:0] classd_phase3_cyc_wd;
	wire classd_phase3_cyc_we;
	wire [31:0] classd_esc_cnt_qs;
	wire classd_esc_cnt_re;
	wire [2:0] classd_state_qs;
	wire classd_state_re;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_classa(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_classa_we),
		.wd(intr_state_classa_wd),
		.de(hw2reg[228]),
		.d(hw2reg[229]),
		.qe(),
		.q(reg2hw[828]),
		.qs(intr_state_classa_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_classb(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_classb_we),
		.wd(intr_state_classb_wd),
		.de(hw2reg[226]),
		.d(hw2reg[227]),
		.qe(),
		.q(reg2hw[827]),
		.qs(intr_state_classb_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_classc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_classc_we),
		.wd(intr_state_classc_wd),
		.de(hw2reg[224]),
		.d(hw2reg[225]),
		.qe(),
		.q(reg2hw[826]),
		.qs(intr_state_classc_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_classd(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_classd_we),
		.wd(intr_state_classd_wd),
		.de(hw2reg[222]),
		.d(hw2reg[223]),
		.qe(),
		.q(reg2hw[825]),
		.qs(intr_state_classd_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_classa(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_classa_we),
		.wd(intr_enable_classa_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[824]),
		.qs(intr_enable_classa_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_classb(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_classb_we),
		.wd(intr_enable_classb_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[823]),
		.qs(intr_enable_classb_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_classc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_classc_we),
		.wd(intr_enable_classc_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[822]),
		.qs(intr_enable_classc_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_classd(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_classd_we),
		.wd(intr_enable_classd_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[821]),
		.qs(intr_enable_classd_qs)
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_classa(
		.re(1'b0),
		.we(intr_test_classa_we),
		.wd(intr_test_classa_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[819]),
		.q(reg2hw[820]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_classb(
		.re(1'b0),
		.we(intr_test_classb_we),
		.wd(intr_test_classb_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[817]),
		.q(reg2hw[818]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_classc(
		.re(1'b0),
		.we(intr_test_classc_we),
		.wd(intr_test_classc_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[815]),
		.q(reg2hw[816]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_classd(
		.re(1'b0),
		.we(intr_test_classd_we),
		.wd(intr_test_classd_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[813]),
		.q(reg2hw[814]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h1)
	) u_regen(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(regen_we),
		.wd(regen_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[812]),
		.qs(regen_qs)
	);
	prim_subreg #(
		.DW(24),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(24'h000020)
	) u_ping_timeout_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ping_timeout_cyc_we & regen_qs),
		.wd(ping_timeout_cyc_wd),
		.de(1'b0),
		.d({24 {1'sb0}}),
		.qe(),
		.q(reg2hw[811-:24]),
		.qs(ping_timeout_cyc_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_alert_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(alert_en_we & regen_qs),
		.wd(alert_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[787]),
		.qs(alert_en_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_alert_class(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(alert_class_we & regen_qs),
		.wd(alert_class_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[786-:2]),
		.qs(alert_class_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_alert_cause(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(alert_cause_we),
		.wd(alert_cause_wd),
		.de(hw2reg[220]),
		.d(hw2reg[221]),
		.qe(),
		.q(reg2hw[784]),
		.qs(alert_cause_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_loc_alert_en_en_la0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_en_en_la0_we & regen_qs),
		.wd(loc_alert_en_en_la0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[780]),
		.qs(loc_alert_en_en_la0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_loc_alert_en_en_la1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_en_en_la1_we & regen_qs),
		.wd(loc_alert_en_en_la1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[781]),
		.qs(loc_alert_en_en_la1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_loc_alert_en_en_la2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_en_en_la2_we & regen_qs),
		.wd(loc_alert_en_en_la2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[782]),
		.qs(loc_alert_en_en_la2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_loc_alert_en_en_la3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_en_en_la3_we & regen_qs),
		.wd(loc_alert_en_en_la3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[783]),
		.qs(loc_alert_en_en_la3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_loc_alert_class_class_la0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_class_class_la0_we & regen_qs),
		.wd(loc_alert_class_class_la0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[773-:2]),
		.qs(loc_alert_class_class_la0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_loc_alert_class_class_la1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_class_class_la1_we & regen_qs),
		.wd(loc_alert_class_class_la1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[775-:2]),
		.qs(loc_alert_class_class_la1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_loc_alert_class_class_la2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_class_class_la2_we & regen_qs),
		.wd(loc_alert_class_class_la2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[777-:2]),
		.qs(loc_alert_class_class_la2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_loc_alert_class_class_la3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_class_class_la3_we & regen_qs),
		.wd(loc_alert_class_class_la3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[779-:2]),
		.qs(loc_alert_class_class_la3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_loc_alert_cause_la0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_cause_la0_we),
		.wd(loc_alert_cause_la0_wd),
		.de(hw2reg[212]),
		.d(hw2reg[213]),
		.qe(),
		.q(reg2hw[768]),
		.qs(loc_alert_cause_la0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_loc_alert_cause_la1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_cause_la1_we),
		.wd(loc_alert_cause_la1_wd),
		.de(hw2reg[214]),
		.d(hw2reg[215]),
		.qe(),
		.q(reg2hw[769]),
		.qs(loc_alert_cause_la1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_loc_alert_cause_la2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_cause_la2_we),
		.wd(loc_alert_cause_la2_wd),
		.de(hw2reg[216]),
		.d(hw2reg[217]),
		.qe(),
		.q(reg2hw[770]),
		.qs(loc_alert_cause_la2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_loc_alert_cause_la3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(loc_alert_cause_la3_we),
		.wd(loc_alert_cause_la3_wd),
		.de(hw2reg[218]),
		.d(hw2reg[219]),
		.qe(),
		.q(reg2hw[771]),
		.qs(loc_alert_cause_la3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classa_ctrl_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_en_we & regen_qs),
		.wd(classa_ctrl_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[767]),
		.qs(classa_ctrl_en_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classa_ctrl_lock(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_lock_we & regen_qs),
		.wd(classa_ctrl_lock_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[766]),
		.qs(classa_ctrl_lock_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classa_ctrl_en_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_en_e0_we & regen_qs),
		.wd(classa_ctrl_en_e0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[765]),
		.qs(classa_ctrl_en_e0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classa_ctrl_en_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_en_e1_we & regen_qs),
		.wd(classa_ctrl_en_e1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[764]),
		.qs(classa_ctrl_en_e1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classa_ctrl_en_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_en_e2_we & regen_qs),
		.wd(classa_ctrl_en_e2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[763]),
		.qs(classa_ctrl_en_e2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classa_ctrl_en_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_en_e3_we & regen_qs),
		.wd(classa_ctrl_en_e3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[762]),
		.qs(classa_ctrl_en_e3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_classa_ctrl_map_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_map_e0_we & regen_qs),
		.wd(classa_ctrl_map_e0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[761-:2]),
		.qs(classa_ctrl_map_e0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h1)
	) u_classa_ctrl_map_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_map_e1_we & regen_qs),
		.wd(classa_ctrl_map_e1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[759-:2]),
		.qs(classa_ctrl_map_e1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h2)
	) u_classa_ctrl_map_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_map_e2_we & regen_qs),
		.wd(classa_ctrl_map_e2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[757-:2]),
		.qs(classa_ctrl_map_e2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h3)
	) u_classa_ctrl_map_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_ctrl_map_e3_we & regen_qs),
		.wd(classa_ctrl_map_e3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[755-:2]),
		.qs(classa_ctrl_map_e3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h1)
	) u_classa_clren(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_clren_we),
		.wd(classa_clren_wd),
		.de(hw2reg[210]),
		.d(hw2reg[211]),
		.qe(),
		.q(),
		.qs(classa_clren_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_classa_clr(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_clr_we & classa_clren_qs),
		.wd(classa_clr_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[752]),
		.q(reg2hw[753]),
		.qs()
	);
	prim_subreg_ext #(.DW(16)) u_classa_accum_cnt(
		.re(classa_accum_cnt_re),
		.we(1'b0),
		.wd({16 {1'sb0}}),
		.d(hw2reg[209-:16]),
		.qre(),
		.qe(),
		.q(),
		.qs(classa_accum_cnt_qs)
	);
	prim_subreg #(
		.DW(16),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(16'h0000)
	) u_classa_accum_thresh(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_accum_thresh_we & regen_qs),
		.wd(classa_accum_thresh_wd),
		.de(1'b0),
		.d({16 {1'sb0}}),
		.qe(),
		.q(reg2hw[751-:16]),
		.qs(classa_accum_thresh_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classa_timeout_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_timeout_cyc_we & regen_qs),
		.wd(classa_timeout_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[735-:32]),
		.qs(classa_timeout_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classa_phase0_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_phase0_cyc_we & regen_qs),
		.wd(classa_phase0_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[703-:32]),
		.qs(classa_phase0_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classa_phase1_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_phase1_cyc_we & regen_qs),
		.wd(classa_phase1_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[671-:32]),
		.qs(classa_phase1_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classa_phase2_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_phase2_cyc_we & regen_qs),
		.wd(classa_phase2_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[639-:32]),
		.qs(classa_phase2_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classa_phase3_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classa_phase3_cyc_we & regen_qs),
		.wd(classa_phase3_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[607-:32]),
		.qs(classa_phase3_cyc_qs)
	);
	prim_subreg_ext #(.DW(32)) u_classa_esc_cnt(
		.re(classa_esc_cnt_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[193-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(classa_esc_cnt_qs)
	);
	prim_subreg_ext #(.DW(3)) u_classa_state(
		.re(classa_state_re),
		.we(1'b0),
		.wd({3 {1'sb0}}),
		.d(hw2reg[161-:3]),
		.qre(),
		.qe(),
		.q(),
		.qs(classa_state_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classb_ctrl_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_en_we & regen_qs),
		.wd(classb_ctrl_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[575]),
		.qs(classb_ctrl_en_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classb_ctrl_lock(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_lock_we & regen_qs),
		.wd(classb_ctrl_lock_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[574]),
		.qs(classb_ctrl_lock_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classb_ctrl_en_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_en_e0_we & regen_qs),
		.wd(classb_ctrl_en_e0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[573]),
		.qs(classb_ctrl_en_e0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classb_ctrl_en_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_en_e1_we & regen_qs),
		.wd(classb_ctrl_en_e1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[572]),
		.qs(classb_ctrl_en_e1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classb_ctrl_en_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_en_e2_we & regen_qs),
		.wd(classb_ctrl_en_e2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[571]),
		.qs(classb_ctrl_en_e2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classb_ctrl_en_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_en_e3_we & regen_qs),
		.wd(classb_ctrl_en_e3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[570]),
		.qs(classb_ctrl_en_e3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_classb_ctrl_map_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_map_e0_we & regen_qs),
		.wd(classb_ctrl_map_e0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[569-:2]),
		.qs(classb_ctrl_map_e0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h1)
	) u_classb_ctrl_map_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_map_e1_we & regen_qs),
		.wd(classb_ctrl_map_e1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[567-:2]),
		.qs(classb_ctrl_map_e1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h2)
	) u_classb_ctrl_map_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_map_e2_we & regen_qs),
		.wd(classb_ctrl_map_e2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[565-:2]),
		.qs(classb_ctrl_map_e2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h3)
	) u_classb_ctrl_map_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_ctrl_map_e3_we & regen_qs),
		.wd(classb_ctrl_map_e3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[563-:2]),
		.qs(classb_ctrl_map_e3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h1)
	) u_classb_clren(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_clren_we),
		.wd(classb_clren_wd),
		.de(hw2reg[157]),
		.d(hw2reg[158]),
		.qe(),
		.q(),
		.qs(classb_clren_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_classb_clr(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_clr_we & classb_clren_qs),
		.wd(classb_clr_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[560]),
		.q(reg2hw[561]),
		.qs()
	);
	prim_subreg_ext #(.DW(16)) u_classb_accum_cnt(
		.re(classb_accum_cnt_re),
		.we(1'b0),
		.wd({16 {1'sb0}}),
		.d(hw2reg[156-:16]),
		.qre(),
		.qe(),
		.q(),
		.qs(classb_accum_cnt_qs)
	);
	prim_subreg #(
		.DW(16),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(16'h0000)
	) u_classb_accum_thresh(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_accum_thresh_we & regen_qs),
		.wd(classb_accum_thresh_wd),
		.de(1'b0),
		.d({16 {1'sb0}}),
		.qe(),
		.q(reg2hw[559-:16]),
		.qs(classb_accum_thresh_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classb_timeout_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_timeout_cyc_we & regen_qs),
		.wd(classb_timeout_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[543-:32]),
		.qs(classb_timeout_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classb_phase0_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_phase0_cyc_we & regen_qs),
		.wd(classb_phase0_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[511-:32]),
		.qs(classb_phase0_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classb_phase1_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_phase1_cyc_we & regen_qs),
		.wd(classb_phase1_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[479-:32]),
		.qs(classb_phase1_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classb_phase2_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_phase2_cyc_we & regen_qs),
		.wd(classb_phase2_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[447-:32]),
		.qs(classb_phase2_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classb_phase3_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classb_phase3_cyc_we & regen_qs),
		.wd(classb_phase3_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[415-:32]),
		.qs(classb_phase3_cyc_qs)
	);
	prim_subreg_ext #(.DW(32)) u_classb_esc_cnt(
		.re(classb_esc_cnt_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[140-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(classb_esc_cnt_qs)
	);
	prim_subreg_ext #(.DW(3)) u_classb_state(
		.re(classb_state_re),
		.we(1'b0),
		.wd({3 {1'sb0}}),
		.d(hw2reg[108-:3]),
		.qre(),
		.qe(),
		.q(),
		.qs(classb_state_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classc_ctrl_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_en_we & regen_qs),
		.wd(classc_ctrl_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[383]),
		.qs(classc_ctrl_en_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classc_ctrl_lock(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_lock_we & regen_qs),
		.wd(classc_ctrl_lock_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[382]),
		.qs(classc_ctrl_lock_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classc_ctrl_en_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_en_e0_we & regen_qs),
		.wd(classc_ctrl_en_e0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[381]),
		.qs(classc_ctrl_en_e0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classc_ctrl_en_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_en_e1_we & regen_qs),
		.wd(classc_ctrl_en_e1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[380]),
		.qs(classc_ctrl_en_e1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classc_ctrl_en_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_en_e2_we & regen_qs),
		.wd(classc_ctrl_en_e2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[379]),
		.qs(classc_ctrl_en_e2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classc_ctrl_en_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_en_e3_we & regen_qs),
		.wd(classc_ctrl_en_e3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[378]),
		.qs(classc_ctrl_en_e3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_classc_ctrl_map_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_map_e0_we & regen_qs),
		.wd(classc_ctrl_map_e0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[377-:2]),
		.qs(classc_ctrl_map_e0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h1)
	) u_classc_ctrl_map_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_map_e1_we & regen_qs),
		.wd(classc_ctrl_map_e1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[375-:2]),
		.qs(classc_ctrl_map_e1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h2)
	) u_classc_ctrl_map_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_map_e2_we & regen_qs),
		.wd(classc_ctrl_map_e2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[373-:2]),
		.qs(classc_ctrl_map_e2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h3)
	) u_classc_ctrl_map_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_ctrl_map_e3_we & regen_qs),
		.wd(classc_ctrl_map_e3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[371-:2]),
		.qs(classc_ctrl_map_e3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h1)
	) u_classc_clren(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_clren_we),
		.wd(classc_clren_wd),
		.de(hw2reg[104]),
		.d(hw2reg[105]),
		.qe(),
		.q(),
		.qs(classc_clren_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_classc_clr(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_clr_we & classc_clren_qs),
		.wd(classc_clr_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[368]),
		.q(reg2hw[369]),
		.qs()
	);
	prim_subreg_ext #(.DW(16)) u_classc_accum_cnt(
		.re(classc_accum_cnt_re),
		.we(1'b0),
		.wd({16 {1'sb0}}),
		.d(hw2reg[103-:16]),
		.qre(),
		.qe(),
		.q(),
		.qs(classc_accum_cnt_qs)
	);
	prim_subreg #(
		.DW(16),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(16'h0000)
	) u_classc_accum_thresh(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_accum_thresh_we & regen_qs),
		.wd(classc_accum_thresh_wd),
		.de(1'b0),
		.d({16 {1'sb0}}),
		.qe(),
		.q(reg2hw[367-:16]),
		.qs(classc_accum_thresh_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classc_timeout_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_timeout_cyc_we & regen_qs),
		.wd(classc_timeout_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[351-:32]),
		.qs(classc_timeout_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classc_phase0_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_phase0_cyc_we & regen_qs),
		.wd(classc_phase0_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[319-:32]),
		.qs(classc_phase0_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classc_phase1_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_phase1_cyc_we & regen_qs),
		.wd(classc_phase1_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[287-:32]),
		.qs(classc_phase1_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classc_phase2_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_phase2_cyc_we & regen_qs),
		.wd(classc_phase2_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[255-:32]),
		.qs(classc_phase2_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classc_phase3_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classc_phase3_cyc_we & regen_qs),
		.wd(classc_phase3_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[223-:32]),
		.qs(classc_phase3_cyc_qs)
	);
	prim_subreg_ext #(.DW(32)) u_classc_esc_cnt(
		.re(classc_esc_cnt_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[87-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(classc_esc_cnt_qs)
	);
	prim_subreg_ext #(.DW(3)) u_classc_state(
		.re(classc_state_re),
		.we(1'b0),
		.wd({3 {1'sb0}}),
		.d(hw2reg[55-:3]),
		.qre(),
		.qe(),
		.q(),
		.qs(classc_state_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classd_ctrl_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_en_we & regen_qs),
		.wd(classd_ctrl_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[191]),
		.qs(classd_ctrl_en_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_classd_ctrl_lock(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_lock_we & regen_qs),
		.wd(classd_ctrl_lock_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[190]),
		.qs(classd_ctrl_lock_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classd_ctrl_en_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_en_e0_we & regen_qs),
		.wd(classd_ctrl_en_e0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[189]),
		.qs(classd_ctrl_en_e0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classd_ctrl_en_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_en_e1_we & regen_qs),
		.wd(classd_ctrl_en_e1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[188]),
		.qs(classd_ctrl_en_e1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classd_ctrl_en_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_en_e2_we & regen_qs),
		.wd(classd_ctrl_en_e2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[187]),
		.qs(classd_ctrl_en_e2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h1)
	) u_classd_ctrl_en_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_en_e3_we & regen_qs),
		.wd(classd_ctrl_en_e3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[186]),
		.qs(classd_ctrl_en_e3_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_classd_ctrl_map_e0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_map_e0_we & regen_qs),
		.wd(classd_ctrl_map_e0_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[185-:2]),
		.qs(classd_ctrl_map_e0_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h1)
	) u_classd_ctrl_map_e1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_map_e1_we & regen_qs),
		.wd(classd_ctrl_map_e1_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[183-:2]),
		.qs(classd_ctrl_map_e1_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h2)
	) u_classd_ctrl_map_e2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_map_e2_we & regen_qs),
		.wd(classd_ctrl_map_e2_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[181-:2]),
		.qs(classd_ctrl_map_e2_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h3)
	) u_classd_ctrl_map_e3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_ctrl_map_e3_we & regen_qs),
		.wd(classd_ctrl_map_e3_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[179-:2]),
		.qs(classd_ctrl_map_e3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h1)
	) u_classd_clren(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_clren_we),
		.wd(classd_clren_wd),
		.de(hw2reg[51]),
		.d(hw2reg[52]),
		.qe(),
		.q(),
		.qs(classd_clren_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_classd_clr(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_clr_we & classd_clren_qs),
		.wd(classd_clr_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[176]),
		.q(reg2hw[177]),
		.qs()
	);
	prim_subreg_ext #(.DW(16)) u_classd_accum_cnt(
		.re(classd_accum_cnt_re),
		.we(1'b0),
		.wd({16 {1'sb0}}),
		.d(hw2reg[50-:16]),
		.qre(),
		.qe(),
		.q(),
		.qs(classd_accum_cnt_qs)
	);
	prim_subreg #(
		.DW(16),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(16'h0000)
	) u_classd_accum_thresh(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_accum_thresh_we & regen_qs),
		.wd(classd_accum_thresh_wd),
		.de(1'b0),
		.d({16 {1'sb0}}),
		.qe(),
		.q(reg2hw[175-:16]),
		.qs(classd_accum_thresh_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classd_timeout_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_timeout_cyc_we & regen_qs),
		.wd(classd_timeout_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[159-:32]),
		.qs(classd_timeout_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classd_phase0_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_phase0_cyc_we & regen_qs),
		.wd(classd_phase0_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[127-:32]),
		.qs(classd_phase0_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classd_phase1_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_phase1_cyc_we & regen_qs),
		.wd(classd_phase1_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[95-:32]),
		.qs(classd_phase1_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classd_phase2_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_phase2_cyc_we & regen_qs),
		.wd(classd_phase2_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[63-:32]),
		.qs(classd_phase2_cyc_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_classd_phase3_cyc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(classd_phase3_cyc_we & regen_qs),
		.wd(classd_phase3_cyc_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[31-:32]),
		.qs(classd_phase3_cyc_qs)
	);
	prim_subreg_ext #(.DW(32)) u_classd_esc_cnt(
		.re(classd_esc_cnt_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[34-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(classd_esc_cnt_qs)
	);
	prim_subreg_ext #(.DW(3)) u_classd_state(
		.re(classd_state_re),
		.we(1'b0),
		.wd({3 {1'sb0}}),
		.d(hw2reg[2-:3]),
		.qre(),
		.qe(),
		.q(),
		.qs(classd_state_qs)
	);
	reg [58:0] addr_hit;
	always @(*) begin
		addr_hit = {59 {1'sb0}};
		addr_hit[0] = reg_addr == ALERT_HANDLER_INTR_STATE_OFFSET;
		addr_hit[1] = reg_addr == ALERT_HANDLER_INTR_ENABLE_OFFSET;
		addr_hit[2] = reg_addr == ALERT_HANDLER_INTR_TEST_OFFSET;
		addr_hit[3] = reg_addr == ALERT_HANDLER_REGEN_OFFSET;
		addr_hit[4] = reg_addr == ALERT_HANDLER_PING_TIMEOUT_CYC_OFFSET;
		addr_hit[5] = reg_addr == ALERT_HANDLER_ALERT_EN_OFFSET;
		addr_hit[6] = reg_addr == ALERT_HANDLER_ALERT_CLASS_OFFSET;
		addr_hit[7] = reg_addr == ALERT_HANDLER_ALERT_CAUSE_OFFSET;
		addr_hit[8] = reg_addr == ALERT_HANDLER_LOC_ALERT_EN_OFFSET;
		addr_hit[9] = reg_addr == ALERT_HANDLER_LOC_ALERT_CLASS_OFFSET;
		addr_hit[10] = reg_addr == ALERT_HANDLER_LOC_ALERT_CAUSE_OFFSET;
		addr_hit[11] = reg_addr == ALERT_HANDLER_CLASSA_CTRL_OFFSET;
		addr_hit[12] = reg_addr == ALERT_HANDLER_CLASSA_CLREN_OFFSET;
		addr_hit[13] = reg_addr == ALERT_HANDLER_CLASSA_CLR_OFFSET;
		addr_hit[14] = reg_addr == ALERT_HANDLER_CLASSA_ACCUM_CNT_OFFSET;
		addr_hit[15] = reg_addr == ALERT_HANDLER_CLASSA_ACCUM_THRESH_OFFSET;
		addr_hit[16] = reg_addr == ALERT_HANDLER_CLASSA_TIMEOUT_CYC_OFFSET;
		addr_hit[17] = reg_addr == ALERT_HANDLER_CLASSA_PHASE0_CYC_OFFSET;
		addr_hit[18] = reg_addr == ALERT_HANDLER_CLASSA_PHASE1_CYC_OFFSET;
		addr_hit[19] = reg_addr == ALERT_HANDLER_CLASSA_PHASE2_CYC_OFFSET;
		addr_hit[20] = reg_addr == ALERT_HANDLER_CLASSA_PHASE3_CYC_OFFSET;
		addr_hit[21] = reg_addr == ALERT_HANDLER_CLASSA_ESC_CNT_OFFSET;
		addr_hit[22] = reg_addr == ALERT_HANDLER_CLASSA_STATE_OFFSET;
		addr_hit[23] = reg_addr == ALERT_HANDLER_CLASSB_CTRL_OFFSET;
		addr_hit[24] = reg_addr == ALERT_HANDLER_CLASSB_CLREN_OFFSET;
		addr_hit[25] = reg_addr == ALERT_HANDLER_CLASSB_CLR_OFFSET;
		addr_hit[26] = reg_addr == ALERT_HANDLER_CLASSB_ACCUM_CNT_OFFSET;
		addr_hit[27] = reg_addr == ALERT_HANDLER_CLASSB_ACCUM_THRESH_OFFSET;
		addr_hit[28] = reg_addr == ALERT_HANDLER_CLASSB_TIMEOUT_CYC_OFFSET;
		addr_hit[29] = reg_addr == ALERT_HANDLER_CLASSB_PHASE0_CYC_OFFSET;
		addr_hit[30] = reg_addr == ALERT_HANDLER_CLASSB_PHASE1_CYC_OFFSET;
		addr_hit[31] = reg_addr == ALERT_HANDLER_CLASSB_PHASE2_CYC_OFFSET;
		addr_hit[32] = reg_addr == ALERT_HANDLER_CLASSB_PHASE3_CYC_OFFSET;
		addr_hit[33] = reg_addr == ALERT_HANDLER_CLASSB_ESC_CNT_OFFSET;
		addr_hit[34] = reg_addr == ALERT_HANDLER_CLASSB_STATE_OFFSET;
		addr_hit[35] = reg_addr == ALERT_HANDLER_CLASSC_CTRL_OFFSET;
		addr_hit[36] = reg_addr == ALERT_HANDLER_CLASSC_CLREN_OFFSET;
		addr_hit[37] = reg_addr == ALERT_HANDLER_CLASSC_CLR_OFFSET;
		addr_hit[38] = reg_addr == ALERT_HANDLER_CLASSC_ACCUM_CNT_OFFSET;
		addr_hit[39] = reg_addr == ALERT_HANDLER_CLASSC_ACCUM_THRESH_OFFSET;
		addr_hit[40] = reg_addr == ALERT_HANDLER_CLASSC_TIMEOUT_CYC_OFFSET;
		addr_hit[41] = reg_addr == ALERT_HANDLER_CLASSC_PHASE0_CYC_OFFSET;
		addr_hit[42] = reg_addr == ALERT_HANDLER_CLASSC_PHASE1_CYC_OFFSET;
		addr_hit[43] = reg_addr == ALERT_HANDLER_CLASSC_PHASE2_CYC_OFFSET;
		addr_hit[44] = reg_addr == ALERT_HANDLER_CLASSC_PHASE3_CYC_OFFSET;
		addr_hit[45] = reg_addr == ALERT_HANDLER_CLASSC_ESC_CNT_OFFSET;
		addr_hit[46] = reg_addr == ALERT_HANDLER_CLASSC_STATE_OFFSET;
		addr_hit[47] = reg_addr == ALERT_HANDLER_CLASSD_CTRL_OFFSET;
		addr_hit[48] = reg_addr == ALERT_HANDLER_CLASSD_CLREN_OFFSET;
		addr_hit[49] = reg_addr == ALERT_HANDLER_CLASSD_CLR_OFFSET;
		addr_hit[50] = reg_addr == ALERT_HANDLER_CLASSD_ACCUM_CNT_OFFSET;
		addr_hit[51] = reg_addr == ALERT_HANDLER_CLASSD_ACCUM_THRESH_OFFSET;
		addr_hit[52] = reg_addr == ALERT_HANDLER_CLASSD_TIMEOUT_CYC_OFFSET;
		addr_hit[53] = reg_addr == ALERT_HANDLER_CLASSD_PHASE0_CYC_OFFSET;
		addr_hit[54] = reg_addr == ALERT_HANDLER_CLASSD_PHASE1_CYC_OFFSET;
		addr_hit[55] = reg_addr == ALERT_HANDLER_CLASSD_PHASE2_CYC_OFFSET;
		addr_hit[56] = reg_addr == ALERT_HANDLER_CLASSD_PHASE3_CYC_OFFSET;
		addr_hit[57] = reg_addr == ALERT_HANDLER_CLASSD_ESC_CNT_OFFSET;
		addr_hit[58] = reg_addr == ALERT_HANDLER_CLASSD_STATE_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (ALERT_HANDLER_PERMIT[232+:4] != (ALERT_HANDLER_PERMIT[232+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (ALERT_HANDLER_PERMIT[228+:4] != (ALERT_HANDLER_PERMIT[228+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (ALERT_HANDLER_PERMIT[224+:4] != (ALERT_HANDLER_PERMIT[224+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (ALERT_HANDLER_PERMIT[220+:4] != (ALERT_HANDLER_PERMIT[220+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (ALERT_HANDLER_PERMIT[216+:4] != (ALERT_HANDLER_PERMIT[216+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (ALERT_HANDLER_PERMIT[212+:4] != (ALERT_HANDLER_PERMIT[212+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (ALERT_HANDLER_PERMIT[208+:4] != (ALERT_HANDLER_PERMIT[208+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (ALERT_HANDLER_PERMIT[204+:4] != (ALERT_HANDLER_PERMIT[204+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (ALERT_HANDLER_PERMIT[200+:4] != (ALERT_HANDLER_PERMIT[200+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (ALERT_HANDLER_PERMIT[196+:4] != (ALERT_HANDLER_PERMIT[196+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (ALERT_HANDLER_PERMIT[192+:4] != (ALERT_HANDLER_PERMIT[192+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (ALERT_HANDLER_PERMIT[188+:4] != (ALERT_HANDLER_PERMIT[188+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (ALERT_HANDLER_PERMIT[184+:4] != (ALERT_HANDLER_PERMIT[184+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (ALERT_HANDLER_PERMIT[180+:4] != (ALERT_HANDLER_PERMIT[180+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (ALERT_HANDLER_PERMIT[176+:4] != (ALERT_HANDLER_PERMIT[176+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[15] && reg_we) && (ALERT_HANDLER_PERMIT[172+:4] != (ALERT_HANDLER_PERMIT[172+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[16] && reg_we) && (ALERT_HANDLER_PERMIT[168+:4] != (ALERT_HANDLER_PERMIT[168+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[17] && reg_we) && (ALERT_HANDLER_PERMIT[164+:4] != (ALERT_HANDLER_PERMIT[164+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[18] && reg_we) && (ALERT_HANDLER_PERMIT[160+:4] != (ALERT_HANDLER_PERMIT[160+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[19] && reg_we) && (ALERT_HANDLER_PERMIT[156+:4] != (ALERT_HANDLER_PERMIT[156+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[20] && reg_we) && (ALERT_HANDLER_PERMIT[152+:4] != (ALERT_HANDLER_PERMIT[152+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[21] && reg_we) && (ALERT_HANDLER_PERMIT[148+:4] != (ALERT_HANDLER_PERMIT[148+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[22] && reg_we) && (ALERT_HANDLER_PERMIT[144+:4] != (ALERT_HANDLER_PERMIT[144+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[23] && reg_we) && (ALERT_HANDLER_PERMIT[140+:4] != (ALERT_HANDLER_PERMIT[140+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[24] && reg_we) && (ALERT_HANDLER_PERMIT[136+:4] != (ALERT_HANDLER_PERMIT[136+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[25] && reg_we) && (ALERT_HANDLER_PERMIT[132+:4] != (ALERT_HANDLER_PERMIT[132+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[26] && reg_we) && (ALERT_HANDLER_PERMIT[128+:4] != (ALERT_HANDLER_PERMIT[128+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[27] && reg_we) && (ALERT_HANDLER_PERMIT[124+:4] != (ALERT_HANDLER_PERMIT[124+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[28] && reg_we) && (ALERT_HANDLER_PERMIT[120+:4] != (ALERT_HANDLER_PERMIT[120+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[29] && reg_we) && (ALERT_HANDLER_PERMIT[116+:4] != (ALERT_HANDLER_PERMIT[116+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[30] && reg_we) && (ALERT_HANDLER_PERMIT[112+:4] != (ALERT_HANDLER_PERMIT[112+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[31] && reg_we) && (ALERT_HANDLER_PERMIT[108+:4] != (ALERT_HANDLER_PERMIT[108+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[32] && reg_we) && (ALERT_HANDLER_PERMIT[104+:4] != (ALERT_HANDLER_PERMIT[104+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[33] && reg_we) && (ALERT_HANDLER_PERMIT[100+:4] != (ALERT_HANDLER_PERMIT[100+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[34] && reg_we) && (ALERT_HANDLER_PERMIT[96+:4] != (ALERT_HANDLER_PERMIT[96+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[35] && reg_we) && (ALERT_HANDLER_PERMIT[92+:4] != (ALERT_HANDLER_PERMIT[92+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[36] && reg_we) && (ALERT_HANDLER_PERMIT[88+:4] != (ALERT_HANDLER_PERMIT[88+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[37] && reg_we) && (ALERT_HANDLER_PERMIT[84+:4] != (ALERT_HANDLER_PERMIT[84+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[38] && reg_we) && (ALERT_HANDLER_PERMIT[80+:4] != (ALERT_HANDLER_PERMIT[80+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[39] && reg_we) && (ALERT_HANDLER_PERMIT[76+:4] != (ALERT_HANDLER_PERMIT[76+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[40] && reg_we) && (ALERT_HANDLER_PERMIT[72+:4] != (ALERT_HANDLER_PERMIT[72+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[41] && reg_we) && (ALERT_HANDLER_PERMIT[68+:4] != (ALERT_HANDLER_PERMIT[68+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[42] && reg_we) && (ALERT_HANDLER_PERMIT[64+:4] != (ALERT_HANDLER_PERMIT[64+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[43] && reg_we) && (ALERT_HANDLER_PERMIT[60+:4] != (ALERT_HANDLER_PERMIT[60+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[44] && reg_we) && (ALERT_HANDLER_PERMIT[56+:4] != (ALERT_HANDLER_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[45] && reg_we) && (ALERT_HANDLER_PERMIT[52+:4] != (ALERT_HANDLER_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[46] && reg_we) && (ALERT_HANDLER_PERMIT[48+:4] != (ALERT_HANDLER_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[47] && reg_we) && (ALERT_HANDLER_PERMIT[44+:4] != (ALERT_HANDLER_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[48] && reg_we) && (ALERT_HANDLER_PERMIT[40+:4] != (ALERT_HANDLER_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[49] && reg_we) && (ALERT_HANDLER_PERMIT[36+:4] != (ALERT_HANDLER_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[50] && reg_we) && (ALERT_HANDLER_PERMIT[32+:4] != (ALERT_HANDLER_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[51] && reg_we) && (ALERT_HANDLER_PERMIT[28+:4] != (ALERT_HANDLER_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[52] && reg_we) && (ALERT_HANDLER_PERMIT[24+:4] != (ALERT_HANDLER_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[53] && reg_we) && (ALERT_HANDLER_PERMIT[20+:4] != (ALERT_HANDLER_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[54] && reg_we) && (ALERT_HANDLER_PERMIT[16+:4] != (ALERT_HANDLER_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[55] && reg_we) && (ALERT_HANDLER_PERMIT[12+:4] != (ALERT_HANDLER_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[56] && reg_we) && (ALERT_HANDLER_PERMIT[8+:4] != (ALERT_HANDLER_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[57] && reg_we) && (ALERT_HANDLER_PERMIT[4+:4] != (ALERT_HANDLER_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[58] && reg_we) && (ALERT_HANDLER_PERMIT[0+:4] != (ALERT_HANDLER_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign intr_state_classa_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_classa_wd = reg_wdata[0];
	assign intr_state_classb_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_classb_wd = reg_wdata[1];
	assign intr_state_classc_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_classc_wd = reg_wdata[2];
	assign intr_state_classd_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_classd_wd = reg_wdata[3];
	assign intr_enable_classa_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_classa_wd = reg_wdata[0];
	assign intr_enable_classb_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_classb_wd = reg_wdata[1];
	assign intr_enable_classc_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_classc_wd = reg_wdata[2];
	assign intr_enable_classd_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_classd_wd = reg_wdata[3];
	assign intr_test_classa_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_classa_wd = reg_wdata[0];
	assign intr_test_classb_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_classb_wd = reg_wdata[1];
	assign intr_test_classc_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_classc_wd = reg_wdata[2];
	assign intr_test_classd_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_classd_wd = reg_wdata[3];
	assign regen_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign regen_wd = reg_wdata[0];
	assign ping_timeout_cyc_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign ping_timeout_cyc_wd = reg_wdata[23:0];
	assign alert_en_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign alert_en_wd = reg_wdata[0];
	assign alert_class_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign alert_class_wd = reg_wdata[1:0];
	assign alert_cause_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign alert_cause_wd = reg_wdata[0];
	assign loc_alert_en_en_la0_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign loc_alert_en_en_la0_wd = reg_wdata[0];
	assign loc_alert_en_en_la1_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign loc_alert_en_en_la1_wd = reg_wdata[1];
	assign loc_alert_en_en_la2_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign loc_alert_en_en_la2_wd = reg_wdata[2];
	assign loc_alert_en_en_la3_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign loc_alert_en_en_la3_wd = reg_wdata[3];
	assign loc_alert_class_class_la0_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign loc_alert_class_class_la0_wd = reg_wdata[1:0];
	assign loc_alert_class_class_la1_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign loc_alert_class_class_la1_wd = reg_wdata[3:2];
	assign loc_alert_class_class_la2_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign loc_alert_class_class_la2_wd = reg_wdata[5:4];
	assign loc_alert_class_class_la3_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign loc_alert_class_class_la3_wd = reg_wdata[7:6];
	assign loc_alert_cause_la0_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign loc_alert_cause_la0_wd = reg_wdata[0];
	assign loc_alert_cause_la1_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign loc_alert_cause_la1_wd = reg_wdata[1];
	assign loc_alert_cause_la2_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign loc_alert_cause_la2_wd = reg_wdata[2];
	assign loc_alert_cause_la3_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign loc_alert_cause_la3_wd = reg_wdata[3];
	assign classa_ctrl_en_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_en_wd = reg_wdata[0];
	assign classa_ctrl_lock_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_lock_wd = reg_wdata[1];
	assign classa_ctrl_en_e0_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_en_e0_wd = reg_wdata[2];
	assign classa_ctrl_en_e1_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_en_e1_wd = reg_wdata[3];
	assign classa_ctrl_en_e2_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_en_e2_wd = reg_wdata[4];
	assign classa_ctrl_en_e3_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_en_e3_wd = reg_wdata[5];
	assign classa_ctrl_map_e0_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_map_e0_wd = reg_wdata[7:6];
	assign classa_ctrl_map_e1_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_map_e1_wd = reg_wdata[9:8];
	assign classa_ctrl_map_e2_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_map_e2_wd = reg_wdata[11:10];
	assign classa_ctrl_map_e3_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign classa_ctrl_map_e3_wd = reg_wdata[13:12];
	assign classa_clren_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign classa_clren_wd = reg_wdata[0];
	assign classa_clr_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign classa_clr_wd = reg_wdata[0];
	assign classa_accum_cnt_re = addr_hit[14] && reg_re;
	assign classa_accum_thresh_we = (addr_hit[15] & reg_we) & ~wr_err;
	assign classa_accum_thresh_wd = reg_wdata[15:0];
	assign classa_timeout_cyc_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign classa_timeout_cyc_wd = reg_wdata[31:0];
	assign classa_phase0_cyc_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign classa_phase0_cyc_wd = reg_wdata[31:0];
	assign classa_phase1_cyc_we = (addr_hit[18] & reg_we) & ~wr_err;
	assign classa_phase1_cyc_wd = reg_wdata[31:0];
	assign classa_phase2_cyc_we = (addr_hit[19] & reg_we) & ~wr_err;
	assign classa_phase2_cyc_wd = reg_wdata[31:0];
	assign classa_phase3_cyc_we = (addr_hit[20] & reg_we) & ~wr_err;
	assign classa_phase3_cyc_wd = reg_wdata[31:0];
	assign classa_esc_cnt_re = addr_hit[21] && reg_re;
	assign classa_state_re = addr_hit[22] && reg_re;
	assign classb_ctrl_en_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_en_wd = reg_wdata[0];
	assign classb_ctrl_lock_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_lock_wd = reg_wdata[1];
	assign classb_ctrl_en_e0_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_en_e0_wd = reg_wdata[2];
	assign classb_ctrl_en_e1_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_en_e1_wd = reg_wdata[3];
	assign classb_ctrl_en_e2_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_en_e2_wd = reg_wdata[4];
	assign classb_ctrl_en_e3_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_en_e3_wd = reg_wdata[5];
	assign classb_ctrl_map_e0_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_map_e0_wd = reg_wdata[7:6];
	assign classb_ctrl_map_e1_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_map_e1_wd = reg_wdata[9:8];
	assign classb_ctrl_map_e2_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_map_e2_wd = reg_wdata[11:10];
	assign classb_ctrl_map_e3_we = (addr_hit[23] & reg_we) & ~wr_err;
	assign classb_ctrl_map_e3_wd = reg_wdata[13:12];
	assign classb_clren_we = (addr_hit[24] & reg_we) & ~wr_err;
	assign classb_clren_wd = reg_wdata[0];
	assign classb_clr_we = (addr_hit[25] & reg_we) & ~wr_err;
	assign classb_clr_wd = reg_wdata[0];
	assign classb_accum_cnt_re = addr_hit[26] && reg_re;
	assign classb_accum_thresh_we = (addr_hit[27] & reg_we) & ~wr_err;
	assign classb_accum_thresh_wd = reg_wdata[15:0];
	assign classb_timeout_cyc_we = (addr_hit[28] & reg_we) & ~wr_err;
	assign classb_timeout_cyc_wd = reg_wdata[31:0];
	assign classb_phase0_cyc_we = (addr_hit[29] & reg_we) & ~wr_err;
	assign classb_phase0_cyc_wd = reg_wdata[31:0];
	assign classb_phase1_cyc_we = (addr_hit[30] & reg_we) & ~wr_err;
	assign classb_phase1_cyc_wd = reg_wdata[31:0];
	assign classb_phase2_cyc_we = (addr_hit[31] & reg_we) & ~wr_err;
	assign classb_phase2_cyc_wd = reg_wdata[31:0];
	assign classb_phase3_cyc_we = (addr_hit[32] & reg_we) & ~wr_err;
	assign classb_phase3_cyc_wd = reg_wdata[31:0];
	assign classb_esc_cnt_re = addr_hit[33] && reg_re;
	assign classb_state_re = addr_hit[34] && reg_re;
	assign classc_ctrl_en_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_en_wd = reg_wdata[0];
	assign classc_ctrl_lock_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_lock_wd = reg_wdata[1];
	assign classc_ctrl_en_e0_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_en_e0_wd = reg_wdata[2];
	assign classc_ctrl_en_e1_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_en_e1_wd = reg_wdata[3];
	assign classc_ctrl_en_e2_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_en_e2_wd = reg_wdata[4];
	assign classc_ctrl_en_e3_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_en_e3_wd = reg_wdata[5];
	assign classc_ctrl_map_e0_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_map_e0_wd = reg_wdata[7:6];
	assign classc_ctrl_map_e1_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_map_e1_wd = reg_wdata[9:8];
	assign classc_ctrl_map_e2_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_map_e2_wd = reg_wdata[11:10];
	assign classc_ctrl_map_e3_we = (addr_hit[35] & reg_we) & ~wr_err;
	assign classc_ctrl_map_e3_wd = reg_wdata[13:12];
	assign classc_clren_we = (addr_hit[36] & reg_we) & ~wr_err;
	assign classc_clren_wd = reg_wdata[0];
	assign classc_clr_we = (addr_hit[37] & reg_we) & ~wr_err;
	assign classc_clr_wd = reg_wdata[0];
	assign classc_accum_cnt_re = addr_hit[38] && reg_re;
	assign classc_accum_thresh_we = (addr_hit[39] & reg_we) & ~wr_err;
	assign classc_accum_thresh_wd = reg_wdata[15:0];
	assign classc_timeout_cyc_we = (addr_hit[40] & reg_we) & ~wr_err;
	assign classc_timeout_cyc_wd = reg_wdata[31:0];
	assign classc_phase0_cyc_we = (addr_hit[41] & reg_we) & ~wr_err;
	assign classc_phase0_cyc_wd = reg_wdata[31:0];
	assign classc_phase1_cyc_we = (addr_hit[42] & reg_we) & ~wr_err;
	assign classc_phase1_cyc_wd = reg_wdata[31:0];
	assign classc_phase2_cyc_we = (addr_hit[43] & reg_we) & ~wr_err;
	assign classc_phase2_cyc_wd = reg_wdata[31:0];
	assign classc_phase3_cyc_we = (addr_hit[44] & reg_we) & ~wr_err;
	assign classc_phase3_cyc_wd = reg_wdata[31:0];
	assign classc_esc_cnt_re = addr_hit[45] && reg_re;
	assign classc_state_re = addr_hit[46] && reg_re;
	assign classd_ctrl_en_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_en_wd = reg_wdata[0];
	assign classd_ctrl_lock_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_lock_wd = reg_wdata[1];
	assign classd_ctrl_en_e0_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_en_e0_wd = reg_wdata[2];
	assign classd_ctrl_en_e1_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_en_e1_wd = reg_wdata[3];
	assign classd_ctrl_en_e2_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_en_e2_wd = reg_wdata[4];
	assign classd_ctrl_en_e3_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_en_e3_wd = reg_wdata[5];
	assign classd_ctrl_map_e0_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_map_e0_wd = reg_wdata[7:6];
	assign classd_ctrl_map_e1_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_map_e1_wd = reg_wdata[9:8];
	assign classd_ctrl_map_e2_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_map_e2_wd = reg_wdata[11:10];
	assign classd_ctrl_map_e3_we = (addr_hit[47] & reg_we) & ~wr_err;
	assign classd_ctrl_map_e3_wd = reg_wdata[13:12];
	assign classd_clren_we = (addr_hit[48] & reg_we) & ~wr_err;
	assign classd_clren_wd = reg_wdata[0];
	assign classd_clr_we = (addr_hit[49] & reg_we) & ~wr_err;
	assign classd_clr_wd = reg_wdata[0];
	assign classd_accum_cnt_re = addr_hit[50] && reg_re;
	assign classd_accum_thresh_we = (addr_hit[51] & reg_we) & ~wr_err;
	assign classd_accum_thresh_wd = reg_wdata[15:0];
	assign classd_timeout_cyc_we = (addr_hit[52] & reg_we) & ~wr_err;
	assign classd_timeout_cyc_wd = reg_wdata[31:0];
	assign classd_phase0_cyc_we = (addr_hit[53] & reg_we) & ~wr_err;
	assign classd_phase0_cyc_wd = reg_wdata[31:0];
	assign classd_phase1_cyc_we = (addr_hit[54] & reg_we) & ~wr_err;
	assign classd_phase1_cyc_wd = reg_wdata[31:0];
	assign classd_phase2_cyc_we = (addr_hit[55] & reg_we) & ~wr_err;
	assign classd_phase2_cyc_wd = reg_wdata[31:0];
	assign classd_phase3_cyc_we = (addr_hit[56] & reg_we) & ~wr_err;
	assign classd_phase3_cyc_wd = reg_wdata[31:0];
	assign classd_esc_cnt_re = addr_hit[57] && reg_re;
	assign classd_state_re = addr_hit[58] && reg_re;
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: begin
				reg_rdata_next[0] = intr_state_classa_qs;
				reg_rdata_next[1] = intr_state_classb_qs;
				reg_rdata_next[2] = intr_state_classc_qs;
				reg_rdata_next[3] = intr_state_classd_qs;
			end
			addr_hit[1]: begin
				reg_rdata_next[0] = intr_enable_classa_qs;
				reg_rdata_next[1] = intr_enable_classb_qs;
				reg_rdata_next[2] = intr_enable_classc_qs;
				reg_rdata_next[3] = intr_enable_classd_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[2] = 1'sb0;
				reg_rdata_next[3] = 1'sb0;
			end
			addr_hit[3]: reg_rdata_next[0] = regen_qs;
			addr_hit[4]: reg_rdata_next[23:0] = ping_timeout_cyc_qs;
			addr_hit[5]: reg_rdata_next[0] = alert_en_qs;
			addr_hit[6]: reg_rdata_next[1:0] = alert_class_qs;
			addr_hit[7]: reg_rdata_next[0] = alert_cause_qs;
			addr_hit[8]: begin
				reg_rdata_next[0] = loc_alert_en_en_la0_qs;
				reg_rdata_next[1] = loc_alert_en_en_la1_qs;
				reg_rdata_next[2] = loc_alert_en_en_la2_qs;
				reg_rdata_next[3] = loc_alert_en_en_la3_qs;
			end
			addr_hit[9]: begin
				reg_rdata_next[1:0] = loc_alert_class_class_la0_qs;
				reg_rdata_next[3:2] = loc_alert_class_class_la1_qs;
				reg_rdata_next[5:4] = loc_alert_class_class_la2_qs;
				reg_rdata_next[7:6] = loc_alert_class_class_la3_qs;
			end
			addr_hit[10]: begin
				reg_rdata_next[0] = loc_alert_cause_la0_qs;
				reg_rdata_next[1] = loc_alert_cause_la1_qs;
				reg_rdata_next[2] = loc_alert_cause_la2_qs;
				reg_rdata_next[3] = loc_alert_cause_la3_qs;
			end
			addr_hit[11]: begin
				reg_rdata_next[0] = classa_ctrl_en_qs;
				reg_rdata_next[1] = classa_ctrl_lock_qs;
				reg_rdata_next[2] = classa_ctrl_en_e0_qs;
				reg_rdata_next[3] = classa_ctrl_en_e1_qs;
				reg_rdata_next[4] = classa_ctrl_en_e2_qs;
				reg_rdata_next[5] = classa_ctrl_en_e3_qs;
				reg_rdata_next[7:6] = classa_ctrl_map_e0_qs;
				reg_rdata_next[9:8] = classa_ctrl_map_e1_qs;
				reg_rdata_next[11:10] = classa_ctrl_map_e2_qs;
				reg_rdata_next[13:12] = classa_ctrl_map_e3_qs;
			end
			addr_hit[12]: reg_rdata_next[0] = classa_clren_qs;
			addr_hit[13]: reg_rdata_next[0] = 1'sb0;
			addr_hit[14]: reg_rdata_next[15:0] = classa_accum_cnt_qs;
			addr_hit[15]: reg_rdata_next[15:0] = classa_accum_thresh_qs;
			addr_hit[16]: reg_rdata_next[31:0] = classa_timeout_cyc_qs;
			addr_hit[17]: reg_rdata_next[31:0] = classa_phase0_cyc_qs;
			addr_hit[18]: reg_rdata_next[31:0] = classa_phase1_cyc_qs;
			addr_hit[19]: reg_rdata_next[31:0] = classa_phase2_cyc_qs;
			addr_hit[20]: reg_rdata_next[31:0] = classa_phase3_cyc_qs;
			addr_hit[21]: reg_rdata_next[31:0] = classa_esc_cnt_qs;
			addr_hit[22]: reg_rdata_next[2:0] = classa_state_qs;
			addr_hit[23]: begin
				reg_rdata_next[0] = classb_ctrl_en_qs;
				reg_rdata_next[1] = classb_ctrl_lock_qs;
				reg_rdata_next[2] = classb_ctrl_en_e0_qs;
				reg_rdata_next[3] = classb_ctrl_en_e1_qs;
				reg_rdata_next[4] = classb_ctrl_en_e2_qs;
				reg_rdata_next[5] = classb_ctrl_en_e3_qs;
				reg_rdata_next[7:6] = classb_ctrl_map_e0_qs;
				reg_rdata_next[9:8] = classb_ctrl_map_e1_qs;
				reg_rdata_next[11:10] = classb_ctrl_map_e2_qs;
				reg_rdata_next[13:12] = classb_ctrl_map_e3_qs;
			end
			addr_hit[24]: reg_rdata_next[0] = classb_clren_qs;
			addr_hit[25]: reg_rdata_next[0] = 1'sb0;
			addr_hit[26]: reg_rdata_next[15:0] = classb_accum_cnt_qs;
			addr_hit[27]: reg_rdata_next[15:0] = classb_accum_thresh_qs;
			addr_hit[28]: reg_rdata_next[31:0] = classb_timeout_cyc_qs;
			addr_hit[29]: reg_rdata_next[31:0] = classb_phase0_cyc_qs;
			addr_hit[30]: reg_rdata_next[31:0] = classb_phase1_cyc_qs;
			addr_hit[31]: reg_rdata_next[31:0] = classb_phase2_cyc_qs;
			addr_hit[32]: reg_rdata_next[31:0] = classb_phase3_cyc_qs;
			addr_hit[33]: reg_rdata_next[31:0] = classb_esc_cnt_qs;
			addr_hit[34]: reg_rdata_next[2:0] = classb_state_qs;
			addr_hit[35]: begin
				reg_rdata_next[0] = classc_ctrl_en_qs;
				reg_rdata_next[1] = classc_ctrl_lock_qs;
				reg_rdata_next[2] = classc_ctrl_en_e0_qs;
				reg_rdata_next[3] = classc_ctrl_en_e1_qs;
				reg_rdata_next[4] = classc_ctrl_en_e2_qs;
				reg_rdata_next[5] = classc_ctrl_en_e3_qs;
				reg_rdata_next[7:6] = classc_ctrl_map_e0_qs;
				reg_rdata_next[9:8] = classc_ctrl_map_e1_qs;
				reg_rdata_next[11:10] = classc_ctrl_map_e2_qs;
				reg_rdata_next[13:12] = classc_ctrl_map_e3_qs;
			end
			addr_hit[36]: reg_rdata_next[0] = classc_clren_qs;
			addr_hit[37]: reg_rdata_next[0] = 1'sb0;
			addr_hit[38]: reg_rdata_next[15:0] = classc_accum_cnt_qs;
			addr_hit[39]: reg_rdata_next[15:0] = classc_accum_thresh_qs;
			addr_hit[40]: reg_rdata_next[31:0] = classc_timeout_cyc_qs;
			addr_hit[41]: reg_rdata_next[31:0] = classc_phase0_cyc_qs;
			addr_hit[42]: reg_rdata_next[31:0] = classc_phase1_cyc_qs;
			addr_hit[43]: reg_rdata_next[31:0] = classc_phase2_cyc_qs;
			addr_hit[44]: reg_rdata_next[31:0] = classc_phase3_cyc_qs;
			addr_hit[45]: reg_rdata_next[31:0] = classc_esc_cnt_qs;
			addr_hit[46]: reg_rdata_next[2:0] = classc_state_qs;
			addr_hit[47]: begin
				reg_rdata_next[0] = classd_ctrl_en_qs;
				reg_rdata_next[1] = classd_ctrl_lock_qs;
				reg_rdata_next[2] = classd_ctrl_en_e0_qs;
				reg_rdata_next[3] = classd_ctrl_en_e1_qs;
				reg_rdata_next[4] = classd_ctrl_en_e2_qs;
				reg_rdata_next[5] = classd_ctrl_en_e3_qs;
				reg_rdata_next[7:6] = classd_ctrl_map_e0_qs;
				reg_rdata_next[9:8] = classd_ctrl_map_e1_qs;
				reg_rdata_next[11:10] = classd_ctrl_map_e2_qs;
				reg_rdata_next[13:12] = classd_ctrl_map_e3_qs;
			end
			addr_hit[48]: reg_rdata_next[0] = classd_clren_qs;
			addr_hit[49]: reg_rdata_next[0] = 1'sb0;
			addr_hit[50]: reg_rdata_next[15:0] = classd_accum_cnt_qs;
			addr_hit[51]: reg_rdata_next[15:0] = classd_accum_thresh_qs;
			addr_hit[52]: reg_rdata_next[31:0] = classd_timeout_cyc_qs;
			addr_hit[53]: reg_rdata_next[31:0] = classd_phase0_cyc_qs;
			addr_hit[54]: reg_rdata_next[31:0] = classd_phase1_cyc_qs;
			addr_hit[55]: reg_rdata_next[31:0] = classd_phase2_cyc_qs;
			addr_hit[56]: reg_rdata_next[31:0] = classd_phase3_cyc_qs;
			addr_hit[57]: reg_rdata_next[31:0] = classd_esc_cnt_qs;
			addr_hit[58]: reg_rdata_next[2:0] = classd_state_qs;
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module alert_handler_reg_wrap (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	irq_o,
	crashdump_o,
	hw2reg_wrap,
	reg2hw_wrap
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [N_CLASSES - 1:0] irq_o;
	output wire [((((NAlerts + N_LOC_ALERT) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + (N_CLASSES * 3)) - 1:0] crashdump_o;
	input wire [((((((NAlerts + N_LOC_ALERT) + N_CLASSES) + N_CLASSES) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + (N_CLASSES * 3)) - 1:0] hw2reg_wrap;
	output wire [((((((((((((1 + PING_CNT_DW) + N_LOC_ALERT) + (N_LOC_ALERT * CLASS_DW)) + NAlerts) + (NAlerts * CLASS_DW)) + N_CLASSES) + N_CLASSES) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + ((N_CLASSES * N_PHASES) * EscCntDw)) + (N_CLASSES * N_ESC_SEV)) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)) - 1:0] reg2hw_wrap;
	wire [N_CLASSES - 1:0] class_autolock_en;
	wire [828:0] reg2hw;
	wire [229:0] hw2reg;
	alert_handler_reg_top i_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	prim_intr_hw #(.Width(1)) i_irq_classa(
		.event_intr_i(hw2reg_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - (N_CLASSES - 1)]),
		.reg2hw_intr_enable_q_i(reg2hw[824]),
		.reg2hw_intr_test_q_i(reg2hw[820]),
		.reg2hw_intr_test_qe_i(reg2hw[819]),
		.reg2hw_intr_state_q_i(reg2hw[828]),
		.hw2reg_intr_state_de_o(hw2reg[228]),
		.hw2reg_intr_state_d_o(hw2reg[229]),
		.intr_o(irq_o[0])
	);
	prim_intr_hw #(.Width(1)) i_irq_classb(
		.event_intr_i(hw2reg_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - (N_CLASSES - 2)]),
		.reg2hw_intr_enable_q_i(reg2hw[823]),
		.reg2hw_intr_test_q_i(reg2hw[818]),
		.reg2hw_intr_test_qe_i(reg2hw[817]),
		.reg2hw_intr_state_q_i(reg2hw[827]),
		.hw2reg_intr_state_de_o(hw2reg[226]),
		.hw2reg_intr_state_d_o(hw2reg[227]),
		.intr_o(irq_o[1])
	);
	prim_intr_hw #(.Width(1)) i_irq_classc(
		.event_intr_i(hw2reg_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - (N_CLASSES - 3)]),
		.reg2hw_intr_enable_q_i(reg2hw[822]),
		.reg2hw_intr_test_q_i(reg2hw[816]),
		.reg2hw_intr_test_qe_i(reg2hw[815]),
		.reg2hw_intr_state_q_i(reg2hw[826]),
		.hw2reg_intr_state_de_o(hw2reg[224]),
		.hw2reg_intr_state_d_o(hw2reg[225]),
		.intr_o(irq_o[2])
	);
	prim_intr_hw #(.Width(1)) i_irq_classd(
		.event_intr_i(hw2reg_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - (N_CLASSES - 4)]),
		.reg2hw_intr_enable_q_i(reg2hw[821]),
		.reg2hw_intr_test_q_i(reg2hw[814]),
		.reg2hw_intr_test_qe_i(reg2hw[813]),
		.reg2hw_intr_state_q_i(reg2hw[825]),
		.hw2reg_intr_state_de_o(hw2reg[222]),
		.hw2reg_intr_state_d_o(hw2reg[223]),
		.intr_o(irq_o[3])
	);
	generate
		genvar k;
		for (k = 0; k < NAlerts; k = k + 1) begin : gen_alert_cause
			assign hw2reg[220 + ((k * 2) + 1)] = 1'b1;
			assign hw2reg[220 + (k * 2)] = reg2hw[784 + k] | hw2reg_wrap[(NAlerts + (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))))) - ((NAlerts - 1) - k)];
		end
	endgenerate
	generate
		for (k = 0; k < N_LOC_ALERT; k = k + 1) begin : gen_loc_alert_cause
			assign hw2reg[212 + ((k * 2) + 1)] = 1'b1;
			assign hw2reg[212 + (k * 2)] = reg2hw[768 + k] | hw2reg_wrap[(N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))) - ((N_LOC_ALERT - 1) - k)];
		end
	endgenerate
	assign {hw2reg[52], hw2reg[105], hw2reg[158], hw2reg[211]} = {4 {1'sb0}};
	assign {hw2reg[51], hw2reg[104], hw2reg[157], hw2reg[210]} = (hw2reg_wrap[N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))-:((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) >= ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))) ? ((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))) + 1 : (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))) - (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) + 1)] & class_autolock_en) & reg2hw_wrap[N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))-:((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) >= (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))) ? ((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) - (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))) + 1 : ((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))) - (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))) + 1)];
	assign {hw2reg[50-:16], hw2reg[103-:16], hw2reg[156-:16], hw2reg[209-:16]} = hw2reg_wrap[(N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))-:(((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) >= ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) ? (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) - ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))) + 1 : (((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) + 1)];
	assign {hw2reg[34-:32], hw2reg[87-:32], hw2reg[140-:32], hw2reg[193-:32]} = hw2reg_wrap[(N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)-:(((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) >= (N_CLASSES * 3) ? (((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) - (N_CLASSES * 3)) + 1 : ((N_CLASSES * 3) - ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) + 1)];
	assign {hw2reg[2-:3], hw2reg[55-:3], hw2reg[108-:3], hw2reg[161-:3]} = hw2reg_wrap[(N_CLASSES * 3) - 1-:N_CLASSES * 3];
	assign reg2hw_wrap[1 + (PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))))] = ~reg2hw[812];
	generate
		for (k = 0; k < NAlerts; k = k + 1) begin : gen_alert_en_class
			assign reg2hw_wrap[(NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) - ((NAlerts - 1) - k)] = reg2hw[787 + k];
			assign reg2hw_wrap[((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))) - (((NAlerts * CLASS_DW) - 1) - (k * CLASS_DW))+:CLASS_DW] = reg2hw[785 + ((k * 2) + 1)-:2];
		end
	endgenerate
	generate
		for (k = 0; k < N_LOC_ALERT; k = k + 1) begin : gen_loc_alert_en_class
			assign reg2hw_wrap[(N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))) - ((N_LOC_ALERT - 1) - k)] = reg2hw[780 + k];
			assign reg2hw_wrap[((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) - (((N_LOC_ALERT * CLASS_DW) - 1) - (k * CLASS_DW))+:CLASS_DW] = reg2hw[772 + ((k * 2) + 1)-:2];
		end
	endgenerate
	assign reg2hw_wrap[PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))-:((PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))) >= (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))))) ? ((PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))) - (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))))))) + 1 : ((N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))))) - (PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))))) + 1)] = reg2hw[811-:24];
	assign reg2hw_wrap[N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))-:((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) >= (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))) ? ((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) - (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))) + 1 : ((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))) - (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))) + 1)] = {reg2hw[191], reg2hw[383], reg2hw[575], reg2hw[767]};
	assign class_autolock_en = {reg2hw[190], reg2hw[382], reg2hw[574], reg2hw[766]};
	assign reg2hw_wrap[(N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)-:(((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)) >= ((N_CLASSES * N_ESC_SEV) * PHASE_DW) ? (((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)) - ((N_CLASSES * N_ESC_SEV) * PHASE_DW)) + 1 : (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))) + 1)] = {reg2hw[186], reg2hw[187], reg2hw[188], reg2hw[189], reg2hw[378], reg2hw[379], reg2hw[380], reg2hw[381], reg2hw[570], reg2hw[571], reg2hw[572], reg2hw[573], reg2hw[762], reg2hw[763], reg2hw[764], reg2hw[765]};
	assign reg2hw_wrap[((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1-:(N_CLASSES * N_ESC_SEV) * PHASE_DW] = {reg2hw[179-:2], reg2hw[181-:2], reg2hw[183-:2], reg2hw[185-:2], reg2hw[371-:2], reg2hw[373-:2], reg2hw[375-:2], reg2hw[377-:2], reg2hw[563-:2], reg2hw[565-:2], reg2hw[567-:2], reg2hw[569-:2], reg2hw[755-:2], reg2hw[757-:2], reg2hw[759-:2], reg2hw[761-:2]};
	assign reg2hw_wrap[N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))-:((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))) >= ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))) ? ((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))) + 1 : (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))) - (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) + 1)] = {reg2hw[177] & reg2hw[176], reg2hw[369] & reg2hw[368], reg2hw[561] & reg2hw[560], reg2hw[753] & reg2hw[752]};
	assign reg2hw_wrap[(N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))-:(((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))) >= ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))) ? (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))) - ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))) + 1 : (((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))) + 1)] = {reg2hw[175-:16], reg2hw[367-:16], reg2hw[559-:16], reg2hw[751-:16]};
	assign reg2hw_wrap[(N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))-:(((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))) >= (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))) ? (((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))) - (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))) + 1 : ((((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))) - ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))) + 1)] = {reg2hw[159-:32], reg2hw[351-:32], reg2hw[543-:32], reg2hw[735-:32]};
	assign reg2hw_wrap[((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))-:((((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))) >= ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)) ? ((((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))) - ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))) + 1 : (((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)) - (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))) + 1)] = {reg2hw[31-:32], reg2hw[63-:32], reg2hw[95-:32], reg2hw[127-:32], reg2hw[223-:32], reg2hw[255-:32], reg2hw[287-:32], reg2hw[319-:32], reg2hw[415-:32], reg2hw[447-:32], reg2hw[479-:32], reg2hw[511-:32], reg2hw[607-:32], reg2hw[639-:32], reg2hw[671-:32], reg2hw[703-:32]};
	generate
		for (k = 0; k < NAlerts; k = k + 1) begin : gen_alert_cause_dump
			assign crashdump_o[(NAlerts + (N_LOC_ALERT + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - ((NAlerts - 1) - k)] = reg2hw[784 + k];
		end
	endgenerate
	generate
		for (k = 0; k < N_LOC_ALERT; k = k + 1) begin : gen_loc_alert_cause_dump
			assign crashdump_o[(N_LOC_ALERT + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) - ((N_LOC_ALERT - 1) - k)] = reg2hw[768 + k];
		end
	endgenerate
	assign crashdump_o[(N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))-:(((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) >= ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) ? (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) - ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))) + 1 : (((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) + 1)] = hw2reg_wrap[(N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))-:(((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) >= ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) ? (((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) - ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))) + 1 : (((N_CLASSES * EscCntDw) + (N_CLASSES * 3)) - ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) + 1)];
	assign crashdump_o[(N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)-:(((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) >= (N_CLASSES * 3) ? (((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) - (N_CLASSES * 3)) + 1 : ((N_CLASSES * 3) - ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) + 1)] = hw2reg_wrap[(N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)-:(((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) >= (N_CLASSES * 3) ? (((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) - (N_CLASSES * 3)) + 1 : ((N_CLASSES * 3) - ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) + 1)];
	assign crashdump_o[(N_CLASSES * 3) - 1-:N_CLASSES * 3] = hw2reg_wrap[(N_CLASSES * 3) - 1-:N_CLASSES * 3];
endmodule
module alert_handler_class (
	alert_trig_i,
	loc_alert_trig_i,
	alert_en_i,
	loc_alert_en_i,
	alert_class_i,
	loc_alert_class_i,
	alert_cause_o,
	loc_alert_cause_o,
	class_trig_o
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	input [NAlerts - 1:0] alert_trig_i;
	input [N_LOC_ALERT - 1:0] loc_alert_trig_i;
	input [NAlerts - 1:0] alert_en_i;
	input [N_LOC_ALERT - 1:0] loc_alert_en_i;
	input [(NAlerts * CLASS_DW) - 1:0] alert_class_i;
	input [(N_LOC_ALERT * CLASS_DW) - 1:0] loc_alert_class_i;
	output wire [NAlerts - 1:0] alert_cause_o;
	output wire [N_LOC_ALERT - 1:0] loc_alert_cause_o;
	output wire [N_CLASSES - 1:0] class_trig_o;
	assign alert_cause_o = alert_en_i & alert_trig_i;
	assign loc_alert_cause_o = loc_alert_en_i & loc_alert_trig_i;
	reg [(N_CLASSES * NAlerts) - 1:0] class_masks;
	reg [(N_CLASSES * N_LOC_ALERT) - 1:0] loc_class_masks;
	always @(*) begin : p_class_mask
		class_masks = {N_CLASSES * NAlerts {1'sb0}};
		loc_class_masks = {N_CLASSES * N_LOC_ALERT {1'sb0}};
		begin : sv2v_autoblock_173
			reg [31:0] kk;
			for (kk = 0; kk < NAlerts; kk = kk + 1)
				class_masks[(alert_class_i[kk * CLASS_DW+:CLASS_DW] * NAlerts) + kk] = 1'b1;
		end
		begin : sv2v_autoblock_174
			reg [31:0] kk;
			for (kk = 0; kk < N_LOC_ALERT; kk = kk + 1)
				loc_class_masks[(loc_alert_class_i[kk * CLASS_DW+:CLASS_DW] * N_LOC_ALERT) + kk] = 1'b1;
		end
	end
	generate
		genvar k;
		for (k = 0; k < N_CLASSES; k = k + 1) begin : gen_classifier
			assign class_trig_o[k] = |{alert_cause_o & class_masks[k * NAlerts+:NAlerts], loc_alert_cause_o & loc_class_masks[k * N_LOC_ALERT+:N_LOC_ALERT]};
		end
	endgenerate
endmodule
module alert_handler_ping_timer (
	clk_i,
	rst_ni,
	entropy_i,
	en_i,
	alert_en_i,
	ping_timeout_cyc_i,
	wait_cyc_mask_i,
	alert_ping_en_o,
	esc_ping_en_o,
	alert_ping_ok_i,
	esc_ping_ok_i,
	alert_ping_fail_o,
	esc_ping_fail_o
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	parameter [0:0] MaxLenSVA = 1'b1;
	parameter [0:0] LockupSVA = 1'b1;
	input clk_i;
	input rst_ni;
	input entropy_i;
	input en_i;
	input [NAlerts - 1:0] alert_en_i;
	input [PING_CNT_DW - 1:0] ping_timeout_cyc_i;
	input [PING_CNT_DW - 1:0] wait_cyc_mask_i;
	output wire [NAlerts - 1:0] alert_ping_en_o;
	output wire [N_ESC_SEV - 1:0] esc_ping_en_o;
	input [NAlerts - 1:0] alert_ping_ok_i;
	input [N_ESC_SEV - 1:0] esc_ping_ok_i;
	output reg alert_ping_fail_o;
	output reg esc_ping_fail_o;
	localparam [31:0] NModsToPing = NAlerts + N_ESC_SEV;
	localparam [31:0] IdDw = $clog2(NModsToPing);
	localparam [1023:0] perm = {32'd4, 32'd11, 32'd25, 32'd3, 32'd15, 32'd16, 32'd1, 32'd10, 32'd2, 32'd22, 32'd7, 32'd0, 32'd23, 32'd28, 32'd30, 32'd19, 32'd27, 32'd12, 32'd24, 32'd26, 32'd14, 32'd21, 32'd18, 32'd5, 32'd13, 32'd8, 32'd29, 32'd31, 32'd20, 32'd6, 32'd9, 32'd17};
	reg lfsr_en;
	wire [31:0] lfsr_state;
	wire [31:0] perm_state;
	wire [(16 - IdDw) - 1:0] unused_perm_state;
	prim_lfsr #(
		.LfsrDw(32),
		.EntropyDw(1),
		.StateOutDw(32),
		.DefaultSeed(LfsrSeed),
		.MaxLenSVA(MaxLenSVA),
		.LockupSVA(LockupSVA),
		.ExtSeedSVA(1'b0)
	) i_prim_lfsr(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.seed_en_i(1'b0),
		.seed_i({32 {1'sb0}}),
		.lfsr_en_i(lfsr_en),
		.entropy_i(entropy_i),
		.state_o(lfsr_state)
	);
	generate
		genvar k;
		for (k = 0; k < 32; k = k + 1) begin : gen_perm
			assign perm_state[k] = lfsr_state[perm[(31 - k) * 32+:32]];
		end
	endgenerate
	wire [IdDw - 1:0] id_to_ping;
	wire [PING_CNT_DW - 1:0] wait_cyc;
	assign id_to_ping = perm_state[16+:IdDw];
	assign unused_perm_state = perm_state[31:16 + IdDw];
	function automatic [23:0] sv2v_cast_24;
		input reg [23:0] inp;
		sv2v_cast_24 = inp;
	endfunction
	assign wait_cyc = sv2v_cast_24({perm_state[15:2], 8'h01, perm_state[1:0]}) & wait_cyc_mask_i;
	reg [(2 ** IdDw) - 1:0] enable_mask;
	always @(*) begin : p_enable_mask
		enable_mask = {2 ** IdDw {1'sb0}};
		enable_mask[NAlerts - 1:0] = alert_en_i;
		enable_mask[NModsToPing - 1:NAlerts] = {((NModsToPing - 1) >= NAlerts ? ((NModsToPing - 1) - NAlerts) + 1 : (NAlerts - (NModsToPing - 1)) + 1) {1'sb1}};
	end
	wire id_vld;
	assign id_vld = enable_mask[id_to_ping];
	wire [PING_CNT_DW - 1:0] cnt_d;
	reg [PING_CNT_DW - 1:0] cnt_q;
	reg cnt_en;
	reg cnt_clr;
	wire wait_ge;
	wire timeout_ge;
	assign cnt_d = cnt_q + 1'b1;
	assign wait_ge = cnt_q >= wait_cyc;
	assign timeout_ge = cnt_q >= ping_timeout_cyc_i;
	reg [1:0] state_d;
	reg [1:0] state_q;
	reg ping_en;
	wire ping_ok;
	wire [NModsToPing - 1:0] ping_sel;
	wire [NModsToPing - 1:0] spurious_ping;
	wire spurious_alert_ping;
	wire spurious_esc_ping;
	function automatic [NModsToPing - 1:0] sv2v_cast_7745B;
		input reg [NModsToPing - 1:0] inp;
		sv2v_cast_7745B = inp;
	endfunction
	assign ping_sel = sv2v_cast_7745B(ping_en) << id_to_ping;
	assign alert_ping_en_o = ping_sel[NAlerts - 1:0];
	assign esc_ping_en_o = ping_sel[NModsToPing - 1:NAlerts];
	assign ping_ok = |({esc_ping_ok_i, alert_ping_ok_i} & ping_sel);
	assign spurious_ping = {esc_ping_ok_i, alert_ping_ok_i} & ~ping_sel;
	assign spurious_alert_ping = |spurious_ping[NAlerts - 1:0];
	assign spurious_esc_ping = |spurious_ping[NModsToPing - 1:NAlerts];
	localparam [1:0] DoPing = 2;
	localparam [1:0] Init = 0;
	localparam [1:0] RespWait = 1;
	always @(*) begin : p_fsm
		state_d = state_q;
		cnt_en = 1'b0;
		cnt_clr = 1'b0;
		lfsr_en = 1'b0;
		ping_en = 1'b0;
		alert_ping_fail_o = spurious_alert_ping;
		esc_ping_fail_o = spurious_esc_ping;
		case (state_q)
			Init: begin
				cnt_clr = 1'b1;
				if (en_i)
					state_d = RespWait;
			end
			RespWait:
				if (!id_vld) begin
					lfsr_en = 1'b1;
					cnt_clr = 1'b1;
				end
				else if (wait_ge) begin
					state_d = DoPing;
					cnt_clr = 1'b1;
				end
				else
					cnt_en = 1'b1;
			DoPing: begin
				cnt_en = 1'b1;
				ping_en = 1'b1;
				if (timeout_ge || ping_ok) begin
					state_d = RespWait;
					lfsr_en = 1'b1;
					cnt_clr = 1'b1;
					if (timeout_ge)
						if (id_to_ping < NAlerts)
							alert_ping_fail_o = 1'b1;
						else
							esc_ping_fail_o = 1'b1;
				end
			end
			default: begin
				alert_ping_fail_o = 1'b1;
				esc_ping_fail_o = 1'b1;
			end
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : p_regs
		if (!rst_ni) begin
			state_q <= Init;
			cnt_q <= {PING_CNT_DW {1'sb0}};
		end
		else begin
			state_q <= state_d;
			if (cnt_clr)
				cnt_q <= {PING_CNT_DW {1'sb0}};
			else if (cnt_en)
				cnt_q <= cnt_d;
		end
	end
endmodule
module alert_handler_esc_timer (
	clk_i,
	rst_ni,
	en_i,
	clr_i,
	accum_trig_i,
	timeout_en_i,
	timeout_cyc_i,
	esc_en_i,
	esc_map_i,
	phase_cyc_i,
	esc_trig_o,
	esc_cnt_o,
	esc_sig_en_o,
	esc_state_o
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	input clk_i;
	input rst_ni;
	input en_i;
	input clr_i;
	input accum_trig_i;
	input timeout_en_i;
	input [EscCntDw - 1:0] timeout_cyc_i;
	input [N_ESC_SEV - 1:0] esc_en_i;
	input [(N_ESC_SEV * PHASE_DW) - 1:0] esc_map_i;
	input [(N_PHASES * EscCntDw) - 1:0] phase_cyc_i;
	output reg esc_trig_o;
	output wire [EscCntDw - 1:0] esc_cnt_o;
	output wire [N_ESC_SEV - 1:0] esc_sig_en_o;
	output wire [2:0] esc_state_o;
	reg [2:0] state_d;
	reg [2:0] state_q;
	reg cnt_en;
	reg cnt_clr;
	wire cnt_ge;
	wire [EscCntDw - 1:0] cnt_d;
	reg [EscCntDw - 1:0] cnt_q;
	assign cnt_d = cnt_q + 1'b1;
	assign esc_state_o = state_q;
	assign esc_cnt_o = cnt_q;
	reg [EscCntDw - 1:0] thresh;
	assign cnt_ge = cnt_q >= thresh;
	reg [N_PHASES - 1:0] phase_oh;
	always @(*) begin : p_fsm
		state_d = state_q;
		cnt_en = 1'b0;
		cnt_clr = 1'b0;
		esc_trig_o = 1'b0;
		phase_oh = {N_PHASES {1'sb0}};
		thresh = timeout_cyc_i;
		case (state_q)
			Idle: begin
				cnt_clr = 1'b1;
				if (accum_trig_i && en_i) begin
					state_d = Phase0;
					cnt_en = 1'b1;
					esc_trig_o = 1'b1;
				end
				else if ((timeout_en_i && !cnt_ge) && en_i) begin
					cnt_en = 1'b1;
					state_d = Timeout;
				end
			end
			Timeout:
				if (accum_trig_i || (cnt_ge && timeout_en_i)) begin
					state_d = Phase0;
					cnt_en = 1'b1;
					cnt_clr = 1'b1;
					esc_trig_o = 1'b1;
				end
				else if (timeout_en_i)
					cnt_en = 1'b1;
				else begin
					state_d = Idle;
					cnt_clr = 1'b1;
				end
			Phase0: begin
				cnt_en = 1'b1;
				phase_oh[0] = 1'b1;
				thresh = phase_cyc_i[0+:EscCntDw];
				if (clr_i) begin
					state_d = Idle;
					cnt_clr = 1'b1;
					cnt_en = 1'b0;
				end
				else if (cnt_ge) begin
					state_d = Phase1;
					cnt_clr = 1'b1;
					cnt_en = 1'b1;
				end
			end
			Phase1: begin
				cnt_en = 1'b1;
				phase_oh[1] = 1'b1;
				thresh = phase_cyc_i[EscCntDw+:EscCntDw];
				if (clr_i) begin
					state_d = Idle;
					cnt_clr = 1'b1;
					cnt_en = 1'b0;
				end
				else if (cnt_ge) begin
					state_d = Phase2;
					cnt_clr = 1'b1;
					cnt_en = 1'b1;
				end
			end
			Phase2: begin
				cnt_en = 1'b1;
				phase_oh[2] = 1'b1;
				thresh = phase_cyc_i[2 * EscCntDw+:EscCntDw];
				if (clr_i) begin
					state_d = Idle;
					cnt_clr = 1'b1;
					cnt_en = 1'b0;
				end
				else if (cnt_ge) begin
					state_d = Phase3;
					cnt_clr = 1'b1;
				end
			end
			Phase3: begin
				cnt_en = 1'b1;
				phase_oh[3] = 1'b1;
				thresh = phase_cyc_i[3 * EscCntDw+:EscCntDw];
				if (clr_i) begin
					state_d = Idle;
					cnt_clr = 1'b1;
					cnt_en = 1'b0;
				end
				else if (cnt_ge) begin
					state_d = Terminal;
					cnt_clr = 1'b1;
				end
			end
			Terminal: begin
				cnt_clr = 1'b1;
				if (clr_i)
					state_d = Idle;
			end
			default: state_d = Idle;
		endcase
	end
	wire [(N_ESC_SEV * N_PHASES) - 1:0] esc_map_oh;
	generate
		genvar k;
		for (k = 0; k < N_ESC_SEV; k = k + 1) begin : gen_phase_map
			function automatic [3:0] sv2v_cast_4;
				input reg [3:0] inp;
				sv2v_cast_4 = inp;
			endfunction
			assign esc_map_oh[k * N_PHASES+:N_PHASES] = sv2v_cast_4(esc_en_i[k]) << esc_map_i[k * PHASE_DW+:PHASE_DW];
			assign esc_sig_en_o[k] = |(esc_map_oh[k * N_PHASES+:N_PHASES] & phase_oh);
		end
	endgenerate
	function automatic [31:0] sv2v_cast_32;
		input reg [31:0] inp;
		sv2v_cast_32 = inp;
	endfunction
	always @(posedge clk_i or negedge rst_ni) begin : p_regs
		if (!rst_ni) begin
			state_q <= Idle;
			cnt_q <= {EscCntDw {1'sb0}};
		end
		else begin
			state_q <= state_d;
			if (cnt_en && cnt_clr)
				cnt_q <= sv2v_cast_32(1'b1);
			else if (cnt_clr)
				cnt_q <= {EscCntDw {1'sb0}};
			else if (cnt_en)
				cnt_q <= cnt_d;
		end
	end
endmodule
module alert_handler_accu (
	clk_i,
	rst_ni,
	class_en_i,
	clr_i,
	class_trig_i,
	thresh_i,
	accu_cnt_o,
	accu_trig_o
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	input clk_i;
	input rst_ni;
	input class_en_i;
	input clr_i;
	input class_trig_i;
	input [AccuCntDw - 1:0] thresh_i;
	output wire [AccuCntDw - 1:0] accu_cnt_o;
	output wire accu_trig_o;
	wire trig_gated;
	wire [AccuCntDw - 1:0] accu_d;
	reg [AccuCntDw - 1:0] accu_q;
	assign trig_gated = class_trig_i & class_en_i;
	assign accu_d = (clr_i ? {AccuCntDw {1'sb0}} : (trig_gated && !(&accu_q) ? accu_q + 1'b1 : accu_q));
	assign accu_trig_o = (accu_q >= thresh_i) & trig_gated;
	assign accu_cnt_o = accu_q;
	always @(posedge clk_i or negedge rst_ni) begin : p_regs
		if (!rst_ni)
			accu_q <= {AccuCntDw {1'sb0}};
		else
			accu_q <= accu_d;
	end
endmodule
module alert_handler (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	intr_classa_o,
	intr_classb_o,
	intr_classc_o,
	intr_classd_o,
	crashdump_o,
	entropy_i,
	alert_tx_i,
	alert_rx_o,
	esc_rx_i,
	esc_tx_o
);
	localparam signed [31:0] alert_handler_reg_pkg_NAlerts = 1;
	localparam [31:0] NAlerts = alert_handler_reg_pkg_NAlerts;
	localparam signed [31:0] alert_handler_reg_pkg_EscCntDw = 32;
	localparam [31:0] EscCntDw = alert_handler_reg_pkg_EscCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_AccuCntDw = 16;
	localparam [31:0] AccuCntDw = alert_handler_reg_pkg_AccuCntDw;
	localparam signed [31:0] alert_handler_reg_pkg_LfsrSeed = 2147483647;
	localparam [31:0] LfsrSeed = alert_handler_reg_pkg_LfsrSeed;
	localparam [alert_handler_reg_pkg_NAlerts - 1:0] alert_handler_reg_pkg_AsyncOn = 1'b0;
	localparam [NAlerts - 1:0] AsyncOn = alert_handler_reg_pkg_AsyncOn;
	localparam signed [31:0] alert_handler_reg_pkg_N_CLASSES = 4;
	localparam [31:0] N_CLASSES = alert_handler_reg_pkg_N_CLASSES;
	localparam signed [31:0] alert_handler_reg_pkg_N_ESC_SEV = 4;
	localparam [31:0] N_ESC_SEV = alert_handler_reg_pkg_N_ESC_SEV;
	localparam signed [31:0] alert_handler_reg_pkg_N_PHASES = 4;
	localparam [31:0] N_PHASES = alert_handler_reg_pkg_N_PHASES;
	localparam signed [31:0] alert_handler_reg_pkg_N_LOC_ALERT = 4;
	localparam [31:0] N_LOC_ALERT = alert_handler_reg_pkg_N_LOC_ALERT;
	localparam signed [31:0] alert_handler_reg_pkg_PING_CNT_DW = 24;
	localparam [31:0] PING_CNT_DW = alert_handler_reg_pkg_PING_CNT_DW;
	localparam signed [31:0] alert_handler_reg_pkg_PHASE_DW = 2;
	localparam [31:0] PHASE_DW = alert_handler_reg_pkg_PHASE_DW;
	localparam signed [31:0] alert_handler_reg_pkg_CLASS_DW = 2;
	localparam [31:0] CLASS_DW = alert_handler_reg_pkg_CLASS_DW;
	localparam [2:0] Idle = 3'b000;
	localparam [2:0] Timeout = 3'b001;
	localparam [2:0] Terminal = 3'b011;
	localparam [2:0] Phase0 = 3'b100;
	localparam [2:0] Phase1 = 3'b101;
	localparam [2:0] Phase2 = 3'b110;
	localparam [2:0] Phase3 = 3'b111;
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire intr_classa_o;
	output wire intr_classb_o;
	output wire intr_classc_o;
	output wire intr_classd_o;
	output wire [((((NAlerts + N_LOC_ALERT) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + (N_CLASSES * 3)) - 1:0] crashdump_o;
	input entropy_i;
	input wire [(NAlerts * 2) - 1:0] alert_tx_i;
	output wire [(NAlerts * 4) - 1:0] alert_rx_o;
	input wire [(N_ESC_SEV * 2) - 1:0] esc_rx_i;
	output wire [(N_ESC_SEV * 2) - 1:0] esc_tx_o;
	wire [N_CLASSES - 1:0] irq;
	wire [((((((NAlerts + N_LOC_ALERT) + N_CLASSES) + N_CLASSES) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + (N_CLASSES * 3)) - 1:0] hw2reg_wrap;
	wire [((((((((((((1 + PING_CNT_DW) + N_LOC_ALERT) + (N_LOC_ALERT * CLASS_DW)) + NAlerts) + (NAlerts * CLASS_DW)) + N_CLASSES) + N_CLASSES) + (N_CLASSES * AccuCntDw)) + (N_CLASSES * EscCntDw)) + ((N_CLASSES * N_PHASES) * EscCntDw)) + (N_CLASSES * N_ESC_SEV)) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)) - 1:0] reg2hw_wrap;
	assign {intr_classd_o, intr_classc_o, intr_classb_o, intr_classa_o} = irq;
	alert_handler_reg_wrap i_reg_wrap(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.irq_o(irq),
		.crashdump_o(crashdump_o),
		.hw2reg_wrap(hw2reg_wrap),
		.reg2hw_wrap(reg2hw_wrap)
	);
	wire [N_LOC_ALERT - 1:0] loc_alert_trig;
	wire [NAlerts - 1:0] alert_ping_en;
	wire [NAlerts - 1:0] alert_ping_ok;
	wire [N_ESC_SEV - 1:0] esc_ping_en;
	wire [N_ESC_SEV - 1:0] esc_ping_ok;
	alert_handler_ping_timer i_ping_timer(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.entropy_i(entropy_i),
		.en_i(reg2hw_wrap[1 + (PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))))]),
		.alert_en_i(reg2hw_wrap[NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))-:((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) >= ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) ? ((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) - ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))) + 1 : (((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) - (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) + 1)]),
		.ping_timeout_cyc_i(reg2hw_wrap[PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))-:((PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))) >= (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))))) ? ((PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))) - (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))))))) + 1 : ((N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))))) - (PING_CNT_DW + (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))))) + 1)]),
		.wait_cyc_mask_i(24'hffffff),
		.alert_ping_en_o(alert_ping_en),
		.esc_ping_en_o(esc_ping_en),
		.alert_ping_ok_i(alert_ping_ok),
		.esc_ping_ok_i(esc_ping_ok),
		.alert_ping_fail_o(loc_alert_trig[0]),
		.esc_ping_fail_o(loc_alert_trig[1])
	);
	wire [NAlerts - 1:0] alert_integfail;
	wire [NAlerts - 1:0] alert_trig;
	generate
		genvar k;
		for (k = 0; k < NAlerts; k = k + 1) begin : gen_alerts
			prim_alert_receiver #(.AsyncOn(AsyncOn[k])) i_alert_receiver(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.ping_en_i(alert_ping_en[k]),
				.ping_ok_o(alert_ping_ok[k]),
				.integ_fail_o(alert_integfail[k]),
				.alert_o(alert_trig[k]),
				.alert_rx_o(alert_rx_o[k * 4+:4]),
				.alert_tx_i(alert_tx_i[k * 2+:2])
			);
		end
	endgenerate
	assign loc_alert_trig[2] = |(reg2hw_wrap[NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))-:((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) >= ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) ? ((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) - ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))) + 1 : (((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) - (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) + 1)] & alert_integfail);
	alert_handler_class i_class(
		.alert_trig_i(alert_trig),
		.loc_alert_trig_i(loc_alert_trig),
		.alert_en_i(reg2hw_wrap[NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))-:((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) >= ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) ? ((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) - ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))) + 1 : (((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) - (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) + 1)]),
		.loc_alert_en_i(reg2hw_wrap[N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))-:((N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))) >= ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))))) ? ((N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))) - ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))))) + 1 : (((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))))) - (N_LOC_ALERT + ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))))) + 1)]),
		.alert_class_i(reg2hw_wrap[(NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))-:(((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))) >= (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))) ? (((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))) - (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))) + 1 : ((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))) - ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))) + 1)]),
		.loc_alert_class_i(reg2hw_wrap[(N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))-:(((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) >= (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))) ? (((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))))))) - (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW)))))))))) + 1 : ((NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + ((N_CLASSES * N_ESC_SEV) * PHASE_DW))))))))) - ((N_LOC_ALERT * CLASS_DW) + (NAlerts + ((NAlerts * CLASS_DW) + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))))))) + 1)]),
		.alert_cause_o(hw2reg_wrap[NAlerts + (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))))-:((NAlerts + (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))))) >= (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))))) ? ((NAlerts + (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))))) - (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))))))) + 1 : ((N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))))) - (NAlerts + (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))))) + 1)]),
		.loc_alert_cause_o(hw2reg_wrap[N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))-:((N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))) >= (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))))) ? ((N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))) - (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))))) + 1 : ((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))))) - (N_LOC_ALERT + (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))))) + 1)]),
		.class_trig_o(hw2reg_wrap[N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))-:((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) >= (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))) ? ((N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3))))) + 1 : ((N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (N_CLASSES * 3)))) - (N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))))) + 1)])
	);
	wire [N_CLASSES - 1:0] class_accum_trig;
	wire [(N_CLASSES * N_ESC_SEV) - 1:0] class_esc_sig_en;
	generate
		for (k = 0; k < N_CLASSES; k = k + 1) begin : gen_classes
			alert_handler_accu i_accu(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.class_en_i(reg2hw_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) - ((N_CLASSES - 1) - k)]),
				.clr_i(reg2hw_wrap[(N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))) - ((N_CLASSES - 1) - k)]),
				.class_trig_i(hw2reg_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))))) - ((N_CLASSES - 1) - k)]),
				.thresh_i(reg2hw_wrap[((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))) - (((N_CLASSES * AccuCntDw) - 1) - (k * AccuCntDw))+:AccuCntDw]),
				.accu_cnt_o(hw2reg_wrap[((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1))) - (((N_CLASSES * AccuCntDw) - 1) - (k * AccuCntDw))+:AccuCntDw]),
				.accu_trig_o(class_accum_trig[k])
			);
			alert_handler_esc_timer i_esc_timer(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.en_i(reg2hw_wrap[(N_CLASSES + (N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))))))) - ((N_CLASSES - 1) - k)]),
				.clr_i(reg2hw_wrap[(N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))))) - ((N_CLASSES - 1) - k)]),
				.timeout_en_i(irq[k]),
				.accum_trig_i(class_accum_trig[k]),
				.timeout_cyc_i(reg2hw_wrap[((N_CLASSES * EscCntDw) + (((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)))) - (((N_CLASSES * EscCntDw) - 1) - (k * EscCntDw))+:EscCntDw]),
				.esc_en_i(reg2hw_wrap[((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1)) - (((N_CLASSES * N_ESC_SEV) - 1) - (k * N_ESC_SEV))+:N_ESC_SEV]),
				.esc_map_i(reg2hw_wrap[(((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1) - ((((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1) - (PHASE_DW * (k * N_ESC_SEV)))+:PHASE_DW * N_ESC_SEV]),
				.phase_cyc_i(reg2hw_wrap[(((N_CLASSES * N_PHASES) * EscCntDw) + ((N_CLASSES * N_ESC_SEV) + (((N_CLASSES * N_ESC_SEV) * PHASE_DW) - 1))) - ((((N_CLASSES * N_PHASES) * EscCntDw) - 1) - (EscCntDw * (k * N_PHASES)))+:EscCntDw * N_PHASES]),
				.esc_trig_o(hw2reg_wrap[(N_CLASSES + ((N_CLASSES * AccuCntDw) + ((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)))) - ((N_CLASSES - 1) - k)]),
				.esc_cnt_o(hw2reg_wrap[((N_CLASSES * EscCntDw) + ((N_CLASSES * 3) - 1)) - (((N_CLASSES * EscCntDw) - 1) - (k * EscCntDw))+:EscCntDw]),
				.esc_state_o(hw2reg_wrap[((N_CLASSES * 3) - 1) - (((N_CLASSES * 3) - 1) - (k * 3))+:3]),
				.esc_sig_en_o(class_esc_sig_en[k * N_ESC_SEV+:N_ESC_SEV])
			);
		end
	endgenerate
	wire [N_ESC_SEV - 1:0] esc_sig_en;
	wire [N_ESC_SEV - 1:0] esc_integfail;
	wire [(N_ESC_SEV * N_CLASSES) - 1:0] esc_sig_en_trsp;
	generate
		for (k = 0; k < N_ESC_SEV; k = k + 1) begin : gen_esc_sev
			genvar j;
			for (j = 0; j < N_CLASSES; j = j + 1) begin : gen_transp
				assign esc_sig_en_trsp[(k * N_CLASSES) + j] = class_esc_sig_en[(j * N_ESC_SEV) + k];
			end
			assign esc_sig_en[k] = |esc_sig_en_trsp[k * N_CLASSES+:N_CLASSES];
			prim_esc_sender i_esc_sender(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.ping_en_i(esc_ping_en[k]),
				.ping_ok_o(esc_ping_ok[k]),
				.integ_fail_o(esc_integfail[k]),
				.esc_en_i(esc_sig_en[k]),
				.esc_rx_i(esc_rx_i[k * 2+:2]),
				.esc_tx_o(esc_tx_o[k * 2+:2])
			);
		end
	endgenerate
	assign loc_alert_trig[3] = |esc_integfail;
endmodule
module padctl (
	cio_uart_tx_d2p,
	cio_uart_tx_en_d2p,
	cio_uart_rx_p2d,
	IO_URX,
	IO_UTX,
	cio_gpio_d2p,
	cio_gpio_en_d2p,
	cio_gpio_p2d,
	IO_GP0,
	IO_GP1,
	IO_GP2,
	IO_GP3,
	IO_GP4,
	IO_GP5,
	IO_GP6,
	IO_GP7,
	IO_GP8,
	IO_GP9,
	IO_GP10,
	IO_GP11,
	IO_GP12,
	IO_GP13,
	IO_GP14,
	IO_GP15,
	cio_spi_device_sck_p2d,
	cio_spi_device_csb_p2d,
	cio_spi_device_mosi_p2d,
	cio_spi_device_miso_d2p,
	cio_spi_device_miso_en_d2p,
	cio_jtag_tck_p2d,
	cio_jtag_tms_p2d,
	cio_jtag_trst_n_p2d,
	cio_jtag_srst_n_p2d,
	cio_jtag_tdi_p2d,
	cio_jtag_tdo_d2p,
	IO_DPS0,
	IO_DPS1,
	IO_DPS2,
	IO_DPS3,
	IO_DPS4,
	IO_DPS5,
	IO_DPS6,
	IO_DPS7
);
	input cio_uart_tx_d2p;
	input cio_uart_tx_en_d2p;
	output cio_uart_rx_p2d;
	input IO_URX;
	output IO_UTX;
	input [31:0] cio_gpio_d2p;
	input [31:0] cio_gpio_en_d2p;
	output [31:0] cio_gpio_p2d;
	inout IO_GP0;
	inout IO_GP1;
	inout IO_GP2;
	inout IO_GP3;
	inout IO_GP4;
	inout IO_GP5;
	inout IO_GP6;
	inout IO_GP7;
	inout IO_GP8;
	inout IO_GP9;
	inout IO_GP10;
	inout IO_GP11;
	inout IO_GP12;
	inout IO_GP13;
	inout IO_GP14;
	inout IO_GP15;
	output cio_spi_device_sck_p2d;
	output cio_spi_device_csb_p2d;
	output cio_spi_device_mosi_p2d;
	input cio_spi_device_miso_d2p;
	input cio_spi_device_miso_en_d2p;
	output cio_jtag_tck_p2d;
	output cio_jtag_tms_p2d;
	output cio_jtag_trst_n_p2d;
	output cio_jtag_srst_n_p2d;
	output cio_jtag_tdi_p2d;
	input cio_jtag_tdo_d2p;
	input IO_DPS0;
	input IO_DPS1;
	output IO_DPS2;
	input IO_DPS3;
	input IO_DPS4;
	input IO_DPS5;
	input IO_DPS6;
	input IO_DPS7;
	wire jtag_spi_n;
	wire dps2;
	wire dps2_en;
	wire boot_strap;
	assign cio_uart_rx_p2d = IO_URX;
	assign IO_UTX = (cio_uart_tx_en_d2p ? cio_uart_tx_d2p : 1'bz);
	assign cio_gpio_p2d = {14'h0000, boot_strap, jtag_spi_n, IO_GP15, IO_GP14, IO_GP13, IO_GP12, IO_GP11, IO_GP10, IO_GP9, IO_GP8, IO_GP7, IO_GP6, IO_GP5, IO_GP4, IO_GP3, IO_GP2, IO_GP1, IO_GP0};
	assign IO_GP0 = (cio_gpio_en_d2p[0] ? cio_gpio_d2p[0] : 1'bz);
	assign IO_GP1 = (cio_gpio_en_d2p[1] ? cio_gpio_d2p[1] : 1'bz);
	assign IO_GP2 = (cio_gpio_en_d2p[2] ? cio_gpio_d2p[2] : 1'bz);
	assign IO_GP3 = (cio_gpio_en_d2p[3] ? cio_gpio_d2p[3] : 1'bz);
	assign IO_GP4 = (cio_gpio_en_d2p[4] ? cio_gpio_d2p[4] : 1'bz);
	assign IO_GP5 = (cio_gpio_en_d2p[5] ? cio_gpio_d2p[5] : 1'bz);
	assign IO_GP6 = (cio_gpio_en_d2p[6] ? cio_gpio_d2p[6] : 1'bz);
	assign IO_GP7 = (cio_gpio_en_d2p[7] ? cio_gpio_d2p[7] : 1'bz);
	assign IO_GP8 = (cio_gpio_en_d2p[8] ? cio_gpio_d2p[8] : 1'bz);
	assign IO_GP9 = (cio_gpio_en_d2p[9] ? cio_gpio_d2p[9] : 1'bz);
	assign IO_GP10 = (cio_gpio_en_d2p[10] ? cio_gpio_d2p[10] : 1'bz);
	assign IO_GP11 = (cio_gpio_en_d2p[11] ? cio_gpio_d2p[11] : 1'bz);
	assign IO_GP12 = (cio_gpio_en_d2p[12] ? cio_gpio_d2p[12] : 1'bz);
	assign IO_GP13 = (cio_gpio_en_d2p[13] ? cio_gpio_d2p[13] : 1'bz);
	assign IO_GP14 = (cio_gpio_en_d2p[14] ? cio_gpio_d2p[14] : 1'bz);
	assign IO_GP15 = (cio_gpio_en_d2p[15] ? cio_gpio_d2p[15] : 1'bz);
	assign jtag_spi_n = IO_DPS6;
	assign boot_strap = IO_DPS7;
	assign cio_spi_device_sck_p2d = (jtag_spi_n ? 0 : IO_DPS0);
	assign cio_jtag_tck_p2d = (jtag_spi_n ? IO_DPS0 : 0);
	assign cio_spi_device_mosi_p2d = (jtag_spi_n ? 0 : IO_DPS1);
	assign cio_jtag_tdi_p2d = (jtag_spi_n ? IO_DPS1 : 0);
	assign dps2 = (jtag_spi_n ? cio_jtag_tdo_d2p : cio_spi_device_miso_d2p);
	assign dps2_en = (jtag_spi_n ? 1 : cio_spi_device_miso_en_d2p);
	assign IO_DPS2 = (dps2_en ? dps2 : 1'bz);
	assign cio_spi_device_csb_p2d = (jtag_spi_n ? 1 : IO_DPS3);
	assign cio_jtag_tms_p2d = (jtag_spi_n ? IO_DPS3 : 0);
	assign cio_jtag_trst_n_p2d = (jtag_spi_n ? IO_DPS4 : 1);
	assign cio_jtag_srst_n_p2d = (jtag_spi_n ? IO_DPS5 : 1);
endmodule
module uart_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [124:0] reg2hw;
	input wire [64:0] hw2reg;
	input devmode_i;
	localparam [5:0] UART_INTR_STATE_OFFSET = 6'h00;
	localparam [5:0] UART_INTR_ENABLE_OFFSET = 6'h04;
	localparam [5:0] UART_INTR_TEST_OFFSET = 6'h08;
	localparam [5:0] UART_CTRL_OFFSET = 6'h0c;
	localparam [5:0] UART_STATUS_OFFSET = 6'h10;
	localparam [5:0] UART_RDATA_OFFSET = 6'h14;
	localparam [5:0] UART_WDATA_OFFSET = 6'h18;
	localparam [5:0] UART_FIFO_CTRL_OFFSET = 6'h1c;
	localparam [5:0] UART_FIFO_STATUS_OFFSET = 6'h20;
	localparam [5:0] UART_OVRD_OFFSET = 6'h24;
	localparam [5:0] UART_VAL_OFFSET = 6'h28;
	localparam [5:0] UART_TIMEOUT_CTRL_OFFSET = 6'h2c;
	localparam signed [31:0] UART_INTR_STATE = 0;
	localparam signed [31:0] UART_INTR_ENABLE = 1;
	localparam signed [31:0] UART_INTR_TEST = 2;
	localparam signed [31:0] UART_CTRL = 3;
	localparam signed [31:0] UART_STATUS = 4;
	localparam signed [31:0] UART_RDATA = 5;
	localparam signed [31:0] UART_WDATA = 6;
	localparam signed [31:0] UART_FIFO_CTRL = 7;
	localparam signed [31:0] UART_FIFO_STATUS = 8;
	localparam signed [31:0] UART_OVRD = 9;
	localparam signed [31:0] UART_VAL = 10;
	localparam signed [31:0] UART_TIMEOUT_CTRL = 11;
	localparam [47:0] UART_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b1111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0111, 4'b0001, 4'b0011, 4'b1111};
	localparam signed [31:0] AW = 6;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire intr_state_tx_watermark_qs;
	wire intr_state_tx_watermark_wd;
	wire intr_state_tx_watermark_we;
	wire intr_state_rx_watermark_qs;
	wire intr_state_rx_watermark_wd;
	wire intr_state_rx_watermark_we;
	wire intr_state_tx_empty_qs;
	wire intr_state_tx_empty_wd;
	wire intr_state_tx_empty_we;
	wire intr_state_rx_overflow_qs;
	wire intr_state_rx_overflow_wd;
	wire intr_state_rx_overflow_we;
	wire intr_state_rx_frame_err_qs;
	wire intr_state_rx_frame_err_wd;
	wire intr_state_rx_frame_err_we;
	wire intr_state_rx_break_err_qs;
	wire intr_state_rx_break_err_wd;
	wire intr_state_rx_break_err_we;
	wire intr_state_rx_timeout_qs;
	wire intr_state_rx_timeout_wd;
	wire intr_state_rx_timeout_we;
	wire intr_state_rx_parity_err_qs;
	wire intr_state_rx_parity_err_wd;
	wire intr_state_rx_parity_err_we;
	wire intr_enable_tx_watermark_qs;
	wire intr_enable_tx_watermark_wd;
	wire intr_enable_tx_watermark_we;
	wire intr_enable_rx_watermark_qs;
	wire intr_enable_rx_watermark_wd;
	wire intr_enable_rx_watermark_we;
	wire intr_enable_tx_empty_qs;
	wire intr_enable_tx_empty_wd;
	wire intr_enable_tx_empty_we;
	wire intr_enable_rx_overflow_qs;
	wire intr_enable_rx_overflow_wd;
	wire intr_enable_rx_overflow_we;
	wire intr_enable_rx_frame_err_qs;
	wire intr_enable_rx_frame_err_wd;
	wire intr_enable_rx_frame_err_we;
	wire intr_enable_rx_break_err_qs;
	wire intr_enable_rx_break_err_wd;
	wire intr_enable_rx_break_err_we;
	wire intr_enable_rx_timeout_qs;
	wire intr_enable_rx_timeout_wd;
	wire intr_enable_rx_timeout_we;
	wire intr_enable_rx_parity_err_qs;
	wire intr_enable_rx_parity_err_wd;
	wire intr_enable_rx_parity_err_we;
	wire intr_test_tx_watermark_wd;
	wire intr_test_tx_watermark_we;
	wire intr_test_rx_watermark_wd;
	wire intr_test_rx_watermark_we;
	wire intr_test_tx_empty_wd;
	wire intr_test_tx_empty_we;
	wire intr_test_rx_overflow_wd;
	wire intr_test_rx_overflow_we;
	wire intr_test_rx_frame_err_wd;
	wire intr_test_rx_frame_err_we;
	wire intr_test_rx_break_err_wd;
	wire intr_test_rx_break_err_we;
	wire intr_test_rx_timeout_wd;
	wire intr_test_rx_timeout_we;
	wire intr_test_rx_parity_err_wd;
	wire intr_test_rx_parity_err_we;
	wire ctrl_tx_qs;
	wire ctrl_tx_wd;
	wire ctrl_tx_we;
	wire ctrl_rx_qs;
	wire ctrl_rx_wd;
	wire ctrl_rx_we;
	wire ctrl_nf_qs;
	wire ctrl_nf_wd;
	wire ctrl_nf_we;
	wire ctrl_slpbk_qs;
	wire ctrl_slpbk_wd;
	wire ctrl_slpbk_we;
	wire ctrl_llpbk_qs;
	wire ctrl_llpbk_wd;
	wire ctrl_llpbk_we;
	wire ctrl_parity_en_qs;
	wire ctrl_parity_en_wd;
	wire ctrl_parity_en_we;
	wire ctrl_parity_odd_qs;
	wire ctrl_parity_odd_wd;
	wire ctrl_parity_odd_we;
	wire [1:0] ctrl_rxblvl_qs;
	wire [1:0] ctrl_rxblvl_wd;
	wire ctrl_rxblvl_we;
	wire [15:0] ctrl_nco_qs;
	wire [15:0] ctrl_nco_wd;
	wire ctrl_nco_we;
	wire status_txfull_qs;
	wire status_txfull_re;
	wire status_rxfull_qs;
	wire status_rxfull_re;
	wire status_txempty_qs;
	wire status_txempty_re;
	wire status_txidle_qs;
	wire status_txidle_re;
	wire status_rxidle_qs;
	wire status_rxidle_re;
	wire status_rxempty_qs;
	wire status_rxempty_re;
	wire [7:0] rdata_qs;
	wire rdata_re;
	wire [7:0] wdata_wd;
	wire wdata_we;
	wire fifo_ctrl_rxrst_wd;
	wire fifo_ctrl_rxrst_we;
	wire fifo_ctrl_txrst_wd;
	wire fifo_ctrl_txrst_we;
	wire [2:0] fifo_ctrl_rxilvl_qs;
	wire [2:0] fifo_ctrl_rxilvl_wd;
	wire fifo_ctrl_rxilvl_we;
	wire [1:0] fifo_ctrl_txilvl_qs;
	wire [1:0] fifo_ctrl_txilvl_wd;
	wire fifo_ctrl_txilvl_we;
	wire [5:0] fifo_status_txlvl_qs;
	wire fifo_status_txlvl_re;
	wire [5:0] fifo_status_rxlvl_qs;
	wire fifo_status_rxlvl_re;
	wire ovrd_txen_qs;
	wire ovrd_txen_wd;
	wire ovrd_txen_we;
	wire ovrd_txval_qs;
	wire ovrd_txval_wd;
	wire ovrd_txval_we;
	wire [15:0] val_qs;
	wire val_re;
	wire [23:0] timeout_ctrl_val_qs;
	wire [23:0] timeout_ctrl_val_wd;
	wire timeout_ctrl_val_we;
	wire timeout_ctrl_en_qs;
	wire timeout_ctrl_en_wd;
	wire timeout_ctrl_en_we;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_tx_watermark(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_tx_watermark_we),
		.wd(intr_state_tx_watermark_wd),
		.de(hw2reg[63]),
		.d(hw2reg[64]),
		.qe(),
		.q(reg2hw[124]),
		.qs(intr_state_tx_watermark_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_watermark(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_watermark_we),
		.wd(intr_state_rx_watermark_wd),
		.de(hw2reg[61]),
		.d(hw2reg[62]),
		.qe(),
		.q(reg2hw[123]),
		.qs(intr_state_rx_watermark_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_tx_empty(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_tx_empty_we),
		.wd(intr_state_tx_empty_wd),
		.de(hw2reg[59]),
		.d(hw2reg[60]),
		.qe(),
		.q(reg2hw[122]),
		.qs(intr_state_tx_empty_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_overflow(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_overflow_we),
		.wd(intr_state_rx_overflow_wd),
		.de(hw2reg[57]),
		.d(hw2reg[58]),
		.qe(),
		.q(reg2hw[121]),
		.qs(intr_state_rx_overflow_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_frame_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_frame_err_we),
		.wd(intr_state_rx_frame_err_wd),
		.de(hw2reg[55]),
		.d(hw2reg[56]),
		.qe(),
		.q(reg2hw[120]),
		.qs(intr_state_rx_frame_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_break_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_break_err_we),
		.wd(intr_state_rx_break_err_wd),
		.de(hw2reg[53]),
		.d(hw2reg[54]),
		.qe(),
		.q(reg2hw[119]),
		.qs(intr_state_rx_break_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_timeout(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_timeout_we),
		.wd(intr_state_rx_timeout_wd),
		.de(hw2reg[51]),
		.d(hw2reg[52]),
		.qe(),
		.q(reg2hw[118]),
		.qs(intr_state_rx_timeout_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_rx_parity_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_rx_parity_err_we),
		.wd(intr_state_rx_parity_err_wd),
		.de(hw2reg[49]),
		.d(hw2reg[50]),
		.qe(),
		.q(reg2hw[117]),
		.qs(intr_state_rx_parity_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_tx_watermark(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_tx_watermark_we),
		.wd(intr_enable_tx_watermark_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[116]),
		.qs(intr_enable_tx_watermark_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_watermark(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_watermark_we),
		.wd(intr_enable_rx_watermark_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[115]),
		.qs(intr_enable_rx_watermark_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_tx_empty(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_tx_empty_we),
		.wd(intr_enable_tx_empty_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[114]),
		.qs(intr_enable_tx_empty_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_overflow(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_overflow_we),
		.wd(intr_enable_rx_overflow_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[113]),
		.qs(intr_enable_rx_overflow_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_frame_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_frame_err_we),
		.wd(intr_enable_rx_frame_err_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[112]),
		.qs(intr_enable_rx_frame_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_break_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_break_err_we),
		.wd(intr_enable_rx_break_err_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[111]),
		.qs(intr_enable_rx_break_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_timeout(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_timeout_we),
		.wd(intr_enable_rx_timeout_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[110]),
		.qs(intr_enable_rx_timeout_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_rx_parity_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_rx_parity_err_we),
		.wd(intr_enable_rx_parity_err_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[109]),
		.qs(intr_enable_rx_parity_err_qs)
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_tx_watermark(
		.re(1'b0),
		.we(intr_test_tx_watermark_we),
		.wd(intr_test_tx_watermark_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[107]),
		.q(reg2hw[108]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_watermark(
		.re(1'b0),
		.we(intr_test_rx_watermark_we),
		.wd(intr_test_rx_watermark_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[105]),
		.q(reg2hw[106]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_tx_empty(
		.re(1'b0),
		.we(intr_test_tx_empty_we),
		.wd(intr_test_tx_empty_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[103]),
		.q(reg2hw[104]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_overflow(
		.re(1'b0),
		.we(intr_test_rx_overflow_we),
		.wd(intr_test_rx_overflow_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[101]),
		.q(reg2hw[102]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_frame_err(
		.re(1'b0),
		.we(intr_test_rx_frame_err_we),
		.wd(intr_test_rx_frame_err_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[99]),
		.q(reg2hw[100]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_break_err(
		.re(1'b0),
		.we(intr_test_rx_break_err_we),
		.wd(intr_test_rx_break_err_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[97]),
		.q(reg2hw[98]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_timeout(
		.re(1'b0),
		.we(intr_test_rx_timeout_we),
		.wd(intr_test_rx_timeout_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[95]),
		.q(reg2hw[96]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_rx_parity_err(
		.re(1'b0),
		.we(intr_test_rx_parity_err_we),
		.wd(intr_test_rx_parity_err_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[93]),
		.q(reg2hw[94]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_tx(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_tx_we),
		.wd(ctrl_tx_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[92]),
		.qs(ctrl_tx_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_rx(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_rx_we),
		.wd(ctrl_rx_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[91]),
		.qs(ctrl_rx_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_nf(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_nf_we),
		.wd(ctrl_nf_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[90]),
		.qs(ctrl_nf_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_slpbk(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_slpbk_we),
		.wd(ctrl_slpbk_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[89]),
		.qs(ctrl_slpbk_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_llpbk(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_llpbk_we),
		.wd(ctrl_llpbk_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[88]),
		.qs(ctrl_llpbk_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_parity_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_parity_en_we),
		.wd(ctrl_parity_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[87]),
		.qs(ctrl_parity_en_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl_parity_odd(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_parity_odd_we),
		.wd(ctrl_parity_odd_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[86]),
		.qs(ctrl_parity_odd_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_ctrl_rxblvl(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_rxblvl_we),
		.wd(ctrl_rxblvl_wd),
		.de(1'b0),
		.d({2 {1'sb0}}),
		.qe(),
		.q(reg2hw[85-:2]),
		.qs(ctrl_rxblvl_qs)
	);
	prim_subreg #(
		.DW(16),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(16'h0000)
	) u_ctrl_nco(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_nco_we),
		.wd(ctrl_nco_wd),
		.de(1'b0),
		.d({16 {1'sb0}}),
		.qe(),
		.q(reg2hw[83-:16]),
		.qs(ctrl_nco_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_txfull(
		.re(status_txfull_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[48]),
		.qre(reg2hw[66]),
		.qe(),
		.q(reg2hw[67]),
		.qs(status_txfull_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_rxfull(
		.re(status_rxfull_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[47]),
		.qre(reg2hw[64]),
		.qe(),
		.q(reg2hw[65]),
		.qs(status_rxfull_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_txempty(
		.re(status_txempty_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[46]),
		.qre(reg2hw[62]),
		.qe(),
		.q(reg2hw[63]),
		.qs(status_txempty_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_txidle(
		.re(status_txidle_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[45]),
		.qre(reg2hw[60]),
		.qe(),
		.q(reg2hw[61]),
		.qs(status_txidle_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_rxidle(
		.re(status_rxidle_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[44]),
		.qre(reg2hw[58]),
		.qe(),
		.q(reg2hw[59]),
		.qs(status_rxidle_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_rxempty(
		.re(status_rxempty_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[43]),
		.qre(reg2hw[56]),
		.qe(),
		.q(reg2hw[57]),
		.qs(status_rxempty_qs)
	);
	prim_subreg_ext #(.DW(8)) u_rdata(
		.re(rdata_re),
		.we(1'b0),
		.wd({8 {1'sb0}}),
		.d(hw2reg[42-:8]),
		.qre(reg2hw[47]),
		.qe(),
		.q(reg2hw[55-:8]),
		.qs(rdata_qs)
	);
	prim_subreg #(
		.DW(8),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(8'h00)
	) u_wdata(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(wdata_we),
		.wd(wdata_wd),
		.de(1'b0),
		.d({8 {1'sb0}}),
		.qe(reg2hw[38]),
		.q(reg2hw[46-:8]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_fifo_ctrl_rxrst(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(fifo_ctrl_rxrst_we),
		.wd(fifo_ctrl_rxrst_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[36]),
		.q(reg2hw[37]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_fifo_ctrl_txrst(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(fifo_ctrl_txrst_we),
		.wd(fifo_ctrl_txrst_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(reg2hw[34]),
		.q(reg2hw[35]),
		.qs()
	);
	prim_subreg #(
		.DW(3),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(3'h0)
	) u_fifo_ctrl_rxilvl(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(fifo_ctrl_rxilvl_we),
		.wd(fifo_ctrl_rxilvl_wd),
		.de(hw2reg[31]),
		.d(hw2reg[34-:3]),
		.qe(reg2hw[30]),
		.q(reg2hw[33-:3]),
		.qs(fifo_ctrl_rxilvl_qs)
	);
	prim_subreg #(
		.DW(2),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(2'h0)
	) u_fifo_ctrl_txilvl(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(fifo_ctrl_txilvl_we),
		.wd(fifo_ctrl_txilvl_wd),
		.de(hw2reg[28]),
		.d(hw2reg[30-:2]),
		.qe(reg2hw[27]),
		.q(reg2hw[29-:2]),
		.qs(fifo_ctrl_txilvl_qs)
	);
	prim_subreg_ext #(.DW(6)) u_fifo_status_txlvl(
		.re(fifo_status_txlvl_re),
		.we(1'b0),
		.wd({6 {1'sb0}}),
		.d(hw2reg[27-:6]),
		.qre(),
		.qe(),
		.q(),
		.qs(fifo_status_txlvl_qs)
	);
	prim_subreg_ext #(.DW(6)) u_fifo_status_rxlvl(
		.re(fifo_status_rxlvl_re),
		.we(1'b0),
		.wd({6 {1'sb0}}),
		.d(hw2reg[21-:6]),
		.qre(),
		.qe(),
		.q(),
		.qs(fifo_status_rxlvl_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ovrd_txen(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ovrd_txen_we),
		.wd(ovrd_txen_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[26]),
		.qs(ovrd_txen_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ovrd_txval(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ovrd_txval_we),
		.wd(ovrd_txval_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[25]),
		.qs(ovrd_txval_qs)
	);
	prim_subreg_ext #(.DW(16)) u_val(
		.re(val_re),
		.we(1'b0),
		.wd({16 {1'sb0}}),
		.d(hw2reg[15-:16]),
		.qre(),
		.qe(),
		.q(),
		.qs(val_qs)
	);
	prim_subreg #(
		.DW(24),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(24'h000000)
	) u_timeout_ctrl_val(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(timeout_ctrl_val_we),
		.wd(timeout_ctrl_val_wd),
		.de(1'b0),
		.d({24 {1'sb0}}),
		.qe(),
		.q(reg2hw[24-:24]),
		.qs(timeout_ctrl_val_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_timeout_ctrl_en(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(timeout_ctrl_en_we),
		.wd(timeout_ctrl_en_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[-0]),
		.qs(timeout_ctrl_en_qs)
	);
	reg [11:0] addr_hit;
	always @(*) begin
		addr_hit = {12 {1'sb0}};
		addr_hit[0] = reg_addr == UART_INTR_STATE_OFFSET;
		addr_hit[1] = reg_addr == UART_INTR_ENABLE_OFFSET;
		addr_hit[2] = reg_addr == UART_INTR_TEST_OFFSET;
		addr_hit[3] = reg_addr == UART_CTRL_OFFSET;
		addr_hit[4] = reg_addr == UART_STATUS_OFFSET;
		addr_hit[5] = reg_addr == UART_RDATA_OFFSET;
		addr_hit[6] = reg_addr == UART_WDATA_OFFSET;
		addr_hit[7] = reg_addr == UART_FIFO_CTRL_OFFSET;
		addr_hit[8] = reg_addr == UART_FIFO_STATUS_OFFSET;
		addr_hit[9] = reg_addr == UART_OVRD_OFFSET;
		addr_hit[10] = reg_addr == UART_VAL_OFFSET;
		addr_hit[11] = reg_addr == UART_TIMEOUT_CTRL_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (UART_PERMIT[44+:4] != (UART_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (UART_PERMIT[40+:4] != (UART_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (UART_PERMIT[36+:4] != (UART_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (UART_PERMIT[32+:4] != (UART_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (UART_PERMIT[28+:4] != (UART_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (UART_PERMIT[24+:4] != (UART_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (UART_PERMIT[20+:4] != (UART_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (UART_PERMIT[16+:4] != (UART_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (UART_PERMIT[12+:4] != (UART_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (UART_PERMIT[8+:4] != (UART_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (UART_PERMIT[4+:4] != (UART_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (UART_PERMIT[0+:4] != (UART_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign intr_state_tx_watermark_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_tx_watermark_wd = reg_wdata[0];
	assign intr_state_rx_watermark_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_watermark_wd = reg_wdata[1];
	assign intr_state_tx_empty_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_tx_empty_wd = reg_wdata[2];
	assign intr_state_rx_overflow_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_overflow_wd = reg_wdata[3];
	assign intr_state_rx_frame_err_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_frame_err_wd = reg_wdata[4];
	assign intr_state_rx_break_err_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_break_err_wd = reg_wdata[5];
	assign intr_state_rx_timeout_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_timeout_wd = reg_wdata[6];
	assign intr_state_rx_parity_err_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_rx_parity_err_wd = reg_wdata[7];
	assign intr_enable_tx_watermark_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_tx_watermark_wd = reg_wdata[0];
	assign intr_enable_rx_watermark_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_watermark_wd = reg_wdata[1];
	assign intr_enable_tx_empty_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_tx_empty_wd = reg_wdata[2];
	assign intr_enable_rx_overflow_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_overflow_wd = reg_wdata[3];
	assign intr_enable_rx_frame_err_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_frame_err_wd = reg_wdata[4];
	assign intr_enable_rx_break_err_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_break_err_wd = reg_wdata[5];
	assign intr_enable_rx_timeout_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_timeout_wd = reg_wdata[6];
	assign intr_enable_rx_parity_err_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_rx_parity_err_wd = reg_wdata[7];
	assign intr_test_tx_watermark_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_tx_watermark_wd = reg_wdata[0];
	assign intr_test_rx_watermark_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_watermark_wd = reg_wdata[1];
	assign intr_test_tx_empty_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_tx_empty_wd = reg_wdata[2];
	assign intr_test_rx_overflow_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_overflow_wd = reg_wdata[3];
	assign intr_test_rx_frame_err_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_frame_err_wd = reg_wdata[4];
	assign intr_test_rx_break_err_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_break_err_wd = reg_wdata[5];
	assign intr_test_rx_timeout_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_timeout_wd = reg_wdata[6];
	assign intr_test_rx_parity_err_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_rx_parity_err_wd = reg_wdata[7];
	assign ctrl_tx_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_tx_wd = reg_wdata[0];
	assign ctrl_rx_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_rx_wd = reg_wdata[1];
	assign ctrl_nf_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_nf_wd = reg_wdata[2];
	assign ctrl_slpbk_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_slpbk_wd = reg_wdata[4];
	assign ctrl_llpbk_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_llpbk_wd = reg_wdata[5];
	assign ctrl_parity_en_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_parity_en_wd = reg_wdata[6];
	assign ctrl_parity_odd_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_parity_odd_wd = reg_wdata[7];
	assign ctrl_rxblvl_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_rxblvl_wd = reg_wdata[9:8];
	assign ctrl_nco_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign ctrl_nco_wd = reg_wdata[31:16];
	assign status_txfull_re = addr_hit[4] && reg_re;
	assign status_rxfull_re = addr_hit[4] && reg_re;
	assign status_txempty_re = addr_hit[4] && reg_re;
	assign status_txidle_re = addr_hit[4] && reg_re;
	assign status_rxidle_re = addr_hit[4] && reg_re;
	assign status_rxempty_re = addr_hit[4] && reg_re;
	assign rdata_re = addr_hit[5] && reg_re;
	assign wdata_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign wdata_wd = reg_wdata[7:0];
	assign fifo_ctrl_rxrst_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign fifo_ctrl_rxrst_wd = reg_wdata[0];
	assign fifo_ctrl_txrst_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign fifo_ctrl_txrst_wd = reg_wdata[1];
	assign fifo_ctrl_rxilvl_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign fifo_ctrl_rxilvl_wd = reg_wdata[4:2];
	assign fifo_ctrl_txilvl_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign fifo_ctrl_txilvl_wd = reg_wdata[6:5];
	assign fifo_status_txlvl_re = addr_hit[8] && reg_re;
	assign fifo_status_rxlvl_re = addr_hit[8] && reg_re;
	assign ovrd_txen_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign ovrd_txen_wd = reg_wdata[0];
	assign ovrd_txval_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign ovrd_txval_wd = reg_wdata[1];
	assign val_re = addr_hit[10] && reg_re;
	assign timeout_ctrl_val_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign timeout_ctrl_val_wd = reg_wdata[23:0];
	assign timeout_ctrl_en_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign timeout_ctrl_en_wd = reg_wdata[31];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: begin
				reg_rdata_next[0] = intr_state_tx_watermark_qs;
				reg_rdata_next[1] = intr_state_rx_watermark_qs;
				reg_rdata_next[2] = intr_state_tx_empty_qs;
				reg_rdata_next[3] = intr_state_rx_overflow_qs;
				reg_rdata_next[4] = intr_state_rx_frame_err_qs;
				reg_rdata_next[5] = intr_state_rx_break_err_qs;
				reg_rdata_next[6] = intr_state_rx_timeout_qs;
				reg_rdata_next[7] = intr_state_rx_parity_err_qs;
			end
			addr_hit[1]: begin
				reg_rdata_next[0] = intr_enable_tx_watermark_qs;
				reg_rdata_next[1] = intr_enable_rx_watermark_qs;
				reg_rdata_next[2] = intr_enable_tx_empty_qs;
				reg_rdata_next[3] = intr_enable_rx_overflow_qs;
				reg_rdata_next[4] = intr_enable_rx_frame_err_qs;
				reg_rdata_next[5] = intr_enable_rx_break_err_qs;
				reg_rdata_next[6] = intr_enable_rx_timeout_qs;
				reg_rdata_next[7] = intr_enable_rx_parity_err_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[2] = 1'sb0;
				reg_rdata_next[3] = 1'sb0;
				reg_rdata_next[4] = 1'sb0;
				reg_rdata_next[5] = 1'sb0;
				reg_rdata_next[6] = 1'sb0;
				reg_rdata_next[7] = 1'sb0;
			end
			addr_hit[3]: begin
				reg_rdata_next[0] = ctrl_tx_qs;
				reg_rdata_next[1] = ctrl_rx_qs;
				reg_rdata_next[2] = ctrl_nf_qs;
				reg_rdata_next[4] = ctrl_slpbk_qs;
				reg_rdata_next[5] = ctrl_llpbk_qs;
				reg_rdata_next[6] = ctrl_parity_en_qs;
				reg_rdata_next[7] = ctrl_parity_odd_qs;
				reg_rdata_next[9:8] = ctrl_rxblvl_qs;
				reg_rdata_next[31:16] = ctrl_nco_qs;
			end
			addr_hit[4]: begin
				reg_rdata_next[0] = status_txfull_qs;
				reg_rdata_next[1] = status_rxfull_qs;
				reg_rdata_next[2] = status_txempty_qs;
				reg_rdata_next[3] = status_txidle_qs;
				reg_rdata_next[4] = status_rxidle_qs;
				reg_rdata_next[5] = status_rxempty_qs;
			end
			addr_hit[5]: reg_rdata_next[7:0] = rdata_qs;
			addr_hit[6]: reg_rdata_next[7:0] = {8 {1'sb0}};
			addr_hit[7]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[4:2] = fifo_ctrl_rxilvl_qs;
				reg_rdata_next[6:5] = fifo_ctrl_txilvl_qs;
			end
			addr_hit[8]: begin
				reg_rdata_next[5:0] = fifo_status_txlvl_qs;
				reg_rdata_next[21:16] = fifo_status_rxlvl_qs;
			end
			addr_hit[9]: begin
				reg_rdata_next[0] = ovrd_txen_qs;
				reg_rdata_next[1] = ovrd_txval_qs;
			end
			addr_hit[10]: reg_rdata_next[15:0] = val_qs;
			addr_hit[11]: begin
				reg_rdata_next[23:0] = timeout_ctrl_val_qs;
				reg_rdata_next[31] = timeout_ctrl_en_qs;
			end
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module uart_rx (
	clk_i,
	rst_ni,
	rx_enable,
	tick_baud_x16,
	parity_enable,
	parity_odd,
	tick_baud,
	rx_valid,
	rx_data,
	idle,
	frame_err,
	rx_parity_err,
	rx
);
	input clk_i;
	input rst_ni;
	input rx_enable;
	input tick_baud_x16;
	input parity_enable;
	input parity_odd;
	output wire tick_baud;
	output wire rx_valid;
	output [7:0] rx_data;
	output wire idle;
	output frame_err;
	output rx_parity_err;
	input rx;
	reg rx_valid_q;
	reg [10:0] sreg_q;
	reg [10:0] sreg_d;
	reg [3:0] bit_cnt_q;
	reg [3:0] bit_cnt_d;
	reg [3:0] baud_div_q;
	reg [3:0] baud_div_d;
	reg tick_baud_d;
	reg tick_baud_q;
	reg idle_d;
	reg idle_q;
	assign tick_baud = tick_baud_q;
	assign idle = idle_q;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			sreg_q <= 11'h000;
			bit_cnt_q <= 4'h0;
			baud_div_q <= 4'h0;
			tick_baud_q <= 1'b0;
			idle_q <= 1'b1;
		end
		else begin
			sreg_q <= sreg_d;
			bit_cnt_q <= bit_cnt_d;
			baud_div_q <= baud_div_d;
			tick_baud_q <= tick_baud_d;
			idle_q <= idle_d;
		end
	always @(*)
		if (!rx_enable) begin
			sreg_d = 11'h000;
			bit_cnt_d = 4'h0;
			baud_div_d = 4'h0;
			tick_baud_d = 1'b0;
			idle_d = 1'b1;
		end
		else begin
			tick_baud_d = 1'b0;
			sreg_d = sreg_q;
			bit_cnt_d = bit_cnt_q;
			baud_div_d = baud_div_q;
			idle_d = idle_q;
			if (tick_baud_x16)
				{tick_baud_d, baud_div_d} = {1'b0, baud_div_q} + 5'h01;
			if (idle_q && !rx) begin
				baud_div_d = 4'd8;
				tick_baud_d = 1'b0;
				bit_cnt_d = (parity_enable ? 4'd11 : 4'd10);
				sreg_d = 11'h000;
				idle_d = 1'b0;
			end
			else if (!idle_q && tick_baud_q)
				if ((bit_cnt_q == (parity_enable ? 4'd11 : 4'd10)) && rx) begin
					idle_d = 1'b1;
					bit_cnt_d = 4'h0;
				end
				else begin
					sreg_d = {rx, sreg_q[10:1]};
					bit_cnt_d = bit_cnt_q - 4'h1;
					idle_d = bit_cnt_q == 4'h1;
				end
		end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			rx_valid_q <= 1'b0;
		else
			rx_valid_q <= tick_baud_q & (bit_cnt_q == 4'h1);
	assign rx_valid = rx_valid_q;
	assign rx_data = (parity_enable ? sreg_q[8:1] : sreg_q[9:2]);
	assign frame_err = rx_valid_q & ~sreg_q[10];
	assign rx_parity_err = (parity_enable & rx_valid_q) & ^{sreg_q[9:1], parity_odd};
endmodule
module uart_tx (
	clk_i,
	rst_ni,
	tx_enable,
	tick_baud_x16,
	parity_enable,
	wr,
	wr_parity,
	wr_data,
	idle,
	tx
);
	input clk_i;
	input rst_ni;
	input tx_enable;
	input tick_baud_x16;
	input wire parity_enable;
	input wr;
	input wire wr_parity;
	input [7:0] wr_data;
	output idle;
	output wire tx;
	reg [3:0] baud_div_q;
	reg tick_baud_q;
	reg [3:0] bit_cnt_q;
	reg [3:0] bit_cnt_d;
	reg [10:0] sreg_q;
	reg [10:0] sreg_d;
	reg tx_q;
	reg tx_d;
	assign tx = tx_q;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			baud_div_q <= 4'h0;
			tick_baud_q <= 1'b0;
		end
		else if (tick_baud_x16)
			{tick_baud_q, baud_div_q} <= {1'b0, baud_div_q} + 5'h01;
		else
			tick_baud_q <= 1'b0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			bit_cnt_q <= 4'h0;
			sreg_q <= 11'h7ff;
			tx_q <= 1'b1;
		end
		else begin
			bit_cnt_q <= bit_cnt_d;
			sreg_q <= sreg_d;
			tx_q <= tx_d;
		end
	always @(*)
		if (!tx_enable) begin
			bit_cnt_d = 4'h0;
			sreg_d = 11'h7ff;
			tx_d = 1'b1;
		end
		else begin
			bit_cnt_d = bit_cnt_q;
			sreg_d = sreg_q;
			tx_d = tx_q;
			if (wr) begin
				sreg_d = {1'b1, (parity_enable ? wr_parity : 1'b1), wr_data, 1'b0};
				bit_cnt_d = (parity_enable ? 4'd11 : 4'd10);
			end
			else if (tick_baud_q && (bit_cnt_q != 4'h0)) begin
				sreg_d = {1'b1, sreg_q[10:1]};
				tx_d = sreg_q[0];
				bit_cnt_d = bit_cnt_q - 4'h1;
			end
		end
	assign idle = (tx_enable ? bit_cnt_q == 4'h0 : 1'b1);
endmodule
module uart_core (
	clk_i,
	rst_ni,
	reg2hw,
	hw2reg,
	rx,
	tx,
	intr_tx_watermark_o,
	intr_rx_watermark_o,
	intr_tx_empty_o,
	intr_rx_overflow_o,
	intr_rx_frame_err_o,
	intr_rx_break_err_o,
	intr_rx_timeout_o,
	intr_rx_parity_err_o
);
	input clk_i;
	input rst_ni;
	input wire [124:0] reg2hw;
	output wire [64:0] hw2reg;
	input rx;
	output wire tx;
	output wire intr_tx_watermark_o;
	output wire intr_rx_watermark_o;
	output wire intr_tx_empty_o;
	output wire intr_rx_overflow_o;
	output wire intr_rx_frame_err_o;
	output wire intr_rx_break_err_o;
	output wire intr_rx_timeout_o;
	output wire intr_rx_parity_err_o;
	localparam [5:0] UART_INTR_STATE_OFFSET = 6'h00;
	localparam [5:0] UART_INTR_ENABLE_OFFSET = 6'h04;
	localparam [5:0] UART_INTR_TEST_OFFSET = 6'h08;
	localparam [5:0] UART_CTRL_OFFSET = 6'h0c;
	localparam [5:0] UART_STATUS_OFFSET = 6'h10;
	localparam [5:0] UART_RDATA_OFFSET = 6'h14;
	localparam [5:0] UART_WDATA_OFFSET = 6'h18;
	localparam [5:0] UART_FIFO_CTRL_OFFSET = 6'h1c;
	localparam [5:0] UART_FIFO_STATUS_OFFSET = 6'h20;
	localparam [5:0] UART_OVRD_OFFSET = 6'h24;
	localparam [5:0] UART_VAL_OFFSET = 6'h28;
	localparam [5:0] UART_TIMEOUT_CTRL_OFFSET = 6'h2c;
	localparam signed [31:0] UART_INTR_STATE = 0;
	localparam signed [31:0] UART_INTR_ENABLE = 1;
	localparam signed [31:0] UART_INTR_TEST = 2;
	localparam signed [31:0] UART_CTRL = 3;
	localparam signed [31:0] UART_STATUS = 4;
	localparam signed [31:0] UART_RDATA = 5;
	localparam signed [31:0] UART_WDATA = 6;
	localparam signed [31:0] UART_FIFO_CTRL = 7;
	localparam signed [31:0] UART_FIFO_STATUS = 8;
	localparam signed [31:0] UART_OVRD = 9;
	localparam signed [31:0] UART_VAL = 10;
	localparam signed [31:0] UART_TIMEOUT_CTRL = 11;
	localparam [47:0] UART_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b1111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0111, 4'b0001, 4'b0011, 4'b1111};
	reg [15:0] rx_val_q;
	wire [7:0] uart_rdata;
	wire tick_baud_x16;
	wire rx_tick_baud;
	wire [5:0] tx_fifo_depth;
	wire [5:0] rx_fifo_depth;
	reg [5:0] rx_fifo_depth_prev_q;
	wire [23:0] rx_timeout_count_d;
	reg [23:0] rx_timeout_count_q;
	wire [23:0] uart_rxto_val;
	wire rx_fifo_depth_changed;
	wire uart_rxto_en;
	wire tx_enable;
	wire rx_enable;
	wire sys_loopback;
	wire line_loopback;
	wire rxnf_enable;
	wire uart_fifo_rxrst;
	wire uart_fifo_txrst;
	wire [2:0] uart_fifo_rxilvl;
	wire [1:0] uart_fifo_txilvl;
	wire ovrd_tx_en;
	wire ovrd_tx_val;
	wire [7:0] tx_fifo_data;
	wire tx_fifo_rready;
	wire tx_fifo_rvalid;
	wire tx_fifo_wready;
	wire tx_uart_idle;
	wire tx_out;
	reg tx_out_q;
	wire [7:0] rx_fifo_data;
	wire rx_valid;
	wire rx_fifo_wvalid;
	wire rx_fifo_rvalid;
	wire rx_fifo_wready;
	wire rx_uart_idle;
	wire rx_sync;
	wire rx_in;
	reg break_err;
	wire [4:0] allzero_cnt_d;
	reg [4:0] allzero_cnt_q;
	wire allzero_err;
	wire not_allzero_char;
	wire event_tx_watermark;
	reg event_rx_watermark;
	wire event_tx_empty;
	wire event_rx_overflow;
	wire event_rx_frame_err;
	wire event_rx_break_err;
	wire event_rx_timeout;
	wire event_rx_parity_err;
	reg tx_watermark_d;
	reg tx_watermark_prev_q;
	wire tx_empty_d;
	reg tx_empty_prev_q;
	assign tx_enable = reg2hw[92];
	assign rx_enable = reg2hw[91];
	assign rxnf_enable = reg2hw[90];
	assign sys_loopback = reg2hw[89];
	assign line_loopback = reg2hw[88];
	assign uart_fifo_rxrst = reg2hw[37] & reg2hw[36];
	assign uart_fifo_txrst = reg2hw[35] & reg2hw[34];
	assign uart_fifo_rxilvl = reg2hw[33-:3];
	assign uart_fifo_txilvl = reg2hw[29-:2];
	assign ovrd_tx_en = reg2hw[26];
	assign ovrd_tx_val = reg2hw[25];
	reg break_st_q;
	assign not_allzero_char = rx_valid & (~event_rx_frame_err | (rx_fifo_data != 8'h00));
	assign allzero_err = event_rx_frame_err & (rx_fifo_data == 8'h00);
	localparam [0:0] BRK_WAIT = 1;
	assign allzero_cnt_d = ((break_st_q == BRK_WAIT) || not_allzero_char ? 5'h00 : (allzero_err ? allzero_cnt_q + 5'd1 : allzero_cnt_q));
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			allzero_cnt_q <= {5 {1'sb0}};
		else if (rx_enable)
			allzero_cnt_q <= allzero_cnt_d;
	always @(*)
		case (reg2hw[85-:2])
			2'h0: break_err = allzero_cnt_d >= 5'd2;
			2'h1: break_err = allzero_cnt_d >= 5'd4;
			2'h2: break_err = allzero_cnt_d >= 5'd8;
			default: break_err = allzero_cnt_d >= 5'd16;
		endcase
	localparam [0:0] BRK_CHK = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			break_st_q <= BRK_CHK;
		else
			case (break_st_q)
				BRK_CHK:
					if (event_rx_break_err)
						break_st_q <= BRK_WAIT;
				BRK_WAIT:
					if (rx_in)
						break_st_q <= BRK_CHK;
				default: break_st_q <= BRK_CHK;
			endcase
	assign hw2reg[15-:16] = rx_val_q;
	assign hw2reg[42-:8] = uart_rdata;
	assign hw2reg[43] = ~rx_fifo_rvalid;
	assign hw2reg[44] = rx_uart_idle;
	assign hw2reg[45] = tx_uart_idle & ~tx_fifo_rvalid;
	assign hw2reg[46] = ~tx_fifo_rvalid;
	assign hw2reg[47] = ~rx_fifo_wready;
	assign hw2reg[48] = ~tx_fifo_wready;
	assign hw2reg[27-:6] = tx_fifo_depth;
	assign hw2reg[21-:6] = rx_fifo_depth;
	assign hw2reg[31] = 1'b0;
	assign hw2reg[34-:3] = 3'h0;
	assign hw2reg[28] = 1'b0;
	assign hw2reg[30-:2] = 2'h0;
	reg [16:0] nco_sum_q;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			nco_sum_q <= 17'h00000;
		else if (tx_enable || rx_enable)
			nco_sum_q <= {1'b0, nco_sum_q[15:0]} + {1'b0, reg2hw[83-:16]};
	assign tick_baud_x16 = nco_sum_q[16];
	assign tx_fifo_rready = (tx_uart_idle & tx_fifo_rvalid) & tx_enable;
	prim_fifo_sync #(
		.Width(8),
		.Pass(1'b0),
		.Depth(32)
	) u_uart_txfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(uart_fifo_txrst),
		.wvalid(reg2hw[38]),
		.wready(tx_fifo_wready),
		.wdata(reg2hw[46-:8]),
		.depth(tx_fifo_depth),
		.rvalid(tx_fifo_rvalid),
		.rready(tx_fifo_rready),
		.rdata(tx_fifo_data)
	);
	uart_tx uart_tx(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tx_enable(tx_enable),
		.tick_baud_x16(tick_baud_x16),
		.parity_enable(reg2hw[87]),
		.wr(tx_fifo_rready),
		.wr_parity(^tx_fifo_data ^ reg2hw[86]),
		.wr_data(tx_fifo_data),
		.idle(tx_uart_idle),
		.tx(tx_out)
	);
	assign tx = (line_loopback ? rx : tx_out_q);
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			tx_out_q <= 1'b1;
		else if (ovrd_tx_en)
			tx_out_q <= ovrd_tx_val;
		else if (sys_loopback)
			tx_out_q <= 1'b1;
		else
			tx_out_q <= tx_out;
	prim_flop_2sync #(
		.Width(1),
		.ResetValue(1)
	) sync_rx(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.d(rx),
		.q(rx_sync)
	);
	reg rx_sync_q1;
	reg rx_sync_q2;
	wire rx_in_mx;
	wire rx_in_maj;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			rx_sync_q1 <= 1'b1;
			rx_sync_q2 <= 1'b1;
		end
		else begin
			rx_sync_q1 <= rx_sync;
			rx_sync_q2 <= rx_sync_q1;
		end
	assign rx_in_maj = ((rx_sync & rx_sync_q1) | (rx_sync & rx_sync_q2)) | (rx_sync_q1 & rx_sync_q2);
	assign rx_in_mx = (rxnf_enable ? rx_in_maj : rx_sync);
	assign rx_in = (sys_loopback ? tx_out : (line_loopback ? 1'b1 : rx_in_mx));
	uart_rx uart_rx(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.rx_enable(rx_enable),
		.tick_baud_x16(tick_baud_x16),
		.parity_enable(reg2hw[87]),
		.parity_odd(reg2hw[86]),
		.tick_baud(rx_tick_baud),
		.rx_valid(rx_valid),
		.rx_data(rx_fifo_data),
		.idle(rx_uart_idle),
		.frame_err(event_rx_frame_err),
		.rx(rx_in),
		.rx_parity_err(event_rx_parity_err)
	);
	assign rx_fifo_wvalid = (rx_valid & ~event_rx_frame_err) & ~event_rx_parity_err;
	prim_fifo_sync #(
		.Width(8),
		.Pass(1'b0),
		.Depth(32)
	) u_uart_rxfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(uart_fifo_rxrst),
		.wvalid(rx_fifo_wvalid),
		.wready(rx_fifo_wready),
		.wdata(rx_fifo_data),
		.depth(rx_fifo_depth),
		.rvalid(rx_fifo_rvalid),
		.rready(reg2hw[47]),
		.rdata(uart_rdata)
	);
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			rx_val_q <= 16'h0000;
		else if (tick_baud_x16)
			rx_val_q <= {rx_val_q[14:0], rx_in};
	always @(*)
		case (uart_fifo_txilvl)
			2'h0: tx_watermark_d = tx_fifo_depth < 6'd1;
			2'h1: tx_watermark_d = tx_fifo_depth < 6'd4;
			2'h2: tx_watermark_d = tx_fifo_depth < 6'd8;
			default: tx_watermark_d = tx_fifo_depth < 6'd16;
		endcase
	assign event_tx_watermark = tx_watermark_d & ~tx_watermark_prev_q;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			tx_watermark_prev_q <= 1'd1;
		else
			tx_watermark_prev_q <= tx_watermark_d;
	always @(*)
		case (uart_fifo_rxilvl)
			3'h0: event_rx_watermark = rx_fifo_depth >= 6'd1;
			3'h1: event_rx_watermark = rx_fifo_depth >= 6'd4;
			3'h2: event_rx_watermark = rx_fifo_depth >= 6'd8;
			3'h3: event_rx_watermark = rx_fifo_depth >= 6'd16;
			3'h4: event_rx_watermark = rx_fifo_depth >= 6'd30;
			default: event_rx_watermark = 1'b0;
		endcase
	assign uart_rxto_en = reg2hw[-0];
	assign uart_rxto_val = reg2hw[24-:24];
	assign rx_fifo_depth_changed = rx_fifo_depth != rx_fifo_depth_prev_q;
	assign rx_timeout_count_d = (uart_rxto_en == 1'b0 ? 24'd0 : (event_rx_timeout ? 24'd0 : (rx_fifo_depth_changed ? 24'd0 : (rx_fifo_depth == 5'd0 ? 24'd0 : (rx_tick_baud ? rx_timeout_count_q + 24'd1 : rx_timeout_count_q)))));
	assign event_rx_timeout = (rx_timeout_count_q == uart_rxto_val) & uart_rxto_en;
	assign tx_empty_d = tx_fifo_depth == 6'h00;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			rx_timeout_count_q <= 24'd0;
			rx_fifo_depth_prev_q <= 6'd0;
			tx_empty_prev_q <= 1'd0;
		end
		else begin
			rx_timeout_count_q <= rx_timeout_count_d;
			rx_fifo_depth_prev_q <= rx_fifo_depth;
			tx_empty_prev_q <= tx_empty_d;
		end
	assign event_rx_overflow = rx_fifo_wvalid & ~rx_fifo_wready;
	assign event_rx_break_err = break_err & (break_st_q == BRK_CHK);
	assign event_tx_empty = tx_empty_d & ~tx_empty_prev_q;
	prim_intr_hw #(.Width(1)) intr_hw_tx_watermark(
		.event_intr_i(event_tx_watermark),
		.reg2hw_intr_enable_q_i(reg2hw[116]),
		.reg2hw_intr_test_q_i(reg2hw[108]),
		.reg2hw_intr_test_qe_i(reg2hw[107]),
		.reg2hw_intr_state_q_i(reg2hw[124]),
		.hw2reg_intr_state_de_o(hw2reg[63]),
		.hw2reg_intr_state_d_o(hw2reg[64]),
		.intr_o(intr_tx_watermark_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_watermark(
		.event_intr_i(event_rx_watermark),
		.reg2hw_intr_enable_q_i(reg2hw[115]),
		.reg2hw_intr_test_q_i(reg2hw[106]),
		.reg2hw_intr_test_qe_i(reg2hw[105]),
		.reg2hw_intr_state_q_i(reg2hw[123]),
		.hw2reg_intr_state_de_o(hw2reg[61]),
		.hw2reg_intr_state_d_o(hw2reg[62]),
		.intr_o(intr_rx_watermark_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_tx_empty(
		.event_intr_i(event_tx_empty),
		.reg2hw_intr_enable_q_i(reg2hw[114]),
		.reg2hw_intr_test_q_i(reg2hw[104]),
		.reg2hw_intr_test_qe_i(reg2hw[103]),
		.reg2hw_intr_state_q_i(reg2hw[122]),
		.hw2reg_intr_state_de_o(hw2reg[59]),
		.hw2reg_intr_state_d_o(hw2reg[60]),
		.intr_o(intr_tx_empty_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_overflow(
		.event_intr_i(event_rx_overflow),
		.reg2hw_intr_enable_q_i(reg2hw[113]),
		.reg2hw_intr_test_q_i(reg2hw[102]),
		.reg2hw_intr_test_qe_i(reg2hw[101]),
		.reg2hw_intr_state_q_i(reg2hw[121]),
		.hw2reg_intr_state_de_o(hw2reg[57]),
		.hw2reg_intr_state_d_o(hw2reg[58]),
		.intr_o(intr_rx_overflow_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_frame_err(
		.event_intr_i(event_rx_frame_err),
		.reg2hw_intr_enable_q_i(reg2hw[112]),
		.reg2hw_intr_test_q_i(reg2hw[100]),
		.reg2hw_intr_test_qe_i(reg2hw[99]),
		.reg2hw_intr_state_q_i(reg2hw[120]),
		.hw2reg_intr_state_de_o(hw2reg[55]),
		.hw2reg_intr_state_d_o(hw2reg[56]),
		.intr_o(intr_rx_frame_err_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_break_err(
		.event_intr_i(event_rx_break_err),
		.reg2hw_intr_enable_q_i(reg2hw[111]),
		.reg2hw_intr_test_q_i(reg2hw[98]),
		.reg2hw_intr_test_qe_i(reg2hw[97]),
		.reg2hw_intr_state_q_i(reg2hw[119]),
		.hw2reg_intr_state_de_o(hw2reg[53]),
		.hw2reg_intr_state_d_o(hw2reg[54]),
		.intr_o(intr_rx_break_err_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_timeout(
		.event_intr_i(event_rx_timeout),
		.reg2hw_intr_enable_q_i(reg2hw[110]),
		.reg2hw_intr_test_q_i(reg2hw[96]),
		.reg2hw_intr_test_qe_i(reg2hw[95]),
		.reg2hw_intr_state_q_i(reg2hw[118]),
		.hw2reg_intr_state_de_o(hw2reg[51]),
		.hw2reg_intr_state_d_o(hw2reg[52]),
		.intr_o(intr_rx_timeout_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_rx_parity_err(
		.event_intr_i(event_rx_parity_err),
		.reg2hw_intr_enable_q_i(reg2hw[109]),
		.reg2hw_intr_test_q_i(reg2hw[94]),
		.reg2hw_intr_test_qe_i(reg2hw[93]),
		.reg2hw_intr_state_q_i(reg2hw[117]),
		.hw2reg_intr_state_de_o(hw2reg[49]),
		.hw2reg_intr_state_d_o(hw2reg[50]),
		.intr_o(intr_rx_parity_err_o)
	);
endmodule
module uart (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	cio_rx_i,
	cio_tx_o,
	cio_tx_en_o,
	intr_tx_watermark_o,
	intr_rx_watermark_o,
	intr_tx_empty_o,
	intr_rx_overflow_o,
	intr_rx_frame_err_o,
	intr_rx_break_err_o,
	intr_rx_timeout_o,
	intr_rx_parity_err_o
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	input cio_rx_i;
	output wire cio_tx_o;
	output wire cio_tx_en_o;
	output wire intr_tx_watermark_o;
	output wire intr_rx_watermark_o;
	output wire intr_tx_empty_o;
	output wire intr_rx_overflow_o;
	output wire intr_rx_frame_err_o;
	output wire intr_rx_break_err_o;
	output wire intr_rx_timeout_o;
	output wire intr_rx_parity_err_o;
	localparam [5:0] UART_INTR_STATE_OFFSET = 6'h00;
	localparam [5:0] UART_INTR_ENABLE_OFFSET = 6'h04;
	localparam [5:0] UART_INTR_TEST_OFFSET = 6'h08;
	localparam [5:0] UART_CTRL_OFFSET = 6'h0c;
	localparam [5:0] UART_STATUS_OFFSET = 6'h10;
	localparam [5:0] UART_RDATA_OFFSET = 6'h14;
	localparam [5:0] UART_WDATA_OFFSET = 6'h18;
	localparam [5:0] UART_FIFO_CTRL_OFFSET = 6'h1c;
	localparam [5:0] UART_FIFO_STATUS_OFFSET = 6'h20;
	localparam [5:0] UART_OVRD_OFFSET = 6'h24;
	localparam [5:0] UART_VAL_OFFSET = 6'h28;
	localparam [5:0] UART_TIMEOUT_CTRL_OFFSET = 6'h2c;
	localparam signed [31:0] UART_INTR_STATE = 0;
	localparam signed [31:0] UART_INTR_ENABLE = 1;
	localparam signed [31:0] UART_INTR_TEST = 2;
	localparam signed [31:0] UART_CTRL = 3;
	localparam signed [31:0] UART_STATUS = 4;
	localparam signed [31:0] UART_RDATA = 5;
	localparam signed [31:0] UART_WDATA = 6;
	localparam signed [31:0] UART_FIFO_CTRL = 7;
	localparam signed [31:0] UART_FIFO_STATUS = 8;
	localparam signed [31:0] UART_OVRD = 9;
	localparam signed [31:0] UART_VAL = 10;
	localparam signed [31:0] UART_TIMEOUT_CTRL = 11;
	localparam [47:0] UART_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b1111, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0111, 4'b0001, 4'b0011, 4'b1111};
	wire [124:0] reg2hw;
	wire [64:0] hw2reg;
	uart_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	uart_core uart_core(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.rx(cio_rx_i),
		.tx(cio_tx_o),
		.intr_tx_watermark_o(intr_tx_watermark_o),
		.intr_rx_watermark_o(intr_rx_watermark_o),
		.intr_tx_empty_o(intr_tx_empty_o),
		.intr_rx_overflow_o(intr_rx_overflow_o),
		.intr_rx_frame_err_o(intr_rx_frame_err_o),
		.intr_rx_break_err_o(intr_rx_break_err_o),
		.intr_rx_timeout_o(intr_rx_timeout_o),
		.intr_rx_parity_err_o(intr_rx_parity_err_o)
	);
	assign cio_tx_en_o = 1'b1;
endmodule
module prim_clock_inverter (
	clk_i,
	scanmode_i,
	clk_no
);
	parameter [0:0] HasScanMode = 1'b1;
	input clk_i;
	input scanmode_i;
	output wire clk_no;
	generate
		if (HasScanMode) begin : gen_scan
			prim_clock_mux2 i_dft_tck_mux(
				.clk0_i(~clk_i),
				.clk1_i(clk_i),
				.sel_i(scanmode_i),
				.clk_o(clk_no)
			);
		end
		else begin : gen_noscan
			wire unused_scanmode;
			assign unused_scanmode = scanmode_i;
			assign clk_no = ~clk_i;
		end
	endgenerate
endmodule
module prim_alert_receiver (
	clk_i,
	rst_ni,
	ping_en_i,
	ping_ok_o,
	integ_fail_o,
	alert_o,
	alert_rx_o,
	alert_tx_i
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	parameter [0:0] AsyncOn = 1'b0;
	input clk_i;
	input rst_ni;
	input ping_en_i;
	output reg ping_ok_o;
	output reg integ_fail_o;
	output reg alert_o;
	output wire [3:0] alert_rx_o;
	input wire [1:0] alert_tx_i;
	wire alert_level;
	wire alert_sigint;
	prim_diff_decode #(.AsyncOn(AsyncOn)) i_decode_alert(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.diff_pi(alert_tx_i[1]),
		.diff_ni(alert_tx_i[0]),
		.level_o(alert_level),
		.rise_o(),
		.fall_o(),
		.event_o(),
		.sigint_o(alert_sigint)
	);
	reg [1:0] state_d;
	reg [1:0] state_q;
	wire ping_rise;
	wire ping_tog_d;
	reg ping_tog_q;
	reg ack_d;
	reg ack_q;
	wire ping_en_d;
	reg ping_en_q;
	wire ping_pending_d;
	reg ping_pending_q;
	assign ping_en_d = ping_en_i;
	assign ping_rise = ping_en_i && !ping_en_q;
	assign ping_tog_d = (ping_rise ? ~ping_tog_q : ping_tog_q);
	assign ping_pending_d = ping_rise | ((~ping_ok_o & ping_en_i) & ping_pending_q);
	assign alert_rx_o[1] = ack_q;
	assign alert_rx_o[0] = ~ack_q;
	assign alert_rx_o[3] = ping_tog_q;
	assign alert_rx_o[2] = ~ping_tog_q;
	localparam [1:0] HsAckWait = 1;
	localparam [1:0] Idle = 0;
	localparam [1:0] Pause0 = 2;
	localparam [1:0] Pause1 = 3;
	always @(*) begin : p_fsm
		state_d = state_q;
		ack_d = 1'b0;
		ping_ok_o = 1'b0;
		integ_fail_o = 1'b0;
		alert_o = 1'b0;
		case (state_q)
			Idle:
				if (alert_level) begin
					state_d = HsAckWait;
					ack_d = 1'b1;
					if (ping_pending_q)
						ping_ok_o = 1'b1;
					else
						alert_o = 1'b1;
				end
			HsAckWait:
				if (!alert_level)
					state_d = Pause0;
				else
					ack_d = 1'b1;
			Pause0: state_d = Pause1;
			Pause1: state_d = Idle;
			default:
				;
		endcase
		if (alert_sigint) begin
			state_d = Idle;
			ack_d = 1'b0;
			ping_ok_o = 1'b0;
			integ_fail_o = 1'b1;
			alert_o = 1'b0;
		end
	end
	always @(posedge clk_i or negedge rst_ni) begin : p_reg
		if (!rst_ni) begin
			state_q <= Idle;
			ack_q <= 1'b0;
			ping_tog_q <= 1'b0;
			ping_en_q <= 1'b0;
			ping_pending_q <= 1'b0;
		end
		else begin
			state_q <= state_d;
			ack_q <= ack_d;
			ping_tog_q <= ping_tog_d;
			ping_en_q <= ping_en_d;
			ping_pending_q <= ping_pending_d;
		end
	end
endmodule
module prim_alert_sender (
	clk_i,
	rst_ni,
	alert_i,
	alert_rx_i,
	alert_tx_o
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	parameter [0:0] AsyncOn = 1'b1;
	input clk_i;
	input rst_ni;
	input alert_i;
	input wire [3:0] alert_rx_i;
	output wire [1:0] alert_tx_o;
	wire ping_sigint;
	wire ping_event;
	prim_diff_decode #(.AsyncOn(AsyncOn)) i_decode_ping(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.diff_pi(alert_rx_i[3]),
		.diff_ni(alert_rx_i[2]),
		.level_o(),
		.rise_o(),
		.fall_o(),
		.event_o(ping_event),
		.sigint_o(ping_sigint)
	);
	wire ack_sigint;
	wire ack_level;
	prim_diff_decode #(.AsyncOn(AsyncOn)) i_decode_ack(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.diff_pi(alert_rx_i[1]),
		.diff_ni(alert_rx_i[0]),
		.level_o(ack_level),
		.rise_o(),
		.fall_o(),
		.event_o(),
		.sigint_o(ack_sigint)
	);
	reg [2:0] state_d;
	reg [2:0] state_q;
	reg alert_pq;
	reg alert_nq;
	reg alert_pd;
	reg alert_nd;
	wire sigint_detected;
	assign sigint_detected = ack_sigint | ping_sigint;
	assign alert_tx_o[1] = alert_pq;
	assign alert_tx_o[0] = alert_nq;
	wire alert_set_d;
	reg alert_set_q;
	reg alert_clr;
	wire ping_set_d;
	reg ping_set_q;
	reg ping_clr;
	assign alert_set_d = (alert_clr ? 1'b0 : alert_set_q | alert_i);
	assign ping_set_d = (ping_clr ? 1'b0 : ping_set_q | ping_event);
	localparam [2:0] HsPhase1 = 1;
	localparam [2:0] HsPhase2 = 2;
	localparam [2:0] Idle = 0;
	localparam [2:0] Pause0 = 4;
	localparam [2:0] Pause1 = 5;
	localparam [2:0] SigInt = 3;
	always @(*) begin : p_fsm
		state_d = state_q;
		alert_pd = 1'b0;
		alert_nd = 1'b1;
		ping_clr = 1'b0;
		alert_clr = 1'b0;
		case (state_q)
			Idle:
				if (((alert_i || alert_set_q) || ping_event) || ping_set_q) begin
					state_d = HsPhase1;
					alert_pd = 1'b1;
					alert_nd = 1'b0;
					if (ping_event || ping_set_q)
						ping_clr = 1'b1;
					else
						alert_clr = 1'b1;
				end
			HsPhase1:
				if (ack_level)
					state_d = HsPhase2;
				else begin
					alert_pd = 1'b1;
					alert_nd = 1'b0;
				end
			HsPhase2:
				if (!ack_level)
					state_d = Pause0;
			Pause0: state_d = Pause1;
			Pause1: state_d = Idle;
			SigInt: begin
				state_d = Idle;
				if (sigint_detected) begin
					state_d = SigInt;
					alert_pd = ~alert_pq;
					alert_nd = ~alert_pq;
				end
			end
			default: state_d = Idle;
		endcase
		if (sigint_detected && (state_q != SigInt)) begin
			state_d = SigInt;
			alert_pd = 1'b0;
			alert_nd = 1'b0;
			ping_clr = 1'b0;
			alert_clr = 1'b0;
		end
	end
	always @(posedge clk_i or negedge rst_ni) begin : p_reg
		if (!rst_ni) begin
			state_q <= Idle;
			alert_pq <= 1'b0;
			alert_nq <= 1'b1;
			alert_set_q <= 1'b0;
			ping_set_q <= 1'b0;
		end
		else begin
			state_q <= state_d;
			alert_pq <= alert_pd;
			alert_nq <= alert_nd;
			alert_set_q <= alert_set_d;
			ping_set_q <= ping_set_d;
		end
	end
endmodule
module prim_arbiter (
	clk_i,
	rst_ni,
	req_i,
	data_i,
	gnt_o,
	idx_o,
	valid_o,
	data_o,
	ready_i
);
	parameter signed [31:0] N = 4;
	parameter signed [31:0] DW = 32;
	input clk_i;
	input rst_ni;
	input [N - 1:0] req_i;
	input [(0 >= (N - 1) ? ((2 - N) * DW) + (((N - 1) * DW) - 1) : (N * DW) - 1):(0 >= (N - 1) ? (N - 1) * DW : 0)] data_i;
	output wire [N - 1:0] gnt_o;
	output reg [$clog2(N) - 1:0] idx_o;
	output wire valid_o;
	output reg [DW - 1:0] data_o;
	input ready_i;
	wire [N - 1:0] masked_req;
	reg [N - 1:0] ppc_out;
	wire [N - 1:0] arb_req;
	reg [N - 1:0] mask;
	wire [N - 1:0] mask_next;
	wire [N - 1:0] winner;
	assign masked_req = mask & req_i;
	assign arb_req = (|masked_req ? masked_req : req_i);
	always @(*) begin
		ppc_out[0] = arb_req[0];
		begin : sv2v_autoblock_231
			reg signed [31:0] i;
			for (i = 1; i < N; i = i + 1)
				ppc_out[i] = ppc_out[i - 1] | arb_req[i];
		end
	end
	assign winner = ppc_out ^ {ppc_out[N - 2:0], 1'b0};
	assign gnt_o = (ready_i ? winner : {N {1'sb0}});
	assign valid_o = |req_i;
	assign mask_next = {ppc_out[N - 2:0], 1'b0};
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			mask <= {N {1'sb0}};
		else if (valid_o && ready_i)
			mask <= mask_next;
		else if (valid_o && !ready_i)
			mask <= ppc_out;
	always @(*) begin
		data_o = {DW {1'sb0}};
		idx_o = {$clog2(N) {1'sb0}};
		begin : sv2v_autoblock_232
			reg signed [31:0] i;
			for (i = 0; i < N; i = i + 1)
				if (winner[i]) begin
					data_o = data_i[(0 >= (N - 1) ? i : (N - 1) - i) * DW+:DW];
					idx_o = i;
				end
		end
	end
	reg [31:0] k;
endmodule
module prim_esc_receiver (
	clk_i,
	rst_ni,
	esc_en_o,
	esc_rx_o,
	esc_tx_i
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	input clk_i;
	input rst_ni;
	output reg esc_en_o;
	output wire [1:0] esc_rx_o;
	input wire [1:0] esc_tx_i;
	wire esc_level;
	wire sigint_detected;
	prim_diff_decode #(.AsyncOn(1'b0)) i_decode_esc(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.diff_pi(esc_tx_i[1]),
		.diff_ni(esc_tx_i[0]),
		.level_o(esc_level),
		.rise_o(),
		.fall_o(),
		.event_o(),
		.sigint_o(sigint_detected)
	);
	reg [2:0] state_d;
	reg [2:0] state_q;
	reg resp_pd;
	reg resp_pq;
	reg resp_nd;
	reg resp_nq;
	assign esc_rx_o[1] = resp_pq;
	assign esc_rx_o[0] = resp_nq;
	localparam [2:0] Check = 1;
	localparam [2:0] EscResp = 3;
	localparam [2:0] Idle = 0;
	localparam [2:0] PingResp = 2;
	localparam [2:0] SigInt = 4;
	always @(*) begin : p_fsm
		state_d = state_q;
		resp_pd = 1'b0;
		resp_nd = 1'b1;
		esc_en_o = 1'b0;
		case (state_q)
			Idle:
				if (esc_level) begin
					state_d = Check;
					resp_pd = 1'b1;
					resp_nd = 1'b0;
				end
			Check: begin
				state_d = PingResp;
				if (esc_level) begin
					state_d = EscResp;
					esc_en_o = 1'b1;
				end
			end
			PingResp: begin
				state_d = Idle;
				resp_pd = 1'b1;
				resp_nd = 1'b0;
				if (esc_level) begin
					state_d = EscResp;
					esc_en_o = 1'b1;
				end
			end
			EscResp: begin
				state_d = Idle;
				if (esc_level) begin
					state_d = EscResp;
					resp_pd = ~resp_pq;
					resp_nd = resp_pq;
					esc_en_o = 1'b1;
				end
			end
			SigInt: begin
				state_d = Idle;
				if (sigint_detected) begin
					state_d = SigInt;
					resp_pd = ~resp_pq;
					resp_nd = ~resp_pq;
				end
			end
			default: state_d = Idle;
		endcase
		if (sigint_detected && (state_q != SigInt)) begin
			state_d = SigInt;
			resp_pd = 1'b0;
			resp_nd = 1'b0;
		end
	end
	always @(posedge clk_i or negedge rst_ni) begin : p_regs
		if (!rst_ni) begin
			state_q <= Idle;
			resp_pq <= 1'b0;
			resp_nq <= 1'b1;
		end
		else begin
			state_q <= state_d;
			resp_pq <= resp_pd;
			resp_nq <= resp_nd;
		end
	end
endmodule
module prim_esc_sender (
	clk_i,
	rst_ni,
	ping_en_i,
	ping_ok_o,
	integ_fail_o,
	esc_en_i,
	esc_rx_i,
	esc_tx_o
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	input clk_i;
	input rst_ni;
	input ping_en_i;
	output reg ping_ok_o;
	output reg integ_fail_o;
	input esc_en_i;
	input wire [1:0] esc_rx_i;
	output wire [1:0] esc_tx_o;
	wire resp;
	wire sigint_detected;
	prim_diff_decode #(.AsyncOn(1'b0)) i_decode_resp(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.diff_pi(esc_rx_i[1]),
		.diff_ni(esc_rx_i[0]),
		.level_o(resp),
		.rise_o(),
		.fall_o(),
		.event_o(),
		.sigint_o(sigint_detected)
	);
	wire ping_en_d;
	reg ping_en_q;
	wire esc_en_d;
	reg esc_en_q;
	reg esc_en_q1;
	assign ping_en_d = ping_en_i;
	assign esc_en_d = esc_en_i;
	assign esc_tx_o[1] = (esc_en_i | esc_en_q) | (ping_en_d & ~ping_en_q);
	assign esc_tx_o[0] = ~esc_tx_o[1];
	reg [2:0] state_d;
	reg [2:0] state_q;
	localparam [2:0] CheckEscRespHi = 2;
	localparam [2:0] CheckEscRespLo = 1;
	localparam [2:0] CheckPingResp0 = 3;
	localparam [2:0] CheckPingResp1 = 4;
	localparam [2:0] CheckPingResp2 = 5;
	localparam [2:0] CheckPingResp3 = 6;
	localparam [2:0] Idle = 0;
	always @(*) begin : p_fsm
		state_d = state_q;
		ping_ok_o = 1'b0;
		integ_fail_o = sigint_detected;
		case (state_q)
			Idle: begin
				if (esc_en_i)
					state_d = CheckEscRespHi;
				else if (ping_en_i)
					state_d = CheckPingResp0;
				if (resp)
					integ_fail_o = 1'b1;
			end
			CheckEscRespLo: begin
				state_d = CheckEscRespHi;
				if (!esc_en_i || resp) begin
					state_d = Idle;
					integ_fail_o = sigint_detected | resp;
				end
			end
			CheckEscRespHi: begin
				state_d = CheckEscRespLo;
				if (!esc_en_i || !resp) begin
					state_d = Idle;
					integ_fail_o = sigint_detected | ~resp;
				end
			end
			CheckPingResp0: begin
				state_d = CheckPingResp1;
				if (esc_en_i)
					state_d = CheckEscRespLo;
				else if (!resp) begin
					state_d = Idle;
					integ_fail_o = 1'b1;
				end
			end
			CheckPingResp1: begin
				state_d = CheckPingResp2;
				if (esc_en_i)
					state_d = CheckEscRespHi;
				else if (resp) begin
					state_d = Idle;
					integ_fail_o = 1'b1;
				end
			end
			CheckPingResp2: begin
				state_d = CheckPingResp3;
				if (esc_en_i)
					state_d = CheckEscRespLo;
				else if (!resp) begin
					state_d = Idle;
					integ_fail_o = 1'b1;
				end
			end
			CheckPingResp3: begin
				state_d = Idle;
				if (esc_en_i)
					state_d = CheckEscRespHi;
				else if (resp)
					integ_fail_o = 1'b1;
				else
					ping_ok_o = ping_en_i;
			end
			default: state_d = Idle;
		endcase
		if (((esc_en_i || esc_en_q) || esc_en_q1) && ping_en_i)
			ping_ok_o = 1'b1;
		if (sigint_detected) begin
			ping_ok_o = 1'b0;
			state_d = Idle;
		end
	end
	always @(posedge clk_i or negedge rst_ni) begin : p_regs
		if (!rst_ni) begin
			state_q <= Idle;
			esc_en_q <= 1'b0;
			esc_en_q1 <= 1'b0;
			ping_en_q <= 1'b0;
		end
		else begin
			state_q <= state_d;
			esc_en_q <= esc_en_d;
			esc_en_q1 <= esc_en_q;
			ping_en_q <= ping_en_d;
		end
	end
endmodule
module prim_sram_arbiter (
	clk_i,
	rst_ni,
	req,
	req_addr,
	req_write,
	req_wdata,
	gnt,
	rsp_rvalid,
	rsp_rdata,
	rsp_error,
	sram_req,
	sram_addr,
	sram_write,
	sram_wdata,
	sram_rvalid,
	sram_rdata,
	sram_rerror
);
	parameter signed [31:0] N = 4;
	parameter signed [31:0] SramDw = 32;
	parameter signed [31:0] SramAw = 12;
	input clk_i;
	input rst_ni;
	input [N - 1:0] req;
	input [(0 >= (N - 1) ? ((2 - N) * SramAw) + (((N - 1) * SramAw) - 1) : (N * SramAw) - 1):(0 >= (N - 1) ? (N - 1) * SramAw : 0)] req_addr;
	input [0:N - 1] req_write;
	input [(0 >= (N - 1) ? ((2 - N) * SramDw) + (((N - 1) * SramDw) - 1) : (N * SramDw) - 1):(0 >= (N - 1) ? (N - 1) * SramDw : 0)] req_wdata;
	output wire [N - 1:0] gnt;
	output wire [N - 1:0] rsp_rvalid;
	output wire [(0 >= (N - 1) ? ((2 - N) * SramDw) + (((N - 1) * SramDw) - 1) : (N * SramDw) - 1):(0 >= (N - 1) ? (N - 1) * SramDw : 0)] rsp_rdata;
	output wire [(0 >= (N - 1) ? ((2 - N) * 2) + (((N - 1) * 2) - 1) : (N * 2) - 1):(0 >= (N - 1) ? (N - 1) * 2 : 0)] rsp_error;
	output wire sram_req;
	output wire [SramAw - 1:0] sram_addr;
	output wire sram_write;
	output wire [SramDw - 1:0] sram_wdata;
	input sram_rvalid;
	input [SramDw - 1:0] sram_rdata;
	input [1:0] sram_rerror;
	localparam signed [31:0] ARB_DW = (1 + SramAw) + SramDw;
	wire [(0 >= (N - 1) ? ((2 - N) * ((1 + SramAw) + SramDw)) + (((N - 1) * ((1 + SramAw) + SramDw)) - 1) : (N * ((1 + SramAw) + SramDw)) - 1):(0 >= (N - 1) ? (N - 1) * ((1 + SramAw) + SramDw) : 0)] req_packed;
	generate
		genvar i;
		for (i = 0; i < N; i = i + 1) begin : gen_reqs
			assign req_packed[(0 >= (N - 1) ? i : (N - 1) - i) * ((1 + SramAw) + SramDw)+:(1 + SramAw) + SramDw] = {req_write[i], req_addr[(0 >= (N - 1) ? i : (N - 1) - i) * SramAw+:SramAw], req_wdata[(0 >= (N - 1) ? i : (N - 1) - i) * SramDw+:SramDw]};
		end
	endgenerate
	wire [((1 + SramAw) + SramDw) - 1:0] sram_packed;
	assign sram_write = sram_packed[1 + (SramAw + (SramDw - 1))];
	assign sram_addr = sram_packed[SramAw + (SramDw - 1)-:((SramAw + (SramDw - 1)) >= SramDw ? ((SramAw + (SramDw - 1)) - SramDw) + 1 : (SramDw - (SramAw + (SramDw - 1))) + 1)];
	assign sram_wdata = sram_packed[SramDw - 1-:SramDw];
	prim_arbiter #(
		.N(N),
		.DW(ARB_DW)
	) u_reqarb(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.req_i(req),
		.data_i(req_packed),
		.gnt_o(gnt),
		.idx_o(),
		.valid_o(sram_req),
		.data_o(sram_packed),
		.ready_i(1'b1)
	);
	wire [N - 1:0] steer;
	wire sram_ack;
	assign sram_ack = sram_rvalid & |steer;
	prim_fifo_sync #(
		.Width(N),
		.Pass(1'b0),
		.Depth(4)
	) u_req_fifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(sram_req && !sram_write),
		.wready(),
		.wdata(gnt),
		.depth(),
		.rvalid(),
		.rready(sram_ack),
		.rdata(steer)
	);
	assign rsp_rvalid = steer & {N {sram_rvalid}};
	generate
		for (i = 0; i < N; i = i + 1) begin : gen_rsp
			assign rsp_rdata[(0 >= (N - 1) ? i : (N - 1) - i) * SramDw+:SramDw] = sram_rdata;
			assign rsp_error[(0 >= (N - 1) ? i : (N - 1) - i) * 2+:2] = sram_rerror;
		end
	endgenerate
endmodule
module prim_fifo_async (
	clk_wr_i,
	rst_wr_ni,
	wvalid,
	wready,
	wdata,
	wdepth,
	clk_rd_i,
	rst_rd_ni,
	rvalid,
	rready,
	rdata,
	rdepth
);
	parameter [31:0] Width = 16;
	parameter [31:0] Depth = 3;
	localparam [31:0] DepthW = $clog2(Depth + 1);
	input clk_wr_i;
	input rst_wr_ni;
	input wvalid;
	output wready;
	input [Width - 1:0] wdata;
	output [DepthW - 1:0] wdepth;
	input clk_rd_i;
	input rst_rd_ni;
	output rvalid;
	input rready;
	output [Width - 1:0] rdata;
	output [DepthW - 1:0] rdepth;
	localparam [31:0] PTRV_W = $clog2(Depth);
	function automatic [PTRV_W - 1:0] sv2v_cast_2173F_unsigned;
		input reg [PTRV_W - 1:0] inp;
		sv2v_cast_2173F_unsigned = inp;
	endfunction
	localparam [PTRV_W - 1:0] DepthMinus1 = sv2v_cast_2173F_unsigned(Depth - 1);
	localparam [31:0] PTR_WIDTH = PTRV_W + 1;
	reg [PTR_WIDTH - 1:0] fifo_wptr;
	reg [PTR_WIDTH - 1:0] fifo_rptr;
	wire [PTR_WIDTH - 1:0] fifo_wptr_sync_combi;
	reg [PTR_WIDTH - 1:0] fifo_rptr_sync;
	wire [PTR_WIDTH - 1:0] fifo_wptr_gray_sync;
	wire [PTR_WIDTH - 1:0] fifo_rptr_gray_sync;
	reg [PTR_WIDTH - 1:0] fifo_wptr_gray;
	reg [PTR_WIDTH - 1:0] fifo_rptr_gray;
	wire fifo_incr_wptr;
	wire fifo_incr_rptr;
	wire empty;
	wire full_wclk;
	wire full_rclk;
	assign wready = !full_wclk;
	assign rvalid = !empty;
	assign fifo_incr_wptr = wvalid & wready;
	assign fifo_incr_rptr = rvalid & rready;
	always @(posedge clk_wr_i or negedge rst_wr_ni)
		if (!rst_wr_ni)
			fifo_wptr <= {PTR_WIDTH {1'b0}};
		else if (fifo_incr_wptr)
			if (fifo_wptr[PTR_WIDTH - 2:0] == DepthMinus1)
				fifo_wptr <= {~fifo_wptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}};
			else
				fifo_wptr <= fifo_wptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1};
	function automatic [PTR_WIDTH - 1:0] dec2gray;
		input reg [PTR_WIDTH - 1:0] decval;
		reg [PTR_WIDTH - 1:0] decval_sub;
		reg [PTR_WIDTH - 2:0] decval_in;
		reg unused_decval_msb;
		begin
			decval_sub = (Depth - {1'b0, decval[PTR_WIDTH - 2:0]}) - 1'b1;
			{unused_decval_msb, decval_in} = (decval[PTR_WIDTH - 1] ? decval_sub : decval);
			dec2gray = {decval[PTR_WIDTH - 1], {1'b0, decval_in[PTR_WIDTH - 2:1]} ^ decval_in[PTR_WIDTH - 2:0]};
		end
	endfunction
	always @(posedge clk_wr_i or negedge rst_wr_ni)
		if (!rst_wr_ni)
			fifo_wptr_gray <= {PTR_WIDTH {1'b0}};
		else if (fifo_incr_wptr)
			if (fifo_wptr[PTR_WIDTH - 2:0] == DepthMinus1)
				fifo_wptr_gray <= dec2gray({~fifo_wptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}});
			else
				fifo_wptr_gray <= dec2gray(fifo_wptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1});
	prim_flop_2sync #(.Width(PTR_WIDTH)) sync_wptr(
		.clk_i(clk_rd_i),
		.rst_ni(rst_rd_ni),
		.d(fifo_wptr_gray),
		.q(fifo_wptr_gray_sync)
	);
	function automatic [PTR_WIDTH - 1:0] gray2dec;
		input reg [PTR_WIDTH - 1:0] grayval;
		reg [PTR_WIDTH - 2:0] dec_tmp;
		reg [PTR_WIDTH - 2:0] dec_tmp_sub;
		reg unused_decsub_msb;
		begin
			dec_tmp[PTR_WIDTH - 2] = grayval[PTR_WIDTH - 2];
			begin : sv2v_autoblock_249
				reg signed [31:0] i;
				for (i = PTR_WIDTH - 3; i >= 0; i = i - 1)
					dec_tmp[i] = dec_tmp[i + 1] ^ grayval[i];
			end
			{unused_decsub_msb, dec_tmp_sub} = (Depth - {1'b0, dec_tmp}) - 1'b1;
			if (grayval[PTR_WIDTH - 1])
				gray2dec = {1'b1, dec_tmp_sub};
			else
				gray2dec = {1'b0, dec_tmp};
		end
	endfunction
	assign fifo_wptr_sync_combi = gray2dec(fifo_wptr_gray_sync);
	always @(posedge clk_rd_i or negedge rst_rd_ni)
		if (!rst_rd_ni)
			fifo_rptr <= {PTR_WIDTH {1'b0}};
		else if (fifo_incr_rptr)
			if (fifo_rptr[PTR_WIDTH - 2:0] == DepthMinus1)
				fifo_rptr <= {~fifo_rptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}};
			else
				fifo_rptr <= fifo_rptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1};
	always @(posedge clk_rd_i or negedge rst_rd_ni)
		if (!rst_rd_ni)
			fifo_rptr_gray <= {PTR_WIDTH {1'b0}};
		else if (fifo_incr_rptr)
			if (fifo_rptr[PTR_WIDTH - 2:0] == DepthMinus1)
				fifo_rptr_gray <= dec2gray({~fifo_rptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}});
			else
				fifo_rptr_gray <= dec2gray(fifo_rptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1});
	prim_flop_2sync #(.Width(PTR_WIDTH)) sync_rptr(
		.clk_i(clk_wr_i),
		.rst_ni(rst_wr_ni),
		.d(fifo_rptr_gray),
		.q(fifo_rptr_gray_sync)
	);
	always @(posedge clk_wr_i or negedge rst_wr_ni)
		if (!rst_wr_ni)
			fifo_rptr_sync <= {PTR_WIDTH {1'b0}};
		else
			fifo_rptr_sync <= gray2dec(fifo_rptr_gray_sync);
	assign full_wclk = fifo_wptr == (fifo_rptr_sync ^ {1'b1, {PTR_WIDTH - 1 {1'b0}}});
	assign full_rclk = fifo_wptr_sync_combi == (fifo_rptr ^ {1'b1, {PTR_WIDTH - 1 {1'b0}}});
	wire wptr_msb;
	wire rptr_sync_msb;
	wire [PTRV_W - 1:0] wptr_value;
	wire [PTRV_W - 1:0] rptr_sync_value;
	assign wptr_msb = fifo_wptr[PTR_WIDTH - 1];
	assign rptr_sync_msb = fifo_rptr_sync[PTR_WIDTH - 1];
	assign wptr_value = fifo_wptr[0+:PTRV_W];
	assign rptr_sync_value = fifo_rptr_sync[0+:PTRV_W];
	function automatic [DepthW - 1:0] sv2v_cast_37EEB_unsigned;
		input reg [DepthW - 1:0] inp;
		sv2v_cast_37EEB_unsigned = inp;
	endfunction
	function automatic [DepthW - 1:0] sv2v_cast_37EEB;
		input reg [DepthW - 1:0] inp;
		sv2v_cast_37EEB = inp;
	endfunction
	assign wdepth = (full_wclk ? sv2v_cast_37EEB_unsigned(Depth) : (wptr_msb == rptr_sync_msb ? sv2v_cast_37EEB(wptr_value) - sv2v_cast_37EEB(rptr_sync_value) : (sv2v_cast_37EEB_unsigned(Depth) - sv2v_cast_37EEB(rptr_sync_value)) + sv2v_cast_37EEB(wptr_value)));
	assign empty = fifo_wptr_sync_combi == fifo_rptr;
	wire rptr_msb;
	wire wptr_sync_msb;
	wire [PTRV_W - 1:0] rptr_value;
	wire [PTRV_W - 1:0] wptr_sync_value;
	assign wptr_sync_msb = fifo_wptr_sync_combi[PTR_WIDTH - 1];
	assign rptr_msb = fifo_rptr[PTR_WIDTH - 1];
	assign wptr_sync_value = fifo_wptr_sync_combi[0+:PTRV_W];
	assign rptr_value = fifo_rptr[0+:PTRV_W];
	assign rdepth = (full_rclk ? sv2v_cast_37EEB_unsigned(Depth) : (wptr_sync_msb == rptr_msb ? sv2v_cast_37EEB(wptr_sync_value) - sv2v_cast_37EEB(rptr_value) : (sv2v_cast_37EEB_unsigned(Depth) - sv2v_cast_37EEB(rptr_value)) + sv2v_cast_37EEB(wptr_sync_value)));
	reg [Width - 1:0] storage [0:Depth - 1];
	always @(posedge clk_wr_i)
		if (fifo_incr_wptr)
			storage[fifo_wptr[PTR_WIDTH - 2:0]] <= wdata;
	assign rdata = storage[fifo_rptr[PTR_WIDTH - 2:0]];
endmodule
module prim_fifo_sync (
	clk_i,
	rst_ni,
	clr_i,
	wvalid,
	wready,
	wdata,
	rvalid,
	rready,
	rdata,
	depth
);
	parameter [31:0] Width = 16;
	parameter [0:0] Pass = 1'b1;
	parameter [31:0] Depth = 4;
	localparam [31:0] DepthWNorm = $clog2(Depth + 1);
	localparam [31:0] DepthW = (DepthWNorm == 0 ? 1 : DepthWNorm);
	input clk_i;
	input rst_ni;
	input clr_i;
	input wvalid;
	output wready;
	input [Width - 1:0] wdata;
	output rvalid;
	input rready;
	output [Width - 1:0] rdata;
	output [DepthW - 1:0] depth;
	generate
		if (Depth == 0) begin : gen_passthru_fifo
			assign depth = 1'b0;
			assign rvalid = wvalid;
			assign rdata = wdata;
			assign wready = rready;
			wire unused_clr;
			assign unused_clr = clr_i;
		end
		else begin : gen_normal_fifo
			localparam [31:0] PTRV_W = $clog2(Depth) + ~|$clog2(Depth);
			localparam [31:0] PTR_WIDTH = PTRV_W + 1;
			reg [PTR_WIDTH - 1:0] fifo_wptr;
			reg [PTR_WIDTH - 1:0] fifo_rptr;
			wire fifo_incr_wptr;
			wire fifo_incr_rptr;
			wire fifo_empty;
			wire full;
			wire empty;
			wire wptr_msb;
			wire rptr_msb;
			wire [PTRV_W - 1:0] wptr_value;
			wire [PTRV_W - 1:0] rptr_value;
			assign wptr_msb = fifo_wptr[PTR_WIDTH - 1];
			assign rptr_msb = fifo_rptr[PTR_WIDTH - 1];
			assign wptr_value = fifo_wptr[0+:PTRV_W];
			assign rptr_value = fifo_rptr[0+:PTRV_W];
			function automatic [DepthW - 1:0] sv2v_cast_37EEB_unsigned;
				input reg [DepthW - 1:0] inp;
				sv2v_cast_37EEB_unsigned = inp;
			endfunction
			function automatic [DepthW - 1:0] sv2v_cast_37EEB;
				input reg [DepthW - 1:0] inp;
				sv2v_cast_37EEB = inp;
			endfunction
			assign depth = (full ? sv2v_cast_37EEB_unsigned(Depth) : (wptr_msb == rptr_msb ? sv2v_cast_37EEB(wptr_value) - sv2v_cast_37EEB(rptr_value) : (sv2v_cast_37EEB_unsigned(Depth) - sv2v_cast_37EEB(rptr_value)) + sv2v_cast_37EEB(wptr_value)));
			assign fifo_incr_wptr = wvalid & wready;
			assign fifo_incr_rptr = rvalid & rready;
			assign wready = ~full;
			assign rvalid = ~empty;
			always @(posedge clk_i or negedge rst_ni)
				if (!rst_ni)
					fifo_wptr <= {PTR_WIDTH {1'b0}};
				else if (clr_i)
					fifo_wptr <= {PTR_WIDTH {1'b0}};
				else if (fifo_incr_wptr)
					if (fifo_wptr[PTR_WIDTH - 2:0] == (Depth - 1))
						fifo_wptr <= {~fifo_wptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}};
					else
						fifo_wptr <= fifo_wptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1};
			always @(posedge clk_i or negedge rst_ni)
				if (!rst_ni)
					fifo_rptr <= {PTR_WIDTH {1'b0}};
				else if (clr_i)
					fifo_rptr <= {PTR_WIDTH {1'b0}};
				else if (fifo_incr_rptr)
					if (fifo_rptr[PTR_WIDTH - 2:0] == (Depth - 1))
						fifo_rptr <= {~fifo_rptr[PTR_WIDTH - 1], {PTR_WIDTH - 1 {1'b0}}};
					else
						fifo_rptr <= fifo_rptr + {{PTR_WIDTH - 1 {1'b0}}, 1'b1};
			assign full = fifo_wptr == (fifo_rptr ^ {1'b1, {PTR_WIDTH - 1 {1'b0}}});
			assign fifo_empty = fifo_wptr == fifo_rptr;
			reg [Width - 1:0] storage [0:Depth - 1];
			wire [Width - 1:0] storage_rdata;
			if (Depth == 1) begin : gen_depth_eq1
				assign storage_rdata = storage[0];
				always @(posedge clk_i)
					if (fifo_incr_wptr)
						storage[0] <= wdata;
			end
			else begin : gen_depth_gt1
				assign storage_rdata = storage[fifo_rptr[PTR_WIDTH - 2:0]];
				always @(posedge clk_i)
					if (fifo_incr_wptr)
						storage[fifo_wptr[PTR_WIDTH - 2:0]] <= wdata;
			end
			if (Pass == 1'b1) begin : gen_pass
				assign rdata = (fifo_empty && wvalid ? wdata : storage_rdata);
				assign empty = fifo_empty & ~wvalid;
			end
			else begin : gen_nopass
				assign rdata = storage_rdata;
				assign empty = fifo_empty;
			end
		end
	endgenerate
endmodule
module prim_flop_2sync (
	clk_i,
	rst_ni,
	d,
	q
);
	parameter signed [31:0] Width = 16;
	parameter [0:0] ResetValue = 0;
	input clk_i;
	input rst_ni;
	input [Width - 1:0] d;
	output reg [Width - 1:0] q;
	reg [Width - 1:0] intq;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			intq <= {Width {ResetValue}};
			q <= {Width {ResetValue}};
		end
		else begin
			intq <= d;
			q <= intq;
		end
endmodule
module prim_lfsr (
	clk_i,
	rst_ni,
	seed_en_i,
	seed_i,
	lfsr_en_i,
	entropy_i,
	state_o
);
	parameter _sv2v_width_LfsrType = 56;
	parameter [_sv2v_width_LfsrType - 1:0] LfsrType = "GAL_XOR";
	parameter [31:0] LfsrDw = 32;
	parameter [31:0] EntropyDw = 8;
	parameter [31:0] StateOutDw = 8;
	function automatic [LfsrDw - 1:0] sv2v_cast_28712;
		input reg [LfsrDw - 1:0] inp;
		sv2v_cast_28712 = inp;
	endfunction
	parameter [LfsrDw - 1:0] DefaultSeed = sv2v_cast_28712(1);
	parameter [LfsrDw - 1:0] CustomCoeffs = 1'sb0;
	parameter [0:0] MaxLenSVA = 1'b1;
	parameter [0:0] LockupSVA = 1'b1;
	parameter [0:0] ExtSeedSVA = 1'b1;
	input clk_i;
	input rst_ni;
	input seed_en_i;
	input [LfsrDw - 1:0] seed_i;
	input lfsr_en_i;
	input [EntropyDw - 1:0] entropy_i;
	output wire [StateOutDw - 1:0] state_o;
	localparam [31:0] GAL_XOR_LUT_OFF = 4;
	localparam [3903:0] GAL_XOR_COEFFS = {64'h0000000000000009, 64'h0000000000000012, 64'h0000000000000021, 64'h0000000000000041, 64'h000000000000008e, 64'h0000000000000108, 64'h0000000000000204, 64'h0000000000000402, 64'h0000000000000829, 64'h000000000000100d, 64'h0000000000002015, 64'h0000000000004001, 64'h0000000000008016, 64'h0000000000010004, 64'h0000000000020013, 64'h0000000000040013, 64'h0000000000080004, 64'h0000000000100002, 64'h0000000000200001, 64'h0000000000400010, 64'h000000000080000d, 64'h0000000001000004, 64'h0000000002000023, 64'h0000000004000013, 64'h0000000008000004, 64'h0000000010000002, 64'h0000000020000029, 64'h0000000040000004, 64'h0000000080000057, 64'h0000000100000029, 64'h0000000200000073, 64'h0000000400000002, 64'h000000080000003b, 64'h000000100000001f, 64'h0000002000000031, 64'h0000004000000008, 64'h000000800000001c, 64'h0000010000000004, 64'h000002000000001f, 64'h000004000000002c, 64'h0000080000000032, 64'h000010000000000d, 64'h0000200000000097, 64'h0000400000000010, 64'h000080000000005b, 64'h0001000000000038, 64'h000200000000000e, 64'h0004000000000025, 64'h0008000000000004, 64'h0010000000000023, 64'h002000000000003e, 64'h0040000000000023, 64'h008000000000004a, 64'h0100000000000016, 64'h0200000000000031, 64'h040000000000003d, 64'h0800000000000001, 64'h1000000000000013, 64'h2000000000000034, 64'h4000000000000001, 64'h800000000000000d};
	localparam [31:0] FIB_XNOR_LUT_OFF = 3;
	localparam [27887:0] FIB_XNOR_COEFFS = {168'h000000000000000000000000000000000000000006, 168'h00000000000000000000000000000000000000000c, 168'h000000000000000000000000000000000000000014, 168'h000000000000000000000000000000000000000030, 168'h000000000000000000000000000000000000000060, 168'h0000000000000000000000000000000000000000b8, 168'h000000000000000000000000000000000000000110, 168'h000000000000000000000000000000000000000240, 168'h000000000000000000000000000000000000000500, 168'h000000000000000000000000000000000000000829, 168'h00000000000000000000000000000000000000100d, 168'h000000000000000000000000000000000000002015, 168'h000000000000000000000000000000000000006000, 168'h00000000000000000000000000000000000000d008, 168'h000000000000000000000000000000000000012000, 168'h000000000000000000000000000000000000020400, 168'h000000000000000000000000000000000000040023, 168'h000000000000000000000000000000000000090000, 168'h000000000000000000000000000000000000140000, 168'h000000000000000000000000000000000000300000, 168'h000000000000000000000000000000000000420000, 168'h000000000000000000000000000000000000e10000, 168'h000000000000000000000000000000000001200000, 168'h000000000000000000000000000000000002000023, 168'h000000000000000000000000000000000004000013, 168'h000000000000000000000000000000000009000000, 168'h000000000000000000000000000000000014000000, 168'h000000000000000000000000000000000020000029, 168'h000000000000000000000000000000000048000000, 168'h000000000000000000000000000000000080200003, 168'h000000000000000000000000000000000100080000, 168'h000000000000000000000000000000000204000003, 168'h000000000000000000000000000000000500000000, 168'h000000000000000000000000000000000801000000, 168'h00000000000000000000000000000000100000001f, 168'h000000000000000000000000000000002000000031, 168'h000000000000000000000000000000004400000000, 168'h00000000000000000000000000000000a000140000, 168'h000000000000000000000000000000012000000000, 168'h0000000000000000000000000000000300000c0000, 168'h000000000000000000000000000000063000000000, 168'h0000000000000000000000000000000c0000030000, 168'h0000000000000000000000000000001b0000000000, 168'h000000000000000000000000000000300003000000, 168'h000000000000000000000000000000420000000000, 168'h000000000000000000000000000000c00000180000, 168'h000000000000000000000000000001008000000000, 168'h000000000000000000000000000003000000c00000, 168'h000000000000000000000000000006000c00000000, 168'h000000000000000000000000000009000000000000, 168'h000000000000000000000000000018003000000000, 168'h000000000000000000000000000030000000030000, 168'h000000000000000000000000000040000040000000, 168'h0000000000000000000000000000c0000600000000, 168'h000000000000000000000000000102000000000000, 168'h000000000000000000000000000200004000000000, 168'h000000000000000000000000000600003000000000, 168'h000000000000000000000000000c00000000000000, 168'h000000000000000000000000001800300000000000, 168'h000000000000000000000000003000000000000030, 168'h000000000000000000000000006000000000000000, 168'h00000000000000000000000000d800000000000000, 168'h000000000000000000000000010000400000000000, 168'h000000000000000000000000030180000000000000, 168'h000000000000000000000000060300000000000000, 168'h000000000000000000000000080400000000000000, 168'h000000000000000000000000140000028000000000, 168'h000000000000000000000000300060000000000000, 168'h000000000000000000000000410000000000000000, 168'h000000000000000000000000820000000001040000, 168'h000000000000000000000001000000800000000000, 168'h000000000000000000000003000600000000000000, 168'h000000000000000000000006018000000000000000, 168'h00000000000000000000000c000000018000000000, 168'h000000000000000000000018000000600000000000, 168'h000000000000000000000030000600000000000000, 168'h000000000000000000000040200000000000000000, 168'h0000000000000000000000c0000000060000000000, 168'h000000000000000000000110000000000000000000, 168'h000000000000000000000240000000480000000000, 168'h000000000000000000000600000000003000000000, 168'h000000000000000000000800400000000000000000, 168'h000000000000000000001800000300000000000000, 168'h000000000000000000003003000000000000000000, 168'h000000000000000000004002000000000000000000, 168'h00000000000000000000c000000000000000018000, 168'h000000000000000000010000000004000000000000, 168'h000000000000000000030000c00000000000000000, 168'h0000000000000000000600000000000000000000c0, 168'h0000000000000000000c00c0000000000000000000, 168'h000000000000000000140000000000000000000000, 168'h000000000000000000200001000000000000000000, 168'h000000000000000000400800000000000000000000, 168'h000000000000000000a00000000001400000000000, 168'h000000000000000001040000000000000000000000, 168'h000000000000000002004000000000000000000000, 168'h000000000000000005000000000028000000000000, 168'h000000000000000008000000004000000000000000, 168'h000000000000000018600000000000000000000000, 168'h000000000000000030000000000000000c00000000, 168'h000000000000000040200000000000000000000000, 168'h0000000000000000c0300000000000000000000000, 168'h000000000000000100010000000000000000000000, 168'h000000000000000200040000000000000000000000, 168'h0000000000000005000000000000000a0000000000, 168'h000000000000000800000010000000000000000000, 168'h000000000000001860000000000000000000000000, 168'h000000000000003003000000000000000000000000, 168'h000000000000004010000000000000000000000000, 168'h00000000000000a000000000140000000000000000, 168'h000000000000010080000000000000000000000000, 168'h000000000000030000000000000000000180000000, 168'h000000000000060018000000000000000000000000, 168'h0000000000000c0000000000000000300000000000, 168'h000000000000140005000000000000000000000000, 168'h000000000000200000001000000000000000000000, 168'h000000000000404000000000000000000000000000, 168'h000000000000810000000000000000000000000102, 168'h000000000001000040000000000000000000000000, 168'h000000000003000000000000006000000000000000, 168'h000000000005000000000000000000000000000000, 168'h000000000008000000004000000000000000000000, 168'h000000000018000000000000000000000000030000, 168'h000000000030000000030000000000000000000000, 168'h000000000060000000000000000000000000000000, 168'h0000000000a0000014000000000000000000000000, 168'h000000000108000000000000000000000000000000, 168'h000000000240000000000000000000000000000000, 168'h000000000600000000000c00000000000000000000, 168'h000000000800000040000000000000000000000000, 168'h000000001800000000000300000000000000000000, 168'h000000002000000000000010000000000000000000, 168'h000000004008000000000000000000000000000000, 168'h00000000c000000000000000000000000000000600, 168'h000000010000080000000000000000000000000000, 168'h000000030600000000000000000000000000000000, 168'h00000004a400000000000000000000000000000000, 168'h000000080000004000000000000000000000000000, 168'h000000180000003000000000000000000000000000, 168'h000000200001000000000000000000000000000000, 168'h000000600006000000000000000000000000000000, 168'h000000c00000000000000006000000000000000000, 168'h000001000000000000100000000000000000000000, 168'h000003000000000000006000000000000000000000, 168'h000006000000003000000000000000000000000000, 168'h000008000001000000000000000000000000000000, 168'h00001800000000000000000000000000c000000000, 168'h000020000000000001000000000000000000000000, 168'h000048000000000000000000000000000000000000, 168'h0000c0000000000000006000000000000000000000, 168'h000180000000000000000000000000000000000000, 168'h000280000000000000000000000000000005000000, 168'h00060000000c000000000000000000000000000000, 168'h000c00000000000000000000000000018000000000, 168'h001800000600000000000000000000000000000000, 168'h003000000c00000000000000000000000000000000, 168'h004000000080000000000000000000000000000000, 168'h00c000300000000000000000000000000000000000, 168'h010000400000000000000000000000000000000000, 168'h030000000000000000000006000000000000000000, 168'h0600000000000000c0000000000000000000000000, 168'h0c0060000000000000000000000000000000000000, 168'h180000006000000000000000000000000000000000, 168'h3000000000c0000000000000000000000000000000, 168'h410000000000000000000000000000000000000000, 168'ha00140000000000000000000000000000000000000};
	wire lockup;
	wire [LfsrDw - 1:0] lfsr_d;
	reg [LfsrDw - 1:0] lfsr_q;
	wire [LfsrDw - 1:0] next_lfsr_state;
	wire [LfsrDw - 1:0] coeffs;
	generate
		function automatic [63:0] sv2v_cast_64;
			input reg [63:0] inp;
			sv2v_cast_64 = inp;
		endfunction
		if (sv2v_cast_64(LfsrType) == sv2v_cast_64("GAL_XOR")) begin : gen_gal_xor
			if (CustomCoeffs > 0) begin : gen_custom
				assign coeffs = CustomCoeffs[LfsrDw - 1:0];
			end
			else begin : gen_lut
				assign coeffs = GAL_XOR_COEFFS[((60 - (LfsrDw - GAL_XOR_LUT_OFF)) * 64) + (LfsrDw - 1)-:LfsrDw];
			end
			assign next_lfsr_state = (sv2v_cast_28712(entropy_i) ^ ({LfsrDw {lfsr_q[0]}} & coeffs)) ^ (lfsr_q >> 1);
			assign lockup = ~(|lfsr_q);
		end
		else if (sv2v_cast_64(LfsrType) == "FIB_XNOR") begin : gen_fib_xnor
			if (CustomCoeffs > 0) begin : gen_custom
				assign coeffs = CustomCoeffs[LfsrDw - 1:0];
			end
			else begin : gen_lut
				assign coeffs = FIB_XNOR_COEFFS[((165 - (LfsrDw - FIB_XNOR_LUT_OFF)) * 168) + (LfsrDw - 1)-:LfsrDw];
			end
			assign next_lfsr_state = sv2v_cast_28712(entropy_i) ^ {lfsr_q[LfsrDw - 2:0], ~(^(lfsr_q & coeffs))};
			assign lockup = &lfsr_q;
		end
	endgenerate
	assign lfsr_d = (seed_en_i ? seed_i : (lfsr_en_i && lockup ? DefaultSeed : (lfsr_en_i ? next_lfsr_state : lfsr_q)));
	assign state_o = lfsr_q[StateOutDw - 1:0];
	always @(posedge clk_i or negedge rst_ni) begin : p_reg
		if (!rst_ni)
			lfsr_q <= DefaultSeed;
		else
			lfsr_q <= lfsr_d;
	end
	function automatic [LfsrDw - 1:0] compute_next_state;
		input reg [LfsrDw - 1:0] lfsrcoeffs;
		input reg [EntropyDw - 1:0] entropy;
		input reg [LfsrDw - 1:0] state;
		reg state0;
		begin
			if (sv2v_cast_64(LfsrType) == sv2v_cast_64("GAL_XOR")) begin
				if (state == 0)
					state = DefaultSeed;
				else begin
					state0 = state[0];
					state = state >> 1;
					if (state0)
						state = state ^ lfsrcoeffs;
					state = state ^ sv2v_cast_28712(entropy);
				end
			end
			else if (sv2v_cast_64(LfsrType) == "FIB_XNOR") begin
				if (&state)
					state = DefaultSeed;
				else begin
					state0 = ~(^(state & lfsrcoeffs));
					state = state << 1;
					state[0] = state0;
					state = state ^ sv2v_cast_28712(entropy);
				end
			end
			else
				$error("unknown lfsr type");
			compute_next_state = state;
		end
	endfunction
	generate
		if (MaxLenSVA) begin : gen_max_len_sva
			wire [LfsrDw - 1:0] cnt_d;
			reg [LfsrDw - 1:0] cnt_q;
			wire perturbed_d;
			reg perturbed_q;
			wire [LfsrDw - 1:0] cmp_val;
			assign cmp_val = {{LfsrDw - 1 {1'b1}}, 1'b0};
			assign cnt_d = (lfsr_en_i && lockup ? {LfsrDw {1'sb0}} : (lfsr_en_i && (cnt_q == cmp_val) ? {LfsrDw {1'sb0}} : (lfsr_en_i ? cnt_q + 1'b1 : cnt_q)));
			assign perturbed_d = (perturbed_q | |entropy_i) | seed_en_i;
			always @(posedge clk_i or negedge rst_ni) begin : p_max_len
				if (!rst_ni) begin
					cnt_q <= {LfsrDw {1'sb0}};
					perturbed_q <= 1'b0;
				end
				else begin
					cnt_q <= cnt_d;
					perturbed_q <= perturbed_d;
				end
			end
		end
	endgenerate
endmodule
module prim_packer (
	clk_i,
	rst_ni,
	valid_i,
	data_i,
	mask_i,
	ready_o,
	valid_o,
	data_o,
	mask_o,
	ready_i,
	flush_i,
	flush_done_o
);
	parameter signed [31:0] InW = 32;
	parameter signed [31:0] OutW = 32;
	input clk_i;
	input rst_ni;
	input valid_i;
	input [InW - 1:0] data_i;
	input [InW - 1:0] mask_i;
	output ready_o;
	output wire valid_o;
	output wire [OutW - 1:0] data_o;
	output wire [OutW - 1:0] mask_o;
	input ready_i;
	input flush_i;
	output wire flush_done_o;
	localparam signed [31:0] Width = InW + OutW;
	localparam signed [31:0] PtrW = $clog2(Width + 1);
	localparam signed [31:0] MaxW = (InW > OutW ? InW : OutW);
	wire valid_next;
	wire ready_next;
	reg [MaxW - 1:0] stored_data;
	reg [MaxW - 1:0] stored_mask;
	wire [Width - 1:0] concat_data;
	wire [Width - 1:0] concat_mask;
	wire [Width - 1:0] shiftl_data;
	wire [Width - 1:0] shiftl_mask;
	reg [PtrW - 1:0] pos;
	wire [PtrW - 1:0] pos_next;
	reg [$clog2(InW) - 1:0] lod_idx;
	reg [$clog2(InW + 1) - 1:0] inmask_ones;
	wire ack_in;
	wire ack_out;
	reg flush_ready;
	always @(*) begin
		inmask_ones = {$clog2(InW + 1) {1'sb0}};
		begin : sv2v_autoblock_266
			reg signed [31:0] i;
			for (i = 0; i < InW; i = i + 1)
				inmask_ones = inmask_ones + mask_i[i];
		end
	end
	function automatic [PtrW - 1:0] sv2v_cast_904A8;
		input reg [PtrW - 1:0] inp;
		sv2v_cast_904A8 = inp;
	endfunction
	assign pos_next = (valid_i ? pos + sv2v_cast_904A8(inmask_ones) : pos);
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			pos <= {PtrW {1'sb0}};
		else if (flush_ready)
			pos <= {PtrW {1'sb0}};
		else if (ack_out)
			pos <= pos_next - OutW;
		else if (ack_in)
			pos <= pos_next;
	always @(*) begin
		lod_idx = 0;
		begin : sv2v_autoblock_267
			reg signed [31:0] i;
			for (i = InW - 1; i >= 0; i = i - 1)
				if (mask_i[i] == 1'b1)
					lod_idx = i;
		end
	end
	assign ack_in = valid_i & ready_o;
	assign ack_out = valid_o & ready_i;
	function automatic [Width - 1:0] sv2v_cast_63616;
		input reg [Width - 1:0] inp;
		sv2v_cast_63616 = inp;
	endfunction
	assign shiftl_data = (valid_i ? sv2v_cast_63616(data_i >> lod_idx) << pos : {Width {1'sb0}});
	assign shiftl_mask = (valid_i ? sv2v_cast_63616(mask_i >> lod_idx) << pos : {Width {1'sb0}});
	assign concat_data = {{Width - MaxW {1'b0}}, stored_data & stored_mask} | (shiftl_data & shiftl_mask);
	assign concat_mask = {{Width - MaxW {1'b0}}, stored_mask} | shiftl_mask;
	wire [MaxW - 1:0] stored_data_next;
	wire [MaxW - 1:0] stored_mask_next;
	generate
		if (InW >= OutW) begin : gen_stored_in
			assign stored_data_next = concat_data[OutW+:InW];
			assign stored_mask_next = concat_mask[OutW+:InW];
		end
		else begin : gen_stored_out
			assign stored_data_next = {{OutW - InW {1'b0}}, concat_data[OutW+:InW]};
			assign stored_mask_next = {{OutW - InW {1'b0}}, concat_mask[OutW+:InW]};
		end
	endgenerate
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			stored_data <= {MaxW {1'sb0}};
			stored_mask <= {MaxW {1'sb0}};
		end
		else if (flush_ready) begin
			stored_data <= {MaxW {1'sb0}};
			stored_mask <= {MaxW {1'sb0}};
		end
		else if (ack_out) begin
			stored_data <= stored_data_next;
			stored_mask <= stored_mask_next;
		end
		else if (ack_in) begin
			stored_data <= concat_data[MaxW - 1:0];
			stored_mask <= concat_mask[MaxW - 1:0];
		end
	reg flush_st;
	reg flush_st_next;
	localparam [0:0] FlushIdle = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			flush_st <= FlushIdle;
		else
			flush_st <= flush_st_next;
	localparam [0:0] FlushWait = 1;
	always @(*) begin
		flush_st_next = FlushIdle;
		flush_ready = 1'b0;
		case (flush_st)
			FlushIdle:
				if (flush_i && !ready_i) begin
					flush_st_next = FlushWait;
					flush_ready = 1'b0;
				end
				else if (flush_i && ready_i) begin
					flush_st_next = FlushIdle;
					flush_ready = 1'b1;
				end
				else
					flush_st_next = FlushIdle;
			FlushWait:
				if (ready_i) begin
					flush_st_next = FlushIdle;
					flush_ready = 1'b1;
				end
				else begin
					flush_st_next = FlushWait;
					flush_ready = 1'b0;
				end
			default: begin
				flush_st_next = FlushIdle;
				flush_ready = 1'b0;
			end
		endcase
	end
	assign flush_done_o = flush_ready;
	assign valid_next = (pos_next >= OutW ? 1'b1 : flush_ready & (pos != {PtrW {1'sb0}}));
	assign ready_next = (ack_out ? 1'b1 : pos_next <= MaxW);
	assign valid_o = valid_next;
	assign data_o = concat_data[OutW - 1:0];
	assign mask_o = concat_mask[OutW - 1:0];
	assign ready_o = ready_next;
endmodule
module prim_pulse_sync (
	clk_src_i,
	rst_src_ni,
	src_pulse_i,
	clk_dst_i,
	rst_dst_ni,
	dst_pulse_o
);
	input wire clk_src_i;
	input wire rst_src_ni;
	input wire src_pulse_i;
	input wire clk_dst_i;
	input wire rst_dst_ni;
	output wire dst_pulse_o;
	reg src_level;
	always @(posedge clk_src_i or negedge rst_src_ni)
		if (!rst_src_ni)
			src_level <= 1'b0;
		else
			src_level <= src_level ^ src_pulse_i;
	wire dst_level;
	prim_flop_2sync #(.Width(1)) prim_flop_2sync(
		.d(src_level),
		.clk_i(clk_dst_i),
		.rst_ni(rst_dst_ni),
		.q(dst_level)
	);
	reg dst_level_q;
	always @(posedge clk_dst_i or negedge rst_dst_ni)
		if (!rst_dst_ni)
			dst_level_q <= 1'b0;
		else
			dst_level_q <= dst_level;
	assign dst_pulse_o = dst_level_q ^ dst_level;
endmodule
module prim_filter (
	clk_i,
	rst_ni,
	enable_i,
	filter_i,
	filter_o
);
	parameter Cycles = 4;
	input clk_i;
	input rst_ni;
	input enable_i;
	input filter_i;
	output filter_o;
	reg [Cycles - 1:0] stored_vector_q;
	wire [Cycles - 1:0] stored_vector_d;
	reg stored_value_q;
	wire update_stored_value;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			stored_value_q <= 1'b0;
		else if (update_stored_value)
			stored_value_q <= filter_i;
	assign stored_vector_d = {stored_vector_q[Cycles - 2:0], filter_i};
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			stored_vector_q <= {Cycles {1'b0}};
		else
			stored_vector_q <= stored_vector_d;
	assign update_stored_value = (stored_vector_d == {Cycles {1'b0}}) | (stored_vector_d == {Cycles {1'b1}});
	assign filter_o = (enable_i ? stored_value_q : filter_i);
endmodule
module prim_filter_ctr (
	clk_i,
	rst_ni,
	enable_i,
	filter_i,
	filter_o
);
	parameter [31:0] Cycles = 4;
	input clk_i;
	input rst_ni;
	input enable_i;
	input filter_i;
	output filter_o;
	localparam [31:0] CTR_WIDTH = $clog2(Cycles);
	function automatic [CTR_WIDTH - 1:0] sv2v_cast_AF84E_unsigned;
		input reg [CTR_WIDTH - 1:0] inp;
		sv2v_cast_AF84E_unsigned = inp;
	endfunction
	localparam [CTR_WIDTH - 1:0] CYCLESM1 = sv2v_cast_AF84E_unsigned(Cycles - 1);
	reg [CTR_WIDTH - 1:0] diff_ctr_q;
	wire [CTR_WIDTH - 1:0] diff_ctr_d;
	reg filter_q;
	reg stored_value_q;
	wire update_stored_value;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			filter_q <= 1'b0;
		else
			filter_q <= filter_i;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			stored_value_q <= 1'b0;
		else if (update_stored_value)
			stored_value_q <= filter_i;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			diff_ctr_q <= {CTR_WIDTH {1'b0}};
		else
			diff_ctr_q <= diff_ctr_d;
	assign diff_ctr_d = (filter_i != filter_q ? {CTR_WIDTH {1'sb0}} : (diff_ctr_q == CYCLESM1 ? CYCLESM1 : diff_ctr_q + 1'b1));
	assign update_stored_value = diff_ctr_d == CYCLESM1;
	assign filter_o = (enable_i ? stored_value_q : filter_i);
endmodule
module prim_subreg (
	clk_i,
	rst_ni,
	we,
	wd,
	de,
	d,
	qe,
	q,
	qs
);
	parameter signed [31:0] DW = 32;
	parameter _sv2v_width_SWACCESS = 16;
	parameter [_sv2v_width_SWACCESS - 1:0] SWACCESS = "RW";
	parameter [DW - 1:0] RESVAL = 1'sb0;
	input clk_i;
	input rst_ni;
	input we;
	input [DW - 1:0] wd;
	input de;
	input [DW - 1:0] d;
	output reg qe;
	output reg [DW - 1:0] q;
	output wire [DW - 1:0] qs;
	wire wr_en;
	wire [DW - 1:0] wr_data;
	generate
		if ((SWACCESS == "RW") || (SWACCESS == "WO")) begin : gen_w
			assign wr_en = we | de;
			assign wr_data = (we == 1'b1 ? wd : d);
		end
		else if (SWACCESS == "RO") begin : gen_ro
			assign wr_en = de;
			assign wr_data = d;
		end
		else if (SWACCESS == "W1S") begin : gen_w1s
			assign wr_en = we | de;
			assign wr_data = (de ? d : q) | (we ? wd : {DW {1'sb0}});
		end
		else if (SWACCESS == "W1C") begin : gen_w1c
			assign wr_en = we | de;
			assign wr_data = (de ? d : q) & (we ? ~wd : {DW {1'sb1}});
		end
		else if (SWACCESS == "W0C") begin : gen_w0c
			assign wr_en = we | de;
			assign wr_data = (de ? d : q) & (we ? wd : {DW {1'sb1}});
		end
		else if (SWACCESS == "RC") begin : gen_rc
			assign wr_en = we | de;
			assign wr_data = (de ? d : q) & (we ? {DW {1'sb0}} : {DW {1'sb1}});
		end
		else begin : gen_hw
			assign wr_en = de;
			assign wr_data = d;
		end
	endgenerate
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			qe <= 1'b0;
		else
			qe <= we;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			q <= RESVAL;
		else if (wr_en)
			q <= wr_data;
	assign qs = q;
endmodule
module prim_subreg_ext (
	re,
	we,
	wd,
	d,
	qe,
	qre,
	q,
	qs
);
	parameter [31:0] DW = 32;
	input re;
	input we;
	input [DW - 1:0] wd;
	input [DW - 1:0] d;
	output wire qe;
	output wire qre;
	output wire [DW - 1:0] q;
	output wire [DW - 1:0] qs;
	assign qs = d;
	assign q = wd;
	assign qe = we;
	assign qre = re;
endmodule
module prim_intr_hw (
	event_intr_i,
	reg2hw_intr_enable_q_i,
	reg2hw_intr_test_q_i,
	reg2hw_intr_test_qe_i,
	reg2hw_intr_state_q_i,
	hw2reg_intr_state_de_o,
	hw2reg_intr_state_d_o,
	intr_o
);
	parameter [31:0] Width = 1;
	input [Width - 1:0] event_intr_i;
	input [Width - 1:0] reg2hw_intr_enable_q_i;
	input [Width - 1:0] reg2hw_intr_test_q_i;
	input reg2hw_intr_test_qe_i;
	input [Width - 1:0] reg2hw_intr_state_q_i;
	output hw2reg_intr_state_de_o;
	output [Width - 1:0] hw2reg_intr_state_d_o;
	output [Width - 1:0] intr_o;
	wire [Width - 1:0] new_event;
	assign new_event = ({Width {reg2hw_intr_test_qe_i}} & reg2hw_intr_test_q_i) | event_intr_i;
	assign hw2reg_intr_state_de_o = |new_event;
	assign hw2reg_intr_state_d_o = new_event | reg2hw_intr_state_q_i;
	assign intr_o = reg2hw_intr_state_q_i & reg2hw_intr_enable_q_i;
endmodule
module prim_secded_39_32_enc (
	in,
	out
);
	input [31:0] in;
	output wire [38:0] out;
	assign out[0] = in[0];
	assign out[1] = in[1];
	assign out[2] = in[2];
	assign out[3] = in[3];
	assign out[4] = in[4];
	assign out[5] = in[5];
	assign out[6] = in[6];
	assign out[7] = in[7];
	assign out[8] = in[8];
	assign out[9] = in[9];
	assign out[10] = in[10];
	assign out[11] = in[11];
	assign out[12] = in[12];
	assign out[13] = in[13];
	assign out[14] = in[14];
	assign out[15] = in[15];
	assign out[16] = in[16];
	assign out[17] = in[17];
	assign out[18] = in[18];
	assign out[19] = in[19];
	assign out[20] = in[20];
	assign out[21] = in[21];
	assign out[22] = in[22];
	assign out[23] = in[23];
	assign out[24] = in[24];
	assign out[25] = in[25];
	assign out[26] = in[26];
	assign out[27] = in[27];
	assign out[28] = in[28];
	assign out[29] = in[29];
	assign out[30] = in[30];
	assign out[31] = in[31];
	assign out[32] = (((((((((((in[2] ^ in[3]) ^ in[7]) ^ in[8]) ^ in[14]) ^ in[15]) ^ in[16]) ^ in[18]) ^ in[19]) ^ in[23]) ^ in[24]) ^ in[28]) ^ in[29];
	assign out[33] = ((((((((((((in[3] ^ in[6]) ^ in[8]) ^ in[12]) ^ in[13]) ^ in[15]) ^ in[17]) ^ in[19]) ^ in[21]) ^ in[25]) ^ in[27]) ^ in[29]) ^ in[30]) ^ in[31];
	assign out[34] = ((((((((((((in[0] ^ in[5]) ^ in[7]) ^ in[9]) ^ in[10]) ^ in[12]) ^ in[13]) ^ in[15]) ^ in[16]) ^ in[22]) ^ in[23]) ^ in[26]) ^ in[27]) ^ in[31];
	assign out[35] = ((((((((((((in[0] ^ in[1]) ^ in[4]) ^ in[6]) ^ in[9]) ^ in[11]) ^ in[12]) ^ in[14]) ^ in[22]) ^ in[23]) ^ in[25]) ^ in[28]) ^ in[29]) ^ in[30];
	assign out[36] = ((((((((((in[0] ^ in[2]) ^ in[3]) ^ in[4]) ^ in[5]) ^ in[11]) ^ in[17]) ^ in[20]) ^ in[24]) ^ in[26]) ^ in[27]) ^ in[30];
	assign out[37] = ((((((((((((in[1] ^ in[2]) ^ in[4]) ^ in[6]) ^ in[10]) ^ in[13]) ^ in[14]) ^ in[16]) ^ in[18]) ^ in[19]) ^ in[20]) ^ in[21]) ^ in[22]) ^ in[26];
	assign out[38] = (((((((((((((in[1] ^ in[5]) ^ in[7]) ^ in[8]) ^ in[9]) ^ in[10]) ^ in[11]) ^ in[17]) ^ in[18]) ^ in[20]) ^ in[21]) ^ in[24]) ^ in[25]) ^ in[28]) ^ in[31];
endmodule
module prim_secded_39_32_dec (
	in,
	d_o,
	syndrome_o,
	err_o
);
	input [38:0] in;
	output wire [31:0] d_o;
	output wire [6:0] syndrome_o;
	output wire [1:0] err_o;
	wire single_error;
	assign syndrome_o[0] = ((((((((((((in[32] ^ in[2]) ^ in[3]) ^ in[7]) ^ in[8]) ^ in[14]) ^ in[15]) ^ in[16]) ^ in[18]) ^ in[19]) ^ in[23]) ^ in[24]) ^ in[28]) ^ in[29];
	assign syndrome_o[1] = (((((((((((((in[33] ^ in[3]) ^ in[6]) ^ in[8]) ^ in[12]) ^ in[13]) ^ in[15]) ^ in[17]) ^ in[19]) ^ in[21]) ^ in[25]) ^ in[27]) ^ in[29]) ^ in[30]) ^ in[31];
	assign syndrome_o[2] = (((((((((((((in[34] ^ in[0]) ^ in[5]) ^ in[7]) ^ in[9]) ^ in[10]) ^ in[12]) ^ in[13]) ^ in[15]) ^ in[16]) ^ in[22]) ^ in[23]) ^ in[26]) ^ in[27]) ^ in[31];
	assign syndrome_o[3] = (((((((((((((in[35] ^ in[0]) ^ in[1]) ^ in[4]) ^ in[6]) ^ in[9]) ^ in[11]) ^ in[12]) ^ in[14]) ^ in[22]) ^ in[23]) ^ in[25]) ^ in[28]) ^ in[29]) ^ in[30];
	assign syndrome_o[4] = (((((((((((in[36] ^ in[0]) ^ in[2]) ^ in[3]) ^ in[4]) ^ in[5]) ^ in[11]) ^ in[17]) ^ in[20]) ^ in[24]) ^ in[26]) ^ in[27]) ^ in[30];
	assign syndrome_o[5] = (((((((((((((in[37] ^ in[1]) ^ in[2]) ^ in[4]) ^ in[6]) ^ in[10]) ^ in[13]) ^ in[14]) ^ in[16]) ^ in[18]) ^ in[19]) ^ in[20]) ^ in[21]) ^ in[22]) ^ in[26];
	assign syndrome_o[6] = ((((((((((((((in[38] ^ in[1]) ^ in[5]) ^ in[7]) ^ in[8]) ^ in[9]) ^ in[10]) ^ in[11]) ^ in[17]) ^ in[18]) ^ in[20]) ^ in[21]) ^ in[24]) ^ in[25]) ^ in[28]) ^ in[31];
	assign d_o[0] = (syndrome_o == 7'h1c) ^ in[0];
	assign d_o[1] = (syndrome_o == 7'h68) ^ in[1];
	assign d_o[2] = (syndrome_o == 7'h31) ^ in[2];
	assign d_o[3] = (syndrome_o == 7'h13) ^ in[3];
	assign d_o[4] = (syndrome_o == 7'h38) ^ in[4];
	assign d_o[5] = (syndrome_o == 7'h54) ^ in[5];
	assign d_o[6] = (syndrome_o == 7'h2a) ^ in[6];
	assign d_o[7] = (syndrome_o == 7'h45) ^ in[7];
	assign d_o[8] = (syndrome_o == 7'h43) ^ in[8];
	assign d_o[9] = (syndrome_o == 7'h4c) ^ in[9];
	assign d_o[10] = (syndrome_o == 7'h64) ^ in[10];
	assign d_o[11] = (syndrome_o == 7'h58) ^ in[11];
	assign d_o[12] = (syndrome_o == 7'h0e) ^ in[12];
	assign d_o[13] = (syndrome_o == 7'h26) ^ in[13];
	assign d_o[14] = (syndrome_o == 7'h29) ^ in[14];
	assign d_o[15] = (syndrome_o == 7'h07) ^ in[15];
	assign d_o[16] = (syndrome_o == 7'h25) ^ in[16];
	assign d_o[17] = (syndrome_o == 7'h52) ^ in[17];
	assign d_o[18] = (syndrome_o == 7'h61) ^ in[18];
	assign d_o[19] = (syndrome_o == 7'h23) ^ in[19];
	assign d_o[20] = (syndrome_o == 7'h70) ^ in[20];
	assign d_o[21] = (syndrome_o == 7'h62) ^ in[21];
	assign d_o[22] = (syndrome_o == 7'h2c) ^ in[22];
	assign d_o[23] = (syndrome_o == 7'h0d) ^ in[23];
	assign d_o[24] = (syndrome_o == 7'h51) ^ in[24];
	assign d_o[25] = (syndrome_o == 7'h4a) ^ in[25];
	assign d_o[26] = (syndrome_o == 7'h34) ^ in[26];
	assign d_o[27] = (syndrome_o == 7'h16) ^ in[27];
	assign d_o[28] = (syndrome_o == 7'h49) ^ in[28];
	assign d_o[29] = (syndrome_o == 7'h0b) ^ in[29];
	assign d_o[30] = (syndrome_o == 7'h1a) ^ in[30];
	assign d_o[31] = (syndrome_o == 7'h46) ^ in[31];
	assign single_error = ^syndrome_o;
	assign err_o[0] = single_error;
	assign err_o[1] = ~single_error & |syndrome_o;
endmodule
module prim_generic_ram_2p (
	clk_a_i,
	clk_b_i,
	a_req_i,
	a_write_i,
	a_addr_i,
	a_wdata_i,
	a_rdata_o,
	b_req_i,
	b_write_i,
	b_addr_i,
	b_wdata_i,
	b_rdata_o
);
	parameter signed [31:0] Width = 32;
	parameter signed [31:0] Depth = 128;
	localparam signed [31:0] Aw = $clog2(Depth);
	input clk_a_i;
	input clk_b_i;
	input a_req_i;
	input a_write_i;
	input [Aw - 1:0] a_addr_i;
	input [Width - 1:0] a_wdata_i;
	output reg [Width - 1:0] a_rdata_o;
	input b_req_i;
	input b_write_i;
	input [Aw - 1:0] b_addr_i;
	input [Width - 1:0] b_wdata_i;
	output reg [Width - 1:0] b_rdata_o;
	reg [Width - 1:0] mem [0:Depth - 1];
	always @(posedge clk_a_i)
		if (a_req_i) begin
			if (a_write_i)
				mem[a_addr_i] <= a_wdata_i;
			a_rdata_o <= mem[a_addr_i];
		end
	always @(posedge clk_b_i)
		if (b_req_i) begin
			if (b_write_i)
				mem[b_addr_i] <= b_wdata_i;
			b_rdata_o <= mem[b_addr_i];
		end
endmodule
module prim_xilinx_ram_2p (
	clk_a_i,
	clk_b_i,
	a_req_i,
	a_write_i,
	a_addr_i,
	a_wdata_i,
	a_rdata_o,
	b_req_i,
	b_write_i,
	b_addr_i,
	b_wdata_i,
	b_rdata_o
);
	parameter signed [31:0] Width = 32;
	parameter signed [31:0] Depth = 128;
	localparam signed [31:0] Aw = $clog2(Depth);
	input clk_a_i;
	input clk_b_i;
	input a_req_i;
	input a_write_i;
	input [Aw - 1:0] a_addr_i;
	input [Width - 1:0] a_wdata_i;
	output reg [Width - 1:0] a_rdata_o;
	input b_req_i;
	input b_write_i;
	input [Aw - 1:0] b_addr_i;
	input [Width - 1:0] b_wdata_i;
	output reg [Width - 1:0] b_rdata_o;
	reg [Width - 1:0] storage [0:Depth - 1];
	always @(posedge clk_a_i)
		if (a_req_i) begin
			if (a_write_i)
				storage[a_addr_i] <= a_wdata_i;
			a_rdata_o <= storage[a_addr_i];
		end
	always @(posedge clk_b_i)
		if (b_req_i) begin
			if (b_write_i)
				storage[b_addr_i] <= b_wdata_i;
			b_rdata_o <= storage[b_addr_i];
		end
endmodule
module prim_diff_decode (
	clk_i,
	rst_ni,
	diff_pi,
	diff_ni,
	level_o,
	rise_o,
	fall_o,
	event_o,
	sigint_o
);
	parameter [0:0] AsyncOn = 1'b0;
	input clk_i;
	input rst_ni;
	input diff_pi;
	input diff_ni;
	output wire level_o;
	output reg rise_o;
	output reg fall_o;
	output wire event_o;
	output reg sigint_o;
	reg level_d;
	reg level_q;
	localparam [1:0] IsSkewed = 1;
	localparam [1:0] IsStd = 0;
	localparam [1:0] SigInt = 2;
	generate
		if (AsyncOn) begin : gen_async
			reg [1:0] state_d;
			reg [1:0] state_q;
			wire diff_p_edge;
			wire diff_n_edge;
			wire diff_check_ok;
			wire level;
			reg diff_pq;
			reg diff_nq;
			wire diff_pd;
			wire diff_nd;
			prim_flop_2sync #(
				.Width(1),
				.ResetValue(0)
			) i_sync_p(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.d(diff_pi),
				.q(diff_pd)
			);
			prim_flop_2sync #(
				.Width(1),
				.ResetValue(1)
			) i_sync_n(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.d(diff_ni),
				.q(diff_nd)
			);
			assign diff_p_edge = diff_pq ^ diff_pd;
			assign diff_n_edge = diff_nq ^ diff_nd;
			assign diff_check_ok = diff_pd ^ diff_nd;
			assign level = diff_pd;
			assign level_o = level_d;
			assign event_o = rise_o | fall_o;
			always @(*) begin : p_diff_fsm
				state_d = state_q;
				level_d = level_q;
				rise_o = 1'b0;
				fall_o = 1'b0;
				sigint_o = 1'b0;
				case (state_q)
					IsStd:
						if (diff_check_ok) begin
							level_d = level;
							if (diff_p_edge && diff_n_edge)
								if (level)
									rise_o = 1'b1;
								else
									fall_o = 1'b1;
						end
						else if (diff_p_edge || diff_n_edge)
							state_d = IsSkewed;
						else begin
							state_d = SigInt;
							sigint_o = 1'b1;
						end
					IsSkewed:
						if (diff_check_ok) begin
							state_d = IsStd;
							level_d = level;
							if (level)
								rise_o = 1'b1;
							else
								fall_o = 1'b1;
						end
						else begin
							state_d = SigInt;
							sigint_o = 1'b1;
						end
					SigInt: begin
						sigint_o = 1'b1;
						if (diff_check_ok) begin
							state_d = IsStd;
							sigint_o = 1'b0;
						end
					end
					default:
						;
				endcase
			end
			always @(posedge clk_i or negedge rst_ni) begin : p_sync_reg
				if (!rst_ni) begin
					state_q <= IsStd;
					diff_pq <= 1'b0;
					diff_nq <= 1'b1;
					level_q <= 1'b0;
				end
				else begin
					state_q <= state_d;
					diff_pq <= diff_pd;
					diff_nq <= diff_nd;
					level_q <= level_d;
				end
			end
		end
		else begin : gen_no_async
			reg diff_pq;
			wire diff_pd;
			assign diff_pd = diff_pi;
			always @(*) sigint_o = ~(diff_pi ^ diff_ni);
			assign level_o = (sigint_o ? level_q : diff_pi);
			always @(*) level_d = level_o;
			always @(*) rise_o = (~diff_pq & diff_pi) & ~sigint_o;
			always @(*) fall_o = (diff_pq & ~diff_pi) & ~sigint_o;
			assign event_o = rise_o | fall_o;
			always @(posedge clk_i or negedge rst_ni) begin : p_edge_reg
				if (!rst_ni) begin
					diff_pq <= 1'b0;
					level_q <= 1'b0;
				end
				else begin
					diff_pq <= diff_pd;
					level_q <= level_d;
				end
			end
		end
	endgenerate
endmodule
module prim_pad_wrapper (
	inout_io,
	in_o,
	out_i,
	oe_i,
	attr_i
);
	localparam integer prim_pkg_ImplGeneric = 0;
	parameter integer Impl = prim_pkg_ImplGeneric;
	parameter [31:0] AttrDw = 6;
	inout wire inout_io;
	output wire in_o;
	input out_i;
	input oe_i;
	input [AttrDw - 1:0] attr_i;
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	generate
		if (Impl == ImplGeneric) begin : gen_pad_generic
			prim_generic_pad_wrapper #(.AttrDw(AttrDw)) i_pad_wrapper(
				.inout_io(inout_io),
				.in_o(in_o),
				.out_i(out_i),
				.oe_i(oe_i),
				.attr_i(attr_i)
			);
		end
		else if (Impl == ImplXilinx) begin : gen_pad_xilinx
			prim_xilinx_pad_wrapper #(.AttrDw(AttrDw)) i_pad_wrapper(
				.inout_io(inout_io),
				.in_o(in_o),
				.out_i(out_i),
				.oe_i(oe_i),
				.attr_i(attr_i)
			);
		end
	endgenerate
endmodule
module prim_generic_pad_wrapper (
	inout_io,
	in_o,
	out_i,
	oe_i,
	attr_i
);
	parameter [31:0] AttrDw = 6;
	inout wire inout_io;
	output wire in_o;
	input out_i;
	input oe_i;
	input [AttrDw - 1:0] attr_i;
	wire kp;
	wire pu;
	wire pd;
	wire od;
	wire inv;
	wire drv;
	assign {drv, kp, pu, pd, od, inv} = attr_i[5:0];
	assign in_o = inv ^ inout_io;
	wire oe;
	wire out;
	assign out = out_i ^ inv;
	assign oe = oe_i & ((od & ~out) | ~od);
	assign inout_io = (oe ? out : 1'bz);
endmodule
module prim_xilinx_pad_wrapper (
	inout_io,
	in_o,
	out_i,
	oe_i,
	attr_i
);
	parameter [31:0] AttrDw = 2;
	inout wire inout_io;
	output wire in_o;
	input out_i;
	input oe_i;
	input [AttrDw - 1:0] attr_i;
	wire od;
	wire inv;
	assign {od, inv} = attr_i[1:0];
	assign in_o = inv ^ inout_io;
	wire oe;
	wire out;
	assign out = out_i ^ inv;
	assign oe = oe_i & ((od & ~out) | ~od);
	assign inout_io = (oe ? out : 1'bz);
endmodule
module prim_clock_mux2 (
	clk0_i,
	clk1_i,
	sel_i,
	clk_o
);
	localparam integer prim_pkg_ImplGeneric = 0;
	parameter integer Impl = prim_pkg_ImplGeneric;
	input clk0_i;
	input clk1_i;
	input sel_i;
	output wire clk_o;
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	generate
		if (Impl == ImplGeneric) begin : gen_generic
			prim_generic_clock_mux2 u_impl_generic(
				.clk0_i(clk0_i),
				.clk1_i(clk1_i),
				.sel_i(sel_i),
				.clk_o(clk_o)
			);
		end
		else if (Impl == ImplXilinx) begin : gen_xilinx
			prim_xilinx_clock_mux2 u_impl_xilinx(
				.clk0_i(clk0_i),
				.clk1_i(clk1_i),
				.sel_i(sel_i),
				.clk_o(clk_o)
			);
		end
	endgenerate
endmodule
module prim_generic_clock_mux2 (
	clk0_i,
	clk1_i,
	sel_i,
	clk_o
);
	input clk0_i;
	input clk1_i;
	input sel_i;
	output wire clk_o;
	assign clk_o = (sel_i ? clk1_i : clk0_i);
endmodule
module prim_xilinx_clock_mux2 (
	clk0_i,
	clk1_i,
	sel_i,
	clk_o
);
	input clk0_i;
	input clk1_i;
	input sel_i;
	output wire clk_o;
	BUFGMUX bufgmux_i(
		.S(sel_i),
		.I0(clk0_i),
		.I1(clk1_i),
		.O(clk_o)
	);
endmodule
module tlul_err_resp (
	clk_i,
	rst_ni,
	tl_h_i,
	tl_h_o
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_h_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_h_o;
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	reg [2:0] err_opcode;
	reg [top_pkg_TL_AIW - 1:0] err_source;
	reg [top_pkg_TL_SZW - 1:0] err_size;
	reg err_req_pending;
	reg err_rsp_pending;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			err_req_pending <= 1'b0;
			err_source <= {top_pkg_TL_AIW {1'b0}};
			err_opcode <= Get;
			err_size <= {top_pkg_TL_SZW {1'sb0}};
		end
		else if (tl_h_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] && tl_h_o[0]) begin
			err_req_pending <= 1'b1;
			err_source <= tl_h_i[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)];
			err_opcode <= tl_h_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)];
			err_size <= tl_h_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)];
		end
		else if (!err_rsp_pending)
			err_req_pending <= 1'b0;
	assign tl_h_o[0] = ~err_rsp_pending & ~(err_req_pending & ~tl_h_i[0]);
	assign tl_h_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] = err_req_pending | err_rsp_pending;
	assign tl_h_o[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1] = {((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1)) + 1)) + 1 {1'sb1}};
	assign tl_h_o[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1] = err_source;
	assign tl_h_o[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1] = {((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1)) + 1)) + 1 {1'sb0}};
	assign tl_h_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)] = {((3 + (top_pkg_TL_SZW + 58)) >= (((3 + (top_pkg_TL_SZW + 58)) - ((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + 58)) - (top_pkg_TL_SZW + 59)) + 1 : ((top_pkg_TL_SZW + 59) - (3 + (top_pkg_TL_SZW + 58))) + 1)) + 1) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - ((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)) + 1)) + 1 : ((((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - ((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)) + 1) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1) {1'sb0}};
	assign tl_h_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)] = err_size;
	assign tl_h_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)] = (err_opcode == Get ? AccessAckData : AccessAck);
	assign tl_h_o[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW] = {((top_pkg_TL_DUW + 1) - (((top_pkg_TL_DUW + 1) - top_pkg_TL_DUW) + 1)) + 1 {1'sb0}};
	assign tl_h_o[1] = 1'b1;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			err_rsp_pending <= 1'b0;
		else if ((err_req_pending || err_rsp_pending) && !tl_h_i[0])
			err_rsp_pending <= 1'b1;
		else
			err_rsp_pending <= 1'b0;
endmodule
module tlul_socket_1n (
	clk_i,
	rst_ni,
	tl_h_i,
	tl_h_o,
	tl_d_o,
	tl_d_i,
	dev_select
);
	parameter [31:0] N = 4;
	parameter [0:0] HReqPass = 1'b1;
	parameter [0:0] HRspPass = 1'b1;
	parameter [N - 1:0] DReqPass = {N {1'b1}};
	parameter [N - 1:0] DRspPass = {N {1'b1}};
	parameter [3:0] HReqDepth = 4'h2;
	parameter [3:0] HRspDepth = 4'h2;
	parameter [(N * 4) - 1:0] DReqDepth = {N {4'h2}};
	parameter [(N * 4) - 1:0] DRspDepth = {N {4'h2}};
	localparam [31:0] NWD = $clog2(N + 1);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_h_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_h_o;
	output wire [(0 >= (N - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((2 - N) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) + (((N - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1) : ((2 - N) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((N - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) - 1)) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (N * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (N * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15))):(0 >= (N - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (N - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((N - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] tl_d_o;
	input wire [(0 >= (N - 1) ? (((7 + top_pkg_TL_SZW) + 58) >= 0 ? ((2 - N) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) + (((N - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1) : ((2 - N) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + ((((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((N - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))) - 1)) : (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (N * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (N * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW))):(0 >= (N - 1) ? (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (N - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((N - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))) : (((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] tl_d_i;
	input [NWD - 1:0] dev_select;
	wire [NWD - 1:0] dev_select_t;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_t_o;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_t_i;
	tlul_fifo_sync #(
		.ReqPass(HReqPass),
		.RspPass(HRspPass),
		.ReqDepth(HReqDepth),
		.RspDepth(HRspDepth),
		.SpareReqW(NWD)
	) fifo_h(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_h_i(tl_h_i),
		.tl_h_o(tl_h_o),
		.tl_d_o(tl_t_o),
		.tl_d_i(tl_t_i),
		.spare_req_i(dev_select),
		.spare_req_o(dev_select_t),
		.spare_rsp_i(1'b0),
		.spare_rsp_o()
	);
	reg [7:0] num_req_outstanding;
	reg [NWD - 1:0] dev_select_outstanding;
	wire hold_all_requests;
	wire accept_t_req;
	wire accept_t_rsp;
	assign accept_t_req = tl_t_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & tl_t_i[0];
	assign accept_t_rsp = tl_t_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] & tl_t_o[0];
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			num_req_outstanding <= 8'h00;
			dev_select_outstanding <= {NWD {1'sb0}};
		end
		else if (accept_t_req) begin
			if (!accept_t_rsp)
				num_req_outstanding <= num_req_outstanding + 8'h01;
			dev_select_outstanding <= dev_select_t;
		end
		else if (accept_t_rsp)
			num_req_outstanding <= num_req_outstanding - 8'h01;
	assign hold_all_requests = (num_req_outstanding != 8'h00) & (dev_select_t != dev_select_outstanding);
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_u_o [0:N];
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_u_i [0:N];
	generate
		genvar i;
		for (i = 0; i < N; i = i + 1) begin : gen_u_o
			function automatic signed [NWD - 1:0] sv2v_cast_3B809_signed;
				input reg signed [NWD - 1:0] inp;
				sv2v_cast_3B809_signed = inp;
			endfunction
			assign tl_u_o[i][7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] = (tl_t_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & (dev_select_t == sv2v_cast_3B809_signed(i))) & ~hold_all_requests;
			assign tl_u_o[i][6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] = tl_t_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)];
			assign tl_u_o[i][3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] = tl_t_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)];
			assign tl_u_o[i][top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)] = tl_t_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)];
			assign tl_u_o[i][top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)] = tl_t_o[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)];
			assign tl_u_o[i][top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] = tl_t_o[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)];
			assign tl_u_o[i][top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] = tl_t_o[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)];
			assign tl_u_o[i][top_pkg_TL_DW + 16-:top_pkg_TL_DW] = tl_t_o[top_pkg_TL_DW + 16-:top_pkg_TL_DW];
			assign tl_u_o[i][16-:16] = tl_t_o[16-:16];
		end
	endgenerate
	reg [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_t_p;
	reg hfifo_reqready;
	function automatic signed [NWD - 1:0] sv2v_cast_3B809_signed;
		input reg signed [NWD - 1:0] inp;
		sv2v_cast_3B809_signed = inp;
	endfunction
	always @(*) begin
		hfifo_reqready = tl_u_i[N][0];
		begin : sv2v_autoblock_344
			reg signed [31:0] idx;
			for (idx = 0; idx < N; idx = idx + 1)
				if (dev_select_t == sv2v_cast_3B809_signed(idx))
					hfifo_reqready = tl_u_i[idx][0];
		end
		if (hold_all_requests)
			hfifo_reqready = 1'b0;
	end
	assign tl_t_i[0] = tl_t_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & hfifo_reqready;
	always @(*) begin
		tl_t_p = tl_u_i[N];
		begin : sv2v_autoblock_345
			reg signed [31:0] idx;
			for (idx = 0; idx < N; idx = idx + 1)
				if (dev_select_outstanding == sv2v_cast_3B809_signed(idx))
					tl_t_p = tl_u_i[idx];
		end
	end
	assign tl_t_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] = tl_t_p[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))];
	assign tl_t_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)] = tl_t_p[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)];
	assign tl_t_i[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)] = tl_t_p[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)];
	assign tl_t_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)] = tl_t_p[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)];
	assign tl_t_i[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1] = tl_t_p[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1];
	assign tl_t_i[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1] = tl_t_p[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1];
	assign tl_t_i[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1] = tl_t_p[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1];
	assign tl_t_i[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW] = tl_t_p[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW];
	assign tl_t_i[1] = tl_t_p[1];
	generate
		for (i = 0; i < (N + 1); i = i + 1) begin : gen_u_o_d_ready
			assign tl_u_o[i][0] = tl_t_o[0];
		end
	endgenerate
	generate
		for (i = 0; i < N; i = i + 1) begin : gen_dfifo
			tlul_fifo_sync #(
				.ReqPass(DReqPass[i]),
				.RspPass(DRspPass[i]),
				.ReqDepth(DReqDepth[i * 4+:4]),
				.RspDepth(DRspDepth[i * 4+:4])
			) fifo_d(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.tl_h_i(tl_u_o[i]),
				.tl_h_o(tl_u_i[i]),
				.tl_d_o(tl_d_o[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((0 >= (N - 1) ? i : (N - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))]),
				.tl_d_i(tl_d_i[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((0 >= (N - 1) ? i : (N - 1) - i) * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))]),
				.spare_req_i(1'b0),
				.spare_req_o(),
				.spare_rsp_i(1'b0),
				.spare_rsp_o()
			);
		end
	endgenerate
	function automatic [NWD - 1:0] sv2v_cast_3B809_unsigned;
		input reg [NWD - 1:0] inp;
		sv2v_cast_3B809_unsigned = inp;
	endfunction
	assign tl_u_o[N][7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] = (tl_t_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & (dev_select_t == sv2v_cast_3B809_unsigned(N))) & ~hold_all_requests;
	assign tl_u_o[N][6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] = tl_t_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)];
	assign tl_u_o[N][3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] = tl_t_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)];
	assign tl_u_o[N][top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)] = tl_t_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)];
	assign tl_u_o[N][top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)] = tl_t_o[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)];
	assign tl_u_o[N][top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] = tl_t_o[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)];
	assign tl_u_o[N][top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] = tl_t_o[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)];
	assign tl_u_o[N][top_pkg_TL_DW + 16-:top_pkg_TL_DW] = tl_t_o[top_pkg_TL_DW + 16-:top_pkg_TL_DW];
	assign tl_u_o[N][16-:16] = tl_t_o[16-:16];
	tlul_err_resp err_resp(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_h_i(tl_u_o[N]),
		.tl_h_o(tl_u_i[N])
	);
endmodule
module tlul_fifo_sync (
	clk_i,
	rst_ni,
	tl_h_i,
	tl_h_o,
	tl_d_o,
	tl_d_i,
	spare_req_i,
	spare_req_o,
	spare_rsp_i,
	spare_rsp_o
);
	parameter [31:0] ReqPass = 1'b1;
	parameter [31:0] RspPass = 1'b1;
	parameter [31:0] ReqDepth = 2;
	parameter [31:0] RspDepth = 2;
	parameter [31:0] SpareReqW = 1;
	parameter [31:0] SpareRspW = 1;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_h_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_h_o;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_d_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_d_i;
	input [SpareReqW - 1:0] spare_req_i;
	output [SpareReqW - 1:0] spare_req_o;
	input [SpareRspW - 1:0] spare_rsp_i;
	output [SpareRspW - 1:0] spare_rsp_o;
	localparam [31:0] REQFIFO_WIDTH = ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15) + SpareReqW;
	prim_fifo_sync #(
		.Width(REQFIFO_WIDTH),
		.Pass(ReqPass),
		.Depth(ReqDepth)
	) reqfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(tl_h_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))]),
		.wready(tl_h_o[0]),
		.wdata({tl_h_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_h_i[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_h_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)], tl_h_i[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)], tl_h_i[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)], tl_h_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)], tl_h_i[top_pkg_TL_DW + 16-:top_pkg_TL_DW], tl_h_i[16-:16], spare_req_i}),
		.depth(),
		.rvalid(tl_d_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))]),
		.rready(tl_d_i[0]),
		.rdata({tl_d_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_d_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_d_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)], tl_d_o[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)], tl_d_o[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)], tl_d_o[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)], tl_d_o[top_pkg_TL_DW + 16-:top_pkg_TL_DW], tl_d_o[16-:16], spare_req_o})
	);
	localparam [31:0] RSPFIFO_WIDTH = ((((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)) - 2) + SpareRspW;
	localparam [2:0] tlul_pkg_AccessAckData = 3'h1;
	prim_fifo_sync #(
		.Width(RSPFIFO_WIDTH),
		.Pass(RspPass),
		.Depth(RspDepth)
	) rspfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(tl_d_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))]),
		.wready(tl_d_o[0]),
		.wdata({tl_d_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_d_i[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_d_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)], tl_d_i[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1], tl_d_i[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1], (tl_d_i[6 + (top_pkg_TL_SZW + 58)-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + 58)) - (3 + (top_pkg_TL_SZW + 59))) + 1 : ((3 + (top_pkg_TL_SZW + 59)) - (6 + (top_pkg_TL_SZW + 58))) + 1)] == 3'h1 ? tl_d_i[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1] : {top_pkg_TL_DW {1'b0}}), tl_d_i[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW], tl_d_i[1], spare_rsp_i}),
		.depth(),
		.rvalid(tl_h_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))]),
		.rready(tl_h_i[0]),
		.rdata({tl_h_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_h_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_h_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)], tl_h_o[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1], tl_h_o[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1], tl_h_o[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1], tl_h_o[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW], tl_h_o[1], spare_rsp_o})
	);
endmodule
module tlul_fifo_async (
	clk_h_i,
	rst_h_ni,
	clk_d_i,
	rst_d_ni,
	tl_h_i,
	tl_h_o,
	tl_d_o,
	tl_d_i
);
	parameter [31:0] ReqDepth = 3;
	parameter [31:0] RspDepth = 3;
	input clk_h_i;
	input rst_h_ni;
	input clk_d_i;
	input rst_d_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_h_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_h_o;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_d_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_d_i;
	localparam [31:0] REQFIFO_WIDTH = (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15;
	prim_fifo_async #(
		.Width(REQFIFO_WIDTH),
		.Depth(ReqDepth)
	) reqfifo(
		.clk_wr_i(clk_h_i),
		.rst_wr_ni(rst_h_ni),
		.clk_rd_i(clk_d_i),
		.rst_rd_ni(rst_d_ni),
		.wvalid(tl_h_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))]),
		.wready(tl_h_o[0]),
		.wdata({tl_h_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_h_i[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_h_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)], tl_h_i[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)], tl_h_i[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)], tl_h_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)], tl_h_i[top_pkg_TL_DW + 16-:top_pkg_TL_DW], tl_h_i[16-:16]}),
		.rvalid(tl_d_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))]),
		.rready(tl_d_i[0]),
		.rdata({tl_d_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_d_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)], tl_d_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)], tl_d_o[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)], tl_d_o[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)], tl_d_o[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)], tl_d_o[top_pkg_TL_DW + 16-:top_pkg_TL_DW], tl_d_o[16-:16]}),
		.wdepth(),
		.rdepth()
	);
	localparam [31:0] RSPFIFO_WIDTH = (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)) - 2;
	prim_fifo_async #(
		.Width(RSPFIFO_WIDTH),
		.Depth(RspDepth)
	) rspfifo(
		.clk_wr_i(clk_d_i),
		.rst_wr_ni(rst_d_ni),
		.clk_rd_i(clk_h_i),
		.rst_rd_ni(rst_h_ni),
		.wvalid(tl_d_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))]),
		.wready(tl_d_o[0]),
		.wdata({tl_d_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_d_i[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_d_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)], tl_d_i[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1], tl_d_i[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1], tl_d_i[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1], tl_d_i[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW], tl_d_i[1]}),
		.rvalid(tl_h_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))]),
		.rready(tl_h_i[0]),
		.rdata({tl_h_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_h_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)], tl_h_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)], tl_h_o[top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))-:((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) + 1], tl_h_o[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1], tl_h_o[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1], tl_h_o[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW], tl_h_o[1]}),
		.wdepth(),
		.rdepth()
	);
endmodule
module tlul_err (
	clk_i,
	rst_ni,
	tl_i,
	err_o
);
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	output wire err_o;
	localparam signed [31:0] IW = top_pkg_TL_AIW;
	localparam signed [31:0] SZW = top_pkg_TL_SZW;
	localparam signed [31:0] DW = top_pkg_TL_DW;
	localparam signed [31:0] MW = top_pkg_TL_DBW;
	localparam signed [31:0] SubAW = 2;
	wire opcode_allowed;
	wire a_config_allowed;
	wire op_full;
	wire op_partial;
	wire op_get;
	assign op_full = tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutFullData;
	assign op_partial = tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutPartialData;
	assign op_get = tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == Get;
	assign err_o = ~(opcode_allowed & a_config_allowed);
	assign opcode_allowed = ((tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutFullData) | (tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutPartialData)) | (tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == Get);
	reg addr_sz_chk;
	reg mask_chk;
	reg fulldata_chk;
	wire [MW - 1:0] mask;
	assign mask = 1 << tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - ((top_pkg_TL_AW - 1) - (SubAW - 1)):(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 1)];
	always @(*) begin
		addr_sz_chk = 1'b0;
		mask_chk = 1'b0;
		fulldata_chk = 1'b0;
		if (tl_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))])
			case (tl_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)])
				'h0: begin
					addr_sz_chk = 1'b1;
					mask_chk = ~|(tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] & ~mask);
					fulldata_chk = |(tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] & mask);
				end
				'h1: begin
					addr_sz_chk = ~tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 1)];
					mask_chk = (tl_i[(32 + (top_pkg_TL_DBW + 48)) - 30] ? ~|(tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] & 4'b0011) : ~|(tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] & 4'b1100));
					fulldata_chk = (tl_i[(32 + (top_pkg_TL_DBW + 48)) - 30] ? &tl_i[(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 4):(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 3)] : &tl_i[(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 2):(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 1)]);
				end
				'h2: begin
					addr_sz_chk = ~|tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - ((top_pkg_TL_AW - 1) - (SubAW - 1)):(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 1)];
					mask_chk = 1'b1;
					fulldata_chk = &tl_i[(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 4):(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DBW - 1)];
				end
				default: begin
					addr_sz_chk = 1'b0;
					mask_chk = 1'b0;
					fulldata_chk = 1'b0;
				end
			endcase
		else begin
			addr_sz_chk = 1'b0;
			mask_chk = 1'b0;
			fulldata_chk = 1'b0;
		end
	end
	assign a_config_allowed = (addr_sz_chk & mask_chk) & ((op_get | op_partial) | fulldata_chk);
endmodule
module tlul_socket_m1 (
	clk_i,
	rst_ni,
	tl_h_i,
	tl_h_o,
	tl_d_o,
	tl_d_i
);
	parameter [31:0] M = 4;
	parameter [M - 1:0] HReqPass = {M {1'b1}};
	parameter [M - 1:0] HRspPass = {M {1'b1}};
	parameter [(M * 4) - 1:0] HReqDepth = {M {4'h2}};
	parameter [(M * 4) - 1:0] HRspDepth = {M {4'h2}};
	parameter [0:0] DReqPass = 1'b1;
	parameter [0:0] DRspPass = 1'b1;
	parameter [3:0] DReqDepth = 4'h2;
	parameter [3:0] DRspDepth = 4'h2;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(0 >= (M - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((2 - M) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) + (((M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1) : ((2 - M) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) - 1)) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (M * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (M * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15))):(0 >= (M - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] tl_h_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(0 >= (M - 1) ? (((7 + top_pkg_TL_SZW) + 58) >= 0 ? ((2 - M) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) + (((M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1) : ((2 - M) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + ((((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))) - 1)) : (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (M * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (M * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW))):(0 >= (M - 1) ? (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))) : (((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] tl_h_o;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_d_o;
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_d_i;
	localparam [31:0] IDW = top_pkg_TL_AIW;
	localparam [31:0] STIDW = $clog2(M);
	wire [(0 >= (M - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((2 - M) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) + (((M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1) : ((2 - M) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) - 1)) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (M * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (M * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15))):(0 >= (M - 1) ? (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (M - 1) * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((M - 1) * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))) : (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] hreq_fifo_o;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] hrsp_fifo_i [0:M - 1];
	wire [M - 1:0] hrequest;
	wire [M - 1:0] hgrant;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] dreq_fifo_i;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] drsp_fifo_o;
	wire arb_valid;
	wire arb_ready;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] arb_data;
	generate
		genvar i;
		for (i = 0; i < M; i = i + 1) begin : gen_host_fifo
			wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] hreq_fifo_i;
			wire [STIDW - 1:0] reqid_sub;
			wire [IDW - 1:0] shifted_id;
			assign reqid_sub = i;
			assign shifted_id = {tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AIW - 1) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AIW - 1))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AIW - 1) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AIW - 1)))) - (IDW - STIDW)) + 1)+:IDW - STIDW], reqid_sub};
			wire [IDW - 1:IDW - STIDW] unused_tl_h_source;
			assign unused_tl_h_source = tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - ((top_pkg_TL_AIW - 1) - (IDW - 1)) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - ((top_pkg_TL_AIW - 1) - (IDW - 1)))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - ((top_pkg_TL_AIW - 1) - (IDW - 1)) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - ((top_pkg_TL_AIW - 1) - (IDW - 1))))) + STIDW) - 1)-:STIDW];
			function automatic [0:0] sv2v_cast_1;
				input reg [0:0] inp;
				sv2v_cast_1 = inp;
			endfunction
			function automatic [2:0] sv2v_cast_3;
				input reg [2:0] inp;
				sv2v_cast_3 = inp;
			endfunction
			function automatic [top_pkg_TL_SZW - 1:0] sv2v_cast_F00AF;
				input reg [top_pkg_TL_SZW - 1:0] inp;
				sv2v_cast_F00AF = inp;
			endfunction
			function automatic [top_pkg_TL_AIW - 1:0] sv2v_cast_F1F18;
				input reg [top_pkg_TL_AIW - 1:0] inp;
				sv2v_cast_F1F18 = inp;
			endfunction
			function automatic [top_pkg_TL_AW - 1:0] sv2v_cast_4CD75;
				input reg [top_pkg_TL_AW - 1:0] inp;
				sv2v_cast_4CD75 = inp;
			endfunction
			function automatic [top_pkg_TL_DBW - 1:0] sv2v_cast_37199;
				input reg [top_pkg_TL_DBW - 1:0] inp;
				sv2v_cast_37199 = inp;
			endfunction
			function automatic [top_pkg_TL_DW - 1:0] sv2v_cast_2497D;
				input reg [top_pkg_TL_DW - 1:0] inp;
				sv2v_cast_2497D = inp;
			endfunction
			function automatic [15:0] sv2v_cast_16;
				input reg [15:0] inp;
				sv2v_cast_16 = inp;
			endfunction
			assign hreq_fifo_i = {sv2v_cast_1(tl_h_i[((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))))]), sv2v_cast_3(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))))) + ((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)) - 1)-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)]), sv2v_cast_3(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))))) + ((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)) - 1)-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)]), sv2v_cast_F00AF(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + ((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)) - 1)-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)]), sv2v_cast_F1F18(shifted_id), sv2v_cast_4CD75(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + ((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)) - 1)-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)]), sv2v_cast_37199(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_DBW + (top_pkg_TL_DW + 16) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_DBW + (top_pkg_TL_DW + 16) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + ((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)) - 1)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)]), sv2v_cast_2497D(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_DW + 16 : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_DW + 16)) : ((((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? top_pkg_TL_DW + 16 : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (top_pkg_TL_DW + 16))) + top_pkg_TL_DW) - 1)-:top_pkg_TL_DW]), sv2v_cast_16(tl_h_i[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 16 : ((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) : (((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 16 : ((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW)) + 15)-:16]), sv2v_cast_1(tl_h_i[((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)])};
			tlul_fifo_sync #(
				.ReqPass(HReqPass[i]),
				.RspPass(HRspPass[i]),
				.ReqDepth(HReqDepth[i * 4+:4]),
				.RspDepth(HRspDepth[i * 4+:4]),
				.SpareReqW(1)
			) u_hostfifo(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.tl_h_i(hreq_fifo_i),
				.tl_h_o(tl_h_o[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + ((0 >= (M - 1) ? i : (M - 1) - i) * (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))]),
				.tl_d_o(hreq_fifo_o[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + ((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))]),
				.tl_d_i(hrsp_fifo_i[i]),
				.spare_req_i(1'b0),
				.spare_req_o(),
				.spare_rsp_i(1'b0),
				.spare_rsp_o()
			);
		end
	endgenerate
	tlul_fifo_sync #(
		.ReqPass(DReqPass),
		.RspPass(DRspPass),
		.ReqDepth(DReqDepth),
		.RspDepth(DRspDepth),
		.SpareReqW(1)
	) u_devicefifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_h_i(dreq_fifo_i),
		.tl_h_o(drsp_fifo_o),
		.tl_d_o(tl_d_o),
		.tl_d_i(tl_d_i),
		.spare_req_i(1'b0),
		.spare_req_o(),
		.spare_rsp_i(1'b0),
		.spare_rsp_o()
	);
	generate
		for (i = 0; i < M; i = i + 1) begin : gen_arbreqgnt
			assign hrequest[i] = hreq_fifo_o[((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) : ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) - (7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))))];
		end
	endgenerate
	assign arb_ready = drsp_fifo_o[0];
	prim_arbiter #(
		.N(M),
		.DW((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)
	) u_reqarb(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.req_i(hrequest),
		.data_i(hreq_fifo_o),
		.gnt_o(hgrant),
		.idx_o(),
		.valid_o(arb_valid),
		.data_o(arb_data),
		.ready_i(arb_ready)
	);
	wire [M - 1:0] hfifo_rspvalid;
	wire [M - 1:0] dfifo_rspready;
	wire [IDW - 1:0] hfifo_rspid;
	wire dfifo_rspready_merged;
	assign dfifo_rspready_merged = |dfifo_rspready;
	function automatic [0:0] sv2v_cast_1;
		input reg [0:0] inp;
		sv2v_cast_1 = inp;
	endfunction
	function automatic [2:0] sv2v_cast_3;
		input reg [2:0] inp;
		sv2v_cast_3 = inp;
	endfunction
	function automatic [top_pkg_TL_SZW - 1:0] sv2v_cast_F00AF;
		input reg [top_pkg_TL_SZW - 1:0] inp;
		sv2v_cast_F00AF = inp;
	endfunction
	function automatic [top_pkg_TL_AIW - 1:0] sv2v_cast_F1F18;
		input reg [top_pkg_TL_AIW - 1:0] inp;
		sv2v_cast_F1F18 = inp;
	endfunction
	function automatic [top_pkg_TL_AW - 1:0] sv2v_cast_4CD75;
		input reg [top_pkg_TL_AW - 1:0] inp;
		sv2v_cast_4CD75 = inp;
	endfunction
	function automatic [top_pkg_TL_DBW - 1:0] sv2v_cast_37199;
		input reg [top_pkg_TL_DBW - 1:0] inp;
		sv2v_cast_37199 = inp;
	endfunction
	function automatic [top_pkg_TL_DW - 1:0] sv2v_cast_2497D;
		input reg [top_pkg_TL_DW - 1:0] inp;
		sv2v_cast_2497D = inp;
	endfunction
	function automatic [15:0] sv2v_cast_16;
		input reg [15:0] inp;
		sv2v_cast_16 = inp;
	endfunction
	assign dreq_fifo_i = {sv2v_cast_1(arb_valid), sv2v_cast_3(arb_data[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)]), sv2v_cast_3(arb_data[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)]), sv2v_cast_F00AF(arb_data[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)]), sv2v_cast_F1F18(arb_data[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)]), sv2v_cast_4CD75(arb_data[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)]), sv2v_cast_37199(arb_data[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)]), sv2v_cast_2497D(arb_data[top_pkg_TL_DW + 16-:top_pkg_TL_DW]), sv2v_cast_16(arb_data[16-:16]), sv2v_cast_1(dfifo_rspready_merged)};
	assign hfifo_rspid = {{STIDW {1'b0}}, drsp_fifo_o[(top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - ((top_pkg_TL_AIW - 1) - (IDW - 1)):(top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - ((top_pkg_TL_AIW - 1) - STIDW)]};
	generate
		for (i = 0; i < M; i = i + 1) begin : gen_idrouting
			assign hfifo_rspvalid[i] = drsp_fifo_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] & (drsp_fifo_o[(top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_AIW - 1)+:STIDW] == i);
			assign dfifo_rspready[i] = (hreq_fifo_o[((0 >= (M - 1) ? i : (M - 1) - i) * (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] & (drsp_fifo_o[(top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))) - (top_pkg_TL_AIW - 1)+:STIDW] == i)) & drsp_fifo_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))];
			function automatic [top_pkg_TL_DIW - 1:0] sv2v_cast_B5AB2;
				input reg [top_pkg_TL_DIW - 1:0] inp;
				sv2v_cast_B5AB2 = inp;
			endfunction
			function automatic [top_pkg_TL_DUW - 1:0] sv2v_cast_92577;
				input reg [top_pkg_TL_DUW - 1:0] inp;
				sv2v_cast_92577 = inp;
			endfunction
			assign hrsp_fifo_i[i] = {sv2v_cast_1(hfifo_rspvalid[i]), sv2v_cast_3(drsp_fifo_o[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((6 + (top_pkg_TL_SZW + 58)) >= (3 + (top_pkg_TL_SZW + 59)) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)]), sv2v_cast_3(drsp_fifo_o[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))-:((3 + (top_pkg_TL_SZW + 58)) >= (top_pkg_TL_SZW + 59) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))))) + 1)]), sv2v_cast_F00AF(drsp_fifo_o[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))-:((top_pkg_TL_SZW + 58) >= 59 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))))) - (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 2)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))) + 1)]), sv2v_cast_F1F18(hfifo_rspid), sv2v_cast_B5AB2(drsp_fifo_o[top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))-:((top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1))) - (top_pkg_TL_DW + (top_pkg_TL_DUW + 2))) + 1]), sv2v_cast_2497D(drsp_fifo_o[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1]), sv2v_cast_92577(drsp_fifo_o[top_pkg_TL_DUW + 1-:top_pkg_TL_DUW]), sv2v_cast_1(drsp_fifo_o[1]), sv2v_cast_1(hgrant[i])};
		end
	endgenerate
endmodule
module tlul_adapter_sram (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	req_o,
	gnt_i,
	we_o,
	addr_o,
	wdata_o,
	wmask_o,
	rdata_i,
	rvalid_i,
	rerror_i
);
	parameter signed [31:0] SramAw = 12;
	parameter signed [31:0] SramDw = 32;
	parameter signed [31:0] Outstanding = 1;
	parameter [0:0] ByteAccess = 1;
	parameter [0:0] ErrOnWrite = 0;
	parameter [0:0] ErrOnRead = 0;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire req_o;
	input gnt_i;
	output wire we_o;
	output wire [SramAw - 1:0] addr_o;
	output reg [SramDw - 1:0] wdata_o;
	output reg [SramDw - 1:0] wmask_o;
	input [SramDw - 1:0] rdata_i;
	input rvalid_i;
	input [1:0] rerror_i;
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	localparam signed [31:0] SramByte = SramDw / 8;
	localparam signed [31:0] DataBitWidth = $clog2(SramByte);
	localparam signed [31:0] ReqFifoWidth = (3 + top_pkg_TL_SZW) + top_pkg_TL_AIW;
	localparam signed [31:0] RspFifoWidth = (SramDw >= 0 ? SramDw + 1 : 1 - SramDw);
	wire reqfifo_wvalid;
	wire reqfifo_wready;
	wire reqfifo_rvalid;
	wire reqfifo_rready;
	wire [((3 + top_pkg_TL_SZW) + top_pkg_TL_AIW) - 1:0] reqfifo_wdata;
	wire [((3 + top_pkg_TL_SZW) + top_pkg_TL_AIW) - 1:0] reqfifo_rdata;
	wire rspfifo_wvalid;
	wire rspfifo_wready;
	wire rspfifo_rvalid;
	wire rspfifo_rready;
	wire [SramDw:0] rspfifo_wdata;
	wire [SramDw:0] rspfifo_rdata;
	wire error_internal;
	wire wr_attr_error;
	wire wr_vld_error;
	wire rd_vld_error;
	wire tlul_error;
	wire a_ack;
	wire d_ack;
	wire unused_sram_ack;
	assign a_ack = tl_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & tl_o[0];
	assign d_ack = tl_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] & tl_i[0];
	assign unused_sram_ack = req_o & gnt_i;
	reg d_valid;
	reg d_error;
	localparam [1:0] OpRead = 1;
	always @(*) begin
		d_valid = 1'b0;
		if (reqfifo_rvalid) begin
			if (reqfifo_rdata[1 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))])
				d_valid = 1'b1;
			else if (reqfifo_rdata[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))-:((3 + (top_pkg_TL_SZW + 7)) >= (1 + (top_pkg_TL_SZW + 8)) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))) - (1 + (top_pkg_TL_SZW + top_pkg_TL_AIW))) + 1 : ((1 + (top_pkg_TL_SZW + top_pkg_TL_AIW)) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1)))) + 1)] == OpRead)
				d_valid = rspfifo_rvalid;
			else
				d_valid = 1'b1;
		end
		else
			d_valid = 1'b0;
	end
	always @(*) begin
		d_error = 1'b0;
		if (reqfifo_rvalid) begin
			if (reqfifo_rdata[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))-:((3 + (top_pkg_TL_SZW + 7)) >= (1 + (top_pkg_TL_SZW + 8)) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))) - (1 + (top_pkg_TL_SZW + top_pkg_TL_AIW))) + 1 : ((1 + (top_pkg_TL_SZW + top_pkg_TL_AIW)) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1)))) + 1)] == OpRead)
				d_error = rspfifo_rdata[0] | reqfifo_rdata[1 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))];
			else
				d_error = reqfifo_rdata[1 + (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))];
		end
		else
			d_error = 1'b0;
	end
	function automatic [0:0] sv2v_cast_1;
		input reg [0:0] inp;
		sv2v_cast_1 = inp;
	endfunction
	function automatic [2:0] sv2v_cast_3;
		input reg [2:0] inp;
		sv2v_cast_3 = inp;
	endfunction
	function automatic [top_pkg_TL_SZW - 1:0] sv2v_cast_F00AF;
		input reg [top_pkg_TL_SZW - 1:0] inp;
		sv2v_cast_F00AF = inp;
	endfunction
	function automatic [top_pkg_TL_AIW - 1:0] sv2v_cast_F1F18;
		input reg [top_pkg_TL_AIW - 1:0] inp;
		sv2v_cast_F1F18 = inp;
	endfunction
	function automatic [top_pkg_TL_DIW - 1:0] sv2v_cast_B5AB2;
		input reg [top_pkg_TL_DIW - 1:0] inp;
		sv2v_cast_B5AB2 = inp;
	endfunction
	function automatic [top_pkg_TL_DW - 1:0] sv2v_cast_2497D;
		input reg [top_pkg_TL_DW - 1:0] inp;
		sv2v_cast_2497D = inp;
	endfunction
	function automatic [top_pkg_TL_DUW - 1:0] sv2v_cast_92577;
		input reg [top_pkg_TL_DUW - 1:0] inp;
		sv2v_cast_92577 = inp;
	endfunction
	assign tl_o = {sv2v_cast_1(d_valid), sv2v_cast_3((d_valid && (reqfifo_rdata[3 + (top_pkg_TL_SZW + 7)-:((3 + (top_pkg_TL_SZW + 7)) >= (1 + (top_pkg_TL_SZW + 8)) ? ((3 + (top_pkg_TL_SZW + 7)) - (1 + (top_pkg_TL_SZW + 8))) + 1 : ((1 + (top_pkg_TL_SZW + 8)) - (3 + (top_pkg_TL_SZW + 7))) + 1)] != 1) ? AccessAck : AccessAckData)), 3'b000, sv2v_cast_F00AF((d_valid ? reqfifo_rdata[top_pkg_TL_SZW + (top_pkg_TL_AIW - 1)-:((top_pkg_TL_SZW + 7) >= 8 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW - 1)) - top_pkg_TL_AIW) + 1 : (top_pkg_TL_AIW - (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))) + 1)] : {((top_pkg_TL_SZW + 7) >= 8 ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW - 1)) - top_pkg_TL_AIW) + 1 : (top_pkg_TL_AIW - (top_pkg_TL_SZW + (top_pkg_TL_AIW - 1))) + 1) {1'sb0}})), sv2v_cast_F1F18((d_valid ? reqfifo_rdata[top_pkg_TL_AIW - 1-:top_pkg_TL_AIW] : {top_pkg_TL_AIW {1'sb0}})), sv2v_cast_B5AB2(1'b0), sv2v_cast_2497D(((d_valid && rspfifo_rvalid) && (reqfifo_rdata[3 + (top_pkg_TL_SZW + 7)-:((3 + (top_pkg_TL_SZW + 7)) >= (1 + (top_pkg_TL_SZW + 8)) ? ((3 + (top_pkg_TL_SZW + 7)) - (1 + (top_pkg_TL_SZW + 8))) + 1 : ((1 + (top_pkg_TL_SZW + 8)) - (3 + (top_pkg_TL_SZW + 7))) + 1)] == 1) ? rspfifo_rdata[SramDw-:(SramDw >= 1 ? SramDw : 2 - SramDw)] : {(SramDw >= 1 ? SramDw : 2 - SramDw) {1'sb0}})), sv2v_cast_92577(1'sb0), sv2v_cast_1(d_error), sv2v_cast_1((gnt_i | error_internal) & reqfifo_wready)};
	assign req_o = (tl_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & reqfifo_wready) & ~error_internal;
	assign we_o = tl_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & sv2v_cast_1(|{tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutFullData, tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutPartialData});
	assign addr_o = (tl_i[7 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))] ? tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - ((top_pkg_TL_AW - 1) - DataBitWidth)+:SramAw] : {SramAw {1'sb0}});
	always @(*) begin : sv2v_autoblock_366
		reg signed [31:0] i;
		for (i = 0; i < (top_pkg_TL_DW / 8); i = i + 1)
			begin
				wmask_o[8 * i+:8] = (tl_i[7 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))] ? {8 {tl_i[(top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - ((top_pkg_TL_DBW - 1) - i)]}} : {8 {1'sb0}});
				wdata_o[8 * i+:8] = (tl_i[(top_pkg_TL_DBW + 48) - ((top_pkg_TL_DBW - 1) - i)] && we_o ? tl_i[(top_pkg_TL_DW + 16) - ((top_pkg_TL_DW - 1) - (8 * i))+:8] : {8 {1'sb0}});
			end
	end
	assign wr_attr_error = ((tl_i[6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) - (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))))) + 1 : ((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) - (6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))))) + 1)] == 3'h0) || (tl_i[6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) - (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))))) + 1 : ((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) - (6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))))) + 1)] == 3'h1) ? (ByteAccess == 0 ? (tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] != {top_pkg_TL_DBW {1'sb1}}) || (tl_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)] != 2'h2) : 1'b0) : 1'b0);
	generate
		if (ErrOnWrite == 1) begin : gen_no_writes
			assign wr_vld_error = tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] != Get;
		end
		else begin : gen_writes_allowed
			assign wr_vld_error = 1'b0;
		end
	endgenerate
	generate
		if (ErrOnRead == 1) begin : gen_no_reads
			assign rd_vld_error = tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == Get;
		end
		else begin : gen_reads_allowed
			assign rd_vld_error = 1'b0;
		end
	endgenerate
	tlul_err u_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.err_o(tlul_error)
	);
	assign error_internal = ((wr_attr_error | wr_vld_error) | rd_vld_error) | tlul_error;
	assign reqfifo_wvalid = a_ack;
	function automatic [1:0] sv2v_cast_2;
		input reg [1:0] inp;
		sv2v_cast_2 = inp;
	endfunction
	localparam [1:0] OpWrite = 0;
	assign reqfifo_wdata = {sv2v_cast_2((tl_i[6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) - (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))))) + 1 : ((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) - (6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))))) + 1)] != 3'h4 ? OpWrite : OpRead)), sv2v_cast_1(error_internal), sv2v_cast_F00AF(tl_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)]), sv2v_cast_F1F18(tl_i[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)])};
	assign reqfifo_rready = d_ack;
	assign rspfifo_wvalid = rvalid_i & reqfifo_rvalid;
	function automatic [SramDw - 1:0] sv2v_cast_D11AA;
		input reg [SramDw - 1:0] inp;
		sv2v_cast_D11AA = inp;
	endfunction
	assign rspfifo_wdata = {sv2v_cast_D11AA(rdata_i), sv2v_cast_1(rerror_i[1])};
	assign rspfifo_rready = ((reqfifo_rdata[3 + (top_pkg_TL_SZW + 7)-:((3 + (top_pkg_TL_SZW + 7)) >= (1 + (top_pkg_TL_SZW + 8)) ? ((3 + (top_pkg_TL_SZW + 7)) - (1 + (top_pkg_TL_SZW + 8))) + 1 : ((1 + (top_pkg_TL_SZW + 8)) - (3 + (top_pkg_TL_SZW + 7))) + 1)] == 1) & ~reqfifo_rdata[1 + (top_pkg_TL_SZW + 7)] ? reqfifo_rready : 1'b0);
	prim_fifo_sync #(
		.Width(ReqFifoWidth),
		.Pass(1'b0),
		.Depth(Outstanding + 1'b1)
	) u_reqfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(reqfifo_wvalid),
		.wready(reqfifo_wready),
		.wdata(reqfifo_wdata),
		.depth(),
		.rvalid(reqfifo_rvalid),
		.rready(reqfifo_rready),
		.rdata(reqfifo_rdata)
	);
	prim_fifo_sync #(
		.Width(RspFifoWidth),
		.Pass(1'b1),
		.Depth(Outstanding)
	) u_rspfifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(rspfifo_wvalid),
		.wready(rspfifo_wready),
		.wdata(rspfifo_wdata),
		.depth(),
		.rvalid(rspfifo_rvalid),
		.rready(rspfifo_rready),
		.rdata(rspfifo_rdata)
	);
endmodule
module tlul_adapter_reg (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	re_o,
	we_o,
	addr_o,
	wdata_o,
	be_o,
	rdata_i,
	error_i
);
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	parameter signed [31:0] RegAw = 8;
	parameter signed [31:0] RegDw = 32;
	localparam signed [31:0] RegBw = RegDw / 8;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire re_o;
	output wire we_o;
	output wire [RegAw - 1:0] addr_o;
	output wire [RegDw - 1:0] wdata_o;
	output wire [RegBw - 1:0] be_o;
	input [RegDw - 1:0] rdata_i;
	input error_i;
	localparam signed [31:0] IW = top_pkg_TL_AIW;
	localparam signed [31:0] SZW = top_pkg_TL_SZW;
	reg outstanding;
	wire a_ack;
	wire d_ack;
	reg [RegDw - 1:0] rdata;
	reg error;
	wire err_internal;
	reg addr_align_err;
	wire malformed_meta_err;
	wire tl_err;
	reg [IW - 1:0] reqid;
	reg [SZW - 1:0] reqsz;
	reg [2:0] rspop;
	wire rd_req;
	wire wr_req;
	assign a_ack = tl_i[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] & tl_o[0];
	assign d_ack = tl_o[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_DIW + (top_pkg_TL_DW + (top_pkg_TL_DUW + 1)))))] & tl_i[0];
	assign wr_req = a_ack & ((tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutFullData) | (tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == PutPartialData));
	assign rd_req = a_ack & (tl_i[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] == Get);
	assign we_o = wr_req & ~err_internal;
	assign re_o = rd_req & ~err_internal;
	assign addr_o = {tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - ((top_pkg_TL_AW - 1) - (RegAw - 1)):(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 3)], 2'b00};
	assign wdata_o = tl_i[top_pkg_TL_DW + 16-:top_pkg_TL_DW];
	assign be_o = tl_i[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)];
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			outstanding <= 1'b0;
		else if (a_ack)
			outstanding <= 1'b1;
		else if (d_ack)
			outstanding <= 1'b0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			reqid <= {IW {1'sb0}};
			reqsz <= {SZW {1'sb0}};
			rspop <= AccessAck;
		end
		else if (a_ack) begin
			reqid <= tl_i[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)];
			reqsz <= tl_i[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)];
			rspop <= (rd_req ? AccessAckData : AccessAck);
		end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni) begin
			rdata <= {RegDw {1'sb0}};
			error <= 1'b0;
		end
		else if (a_ack) begin
			rdata <= (err_internal ? {RegDw {1'sb1}} : rdata_i);
			error <= error_i | err_internal;
		end
	function automatic [0:0] sv2v_cast_1;
		input reg [0:0] inp;
		sv2v_cast_1 = inp;
	endfunction
	function automatic [2:0] sv2v_cast_3;
		input reg [2:0] inp;
		sv2v_cast_3 = inp;
	endfunction
	function automatic [top_pkg_TL_SZW - 1:0] sv2v_cast_F00AF;
		input reg [top_pkg_TL_SZW - 1:0] inp;
		sv2v_cast_F00AF = inp;
	endfunction
	function automatic [top_pkg_TL_AIW - 1:0] sv2v_cast_F1F18;
		input reg [top_pkg_TL_AIW - 1:0] inp;
		sv2v_cast_F1F18 = inp;
	endfunction
	function automatic [top_pkg_TL_DIW - 1:0] sv2v_cast_B5AB2;
		input reg [top_pkg_TL_DIW - 1:0] inp;
		sv2v_cast_B5AB2 = inp;
	endfunction
	function automatic [top_pkg_TL_DW - 1:0] sv2v_cast_2497D;
		input reg [top_pkg_TL_DW - 1:0] inp;
		sv2v_cast_2497D = inp;
	endfunction
	function automatic [top_pkg_TL_DUW - 1:0] sv2v_cast_92577;
		input reg [top_pkg_TL_DUW - 1:0] inp;
		sv2v_cast_92577 = inp;
	endfunction
	assign tl_o = {sv2v_cast_1(outstanding), sv2v_cast_3(rspop), 3'b000, sv2v_cast_F00AF(reqsz), sv2v_cast_F1F18(reqid), sv2v_cast_B5AB2(1'sb0), sv2v_cast_2497D(rdata), sv2v_cast_92577(1'sb0), sv2v_cast_1(error), sv2v_cast_1(~outstanding)};
	assign err_internal = (addr_align_err | malformed_meta_err) | tl_err;
	assign malformed_meta_err = tl_i[9] == 1'b1;
	always @(*)
		if (wr_req)
			addr_align_err = |tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 2):(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 1)];
		else
			addr_align_err = 1'b0;
	tlul_err u_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.err_o(tl_err)
	);
endmodule
module gpio (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	cio_gpio_i,
	cio_gpio_o,
	cio_gpio_en_o,
	intr_gpio_o
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	input [31:0] cio_gpio_i;
	output wire [31:0] cio_gpio_o;
	output wire [31:0] cio_gpio_en_o;
	output wire [31:0] intr_gpio_o;
	localparam [5:0] GPIO_INTR_STATE_OFFSET = 6'h00;
	localparam [5:0] GPIO_INTR_ENABLE_OFFSET = 6'h04;
	localparam [5:0] GPIO_INTR_TEST_OFFSET = 6'h08;
	localparam [5:0] GPIO_DATA_IN_OFFSET = 6'h0c;
	localparam [5:0] GPIO_DIRECT_OUT_OFFSET = 6'h10;
	localparam [5:0] GPIO_MASKED_OUT_LOWER_OFFSET = 6'h14;
	localparam [5:0] GPIO_MASKED_OUT_UPPER_OFFSET = 6'h18;
	localparam [5:0] GPIO_DIRECT_OE_OFFSET = 6'h1c;
	localparam [5:0] GPIO_MASKED_OE_LOWER_OFFSET = 6'h20;
	localparam [5:0] GPIO_MASKED_OE_UPPER_OFFSET = 6'h24;
	localparam [5:0] GPIO_INTR_CTRL_EN_RISING_OFFSET = 6'h28;
	localparam [5:0] GPIO_INTR_CTRL_EN_FALLING_OFFSET = 6'h2c;
	localparam [5:0] GPIO_INTR_CTRL_EN_LVLHIGH_OFFSET = 6'h30;
	localparam [5:0] GPIO_INTR_CTRL_EN_LVLLOW_OFFSET = 6'h34;
	localparam [5:0] GPIO_CTRL_EN_INPUT_FILTER_OFFSET = 6'h38;
	localparam signed [31:0] GPIO_INTR_STATE = 0;
	localparam signed [31:0] GPIO_INTR_ENABLE = 1;
	localparam signed [31:0] GPIO_INTR_TEST = 2;
	localparam signed [31:0] GPIO_DATA_IN = 3;
	localparam signed [31:0] GPIO_DIRECT_OUT = 4;
	localparam signed [31:0] GPIO_MASKED_OUT_LOWER = 5;
	localparam signed [31:0] GPIO_MASKED_OUT_UPPER = 6;
	localparam signed [31:0] GPIO_DIRECT_OE = 7;
	localparam signed [31:0] GPIO_MASKED_OE_LOWER = 8;
	localparam signed [31:0] GPIO_MASKED_OE_UPPER = 9;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_RISING = 10;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_FALLING = 11;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_LVLHIGH = 12;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_LVLLOW = 13;
	localparam signed [31:0] GPIO_CTRL_EN_INPUT_FILTER = 14;
	localparam [59:0] GPIO_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111};
	wire [458:0] reg2hw;
	wire [257:0] hw2reg;
	reg [31:0] cio_gpio_q;
	reg [31:0] cio_gpio_en_q;
	wire [31:0] data_in_d;
	generate
		genvar i;
		for (i = 0; i < 32; i = i + 1) begin : gen_filter
			prim_filter_ctr #(.Cycles(16)) filter(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.enable_i(reg2hw[i]),
				.filter_i(cio_gpio_i[i]),
				.filter_o(data_in_d[i])
			);
		end
	endgenerate
	assign hw2reg[192] = 1'b1;
	assign hw2reg[224-:32] = data_in_d;
	assign cio_gpio_o = cio_gpio_q;
	assign cio_gpio_en_o = cio_gpio_en_q;
	assign hw2reg[191-:32] = cio_gpio_q;
	assign hw2reg[127-:16] = cio_gpio_q[31:16];
	assign hw2reg[111-:16] = 16'h0000;
	assign hw2reg[159-:16] = cio_gpio_q[15:0];
	assign hw2reg[143-:16] = 16'h0000;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			cio_gpio_q <= {32 {1'sb0}};
		else if (reg2hw[329])
			cio_gpio_q <= reg2hw[361-:32];
		else if (reg2hw[278])
			cio_gpio_q[31:16] <= (reg2hw[277-:16] & reg2hw[294-:16]) | (~reg2hw[277-:16] & cio_gpio_q[31:16]);
		else if (reg2hw[312])
			cio_gpio_q[15:0] <= (reg2hw[311-:16] & reg2hw[328-:16]) | (~reg2hw[311-:16] & cio_gpio_q[15:0]);
	assign hw2reg[95-:32] = cio_gpio_en_q;
	assign hw2reg[31-:16] = cio_gpio_en_q[31:16];
	assign hw2reg[15-:16] = 16'h0000;
	assign hw2reg[63-:16] = cio_gpio_en_q[15:0];
	assign hw2reg[47-:16] = 16'h0000;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			cio_gpio_en_q <= {32 {1'sb0}};
		else if (reg2hw[228])
			cio_gpio_en_q <= reg2hw[260-:32];
		else if (reg2hw[177])
			cio_gpio_en_q[31:16] <= (reg2hw[176-:16] & reg2hw[193-:16]) | (~reg2hw[176-:16] & cio_gpio_en_q[31:16]);
		else if (reg2hw[211])
			cio_gpio_en_q[15:0] <= (reg2hw[210-:16] & reg2hw[227-:16]) | (~reg2hw[210-:16] & cio_gpio_en_q[15:0]);
	reg [31:0] data_in_q;
	always @(posedge clk_i) data_in_q <= data_in_d;
	wire [31:0] event_intr_rise;
	wire [31:0] event_intr_fall;
	wire [31:0] event_intr_actlow;
	wire [31:0] event_intr_acthigh;
	wire [31:0] event_intr_combined;
	prim_intr_hw #(.Width(32)) intr_hw(
		.event_intr_i(event_intr_combined),
		.reg2hw_intr_enable_q_i(reg2hw[426-:32]),
		.reg2hw_intr_test_q_i(reg2hw[394-:32]),
		.reg2hw_intr_test_qe_i(reg2hw[362]),
		.reg2hw_intr_state_q_i(reg2hw[458-:32]),
		.hw2reg_intr_state_de_o(hw2reg[225]),
		.hw2reg_intr_state_d_o(hw2reg[257-:32]),
		.intr_o(intr_gpio_o)
	);
	assign event_intr_rise = (~data_in_q & data_in_d) & reg2hw[159-:32];
	assign event_intr_fall = (data_in_q & ~data_in_d) & reg2hw[127-:32];
	assign event_intr_acthigh = data_in_d & reg2hw[95-:32];
	assign event_intr_actlow = ~data_in_d & reg2hw[63-:32];
	assign event_intr_combined = ((event_intr_rise | event_intr_fall) | event_intr_actlow) | event_intr_acthigh;
	gpio_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
endmodule
module gpio_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [458:0] reg2hw;
	input wire [257:0] hw2reg;
	input devmode_i;
	localparam [5:0] GPIO_INTR_STATE_OFFSET = 6'h00;
	localparam [5:0] GPIO_INTR_ENABLE_OFFSET = 6'h04;
	localparam [5:0] GPIO_INTR_TEST_OFFSET = 6'h08;
	localparam [5:0] GPIO_DATA_IN_OFFSET = 6'h0c;
	localparam [5:0] GPIO_DIRECT_OUT_OFFSET = 6'h10;
	localparam [5:0] GPIO_MASKED_OUT_LOWER_OFFSET = 6'h14;
	localparam [5:0] GPIO_MASKED_OUT_UPPER_OFFSET = 6'h18;
	localparam [5:0] GPIO_DIRECT_OE_OFFSET = 6'h1c;
	localparam [5:0] GPIO_MASKED_OE_LOWER_OFFSET = 6'h20;
	localparam [5:0] GPIO_MASKED_OE_UPPER_OFFSET = 6'h24;
	localparam [5:0] GPIO_INTR_CTRL_EN_RISING_OFFSET = 6'h28;
	localparam [5:0] GPIO_INTR_CTRL_EN_FALLING_OFFSET = 6'h2c;
	localparam [5:0] GPIO_INTR_CTRL_EN_LVLHIGH_OFFSET = 6'h30;
	localparam [5:0] GPIO_INTR_CTRL_EN_LVLLOW_OFFSET = 6'h34;
	localparam [5:0] GPIO_CTRL_EN_INPUT_FILTER_OFFSET = 6'h38;
	localparam signed [31:0] GPIO_INTR_STATE = 0;
	localparam signed [31:0] GPIO_INTR_ENABLE = 1;
	localparam signed [31:0] GPIO_INTR_TEST = 2;
	localparam signed [31:0] GPIO_DATA_IN = 3;
	localparam signed [31:0] GPIO_DIRECT_OUT = 4;
	localparam signed [31:0] GPIO_MASKED_OUT_LOWER = 5;
	localparam signed [31:0] GPIO_MASKED_OUT_UPPER = 6;
	localparam signed [31:0] GPIO_DIRECT_OE = 7;
	localparam signed [31:0] GPIO_MASKED_OE_LOWER = 8;
	localparam signed [31:0] GPIO_MASKED_OE_UPPER = 9;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_RISING = 10;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_FALLING = 11;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_LVLHIGH = 12;
	localparam signed [31:0] GPIO_INTR_CTRL_EN_LVLLOW = 13;
	localparam signed [31:0] GPIO_CTRL_EN_INPUT_FILTER = 14;
	localparam [59:0] GPIO_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111};
	localparam signed [31:0] AW = 6;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire [31:0] intr_state_qs;
	wire [31:0] intr_state_wd;
	wire intr_state_we;
	wire [31:0] intr_enable_qs;
	wire [31:0] intr_enable_wd;
	wire intr_enable_we;
	wire [31:0] intr_test_wd;
	wire intr_test_we;
	wire [31:0] data_in_qs;
	wire [31:0] direct_out_qs;
	wire [31:0] direct_out_wd;
	wire direct_out_we;
	wire direct_out_re;
	wire [15:0] masked_out_lower_data_qs;
	wire [15:0] masked_out_lower_data_wd;
	wire masked_out_lower_data_we;
	wire masked_out_lower_data_re;
	wire [15:0] masked_out_lower_mask_wd;
	wire masked_out_lower_mask_we;
	wire [15:0] masked_out_upper_data_qs;
	wire [15:0] masked_out_upper_data_wd;
	wire masked_out_upper_data_we;
	wire masked_out_upper_data_re;
	wire [15:0] masked_out_upper_mask_wd;
	wire masked_out_upper_mask_we;
	wire [31:0] direct_oe_qs;
	wire [31:0] direct_oe_wd;
	wire direct_oe_we;
	wire direct_oe_re;
	wire [15:0] masked_oe_lower_data_qs;
	wire [15:0] masked_oe_lower_data_wd;
	wire masked_oe_lower_data_we;
	wire masked_oe_lower_data_re;
	wire [15:0] masked_oe_lower_mask_qs;
	wire [15:0] masked_oe_lower_mask_wd;
	wire masked_oe_lower_mask_we;
	wire masked_oe_lower_mask_re;
	wire [15:0] masked_oe_upper_data_qs;
	wire [15:0] masked_oe_upper_data_wd;
	wire masked_oe_upper_data_we;
	wire masked_oe_upper_data_re;
	wire [15:0] masked_oe_upper_mask_qs;
	wire [15:0] masked_oe_upper_mask_wd;
	wire masked_oe_upper_mask_we;
	wire masked_oe_upper_mask_re;
	wire [31:0] intr_ctrl_en_rising_qs;
	wire [31:0] intr_ctrl_en_rising_wd;
	wire intr_ctrl_en_rising_we;
	wire [31:0] intr_ctrl_en_falling_qs;
	wire [31:0] intr_ctrl_en_falling_wd;
	wire intr_ctrl_en_falling_we;
	wire [31:0] intr_ctrl_en_lvlhigh_qs;
	wire [31:0] intr_ctrl_en_lvlhigh_wd;
	wire intr_ctrl_en_lvlhigh_we;
	wire [31:0] intr_ctrl_en_lvllow_qs;
	wire [31:0] intr_ctrl_en_lvllow_wd;
	wire intr_ctrl_en_lvllow_we;
	wire [31:0] ctrl_en_input_filter_qs;
	wire [31:0] ctrl_en_input_filter_wd;
	wire ctrl_en_input_filter_we;
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(32'h00000000)
	) u_intr_state(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_we),
		.wd(intr_state_wd),
		.de(hw2reg[225]),
		.d(hw2reg[257-:32]),
		.qe(),
		.q(reg2hw[458-:32]),
		.qs(intr_state_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_intr_enable(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_we),
		.wd(intr_enable_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[426-:32]),
		.qs(intr_enable_qs)
	);
	prim_subreg_ext #(.DW(32)) u_intr_test(
		.re(1'b0),
		.we(intr_test_we),
		.wd(intr_test_wd),
		.d({32 {1'sb0}}),
		.qre(),
		.qe(reg2hw[362]),
		.q(reg2hw[394-:32]),
		.qs()
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(32'h00000000)
	) u_data_in(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.de(hw2reg[192]),
		.d(hw2reg[224-:32]),
		.qe(),
		.q(),
		.qs(data_in_qs)
	);
	prim_subreg_ext #(.DW(32)) u_direct_out(
		.re(direct_out_re),
		.we(direct_out_we),
		.wd(direct_out_wd),
		.d(hw2reg[191-:32]),
		.qre(),
		.qe(reg2hw[329]),
		.q(reg2hw[361-:32]),
		.qs(direct_out_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_out_lower_data(
		.re(masked_out_lower_data_re),
		.we(masked_out_lower_data_we),
		.wd(masked_out_lower_data_wd),
		.d(hw2reg[159-:16]),
		.qre(),
		.qe(reg2hw[312]),
		.q(reg2hw[328-:16]),
		.qs(masked_out_lower_data_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_out_lower_mask(
		.re(1'b0),
		.we(masked_out_lower_mask_we),
		.wd(masked_out_lower_mask_wd),
		.d(hw2reg[143-:16]),
		.qre(),
		.qe(reg2hw[295]),
		.q(reg2hw[311-:16]),
		.qs()
	);
	prim_subreg_ext #(.DW(16)) u_masked_out_upper_data(
		.re(masked_out_upper_data_re),
		.we(masked_out_upper_data_we),
		.wd(masked_out_upper_data_wd),
		.d(hw2reg[127-:16]),
		.qre(),
		.qe(reg2hw[278]),
		.q(reg2hw[294-:16]),
		.qs(masked_out_upper_data_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_out_upper_mask(
		.re(1'b0),
		.we(masked_out_upper_mask_we),
		.wd(masked_out_upper_mask_wd),
		.d(hw2reg[111-:16]),
		.qre(),
		.qe(reg2hw[261]),
		.q(reg2hw[277-:16]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_direct_oe(
		.re(direct_oe_re),
		.we(direct_oe_we),
		.wd(direct_oe_wd),
		.d(hw2reg[95-:32]),
		.qre(),
		.qe(reg2hw[228]),
		.q(reg2hw[260-:32]),
		.qs(direct_oe_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_oe_lower_data(
		.re(masked_oe_lower_data_re),
		.we(masked_oe_lower_data_we),
		.wd(masked_oe_lower_data_wd),
		.d(hw2reg[63-:16]),
		.qre(),
		.qe(reg2hw[211]),
		.q(reg2hw[227-:16]),
		.qs(masked_oe_lower_data_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_oe_lower_mask(
		.re(masked_oe_lower_mask_re),
		.we(masked_oe_lower_mask_we),
		.wd(masked_oe_lower_mask_wd),
		.d(hw2reg[47-:16]),
		.qre(),
		.qe(reg2hw[194]),
		.q(reg2hw[210-:16]),
		.qs(masked_oe_lower_mask_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_oe_upper_data(
		.re(masked_oe_upper_data_re),
		.we(masked_oe_upper_data_we),
		.wd(masked_oe_upper_data_wd),
		.d(hw2reg[31-:16]),
		.qre(),
		.qe(reg2hw[177]),
		.q(reg2hw[193-:16]),
		.qs(masked_oe_upper_data_qs)
	);
	prim_subreg_ext #(.DW(16)) u_masked_oe_upper_mask(
		.re(masked_oe_upper_mask_re),
		.we(masked_oe_upper_mask_we),
		.wd(masked_oe_upper_mask_wd),
		.d(hw2reg[15-:16]),
		.qre(),
		.qe(reg2hw[160]),
		.q(reg2hw[176-:16]),
		.qs(masked_oe_upper_mask_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_intr_ctrl_en_rising(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_ctrl_en_rising_we),
		.wd(intr_ctrl_en_rising_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[159-:32]),
		.qs(intr_ctrl_en_rising_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_intr_ctrl_en_falling(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_ctrl_en_falling_we),
		.wd(intr_ctrl_en_falling_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[127-:32]),
		.qs(intr_ctrl_en_falling_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_intr_ctrl_en_lvlhigh(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_ctrl_en_lvlhigh_we),
		.wd(intr_ctrl_en_lvlhigh_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[95-:32]),
		.qs(intr_ctrl_en_lvlhigh_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_intr_ctrl_en_lvllow(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_ctrl_en_lvllow_we),
		.wd(intr_ctrl_en_lvllow_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[63-:32]),
		.qs(intr_ctrl_en_lvllow_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_ctrl_en_input_filter(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_en_input_filter_we),
		.wd(ctrl_en_input_filter_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[31-:32]),
		.qs(ctrl_en_input_filter_qs)
	);
	reg [14:0] addr_hit;
	always @(*) begin
		addr_hit = {15 {1'sb0}};
		addr_hit[0] = reg_addr == GPIO_INTR_STATE_OFFSET;
		addr_hit[1] = reg_addr == GPIO_INTR_ENABLE_OFFSET;
		addr_hit[2] = reg_addr == GPIO_INTR_TEST_OFFSET;
		addr_hit[3] = reg_addr == GPIO_DATA_IN_OFFSET;
		addr_hit[4] = reg_addr == GPIO_DIRECT_OUT_OFFSET;
		addr_hit[5] = reg_addr == GPIO_MASKED_OUT_LOWER_OFFSET;
		addr_hit[6] = reg_addr == GPIO_MASKED_OUT_UPPER_OFFSET;
		addr_hit[7] = reg_addr == GPIO_DIRECT_OE_OFFSET;
		addr_hit[8] = reg_addr == GPIO_MASKED_OE_LOWER_OFFSET;
		addr_hit[9] = reg_addr == GPIO_MASKED_OE_UPPER_OFFSET;
		addr_hit[10] = reg_addr == GPIO_INTR_CTRL_EN_RISING_OFFSET;
		addr_hit[11] = reg_addr == GPIO_INTR_CTRL_EN_FALLING_OFFSET;
		addr_hit[12] = reg_addr == GPIO_INTR_CTRL_EN_LVLHIGH_OFFSET;
		addr_hit[13] = reg_addr == GPIO_INTR_CTRL_EN_LVLLOW_OFFSET;
		addr_hit[14] = reg_addr == GPIO_CTRL_EN_INPUT_FILTER_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (GPIO_PERMIT[56+:4] != (GPIO_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (GPIO_PERMIT[52+:4] != (GPIO_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (GPIO_PERMIT[48+:4] != (GPIO_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (GPIO_PERMIT[44+:4] != (GPIO_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (GPIO_PERMIT[40+:4] != (GPIO_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (GPIO_PERMIT[36+:4] != (GPIO_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (GPIO_PERMIT[32+:4] != (GPIO_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (GPIO_PERMIT[28+:4] != (GPIO_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (GPIO_PERMIT[24+:4] != (GPIO_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (GPIO_PERMIT[20+:4] != (GPIO_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (GPIO_PERMIT[16+:4] != (GPIO_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (GPIO_PERMIT[12+:4] != (GPIO_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (GPIO_PERMIT[8+:4] != (GPIO_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (GPIO_PERMIT[4+:4] != (GPIO_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (GPIO_PERMIT[0+:4] != (GPIO_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign intr_state_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_wd = reg_wdata[31:0];
	assign intr_enable_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_wd = reg_wdata[31:0];
	assign intr_test_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_wd = reg_wdata[31:0];
	assign direct_out_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign direct_out_wd = reg_wdata[31:0];
	assign direct_out_re = addr_hit[4] && reg_re;
	assign masked_out_lower_data_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign masked_out_lower_data_wd = reg_wdata[15:0];
	assign masked_out_lower_data_re = addr_hit[5] && reg_re;
	assign masked_out_lower_mask_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign masked_out_lower_mask_wd = reg_wdata[31:16];
	assign masked_out_upper_data_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign masked_out_upper_data_wd = reg_wdata[15:0];
	assign masked_out_upper_data_re = addr_hit[6] && reg_re;
	assign masked_out_upper_mask_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign masked_out_upper_mask_wd = reg_wdata[31:16];
	assign direct_oe_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign direct_oe_wd = reg_wdata[31:0];
	assign direct_oe_re = addr_hit[7] && reg_re;
	assign masked_oe_lower_data_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign masked_oe_lower_data_wd = reg_wdata[15:0];
	assign masked_oe_lower_data_re = addr_hit[8] && reg_re;
	assign masked_oe_lower_mask_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign masked_oe_lower_mask_wd = reg_wdata[31:16];
	assign masked_oe_lower_mask_re = addr_hit[8] && reg_re;
	assign masked_oe_upper_data_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign masked_oe_upper_data_wd = reg_wdata[15:0];
	assign masked_oe_upper_data_re = addr_hit[9] && reg_re;
	assign masked_oe_upper_mask_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign masked_oe_upper_mask_wd = reg_wdata[31:16];
	assign masked_oe_upper_mask_re = addr_hit[9] && reg_re;
	assign intr_ctrl_en_rising_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign intr_ctrl_en_rising_wd = reg_wdata[31:0];
	assign intr_ctrl_en_falling_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign intr_ctrl_en_falling_wd = reg_wdata[31:0];
	assign intr_ctrl_en_lvlhigh_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign intr_ctrl_en_lvlhigh_wd = reg_wdata[31:0];
	assign intr_ctrl_en_lvllow_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign intr_ctrl_en_lvllow_wd = reg_wdata[31:0];
	assign ctrl_en_input_filter_we = (addr_hit[14] & reg_we) & ~wr_err;
	assign ctrl_en_input_filter_wd = reg_wdata[31:0];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: reg_rdata_next[31:0] = intr_state_qs;
			addr_hit[1]: reg_rdata_next[31:0] = intr_enable_qs;
			addr_hit[2]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[3]: reg_rdata_next[31:0] = data_in_qs;
			addr_hit[4]: reg_rdata_next[31:0] = direct_out_qs;
			addr_hit[5]: begin
				reg_rdata_next[15:0] = masked_out_lower_data_qs;
				reg_rdata_next[31:16] = {16 {1'sb0}};
			end
			addr_hit[6]: begin
				reg_rdata_next[15:0] = masked_out_upper_data_qs;
				reg_rdata_next[31:16] = {16 {1'sb0}};
			end
			addr_hit[7]: reg_rdata_next[31:0] = direct_oe_qs;
			addr_hit[8]: begin
				reg_rdata_next[15:0] = masked_oe_lower_data_qs;
				reg_rdata_next[31:16] = masked_oe_lower_mask_qs;
			end
			addr_hit[9]: begin
				reg_rdata_next[15:0] = masked_oe_upper_data_qs;
				reg_rdata_next[31:16] = masked_oe_upper_mask_qs;
			end
			addr_hit[10]: reg_rdata_next[31:0] = intr_ctrl_en_rising_qs;
			addr_hit[11]: reg_rdata_next[31:0] = intr_ctrl_en_falling_qs;
			addr_hit[12]: reg_rdata_next[31:0] = intr_ctrl_en_lvlhigh_qs;
			addr_hit[13]: reg_rdata_next[31:0] = intr_ctrl_en_lvllow_qs;
			addr_hit[14]: reg_rdata_next[31:0] = ctrl_en_input_filter_qs;
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module prim_clock_gating (
	clk_i,
	en_i,
	test_en_i,
	clk_o
);
	input clk_i;
	input en_i;
	input test_en_i;
	output wire clk_o;
	assign clk_o = clk_i & (en_i | test_en_i);
endmodule
module rv_plic_gateway (
	clk_i,
	rst_ni,
	src,
	le,
	claim,
	complete,
	ip
);
	parameter signed [31:0] N_SOURCE = 32;
	input clk_i;
	input rst_ni;
	input [N_SOURCE - 1:0] src;
	input [N_SOURCE - 1:0] le;
	input [N_SOURCE - 1:0] claim;
	input [N_SOURCE - 1:0] complete;
	output reg [N_SOURCE - 1:0] ip;
	reg [N_SOURCE - 1:0] ia;
	reg [N_SOURCE - 1:0] set;
	reg [N_SOURCE - 1:0] src_d;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			src_d <= {N_SOURCE {1'sb0}};
		else
			src_d <= src;
	always @(*) begin : sv2v_autoblock_387
		reg signed [31:0] i;
		for (i = 0; i < N_SOURCE; i = i + 1)
			set[i] = (le[i] ? src[i] & ~src_d[i] : src[i]);
	end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			ip <= {N_SOURCE {1'sb0}};
		else
			ip <= (ip | ((set & ~ia) & ~ip)) & ~(ip & claim);
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			ia <= {N_SOURCE {1'sb0}};
		else
			ia <= (ia | (set & ~ia)) & ~((ia & complete) & ~ip);
endmodule
module rv_plic_target (
	clk_i,
	rst_ni,
	ip,
	ie,
	prio,
	threshold,
	irq,
	irq_id
);
	parameter signed [31:0] N_SOURCE = 32;
	parameter signed [31:0] MAX_PRIO = 7;
	localparam [31:0] SRCW = $clog2(N_SOURCE + 1);
	localparam [31:0] PRIOW = $clog2(MAX_PRIO + 1);
	input clk_i;
	input rst_ni;
	input [N_SOURCE - 1:0] ip;
	input [N_SOURCE - 1:0] ie;
	input [(0 >= (N_SOURCE - 1) ? ((2 - N_SOURCE) * PRIOW) + (((N_SOURCE - 1) * PRIOW) - 1) : (N_SOURCE * PRIOW) - 1):(0 >= (N_SOURCE - 1) ? (N_SOURCE - 1) * PRIOW : 0)] prio;
	input [PRIOW - 1:0] threshold;
	output wire irq;
	output wire [SRCW - 1:0] irq_id;
	localparam [31:0] N_LEVELS = $clog2(N_SOURCE);
	wire [(2 ** (N_LEVELS + 1)) - 2:0] is_tree;
	wire [(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? (((2 ** (N_LEVELS + 1)) - 1) * SRCW) - 1 : ((3 - (2 ** (N_LEVELS + 1))) * SRCW) + ((((2 ** (N_LEVELS + 1)) - 2) * SRCW) - 1)):(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? 0 : ((2 ** (N_LEVELS + 1)) - 2) * SRCW)] id_tree;
	wire [(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? (((2 ** (N_LEVELS + 1)) - 1) * PRIOW) - 1 : ((3 - (2 ** (N_LEVELS + 1))) * PRIOW) + ((((2 ** (N_LEVELS + 1)) - 2) * PRIOW) - 1)):(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? 0 : ((2 ** (N_LEVELS + 1)) - 2) * PRIOW)] max_tree;
	generate
		genvar level;
		for (level = 0; level < (N_LEVELS + 1); level = level + 1) begin : gen_tree
			localparam [31:0] base0 = (2 ** level) - 1;
			localparam [31:0] base1 = (2 ** (level + 1)) - 1;
			genvar offset;
			for (offset = 0; offset < (2 ** level); offset = offset + 1) begin : gen_level
				localparam [31:0] pa = base0 + offset;
				localparam [31:0] c0 = base1 + (2 * offset);
				localparam [31:0] c1 = (base1 + (2 * offset)) + 1;
				if (level == N_LEVELS) begin : gen_leafs
					if (offset < N_SOURCE) begin : gen_assign
						assign is_tree[pa] = ip[offset] & ie[offset];
						assign id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * SRCW+:SRCW] = offset + 1'b1;
						assign max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * PRIOW+:PRIOW] = prio[(0 >= (N_SOURCE - 1) ? offset : (N_SOURCE - 1) - offset) * PRIOW+:PRIOW];
					end
					else begin : gen_tie_off
						assign is_tree[pa] = 1'sb0;
						assign id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * SRCW+:SRCW] = {SRCW {1'sb0}};
						assign max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * PRIOW+:PRIOW] = {PRIOW {1'sb0}};
					end
				end
				else begin : gen_nodes
					wire sel;
					function automatic [0:0] sv2v_cast_1;
						input reg [0:0] inp;
						sv2v_cast_1 = inp;
					endfunction
					assign sel = (~is_tree[c0] & is_tree[c1]) | ((is_tree[c0] & is_tree[c1]) & sv2v_cast_1(max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c1 : ((2 ** (N_LEVELS + 1)) - 2) - c1) * PRIOW+:PRIOW] > max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c0 : ((2 ** (N_LEVELS + 1)) - 2) - c0) * PRIOW+:PRIOW]));
					assign is_tree[pa] = (sel & is_tree[c1]) | (~sel & is_tree[c0]);
					assign id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * SRCW+:SRCW] = ({SRCW {sel}} & id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c1 : ((2 ** (N_LEVELS + 1)) - 2) - c1) * SRCW+:SRCW]) | ({SRCW {~sel}} & id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c0 : ((2 ** (N_LEVELS + 1)) - 2) - c0) * SRCW+:SRCW]);
					assign max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? pa : ((2 ** (N_LEVELS + 1)) - 2) - pa) * PRIOW+:PRIOW] = ({PRIOW {sel}} & max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c1 : ((2 ** (N_LEVELS + 1)) - 2) - c1) * PRIOW+:PRIOW]) | ({PRIOW {~sel}} & max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? c0 : ((2 ** (N_LEVELS + 1)) - 2) - c0) * PRIOW+:PRIOW]);
				end
			end
		end
	endgenerate
	wire irq_d;
	reg irq_q;
	wire [SRCW - 1:0] irq_id_d;
	reg [SRCW - 1:0] irq_id_q;
	assign irq_d = (max_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? 0 : (2 ** (N_LEVELS + 1)) - 2) * PRIOW+:PRIOW] > threshold ? is_tree[0] : 1'b0);
	assign irq_id_d = (is_tree[0] ? id_tree[(((2 ** (N_LEVELS + 1)) - 2) >= 0 ? 0 : (2 ** (N_LEVELS + 1)) - 2) * SRCW+:SRCW] : {SRCW {1'sb0}});
	always @(posedge clk_i or negedge rst_ni) begin : gen_regs
		if (!rst_ni) begin
			irq_q <= 1'b0;
			irq_id_q <= {SRCW {1'sb0}};
		end
		else begin
			irq_q <= irq_d;
			irq_id_q <= irq_id_d;
		end
	end
	assign irq = irq_q;
	assign irq_id = irq_id_q;
endmodule
module rv_timer_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [152:0] reg2hw;
	input wire [67:0] hw2reg;
	input devmode_i;
	localparam signed [31:0] N_HARTS = 1;
	localparam signed [31:0] N_TIMERS = 1;
	localparam [8:0] RV_TIMER_CTRL_OFFSET = 9'h000;
	localparam [8:0] RV_TIMER_CFG0_OFFSET = 9'h100;
	localparam [8:0] RV_TIMER_TIMER_V_LOWER0_OFFSET = 9'h104;
	localparam [8:0] RV_TIMER_TIMER_V_UPPER0_OFFSET = 9'h108;
	localparam [8:0] RV_TIMER_COMPARE_LOWER0_0_OFFSET = 9'h10c;
	localparam [8:0] RV_TIMER_COMPARE_UPPER0_0_OFFSET = 9'h110;
	localparam [8:0] RV_TIMER_INTR_ENABLE0_OFFSET = 9'h114;
	localparam [8:0] RV_TIMER_INTR_STATE0_OFFSET = 9'h118;
	localparam [8:0] RV_TIMER_INTR_TEST0_OFFSET = 9'h11c;
	localparam signed [31:0] RV_TIMER_CTRL = 0;
	localparam signed [31:0] RV_TIMER_CFG0 = 1;
	localparam signed [31:0] RV_TIMER_TIMER_V_LOWER0 = 2;
	localparam signed [31:0] RV_TIMER_TIMER_V_UPPER0 = 3;
	localparam signed [31:0] RV_TIMER_COMPARE_LOWER0_0 = 4;
	localparam signed [31:0] RV_TIMER_COMPARE_UPPER0_0 = 5;
	localparam signed [31:0] RV_TIMER_INTR_ENABLE0 = 6;
	localparam signed [31:0] RV_TIMER_INTR_STATE0 = 7;
	localparam signed [31:0] RV_TIMER_INTR_TEST0 = 8;
	localparam [35:0] RV_TIMER_PERMIT = {4'b0001, 4'b0111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam signed [31:0] AW = 9;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire ctrl_qs;
	wire ctrl_wd;
	wire ctrl_we;
	wire [11:0] cfg0_prescale_qs;
	wire [11:0] cfg0_prescale_wd;
	wire cfg0_prescale_we;
	wire [7:0] cfg0_step_qs;
	wire [7:0] cfg0_step_wd;
	wire cfg0_step_we;
	wire [31:0] timer_v_lower0_qs;
	wire [31:0] timer_v_lower0_wd;
	wire timer_v_lower0_we;
	wire [31:0] timer_v_upper0_qs;
	wire [31:0] timer_v_upper0_wd;
	wire timer_v_upper0_we;
	wire [31:0] compare_lower0_0_qs;
	wire [31:0] compare_lower0_0_wd;
	wire compare_lower0_0_we;
	wire [31:0] compare_upper0_0_qs;
	wire [31:0] compare_upper0_0_wd;
	wire compare_upper0_0_we;
	wire intr_enable0_qs;
	wire intr_enable0_wd;
	wire intr_enable0_we;
	wire intr_state0_qs;
	wire intr_state0_wd;
	wire intr_state0_we;
	wire intr_test0_wd;
	wire intr_test0_we;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_ctrl(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(ctrl_we),
		.wd(ctrl_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[152]),
		.qs(ctrl_qs)
	);
	prim_subreg #(
		.DW(12),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(12'h000)
	) u_cfg0_prescale(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(cfg0_prescale_we),
		.wd(cfg0_prescale_wd),
		.de(1'b0),
		.d({12 {1'sb0}}),
		.qe(),
		.q(reg2hw[151-:12]),
		.qs(cfg0_prescale_qs)
	);
	prim_subreg #(
		.DW(8),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(8'h01)
	) u_cfg0_step(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(cfg0_step_we),
		.wd(cfg0_step_wd),
		.de(1'b0),
		.d({8 {1'sb0}}),
		.qe(),
		.q(reg2hw[139-:8]),
		.qs(cfg0_step_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_timer_v_lower0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(timer_v_lower0_we),
		.wd(timer_v_lower0_wd),
		.de(hw2reg[35]),
		.d(hw2reg[67-:32]),
		.qe(),
		.q(reg2hw[131-:32]),
		.qs(timer_v_lower0_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'h00000000)
	) u_timer_v_upper0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(timer_v_upper0_we),
		.wd(timer_v_upper0_wd),
		.de(hw2reg[2]),
		.d(hw2reg[34-:32]),
		.qe(),
		.q(reg2hw[99-:32]),
		.qs(timer_v_upper0_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'hffffffff)
	) u_compare_lower0_0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(compare_lower0_0_we),
		.wd(compare_lower0_0_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[67-:32]),
		.qs(compare_lower0_0_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(32'hffffffff)
	) u_compare_upper0_0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(compare_upper0_0_we),
		.wd(compare_upper0_0_wd),
		.de(1'b0),
		.d({32 {1'sb0}}),
		.qe(),
		.q(reg2hw[35-:32]),
		.qs(compare_upper0_0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable0_we),
		.wd(intr_enable0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[3]),
		.qs(intr_enable0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state0_we),
		.wd(intr_state0_wd),
		.de(hw2reg[0]),
		.d(hw2reg[1]),
		.qe(),
		.q(reg2hw[2]),
		.qs(intr_state0_qs)
	);
	prim_subreg_ext #(.DW(1)) u_intr_test0(
		.re(1'b0),
		.we(intr_test0_we),
		.wd(intr_test0_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[0]),
		.q(reg2hw[1]),
		.qs()
	);
	reg [8:0] addr_hit;
	always @(*) begin
		addr_hit = {9 {1'sb0}};
		addr_hit[0] = reg_addr == RV_TIMER_CTRL_OFFSET;
		addr_hit[1] = reg_addr == RV_TIMER_CFG0_OFFSET;
		addr_hit[2] = reg_addr == RV_TIMER_TIMER_V_LOWER0_OFFSET;
		addr_hit[3] = reg_addr == RV_TIMER_TIMER_V_UPPER0_OFFSET;
		addr_hit[4] = reg_addr == RV_TIMER_COMPARE_LOWER0_0_OFFSET;
		addr_hit[5] = reg_addr == RV_TIMER_COMPARE_UPPER0_0_OFFSET;
		addr_hit[6] = reg_addr == RV_TIMER_INTR_ENABLE0_OFFSET;
		addr_hit[7] = reg_addr == RV_TIMER_INTR_STATE0_OFFSET;
		addr_hit[8] = reg_addr == RV_TIMER_INTR_TEST0_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (RV_TIMER_PERMIT[32+:4] != (RV_TIMER_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (RV_TIMER_PERMIT[28+:4] != (RV_TIMER_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (RV_TIMER_PERMIT[24+:4] != (RV_TIMER_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (RV_TIMER_PERMIT[20+:4] != (RV_TIMER_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (RV_TIMER_PERMIT[16+:4] != (RV_TIMER_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (RV_TIMER_PERMIT[12+:4] != (RV_TIMER_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (RV_TIMER_PERMIT[8+:4] != (RV_TIMER_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (RV_TIMER_PERMIT[4+:4] != (RV_TIMER_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (RV_TIMER_PERMIT[0+:4] != (RV_TIMER_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign ctrl_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign ctrl_wd = reg_wdata[0];
	assign cfg0_prescale_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign cfg0_prescale_wd = reg_wdata[11:0];
	assign cfg0_step_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign cfg0_step_wd = reg_wdata[23:16];
	assign timer_v_lower0_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign timer_v_lower0_wd = reg_wdata[31:0];
	assign timer_v_upper0_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign timer_v_upper0_wd = reg_wdata[31:0];
	assign compare_lower0_0_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign compare_lower0_0_wd = reg_wdata[31:0];
	assign compare_upper0_0_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign compare_upper0_0_wd = reg_wdata[31:0];
	assign intr_enable0_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign intr_enable0_wd = reg_wdata[0];
	assign intr_state0_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign intr_state0_wd = reg_wdata[0];
	assign intr_test0_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign intr_test0_wd = reg_wdata[0];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: reg_rdata_next[0] = ctrl_qs;
			addr_hit[1]: begin
				reg_rdata_next[11:0] = cfg0_prescale_qs;
				reg_rdata_next[23:16] = cfg0_step_qs;
			end
			addr_hit[2]: reg_rdata_next[31:0] = timer_v_lower0_qs;
			addr_hit[3]: reg_rdata_next[31:0] = timer_v_upper0_qs;
			addr_hit[4]: reg_rdata_next[31:0] = compare_lower0_0_qs;
			addr_hit[5]: reg_rdata_next[31:0] = compare_upper0_0_qs;
			addr_hit[6]: reg_rdata_next[0] = intr_enable0_qs;
			addr_hit[7]: reg_rdata_next[0] = intr_state0_qs;
			addr_hit[8]: reg_rdata_next[0] = 1'sb0;
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module timer_core (
	clk_i,
	rst_ni,
	active,
	prescaler,
	step,
	tick,
	mtime_d,
	mtime,
	mtimecmp,
	intr
);
	parameter signed [31:0] N = 1;
	input clk_i;
	input rst_ni;
	input active;
	input [11:0] prescaler;
	input [7:0] step;
	output wire tick;
	output wire [63:0] mtime_d;
	input [63:0] mtime;
	input [(0 >= (N - 1) ? ((2 - N) * 64) + (((N - 1) * 64) - 1) : (N * 64) - 1):(0 >= (N - 1) ? (N - 1) * 64 : 0)] mtimecmp;
	output wire [N - 1:0] intr;
	reg [11:0] tick_count;
	always @(posedge clk_i or negedge rst_ni) begin : generate_tick
		if (!rst_ni)
			tick_count <= 12'h000;
		else if (!active)
			tick_count <= 12'h000;
		else if (tick_count == prescaler)
			tick_count <= 12'h000;
		else
			tick_count <= tick_count + 1'b1;
	end
	assign tick = active & (tick_count >= prescaler);
	function automatic [63:0] sv2v_cast_64;
		input reg [63:0] inp;
		sv2v_cast_64 = inp;
	endfunction
	assign mtime_d = mtime + sv2v_cast_64(step);
	generate
		genvar t;
		for (t = 0; t < N; t = t + 1) begin : gen_intr
			assign intr[t] = active & (mtime >= mtimecmp[(0 >= (N - 1) ? t : (N - 1) - t) * 64+:64]);
		end
	endgenerate
endmodule
module rv_timer (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	intr_timer_expired_0_0_o
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire intr_timer_expired_0_0_o;
	localparam signed [31:0] N_HARTS = 1;
	localparam signed [31:0] N_TIMERS = 1;
	localparam signed [31:0] rv_timer_reg_pkg_N_HARTS = 1;
	localparam signed [31:0] rv_timer_reg_pkg_N_TIMERS = 1;
	localparam [8:0] RV_TIMER_CTRL_OFFSET = 9'h000;
	localparam [8:0] RV_TIMER_CFG0_OFFSET = 9'h100;
	localparam [8:0] RV_TIMER_TIMER_V_LOWER0_OFFSET = 9'h104;
	localparam [8:0] RV_TIMER_TIMER_V_UPPER0_OFFSET = 9'h108;
	localparam [8:0] RV_TIMER_COMPARE_LOWER0_0_OFFSET = 9'h10c;
	localparam [8:0] RV_TIMER_COMPARE_UPPER0_0_OFFSET = 9'h110;
	localparam [8:0] RV_TIMER_INTR_ENABLE0_OFFSET = 9'h114;
	localparam [8:0] RV_TIMER_INTR_STATE0_OFFSET = 9'h118;
	localparam [8:0] RV_TIMER_INTR_TEST0_OFFSET = 9'h11c;
	localparam signed [31:0] RV_TIMER_CTRL = 0;
	localparam signed [31:0] RV_TIMER_CFG0 = 1;
	localparam signed [31:0] RV_TIMER_TIMER_V_LOWER0 = 2;
	localparam signed [31:0] RV_TIMER_TIMER_V_UPPER0 = 3;
	localparam signed [31:0] RV_TIMER_COMPARE_LOWER0_0 = 4;
	localparam signed [31:0] RV_TIMER_COMPARE_UPPER0_0 = 5;
	localparam signed [31:0] RV_TIMER_INTR_ENABLE0 = 6;
	localparam signed [31:0] RV_TIMER_INTR_STATE0 = 7;
	localparam signed [31:0] RV_TIMER_INTR_TEST0 = 8;
	localparam [35:0] RV_TIMER_PERMIT = {4'b0001, 4'b0111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	wire [152:0] reg2hw;
	wire [67:0] hw2reg;
	wire [N_HARTS - 1:0] active;
	wire [((2 - N_HARTS) * 12) + (((N_HARTS - 1) * 12) - 1):(N_HARTS - 1) * 12] prescaler;
	wire [((2 - N_HARTS) * 8) + (((N_HARTS - 1) * 8) - 1):(N_HARTS - 1) * 8] step;
	wire [N_HARTS - 1:0] tick;
	wire [63:0] mtime_d [0:N_HARTS - 1];
	wire [63:0] mtime [0:N_HARTS - 1];
	wire [((((((2 - N_HARTS) * (2 - N_TIMERS)) + (((N_TIMERS - 1) + ((N_HARTS - 1) * (2 - N_TIMERS))) - 1)) - ((N_TIMERS - 1) + ((N_HARTS - 1) * (2 - N_TIMERS)))) + 1) * 64) + ((((N_TIMERS - 1) + ((N_HARTS - 1) * (2 - N_TIMERS))) * 64) - 1):((N_TIMERS - 1) + ((N_HARTS - 1) * (2 - N_TIMERS))) * 64] mtimecmp;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_timer_set;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_timer_en;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_timer_test_q;
	wire [N_HARTS - 1:0] intr_timer_test_qe;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_timer_state_q;
	wire [N_HARTS - 1:0] intr_timer_state_de;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_timer_state_d;
	wire [(N_HARTS * N_TIMERS) - 1:0] intr_out;
	assign active[0] = reg2hw[152];
	assign prescaler = reg2hw[151-:12];
	assign step = reg2hw[139-:8];
	assign hw2reg[2] = tick[0];
	assign hw2reg[35] = tick[0];
	assign hw2reg[34-:32] = mtime_d[0][63:32];
	assign hw2reg[67-:32] = mtime_d[0][31:0];
	assign mtime[0] = {reg2hw[99-:32], reg2hw[131-:32]};
	assign mtimecmp = {reg2hw[35-:32], reg2hw[67-:32]};
	assign intr_timer_expired_0_0_o = intr_out[0];
	assign intr_timer_en = reg2hw[3];
	assign intr_timer_state_q = reg2hw[2];
	assign intr_timer_test_q = reg2hw[1];
	assign intr_timer_test_qe = reg2hw[0];
	assign hw2reg[0] = intr_timer_state_de;
	assign hw2reg[1] = intr_timer_state_d;
	generate
		genvar h;
		for (h = 0; h < N_HARTS; h = h + 1) begin : gen_harts
			prim_intr_hw #(.Width(N_TIMERS)) u_intr_hw(
				.event_intr_i(intr_timer_set),
				.reg2hw_intr_enable_q_i(intr_timer_en[h * N_TIMERS+:N_TIMERS]),
				.reg2hw_intr_test_q_i(intr_timer_test_q[h * N_TIMERS+:N_TIMERS]),
				.reg2hw_intr_test_qe_i(intr_timer_test_qe[h]),
				.reg2hw_intr_state_q_i(intr_timer_state_q[h * N_TIMERS+:N_TIMERS]),
				.hw2reg_intr_state_de_o(intr_timer_state_de),
				.hw2reg_intr_state_d_o(intr_timer_state_d[h * N_TIMERS+:N_TIMERS]),
				.intr_o(intr_out[h * N_TIMERS+:N_TIMERS])
			);
			timer_core #(.N(N_TIMERS)) u_core(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.active(active[h]),
				.prescaler(prescaler[h * 12+:12]),
				.step(step[h * 8+:8]),
				.tick(tick[h]),
				.mtime_d(mtime_d[h]),
				.mtime(mtime[h]),
				.mtimecmp(mtimecmp[64 * ((N_TIMERS - 1) + (h * (2 - N_TIMERS)))+:64 * (2 - N_TIMERS)]),
				.intr(intr_timer_set[h * N_TIMERS+:N_TIMERS])
			);
		end
	endgenerate
	rv_timer_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
endmodule
module aes_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [541:0] reg2hw;
	input wire [534:0] hw2reg;
	input devmode_i;
	localparam signed [31:0] NumRegsKey = 8;
	localparam signed [31:0] NumRegsData = 4;
	localparam [6:0] AES_KEY0_OFFSET = 7'h00;
	localparam [6:0] AES_KEY1_OFFSET = 7'h04;
	localparam [6:0] AES_KEY2_OFFSET = 7'h08;
	localparam [6:0] AES_KEY3_OFFSET = 7'h0c;
	localparam [6:0] AES_KEY4_OFFSET = 7'h10;
	localparam [6:0] AES_KEY5_OFFSET = 7'h14;
	localparam [6:0] AES_KEY6_OFFSET = 7'h18;
	localparam [6:0] AES_KEY7_OFFSET = 7'h1c;
	localparam [6:0] AES_DATA_IN0_OFFSET = 7'h20;
	localparam [6:0] AES_DATA_IN1_OFFSET = 7'h24;
	localparam [6:0] AES_DATA_IN2_OFFSET = 7'h28;
	localparam [6:0] AES_DATA_IN3_OFFSET = 7'h2c;
	localparam [6:0] AES_DATA_OUT0_OFFSET = 7'h30;
	localparam [6:0] AES_DATA_OUT1_OFFSET = 7'h34;
	localparam [6:0] AES_DATA_OUT2_OFFSET = 7'h38;
	localparam [6:0] AES_DATA_OUT3_OFFSET = 7'h3c;
	localparam [6:0] AES_CTRL_OFFSET = 7'h40;
	localparam [6:0] AES_TRIGGER_OFFSET = 7'h44;
	localparam [6:0] AES_STATUS_OFFSET = 7'h48;
	localparam signed [31:0] AES_KEY0 = 0;
	localparam signed [31:0] AES_KEY1 = 1;
	localparam signed [31:0] AES_KEY2 = 2;
	localparam signed [31:0] AES_KEY3 = 3;
	localparam signed [31:0] AES_KEY4 = 4;
	localparam signed [31:0] AES_KEY5 = 5;
	localparam signed [31:0] AES_KEY6 = 6;
	localparam signed [31:0] AES_KEY7 = 7;
	localparam signed [31:0] AES_DATA_IN0 = 8;
	localparam signed [31:0] AES_DATA_IN1 = 9;
	localparam signed [31:0] AES_DATA_IN2 = 10;
	localparam signed [31:0] AES_DATA_IN3 = 11;
	localparam signed [31:0] AES_DATA_OUT0 = 12;
	localparam signed [31:0] AES_DATA_OUT1 = 13;
	localparam signed [31:0] AES_DATA_OUT2 = 14;
	localparam signed [31:0] AES_DATA_OUT3 = 15;
	localparam signed [31:0] AES_CTRL = 16;
	localparam signed [31:0] AES_TRIGGER = 17;
	localparam signed [31:0] AES_STATUS = 18;
	localparam [75:0] AES_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam signed [31:0] AW = 7;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire [31:0] key0_wd;
	wire key0_we;
	wire [31:0] key1_wd;
	wire key1_we;
	wire [31:0] key2_wd;
	wire key2_we;
	wire [31:0] key3_wd;
	wire key3_we;
	wire [31:0] key4_wd;
	wire key4_we;
	wire [31:0] key5_wd;
	wire key5_we;
	wire [31:0] key6_wd;
	wire key6_we;
	wire [31:0] key7_wd;
	wire key7_we;
	wire [31:0] data_in0_wd;
	wire data_in0_we;
	wire [31:0] data_in1_wd;
	wire data_in1_we;
	wire [31:0] data_in2_wd;
	wire data_in2_we;
	wire [31:0] data_in3_wd;
	wire data_in3_we;
	wire [31:0] data_out0_qs;
	wire data_out0_re;
	wire [31:0] data_out1_qs;
	wire data_out1_re;
	wire [31:0] data_out2_qs;
	wire data_out2_re;
	wire [31:0] data_out3_qs;
	wire data_out3_re;
	wire ctrl_mode_qs;
	wire ctrl_mode_wd;
	wire ctrl_mode_we;
	wire ctrl_mode_re;
	wire [2:0] ctrl_key_len_qs;
	wire [2:0] ctrl_key_len_wd;
	wire ctrl_key_len_we;
	wire ctrl_key_len_re;
	wire ctrl_manual_start_trigger_qs;
	wire ctrl_manual_start_trigger_wd;
	wire ctrl_manual_start_trigger_we;
	wire ctrl_manual_start_trigger_re;
	wire ctrl_force_data_overwrite_qs;
	wire ctrl_force_data_overwrite_wd;
	wire ctrl_force_data_overwrite_we;
	wire ctrl_force_data_overwrite_re;
	wire trigger_start_wd;
	wire trigger_start_we;
	wire trigger_key_clear_wd;
	wire trigger_key_clear_we;
	wire trigger_data_in_clear_wd;
	wire trigger_data_in_clear_we;
	wire trigger_data_out_clear_wd;
	wire trigger_data_out_clear_we;
	wire status_idle_qs;
	wire status_stall_qs;
	wire status_output_valid_qs;
	wire status_input_ready_qs;
	prim_subreg_ext #(.DW(32)) u_key0(
		.re(1'b0),
		.we(key0_we),
		.wd(key0_wd),
		.d(hw2reg[310-:32]),
		.qre(),
		.qe(reg2hw[278]),
		.q(reg2hw[310-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key1(
		.re(1'b0),
		.we(key1_we),
		.wd(key1_wd),
		.d(hw2reg[342-:32]),
		.qre(),
		.qe(reg2hw[311]),
		.q(reg2hw[343-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key2(
		.re(1'b0),
		.we(key2_we),
		.wd(key2_wd),
		.d(hw2reg[374-:32]),
		.qre(),
		.qe(reg2hw[344]),
		.q(reg2hw[376-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key3(
		.re(1'b0),
		.we(key3_we),
		.wd(key3_wd),
		.d(hw2reg[406-:32]),
		.qre(),
		.qe(reg2hw[377]),
		.q(reg2hw[409-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key4(
		.re(1'b0),
		.we(key4_we),
		.wd(key4_wd),
		.d(hw2reg[438-:32]),
		.qre(),
		.qe(reg2hw[410]),
		.q(reg2hw[442-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key5(
		.re(1'b0),
		.we(key5_we),
		.wd(key5_wd),
		.d(hw2reg[470-:32]),
		.qre(),
		.qe(reg2hw[443]),
		.q(reg2hw[475-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key6(
		.re(1'b0),
		.we(key6_we),
		.wd(key6_wd),
		.d(hw2reg[502-:32]),
		.qre(),
		.qe(reg2hw[476]),
		.q(reg2hw[508-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key7(
		.re(1'b0),
		.we(key7_we),
		.wd(key7_wd),
		.d(hw2reg[534-:32]),
		.qre(),
		.qe(reg2hw[509]),
		.q(reg2hw[541-:32]),
		.qs()
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(32'h00000000)
	) u_data_in0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(data_in0_we),
		.wd(data_in0_wd),
		.de(hw2reg[147]),
		.d(hw2reg[179-:32]),
		.qe(reg2hw[146]),
		.q(reg2hw[178-:32]),
		.qs()
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(32'h00000000)
	) u_data_in1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(data_in1_we),
		.wd(data_in1_wd),
		.de(hw2reg[180]),
		.d(hw2reg[212-:32]),
		.qe(reg2hw[179]),
		.q(reg2hw[211-:32]),
		.qs()
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(32'h00000000)
	) u_data_in2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(data_in2_we),
		.wd(data_in2_wd),
		.de(hw2reg[213]),
		.d(hw2reg[245-:32]),
		.qe(reg2hw[212]),
		.q(reg2hw[244-:32]),
		.qs()
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(32'h00000000)
	) u_data_in3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(data_in3_we),
		.wd(data_in3_wd),
		.de(hw2reg[246]),
		.d(hw2reg[278-:32]),
		.qe(reg2hw[245]),
		.q(reg2hw[277-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_data_out0(
		.re(data_out0_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[50-:32]),
		.qre(reg2hw[14]),
		.qe(),
		.q(reg2hw[46-:32]),
		.qs(data_out0_qs)
	);
	prim_subreg_ext #(.DW(32)) u_data_out1(
		.re(data_out1_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[82-:32]),
		.qre(reg2hw[47]),
		.qe(),
		.q(reg2hw[79-:32]),
		.qs(data_out1_qs)
	);
	prim_subreg_ext #(.DW(32)) u_data_out2(
		.re(data_out2_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[114-:32]),
		.qre(reg2hw[80]),
		.qe(),
		.q(reg2hw[112-:32]),
		.qs(data_out2_qs)
	);
	prim_subreg_ext #(.DW(32)) u_data_out3(
		.re(data_out3_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[146-:32]),
		.qre(reg2hw[113]),
		.qe(),
		.q(reg2hw[145-:32]),
		.qs(data_out3_qs)
	);
	prim_subreg_ext #(.DW(1)) u_ctrl_mode(
		.re(ctrl_mode_re),
		.we(ctrl_mode_we),
		.wd(ctrl_mode_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[12]),
		.q(reg2hw[13]),
		.qs(ctrl_mode_qs)
	);
	prim_subreg_ext #(.DW(3)) u_ctrl_key_len(
		.re(ctrl_key_len_re),
		.we(ctrl_key_len_we),
		.wd(ctrl_key_len_wd),
		.d(hw2reg[18-:3]),
		.qre(),
		.qe(reg2hw[8]),
		.q(reg2hw[11-:3]),
		.qs(ctrl_key_len_qs)
	);
	prim_subreg_ext #(.DW(1)) u_ctrl_manual_start_trigger(
		.re(ctrl_manual_start_trigger_re),
		.we(ctrl_manual_start_trigger_we),
		.wd(ctrl_manual_start_trigger_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[6]),
		.q(reg2hw[7]),
		.qs(ctrl_manual_start_trigger_qs)
	);
	prim_subreg_ext #(.DW(1)) u_ctrl_force_data_overwrite(
		.re(ctrl_force_data_overwrite_re),
		.we(ctrl_force_data_overwrite_we),
		.wd(ctrl_force_data_overwrite_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[4]),
		.q(reg2hw[5]),
		.qs(ctrl_force_data_overwrite_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_trigger_start(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(trigger_start_we),
		.wd(trigger_start_wd),
		.de(hw2reg[14]),
		.d(hw2reg[15]),
		.qe(),
		.q(reg2hw[3]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_trigger_key_clear(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(trigger_key_clear_we),
		.wd(trigger_key_clear_wd),
		.de(hw2reg[12]),
		.d(hw2reg[13]),
		.qe(),
		.q(reg2hw[2]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_trigger_data_in_clear(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(trigger_data_in_clear_we),
		.wd(trigger_data_in_clear_wd),
		.de(hw2reg[10]),
		.d(hw2reg[11]),
		.qe(),
		.q(reg2hw[1]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("WO"),
		.RESVAL(1'h0)
	) u_trigger_data_out_clear(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(trigger_data_out_clear_we),
		.wd(trigger_data_out_clear_wd),
		.de(hw2reg[8]),
		.d(hw2reg[9]),
		.qe(),
		.q(reg2hw[-0]),
		.qs()
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_status_idle(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[6]),
		.d(hw2reg[7]),
		.qe(),
		.q(),
		.qs(status_idle_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_status_stall(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[4]),
		.d(hw2reg[5]),
		.qe(),
		.q(),
		.qs(status_stall_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h0)
	) u_status_output_valid(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[2]),
		.d(hw2reg[3]),
		.qe(),
		.q(),
		.qs(status_output_valid_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(1'h1)
	) u_status_input_ready(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd(1'sb0),
		.de(hw2reg[0]),
		.d(hw2reg[1]),
		.qe(),
		.q(),
		.qs(status_input_ready_qs)
	);
	reg [18:0] addr_hit;
	always @(*) begin
		addr_hit = {19 {1'sb0}};
		addr_hit[0] = reg_addr == AES_KEY0_OFFSET;
		addr_hit[1] = reg_addr == AES_KEY1_OFFSET;
		addr_hit[2] = reg_addr == AES_KEY2_OFFSET;
		addr_hit[3] = reg_addr == AES_KEY3_OFFSET;
		addr_hit[4] = reg_addr == AES_KEY4_OFFSET;
		addr_hit[5] = reg_addr == AES_KEY5_OFFSET;
		addr_hit[6] = reg_addr == AES_KEY6_OFFSET;
		addr_hit[7] = reg_addr == AES_KEY7_OFFSET;
		addr_hit[8] = reg_addr == AES_DATA_IN0_OFFSET;
		addr_hit[9] = reg_addr == AES_DATA_IN1_OFFSET;
		addr_hit[10] = reg_addr == AES_DATA_IN2_OFFSET;
		addr_hit[11] = reg_addr == AES_DATA_IN3_OFFSET;
		addr_hit[12] = reg_addr == AES_DATA_OUT0_OFFSET;
		addr_hit[13] = reg_addr == AES_DATA_OUT1_OFFSET;
		addr_hit[14] = reg_addr == AES_DATA_OUT2_OFFSET;
		addr_hit[15] = reg_addr == AES_DATA_OUT3_OFFSET;
		addr_hit[16] = reg_addr == AES_CTRL_OFFSET;
		addr_hit[17] = reg_addr == AES_TRIGGER_OFFSET;
		addr_hit[18] = reg_addr == AES_STATUS_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (AES_PERMIT[72+:4] != (AES_PERMIT[72+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (AES_PERMIT[68+:4] != (AES_PERMIT[68+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (AES_PERMIT[64+:4] != (AES_PERMIT[64+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (AES_PERMIT[60+:4] != (AES_PERMIT[60+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (AES_PERMIT[56+:4] != (AES_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (AES_PERMIT[52+:4] != (AES_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (AES_PERMIT[48+:4] != (AES_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (AES_PERMIT[44+:4] != (AES_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (AES_PERMIT[40+:4] != (AES_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (AES_PERMIT[36+:4] != (AES_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (AES_PERMIT[32+:4] != (AES_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (AES_PERMIT[28+:4] != (AES_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (AES_PERMIT[24+:4] != (AES_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (AES_PERMIT[20+:4] != (AES_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (AES_PERMIT[16+:4] != (AES_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[15] && reg_we) && (AES_PERMIT[12+:4] != (AES_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[16] && reg_we) && (AES_PERMIT[8+:4] != (AES_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[17] && reg_we) && (AES_PERMIT[4+:4] != (AES_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[18] && reg_we) && (AES_PERMIT[0+:4] != (AES_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign key0_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign key0_wd = reg_wdata[31:0];
	assign key1_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign key1_wd = reg_wdata[31:0];
	assign key2_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign key2_wd = reg_wdata[31:0];
	assign key3_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign key3_wd = reg_wdata[31:0];
	assign key4_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign key4_wd = reg_wdata[31:0];
	assign key5_we = (addr_hit[5] & reg_we) & ~wr_err;
	assign key5_wd = reg_wdata[31:0];
	assign key6_we = (addr_hit[6] & reg_we) & ~wr_err;
	assign key6_wd = reg_wdata[31:0];
	assign key7_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign key7_wd = reg_wdata[31:0];
	assign data_in0_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign data_in0_wd = reg_wdata[31:0];
	assign data_in1_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign data_in1_wd = reg_wdata[31:0];
	assign data_in2_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign data_in2_wd = reg_wdata[31:0];
	assign data_in3_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign data_in3_wd = reg_wdata[31:0];
	assign data_out0_re = addr_hit[12] && reg_re;
	assign data_out1_re = addr_hit[13] && reg_re;
	assign data_out2_re = addr_hit[14] && reg_re;
	assign data_out3_re = addr_hit[15] && reg_re;
	assign ctrl_mode_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign ctrl_mode_wd = reg_wdata[0];
	assign ctrl_mode_re = addr_hit[16] && reg_re;
	assign ctrl_key_len_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign ctrl_key_len_wd = reg_wdata[3:1];
	assign ctrl_key_len_re = addr_hit[16] && reg_re;
	assign ctrl_manual_start_trigger_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign ctrl_manual_start_trigger_wd = reg_wdata[4];
	assign ctrl_manual_start_trigger_re = addr_hit[16] && reg_re;
	assign ctrl_force_data_overwrite_we = (addr_hit[16] & reg_we) & ~wr_err;
	assign ctrl_force_data_overwrite_wd = reg_wdata[5];
	assign ctrl_force_data_overwrite_re = addr_hit[16] && reg_re;
	assign trigger_start_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign trigger_start_wd = reg_wdata[0];
	assign trigger_key_clear_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign trigger_key_clear_wd = reg_wdata[1];
	assign trigger_data_in_clear_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign trigger_data_in_clear_wd = reg_wdata[2];
	assign trigger_data_out_clear_we = (addr_hit[17] & reg_we) & ~wr_err;
	assign trigger_data_out_clear_wd = reg_wdata[3];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[1]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[2]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[3]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[4]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[5]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[6]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[7]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[8]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[9]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[10]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[11]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[12]: reg_rdata_next[31:0] = data_out0_qs;
			addr_hit[13]: reg_rdata_next[31:0] = data_out1_qs;
			addr_hit[14]: reg_rdata_next[31:0] = data_out2_qs;
			addr_hit[15]: reg_rdata_next[31:0] = data_out3_qs;
			addr_hit[16]: begin
				reg_rdata_next[0] = ctrl_mode_qs;
				reg_rdata_next[3:1] = ctrl_key_len_qs;
				reg_rdata_next[4] = ctrl_manual_start_trigger_qs;
				reg_rdata_next[5] = ctrl_force_data_overwrite_qs;
			end
			addr_hit[17]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[2] = 1'sb0;
				reg_rdata_next[3] = 1'sb0;
			end
			addr_hit[18]: begin
				reg_rdata_next[0] = status_idle_qs;
				reg_rdata_next[1] = status_stall_qs;
				reg_rdata_next[2] = status_output_valid_qs;
				reg_rdata_next[3] = status_input_ready_qs;
			end
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module aes_core (
	clk_i,
	rst_ni,
	reg2hw,
	hw2reg
);
	parameter [0:0] AES192Enable = 1;
	parameter _sv2v_width_SBoxImpl = 24;
	parameter [_sv2v_width_SBoxImpl - 1:0] SBoxImpl = "lut";
	input clk_i;
	input rst_ni;
	input wire [541:0] reg2hw;
	output reg [534:0] hw2reg;
	localparam signed [31:0] NumRegsKey = 8;
	localparam signed [31:0] NumRegsData = 4;
	localparam [6:0] AES_KEY0_OFFSET = 7'h00;
	localparam [6:0] AES_KEY1_OFFSET = 7'h04;
	localparam [6:0] AES_KEY2_OFFSET = 7'h08;
	localparam [6:0] AES_KEY3_OFFSET = 7'h0c;
	localparam [6:0] AES_KEY4_OFFSET = 7'h10;
	localparam [6:0] AES_KEY5_OFFSET = 7'h14;
	localparam [6:0] AES_KEY6_OFFSET = 7'h18;
	localparam [6:0] AES_KEY7_OFFSET = 7'h1c;
	localparam [6:0] AES_DATA_IN0_OFFSET = 7'h20;
	localparam [6:0] AES_DATA_IN1_OFFSET = 7'h24;
	localparam [6:0] AES_DATA_IN2_OFFSET = 7'h28;
	localparam [6:0] AES_DATA_IN3_OFFSET = 7'h2c;
	localparam [6:0] AES_DATA_OUT0_OFFSET = 7'h30;
	localparam [6:0] AES_DATA_OUT1_OFFSET = 7'h34;
	localparam [6:0] AES_DATA_OUT2_OFFSET = 7'h38;
	localparam [6:0] AES_DATA_OUT3_OFFSET = 7'h3c;
	localparam [6:0] AES_CTRL_OFFSET = 7'h40;
	localparam [6:0] AES_TRIGGER_OFFSET = 7'h44;
	localparam [6:0] AES_STATUS_OFFSET = 7'h48;
	localparam signed [31:0] AES_KEY0 = 0;
	localparam signed [31:0] AES_KEY1 = 1;
	localparam signed [31:0] AES_KEY2 = 2;
	localparam signed [31:0] AES_KEY3 = 3;
	localparam signed [31:0] AES_KEY4 = 4;
	localparam signed [31:0] AES_KEY5 = 5;
	localparam signed [31:0] AES_KEY6 = 6;
	localparam signed [31:0] AES_KEY7 = 7;
	localparam signed [31:0] AES_DATA_IN0 = 8;
	localparam signed [31:0] AES_DATA_IN1 = 9;
	localparam signed [31:0] AES_DATA_IN2 = 10;
	localparam signed [31:0] AES_DATA_IN3 = 11;
	localparam signed [31:0] AES_DATA_OUT0 = 12;
	localparam signed [31:0] AES_DATA_OUT1 = 13;
	localparam signed [31:0] AES_DATA_OUT2 = 14;
	localparam signed [31:0] AES_DATA_OUT3 = 15;
	localparam signed [31:0] AES_CTRL = 16;
	localparam signed [31:0] AES_TRIGGER = 17;
	localparam signed [31:0] AES_STATUS = 18;
	localparam [75:0] AES_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_412
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_413
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_414
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_415
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	reg [127:0] data_in;
	reg [3:0] data_in_qe;
	wire data_in_we;
	reg [255:0] key_init;
	reg [7:0] key_init_qe;
	wire ctrl_qe;
	wire ctrl_we;
	wire mode_d;
	reg mode_q;
	wire [2:0] key_len;
	reg [2:0] key_len_d;
	reg [2:0] key_len_q;
	reg manual_start_trigger_q;
	reg force_data_overwrite_q;
	wire [127:0] state_init;
	reg [127:0] state_d;
	reg [127:0] state_q;
	wire state_we;
	wire [1:0] state_sel;
	wire [127:0] sub_bytes_out;
	wire [127:0] shift_rows_out;
	wire [127:0] mix_columns_out;
	reg [127:0] add_round_key_in;
	wire [127:0] add_round_key_out;
	wire [1:0] add_round_key_in_sel;
	reg [255:0] key_init_d;
	reg [255:0] key_init_q;
	wire [7:0] key_init_we;
	wire key_init_sel;
	reg [255:0] key_full_d;
	reg [255:0] key_full_q;
	wire key_full_we;
	wire [1:0] key_full_sel;
	reg [255:0] key_dec_d;
	reg [255:0] key_dec_q;
	wire key_dec_we;
	wire key_dec_sel;
	wire [255:0] key_expand_out;
	wire key_expand_mode;
	wire key_expand_step;
	wire key_expand_clear;
	wire [3:0] key_expand_round;
	wire [1:0] key_words_sel;
	reg [127:0] key_words;
	wire [127:0] key_bytes;
	wire [127:0] key_mix_columns_out;
	reg [127:0] round_key;
	wire round_key_sel;
	wire [127:0] data_out_d;
	reg [127:0] data_out_q;
	wire data_out_we;
	reg [3:0] data_out_re;
	reg [127:0] unused_data_out_q;
	always @(*) begin : key_init_get
		begin : sv2v_autoblock_416
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				begin
					key_init[i * 32+:32] = reg2hw[278 + ((i * 33) + 32)-:32];
					key_init_qe[i] = reg2hw[278 + (i * 33)];
				end
		end
	end
	always @(*) begin : data_in_get
		begin : sv2v_autoblock_417
			reg signed [31:0] i;
			for (i = 0; i < 4; i = i + 1)
				begin
					data_in[i * 32+:32] = reg2hw[146 + ((i * 33) + 32)-:32];
					data_in_qe[i] = reg2hw[146 + (i * 33)];
				end
		end
	end
	always @(*) begin : data_out_get
		begin : sv2v_autoblock_418
			reg signed [31:0] i;
			for (i = 0; i < 4; i = i + 1)
				begin
					unused_data_out_q[i * 32+:32] = reg2hw[14 + ((i * 33) + 32)-:32];
					data_out_re[i] = reg2hw[14 + (i * 33)];
				end
		end
	end
	function automatic [0:0] sv2v_cast_1;
		input reg [0:0] inp;
		sv2v_cast_1 = inp;
	endfunction
	assign mode_d = sv2v_cast_1(reg2hw[13]);
	function automatic [2:0] sv2v_cast_3;
		input reg [2:0] inp;
		sv2v_cast_3 = inp;
	endfunction
	assign key_len = sv2v_cast_3(reg2hw[11-:3]);
	always @(*) begin : get_key_len
		case (key_len)
			AES_128: key_len_d = AES_128;
			AES_256: key_len_d = AES_256;
			AES_192: key_len_d = (AES192Enable ? AES_192 : AES_128);
			default: key_len_d = AES_128;
		endcase
	end
	assign ctrl_qe = ((reg2hw[12] & reg2hw[8]) & reg2hw[6]) & reg2hw[4];
	assign state_init = aes_transpose(data_in);
	always @(*) begin : state_mux
		case (state_sel)
			STATE_INIT: state_d = state_init;
			STATE_ROUND: state_d = add_round_key_out;
			STATE_CLEAR: state_d = {128 {1'sb0}};
			default: state_d = state_init;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : state_reg
		if (!rst_ni)
			state_q <= {128 {1'sb0}};
		else if (state_we)
			state_q <= state_d;
	end
	aes_sub_bytes #(
		._sv2v_width_SBoxImpl(_sv2v_width_SBoxImpl),
		.SBoxImpl(SBoxImpl)
	) aes_sub_bytes(
		.mode_i(mode_q),
		.data_i(state_q),
		.data_o(sub_bytes_out)
	);
	aes_shift_rows aes_shift_rows(
		.mode_i(mode_q),
		.data_i(sub_bytes_out),
		.data_o(shift_rows_out)
	);
	aes_mix_columns aes_mix_columns(
		.mode_i(mode_q),
		.data_i(shift_rows_out),
		.data_o(mix_columns_out)
	);
	always @(*) begin : add_round_key_in_mux
		case (add_round_key_in_sel)
			ADD_RK_INIT: add_round_key_in = state_q;
			ADD_RK_ROUND: add_round_key_in = mix_columns_out;
			ADD_RK_FINAL: add_round_key_in = shift_rows_out;
			default: add_round_key_in = state_q;
		endcase
	end
	assign add_round_key_out = add_round_key_in ^ round_key;
	always @(*) begin : key_init_mux
		case (key_init_sel)
			KEY_INIT_INPUT: key_init_d = key_init;
			KEY_INIT_CLEAR: key_init_d = {256 {1'sb0}};
			default: key_init_d = key_init;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : key_init_reg
		if (!rst_ni)
			key_init_q <= {256 {1'sb0}};
		else begin : sv2v_autoblock_419
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				if (key_init_we[i])
					key_init_q[i * 32+:32] <= key_init_d[i * 32+:32];
		end
	end
	always @(*) begin : key_full_mux
		case (key_full_sel)
			KEY_FULL_ENC_INIT: key_full_d = key_init_q;
			KEY_FULL_DEC_INIT: key_full_d = key_dec_q;
			KEY_FULL_ROUND: key_full_d = key_expand_out;
			KEY_FULL_CLEAR: key_full_d = {256 {1'sb0}};
			default: key_full_d = key_init_q;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : key_full_reg
		if (!rst_ni)
			key_full_q <= {256 {1'sb0}};
		else if (key_full_we)
			key_full_q <= key_full_d;
	end
	always @(*) begin : key_dec_mux
		case (key_dec_sel)
			KEY_DEC_EXPAND: key_dec_d = key_expand_out;
			KEY_DEC_CLEAR: key_dec_d = {256 {1'sb0}};
			default: key_dec_d = key_expand_out;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : key_dec_reg
		if (!rst_ni)
			key_dec_q <= {256 {1'sb0}};
		else if (key_dec_we)
			key_dec_q <= key_dec_d;
	end
	aes_key_expand #(
		.AES192Enable(AES192Enable),
		._sv2v_width_SBoxImpl(_sv2v_width_SBoxImpl),
		.SBoxImpl(SBoxImpl)
	) aes_key_expand(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.mode_i(key_expand_mode),
		.step_i(key_expand_step),
		.clear_i(key_expand_clear),
		.round_i(key_expand_round),
		.key_len_i(key_len_q),
		.key_i(key_full_q),
		.key_o(key_expand_out)
	);
	always @(*) begin : key_words_mux
		case (key_words_sel)
			KEY_WORDS_0123: key_words = key_full_q[0+:128];
			KEY_WORDS_2345: key_words = (AES192Enable ? key_full_q[64+:128] : key_full_q[0+:128]);
			KEY_WORDS_4567: key_words = key_full_q[128+:128];
			KEY_WORDS_ZERO: key_words = {128 {1'sb0}};
			default: key_words = key_full_q[0+:128];
		endcase
	end
	assign key_bytes = aes_transpose(key_words);
	aes_mix_columns aes_key_mix_columns(
		.mode_i(AES_DEC),
		.data_i(key_bytes),
		.data_o(key_mix_columns_out)
	);
	always @(*) begin : round_key_mux
		case (round_key_sel)
			ROUND_KEY_DIRECT: round_key = key_bytes;
			ROUND_KEY_MIXED: round_key = key_mix_columns_out;
			default: round_key = key_bytes;
		endcase
	end
	wire [1:1] sv2v_tmp_aes_control_start_o;
	always @(*) hw2reg[15] = sv2v_tmp_aes_control_start_o;
	wire [1:1] sv2v_tmp_aes_control_start_we_o;
	always @(*) hw2reg[14] = sv2v_tmp_aes_control_start_we_o;
	wire [1:1] sv2v_tmp_aes_control_key_clear_o;
	always @(*) hw2reg[13] = sv2v_tmp_aes_control_key_clear_o;
	wire [1:1] sv2v_tmp_aes_control_key_clear_we_o;
	always @(*) hw2reg[12] = sv2v_tmp_aes_control_key_clear_we_o;
	wire [1:1] sv2v_tmp_aes_control_data_in_clear_o;
	always @(*) hw2reg[11] = sv2v_tmp_aes_control_data_in_clear_o;
	wire [1:1] sv2v_tmp_aes_control_data_in_clear_we_o;
	always @(*) hw2reg[10] = sv2v_tmp_aes_control_data_in_clear_we_o;
	wire [1:1] sv2v_tmp_aes_control_data_out_clear_o;
	always @(*) hw2reg[9] = sv2v_tmp_aes_control_data_out_clear_o;
	wire [1:1] sv2v_tmp_aes_control_data_out_clear_we_o;
	always @(*) hw2reg[8] = sv2v_tmp_aes_control_data_out_clear_we_o;
	wire [1:1] sv2v_tmp_aes_control_output_valid_o;
	always @(*) hw2reg[3] = sv2v_tmp_aes_control_output_valid_o;
	wire [1:1] sv2v_tmp_aes_control_output_valid_we_o;
	always @(*) hw2reg[2] = sv2v_tmp_aes_control_output_valid_we_o;
	wire [1:1] sv2v_tmp_aes_control_input_ready_o;
	always @(*) hw2reg[1] = sv2v_tmp_aes_control_input_ready_o;
	wire [1:1] sv2v_tmp_aes_control_input_ready_we_o;
	always @(*) hw2reg[0] = sv2v_tmp_aes_control_input_ready_we_o;
	wire [1:1] sv2v_tmp_aes_control_idle_o;
	always @(*) hw2reg[7] = sv2v_tmp_aes_control_idle_o;
	wire [1:1] sv2v_tmp_aes_control_idle_we_o;
	always @(*) hw2reg[6] = sv2v_tmp_aes_control_idle_we_o;
	wire [1:1] sv2v_tmp_aes_control_stall_o;
	always @(*) hw2reg[5] = sv2v_tmp_aes_control_stall_o;
	wire [1:1] sv2v_tmp_aes_control_stall_we_o;
	always @(*) hw2reg[4] = sv2v_tmp_aes_control_stall_we_o;
	aes_control aes_control(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.mode_i(mode_q),
		.key_len_i(key_len_q),
		.manual_start_trigger_i(manual_start_trigger_q),
		.force_data_overwrite_i(force_data_overwrite_q),
		.start_i(reg2hw[3]),
		.key_clear_i(reg2hw[2]),
		.data_in_clear_i(reg2hw[1]),
		.data_out_clear_i(reg2hw[-0]),
		.data_in_qe_i(data_in_qe),
		.key_init_qe_i(key_init_qe),
		.data_out_re_i(data_out_re),
		.state_sel_o(state_sel),
		.state_we_o(state_we),
		.add_rk_sel_o(add_round_key_in_sel),
		.key_expand_mode_o(key_expand_mode),
		.key_init_sel_o(key_init_sel),
		.key_init_we_o(key_init_we),
		.key_full_sel_o(key_full_sel),
		.key_full_we_o(key_full_we),
		.key_dec_sel_o(key_dec_sel),
		.key_dec_we_o(key_dec_we),
		.key_expand_step_o(key_expand_step),
		.key_expand_clear_o(key_expand_clear),
		.key_expand_round_o(key_expand_round),
		.key_words_sel_o(key_words_sel),
		.round_key_sel_o(round_key_sel),
		.data_in_we_o(data_in_we),
		.data_out_we_o(data_out_we),
		.start_o(sv2v_tmp_aes_control_start_o),
		.start_we_o(sv2v_tmp_aes_control_start_we_o),
		.key_clear_o(sv2v_tmp_aes_control_key_clear_o),
		.key_clear_we_o(sv2v_tmp_aes_control_key_clear_we_o),
		.data_in_clear_o(sv2v_tmp_aes_control_data_in_clear_o),
		.data_in_clear_we_o(sv2v_tmp_aes_control_data_in_clear_we_o),
		.data_out_clear_o(sv2v_tmp_aes_control_data_out_clear_o),
		.data_out_clear_we_o(sv2v_tmp_aes_control_data_out_clear_we_o),
		.output_valid_o(sv2v_tmp_aes_control_output_valid_o),
		.output_valid_we_o(sv2v_tmp_aes_control_output_valid_we_o),
		.input_ready_o(sv2v_tmp_aes_control_input_ready_o),
		.input_ready_we_o(sv2v_tmp_aes_control_input_ready_we_o),
		.idle_o(sv2v_tmp_aes_control_idle_o),
		.idle_we_o(sv2v_tmp_aes_control_idle_we_o),
		.stall_o(sv2v_tmp_aes_control_stall_o),
		.stall_we_o(sv2v_tmp_aes_control_stall_we_o)
	);
	always @(*) begin : data_in_reg_clear
		begin : sv2v_autoblock_420
			reg signed [31:0] i;
			for (i = 0; i < 4; i = i + 1)
				begin
					hw2reg[147 + ((i * 33) + 32)-:32] = {32 {1'sb0}};
					hw2reg[147 + (i * 33)] = data_in_we;
				end
		end
	end
	assign ctrl_we = ctrl_qe & hw2reg[7];
	always @(posedge clk_i or negedge rst_ni) begin : ctrl_reg
		if (!rst_ni) begin
			mode_q <= AES_ENC;
			key_len_q <= AES_128;
			manual_start_trigger_q <= 1'sb0;
			force_data_overwrite_q <= 1'sb0;
		end
		else if (ctrl_we) begin
			mode_q <= mode_d;
			key_len_q <= key_len_d;
			manual_start_trigger_q <= reg2hw[7];
			force_data_overwrite_q <= reg2hw[5];
		end
	end
	assign data_out_d = aes_transpose(add_round_key_out);
	always @(posedge clk_i or negedge rst_ni) begin : data_out_reg
		if (!rst_ni)
			data_out_q <= {128 {1'sb0}};
		else if (data_out_we)
			data_out_q <= data_out_d;
	end
	always @(*) begin : key_reg_put
		begin : sv2v_autoblock_421
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				hw2reg[279 + ((i * 32) + 31)-:32] = key_init_q[i * 32+:32];
		end
	end
	always @(*) begin : data_out_put
		begin : sv2v_autoblock_422
			reg signed [31:0] i;
			for (i = 0; i < 4; i = i + 1)
				hw2reg[19 + ((i * 32) + 31)-:32] = data_out_q[i * 32+:32];
		end
	end
	always @(*) hw2reg[18-:3] = key_len_q;
endmodule
module aes_sub_bytes (
	mode_i,
	data_i,
	data_o
);
	parameter _sv2v_width_SBoxImpl = 24;
	parameter [_sv2v_width_SBoxImpl - 1:0] SBoxImpl = "lut";
	input wire mode_i;
	input wire [127:0] data_i;
	output wire [127:0] data_o;
	generate
		genvar j;
		for (j = 0; j < 4; j = j + 1) begin : gen_sbox_j
			genvar i;
			for (i = 0; i < 4; i = i + 1) begin : gen_sbox_i
				aes_sbox #(
					._sv2v_width_SBoxImpl(_sv2v_width_SBoxImpl),
					.SBoxImpl(SBoxImpl)
				) aes_sbox_ij(
					.mode_i(mode_i),
					.data_i(data_i[((i * 4) + j) * 8+:8]),
					.data_o(data_o[((i * 4) + j) * 8+:8])
				);
			end
		end
	endgenerate
endmodule
module aes_sbox (
	mode_i,
	data_i,
	data_o
);
	parameter _sv2v_width_SBoxImpl = 24;
	parameter [_sv2v_width_SBoxImpl - 1:0] SBoxImpl = "lut";
	input wire mode_i;
	input wire [7:0] data_i;
	output wire [7:0] data_o;
	generate
		if (SBoxImpl == "lut") begin : gen_sbox_lut
			aes_sbox_lut aes_sbox(
				.mode_i(mode_i),
				.data_i(data_i),
				.data_o(data_o)
			);
		end
		else if (SBoxImpl == "canright") begin : gen_sbox_canright
			aes_sbox_canright aes_sbox(
				.mode_i(mode_i),
				.data_i(data_i),
				.data_o(data_o)
			);
		end
	endgenerate
endmodule
module aes_sbox_lut (
	mode_i,
	data_i,
	data_o
);
	input wire mode_i;
	input wire [7:0] data_i;
	output wire [7:0] data_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_435
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_436
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_437
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_438
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	wire [2047:0] sbox_enc = {8'h63, 8'h7c, 8'h77, 8'h7b, 8'hf2, 8'h6b, 8'h6f, 8'hc5, 8'h30, 8'h01, 8'h67, 8'h2b, 8'hfe, 8'hd7, 8'hab, 8'h76, 8'hca, 8'h82, 8'hc9, 8'h7d, 8'hfa, 8'h59, 8'h47, 8'hf0, 8'had, 8'hd4, 8'ha2, 8'haf, 8'h9c, 8'ha4, 8'h72, 8'hc0, 8'hb7, 8'hfd, 8'h93, 8'h26, 8'h36, 8'h3f, 8'hf7, 8'hcc, 8'h34, 8'ha5, 8'he5, 8'hf1, 8'h71, 8'hd8, 8'h31, 8'h15, 8'h04, 8'hc7, 8'h23, 8'hc3, 8'h18, 8'h96, 8'h05, 8'h9a, 8'h07, 8'h12, 8'h80, 8'he2, 8'heb, 8'h27, 8'hb2, 8'h75, 8'h09, 8'h83, 8'h2c, 8'h1a, 8'h1b, 8'h6e, 8'h5a, 8'ha0, 8'h52, 8'h3b, 8'hd6, 8'hb3, 8'h29, 8'he3, 8'h2f, 8'h84, 8'h53, 8'hd1, 8'h00, 8'hed, 8'h20, 8'hfc, 8'hb1, 8'h5b, 8'h6a, 8'hcb, 8'hbe, 8'h39, 8'h4a, 8'h4c, 8'h58, 8'hcf, 8'hd0, 8'hef, 8'haa, 8'hfb, 8'h43, 8'h4d, 8'h33, 8'h85, 8'h45, 8'hf9, 8'h02, 8'h7f, 8'h50, 8'h3c, 8'h9f, 8'ha8, 8'h51, 8'ha3, 8'h40, 8'h8f, 8'h92, 8'h9d, 8'h38, 8'hf5, 8'hbc, 8'hb6, 8'hda, 8'h21, 8'h10, 8'hff, 8'hf3, 8'hd2, 8'hcd, 8'h0c, 8'h13, 8'hec, 8'h5f, 8'h97, 8'h44, 8'h17, 8'hc4, 8'ha7, 8'h7e, 8'h3d, 8'h64, 8'h5d, 8'h19, 8'h73, 8'h60, 8'h81, 8'h4f, 8'hdc, 8'h22, 8'h2a, 8'h90, 8'h88, 8'h46, 8'hee, 8'hb8, 8'h14, 8'hde, 8'h5e, 8'h0b, 8'hdb, 8'he0, 8'h32, 8'h3a, 8'h0a, 8'h49, 8'h06, 8'h24, 8'h5c, 8'hc2, 8'hd3, 8'hac, 8'h62, 8'h91, 8'h95, 8'he4, 8'h79, 8'he7, 8'hc8, 8'h37, 8'h6d, 8'h8d, 8'hd5, 8'h4e, 8'ha9, 8'h6c, 8'h56, 8'hf4, 8'hea, 8'h65, 8'h7a, 8'hae, 8'h08, 8'hba, 8'h78, 8'h25, 8'h2e, 8'h1c, 8'ha6, 8'hb4, 8'hc6, 8'he8, 8'hdd, 8'h74, 8'h1f, 8'h4b, 8'hbd, 8'h8b, 8'h8a, 8'h70, 8'h3e, 8'hb5, 8'h66, 8'h48, 8'h03, 8'hf6, 8'h0e, 8'h61, 8'h35, 8'h57, 8'hb9, 8'h86, 8'hc1, 8'h1d, 8'h9e, 8'he1, 8'hf8, 8'h98, 8'h11, 8'h69, 8'hd9, 8'h8e, 8'h94, 8'h9b, 8'h1e, 8'h87, 8'he9, 8'hce, 8'h55, 8'h28, 8'hdf, 8'h8c, 8'ha1, 8'h89, 8'h0d, 8'hbf, 8'he6, 8'h42, 8'h68, 8'h41, 8'h99, 8'h2d, 8'h0f, 8'hb0, 8'h54, 8'hbb, 8'h16};
	wire [2047:0] sbox_dec = {8'h52, 8'h09, 8'h6a, 8'hd5, 8'h30, 8'h36, 8'ha5, 8'h38, 8'hbf, 8'h40, 8'ha3, 8'h9e, 8'h81, 8'hf3, 8'hd7, 8'hfb, 8'h7c, 8'he3, 8'h39, 8'h82, 8'h9b, 8'h2f, 8'hff, 8'h87, 8'h34, 8'h8e, 8'h43, 8'h44, 8'hc4, 8'hde, 8'he9, 8'hcb, 8'h54, 8'h7b, 8'h94, 8'h32, 8'ha6, 8'hc2, 8'h23, 8'h3d, 8'hee, 8'h4c, 8'h95, 8'h0b, 8'h42, 8'hfa, 8'hc3, 8'h4e, 8'h08, 8'h2e, 8'ha1, 8'h66, 8'h28, 8'hd9, 8'h24, 8'hb2, 8'h76, 8'h5b, 8'ha2, 8'h49, 8'h6d, 8'h8b, 8'hd1, 8'h25, 8'h72, 8'hf8, 8'hf6, 8'h64, 8'h86, 8'h68, 8'h98, 8'h16, 8'hd4, 8'ha4, 8'h5c, 8'hcc, 8'h5d, 8'h65, 8'hb6, 8'h92, 8'h6c, 8'h70, 8'h48, 8'h50, 8'hfd, 8'hed, 8'hb9, 8'hda, 8'h5e, 8'h15, 8'h46, 8'h57, 8'ha7, 8'h8d, 8'h9d, 8'h84, 8'h90, 8'hd8, 8'hab, 8'h00, 8'h8c, 8'hbc, 8'hd3, 8'h0a, 8'hf7, 8'he4, 8'h58, 8'h05, 8'hb8, 8'hb3, 8'h45, 8'h06, 8'hd0, 8'h2c, 8'h1e, 8'h8f, 8'hca, 8'h3f, 8'h0f, 8'h02, 8'hc1, 8'haf, 8'hbd, 8'h03, 8'h01, 8'h13, 8'h8a, 8'h6b, 8'h3a, 8'h91, 8'h11, 8'h41, 8'h4f, 8'h67, 8'hdc, 8'hea, 8'h97, 8'hf2, 8'hcf, 8'hce, 8'hf0, 8'hb4, 8'he6, 8'h73, 8'h96, 8'hac, 8'h74, 8'h22, 8'he7, 8'had, 8'h35, 8'h85, 8'he2, 8'hf9, 8'h37, 8'he8, 8'h1c, 8'h75, 8'hdf, 8'h6e, 8'h47, 8'hf1, 8'h1a, 8'h71, 8'h1d, 8'h29, 8'hc5, 8'h89, 8'h6f, 8'hb7, 8'h62, 8'h0e, 8'haa, 8'h18, 8'hbe, 8'h1b, 8'hfc, 8'h56, 8'h3e, 8'h4b, 8'hc6, 8'hd2, 8'h79, 8'h20, 8'h9a, 8'hdb, 8'hc0, 8'hfe, 8'h78, 8'hcd, 8'h5a, 8'hf4, 8'h1f, 8'hdd, 8'ha8, 8'h33, 8'h88, 8'h07, 8'hc7, 8'h31, 8'hb1, 8'h12, 8'h10, 8'h59, 8'h27, 8'h80, 8'hec, 8'h5f, 8'h60, 8'h51, 8'h7f, 8'ha9, 8'h19, 8'hb5, 8'h4a, 8'h0d, 8'h2d, 8'he5, 8'h7a, 8'h9f, 8'h93, 8'hc9, 8'h9c, 8'hef, 8'ha0, 8'he0, 8'h3b, 8'h4d, 8'hae, 8'h2a, 8'hf5, 8'hb0, 8'hc8, 8'heb, 8'hbb, 8'h3c, 8'h83, 8'h53, 8'h99, 8'h61, 8'h17, 8'h2b, 8'h04, 8'h7e, 8'hba, 8'h77, 8'hd6, 8'h26, 8'he1, 8'h69, 8'h14, 8'h63, 8'h55, 8'h21, 8'h0c, 8'h7d};
	assign data_o = (mode_i == AES_ENC ? sbox_enc[(255 - data_i) * 8+:8] : sbox_dec[(255 - data_i) * 8+:8]);
endmodule
module aes_sbox_canright (
	mode_i,
	data_i,
	data_o
);
	input wire mode_i;
	input wire [7:0] data_i;
	output wire [7:0] data_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_443
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_444
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_445
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_446
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	function automatic [1:0] aes_mul_gf2p2;
		input reg [1:0] g;
		input reg [1:0] d;
		reg [1:0] f;
		reg a;
		reg b;
		reg c;
		begin
			a = g[1] & d[1];
			b = ^g & ^d;
			c = g[0] & d[0];
			f[1] = a ^ b;
			f[0] = c ^ b;
			aes_mul_gf2p2 = f;
		end
	endfunction
	function automatic [1:0] aes_scale_omega2_gf2p2;
		input reg [1:0] g;
		reg [1:0] d;
		begin
			d[1] = g[0];
			d[0] = g[1] ^ g[0];
			aes_scale_omega2_gf2p2 = d;
		end
	endfunction
	function automatic [1:0] aes_scale_omega_gf2p2;
		input reg [1:0] g;
		reg [1:0] d;
		begin
			d[1] = g[1] ^ g[0];
			d[0] = g[1];
			aes_scale_omega_gf2p2 = d;
		end
	endfunction
	function automatic [1:0] aes_square_gf2p2;
		input reg [1:0] g;
		reg [1:0] d;
		begin
			d[1] = g[0];
			d[0] = g[1];
			aes_square_gf2p2 = d;
		end
	endfunction
	function automatic [3:0] aes_mul_gf2p4;
		input reg [3:0] gamma;
		input reg [3:0] delta;
		reg [3:0] theta;
		reg [1:0] a;
		reg [1:0] b;
		reg [1:0] c;
		begin
			a = aes_mul_gf2p2(gamma[3:2], delta[3:2]);
			b = aes_mul_gf2p2(gamma[3:2] ^ gamma[1:0], delta[3:2] ^ delta[1:0]);
			c = aes_mul_gf2p2(gamma[1:0], delta[1:0]);
			theta[3:2] = a ^ aes_scale_omega2_gf2p2(b);
			theta[1:0] = c ^ aes_scale_omega2_gf2p2(b);
			aes_mul_gf2p4 = theta;
		end
	endfunction
	function automatic [3:0] aes_square_scale_gf2p4_gf2p2;
		input reg [3:0] gamma;
		reg [3:0] delta;
		reg [1:0] a;
		reg [1:0] b;
		begin
			a = gamma[3:2] ^ gamma[1:0];
			b = aes_square_gf2p2(gamma[1:0]);
			delta[3:2] = aes_square_gf2p2(a);
			delta[1:0] = aes_scale_omega_gf2p2(b);
			aes_square_scale_gf2p4_gf2p2 = delta;
		end
	endfunction
	function automatic [3:0] aes_inverse_gf2p4;
		input reg [3:0] gamma;
		reg [3:0] delta;
		reg [1:0] a;
		reg [1:0] b;
		reg [1:0] c;
		reg [1:0] d;
		begin
			a = gamma[3:2] ^ gamma[1:0];
			b = aes_mul_gf2p2(gamma[3:2], gamma[1:0]);
			c = aes_scale_omega2_gf2p2(aes_square_gf2p2(a));
			d = aes_square_gf2p2(c ^ b);
			delta[3:2] = aes_mul_gf2p2(d, gamma[1:0]);
			delta[1:0] = aes_mul_gf2p2(d, gamma[3:2]);
			aes_inverse_gf2p4 = delta;
		end
	endfunction
	function automatic [7:0] aes_inverse_gf2p8;
		input reg [7:0] gamma;
		reg [7:0] delta;
		reg [3:0] a;
		reg [3:0] b;
		reg [3:0] c;
		reg [3:0] d;
		begin
			a = gamma[7:4] ^ gamma[3:0];
			b = aes_mul_gf2p4(gamma[7:4], gamma[3:0]);
			c = aes_square_scale_gf2p4_gf2p2(a);
			d = aes_inverse_gf2p4(c ^ b);
			delta[7:4] = aes_mul_gf2p4(d, gamma[3:0]);
			delta[3:0] = aes_mul_gf2p4(d, gamma[7:4]);
			aes_inverse_gf2p8 = delta;
		end
	endfunction
	wire [63:0] a2x = {8'h98, 8'hf3, 8'hf2, 8'h48, 8'h09, 8'h81, 8'ha9, 8'hff};
	wire [63:0] x2a = {8'h64, 8'h78, 8'h6e, 8'h8c, 8'h68, 8'h29, 8'hde, 8'h60};
	wire [63:0] x2s = {8'h58, 8'h2d, 8'h9e, 8'h0b, 8'hdc, 8'h04, 8'h03, 8'h24};
	wire [63:0] s2x = {8'h8c, 8'h79, 8'h05, 8'heb, 8'h12, 8'h04, 8'h51, 8'h53};
	wire [7:0] data_basis_x;
	wire [7:0] data_inverse;
	assign data_basis_x = (mode_i == AES_ENC ? aes_mvm(data_i, a2x) : aes_mvm(data_i ^ 8'h63, s2x));
	assign data_inverse = aes_inverse_gf2p8(data_basis_x);
	assign data_o = (mode_i == AES_ENC ? aes_mvm(data_inverse, x2s) ^ 8'h63 : aes_mvm(data_inverse, x2a));
endmodule
module aes_shift_rows (
	mode_i,
	data_i,
	data_o
);
	input wire mode_i;
	input wire [127:0] data_i;
	output wire [127:0] data_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_451
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_452
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_453
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_454
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	assign data_o[0+:32] = data_i[0+:32];
	assign data_o[64+:32] = aes_circ_byte_shift(data_i[64+:32], 2);
	assign data_o[32+:32] = (mode_i == AES_ENC ? aes_circ_byte_shift(data_i[32+:32], -1) : aes_circ_byte_shift(data_i[32+:32], 1));
	assign data_o[96+:32] = (mode_i == AES_ENC ? aes_circ_byte_shift(data_i[96+:32], 1) : aes_circ_byte_shift(data_i[96+:32], -1));
endmodule
module aes_mix_columns (
	mode_i,
	data_i,
	data_o
);
	input wire mode_i;
	input wire [127:0] data_i;
	output wire [127:0] data_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_459
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_460
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_461
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_462
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	wire [127:0] data_i_transposed;
	wire [127:0] data_o_transposed;
	assign data_i_transposed = aes_transpose(data_i);
	generate
		genvar i;
		for (i = 0; i < 4; i = i + 1) begin : gen_mix_column
			aes_mix_single_column aes_mix_column_i(
				.mode_i(mode_i),
				.data_i(data_i_transposed[8 * (i * 4)+:32]),
				.data_o(data_o_transposed[8 * (i * 4)+:32])
			);
		end
	endgenerate
	assign data_o = aes_transpose(data_o_transposed);
endmodule
module aes_mix_single_column (
	mode_i,
	data_i,
	data_o
);
	input wire mode_i;
	input wire [31:0] data_i;
	output wire [31:0] data_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_467
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_468
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_469
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_470
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	wire [31:0] x;
	wire [15:0] y;
	wire [15:0] z;
	wire [31:0] x_mul2;
	wire [15:0] y_pre_mul4;
	wire [7:0] y2;
	wire [7:0] y2_pre_mul2;
	wire [15:0] z_muxed;
	assign x[0+:8] = data_i[0+:8] ^ data_i[24+:8];
	assign x[8+:8] = data_i[24+:8] ^ data_i[16+:8];
	assign x[16+:8] = data_i[16+:8] ^ data_i[8+:8];
	assign x[24+:8] = data_i[8+:8] ^ data_i[0+:8];
	generate
		genvar i;
		for (i = 0; i < 4; i = i + 1) begin : gen_x_mul2
			assign x_mul2[i * 8+:8] = aes_mul2(x[i * 8+:8]);
		end
	endgenerate
	assign y_pre_mul4[0+:8] = data_i[24+:8] ^ data_i[8+:8];
	assign y_pre_mul4[8+:8] = data_i[16+:8] ^ data_i[0+:8];
	generate
		for (i = 0; i < 2; i = i + 1) begin : gen_mul4
			assign y[i * 8+:8] = aes_mul4(y_pre_mul4[i * 8+:8]);
		end
	endgenerate
	assign y2_pre_mul2 = y[0+:8] ^ y[8+:8];
	assign y2 = aes_mul2(y2_pre_mul2);
	assign z[0+:8] = y2 ^ y[0+:8];
	assign z[8+:8] = y2 ^ y[8+:8];
	assign z_muxed[0+:8] = (mode_i == AES_ENC ? 8'b00000000 : z[0+:8]);
	assign z_muxed[8+:8] = (mode_i == AES_ENC ? 8'b00000000 : z[8+:8]);
	assign data_o[0+:8] = ((data_i[8+:8] ^ x_mul2[24+:8]) ^ x[8+:8]) ^ z_muxed[8+:8];
	assign data_o[8+:8] = ((data_i[0+:8] ^ x_mul2[16+:8]) ^ x[8+:8]) ^ z_muxed[0+:8];
	assign data_o[16+:8] = ((data_i[24+:8] ^ x_mul2[8+:8]) ^ x[24+:8]) ^ z_muxed[8+:8];
	assign data_o[24+:8] = ((data_i[16+:8] ^ x_mul2[0+:8]) ^ x[24+:8]) ^ z_muxed[0+:8];
endmodule
module aes_key_expand (
	clk_i,
	rst_ni,
	mode_i,
	step_i,
	clear_i,
	round_i,
	key_len_i,
	key_i,
	key_o
);
	parameter [0:0] AES192Enable = 1;
	parameter _sv2v_width_SBoxImpl = 24;
	parameter [_sv2v_width_SBoxImpl - 1:0] SBoxImpl = "lut";
	input wire clk_i;
	input wire rst_ni;
	input wire mode_i;
	input wire step_i;
	input wire clear_i;
	input wire [3:0] round_i;
	input wire [2:0] key_len_i;
	input wire [255:0] key_i;
	output wire [255:0] key_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_475
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_476
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_477
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_478
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	reg [7:0] rcon_d;
	reg [7:0] rcon_q;
	wire rcon_we;
	reg use_rcon;
	wire [3:0] rnd;
	reg [3:0] rnd_type;
	wire [31:0] spec_in_128;
	wire [31:0] spec_in_192;
	reg [31:0] rot_word_in;
	wire [31:0] rot_word_out;
	wire use_rot_word;
	wire [31:0] sub_word_in;
	wire [31:0] sub_word_out;
	wire [7:0] rcon_add_in;
	wire [7:0] rcon_add_out;
	wire [31:0] rcon_added;
	wire [31:0] irregular;
	reg [255:0] regular;
	assign rnd = round_i;
	always @(*) begin : get_rnd_type
		if (AES192Enable) begin
			rnd_type[0] = rnd == 0;
			rnd_type[1] = (((rnd == 1) || (rnd == 4)) || (rnd == 7)) || (rnd == 10);
			rnd_type[2] = (((rnd == 2) || (rnd == 5)) || (rnd == 8)) || (rnd == 11);
			rnd_type[3] = (((rnd == 3) || (rnd == 6)) || (rnd == 9)) || (rnd == 12);
		end
		else
			rnd_type = {4 {1'sb0}};
	end
	assign use_rot_word = ((key_len_i == AES_256) && (rnd[0] == 1'b0) ? 1'b0 : 1'b1);
	always @(*) begin : rcon_usage
		use_rcon = 1'b1;
		if (AES192Enable)
			if ((key_len_i == AES_192) && (((mode_i == AES_ENC) && rnd_type[1]) || ((mode_i == AES_DEC) && (rnd_type[0] || rnd_type[3]))))
				use_rcon = 1'b0;
		if ((key_len_i == AES_256) && (rnd[0] == 1'b0))
			use_rcon = 1'b0;
	end
	always @(*) begin : rcon_update
		rcon_d = rcon_q;
		if (clear_i)
			rcon_d = (mode_i == AES_ENC ? 8'h01 : ((mode_i == AES_DEC) && (key_len_i == AES_128) ? 8'h36 : ((mode_i == AES_DEC) && (key_len_i == AES_192) ? 8'h80 : ((mode_i == AES_DEC) && (key_len_i == AES_256) ? 8'h40 : 8'h01))));
		else
			rcon_d = (mode_i == AES_ENC ? aes_mul2(rcon_q) : (mode_i == AES_DEC ? aes_div2(rcon_q) : 8'h01));
	end
	assign rcon_we = clear_i | (step_i & use_rcon);
	always @(posedge clk_i or negedge rst_ni) begin : reg_rcon
		if (!rst_ni)
			rcon_q <= {8 {1'sb0}};
		else if (rcon_we)
			rcon_q <= rcon_d;
	end
	assign spec_in_128 = key_i[96+:32] ^ key_i[64+:32];
	assign spec_in_192 = (AES192Enable ? (key_i[160+:32] ^ key_i[32+:32]) ^ key_i[0+:32] : {32 {1'sb0}});
	always @(*) begin : rot_word_in_mux
		case (key_len_i)
			AES_128:
				case (mode_i)
					AES_ENC: rot_word_in = key_i[96+:32];
					AES_DEC: rot_word_in = spec_in_128;
					default: rot_word_in = key_i[96+:32];
				endcase
			AES_192:
				if (AES192Enable)
					case (mode_i)
						AES_ENC: rot_word_in = (rnd_type[0] ? key_i[160+:32] : (rnd_type[2] ? key_i[160+:32] : (rnd_type[3] ? spec_in_192 : key_i[96+:32])));
						AES_DEC: rot_word_in = (rnd_type[1] ? key_i[96+:32] : (rnd_type[2] ? key_i[32+:32] : key_i[96+:32]));
						default: rot_word_in = key_i[96+:32];
					endcase
				else
					rot_word_in = key_i[96+:32];
			AES_256:
				case (mode_i)
					AES_ENC: rot_word_in = key_i[224+:32];
					AES_DEC: rot_word_in = key_i[96+:32];
					default: rot_word_in = key_i[224+:32];
				endcase
			default: rot_word_in = key_i[96+:32];
		endcase
	end
	assign rot_word_out = aes_circ_byte_shift(rot_word_in, 3);
	assign sub_word_in = (use_rot_word ? rot_word_out : rot_word_in);
	generate
		genvar i;
		for (i = 0; i < 4; i = i + 1) begin : gen_sbox
			aes_sbox #(
				._sv2v_width_SBoxImpl(_sv2v_width_SBoxImpl),
				.SBoxImpl(SBoxImpl)
			) aes_sbox_i(
				.mode_i(AES_ENC),
				.data_i(sub_word_in[8 * i+:8]),
				.data_o(sub_word_out[8 * i+:8])
			);
		end
	endgenerate
	assign rcon_add_in = sub_word_out[7:0];
	assign rcon_add_out = rcon_add_in ^ rcon_q;
	assign rcon_added = {sub_word_out[31:8], rcon_add_out};
	assign irregular = (use_rcon ? rcon_added : sub_word_out);
	always @(*) begin : drive_regular
		case (key_len_i)
			AES_128: begin
				regular[128+:128] = key_i[0+:128];
				regular[0+:32] = irregular ^ key_i[0+:32];
				case (mode_i)
					AES_ENC: begin : sv2v_autoblock_479
						reg signed [31:0] i;
						for (i = 1; i < 4; i = i + 1)
							regular[i * 32+:32] = regular[(i - 1) * 32+:32] ^ key_i[i * 32+:32];
					end
					AES_DEC: begin : sv2v_autoblock_480
						reg signed [31:0] i;
						for (i = 1; i < 4; i = i + 1)
							regular[i * 32+:32] = key_i[(i - 1) * 32+:32] ^ key_i[i * 32+:32];
					end
					default: regular = {key_i[0+:128], key_i[128+:128]};
				endcase
			end
			AES_192: begin
				regular[192+:64] = key_i[64+:64];
				if (AES192Enable)
					case (mode_i)
						AES_ENC:
							if (rnd_type[0]) begin
								regular[0+:128] = key_i[64+:128];
								regular[128+:32] = irregular ^ key_i[0+:32];
								regular[160+:32] = regular[128+:32] ^ key_i[32+:32];
							end
							else begin
								regular[0+:64] = key_i[128+:64];
								begin : sv2v_autoblock_481
									reg signed [31:0] i;
									for (i = 0; i < 4; i = i + 1)
										if (((i == 0) && rnd_type[2]) || ((i == 2) && rnd_type[3]))
											regular[(i + 2) * 32+:32] = irregular ^ key_i[i * 32+:32];
										else
											regular[(i + 2) * 32+:32] = regular[(i + 1) * 32+:32] ^ key_i[i * 32+:32];
								end
							end
						AES_DEC:
							if (rnd_type[0]) begin
								regular[64+:128] = key_i[0+:128];
								begin : sv2v_autoblock_482
									reg signed [31:0] i;
									for (i = 0; i < 2; i = i + 1)
										regular[i * 32+:32] = key_i[(3 + i) * 32+:32] ^ key_i[((3 + i) + 1) * 32+:32];
								end
							end
							else begin
								regular[128+:64] = key_i[0+:64];
								begin : sv2v_autoblock_483
									reg signed [31:0] i;
									for (i = 0; i < 4; i = i + 1)
										if (((i == 2) && rnd_type[1]) || ((i == 0) && rnd_type[2]))
											regular[i * 32+:32] = irregular ^ key_i[(i + 2) * 32+:32];
										else
											regular[i * 32+:32] = key_i[(i + 1) * 32+:32] ^ key_i[(i + 2) * 32+:32];
								end
							end
						default: regular = {key_i[0+:128], key_i[128+:128]};
					endcase
				else
					regular = {key_i[0+:128], key_i[128+:128]};
			end
			AES_256:
				case (mode_i)
					AES_ENC:
						if (rnd == 0)
							regular = {key_i[0+:128], key_i[128+:128]};
						else begin
							regular[0+:128] = key_i[128+:128];
							regular[128+:32] = irregular ^ key_i[0+:32];
							begin : sv2v_autoblock_484
								reg signed [31:0] i;
								for (i = 1; i < 4; i = i + 1)
									regular[(i + 4) * 32+:32] = regular[(i + 3) * 32+:32] ^ key_i[i * 32+:32];
							end
						end
					AES_DEC:
						if (rnd == 0)
							regular = {key_i[0+:128], key_i[128+:128]};
						else begin
							regular[128+:128] = key_i[0+:128];
							regular[0+:32] = irregular ^ key_i[128+:32];
							begin : sv2v_autoblock_485
								reg signed [31:0] i;
								for (i = 0; i < 3; i = i + 1)
									regular[(i + 1) * 32+:32] = key_i[(4 + i) * 32+:32] ^ key_i[((4 + i) + 1) * 32+:32];
							end
						end
					default: regular = {key_i[0+:128], key_i[128+:128]};
				endcase
			default: regular = {key_i[0+:128], key_i[128+:128]};
		endcase
	end
	assign key_o = regular;
endmodule
module aes_control (
	clk_i,
	rst_ni,
	mode_i,
	key_len_i,
	manual_start_trigger_i,
	force_data_overwrite_i,
	start_i,
	key_clear_i,
	data_in_clear_i,
	data_out_clear_i,
	data_in_qe_i,
	key_init_qe_i,
	data_out_re_i,
	state_sel_o,
	state_we_o,
	add_rk_sel_o,
	key_expand_mode_o,
	key_init_sel_o,
	key_init_we_o,
	key_full_sel_o,
	key_full_we_o,
	key_dec_sel_o,
	key_dec_we_o,
	key_expand_step_o,
	key_expand_clear_o,
	key_expand_round_o,
	key_words_sel_o,
	round_key_sel_o,
	data_in_we_o,
	data_out_we_o,
	start_o,
	start_we_o,
	key_clear_o,
	key_clear_we_o,
	data_in_clear_o,
	data_in_clear_we_o,
	data_out_clear_o,
	data_out_clear_we_o,
	output_valid_o,
	output_valid_we_o,
	input_ready_o,
	input_ready_we_o,
	idle_o,
	idle_we_o,
	stall_o,
	stall_we_o
);
	input wire clk_i;
	input wire rst_ni;
	input wire mode_i;
	input wire [2:0] key_len_i;
	input wire manual_start_trigger_i;
	input wire force_data_overwrite_i;
	input wire start_i;
	input wire key_clear_i;
	input wire data_in_clear_i;
	input wire data_out_clear_i;
	input wire [3:0] data_in_qe_i;
	input wire [7:0] key_init_qe_i;
	input wire [3:0] data_out_re_i;
	output reg [1:0] state_sel_o;
	output reg state_we_o;
	output reg [1:0] add_rk_sel_o;
	output wire key_expand_mode_o;
	output reg key_init_sel_o;
	output reg [7:0] key_init_we_o;
	output reg [1:0] key_full_sel_o;
	output reg key_full_we_o;
	output reg key_dec_sel_o;
	output reg key_dec_we_o;
	output reg key_expand_step_o;
	output reg key_expand_clear_o;
	output wire [3:0] key_expand_round_o;
	output reg [1:0] key_words_sel_o;
	output reg round_key_sel_o;
	output reg data_in_we_o;
	output reg data_out_we_o;
	output wire start_o;
	output reg start_we_o;
	output wire key_clear_o;
	output reg key_clear_we_o;
	output wire data_in_clear_o;
	output reg data_in_clear_we_o;
	output wire data_out_clear_o;
	output reg data_out_clear_we_o;
	output wire output_valid_o;
	output wire output_valid_we_o;
	output wire input_ready_o;
	output wire input_ready_we_o;
	output reg idle_o;
	output reg idle_we_o;
	output reg stall_o;
	output reg stall_we_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_490
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_491
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_492
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_493
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	reg [2:0] aes_ctrl_ns;
	reg [2:0] aes_ctrl_cs;
	wire [3:0] data_in_new_d;
	reg [3:0] data_in_new_q;
	wire data_in_new;
	reg data_in_load;
	wire key_init_clear;
	wire [7:0] key_init_new_d;
	reg [7:0] key_init_new_q;
	wire key_init_new;
	reg dec_key_gen;
	wire [3:0] data_out_read_d;
	reg [3:0] data_out_read_q;
	wire data_out_read;
	reg output_valid_q;
	reg [3:0] round_d;
	reg [3:0] round_q;
	reg [3:0] num_rounds_d;
	reg [3:0] num_rounds_q;
	wire [3:0] num_rounds_regular;
	reg dec_key_gen_d;
	reg dec_key_gen_q;
	wire start;
	wire finish;
	assign start = (manual_start_trigger_i ? start_i : data_in_new);
	assign finish = (force_data_overwrite_i ? 1'b1 : ~output_valid_q);
	localparam [2:0] CLEAR = 4;
	localparam [2:0] FINISH = 3;
	localparam [2:0] IDLE = 0;
	localparam [2:0] INIT = 1;
	localparam [2:0] ROUND = 2;
	always @(*) begin : aes_ctrl_fsm
		state_sel_o = STATE_ROUND;
		state_we_o = 1'b0;
		add_rk_sel_o = ADD_RK_ROUND;
		key_init_sel_o = KEY_INIT_INPUT;
		key_init_we_o = 8'h00;
		key_full_sel_o = KEY_FULL_ROUND;
		key_full_we_o = 1'b0;
		key_dec_sel_o = KEY_DEC_EXPAND;
		key_dec_we_o = 1'b0;
		key_expand_step_o = 1'b0;
		key_expand_clear_o = 1'b0;
		key_words_sel_o = KEY_WORDS_ZERO;
		round_key_sel_o = ROUND_KEY_DIRECT;
		start_we_o = 1'b0;
		key_clear_we_o = 1'b0;
		data_in_clear_we_o = 1'b0;
		data_out_clear_we_o = 1'b0;
		idle_o = 1'b0;
		idle_we_o = 1'b0;
		stall_o = 1'b0;
		stall_we_o = 1'b0;
		dec_key_gen = 1'b0;
		data_in_load = 1'b0;
		data_in_we_o = 1'b0;
		data_out_we_o = 1'b0;
		aes_ctrl_ns = aes_ctrl_cs;
		round_d = round_q;
		num_rounds_d = num_rounds_q;
		dec_key_gen_d = dec_key_gen_q;
		case (aes_ctrl_cs)
			IDLE: begin
				idle_o = 1'b1;
				idle_we_o = 1'b1;
				stall_o = 1'b0;
				stall_we_o = 1'b1;
				dec_key_gen_d = 1'b0;
				if (start) begin
					dec_key_gen_d = key_init_new & (mode_i == AES_DEC);
					state_sel_o = (dec_key_gen_d ? STATE_CLEAR : STATE_INIT);
					state_we_o = 1'b1;
					key_expand_clear_o = 1'b1;
					key_full_sel_o = (dec_key_gen_d ? KEY_FULL_ENC_INIT : (mode_i == AES_ENC ? KEY_FULL_ENC_INIT : KEY_FULL_DEC_INIT));
					key_full_we_o = 1'b1;
					round_d = {4 {1'sb0}};
					num_rounds_d = (key_len_i == AES_128 ? 4'd10 : (key_len_i == AES_192 ? 4'd12 : 4'd14));
					idle_o = 1'b0;
					idle_we_o = 1'b1;
					start_we_o = 1'b1;
					aes_ctrl_ns = INIT;
				end
				else if ((key_clear_i || data_in_clear_i) || data_out_clear_i) begin
					idle_o = 1'b0;
					idle_we_o = 1'b1;
					aes_ctrl_ns = CLEAR;
				end
				key_init_we_o = (idle_o ? key_init_qe_i : 8'h00);
			end
			INIT: begin
				state_we_o = ~dec_key_gen_q;
				add_rk_sel_o = ADD_RK_INIT;
				key_words_sel_o = (dec_key_gen_q ? KEY_WORDS_ZERO : (key_len_i == AES_128 ? KEY_WORDS_0123 : ((key_len_i == AES_192) && (mode_i == AES_ENC) ? KEY_WORDS_0123 : ((key_len_i == AES_192) && (mode_i == AES_DEC) ? KEY_WORDS_2345 : ((key_len_i == AES_256) && (mode_i == AES_ENC) ? KEY_WORDS_0123 : ((key_len_i == AES_256) && (mode_i == AES_DEC) ? KEY_WORDS_4567 : KEY_WORDS_ZERO))))));
				if (key_len_i != AES_256) begin
					key_expand_step_o = 1'b1;
					key_full_we_o = 1'b1;
				end
				data_in_load = ~dec_key_gen_q;
				dec_key_gen = dec_key_gen_q;
				aes_ctrl_ns = ROUND;
			end
			ROUND: begin
				state_we_o = ~dec_key_gen_q;
				key_words_sel_o = (dec_key_gen_q ? KEY_WORDS_ZERO : (key_len_i == AES_128 ? KEY_WORDS_0123 : ((key_len_i == AES_192) && (mode_i == AES_ENC) ? KEY_WORDS_2345 : ((key_len_i == AES_192) && (mode_i == AES_DEC) ? KEY_WORDS_0123 : ((key_len_i == AES_256) && (mode_i == AES_ENC) ? KEY_WORDS_4567 : ((key_len_i == AES_256) && (mode_i == AES_DEC) ? KEY_WORDS_0123 : KEY_WORDS_ZERO))))));
				key_expand_step_o = 1'b1;
				key_full_we_o = 1'b1;
				round_key_sel_o = (mode_i == AES_ENC ? ROUND_KEY_DIRECT : ROUND_KEY_MIXED);
				round_d = round_q + 1;
				if (round_q == num_rounds_regular)
					if (dec_key_gen_q) begin
						key_dec_we_o = 1'b1;
						dec_key_gen_d = 1'b0;
						aes_ctrl_ns = IDLE;
					end
					else
						aes_ctrl_ns = FINISH;
			end
			FINISH: begin
				key_words_sel_o = (dec_key_gen_q ? KEY_WORDS_ZERO : (key_len_i == AES_128 ? KEY_WORDS_0123 : ((key_len_i == AES_192) && (mode_i == AES_ENC) ? KEY_WORDS_2345 : ((key_len_i == AES_192) && (mode_i == AES_DEC) ? KEY_WORDS_0123 : ((key_len_i == AES_256) && (mode_i == AES_ENC) ? KEY_WORDS_4567 : ((key_len_i == AES_256) && (mode_i == AES_DEC) ? KEY_WORDS_0123 : KEY_WORDS_ZERO))))));
				add_rk_sel_o = ADD_RK_FINAL;
				if (!finish) begin
					stall_o = 1'b1;
					stall_we_o = 1'b1;
				end
				else begin
					stall_o = 1'b0;
					stall_we_o = 1'b1;
					data_out_we_o = 1'b1;
					aes_ctrl_ns = IDLE;
					state_we_o = 1'b1;
					state_sel_o = STATE_CLEAR;
				end
			end
			CLEAR: begin
				if (key_clear_i) begin
					key_init_sel_o = KEY_INIT_CLEAR;
					key_init_we_o = 8'hff;
					key_full_sel_o = KEY_FULL_CLEAR;
					key_full_we_o = 1'b1;
					key_dec_sel_o = KEY_DEC_CLEAR;
					key_dec_we_o = 1'b1;
					key_clear_we_o = 1'b1;
				end
				if (data_in_clear_i) begin
					data_in_we_o = 1'b1;
					data_in_clear_we_o = 1'b1;
				end
				if (data_out_clear_i) begin
					add_rk_sel_o = ADD_RK_INIT;
					key_words_sel_o = KEY_WORDS_ZERO;
					round_key_sel_o = ROUND_KEY_DIRECT;
					data_out_we_o = 1'b1;
					data_out_clear_we_o = 1'b1;
				end
				aes_ctrl_ns = IDLE;
			end
			default: aes_ctrl_ns = IDLE;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni) begin : reg_fsm
		if (!rst_ni) begin
			aes_ctrl_cs <= IDLE;
			round_q <= {4 {1'sb0}};
			num_rounds_q <= {4 {1'sb0}};
			dec_key_gen_q <= 1'b0;
		end
		else begin
			aes_ctrl_cs <= aes_ctrl_ns;
			round_q <= round_d;
			num_rounds_q <= num_rounds_d;
			dec_key_gen_q <= dec_key_gen_d;
		end
	end
	assign num_rounds_regular = num_rounds_q - 4'd2;
	assign key_init_clear = (key_init_sel_o == KEY_INIT_CLEAR) & &key_init_we_o;
	assign key_init_new_d = (dec_key_gen | key_init_clear ? {8 {1'sb0}} : key_init_new_q | key_init_qe_i);
	assign key_init_new = &key_init_new_d;
	assign data_in_new_d = (data_in_load | data_in_we_o ? {4 {1'sb0}} : data_in_new_q | data_in_qe_i);
	assign data_in_new = &data_in_new_d;
	assign data_out_read_d = (data_out_we_o ? {4 {1'sb0}} : data_out_read_q | data_out_re_i);
	assign data_out_read = &data_out_read_d;
	always @(posedge clk_i or negedge rst_ni) begin : reg_edge_detection
		if (!rst_ni) begin
			key_init_new_q <= {8 {1'sb0}};
			data_in_new_q <= {4 {1'sb0}};
			data_out_read_q <= {4 {1'sb0}};
		end
		else begin
			key_init_new_q <= key_init_new_d;
			data_in_new_q <= data_in_new_d;
			data_out_read_q <= data_out_read_d;
		end
	end
	assign output_valid_o = data_out_we_o & ~data_out_clear_we_o;
	assign output_valid_we_o = (data_out_we_o | data_out_read) | data_out_clear_we_o;
	always @(posedge clk_i or negedge rst_ni) begin : reg_output_valid
		if (!rst_ni)
			output_valid_q <= 1'sb0;
		else if (output_valid_we_o)
			output_valid_q <= output_valid_o;
	end
	assign input_ready_o = ~data_in_new;
	assign input_ready_we_o = (data_in_new | data_in_load) | data_in_we_o;
	assign key_expand_mode_o = (dec_key_gen_d || dec_key_gen_q ? AES_ENC : mode_i);
	assign key_expand_round_o = round_d;
	assign start_o = 1'b0;
	assign key_clear_o = 1'b0;
	assign data_in_clear_o = 1'b0;
	assign data_out_clear_o = 1'b0;
endmodule
module aes (
	clk_i,
	rst_ni,
	tl_i,
	tl_o
);
	parameter [0:0] AES192Enable = 1;
	parameter _sv2v_width_SBoxImpl = 24;
	parameter [_sv2v_width_SBoxImpl - 1:0] SBoxImpl = "lut";
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	localparam signed [31:0] NumRegsKey = 8;
	localparam signed [31:0] NumRegsData = 4;
	localparam [6:0] AES_KEY0_OFFSET = 7'h00;
	localparam [6:0] AES_KEY1_OFFSET = 7'h04;
	localparam [6:0] AES_KEY2_OFFSET = 7'h08;
	localparam [6:0] AES_KEY3_OFFSET = 7'h0c;
	localparam [6:0] AES_KEY4_OFFSET = 7'h10;
	localparam [6:0] AES_KEY5_OFFSET = 7'h14;
	localparam [6:0] AES_KEY6_OFFSET = 7'h18;
	localparam [6:0] AES_KEY7_OFFSET = 7'h1c;
	localparam [6:0] AES_DATA_IN0_OFFSET = 7'h20;
	localparam [6:0] AES_DATA_IN1_OFFSET = 7'h24;
	localparam [6:0] AES_DATA_IN2_OFFSET = 7'h28;
	localparam [6:0] AES_DATA_IN3_OFFSET = 7'h2c;
	localparam [6:0] AES_DATA_OUT0_OFFSET = 7'h30;
	localparam [6:0] AES_DATA_OUT1_OFFSET = 7'h34;
	localparam [6:0] AES_DATA_OUT2_OFFSET = 7'h38;
	localparam [6:0] AES_DATA_OUT3_OFFSET = 7'h3c;
	localparam [6:0] AES_CTRL_OFFSET = 7'h40;
	localparam [6:0] AES_TRIGGER_OFFSET = 7'h44;
	localparam [6:0] AES_STATUS_OFFSET = 7'h48;
	localparam signed [31:0] AES_KEY0 = 0;
	localparam signed [31:0] AES_KEY1 = 1;
	localparam signed [31:0] AES_KEY2 = 2;
	localparam signed [31:0] AES_KEY3 = 3;
	localparam signed [31:0] AES_KEY4 = 4;
	localparam signed [31:0] AES_KEY5 = 5;
	localparam signed [31:0] AES_KEY6 = 6;
	localparam signed [31:0] AES_KEY7 = 7;
	localparam signed [31:0] AES_DATA_IN0 = 8;
	localparam signed [31:0] AES_DATA_IN1 = 9;
	localparam signed [31:0] AES_DATA_IN2 = 10;
	localparam signed [31:0] AES_DATA_IN3 = 11;
	localparam signed [31:0] AES_DATA_OUT0 = 12;
	localparam signed [31:0] AES_DATA_OUT1 = 13;
	localparam signed [31:0] AES_DATA_OUT2 = 14;
	localparam signed [31:0] AES_DATA_OUT3 = 15;
	localparam signed [31:0] AES_CTRL = 16;
	localparam signed [31:0] AES_TRIGGER = 17;
	localparam signed [31:0] AES_STATUS = 18;
	localparam [75:0] AES_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	wire [541:0] reg2hw;
	wire [534:0] hw2reg;
	aes_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	aes_core #(
		.AES192Enable(AES192Enable),
		._sv2v_width_SBoxImpl(_sv2v_width_SBoxImpl),
		.SBoxImpl(SBoxImpl)
	) aes_core(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg)
	);
endmodule
module sha2 (
	clk_i,
	rst_ni,
	wipe_secret,
	wipe_v,
	fifo_rvalid,
	fifo_rdata,
	fifo_rready,
	sha_en,
	hash_start,
	hash_process,
	hash_done,
	message_length,
	digest
);
	localparam signed [31:0] NumAlerts = 1;
	localparam [NumAlerts - 1:0] AlertAsyncOn = 1'b1;
	localparam signed [31:0] MsgFifoDepth = 16;
	localparam signed [31:0] NumRound = 64;
	localparam signed [31:0] WordByte = 4;
	localparam [255:0] InitHash = {32'h6a09e667, 32'hbb67ae85, 32'h3c6ef372, 32'ha54ff53a, 32'h510e527f, 32'h9b05688c, 32'h1f83d9ab, 32'h5be0cd19};
	localparam [2047:0] CubicRootPrime = {32'h428a2f98, 32'h71374491, 32'hb5c0fbcf, 32'he9b5dba5, 32'h3956c25b, 32'h59f111f1, 32'h923f82a4, 32'hab1c5ed5, 32'hd807aa98, 32'h12835b01, 32'h243185be, 32'h550c7dc3, 32'h72be5d74, 32'h80deb1fe, 32'h9bdc06a7, 32'hc19bf174, 32'he49b69c1, 32'hefbe4786, 32'h0fc19dc6, 32'h240ca1cc, 32'h2de92c6f, 32'h4a7484aa, 32'h5cb0a9dc, 32'h76f988da, 32'h983e5152, 32'ha831c66d, 32'hb00327c8, 32'hbf597fc7, 32'hc6e00bf3, 32'hd5a79147, 32'h06ca6351, 32'h14292967, 32'h27b70a85, 32'h2e1b2138, 32'h4d2c6dfc, 32'h53380d13, 32'h650a7354, 32'h766a0abb, 32'h81c2c92e, 32'h92722c85, 32'ha2bfe8a1, 32'ha81a664b, 32'hc24b8b70, 32'hc76c51a3, 32'hd192e819, 32'hd6990624, 32'hf40e3585, 32'h106aa070, 32'h19a4c116, 32'h1e376c08, 32'h2748774c, 32'h34b0bcb5, 32'h391c0cb3, 32'h4ed8aa4a, 32'h5b9cca4f, 32'h682e6ff3, 32'h748f82ee, 32'h78a5636f, 32'h84c87814, 32'h8cc70208, 32'h90befffa, 32'ha4506ceb, 32'hbef9a3f7, 32'hc67178f2};
	function automatic [31:0] conv_endian;
		input reg [31:0] v;
		input reg swap;
		reg [31:0] conv_data;
		reg [31:0] _sv2v_strm_61C2C_inp;
		reg [31:0] _sv2v_strm_61C2C_out;
		integer _sv2v_strm_61C2C_idx;
		begin
			_sv2v_strm_61C2C_inp = v;
			for (_sv2v_strm_61C2C_idx = 0; _sv2v_strm_61C2C_idx <= 24; _sv2v_strm_61C2C_idx = _sv2v_strm_61C2C_idx + 8)
				_sv2v_strm_61C2C_out[31 - _sv2v_strm_61C2C_idx-:8] = _sv2v_strm_61C2C_inp[_sv2v_strm_61C2C_idx+:8];
			conv_data = _sv2v_strm_61C2C_out << 0;
			conv_endian = (swap ? conv_data : v);
		end
	endfunction
	function automatic [31:0] rotr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		rotr = (v >> amt) | (v << (32 - amt));
	endfunction
	function automatic [31:0] shiftr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		shiftr = v >> amt;
	endfunction
	function automatic [255:0] compress;
		input reg [31:0] w;
		input reg [31:0] k;
		input reg [255:0] h_i;
		reg [31:0] sigma_0;
		reg [31:0] sigma_1;
		reg [31:0] ch;
		reg [31:0] maj;
		reg [31:0] temp1;
		reg [31:0] temp2;
		begin
			sigma_1 = (rotr(h_i[128+:32], 6) ^ rotr(h_i[128+:32], 11)) ^ rotr(h_i[128+:32], 25);
			ch = (h_i[128+:32] & h_i[160+:32]) ^ (~h_i[128+:32] & h_i[192+:32]);
			temp1 = (((h_i[224+:32] + sigma_1) + ch) + k) + w;
			sigma_0 = (rotr(h_i[0+:32], 2) ^ rotr(h_i[0+:32], 13)) ^ rotr(h_i[0+:32], 22);
			maj = ((h_i[0+:32] & h_i[32+:32]) ^ (h_i[0+:32] & h_i[64+:32])) ^ (h_i[32+:32] & h_i[64+:32]);
			temp2 = sigma_0 + maj;
			compress[224+:32] = h_i[192+:32];
			compress[192+:32] = h_i[160+:32];
			compress[160+:32] = h_i[128+:32];
			compress[128+:32] = h_i[96+:32] + temp1;
			compress[96+:32] = h_i[64+:32];
			compress[64+:32] = h_i[32+:32];
			compress[32+:32] = h_i[0+:32];
			compress[0+:32] = temp1 + temp2;
		end
	endfunction
	function automatic [31:0] calc_w;
		input reg [31:0] w_0;
		input reg [31:0] w_1;
		input reg [31:0] w_9;
		input reg [31:0] w_14;
		reg [31:0] sum0;
		reg [31:0] sum1;
		begin
			sum0 = (rotr(w_1, 7) ^ rotr(w_1, 18)) ^ shiftr(w_1, 3);
			sum1 = (rotr(w_14, 17) ^ rotr(w_14, 19)) ^ shiftr(w_14, 10);
			calc_w = ((w_0 + sum0) + w_9) + sum1;
		end
	endfunction
	localparam [31:0] NoError = 32'h00000000;
	localparam [31:0] SwPushMsgWhenShaDisabled = 32'h00000001;
	localparam [31:0] SwHashStartWhenShaDisabled = 32'h00000002;
	localparam [31:0] SwUpdateSecretKeyInProcess = 32'h00000003;
	input clk_i;
	input rst_ni;
	input wipe_secret;
	input wire [31:0] wipe_v;
	input fifo_rvalid;
	input wire [(32 + WordByte) - 1:0] fifo_rdata;
	output wire fifo_rready;
	input sha_en;
	input hash_start;
	input hash_process;
	output reg hash_done;
	input [63:0] message_length;
	output reg [255:0] digest;
	wire msg_feed_complete;
	wire shaf_rready;
	wire [31:0] shaf_rdata;
	wire shaf_rvalid;
	reg [5:0] round;
	reg [3:0] w_index;
	reg [511:0] w;
	reg update_w_from_fifo;
	reg calculate_next_w;
	reg init_hash;
	reg run_hash;
	wire complete_one_chunk;
	reg update_digest;
	wire clear_digest;
	reg hash_done_next;
	reg [255:0] hash;
	always @(posedge clk_i or negedge rst_ni) begin : fill_w
		if (!rst_ni)
			w <= {512 {1'sb0}};
		else if (wipe_secret)
			w <= w ^ {16 {wipe_v}};
		else if (!sha_en)
			w <= {512 {1'sb0}};
		else if (!run_hash && update_w_from_fifo)
			w <= {shaf_rdata, w[32+:480]};
		else if (calculate_next_w)
			w <= {calc_w(w[0+:32], w[32+:32], w[288+:32], w[448+:32]), w[32+:480]};
		else if (run_hash)
			w <= {32'd0, w[32+:480]};
	end
	always @(posedge clk_i or negedge rst_ni) begin : compress_round
		if (!rst_ni)
			hash <= {256 {1'sb0}};
		else if (wipe_secret) begin : sv2v_autoblock_502
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				hash[i * 32+:32] <= hash[i * 32+:32] ^ wipe_v;
		end
		else if (init_hash)
			hash <= digest;
		else if (run_hash)
			hash <= compress(w[0+:32], CubicRootPrime[(63 - round) * 32+:32], hash);
	end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			digest <= {256 {1'sb0}};
		else if (wipe_secret) begin : sv2v_autoblock_503
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				digest[i * 32+:32] <= digest[i * 32+:32] ^ wipe_v;
		end
		else if (hash_start) begin : sv2v_autoblock_504
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				digest[i * 32+:32] <= InitHash[(7 - i) * 32+:32];
		end
		else if (!sha_en || clear_digest)
			digest <= {256 {1'sb0}};
		else if (update_digest) begin : sv2v_autoblock_505
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				digest[i * 32+:32] <= digest[i * 32+:32] + hash[i * 32+:32];
		end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			round <= {6 {1'sb0}};
		else if (!sha_en)
			round <= {6 {1'sb0}};
		else if (run_hash)
			if (round == (NumRound - 1))
				round <= {6 {1'sb0}};
			else
				round <= round + 1;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			w_index <= {4 {1'sb0}};
		else if (!sha_en)
			w_index <= {4 {1'sb0}};
		else if (update_w_from_fifo)
			w_index <= w_index + 1;
	assign shaf_rready = update_w_from_fifo;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			hash_done <= 1'b0;
		else
			hash_done <= hash_done_next;
	reg [1:0] fifo_st_q;
	reg [1:0] fifo_st_d;
	localparam [1:0] FifoIdle = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			fifo_st_q <= FifoIdle;
		else
			fifo_st_q <= fifo_st_d;
	localparam [1:0] FifoLoadFromFifo = 1;
	localparam [1:0] FifoWait = 2;
	always @(*) begin
		fifo_st_d = FifoIdle;
		update_w_from_fifo = 1'b0;
		hash_done_next = 1'b0;
		case (fifo_st_q)
			FifoIdle:
				if (hash_start)
					fifo_st_d = FifoLoadFromFifo;
				else
					fifo_st_d = FifoIdle;
			FifoLoadFromFifo:
				if (!sha_en) begin
					fifo_st_d = FifoIdle;
					update_w_from_fifo = 1'b0;
				end
				else if (!shaf_rvalid) begin
					fifo_st_d = FifoLoadFromFifo;
					update_w_from_fifo = 1'b0;
				end
				else if (w_index == 4'd15) begin
					fifo_st_d = FifoWait;
					update_w_from_fifo = 1'b1;
				end
				else begin
					fifo_st_d = FifoLoadFromFifo;
					update_w_from_fifo = 1'b1;
				end
			FifoWait:
				if (msg_feed_complete && complete_one_chunk) begin
					fifo_st_d = FifoIdle;
					hash_done_next = 1'b1;
				end
				else if (complete_one_chunk)
					fifo_st_d = FifoLoadFromFifo;
				else
					fifo_st_d = FifoWait;
			default: fifo_st_d = FifoIdle;
		endcase
	end
	reg [1:0] sha_st_q;
	reg [1:0] sha_st_d;
	localparam [1:0] ShaIdle = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			sha_st_q <= ShaIdle;
		else
			sha_st_q <= sha_st_d;
	assign clear_digest = hash_start;
	localparam [1:0] ShaCompress = 1;
	localparam [1:0] ShaUpdateDigest = 2;
	always @(*) begin
		update_digest = 1'b0;
		calculate_next_w = 1'b0;
		init_hash = 1'b0;
		run_hash = 1'b0;
		case (sha_st_q)
			ShaIdle:
				if (fifo_st_q == FifoWait) begin
					init_hash = 1'b1;
					sha_st_d = ShaCompress;
				end
				else
					sha_st_d = ShaIdle;
			ShaCompress: begin
				run_hash = 1'b1;
				if (round < 48)
					calculate_next_w = 1'b1;
				if (complete_one_chunk)
					sha_st_d = ShaUpdateDigest;
				else
					sha_st_d = ShaCompress;
			end
			ShaUpdateDigest: begin
				update_digest = 1'b1;
				if (fifo_st_q == FifoWait) begin
					init_hash = 1'b1;
					sha_st_d = ShaCompress;
				end
				else
					sha_st_d = ShaIdle;
			end
			default: sha_st_d = ShaIdle;
		endcase
	end
	assign complete_one_chunk = round == 6'd63;
	sha2_pad u_pad(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.wipe_secret(wipe_secret),
		.wipe_v(wipe_v),
		.fifo_rvalid(fifo_rvalid),
		.fifo_rdata(fifo_rdata),
		.fifo_rready(fifo_rready),
		.shaf_rvalid(shaf_rvalid),
		.shaf_rdata(shaf_rdata),
		.shaf_rready(shaf_rready),
		.sha_en(sha_en),
		.hash_start(hash_start),
		.hash_process(hash_process),
		.hash_done(hash_done),
		.message_length(message_length),
		.msg_feed_complete(msg_feed_complete)
	);
endmodule
module sha2_pad (
	clk_i,
	rst_ni,
	wipe_secret,
	wipe_v,
	fifo_rvalid,
	fifo_rdata,
	fifo_rready,
	shaf_rvalid,
	shaf_rdata,
	shaf_rready,
	sha_en,
	hash_start,
	hash_process,
	hash_done,
	message_length,
	msg_feed_complete
);
	localparam signed [31:0] NumAlerts = 1;
	localparam [NumAlerts - 1:0] AlertAsyncOn = 1'b1;
	localparam signed [31:0] MsgFifoDepth = 16;
	localparam signed [31:0] NumRound = 64;
	localparam signed [31:0] WordByte = 4;
	localparam [255:0] InitHash = {32'h6a09e667, 32'hbb67ae85, 32'h3c6ef372, 32'ha54ff53a, 32'h510e527f, 32'h9b05688c, 32'h1f83d9ab, 32'h5be0cd19};
	localparam [2047:0] CubicRootPrime = {32'h428a2f98, 32'h71374491, 32'hb5c0fbcf, 32'he9b5dba5, 32'h3956c25b, 32'h59f111f1, 32'h923f82a4, 32'hab1c5ed5, 32'hd807aa98, 32'h12835b01, 32'h243185be, 32'h550c7dc3, 32'h72be5d74, 32'h80deb1fe, 32'h9bdc06a7, 32'hc19bf174, 32'he49b69c1, 32'hefbe4786, 32'h0fc19dc6, 32'h240ca1cc, 32'h2de92c6f, 32'h4a7484aa, 32'h5cb0a9dc, 32'h76f988da, 32'h983e5152, 32'ha831c66d, 32'hb00327c8, 32'hbf597fc7, 32'hc6e00bf3, 32'hd5a79147, 32'h06ca6351, 32'h14292967, 32'h27b70a85, 32'h2e1b2138, 32'h4d2c6dfc, 32'h53380d13, 32'h650a7354, 32'h766a0abb, 32'h81c2c92e, 32'h92722c85, 32'ha2bfe8a1, 32'ha81a664b, 32'hc24b8b70, 32'hc76c51a3, 32'hd192e819, 32'hd6990624, 32'hf40e3585, 32'h106aa070, 32'h19a4c116, 32'h1e376c08, 32'h2748774c, 32'h34b0bcb5, 32'h391c0cb3, 32'h4ed8aa4a, 32'h5b9cca4f, 32'h682e6ff3, 32'h748f82ee, 32'h78a5636f, 32'h84c87814, 32'h8cc70208, 32'h90befffa, 32'ha4506ceb, 32'hbef9a3f7, 32'hc67178f2};
	function automatic [31:0] conv_endian;
		input reg [31:0] v;
		input reg swap;
		reg [31:0] conv_data;
		reg [31:0] _sv2v_strm_61C2C_inp;
		reg [31:0] _sv2v_strm_61C2C_out;
		integer _sv2v_strm_61C2C_idx;
		begin
			_sv2v_strm_61C2C_inp = v;
			for (_sv2v_strm_61C2C_idx = 0; _sv2v_strm_61C2C_idx <= 24; _sv2v_strm_61C2C_idx = _sv2v_strm_61C2C_idx + 8)
				_sv2v_strm_61C2C_out[31 - _sv2v_strm_61C2C_idx-:8] = _sv2v_strm_61C2C_inp[_sv2v_strm_61C2C_idx+:8];
			conv_data = _sv2v_strm_61C2C_out << 0;
			conv_endian = (swap ? conv_data : v);
		end
	endfunction
	function automatic [31:0] rotr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		rotr = (v >> amt) | (v << (32 - amt));
	endfunction
	function automatic [31:0] shiftr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		shiftr = v >> amt;
	endfunction
	function automatic [255:0] compress;
		input reg [31:0] w;
		input reg [31:0] k;
		input reg [255:0] h_i;
		reg [31:0] sigma_0;
		reg [31:0] sigma_1;
		reg [31:0] ch;
		reg [31:0] maj;
		reg [31:0] temp1;
		reg [31:0] temp2;
		begin
			sigma_1 = (rotr(h_i[128+:32], 6) ^ rotr(h_i[128+:32], 11)) ^ rotr(h_i[128+:32], 25);
			ch = (h_i[128+:32] & h_i[160+:32]) ^ (~h_i[128+:32] & h_i[192+:32]);
			temp1 = (((h_i[224+:32] + sigma_1) + ch) + k) + w;
			sigma_0 = (rotr(h_i[0+:32], 2) ^ rotr(h_i[0+:32], 13)) ^ rotr(h_i[0+:32], 22);
			maj = ((h_i[0+:32] & h_i[32+:32]) ^ (h_i[0+:32] & h_i[64+:32])) ^ (h_i[32+:32] & h_i[64+:32]);
			temp2 = sigma_0 + maj;
			compress[224+:32] = h_i[192+:32];
			compress[192+:32] = h_i[160+:32];
			compress[160+:32] = h_i[128+:32];
			compress[128+:32] = h_i[96+:32] + temp1;
			compress[96+:32] = h_i[64+:32];
			compress[64+:32] = h_i[32+:32];
			compress[32+:32] = h_i[0+:32];
			compress[0+:32] = temp1 + temp2;
		end
	endfunction
	function automatic [31:0] calc_w;
		input reg [31:0] w_0;
		input reg [31:0] w_1;
		input reg [31:0] w_9;
		input reg [31:0] w_14;
		reg [31:0] sum0;
		reg [31:0] sum1;
		begin
			sum0 = (rotr(w_1, 7) ^ rotr(w_1, 18)) ^ shiftr(w_1, 3);
			sum1 = (rotr(w_14, 17) ^ rotr(w_14, 19)) ^ shiftr(w_14, 10);
			calc_w = ((w_0 + sum0) + w_9) + sum1;
		end
	endfunction
	localparam [31:0] NoError = 32'h00000000;
	localparam [31:0] SwPushMsgWhenShaDisabled = 32'h00000001;
	localparam [31:0] SwHashStartWhenShaDisabled = 32'h00000002;
	localparam [31:0] SwUpdateSecretKeyInProcess = 32'h00000003;
	input clk_i;
	input rst_ni;
	input wipe_secret;
	input wire [31:0] wipe_v;
	input fifo_rvalid;
	input wire [(32 + WordByte) - 1:0] fifo_rdata;
	output reg fifo_rready;
	output reg shaf_rvalid;
	output reg [31:0] shaf_rdata;
	input shaf_rready;
	input sha_en;
	input hash_start;
	input hash_process;
	input hash_done;
	input [63:0] message_length;
	output wire msg_feed_complete;
	reg [63:0] tx_count;
	reg inc_txcount;
	wire fifo_partial;
	wire txcnt_eq_1a0;
	reg hash_process_flag;
	assign fifo_partial = ~&fifo_rdata[WordByte - 1-:WordByte];
	assign txcnt_eq_1a0 = tx_count[8:0] == 9'h1a0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			hash_process_flag <= 1'b0;
		else if (hash_process)
			hash_process_flag <= 1'b1;
		else if (hash_done || hash_start)
			hash_process_flag <= 1'b0;
	reg [2:0] sel_data;
	localparam [2:0] FifoIn = 0;
	localparam [2:0] LenHi = 3;
	localparam [2:0] LenLo = 4;
	localparam [2:0] Pad00 = 2;
	localparam [2:0] Pad80 = 1;
	always @(*)
		case (sel_data)
			FifoIn: shaf_rdata = fifo_rdata[WordByte + 31-:((WordByte + 31) - WordByte) + 1];
			Pad80:
				case (message_length[4:3])
					2'b00: shaf_rdata = 32'h80000000;
					2'b01: shaf_rdata = {fifo_rdata[WordByte + 31:WordByte + 24], 24'h800000};
					2'b10: shaf_rdata = {fifo_rdata[WordByte + 31:WordByte + 16], 16'h8000};
					2'b11: shaf_rdata = {fifo_rdata[WordByte + 31:WordByte + 8], 8'h80};
					default: shaf_rdata = 32'h00000000;
				endcase
			Pad00: shaf_rdata = {32 {1'sb0}};
			LenHi: shaf_rdata = message_length[63:32];
			LenLo: shaf_rdata = message_length[31:0];
			default: shaf_rdata = {32 {1'sb0}};
		endcase
	reg [2:0] st_q;
	reg [2:0] st_d;
	localparam [2:0] StIdle = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			st_q <= StIdle;
		else
			st_q <= st_d;
	localparam [2:0] StFifoReceive = 1;
	localparam [2:0] StLenHi = 4;
	localparam [2:0] StLenLo = 5;
	localparam [2:0] StPad00 = 3;
	localparam [2:0] StPad80 = 2;
	always @(*) begin
		shaf_rvalid = 1'b0;
		inc_txcount = 1'b0;
		sel_data = FifoIn;
		fifo_rready = 1'b0;
		st_d = StIdle;
		case (st_q)
			StIdle: begin
				sel_data = FifoIn;
				shaf_rvalid = 1'b0;
				if (sha_en && hash_start) begin
					inc_txcount = 1'b0;
					st_d = StFifoReceive;
				end
				else
					st_d = StIdle;
			end
			StFifoReceive: begin
				sel_data = FifoIn;
				if (fifo_partial && fifo_rvalid) begin
					shaf_rvalid = 1'b0;
					inc_txcount = 1'b0;
					fifo_rready = 1'b0;
					st_d = StPad80;
				end
				else if (!hash_process_flag) begin
					fifo_rready = shaf_rready;
					shaf_rvalid = fifo_rvalid;
					inc_txcount = shaf_rready;
					st_d = StFifoReceive;
				end
				else if (tx_count == message_length) begin
					shaf_rvalid = 1'b0;
					inc_txcount = 1'b0;
					fifo_rready = 1'b0;
					st_d = StPad80;
				end
				else begin
					shaf_rvalid = fifo_rvalid;
					fifo_rready = shaf_rready;
					inc_txcount = shaf_rready;
					st_d = StFifoReceive;
				end
			end
			StPad80: begin
				sel_data = Pad80;
				shaf_rvalid = 1'b1;
				fifo_rready = shaf_rready && |message_length[4:3];
				if (shaf_rready && txcnt_eq_1a0) begin
					st_d = StLenHi;
					inc_txcount = 1'b1;
				end
				else if (shaf_rready && !txcnt_eq_1a0) begin
					st_d = StPad00;
					inc_txcount = 1'b1;
				end
				else begin
					st_d = StPad80;
					inc_txcount = 1'b0;
				end
			end
			StPad00: begin
				sel_data = Pad00;
				shaf_rvalid = 1'b1;
				if (shaf_rready) begin
					inc_txcount = 1'b1;
					if (txcnt_eq_1a0)
						st_d = StLenHi;
					else
						st_d = StPad00;
				end
				else
					st_d = StPad00;
			end
			StLenHi: begin
				sel_data = LenHi;
				shaf_rvalid = 1'b1;
				if (shaf_rready) begin
					st_d = StLenLo;
					inc_txcount = 1'b1;
				end
				else begin
					st_d = StLenHi;
					inc_txcount = 1'b0;
				end
			end
			StLenLo: begin
				sel_data = LenLo;
				shaf_rvalid = 1'b1;
				if (shaf_rready) begin
					st_d = StIdle;
					inc_txcount = 1'b1;
				end
				else begin
					st_d = StLenLo;
					inc_txcount = 1'b0;
				end
			end
			default: st_d = StIdle;
		endcase
	end
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			tx_count <= {64 {1'sb0}};
		else if (hash_start)
			tx_count <= {64 {1'sb0}};
		else if (inc_txcount)
			tx_count[63:5] <= tx_count[63:5] + 1'b1;
	assign msg_feed_complete = hash_process_flag && (st_q == StIdle);
endmodule
module hmac_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	tl_win_o,
	tl_win_i,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16 : (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_win_o;
	input wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1 : (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_win_i;
	output wire [320:0] reg2hw;
	input wire [627:0] hw2reg;
	input devmode_i;
	localparam signed [31:0] NumWords = 8;
	localparam [11:0] HMAC_INTR_STATE_OFFSET = 12'h000;
	localparam [11:0] HMAC_INTR_ENABLE_OFFSET = 12'h004;
	localparam [11:0] HMAC_INTR_TEST_OFFSET = 12'h008;
	localparam [11:0] HMAC_CFG_OFFSET = 12'h00c;
	localparam [11:0] HMAC_CMD_OFFSET = 12'h010;
	localparam [11:0] HMAC_STATUS_OFFSET = 12'h014;
	localparam [11:0] HMAC_ERR_CODE_OFFSET = 12'h018;
	localparam [11:0] HMAC_WIPE_SECRET_OFFSET = 12'h01c;
	localparam [11:0] HMAC_KEY0_OFFSET = 12'h020;
	localparam [11:0] HMAC_KEY1_OFFSET = 12'h024;
	localparam [11:0] HMAC_KEY2_OFFSET = 12'h028;
	localparam [11:0] HMAC_KEY3_OFFSET = 12'h02c;
	localparam [11:0] HMAC_KEY4_OFFSET = 12'h030;
	localparam [11:0] HMAC_KEY5_OFFSET = 12'h034;
	localparam [11:0] HMAC_KEY6_OFFSET = 12'h038;
	localparam [11:0] HMAC_KEY7_OFFSET = 12'h03c;
	localparam [11:0] HMAC_DIGEST0_OFFSET = 12'h040;
	localparam [11:0] HMAC_DIGEST1_OFFSET = 12'h044;
	localparam [11:0] HMAC_DIGEST2_OFFSET = 12'h048;
	localparam [11:0] HMAC_DIGEST3_OFFSET = 12'h04c;
	localparam [11:0] HMAC_DIGEST4_OFFSET = 12'h050;
	localparam [11:0] HMAC_DIGEST5_OFFSET = 12'h054;
	localparam [11:0] HMAC_DIGEST6_OFFSET = 12'h058;
	localparam [11:0] HMAC_DIGEST7_OFFSET = 12'h05c;
	localparam [11:0] HMAC_MSG_LENGTH_LOWER_OFFSET = 12'h060;
	localparam [11:0] HMAC_MSG_LENGTH_UPPER_OFFSET = 12'h064;
	localparam [11:0] HMAC_MSG_FIFO_OFFSET = 12'h800;
	localparam [11:0] HMAC_MSG_FIFO_SIZE = 12'h800;
	localparam signed [31:0] HMAC_INTR_STATE = 0;
	localparam signed [31:0] HMAC_INTR_ENABLE = 1;
	localparam signed [31:0] HMAC_INTR_TEST = 2;
	localparam signed [31:0] HMAC_CFG = 3;
	localparam signed [31:0] HMAC_CMD = 4;
	localparam signed [31:0] HMAC_STATUS = 5;
	localparam signed [31:0] HMAC_ERR_CODE = 6;
	localparam signed [31:0] HMAC_WIPE_SECRET = 7;
	localparam signed [31:0] HMAC_KEY0 = 8;
	localparam signed [31:0] HMAC_KEY1 = 9;
	localparam signed [31:0] HMAC_KEY2 = 10;
	localparam signed [31:0] HMAC_KEY3 = 11;
	localparam signed [31:0] HMAC_KEY4 = 12;
	localparam signed [31:0] HMAC_KEY5 = 13;
	localparam signed [31:0] HMAC_KEY6 = 14;
	localparam signed [31:0] HMAC_KEY7 = 15;
	localparam signed [31:0] HMAC_DIGEST0 = 16;
	localparam signed [31:0] HMAC_DIGEST1 = 17;
	localparam signed [31:0] HMAC_DIGEST2 = 18;
	localparam signed [31:0] HMAC_DIGEST3 = 19;
	localparam signed [31:0] HMAC_DIGEST4 = 20;
	localparam signed [31:0] HMAC_DIGEST5 = 21;
	localparam signed [31:0] HMAC_DIGEST6 = 22;
	localparam signed [31:0] HMAC_DIGEST7 = 23;
	localparam signed [31:0] HMAC_MSG_LENGTH_LOWER = 24;
	localparam signed [31:0] HMAC_MSG_LENGTH_UPPER = 25;
	localparam [103:0] HMAC_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111};
	localparam signed [31:0] AW = 12;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_socket_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (2 * ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2)) - 1 : (2 * (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_socket_d2h;
	reg [1:0] reg_steer;
	assign tl_reg_h2d = tl_socket_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_socket_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_reg_d2h;
	assign tl_win_o[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))] = tl_socket_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16) + (((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))];
	assign tl_socket_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1) + (((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))] = tl_win_i[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))];
	tlul_socket_1n #(
		.N(2),
		.HReqPass(1'b1),
		.HRspPass(1'b1),
		.DReqPass({2 {1'b1}}),
		.DRspPass({2 {1'b1}}),
		.HReqDepth(4'h0),
		.HRspDepth(4'h0),
		.DReqDepth({2 {4'h0}}),
		.DRspDepth({2 {4'h0}})
	) u_socket(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_h_i(tl_i),
		.tl_h_o(tl_o),
		.tl_d_o(tl_socket_h2d),
		.tl_d_i(tl_socket_d2h),
		.dev_select(reg_steer)
	);
	always @(*) begin
		reg_steer = 1;
		if (tl_i[(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - ((top_pkg_TL_AW - 1) - (AW - 1)):(top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_AW - 1)] >= 2048)
			reg_steer = 0;
	end
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire intr_state_hmac_done_qs;
	wire intr_state_hmac_done_wd;
	wire intr_state_hmac_done_we;
	wire intr_state_fifo_full_qs;
	wire intr_state_fifo_full_wd;
	wire intr_state_fifo_full_we;
	wire intr_state_hmac_err_qs;
	wire intr_state_hmac_err_wd;
	wire intr_state_hmac_err_we;
	wire intr_enable_hmac_done_qs;
	wire intr_enable_hmac_done_wd;
	wire intr_enable_hmac_done_we;
	wire intr_enable_fifo_full_qs;
	wire intr_enable_fifo_full_wd;
	wire intr_enable_fifo_full_we;
	wire intr_enable_hmac_err_qs;
	wire intr_enable_hmac_err_wd;
	wire intr_enable_hmac_err_we;
	wire intr_test_hmac_done_wd;
	wire intr_test_hmac_done_we;
	wire intr_test_fifo_full_wd;
	wire intr_test_fifo_full_we;
	wire intr_test_hmac_err_wd;
	wire intr_test_hmac_err_we;
	wire cfg_hmac_en_qs;
	wire cfg_hmac_en_wd;
	wire cfg_hmac_en_we;
	wire cfg_hmac_en_re;
	wire cfg_sha_en_qs;
	wire cfg_sha_en_wd;
	wire cfg_sha_en_we;
	wire cfg_sha_en_re;
	wire cfg_endian_swap_qs;
	wire cfg_endian_swap_wd;
	wire cfg_endian_swap_we;
	wire cfg_endian_swap_re;
	wire cfg_digest_swap_qs;
	wire cfg_digest_swap_wd;
	wire cfg_digest_swap_we;
	wire cfg_digest_swap_re;
	wire cmd_hash_start_wd;
	wire cmd_hash_start_we;
	wire cmd_hash_process_wd;
	wire cmd_hash_process_we;
	wire status_fifo_empty_qs;
	wire status_fifo_empty_re;
	wire status_fifo_full_qs;
	wire status_fifo_full_re;
	wire [4:0] status_fifo_depth_qs;
	wire status_fifo_depth_re;
	wire [31:0] err_code_qs;
	wire [31:0] wipe_secret_wd;
	wire wipe_secret_we;
	wire [31:0] key0_wd;
	wire key0_we;
	wire [31:0] key1_wd;
	wire key1_we;
	wire [31:0] key2_wd;
	wire key2_we;
	wire [31:0] key3_wd;
	wire key3_we;
	wire [31:0] key4_wd;
	wire key4_we;
	wire [31:0] key5_wd;
	wire key5_we;
	wire [31:0] key6_wd;
	wire key6_we;
	wire [31:0] key7_wd;
	wire key7_we;
	wire [31:0] digest0_qs;
	wire digest0_re;
	wire [31:0] digest1_qs;
	wire digest1_re;
	wire [31:0] digest2_qs;
	wire digest2_re;
	wire [31:0] digest3_qs;
	wire digest3_re;
	wire [31:0] digest4_qs;
	wire digest4_re;
	wire [31:0] digest5_qs;
	wire digest5_re;
	wire [31:0] digest6_qs;
	wire digest6_re;
	wire [31:0] digest7_qs;
	wire digest7_re;
	wire [31:0] msg_length_lower_qs;
	wire [31:0] msg_length_upper_qs;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_hmac_done(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_hmac_done_we),
		.wd(intr_state_hmac_done_wd),
		.de(hw2reg[626]),
		.d(hw2reg[627]),
		.qe(),
		.q(reg2hw[320]),
		.qs(intr_state_hmac_done_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_fifo_full(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_fifo_full_we),
		.wd(intr_state_fifo_full_wd),
		.de(hw2reg[624]),
		.d(hw2reg[625]),
		.qe(),
		.q(reg2hw[319]),
		.qs(intr_state_fifo_full_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_hmac_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_hmac_err_we),
		.wd(intr_state_hmac_err_wd),
		.de(hw2reg[622]),
		.d(hw2reg[623]),
		.qe(),
		.q(reg2hw[318]),
		.qs(intr_state_hmac_err_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_hmac_done(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_hmac_done_we),
		.wd(intr_enable_hmac_done_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[317]),
		.qs(intr_enable_hmac_done_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_fifo_full(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_fifo_full_we),
		.wd(intr_enable_fifo_full_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[316]),
		.qs(intr_enable_fifo_full_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_hmac_err(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_hmac_err_we),
		.wd(intr_enable_hmac_err_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[315]),
		.qs(intr_enable_hmac_err_qs)
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_hmac_done(
		.re(1'b0),
		.we(intr_test_hmac_done_we),
		.wd(intr_test_hmac_done_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[313]),
		.q(reg2hw[314]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_fifo_full(
		.re(1'b0),
		.we(intr_test_fifo_full_we),
		.wd(intr_test_fifo_full_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[311]),
		.q(reg2hw[312]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_hmac_err(
		.re(1'b0),
		.we(intr_test_hmac_err_we),
		.wd(intr_test_hmac_err_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[309]),
		.q(reg2hw[310]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_cfg_hmac_en(
		.re(cfg_hmac_en_re),
		.we(cfg_hmac_en_we),
		.wd(cfg_hmac_en_wd),
		.d(hw2reg[621]),
		.qre(),
		.qe(reg2hw[307]),
		.q(reg2hw[308]),
		.qs(cfg_hmac_en_qs)
	);
	prim_subreg_ext #(.DW(1)) u_cfg_sha_en(
		.re(cfg_sha_en_re),
		.we(cfg_sha_en_we),
		.wd(cfg_sha_en_wd),
		.d(hw2reg[620]),
		.qre(),
		.qe(reg2hw[305]),
		.q(reg2hw[306]),
		.qs(cfg_sha_en_qs)
	);
	prim_subreg_ext #(.DW(1)) u_cfg_endian_swap(
		.re(cfg_endian_swap_re),
		.we(cfg_endian_swap_we),
		.wd(cfg_endian_swap_wd),
		.d(hw2reg[619]),
		.qre(),
		.qe(reg2hw[303]),
		.q(reg2hw[304]),
		.qs(cfg_endian_swap_qs)
	);
	prim_subreg_ext #(.DW(1)) u_cfg_digest_swap(
		.re(cfg_digest_swap_re),
		.we(cfg_digest_swap_we),
		.wd(cfg_digest_swap_wd),
		.d(hw2reg[618]),
		.qre(),
		.qe(reg2hw[301]),
		.q(reg2hw[302]),
		.qs(cfg_digest_swap_qs)
	);
	prim_subreg_ext #(.DW(1)) u_cmd_hash_start(
		.re(1'b0),
		.we(cmd_hash_start_we),
		.wd(cmd_hash_start_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[299]),
		.q(reg2hw[300]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_cmd_hash_process(
		.re(1'b0),
		.we(cmd_hash_process_we),
		.wd(cmd_hash_process_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[297]),
		.q(reg2hw[298]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_status_fifo_empty(
		.re(status_fifo_empty_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[617]),
		.qre(),
		.qe(),
		.q(),
		.qs(status_fifo_empty_qs)
	);
	prim_subreg_ext #(.DW(1)) u_status_fifo_full(
		.re(status_fifo_full_re),
		.we(1'b0),
		.wd(1'sb0),
		.d(hw2reg[616]),
		.qre(),
		.qe(),
		.q(),
		.qs(status_fifo_full_qs)
	);
	prim_subreg_ext #(.DW(5)) u_status_fifo_depth(
		.re(status_fifo_depth_re),
		.we(1'b0),
		.wd({5 {1'sb0}}),
		.d(hw2reg[615-:5]),
		.qre(),
		.qe(),
		.q(),
		.qs(status_fifo_depth_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(32'h00000000)
	) u_err_code(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.de(hw2reg[578]),
		.d(hw2reg[610-:32]),
		.qe(),
		.q(),
		.qs(err_code_qs)
	);
	prim_subreg_ext #(.DW(32)) u_wipe_secret(
		.re(1'b0),
		.we(wipe_secret_we),
		.wd(wipe_secret_wd),
		.d({32 {1'sb0}}),
		.qre(),
		.qe(reg2hw[264]),
		.q(reg2hw[296-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key0(
		.re(1'b0),
		.we(key0_we),
		.wd(key0_wd),
		.d(hw2reg[353-:32]),
		.qre(),
		.qe(reg2hw[0]),
		.q(reg2hw[32-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key1(
		.re(1'b0),
		.we(key1_we),
		.wd(key1_wd),
		.d(hw2reg[385-:32]),
		.qre(),
		.qe(reg2hw[33]),
		.q(reg2hw[65-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key2(
		.re(1'b0),
		.we(key2_we),
		.wd(key2_wd),
		.d(hw2reg[417-:32]),
		.qre(),
		.qe(reg2hw[66]),
		.q(reg2hw[98-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key3(
		.re(1'b0),
		.we(key3_we),
		.wd(key3_wd),
		.d(hw2reg[449-:32]),
		.qre(),
		.qe(reg2hw[99]),
		.q(reg2hw[131-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key4(
		.re(1'b0),
		.we(key4_we),
		.wd(key4_wd),
		.d(hw2reg[481-:32]),
		.qre(),
		.qe(reg2hw[132]),
		.q(reg2hw[164-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key5(
		.re(1'b0),
		.we(key5_we),
		.wd(key5_wd),
		.d(hw2reg[513-:32]),
		.qre(),
		.qe(reg2hw[165]),
		.q(reg2hw[197-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key6(
		.re(1'b0),
		.we(key6_we),
		.wd(key6_wd),
		.d(hw2reg[545-:32]),
		.qre(),
		.qe(reg2hw[198]),
		.q(reg2hw[230-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_key7(
		.re(1'b0),
		.we(key7_we),
		.wd(key7_wd),
		.d(hw2reg[577-:32]),
		.qre(),
		.qe(reg2hw[231]),
		.q(reg2hw[263-:32]),
		.qs()
	);
	prim_subreg_ext #(.DW(32)) u_digest0(
		.re(digest0_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[97-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest0_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest1(
		.re(digest1_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[129-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest1_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest2(
		.re(digest2_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[161-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest2_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest3(
		.re(digest3_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[193-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest3_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest4(
		.re(digest4_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[225-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest4_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest5(
		.re(digest5_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[257-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest5_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest6(
		.re(digest6_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[289-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest6_qs)
	);
	prim_subreg_ext #(.DW(32)) u_digest7(
		.re(digest7_re),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.d(hw2reg[321-:32]),
		.qre(),
		.qe(),
		.q(),
		.qs(digest7_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(32'h00000000)
	) u_msg_length_lower(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.de(hw2reg[33]),
		.d(hw2reg[65-:32]),
		.qe(),
		.q(),
		.qs(msg_length_lower_qs)
	);
	prim_subreg #(
		.DW(32),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RO"),
		.RESVAL(32'h00000000)
	) u_msg_length_upper(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(1'b0),
		.wd({32 {1'sb0}}),
		.de(hw2reg[0]),
		.d(hw2reg[32-:32]),
		.qe(),
		.q(),
		.qs(msg_length_upper_qs)
	);
	reg [25:0] addr_hit;
	always @(*) begin
		addr_hit = {26 {1'sb0}};
		addr_hit[0] = reg_addr == HMAC_INTR_STATE_OFFSET;
		addr_hit[1] = reg_addr == HMAC_INTR_ENABLE_OFFSET;
		addr_hit[2] = reg_addr == HMAC_INTR_TEST_OFFSET;
		addr_hit[3] = reg_addr == HMAC_CFG_OFFSET;
		addr_hit[4] = reg_addr == HMAC_CMD_OFFSET;
		addr_hit[5] = reg_addr == HMAC_STATUS_OFFSET;
		addr_hit[6] = reg_addr == HMAC_ERR_CODE_OFFSET;
		addr_hit[7] = reg_addr == HMAC_WIPE_SECRET_OFFSET;
		addr_hit[8] = reg_addr == HMAC_KEY0_OFFSET;
		addr_hit[9] = reg_addr == HMAC_KEY1_OFFSET;
		addr_hit[10] = reg_addr == HMAC_KEY2_OFFSET;
		addr_hit[11] = reg_addr == HMAC_KEY3_OFFSET;
		addr_hit[12] = reg_addr == HMAC_KEY4_OFFSET;
		addr_hit[13] = reg_addr == HMAC_KEY5_OFFSET;
		addr_hit[14] = reg_addr == HMAC_KEY6_OFFSET;
		addr_hit[15] = reg_addr == HMAC_KEY7_OFFSET;
		addr_hit[16] = reg_addr == HMAC_DIGEST0_OFFSET;
		addr_hit[17] = reg_addr == HMAC_DIGEST1_OFFSET;
		addr_hit[18] = reg_addr == HMAC_DIGEST2_OFFSET;
		addr_hit[19] = reg_addr == HMAC_DIGEST3_OFFSET;
		addr_hit[20] = reg_addr == HMAC_DIGEST4_OFFSET;
		addr_hit[21] = reg_addr == HMAC_DIGEST5_OFFSET;
		addr_hit[22] = reg_addr == HMAC_DIGEST6_OFFSET;
		addr_hit[23] = reg_addr == HMAC_DIGEST7_OFFSET;
		addr_hit[24] = reg_addr == HMAC_MSG_LENGTH_LOWER_OFFSET;
		addr_hit[25] = reg_addr == HMAC_MSG_LENGTH_UPPER_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (HMAC_PERMIT[100+:4] != (HMAC_PERMIT[100+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (HMAC_PERMIT[96+:4] != (HMAC_PERMIT[96+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (HMAC_PERMIT[92+:4] != (HMAC_PERMIT[92+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[3] && reg_we) && (HMAC_PERMIT[88+:4] != (HMAC_PERMIT[88+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[4] && reg_we) && (HMAC_PERMIT[84+:4] != (HMAC_PERMIT[84+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[5] && reg_we) && (HMAC_PERMIT[80+:4] != (HMAC_PERMIT[80+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[6] && reg_we) && (HMAC_PERMIT[76+:4] != (HMAC_PERMIT[76+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[7] && reg_we) && (HMAC_PERMIT[72+:4] != (HMAC_PERMIT[72+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[8] && reg_we) && (HMAC_PERMIT[68+:4] != (HMAC_PERMIT[68+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[9] && reg_we) && (HMAC_PERMIT[64+:4] != (HMAC_PERMIT[64+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[10] && reg_we) && (HMAC_PERMIT[60+:4] != (HMAC_PERMIT[60+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[11] && reg_we) && (HMAC_PERMIT[56+:4] != (HMAC_PERMIT[56+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[12] && reg_we) && (HMAC_PERMIT[52+:4] != (HMAC_PERMIT[52+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[13] && reg_we) && (HMAC_PERMIT[48+:4] != (HMAC_PERMIT[48+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[14] && reg_we) && (HMAC_PERMIT[44+:4] != (HMAC_PERMIT[44+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[15] && reg_we) && (HMAC_PERMIT[40+:4] != (HMAC_PERMIT[40+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[16] && reg_we) && (HMAC_PERMIT[36+:4] != (HMAC_PERMIT[36+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[17] && reg_we) && (HMAC_PERMIT[32+:4] != (HMAC_PERMIT[32+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[18] && reg_we) && (HMAC_PERMIT[28+:4] != (HMAC_PERMIT[28+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[19] && reg_we) && (HMAC_PERMIT[24+:4] != (HMAC_PERMIT[24+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[20] && reg_we) && (HMAC_PERMIT[20+:4] != (HMAC_PERMIT[20+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[21] && reg_we) && (HMAC_PERMIT[16+:4] != (HMAC_PERMIT[16+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[22] && reg_we) && (HMAC_PERMIT[12+:4] != (HMAC_PERMIT[12+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[23] && reg_we) && (HMAC_PERMIT[8+:4] != (HMAC_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[24] && reg_we) && (HMAC_PERMIT[4+:4] != (HMAC_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[25] && reg_we) && (HMAC_PERMIT[0+:4] != (HMAC_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign intr_state_hmac_done_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_hmac_done_wd = reg_wdata[0];
	assign intr_state_fifo_full_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_fifo_full_wd = reg_wdata[1];
	assign intr_state_hmac_err_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_hmac_err_wd = reg_wdata[2];
	assign intr_enable_hmac_done_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_hmac_done_wd = reg_wdata[0];
	assign intr_enable_fifo_full_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_fifo_full_wd = reg_wdata[1];
	assign intr_enable_hmac_err_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_hmac_err_wd = reg_wdata[2];
	assign intr_test_hmac_done_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_hmac_done_wd = reg_wdata[0];
	assign intr_test_fifo_full_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_fifo_full_wd = reg_wdata[1];
	assign intr_test_hmac_err_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_hmac_err_wd = reg_wdata[2];
	assign cfg_hmac_en_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign cfg_hmac_en_wd = reg_wdata[0];
	assign cfg_hmac_en_re = addr_hit[3] && reg_re;
	assign cfg_sha_en_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign cfg_sha_en_wd = reg_wdata[1];
	assign cfg_sha_en_re = addr_hit[3] && reg_re;
	assign cfg_endian_swap_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign cfg_endian_swap_wd = reg_wdata[2];
	assign cfg_endian_swap_re = addr_hit[3] && reg_re;
	assign cfg_digest_swap_we = (addr_hit[3] & reg_we) & ~wr_err;
	assign cfg_digest_swap_wd = reg_wdata[3];
	assign cfg_digest_swap_re = addr_hit[3] && reg_re;
	assign cmd_hash_start_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign cmd_hash_start_wd = reg_wdata[0];
	assign cmd_hash_process_we = (addr_hit[4] & reg_we) & ~wr_err;
	assign cmd_hash_process_wd = reg_wdata[1];
	assign status_fifo_empty_re = addr_hit[5] && reg_re;
	assign status_fifo_full_re = addr_hit[5] && reg_re;
	assign status_fifo_depth_re = addr_hit[5] && reg_re;
	assign wipe_secret_we = (addr_hit[7] & reg_we) & ~wr_err;
	assign wipe_secret_wd = reg_wdata[31:0];
	assign key0_we = (addr_hit[8] & reg_we) & ~wr_err;
	assign key0_wd = reg_wdata[31:0];
	assign key1_we = (addr_hit[9] & reg_we) & ~wr_err;
	assign key1_wd = reg_wdata[31:0];
	assign key2_we = (addr_hit[10] & reg_we) & ~wr_err;
	assign key2_wd = reg_wdata[31:0];
	assign key3_we = (addr_hit[11] & reg_we) & ~wr_err;
	assign key3_wd = reg_wdata[31:0];
	assign key4_we = (addr_hit[12] & reg_we) & ~wr_err;
	assign key4_wd = reg_wdata[31:0];
	assign key5_we = (addr_hit[13] & reg_we) & ~wr_err;
	assign key5_wd = reg_wdata[31:0];
	assign key6_we = (addr_hit[14] & reg_we) & ~wr_err;
	assign key6_wd = reg_wdata[31:0];
	assign key7_we = (addr_hit[15] & reg_we) & ~wr_err;
	assign key7_wd = reg_wdata[31:0];
	assign digest0_re = addr_hit[16] && reg_re;
	assign digest1_re = addr_hit[17] && reg_re;
	assign digest2_re = addr_hit[18] && reg_re;
	assign digest3_re = addr_hit[19] && reg_re;
	assign digest4_re = addr_hit[20] && reg_re;
	assign digest5_re = addr_hit[21] && reg_re;
	assign digest6_re = addr_hit[22] && reg_re;
	assign digest7_re = addr_hit[23] && reg_re;
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: begin
				reg_rdata_next[0] = intr_state_hmac_done_qs;
				reg_rdata_next[1] = intr_state_fifo_full_qs;
				reg_rdata_next[2] = intr_state_hmac_err_qs;
			end
			addr_hit[1]: begin
				reg_rdata_next[0] = intr_enable_hmac_done_qs;
				reg_rdata_next[1] = intr_enable_fifo_full_qs;
				reg_rdata_next[2] = intr_enable_hmac_err_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[2] = 1'sb0;
			end
			addr_hit[3]: begin
				reg_rdata_next[0] = cfg_hmac_en_qs;
				reg_rdata_next[1] = cfg_sha_en_qs;
				reg_rdata_next[2] = cfg_endian_swap_qs;
				reg_rdata_next[3] = cfg_digest_swap_qs;
			end
			addr_hit[4]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
			end
			addr_hit[5]: begin
				reg_rdata_next[0] = status_fifo_empty_qs;
				reg_rdata_next[1] = status_fifo_full_qs;
				reg_rdata_next[8:4] = status_fifo_depth_qs;
			end
			addr_hit[6]: reg_rdata_next[31:0] = err_code_qs;
			addr_hit[7]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[8]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[9]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[10]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[11]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[12]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[13]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[14]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[15]: reg_rdata_next[31:0] = {32 {1'sb0}};
			addr_hit[16]: reg_rdata_next[31:0] = digest0_qs;
			addr_hit[17]: reg_rdata_next[31:0] = digest1_qs;
			addr_hit[18]: reg_rdata_next[31:0] = digest2_qs;
			addr_hit[19]: reg_rdata_next[31:0] = digest3_qs;
			addr_hit[20]: reg_rdata_next[31:0] = digest4_qs;
			addr_hit[21]: reg_rdata_next[31:0] = digest5_qs;
			addr_hit[22]: reg_rdata_next[31:0] = digest6_qs;
			addr_hit[23]: reg_rdata_next[31:0] = digest7_qs;
			addr_hit[24]: reg_rdata_next[31:0] = msg_length_lower_qs;
			addr_hit[25]: reg_rdata_next[31:0] = msg_length_upper_qs;
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module hmac_core (
	clk_i,
	rst_ni,
	secret_key,
	wipe_secret,
	wipe_v,
	hmac_en,
	reg_hash_start,
	reg_hash_process,
	hash_done,
	sha_hash_start,
	sha_hash_process,
	sha_hash_done,
	sha_rvalid,
	sha_rdata,
	sha_rready,
	fifo_rvalid,
	fifo_rdata,
	fifo_rready,
	fifo_wsel,
	fifo_wvalid,
	fifo_wdata_sel,
	fifo_wready,
	message_length,
	sha_message_length
);
	localparam signed [31:0] NumAlerts = 1;
	localparam [NumAlerts - 1:0] AlertAsyncOn = 1'b1;
	localparam signed [31:0] MsgFifoDepth = 16;
	localparam signed [31:0] NumRound = 64;
	localparam signed [31:0] WordByte = 4;
	localparam [255:0] InitHash = {32'h6a09e667, 32'hbb67ae85, 32'h3c6ef372, 32'ha54ff53a, 32'h510e527f, 32'h9b05688c, 32'h1f83d9ab, 32'h5be0cd19};
	localparam [2047:0] CubicRootPrime = {32'h428a2f98, 32'h71374491, 32'hb5c0fbcf, 32'he9b5dba5, 32'h3956c25b, 32'h59f111f1, 32'h923f82a4, 32'hab1c5ed5, 32'hd807aa98, 32'h12835b01, 32'h243185be, 32'h550c7dc3, 32'h72be5d74, 32'h80deb1fe, 32'h9bdc06a7, 32'hc19bf174, 32'he49b69c1, 32'hefbe4786, 32'h0fc19dc6, 32'h240ca1cc, 32'h2de92c6f, 32'h4a7484aa, 32'h5cb0a9dc, 32'h76f988da, 32'h983e5152, 32'ha831c66d, 32'hb00327c8, 32'hbf597fc7, 32'hc6e00bf3, 32'hd5a79147, 32'h06ca6351, 32'h14292967, 32'h27b70a85, 32'h2e1b2138, 32'h4d2c6dfc, 32'h53380d13, 32'h650a7354, 32'h766a0abb, 32'h81c2c92e, 32'h92722c85, 32'ha2bfe8a1, 32'ha81a664b, 32'hc24b8b70, 32'hc76c51a3, 32'hd192e819, 32'hd6990624, 32'hf40e3585, 32'h106aa070, 32'h19a4c116, 32'h1e376c08, 32'h2748774c, 32'h34b0bcb5, 32'h391c0cb3, 32'h4ed8aa4a, 32'h5b9cca4f, 32'h682e6ff3, 32'h748f82ee, 32'h78a5636f, 32'h84c87814, 32'h8cc70208, 32'h90befffa, 32'ha4506ceb, 32'hbef9a3f7, 32'hc67178f2};
	function automatic [31:0] conv_endian;
		input reg [31:0] v;
		input reg swap;
		reg [31:0] conv_data;
		reg [31:0] _sv2v_strm_61C2C_inp;
		reg [31:0] _sv2v_strm_61C2C_out;
		integer _sv2v_strm_61C2C_idx;
		begin
			_sv2v_strm_61C2C_inp = v;
			for (_sv2v_strm_61C2C_idx = 0; _sv2v_strm_61C2C_idx <= 24; _sv2v_strm_61C2C_idx = _sv2v_strm_61C2C_idx + 8)
				_sv2v_strm_61C2C_out[31 - _sv2v_strm_61C2C_idx-:8] = _sv2v_strm_61C2C_inp[_sv2v_strm_61C2C_idx+:8];
			conv_data = _sv2v_strm_61C2C_out << 0;
			conv_endian = (swap ? conv_data : v);
		end
	endfunction
	function automatic [31:0] rotr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		rotr = (v >> amt) | (v << (32 - amt));
	endfunction
	function automatic [31:0] shiftr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		shiftr = v >> amt;
	endfunction
	function automatic [255:0] compress;
		input reg [31:0] w;
		input reg [31:0] k;
		input reg [255:0] h_i;
		reg [31:0] sigma_0;
		reg [31:0] sigma_1;
		reg [31:0] ch;
		reg [31:0] maj;
		reg [31:0] temp1;
		reg [31:0] temp2;
		begin
			sigma_1 = (rotr(h_i[128+:32], 6) ^ rotr(h_i[128+:32], 11)) ^ rotr(h_i[128+:32], 25);
			ch = (h_i[128+:32] & h_i[160+:32]) ^ (~h_i[128+:32] & h_i[192+:32]);
			temp1 = (((h_i[224+:32] + sigma_1) + ch) + k) + w;
			sigma_0 = (rotr(h_i[0+:32], 2) ^ rotr(h_i[0+:32], 13)) ^ rotr(h_i[0+:32], 22);
			maj = ((h_i[0+:32] & h_i[32+:32]) ^ (h_i[0+:32] & h_i[64+:32])) ^ (h_i[32+:32] & h_i[64+:32]);
			temp2 = sigma_0 + maj;
			compress[224+:32] = h_i[192+:32];
			compress[192+:32] = h_i[160+:32];
			compress[160+:32] = h_i[128+:32];
			compress[128+:32] = h_i[96+:32] + temp1;
			compress[96+:32] = h_i[64+:32];
			compress[64+:32] = h_i[32+:32];
			compress[32+:32] = h_i[0+:32];
			compress[0+:32] = temp1 + temp2;
		end
	endfunction
	function automatic [31:0] calc_w;
		input reg [31:0] w_0;
		input reg [31:0] w_1;
		input reg [31:0] w_9;
		input reg [31:0] w_14;
		reg [31:0] sum0;
		reg [31:0] sum1;
		begin
			sum0 = (rotr(w_1, 7) ^ rotr(w_1, 18)) ^ shiftr(w_1, 3);
			sum1 = (rotr(w_14, 17) ^ rotr(w_14, 19)) ^ shiftr(w_14, 10);
			calc_w = ((w_0 + sum0) + w_9) + sum1;
		end
	endfunction
	localparam [31:0] NoError = 32'h00000000;
	localparam [31:0] SwPushMsgWhenShaDisabled = 32'h00000001;
	localparam [31:0] SwHashStartWhenShaDisabled = 32'h00000002;
	localparam [31:0] SwUpdateSecretKeyInProcess = 32'h00000003;
	input clk_i;
	input rst_ni;
	input [255:0] secret_key;
	input wipe_secret;
	input [31:0] wipe_v;
	input hmac_en;
	input reg_hash_start;
	input reg_hash_process;
	output wire hash_done;
	output wire sha_hash_start;
	output wire sha_hash_process;
	input sha_hash_done;
	output wire sha_rvalid;
	output wire [(32 + WordByte) - 1:0] sha_rdata;
	input sha_rready;
	input fifo_rvalid;
	input wire [(32 + WordByte) - 1:0] fifo_rdata;
	output wire fifo_rready;
	output reg fifo_wsel;
	output reg fifo_wvalid;
	output reg [2:0] fifo_wdata_sel;
	input fifo_wready;
	input [63:0] message_length;
	output [63:0] sha_message_length;
	localparam [31:0] BlockSize = 512;
	localparam [31:0] BlockSizeBits = 9;
	localparam [31:0] HashWordBits = 5;
	reg hash_start;
	reg hash_process;
	reg hmac_hash_done;
	wire [BlockSize - 1:0] i_pad;
	wire [BlockSize - 1:0] o_pad;
	reg [63:0] txcount;
	wire [(BlockSizeBits - HashWordBits) - 1:0] pad_index;
	reg clr_txcount;
	wire inc_txcount;
	reg hmac_sha_rvalid;
	reg [1:0] sel_rdata;
	wire sel_msglen;
	reg update_round;
	reg round_q;
	reg round_d;
	reg [2:0] st_q;
	reg [2:0] st_d;
	reg clr_fifo_wdata_sel;
	wire txcnt_eq_blksz;
	reg reg_hash_process_flag;
	assign sha_hash_start = (hmac_en ? hash_start : reg_hash_start);
	assign sha_hash_process = (hmac_en ? reg_hash_process | hash_process : reg_hash_process);
	assign hash_done = (hmac_en ? hmac_hash_done : sha_hash_done);
	assign pad_index = txcount[BlockSizeBits - 1:HashWordBits];
	assign i_pad = {secret_key, {BlockSize - 256 {1'b0}}} ^ {BlockSize / 8 {8'h36}};
	assign o_pad = {secret_key, {BlockSize - 256 {1'b0}}} ^ {BlockSize / 8 {8'h5c}};
	localparam [2:0] StMsg = 2;
	assign fifo_rready = (hmac_en ? (st_q == StMsg) & sha_rready : sha_rready);
	assign sha_rvalid = (!hmac_en ? fifo_rvalid : hmac_sha_rvalid);
	function automatic [31:0] sv2v_cast_32;
		input reg [31:0] inp;
		sv2v_cast_32 = inp;
	endfunction
	function automatic [WordByte - 1:0] sv2v_cast_71A47;
		input reg [WordByte - 1:0] inp;
		sv2v_cast_71A47 = inp;
	endfunction
	localparam [1:0] SelFifo = 2;
	localparam [1:0] SelIPad = 0;
	localparam [1:0] SelOPad = 1;
	assign sha_rdata = (!hmac_en ? fifo_rdata : (sel_rdata == SelIPad ? {sv2v_cast_32(i_pad[(BlockSize - 1) - (32 * pad_index)-:32]), sv2v_cast_71A47(1'sb1)} : (sel_rdata == SelOPad ? {sv2v_cast_32(o_pad[(BlockSize - 1) - (32 * pad_index)-:32]), sv2v_cast_71A47(1'sb1)} : (sel_rdata == SelFifo ? fifo_rdata : {32 + WordByte {1'sb0}}))));
	localparam [0:0] SelIPadMsg = 0;
	localparam [0:0] SelOPadMsg = 1;
	assign sha_message_length = (!hmac_en ? message_length : (sel_msglen == SelIPadMsg ? message_length + BlockSize : (sel_msglen == SelOPadMsg ? BlockSize + 256 : {64 {1'sb0}})));
	assign txcnt_eq_blksz = txcount[BlockSizeBits:0] == BlockSize;
	assign inc_txcount = sha_rready && sha_rvalid;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			txcount <= {64 {1'sb0}};
		else if (clr_txcount)
			txcount <= {64 {1'sb0}};
		else if (inc_txcount)
			txcount[63:5] <= txcount[63:5] + 1'b1;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			reg_hash_process_flag <= 1'b0;
		else if (reg_hash_process)
			reg_hash_process_flag <= 1'b1;
		else if (hmac_hash_done || reg_hash_start)
			reg_hash_process_flag <= 1'b0;
	localparam [0:0] Inner = 0;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			round_q <= Inner;
		else if (update_round)
			round_q <= round_d;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			fifo_wdata_sel <= 3'h0;
		else if (clr_fifo_wdata_sel)
			fifo_wdata_sel <= 3'h0;
		else if (fifo_wsel && fifo_wvalid)
			fifo_wdata_sel <= fifo_wdata_sel + 1'b1;
	assign sel_msglen = (round_q == Inner ? SelIPadMsg : SelOPadMsg);
	localparam [2:0] StIdle = 0;
	always @(posedge clk_i or negedge rst_ni) begin : state_ff
		if (!rst_ni)
			st_q <= StIdle;
		else
			st_q <= st_d;
	end
	localparam [0:0] Outer = 1;
	localparam [2:0] StDone = 6;
	localparam [2:0] StIPad = 1;
	localparam [2:0] StOPad = 5;
	localparam [2:0] StPushToMsgFifo = 3;
	localparam [2:0] StWaitResp = 4;
	always @(*) begin : next_state
		hmac_hash_done = 1'b0;
		hmac_sha_rvalid = 1'b0;
		clr_txcount = 1'b0;
		update_round = 1'b0;
		round_d = Inner;
		fifo_wsel = 1'b0;
		fifo_wvalid = 1'b0;
		clr_fifo_wdata_sel = 1'b1;
		sel_rdata = SelFifo;
		hash_start = 1'b0;
		hash_process = 1'b0;
		case (st_q)
			StIdle:
				if (hmac_en && reg_hash_start) begin
					st_d = StIPad;
					clr_txcount = 1'b1;
					update_round = 1'b1;
					round_d = Inner;
					hash_start = 1'b1;
				end
				else
					st_d = StIdle;
			StIPad: begin
				sel_rdata = SelIPad;
				if (txcnt_eq_blksz) begin
					st_d = StMsg;
					hmac_sha_rvalid = 1'b0;
				end
				else begin
					st_d = StIPad;
					hmac_sha_rvalid = 1'b1;
				end
			end
			StMsg: begin
				sel_rdata = SelFifo;
				if ((((round_q == Inner) && reg_hash_process_flag) || (round_q == Outer)) && (txcount >= sha_message_length)) begin
					st_d = StWaitResp;
					hmac_sha_rvalid = 1'b0;
					hash_process = round_q == Outer;
				end
				else begin
					st_d = StMsg;
					hmac_sha_rvalid = fifo_rvalid;
				end
			end
			StWaitResp: begin
				hmac_sha_rvalid = 1'b0;
				if (sha_hash_done) begin
					if (round_q == Outer)
						st_d = StDone;
					else
						st_d = StPushToMsgFifo;
				end
				else
					st_d = StWaitResp;
			end
			StPushToMsgFifo: begin
				hmac_sha_rvalid = 1'b0;
				fifo_wsel = 1'b1;
				fifo_wvalid = 1'b1;
				clr_fifo_wdata_sel = 1'b0;
				if (fifo_wready && (fifo_wdata_sel == 3'h7)) begin
					st_d = StOPad;
					clr_txcount = 1'b1;
					update_round = 1'b1;
					round_d = Outer;
					hash_start = 1'b1;
				end
				else
					st_d = StPushToMsgFifo;
			end
			StOPad: begin
				sel_rdata = SelOPad;
				if (txcnt_eq_blksz) begin
					st_d = StMsg;
					hmac_sha_rvalid = 1'b0;
				end
				else begin
					st_d = StOPad;
					hmac_sha_rvalid = 1'b1;
				end
			end
			StDone: begin
				st_d = StIdle;
				hmac_hash_done = 1'b1;
			end
			default: st_d = StIdle;
		endcase
	end
endmodule
module hmac (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	intr_hmac_done_o,
	intr_fifo_full_o,
	intr_hmac_err_o,
	alert_rx_i,
	alert_tx_o
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	localparam signed [31:0] NumAlerts = 1;
	localparam [NumAlerts - 1:0] AlertAsyncOn = 1'b1;
	localparam signed [31:0] MsgFifoDepth = 16;
	localparam signed [31:0] NumRound = 64;
	localparam signed [31:0] WordByte = 4;
	localparam [255:0] InitHash = {32'h6a09e667, 32'hbb67ae85, 32'h3c6ef372, 32'ha54ff53a, 32'h510e527f, 32'h9b05688c, 32'h1f83d9ab, 32'h5be0cd19};
	localparam [2047:0] CubicRootPrime = {32'h428a2f98, 32'h71374491, 32'hb5c0fbcf, 32'he9b5dba5, 32'h3956c25b, 32'h59f111f1, 32'h923f82a4, 32'hab1c5ed5, 32'hd807aa98, 32'h12835b01, 32'h243185be, 32'h550c7dc3, 32'h72be5d74, 32'h80deb1fe, 32'h9bdc06a7, 32'hc19bf174, 32'he49b69c1, 32'hefbe4786, 32'h0fc19dc6, 32'h240ca1cc, 32'h2de92c6f, 32'h4a7484aa, 32'h5cb0a9dc, 32'h76f988da, 32'h983e5152, 32'ha831c66d, 32'hb00327c8, 32'hbf597fc7, 32'hc6e00bf3, 32'hd5a79147, 32'h06ca6351, 32'h14292967, 32'h27b70a85, 32'h2e1b2138, 32'h4d2c6dfc, 32'h53380d13, 32'h650a7354, 32'h766a0abb, 32'h81c2c92e, 32'h92722c85, 32'ha2bfe8a1, 32'ha81a664b, 32'hc24b8b70, 32'hc76c51a3, 32'hd192e819, 32'hd6990624, 32'hf40e3585, 32'h106aa070, 32'h19a4c116, 32'h1e376c08, 32'h2748774c, 32'h34b0bcb5, 32'h391c0cb3, 32'h4ed8aa4a, 32'h5b9cca4f, 32'h682e6ff3, 32'h748f82ee, 32'h78a5636f, 32'h84c87814, 32'h8cc70208, 32'h90befffa, 32'ha4506ceb, 32'hbef9a3f7, 32'hc67178f2};
	function automatic [31:0] conv_endian;
		input reg [31:0] v;
		input reg swap;
		reg [31:0] conv_data;
		reg [31:0] _sv2v_strm_61C2C_inp;
		reg [31:0] _sv2v_strm_61C2C_out;
		integer _sv2v_strm_61C2C_idx;
		begin
			_sv2v_strm_61C2C_inp = v;
			for (_sv2v_strm_61C2C_idx = 0; _sv2v_strm_61C2C_idx <= 24; _sv2v_strm_61C2C_idx = _sv2v_strm_61C2C_idx + 8)
				_sv2v_strm_61C2C_out[31 - _sv2v_strm_61C2C_idx-:8] = _sv2v_strm_61C2C_inp[_sv2v_strm_61C2C_idx+:8];
			conv_data = _sv2v_strm_61C2C_out << 0;
			conv_endian = (swap ? conv_data : v);
		end
	endfunction
	function automatic [31:0] rotr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		rotr = (v >> amt) | (v << (32 - amt));
	endfunction
	function automatic [31:0] shiftr;
		input reg [31:0] v;
		input reg signed [31:0] amt;
		shiftr = v >> amt;
	endfunction
	function automatic [255:0] compress;
		input reg [31:0] w;
		input reg [31:0] k;
		input reg [255:0] h_i;
		reg [31:0] sigma_0;
		reg [31:0] sigma_1;
		reg [31:0] ch;
		reg [31:0] maj;
		reg [31:0] temp1;
		reg [31:0] temp2;
		begin
			sigma_1 = (rotr(h_i[128+:32], 6) ^ rotr(h_i[128+:32], 11)) ^ rotr(h_i[128+:32], 25);
			ch = (h_i[128+:32] & h_i[160+:32]) ^ (~h_i[128+:32] & h_i[192+:32]);
			temp1 = (((h_i[224+:32] + sigma_1) + ch) + k) + w;
			sigma_0 = (rotr(h_i[0+:32], 2) ^ rotr(h_i[0+:32], 13)) ^ rotr(h_i[0+:32], 22);
			maj = ((h_i[0+:32] & h_i[32+:32]) ^ (h_i[0+:32] & h_i[64+:32])) ^ (h_i[32+:32] & h_i[64+:32]);
			temp2 = sigma_0 + maj;
			compress[224+:32] = h_i[192+:32];
			compress[192+:32] = h_i[160+:32];
			compress[160+:32] = h_i[128+:32];
			compress[128+:32] = h_i[96+:32] + temp1;
			compress[96+:32] = h_i[64+:32];
			compress[64+:32] = h_i[32+:32];
			compress[32+:32] = h_i[0+:32];
			compress[0+:32] = temp1 + temp2;
		end
	endfunction
	function automatic [31:0] calc_w;
		input reg [31:0] w_0;
		input reg [31:0] w_1;
		input reg [31:0] w_9;
		input reg [31:0] w_14;
		reg [31:0] sum0;
		reg [31:0] sum1;
		begin
			sum0 = (rotr(w_1, 7) ^ rotr(w_1, 18)) ^ shiftr(w_1, 3);
			sum1 = (rotr(w_14, 17) ^ rotr(w_14, 19)) ^ shiftr(w_14, 10);
			calc_w = ((w_0 + sum0) + w_9) + sum1;
		end
	endfunction
	localparam [31:0] NoError = 32'h00000000;
	localparam [31:0] SwPushMsgWhenShaDisabled = 32'h00000001;
	localparam [31:0] SwHashStartWhenShaDisabled = 32'h00000002;
	localparam [31:0] SwUpdateSecretKeyInProcess = 32'h00000003;
	localparam signed [31:0] NumWords = 8;
	localparam [11:0] HMAC_INTR_STATE_OFFSET = 12'h000;
	localparam [11:0] HMAC_INTR_ENABLE_OFFSET = 12'h004;
	localparam [11:0] HMAC_INTR_TEST_OFFSET = 12'h008;
	localparam [11:0] HMAC_CFG_OFFSET = 12'h00c;
	localparam [11:0] HMAC_CMD_OFFSET = 12'h010;
	localparam [11:0] HMAC_STATUS_OFFSET = 12'h014;
	localparam [11:0] HMAC_ERR_CODE_OFFSET = 12'h018;
	localparam [11:0] HMAC_WIPE_SECRET_OFFSET = 12'h01c;
	localparam [11:0] HMAC_KEY0_OFFSET = 12'h020;
	localparam [11:0] HMAC_KEY1_OFFSET = 12'h024;
	localparam [11:0] HMAC_KEY2_OFFSET = 12'h028;
	localparam [11:0] HMAC_KEY3_OFFSET = 12'h02c;
	localparam [11:0] HMAC_KEY4_OFFSET = 12'h030;
	localparam [11:0] HMAC_KEY5_OFFSET = 12'h034;
	localparam [11:0] HMAC_KEY6_OFFSET = 12'h038;
	localparam [11:0] HMAC_KEY7_OFFSET = 12'h03c;
	localparam [11:0] HMAC_DIGEST0_OFFSET = 12'h040;
	localparam [11:0] HMAC_DIGEST1_OFFSET = 12'h044;
	localparam [11:0] HMAC_DIGEST2_OFFSET = 12'h048;
	localparam [11:0] HMAC_DIGEST3_OFFSET = 12'h04c;
	localparam [11:0] HMAC_DIGEST4_OFFSET = 12'h050;
	localparam [11:0] HMAC_DIGEST5_OFFSET = 12'h054;
	localparam [11:0] HMAC_DIGEST6_OFFSET = 12'h058;
	localparam [11:0] HMAC_DIGEST7_OFFSET = 12'h05c;
	localparam [11:0] HMAC_MSG_LENGTH_LOWER_OFFSET = 12'h060;
	localparam [11:0] HMAC_MSG_LENGTH_UPPER_OFFSET = 12'h064;
	localparam [11:0] HMAC_MSG_FIFO_OFFSET = 12'h800;
	localparam [11:0] HMAC_MSG_FIFO_SIZE = 12'h800;
	localparam signed [31:0] HMAC_INTR_STATE = 0;
	localparam signed [31:0] HMAC_INTR_ENABLE = 1;
	localparam signed [31:0] HMAC_INTR_TEST = 2;
	localparam signed [31:0] HMAC_CFG = 3;
	localparam signed [31:0] HMAC_CMD = 4;
	localparam signed [31:0] HMAC_STATUS = 5;
	localparam signed [31:0] HMAC_ERR_CODE = 6;
	localparam signed [31:0] HMAC_WIPE_SECRET = 7;
	localparam signed [31:0] HMAC_KEY0 = 8;
	localparam signed [31:0] HMAC_KEY1 = 9;
	localparam signed [31:0] HMAC_KEY2 = 10;
	localparam signed [31:0] HMAC_KEY3 = 11;
	localparam signed [31:0] HMAC_KEY4 = 12;
	localparam signed [31:0] HMAC_KEY5 = 13;
	localparam signed [31:0] HMAC_KEY6 = 14;
	localparam signed [31:0] HMAC_KEY7 = 15;
	localparam signed [31:0] HMAC_DIGEST0 = 16;
	localparam signed [31:0] HMAC_DIGEST1 = 17;
	localparam signed [31:0] HMAC_DIGEST2 = 18;
	localparam signed [31:0] HMAC_DIGEST3 = 19;
	localparam signed [31:0] HMAC_DIGEST4 = 20;
	localparam signed [31:0] HMAC_DIGEST5 = 21;
	localparam signed [31:0] HMAC_DIGEST6 = 22;
	localparam signed [31:0] HMAC_DIGEST7 = 23;
	localparam signed [31:0] HMAC_MSG_LENGTH_LOWER = 24;
	localparam signed [31:0] HMAC_MSG_LENGTH_UPPER = 25;
	localparam [103:0] HMAC_PERMIT = {4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0001, 4'b0011, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111};
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire intr_hmac_done_o;
	output wire intr_fifo_full_o;
	output wire intr_hmac_err_o;
	input wire [(NumAlerts * 4) - 1:0] alert_rx_i;
	output wire [(NumAlerts * 2) - 1:0] alert_tx_o;
	wire [320:0] reg2hw;
	wire [627:0] hw2reg;
	wire [(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16 : (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)) + ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 15)):(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)] tl_win_h2d;
	wire [(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1 : (1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)) + (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW)):(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)] tl_win_d2h;
	reg [255:0] secret_key;
	wire wipe_secret;
	wire [31:0] wipe_v;
	wire fifo_rvalid;
	wire fifo_rready;
	wire [(32 + WordByte) - 1:0] fifo_rdata;
	wire fifo_wvalid;
	wire fifo_wready;
	wire [(32 + WordByte) - 1:0] fifo_wdata;
	wire fifo_full;
	wire fifo_empty;
	wire [4:0] fifo_depth;
	wire msg_fifo_req;
	wire msg_fifo_gnt;
	wire msg_fifo_we;
	wire [8:0] msg_fifo_addr;
	wire [31:0] msg_fifo_wdata;
	wire [31:0] msg_fifo_wmask;
	wire [31:0] msg_fifo_rdata;
	wire msg_fifo_rvalid;
	wire [1:0] msg_fifo_rerror;
	wire [31:0] msg_fifo_wdata_endian;
	wire [31:0] msg_fifo_wmask_endian;
	wire packer_ready;
	wire packer_flush_done;
	wire reg_fifo_wvalid;
	wire [31:0] reg_fifo_wdata;
	wire [31:0] reg_fifo_wmask;
	wire hmac_fifo_wsel;
	wire hmac_fifo_wvalid;
	wire [2:0] hmac_fifo_wdata_sel;
	wire shaf_rvalid;
	wire [(32 + WordByte) - 1:0] shaf_rdata;
	wire shaf_rready;
	wire sha_en;
	wire hmac_en;
	wire endian_swap;
	wire digest_swap;
	wire reg_hash_start;
	wire sha_hash_start;
	wire hash_start;
	wire reg_hash_process;
	wire sha_hash_process;
	wire reg_hash_done;
	wire sha_hash_done;
	reg [63:0] message_length;
	wire [63:0] sha_message_length;
	reg [31:0] err_code;
	wire err_valid;
	wire [255:0] digest;
	reg [7:0] cfg_reg;
	reg cfg_block;
	assign hw2reg[616] = fifo_full;
	assign hw2reg[617] = fifo_empty;
	assign hw2reg[615-:5] = fifo_depth;
	assign wipe_secret = reg2hw[264];
	assign wipe_v = reg2hw[296-:32];
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			secret_key <= {256 {1'sb0}};
		else if (wipe_secret)
			secret_key <= secret_key ^ {8 {wipe_v}};
		else if (!cfg_block) begin : sv2v_autoblock_522
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				if (reg2hw[(7 - i) * 33])
					secret_key[32 * i+:32] <= reg2hw[((7 - i) * 33) + 32-:32];
		end
	generate
		genvar i;
		for (i = 0; i < 8; i = i + 1) begin : gen_key_digest
			assign hw2reg[322 + (((7 - i) * 32) + 31)-:32] = {32 {1'sb0}};
			assign hw2reg[66 + ((i * 32) + 31)-:32] = conv_endian(digest[i * 32+:32], digest_swap);
		end
	endgenerate
	wire [3:0] unused_cfg_qe;
	assign unused_cfg_qe = {cfg_reg[4], cfg_reg[6], cfg_reg[2], cfg_reg[0]};
	assign sha_en = cfg_reg[5];
	assign hmac_en = cfg_reg[7];
	assign endian_swap = cfg_reg[3];
	assign digest_swap = cfg_reg[1];
	assign hw2reg[621] = cfg_reg[7];
	assign hw2reg[620] = cfg_reg[5];
	assign hw2reg[619] = cfg_reg[3];
	assign hw2reg[618] = cfg_reg[1];
	assign reg_hash_start = reg2hw[299] & reg2hw[300];
	assign reg_hash_process = reg2hw[297] & reg2hw[298];
	assign hw2reg[578] = err_valid;
	assign hw2reg[610-:32] = err_code;
	assign hash_start = reg_hash_start & sha_en;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			cfg_block <= 1'sb0;
		else if (hash_start)
			cfg_block <= 1'b1;
		else if (reg_hash_done)
			cfg_block <= 1'b0;
	function automatic [1:0] sv2v_cast_2;
		input reg [1:0] inp;
		sv2v_cast_2 = inp;
	endfunction
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			cfg_reg <= {sv2v_cast_2({1'sb0, 1'sb0}), sv2v_cast_2({1'sb0, 1'sb0}), sv2v_cast_2({1'b1, 1'b0}), sv2v_cast_2({1'sb0, 1'sb0})};
		else if (!cfg_block && reg2hw[307])
			cfg_reg <= reg2hw[308-:8];
	reg fifo_full_q;
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			fifo_full_q <= 1'b0;
		else
			fifo_full_q <= fifo_full;
	wire fifo_full_event;
	assign fifo_full_event = fifo_full & !fifo_full_q;
	wire [2:0] event_intr;
	assign event_intr = {err_valid, fifo_full_event, reg_hash_done};
	prim_intr_hw #(.Width(1)) intr_hw_hmac_done(
		.event_intr_i(event_intr[0]),
		.reg2hw_intr_enable_q_i(reg2hw[317]),
		.reg2hw_intr_test_q_i(reg2hw[314]),
		.reg2hw_intr_test_qe_i(reg2hw[313]),
		.reg2hw_intr_state_q_i(reg2hw[320]),
		.hw2reg_intr_state_de_o(hw2reg[626]),
		.hw2reg_intr_state_d_o(hw2reg[627]),
		.intr_o(intr_hmac_done_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_fifo_full(
		.event_intr_i(event_intr[1]),
		.reg2hw_intr_enable_q_i(reg2hw[316]),
		.reg2hw_intr_test_q_i(reg2hw[312]),
		.reg2hw_intr_test_qe_i(reg2hw[311]),
		.reg2hw_intr_state_q_i(reg2hw[319]),
		.hw2reg_intr_state_de_o(hw2reg[624]),
		.hw2reg_intr_state_d_o(hw2reg[625]),
		.intr_o(intr_fifo_full_o)
	);
	prim_intr_hw #(.Width(1)) intr_hw_hmac_err(
		.event_intr_i(event_intr[2]),
		.reg2hw_intr_enable_q_i(reg2hw[315]),
		.reg2hw_intr_test_q_i(reg2hw[310]),
		.reg2hw_intr_test_qe_i(reg2hw[309]),
		.reg2hw_intr_state_q_i(reg2hw[318]),
		.hw2reg_intr_state_de_o(hw2reg[622]),
		.hw2reg_intr_state_d_o(hw2reg[623]),
		.intr_o(intr_hmac_err_o)
	);
	assign msg_fifo_rvalid = msg_fifo_req & ~msg_fifo_we;
	assign msg_fifo_rdata = {32 {1'sb1}};
	assign msg_fifo_rerror = {2 {1'sb1}};
	assign msg_fifo_gnt = (msg_fifo_req & ~hmac_fifo_wsel) & packer_ready;
	wire [(32 + WordByte) - 1:0] reg_fifo_wentry;
	assign reg_fifo_wentry[WordByte + 31-:((WordByte + 31) - WordByte) + 1] = conv_endian(reg_fifo_wdata, 1'b1);
	assign reg_fifo_wentry[WordByte - 1-:WordByte] = {reg_fifo_wmask[0], reg_fifo_wmask[8], reg_fifo_wmask[16], reg_fifo_wmask[24]};
	assign fifo_full = ~fifo_wready;
	assign fifo_empty = ~fifo_rvalid;
	assign fifo_wvalid = (hmac_fifo_wsel && fifo_wready ? hmac_fifo_wvalid : reg_fifo_wvalid);
	function automatic [31:0] sv2v_cast_32;
		input reg [31:0] inp;
		sv2v_cast_32 = inp;
	endfunction
	function automatic [WordByte - 1:0] sv2v_cast_71A47;
		input reg [WordByte - 1:0] inp;
		sv2v_cast_71A47 = inp;
	endfunction
	assign fifo_wdata = (hmac_fifo_wsel ? {sv2v_cast_32(digest[hmac_fifo_wdata_sel * 32+:32]), sv2v_cast_71A47(1'sb1)} : reg_fifo_wentry);
	prim_fifo_sync #(
		.Width(32 + WordByte),
		.Pass(1'b0),
		.Depth(MsgFifoDepth)
	) u_msg_fifo(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.clr_i(1'b0),
		.wvalid(fifo_wvalid & sha_en),
		.wready(fifo_wready),
		.wdata(fifo_wdata),
		.depth(fifo_depth),
		.rvalid(fifo_rvalid),
		.rready(fifo_rready),
		.rdata(fifo_rdata)
	);
	tlul_adapter_sram #(
		.SramAw(9),
		.SramDw(32),
		.Outstanding(1),
		.ByteAccess(1),
		.ErrOnRead(1)
	) u_tlul_adapter(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_win_h2d[(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16)+:(((((7 + top_pkg_TL_SZW) + 40) + top_pkg_TL_DBW) + 48) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 17 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16))]),
		.tl_o(tl_win_d2h[(((7 + top_pkg_TL_SZW) + 58) >= 0 ? 0 : (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1)+:(((7 + top_pkg_TL_SZW) + 58) >= 0 ? (((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 2 : 1 - ((((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1))]),
		.req_o(msg_fifo_req),
		.gnt_i(msg_fifo_gnt),
		.we_o(msg_fifo_we),
		.addr_o(msg_fifo_addr),
		.wdata_o(msg_fifo_wdata),
		.wmask_o(msg_fifo_wmask),
		.rdata_i(msg_fifo_rdata),
		.rvalid_i(msg_fifo_rvalid),
		.rerror_i(msg_fifo_rerror)
	);
	wire msg_write;
	assign msg_write = (msg_fifo_req & msg_fifo_we) & ~hmac_fifo_wsel;
	reg [5:0] wmask_ones;
	always @(*) begin
		wmask_ones = {6 {1'sb0}};
		begin : sv2v_autoblock_523
			reg signed [31:0] i;
			for (i = 0; i < 32; i = i + 1)
				wmask_ones = wmask_ones + reg_fifo_wmask[i];
		end
	end
	function automatic [63:0] sv2v_cast_64;
		input reg [63:0] inp;
		sv2v_cast_64 = inp;
	endfunction
	always @(posedge clk_i or negedge rst_ni)
		if (!rst_ni)
			message_length <= {64 {1'sb0}};
		else if (hash_start)
			message_length <= {64 {1'sb0}};
		else if ((reg_fifo_wvalid && fifo_wready) && !hmac_fifo_wsel)
			message_length <= message_length + sv2v_cast_64(wmask_ones);
	assign hw2reg[0] = 1'b1;
	assign hw2reg[32-:32] = message_length[63:32];
	assign hw2reg[33] = 1'b1;
	assign hw2reg[65-:32] = message_length[31:0];
	assign msg_fifo_wdata_endian = conv_endian(msg_fifo_wdata, ~endian_swap);
	assign msg_fifo_wmask_endian = conv_endian(msg_fifo_wmask, ~endian_swap);
	prim_packer #(
		.InW(32),
		.OutW(32)
	) u_packer(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.valid_i(msg_write & sha_en),
		.data_i(msg_fifo_wdata_endian),
		.mask_i(msg_fifo_wmask_endian),
		.ready_o(packer_ready),
		.valid_o(reg_fifo_wvalid),
		.data_o(reg_fifo_wdata),
		.mask_o(reg_fifo_wmask),
		.ready_i(fifo_wready & ~hmac_fifo_wsel),
		.flush_i(reg_hash_process),
		.flush_done_o(packer_flush_done)
	);
	hmac_core u_hmac(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.secret_key(secret_key),
		.wipe_secret(wipe_secret),
		.wipe_v(wipe_v),
		.hmac_en(hmac_en),
		.reg_hash_start(hash_start),
		.reg_hash_process(packer_flush_done),
		.hash_done(reg_hash_done),
		.sha_hash_start(sha_hash_start),
		.sha_hash_process(sha_hash_process),
		.sha_hash_done(sha_hash_done),
		.sha_rvalid(shaf_rvalid),
		.sha_rdata(shaf_rdata),
		.sha_rready(shaf_rready),
		.fifo_rvalid(fifo_rvalid),
		.fifo_rdata(fifo_rdata),
		.fifo_rready(fifo_rready),
		.fifo_wsel(hmac_fifo_wsel),
		.fifo_wvalid(hmac_fifo_wvalid),
		.fifo_wdata_sel(hmac_fifo_wdata_sel),
		.fifo_wready(fifo_wready),
		.message_length(message_length),
		.sha_message_length(sha_message_length)
	);
	sha2 u_sha2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.wipe_secret(wipe_secret),
		.wipe_v(wipe_v),
		.fifo_rvalid(shaf_rvalid),
		.fifo_rdata(shaf_rdata),
		.fifo_rready(shaf_rready),
		.sha_en(sha_en),
		.hash_start(sha_hash_start),
		.hash_process(sha_hash_process),
		.hash_done(sha_hash_done),
		.message_length(sha_message_length),
		.digest(digest)
	);
	hmac_reg_top u_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.tl_win_o(tl_win_h2d),
		.tl_win_i(tl_win_d2h),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	wire msg_push_sha_disabled;
	wire hash_start_sha_disabled;
	reg update_seckey_inprocess;
	assign msg_push_sha_disabled = msg_write & ~sha_en;
	assign hash_start_sha_disabled = reg_hash_start & ~sha_en;
	always @(*) begin
		update_seckey_inprocess = 1'b0;
		if (cfg_block) begin : sv2v_autoblock_524
			reg signed [31:0] i;
			for (i = 0; i < 8; i = i + 1)
				if (reg2hw[i * 33])
					update_seckey_inprocess = update_seckey_inprocess | 1'b1;
		end
		else
			update_seckey_inprocess = 1'b0;
	end
	assign err_valid = ~reg2hw[318] & ((msg_push_sha_disabled | hash_start_sha_disabled) | update_seckey_inprocess);
	always @(*) begin
		err_code = NoError;
		case (1'b1)
			msg_push_sha_disabled: err_code = SwPushMsgWhenShaDisabled;
			hash_start_sha_disabled: err_code = SwHashStartWhenShaDisabled;
			update_seckey_inprocess: err_code = SwUpdateSecretKeyInProcess;
			default: err_code = NoError;
		endcase
	end
	wire [NumAlerts - 1:0] alerts;
	assign alerts = msg_push_sha_disabled;
	localparam signed [31:0] hmac_pkg_NumAlerts = 1;
	localparam [hmac_pkg_NumAlerts - 1:0] hmac_pkg_AlertAsyncOn = 1'b1;
	generate
		genvar j;
		for (j = 0; j < hmac_pkg_NumAlerts; j = j + 1) begin : gen_alert_tx
			prim_alert_sender #(.AsyncOn(hmac_pkg_AlertAsyncOn[j])) i_prim_alert_sender(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.alert_i(alerts[j]),
				.alert_rx_i(alert_rx_i[j * 4+:4]),
				.alert_tx_o(alert_tx_o[j * 2+:2])
			);
		end
	endgenerate
endmodule
module nmi_gen_reg_top (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	reg2hw,
	hw2reg,
	devmode_i
);
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire [15:0] reg2hw;
	input wire [7:0] hw2reg;
	input devmode_i;
	localparam [3:0] NMI_GEN_INTR_STATE_OFFSET = 4'h0;
	localparam [3:0] NMI_GEN_INTR_ENABLE_OFFSET = 4'h4;
	localparam [3:0] NMI_GEN_INTR_TEST_OFFSET = 4'h8;
	localparam signed [31:0] NMI_GEN_INTR_STATE = 0;
	localparam signed [31:0] NMI_GEN_INTR_ENABLE = 1;
	localparam signed [31:0] NMI_GEN_INTR_TEST = 2;
	localparam [11:0] NMI_GEN_PERMIT = {4'b0001, 4'b0001, 4'b0001};
	localparam signed [31:0] AW = 4;
	localparam signed [31:0] DW = 32;
	localparam signed [31:0] DBW = DW / 8;
	wire reg_we;
	wire reg_re;
	wire [AW - 1:0] reg_addr;
	wire [DW - 1:0] reg_wdata;
	wire [DBW - 1:0] reg_be;
	wire [DW - 1:0] reg_rdata;
	wire reg_error;
	wire addrmiss;
	reg wr_err;
	reg [DW - 1:0] reg_rdata_next;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_reg_h2d;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_reg_d2h;
	assign tl_reg_h2d = tl_i;
	assign tl_o = tl_reg_d2h;
	tlul_adapter_reg #(
		.RegAw(AW),
		.RegDw(DW)
	) u_reg_if(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_reg_h2d),
		.tl_o(tl_reg_d2h),
		.we_o(reg_we),
		.re_o(reg_re),
		.addr_o(reg_addr),
		.wdata_o(reg_wdata),
		.be_o(reg_be),
		.rdata_i(reg_rdata),
		.error_i(reg_error)
	);
	assign reg_rdata = reg_rdata_next;
	assign reg_error = (devmode_i & addrmiss) | wr_err;
	wire intr_state_esc0_qs;
	wire intr_state_esc0_wd;
	wire intr_state_esc0_we;
	wire intr_state_esc1_qs;
	wire intr_state_esc1_wd;
	wire intr_state_esc1_we;
	wire intr_state_esc2_qs;
	wire intr_state_esc2_wd;
	wire intr_state_esc2_we;
	wire intr_state_esc3_qs;
	wire intr_state_esc3_wd;
	wire intr_state_esc3_we;
	wire intr_enable_esc0_qs;
	wire intr_enable_esc0_wd;
	wire intr_enable_esc0_we;
	wire intr_enable_esc1_qs;
	wire intr_enable_esc1_wd;
	wire intr_enable_esc1_we;
	wire intr_enable_esc2_qs;
	wire intr_enable_esc2_wd;
	wire intr_enable_esc2_we;
	wire intr_enable_esc3_qs;
	wire intr_enable_esc3_wd;
	wire intr_enable_esc3_we;
	wire intr_test_esc0_wd;
	wire intr_test_esc0_we;
	wire intr_test_esc1_wd;
	wire intr_test_esc1_we;
	wire intr_test_esc2_wd;
	wire intr_test_esc2_we;
	wire intr_test_esc3_wd;
	wire intr_test_esc3_we;
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_esc0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_esc0_we),
		.wd(intr_state_esc0_wd),
		.de(hw2reg[6]),
		.d(hw2reg[7]),
		.qe(),
		.q(reg2hw[15]),
		.qs(intr_state_esc0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_esc1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_esc1_we),
		.wd(intr_state_esc1_wd),
		.de(hw2reg[4]),
		.d(hw2reg[5]),
		.qe(),
		.q(reg2hw[14]),
		.qs(intr_state_esc1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_esc2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_esc2_we),
		.wd(intr_state_esc2_wd),
		.de(hw2reg[2]),
		.d(hw2reg[3]),
		.qe(),
		.q(reg2hw[13]),
		.qs(intr_state_esc2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(24),
		.SWACCESS("W1C"),
		.RESVAL(1'h0)
	) u_intr_state_esc3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_state_esc3_we),
		.wd(intr_state_esc3_wd),
		.de(hw2reg[0]),
		.d(hw2reg[1]),
		.qe(),
		.q(reg2hw[12]),
		.qs(intr_state_esc3_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_esc0(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_esc0_we),
		.wd(intr_enable_esc0_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[11]),
		.qs(intr_enable_esc0_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_esc1(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_esc1_we),
		.wd(intr_enable_esc1_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[10]),
		.qs(intr_enable_esc1_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_esc2(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_esc2_we),
		.wd(intr_enable_esc2_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[9]),
		.qs(intr_enable_esc2_qs)
	);
	prim_subreg #(
		.DW(1),
		._sv2v_width_SWACCESS(16),
		.SWACCESS("RW"),
		.RESVAL(1'h0)
	) u_intr_enable_esc3(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.we(intr_enable_esc3_we),
		.wd(intr_enable_esc3_wd),
		.de(1'b0),
		.d(1'sb0),
		.qe(),
		.q(reg2hw[8]),
		.qs(intr_enable_esc3_qs)
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_esc0(
		.re(1'b0),
		.we(intr_test_esc0_we),
		.wd(intr_test_esc0_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[6]),
		.q(reg2hw[7]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_esc1(
		.re(1'b0),
		.we(intr_test_esc1_we),
		.wd(intr_test_esc1_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[4]),
		.q(reg2hw[5]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_esc2(
		.re(1'b0),
		.we(intr_test_esc2_we),
		.wd(intr_test_esc2_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[2]),
		.q(reg2hw[3]),
		.qs()
	);
	prim_subreg_ext #(.DW(1)) u_intr_test_esc3(
		.re(1'b0),
		.we(intr_test_esc3_we),
		.wd(intr_test_esc3_wd),
		.d(1'sb0),
		.qre(),
		.qe(reg2hw[0]),
		.q(reg2hw[1]),
		.qs()
	);
	reg [2:0] addr_hit;
	always @(*) begin
		addr_hit = {3 {1'sb0}};
		addr_hit[0] = reg_addr == NMI_GEN_INTR_STATE_OFFSET;
		addr_hit[1] = reg_addr == NMI_GEN_INTR_ENABLE_OFFSET;
		addr_hit[2] = reg_addr == NMI_GEN_INTR_TEST_OFFSET;
	end
	assign addrmiss = (reg_re || reg_we ? ~|addr_hit : 1'b0);
	always @(*) begin
		wr_err = 1'b0;
		if ((addr_hit[0] && reg_we) && (NMI_GEN_PERMIT[8+:4] != (NMI_GEN_PERMIT[8+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[1] && reg_we) && (NMI_GEN_PERMIT[4+:4] != (NMI_GEN_PERMIT[4+:4] & reg_be)))
			wr_err = 1'b1;
		if ((addr_hit[2] && reg_we) && (NMI_GEN_PERMIT[0+:4] != (NMI_GEN_PERMIT[0+:4] & reg_be)))
			wr_err = 1'b1;
	end
	assign intr_state_esc0_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_esc0_wd = reg_wdata[0];
	assign intr_state_esc1_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_esc1_wd = reg_wdata[1];
	assign intr_state_esc2_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_esc2_wd = reg_wdata[2];
	assign intr_state_esc3_we = (addr_hit[0] & reg_we) & ~wr_err;
	assign intr_state_esc3_wd = reg_wdata[3];
	assign intr_enable_esc0_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_esc0_wd = reg_wdata[0];
	assign intr_enable_esc1_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_esc1_wd = reg_wdata[1];
	assign intr_enable_esc2_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_esc2_wd = reg_wdata[2];
	assign intr_enable_esc3_we = (addr_hit[1] & reg_we) & ~wr_err;
	assign intr_enable_esc3_wd = reg_wdata[3];
	assign intr_test_esc0_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_esc0_wd = reg_wdata[0];
	assign intr_test_esc1_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_esc1_wd = reg_wdata[1];
	assign intr_test_esc2_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_esc2_wd = reg_wdata[2];
	assign intr_test_esc3_we = (addr_hit[2] & reg_we) & ~wr_err;
	assign intr_test_esc3_wd = reg_wdata[3];
	always @(*) begin
		reg_rdata_next = {DW {1'sb0}};
		case (1'b1)
			addr_hit[0]: begin
				reg_rdata_next[0] = intr_state_esc0_qs;
				reg_rdata_next[1] = intr_state_esc1_qs;
				reg_rdata_next[2] = intr_state_esc2_qs;
				reg_rdata_next[3] = intr_state_esc3_qs;
			end
			addr_hit[1]: begin
				reg_rdata_next[0] = intr_enable_esc0_qs;
				reg_rdata_next[1] = intr_enable_esc1_qs;
				reg_rdata_next[2] = intr_enable_esc2_qs;
				reg_rdata_next[3] = intr_enable_esc3_qs;
			end
			addr_hit[2]: begin
				reg_rdata_next[0] = 1'sb0;
				reg_rdata_next[1] = 1'sb0;
				reg_rdata_next[2] = 1'sb0;
				reg_rdata_next[3] = 1'sb0;
			end
			default: reg_rdata_next = {DW {1'sb1}};
		endcase
	end
endmodule
module nmi_gen (
	clk_i,
	rst_ni,
	tl_i,
	tl_o,
	intr_esc0_o,
	intr_esc1_o,
	intr_esc2_o,
	intr_esc3_o,
	esc_tx_i,
	esc_rx_o
);
	localparam integer ImplGeneric = 0;
	localparam integer ImplXilinx = 1;
	localparam [31:0] N_ESC_SEV = 4;
	input clk_i;
	input rst_ni;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	input wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] tl_i;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	output wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] tl_o;
	output wire intr_esc0_o;
	output wire intr_esc1_o;
	output wire intr_esc2_o;
	output wire intr_esc3_o;
	input wire [(N_ESC_SEV * 2) - 1:0] esc_tx_i;
	output wire [(N_ESC_SEV * 2) - 1:0] esc_rx_o;
	wire [N_ESC_SEV - 1:0] esc_en;
	wire [15:0] reg2hw;
	wire [7:0] hw2reg;
	nmi_gen_reg_top i_reg(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(tl_i),
		.tl_o(tl_o),
		.reg2hw(reg2hw),
		.hw2reg(hw2reg),
		.devmode_i(1'b1)
	);
	prim_intr_hw #(.Width(1)) i_intr_esc0(
		.event_intr_i(esc_en[0]),
		.reg2hw_intr_enable_q_i(reg2hw[11]),
		.reg2hw_intr_test_q_i(reg2hw[7]),
		.reg2hw_intr_test_qe_i(reg2hw[6]),
		.reg2hw_intr_state_q_i(reg2hw[15]),
		.hw2reg_intr_state_de_o(hw2reg[6]),
		.hw2reg_intr_state_d_o(hw2reg[7]),
		.intr_o(intr_esc0_o)
	);
	prim_intr_hw #(.Width(1)) i_intr_esc1(
		.event_intr_i(esc_en[1]),
		.reg2hw_intr_enable_q_i(reg2hw[10]),
		.reg2hw_intr_test_q_i(reg2hw[5]),
		.reg2hw_intr_test_qe_i(reg2hw[4]),
		.reg2hw_intr_state_q_i(reg2hw[14]),
		.hw2reg_intr_state_de_o(hw2reg[4]),
		.hw2reg_intr_state_d_o(hw2reg[5]),
		.intr_o(intr_esc1_o)
	);
	prim_intr_hw #(.Width(1)) i_intr_esc2(
		.event_intr_i(esc_en[2]),
		.reg2hw_intr_enable_q_i(reg2hw[9]),
		.reg2hw_intr_test_q_i(reg2hw[3]),
		.reg2hw_intr_test_qe_i(reg2hw[2]),
		.reg2hw_intr_state_q_i(reg2hw[13]),
		.hw2reg_intr_state_de_o(hw2reg[2]),
		.hw2reg_intr_state_d_o(hw2reg[3]),
		.intr_o(intr_esc2_o)
	);
	prim_intr_hw #(.Width(1)) i_intr_esc3(
		.event_intr_i(esc_en[3]),
		.reg2hw_intr_enable_q_i(reg2hw[8]),
		.reg2hw_intr_test_q_i(reg2hw[1]),
		.reg2hw_intr_test_qe_i(reg2hw[0]),
		.reg2hw_intr_state_q_i(reg2hw[12]),
		.hw2reg_intr_state_de_o(hw2reg[0]),
		.hw2reg_intr_state_d_o(hw2reg[1]),
		.intr_o(intr_esc3_o)
	);
	generate
		genvar k;
		for (k = 0; k < N_ESC_SEV; k = k + 1) begin : gen_esc_sev
			prim_esc_receiver i_prim_esc_receiver(
				.clk_i(clk_i),
				.rst_ni(rst_ni),
				.esc_en_o(esc_en[k]),
				.esc_rx_o(esc_rx_o[k * 2+:2]),
				.esc_tx_i(esc_tx_i[k * 2+:2])
			);
		end
	endgenerate
endmodule
module otaes (
	clk_i,
	rst_ni,
	aes_input,
	aes_key,
	aes_output,
	test_done_o,
	test_passed_o
);
	input wire clk_i;
	input wire rst_ni;
	input wire [127:0] aes_input;
	input wire [127:0] aes_key;
	output reg [127:0] aes_output;
	output reg test_done_o;
	output reg test_passed_o;
	localparam [0:0] AES_ENC = 1'b0;
	localparam [0:0] AES_DEC = 1'b1;
	localparam [2:0] AES_128 = 3'b001;
	localparam [2:0] AES_192 = 3'b010;
	localparam [2:0] AES_256 = 3'b100;
	localparam [1:0] STATE_INIT = 0;
	localparam [1:0] STATE_ROUND = 1;
	localparam [1:0] STATE_CLEAR = 2;
	localparam [1:0] ADD_RK_INIT = 0;
	localparam [1:0] ADD_RK_ROUND = 1;
	localparam [1:0] ADD_RK_FINAL = 2;
	localparam [0:0] KEY_INIT_INPUT = 0;
	localparam [0:0] KEY_INIT_CLEAR = 1;
	localparam [1:0] KEY_FULL_ENC_INIT = 0;
	localparam [1:0] KEY_FULL_DEC_INIT = 1;
	localparam [1:0] KEY_FULL_ROUND = 2;
	localparam [1:0] KEY_FULL_CLEAR = 3;
	localparam [0:0] KEY_DEC_EXPAND = 0;
	localparam [0:0] KEY_DEC_CLEAR = 1;
	localparam [1:0] KEY_WORDS_0123 = 0;
	localparam [1:0] KEY_WORDS_2345 = 1;
	localparam [1:0] KEY_WORDS_4567 = 2;
	localparam [1:0] KEY_WORDS_ZERO = 3;
	localparam [0:0] ROUND_KEY_DIRECT = 0;
	localparam [0:0] ROUND_KEY_MIXED = 1;
	function automatic [7:0] aes_mul2;
		input reg [7:0] in;
		begin
			aes_mul2[7] = in[6];
			aes_mul2[6] = in[5];
			aes_mul2[5] = in[4];
			aes_mul2[4] = in[3] ^ in[7];
			aes_mul2[3] = in[2] ^ in[7];
			aes_mul2[2] = in[1];
			aes_mul2[1] = in[0] ^ in[7];
			aes_mul2[0] = in[7];
		end
	endfunction
	function automatic [7:0] aes_mul4;
		input reg [7:0] in;
		aes_mul4 = aes_mul2(aes_mul2(in));
	endfunction
	function automatic [7:0] aes_div2;
		input reg [7:0] in;
		begin
			aes_div2[7] = in[0];
			aes_div2[6] = in[7];
			aes_div2[5] = in[6];
			aes_div2[4] = in[5];
			aes_div2[3] = in[4] ^ in[0];
			aes_div2[2] = in[3] ^ in[0];
			aes_div2[1] = in[2];
			aes_div2[0] = in[1] ^ in[0];
		end
	endfunction
	function automatic [31:0] aes_circ_byte_shift;
		input reg [31:0] in;
		input integer shift;
		integer s;
		begin
			s = shift % 4;
			aes_circ_byte_shift = {in[8 * ((7 - s) % 4)+:8], in[8 * ((6 - s) % 4)+:8], in[8 * ((5 - s) % 4)+:8], in[8 * ((4 - s) % 4)+:8]};
		end
	endfunction
	function automatic [127:0] aes_transpose;
		input reg [127:0] in;
		reg [127:0] transpose;
		begin
			transpose = {128 {1'sb0}};
			begin : sv2v_autoblock_541
				reg signed [31:0] j;
				for (j = 0; j < 4; j = j + 1)
					begin : sv2v_autoblock_542
						reg signed [31:0] i;
						for (i = 0; i < 4; i = i + 1)
							transpose[((i * 4) + j) * 8+:8] = in[((j * 4) + i) * 8+:8];
					end
			end
			aes_transpose = transpose;
		end
	endfunction
	function automatic [31:0] aes_col_get;
		input reg [127:0] in;
		input reg signed [31:0] idx;
		reg signed [31:0] i;
		for (i = 0; i < 4; i = i + 1)
			aes_col_get[i * 8+:8] = in[((i * 4) + idx) * 8+:8];
	endfunction
	function automatic [7:0] aes_mvm;
		input reg [7:0] vec_b;
		input reg [63:0] mat_a;
		reg [7:0] vec_c;
		begin
			vec_c = {8 {1'sb0}};
			begin : sv2v_autoblock_543
				reg signed [31:0] i;
				for (i = 0; i < 8; i = i + 1)
					begin : sv2v_autoblock_544
						reg signed [31:0] j;
						for (j = 0; j < 8; j = j + 1)
							vec_c[i] = vec_c[i] ^ (mat_a[((7 - j) * 8) + i] & vec_b[7 - j]);
					end
			end
			aes_mvm = vec_c;
		end
	endfunction
	localparam signed [31:0] NumRegsKey = 8;
	localparam signed [31:0] NumRegsData = 4;
	localparam [6:0] AES_KEY0_OFFSET = 7'h00;
	localparam [6:0] AES_KEY1_OFFSET = 7'h04;
	localparam [6:0] AES_KEY2_OFFSET = 7'h08;
	localparam [6:0] AES_KEY3_OFFSET = 7'h0c;
	localparam [6:0] AES_KEY4_OFFSET = 7'h10;
	localparam [6:0] AES_KEY5_OFFSET = 7'h14;
	localparam [6:0] AES_KEY6_OFFSET = 7'h18;
	localparam [6:0] AES_KEY7_OFFSET = 7'h1c;
	localparam [6:0] AES_DATA_IN0_OFFSET = 7'h20;
	localparam [6:0] AES_DATA_IN1_OFFSET = 7'h24;
	localparam [6:0] AES_DATA_IN2_OFFSET = 7'h28;
	localparam [6:0] AES_DATA_IN3_OFFSET = 7'h2c;
	localparam [6:0] AES_DATA_OUT0_OFFSET = 7'h30;
	localparam [6:0] AES_DATA_OUT1_OFFSET = 7'h34;
	localparam [6:0] AES_DATA_OUT2_OFFSET = 7'h38;
	localparam [6:0] AES_DATA_OUT3_OFFSET = 7'h3c;
	localparam [6:0] AES_CTRL_OFFSET = 7'h40;
	localparam [6:0] AES_TRIGGER_OFFSET = 7'h44;
	localparam [6:0] AES_STATUS_OFFSET = 7'h48;
	localparam signed [31:0] AES_KEY0 = 0;
	localparam signed [31:0] AES_KEY1 = 1;
	localparam signed [31:0] AES_KEY2 = 2;
	localparam signed [31:0] AES_KEY3 = 3;
	localparam signed [31:0] AES_KEY4 = 4;
	localparam signed [31:0] AES_KEY5 = 5;
	localparam signed [31:0] AES_KEY6 = 6;
	localparam signed [31:0] AES_KEY7 = 7;
	localparam signed [31:0] AES_DATA_IN0 = 8;
	localparam signed [31:0] AES_DATA_IN1 = 9;
	localparam signed [31:0] AES_DATA_IN2 = 10;
	localparam signed [31:0] AES_DATA_IN3 = 11;
	localparam signed [31:0] AES_DATA_OUT0 = 12;
	localparam signed [31:0] AES_DATA_OUT1 = 13;
	localparam signed [31:0] AES_DATA_OUT2 = 14;
	localparam signed [31:0] AES_DATA_OUT3 = 15;
	localparam signed [31:0] AES_CTRL = 16;
	localparam signed [31:0] AES_TRIGGER = 17;
	localparam signed [31:0] AES_STATUS = 18;
	localparam [75:0] AES_PERMIT = {4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b1111, 4'b0001, 4'b0001, 4'b0001};
	localparam [2:0] PutFullData = 3'h0;
	localparam [2:0] PutPartialData = 3'h1;
	localparam [2:0] Get = 3'h4;
	localparam [2:0] AccessAck = 3'h0;
	localparam [2:0] AccessAckData = 3'h1;
	localparam top_pkg_TL_AIW = 8;
	localparam top_pkg_TL_AW = 32;
	localparam top_pkg_TL_DW = 32;
	localparam top_pkg_TL_DBW = top_pkg_TL_DW >> 3;
	localparam top_pkg_TL_SZW = $clog2($clog2(top_pkg_TL_DBW) + 1);
	reg [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_AW) + top_pkg_TL_DBW) + top_pkg_TL_DW) + 16:0] h2d;
	localparam top_pkg_TL_DIW = 1;
	localparam top_pkg_TL_DUW = 16;
	wire [(((((7 + top_pkg_TL_SZW) + top_pkg_TL_AIW) + top_pkg_TL_DIW) + top_pkg_TL_DW) + top_pkg_TL_DUW) + 1:0] d2h;
	aes aes(
		.clk_i(clk_i),
		.rst_ni(rst_ni),
		.tl_i(h2d),
		.tl_o(d2h)
	);
	reg [31:0] count;
	reg [7:0] fsm;
	always @(posedge clk_i or negedge rst_ni) begin : tb_ctrl
		if (!rst_ni) begin
			count <= 32'b00000000000000000000000000000000;
			test_done_o <= 1'b0;
			test_passed_o <= 1'b0;
			fsm <= 8'b00000000;
			h2d[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] <= 1'b0;
			h2d[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] <= 3'b000;
			h2d[3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49))) ? ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) + 1 : ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] <= 3'b000;
			h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= 32'hffffffff;
			h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'hffffffff;
			h2d[top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))-:((40 + (top_pkg_TL_DBW + 48)) >= (32 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) + 1 : ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) + 1)] <= 8'hde;
			h2d[top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))-:((top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48))) >= (40 + (top_pkg_TL_DBW + 49)) ? ((top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))) - (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))) + 1 : ((top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))) - (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) + 1)] <= 2'b10;
			h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
			h2d[16-:16] <= 16'hdead;
			h2d[0] <= 1'b1;
		end
		else begin
			count <= count + 32'b00000000000000000000000000000001;
			if (fsm == 8'b00000000)
				if (count > 32'h00000020) begin
					fsm <= {1'b1, AES_CTRL_OFFSET};
					count <= 32'b00000000000000000000000000000000;
					h2d[0] <= 1'b1;
					h2d[7 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))] <= 1'b1;
				end
			if (fsm == {1'b1, AES_CTRL_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_CTRL_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= {23'b00000000000000000000000, 1'b1, 3'b001, 4'b0001, 1'b0};
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 3) begin
					fsm <= {1'b1, AES_KEY0_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY0_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY0_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_key[31:0];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY1_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY1_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY1_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_key[63:32];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY2_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY2_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY2_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_key[95:64];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY3_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY3_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY3_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_key[127:96];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY4_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY4_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY4_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'b00000000000000000000000000000000;
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY5_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY5_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY5_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'b00000000000000000000000000000000;
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY6_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY6_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY6_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'b00000000000000000000000000000000;
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_KEY7_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_KEY7_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_KEY7_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'b00000000000000000000000000000000;
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_DATA_IN0_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_IN0_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_IN0_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_input[31:0];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_DATA_IN1_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_IN1_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_IN1_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_input[63:32];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_DATA_IN2_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_IN2_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_IN2_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_input[95:64];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_DATA_IN3_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_IN3_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_IN3_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= aes_input[127:96];
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_TRIGGER_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_TRIGGER_OFFSET}) begin
				if (count == 0) begin
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_TRIGGER_OFFSET};
					h2d[top_pkg_TL_DW + 16-:top_pkg_TL_DW] <= 32'b00000000000000000000000000000001;
					h2d[top_pkg_TL_DBW + (top_pkg_TL_DW + 16)-:((top_pkg_TL_DBW + 48) >= 49 ? ((top_pkg_TL_DBW + (top_pkg_TL_DW + 16)) - (top_pkg_TL_DW + 17)) + 1 : ((top_pkg_TL_DW + 17) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) + 1)] <= 4'b1111;
				end
				if (count == 1) begin
					fsm <= {1'b1, AES_STATUS_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_STATUS_OFFSET}) begin
				if ((count % 32'h00000008) == 0) begin
					h2d[6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))-:((6 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 48)))) >= (3 + (top_pkg_TL_SZW + (40 + (top_pkg_TL_DBW + 49)))) ? ((6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))))) - (3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))))))) + 1 : ((3 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 17)))))) - (6 + (top_pkg_TL_SZW + (top_pkg_TL_AIW + (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))))))) + 1)] <= 3'b001;
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_STATUS_OFFSET};
				end
				if (|(d2h[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1] & 32'b00000000000000000000000000000100)) begin
					fsm <= {1'b1, AES_DATA_OUT0_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_OUT0_OFFSET}) begin
				if (count == 0)
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_OUT0_OFFSET};
				if (count == 3) begin
					aes_output[31:0] <= d2h[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1];
					fsm <= {1'b1, AES_DATA_OUT1_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_OUT1_OFFSET}) begin
				if (count == 0)
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_OUT1_OFFSET};
				if (count == 3) begin
					aes_output[63:32] <= d2h[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1];
					fsm <= {1'b1, AES_DATA_OUT2_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_OUT2_OFFSET}) begin
				if (count == 0)
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_OUT2_OFFSET};
				if (count == 3) begin
					aes_output[95:64] <= d2h[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1];
					fsm <= {1'b1, AES_DATA_OUT3_OFFSET};
					count <= 32'b00000000000000000000000000000000;
				end
			end
			if (fsm == {1'b1, AES_DATA_OUT3_OFFSET}) begin
				if (count == 0)
					h2d[top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))-:((32 + (top_pkg_TL_DBW + 48)) >= (top_pkg_TL_DBW + 49) ? ((top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16))) - (top_pkg_TL_DBW + (top_pkg_TL_DW + 17))) + 1 : ((top_pkg_TL_DBW + (top_pkg_TL_DW + 17)) - (top_pkg_TL_AW + (top_pkg_TL_DBW + (top_pkg_TL_DW + 16)))) + 1)] <= {25'b0000000000000000000000000, AES_DATA_OUT3_OFFSET};
				if (count == 3) begin
					aes_output[127:96] <= d2h[top_pkg_TL_DW + (top_pkg_TL_DUW + 1)-:((top_pkg_TL_DW + (top_pkg_TL_DUW + 1)) - (top_pkg_TL_DUW + 2)) + 1];
					fsm <= 8'hff;
					count <= 32'b00000000000000000000000000000000;
					test_done_o <= 1'b1;
				end
			end
			if (fsm == 8'hff)
				test_done_o <= 1'b1;
			if (count > 32'h000000ff)
				test_done_o <= 1'b1;
		end
	end
endmodule
