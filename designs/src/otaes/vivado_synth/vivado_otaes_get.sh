#!/bin/bash

# First argument is GIT ID
set -e # Bail on errors

OTDIR=opentitan/
GITID=${1:-HEAD}
OUTDIR=otaes-$GITID
RTLHACKS=rtl/
LR_SYNTH_SRC_DIR="$OTDIR/hw/ip/aes"

echo Checking out $GITID
(cd $OTDIR && git -c advice.detachedHead=false checkout $GITID)

echo copying sources from $RTLHACKS and $OTDIR to $OUTDIR
# Get OpenTitan dependency sources
ot_dep_sources=("$LR_SYNTH_SRC_DIR"/../tlul/rtl/tlul_adapter_reg.sv
             "$LR_SYNTH_SRC_DIR"/../tlul/rtl/tlul_err.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_ext.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_shadow.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_arb.sv             
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_sender.sv
             #"$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_receiver.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_diff_decode.sv
	     "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_assert_standard_macros.svh
	     "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_assert_dummy_macros.svh
	     "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_assert.sv
             $RTLHACKS/prim_flop_2sync.sv
             "$LR_SYNTH_SRC_DIR"/../prim_generic/rtl/prim_generic_flop_2sync.sv
             $RTLHACKS/prim_flop.sv
             #"$LR_SYNTH_SRC_DIR"/../prim_xilinx/rtl/prim_xilinx_flop.sv - need patched version
             $RTLHACKS/prim_xilinx_flop.sv
             $RTLHACKS/prim_buf.sv
             $RTLHACKS/prim_xilinx_buf.sv
             #"$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_lfsr.sv - need a patched version
	     $RTLHACKS/otaes.sv
	     $RTLHACKS/opentitan_tb.v
             $RTLHACKS/prim_lfsr.sv)

rm -rf $OUTDIR
mkdir -p $OUTDIR
echo $GITID > $OUTDIR/gitid

for file in ${ot_dep_sources[@]} "$LR_SYNTH_SRC_DIR"/rtl/*.sv; do
  cp -f "$file" $OUTDIR/ 
done

for file in "$LR_SYNTH_SRC_DIR"/../../top_earlgrey/rtl/*_pkg.sv \
	    "$LR_SYNTH_SRC_DIR"/../tlul/rtl/*_pkg.sv \
	        "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_pkg.sv \
		    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_cipher_pkg.sv \
		        "$LR_SYNTH_SRC_DIR"/rtl/*_pkg.sv; do
  cp -f "$file" $OUTDIR/ 
done

echo Patching files in $OUTDIR
sed -i 's/(\* keep = "true" \*)//g' $OUTDIR/*  # Yosys chokes on this
