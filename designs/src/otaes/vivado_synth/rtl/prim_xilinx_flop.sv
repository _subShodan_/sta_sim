// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

// NOTE: This is a modified version of the OpenTitan version of prim_xilinx_flop.sv that is
// synthesizable with sv2v and Yosys.
//
// Reason for the modification: sv2v doesn't accept attributes on the input/output ports.

`include "prim_assert.sv"

module prim_xilinx_flop # (
  parameter int Width      = 1,
  localparam int WidthSubOne = Width-1,
  parameter logic [WidthSubOne:0] ResetValue = 0
) (
  input clk_i,
  input rst_ni,
  input [Width-1:0] d_i,
  output logic [Width-1:0] q_o
);

  // Prevent Vivado from optimizing this signal away.
  (* keep = "true" *) logic [Width-1:0] d, q;
  assign d = d_i;
  always_ff @(posedge clk_i or negedge rst_ni) begin
    if (!rst_ni) begin
      q <= ResetValue;
    end else begin
      q <= d;
    end
  end
  assign q_o = q;

endmodule // prim_xilinx_flop
