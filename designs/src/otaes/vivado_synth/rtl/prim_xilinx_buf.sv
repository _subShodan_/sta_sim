// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

module prim_xilinx_buf (
  input in_i,
  output logic out_o
);
  (* keep = "true" *) logic out;
  assign out = in_i;
  assign out_o = out;

endmodule : prim_xilinx_buf
