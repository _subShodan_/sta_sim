// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

// NOTE: This file is derived from an auto-generated file. The .* in the port list needs to be
// replaced by the actual port names to make Yosys accepting it.

module prim_buf (
  input in_i,
  output logic out_o
);

  prim_xilinx_buf u_impl_xilinx (
    .in_i,
    .out_o
  );

endmodule
