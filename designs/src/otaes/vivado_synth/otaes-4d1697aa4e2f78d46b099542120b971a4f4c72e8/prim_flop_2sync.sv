// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

// NOTE: This file is derived from an auto-generated file. The .* in the port list needs to be
// replaced by the actual port names to make Yosys accepting it.

module prim_flop_2sync #(
  parameter int Width       = 16,
  localparam int WidthSubOne = Width-1, // temp work around #2679
  parameter logic [WidthSubOne:0] ResetValue = '0
) (
  input                    clk_i,       // receive clock
  input                    rst_ni,
  input        [Width-1:0] d_i,
  output logic [Width-1:0] q_o
);

  prim_generic_flop_2sync #(
    .Width(Width),
    .ResetValue(ResetValue)
  ) u_impl_generic (
    .clk_i,
    .rst_ni,
    .d_i,
    .q_o
  );

endmodule
