#!/bin/sh

# Install particular otaes in the design flow

set -e

DESIGN=$1 # Should be like otaes-fd3708aa4d674ce7ed871e4f41ad7fc7abfa2f69

CELLSIM=cells_sim.v
OTAESV=$DESIGN/$DESIGN.v

DESIGN_DIR=../../../../designs/
CONFIG_DIR=$DESIGN_DIR/sky130/$DESIGN
SRC_DIR=$DESIGN_DIR/src/$DESIGN
TB_DIR=$DESIGN_DIR/tb

[ -x $DESIGN_DIR ] # Test

# Creat config files
echo Creating config files in $CONFIG_DIR
mkdir -p $CONFIG_DIR

cat > $CONFIG_DIR/config.mk <<ENDOFIT
export DESIGN_NICKNAME = $DESIGN
# DESIGN_NAME is the module top name
export DESIGN_NAME = otaes
export PLATFORM    = sky130

export VERILOG_FILES = \$(sort \$(wildcard ./designs/src/\$(DESIGN_NICKNAME)/opentitan.v))
export SDC_FILE      = ./designs/\$(PLATFORM)/\$(DESIGN_NICKNAME)/constraint.sdc

# These values must be multiples of placement site
export DIE_AREA    = 0 0 11200 10208
export CORE_AREA   = 10 12 11100 10112
export PLACE_DENSITY = 0.99 # Jasper added this, not sure if it's sane

ENDOFIT

echo "create_clock [get_ports clk] -period 4" > $CONFIG_DIR/constraint.sdc

# Create source
echo Creating source by cat $OTAESV $CELLSIM  to $SRC_DIR/opentitan.v
mkdir -p $SRC_DIR
cat $OTAESV $CELLSIM >$SRC_DIR/opentitan.v

# Create TB
echo Create TB link in $TB_DIR
cd $TB_DIR
rm -f $DESIGN
ln -s otaes $DESIGN
