#!/bin/sh

set -e

CHECKOUT=$1
VADO_V=otaes-elabvado-$CHECKOUT.v
ELAB_V=otaes-elab-$CHECKOUT.v
PATCH_V=otaes-elab-$CHECKOUT.v.patch
TARGETDIR=../../otaes-$CHECKOUT
TARGET=$TARGETDIR/opentitan.v
LIB=merged.lib
HILOCELL=sky130_fd_sc_hs__conb_1
HILOCMD="hilomap -singleton -hicell $HILOCELL HI -locell $HILOCELL LO"

if [ -e $TARGET ] && [ ! -L $TARGET ]; then
	echo $TARGET exists and is not a link
	exit 2
fi


if [ ! -e $ELAB_V ]; then
	echo $ELAB_V doesnt exist
	exit 1
fi

if [ ! -e $LIB ]; then
	echo $LIB doesnt exist. Copy e.g. merged.lib from one of the OpenROAD flows
	exit 3
fi

# Check we have the right cell lib
echo [*] Checking for correct lib
grep $HILOCELL $LIB

# Run vado
echo [*] Processing vivado elaborated netlist
python3 ../../../../tools/vado.py $ELAB_V ../../../../tools/vado_modules.v $VADO_V

# Fix PRE/CLR
echo [*] Before patching rst
grep rst_ni $VADO_V | sort -u
sed -i 's/CLR(rst_ni/CLR(~rst_ni/g' $VADO_V
sed -i 's/PRE(rst_ni/PRE(~rst_ni/g' $VADO_V

echo [*] After
grep rst_ni $VADO_V | sort -u 

# Fix dangling wire
echo [*] Patching with $PATCH_V
cat $PATCH_V | patch $VADO_V

# Try it works
echo [*] Testing with iverilog
iverilog $VADO_V ../vivado_synth/rtl/opentitan_tb.v -o otaes-vado
./otaes-vado
rm otaes-vado

# Now run yosys script over it to add 'keep' in the right places
#echo [*] Adding "keep" with yosys
#yosys -p "read_verilog $VADO_V; setattr -set keep 1 prim_flop*/d_i prim_flop*/q_o; write_verilog $VADO_V" >/tmp/yosys.log
echo [*] Synthesizing with yosys
# Use keep
#yosys -p "read_verilog $VADO_V; setattr -set keep 1 prim_flop*/d_i prim_flop*/q_o; synth -flatten; opt -purge; techmap; dfflibmap -liberty merged.lib; opt; abc  -liberty merged.lib; opt_clean -purge; hilomap -singleton; stat; check; write_verilog $VADO_V" >/tmp/yosys.log
# Use keep_hierarchy
yosys -p "read_verilog $VADO_V; flatten prim_flop*; setattr -set keep_hierarchy 1 prim_flop*; synth -flatten; opt -purge; techmap; dfflibmap -liberty $LIB; opt; abc  -liberty $LIB; opt_clean -purge; $HILOCMD; stat; check; write_verilog $VADO_V" >/tmp/yosys.log

# Link in the right place
echo [*] Adding src link
mkdir -p $TARGETDIR
rm -f $TARGET
#cd $TARGETDIR
ln -s ../otaes/vivado_elab/$VADO_V $TARGET
