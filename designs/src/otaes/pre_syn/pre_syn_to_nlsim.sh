#!/bin/bash

# Error out when nonzero result
set -e

COMMIT=$1
OTDIR=../opentitan
OTAES=$OTDIR/hw/ip/aes/pre_syn.$COMMIT
DIFF=pre_syn.$COMMIT/tree.diff
SRC_V=$OTAES/syn_out/latest/generated/otaes.pre_map.v
DESIGNS=../../..
NLSIM_SRC=$DESIGNS/src/otaes-$COMMIT
NLSIM_SKY130=$DESIGNS/sky130/otaes-$COMMIT
NLSIM_SKY130_ORG=$DESIGNS/sky130/otaes
NLSIM_TB=$DESIGNS/tb

echo [*] Checking out $COMMIT
(cd $OTDIR && git reset --hard && git checkout $COMMIT)
if [ -e $DIFF ]; then
	echo [*] Applying diff
	cat $DIFF | (cd $OTDIR && patch -p1)
fi
echo [*] Adding pre_syn.$COMMIT
rm -rf $OTAES
cp -ra pre_syn.$COMMIT $OTAES

echo [*] Doing synthesis
(cd $OTAES && nice -n 19 ./syn_yosys.sh)

echo [*] Copying into $NLSIM_SRC source
rm -rf $NLSIM_SRC
mkdir -p $NLSIM_SRC
cp $SRC_V $NLSIM_SRC
(cd $NLSIM_SRC && ln -s otaes.pre_map.v opentitan.v)

echo [*] Creating $NLSIM_SKY130 contents
rm -rf $NLSIM_SKY130
cp -r $NLSIM_SKY130_ORG $NLSIM_SKY130
sed -i "s/DESIGN_NICKNAME = otaes/DESIGN_NICKNAME = otaes-$COMMIT/" $NLSIM_SKY130/config.mk

echo [*] Adding testbench in $NLSIM_TB
(cd $NLSIM_TB && rm -f otaes-$COMMIT && ln -s otaes otaes-$COMMIT)

echo [*] Successfully added $NLSIM_SRC and friends
