#!/bin/bash

# Copyright lowRISC contributors.
# Licensed under the Apache License, Version 2.0, see LICENSE for details.
# SPDX-License-Identifier: Apache-2.0

# This script drives the experimental Ibex synthesis flow. More details can be
# found in README.md

if [ ! -f syn_setup.sh ]; then
  echo "Must provide syn_setup.sh file"
  echo "See example in syn_setup.example.sh and README.md for instructions"
  exit 1
fi

#-------------------------------------------------------------------------
# setup flow variables
#-------------------------------------------------------------------------
source syn_setup.sh

#-------------------------------------------------------------------------
# use sv2v to convert all SystemVerilog files to Verilog
#-------------------------------------------------------------------------

rm -rf $LR_SYNTH_OUT_DIR
mkdir -p "$LR_SYNTH_OUT_DIR/generated"
mkdir -p "$LR_SYNTH_OUT_DIR/log"
mkdir -p "$LR_SYNTH_OUT_DIR/reports/timing"

rm -f latest
ln -s "$LR_SYNTH_OUT_DIR" latest

export LR_SYNTH_SRC_DIR="$LR_SYNTH_OPENTITAN_PATH/hw/ip/aes"

# Get OpenTitatn dependency sources
ot_dep_sources=("$LR_SYNTH_SRC_DIR"/../tlul/rtl/tlul_adapter_reg.sv
             "$LR_SYNTH_SRC_DIR"/../tlul/rtl/tlul_err.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_ext.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_shadow.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_subreg_arb.sv             
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_sender.sv
             "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_diff_decode.sv
             rtl/prim_flop_2sync.sv
             "$LR_SYNTH_SRC_DIR"/../prim_generic/rtl/prim_generic_flop_2sync.sv
             rtl/prim_flop.sv
             #"$LR_SYNTH_SRC_DIR"/../prim_xilinx/rtl/prim_xilinx_flop.sv - need patched version
             rtl/prim_xilinx_flop.sv
             rtl/prim_buf.sv
             rtl/prim_xilinx_buf.sv
             #"$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_lfsr.sv - need a patched version
             rtl/prim_lfsr.sv)
for file in ${ot_dep_sources[@]}; do
  module=`basename -s .sv $file`
  sv2v \
    --define=SYNTHESIS --define=YOSYS \
    "$LR_SYNTH_SRC_DIR"/../../top_earlgrey/rtl/*_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../tlul/rtl/*_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_pkg.sv \
    -I"$LR_SYNTH_SRC_DIR"/../prim/rtl \
    $file \
    > $LR_SYNTH_OUT_DIR/generated/${module}.v

  # TODO: eventually remove below hack. It removes "unsigned" from params
  # because Yosys doesn't support unsigned parameters
  sed -i 's/parameter unsigned/parameter/g'   $LR_SYNTH_OUT_DIR/generated/${module}.v
  sed -i 's/localparam unsigned/localparam/g' $LR_SYNTH_OUT_DIR/generated/${module}.v
  sed -i 's/reg unsigned/reg/g'   $LR_SYNTH_OUT_DIR/generated/${module}.v
done

# Get core sources
for file in "$LR_SYNTH_SRC_DIR"/rtl/*.sv rtl/otaes.sv; do
  module=`basename -s .sv $file`
  sv2v \
    --define=SYNTHESIS \
    "$LR_SYNTH_SRC_DIR"/../../top_earlgrey/rtl/*_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../tlul/rtl/*_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../lc_ctrl/rtl/lc_ctrl_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_util_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_secded_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_alert_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/../prim/rtl/prim_cipher_pkg.sv \
    "$LR_SYNTH_SRC_DIR"/rtl/*_pkg.sv \
    -I"$LR_SYNTH_SRC_DIR"/../prim/rtl \
    $file \
    > $LR_SYNTH_OUT_DIR/generated/${module}.v

  # TODO: eventually remove below hack. It removes "unsigned" from params
  # because Yosys doesn't support unsigned parameters
  sed -i 's/parameter unsigned/parameter/g'   $LR_SYNTH_OUT_DIR/generated/${module}.v
  sed -i 's/localparam unsigned/localparam/g' $LR_SYNTH_OUT_DIR/generated/${module}.v
  sed -i 's/reg unsigned/reg/g'   $LR_SYNTH_OUT_DIR/generated/${module}.v
done

# remove generated *pkg.v files (they are empty files and not needed)
rm -f $LR_SYNTH_OUT_DIR/generated/*_pkg.v

yosys -c ./tcl/yosys_run_synth.tcl | tee ./$LR_SYNTH_OUT_DIR/log/syn.log

# Mix in Testbench code
cp opentitan_tb.v $LR_SYNTH_OUT_DIR/generated

if [[ $LR_SYNTH_TIMING_RUN == 1 ]] ;
then
  sta ./tcl/sta_run_reports.tcl | tee ./$LR_SYNTH_OUT_DIR/log/sta.log

  ./translate_timing_rpts.sh
fi
