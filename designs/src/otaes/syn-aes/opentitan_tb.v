`timescale 1ns/1ps

module opentitan_tb();

reg clk;
reg rst;

reg [127:0] aes_key = 128'h00000000_00000000_00000000_00000000;
wire [127:0] aes_output;
reg [127:0] aes_input = 128'h00000000_00000000_00000000_00000000;
wire test_done_o;

otaes aes_testbench (
  .clk_i(clk),
  .rst_ni(rst),

  .aes_input(aes_input),
  .aes_output(aes_output),
  .aes_key(aes_key),

  .test_done_o(test_done_o),
  .test_passed_o(test_passed_o)
);

initial begin
    forever #10 clk = !clk;
end

always @(posedge clk) begin
    if (test_done_o) begin
        $display("key: 0x%016h", aes_key);
        $display("in : 0x%016h", aes_input);
        $display("out: 0x%016h", aes_output);
        $finish;
    end
end

initial begin
    // Set variables to be dumped to vcd file here
    $dumpfile("opentitan.vcd");
    $dumpvars;
    // $dumpvars(0, opentitan_tb);

    clk = 1'b0;
    rst = 1'b1;
    #5;
    rst = 1'b0;
    #5;
    rst = 1'b1;
    #1000000 // More than enough
    $finish;
end

endmodule


