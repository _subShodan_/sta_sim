#!/bin/bash

# Copyright lowRISC contributors.
# Licensed under the Apache License, Version 2.0, see LICENSE for details.
# SPDX-License-Identifier: Apache-2.0

export LR_SYNTH_TIMING_RUN=0
export LR_SYNTH_FLATTEN=0

# SETUP CELL LIBRARY PATH
# Uncomment the lines below and set the path to an appropriate .lib file
export LR_SYNTH_CELL_LIBRARY_PATH=/OpenROAD-flow-public/flow/platforms/nangate45/lib/NangateOpenCellLibrary_typical.lib
export LR_SYNTH_CELL_LIBRARY_NAME=nangate

# SETUP OPENTITAN REPO PATH
export LR_SYNTH_OPENTITAN_PATH=../opentitan

if [ $# -eq 1 ]; then
  export LR_SYNTH_OUT_DIR=$1
elif [ $# -eq 0 ]; then
  export LR_SYNTH_OUT_DIR="syn_out/aes"$(cd $LR_SYNTH_OPENTITAN_PATH && git show -s --format=%ci HEAD | tr ' \n' '__' && git rev-parse HEAD)
else
  echo "Usage $0 [synth_out_dir]"
  exit 1
fi


