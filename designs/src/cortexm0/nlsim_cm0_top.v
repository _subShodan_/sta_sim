module nlsim_cm0_top
    (// ----------------------------------------------------------------------
    // CORTEX-M0 PRIMARY INPUTS AND OUTPUTS
    // ----------------------------------------------------------------------

    // CLOCK AND RESETS -----------------------------------------------------
    input  wire        clk,          // Global clock
    input  wire        DBGRESETn,     // Debug logic asynchronous reset
    input  wire        HRESETn,       // System logic asynchronous reset

    // AHB-LITE MASTER PORT -------------------------------------------------
    output wire [31:0] HADDR,         // AHB transaction address
    output wire [ 2:0] HBURST,        // AHB burst, tied to single
    output wire        HMASTLOCK,     // AHB locked transfer (always zero)
    output wire [ 3:0] HPROT,         // AHB protection, priv-data or priv-inst
    output wire [ 2:0] HSIZE,         // AHB size, only byte, half-word or word
    output wire [ 1:0] HTRANS,        // AHB transfer, non-sequential only
    output wire [31:0] HWDATA,        // AHB write-data
    output wire        HWRITE,        // AHB write control
    input  wire [31:0] HRDATA,        // AHB read-data
    input  wire        HREADY,        // AHB stall signal
    input  wire        HRESP,         // AHB error response

    output wire        HMASTER,       // AHB master, 0 = core, 1 = debug/slave

    // DEBUG ----------------------------------------------------------------
    input  wire        DBGRESTART,    // CoreSight exit-debug request
    output wire        DBGRESTARTED,  // CoreSight have-left debug acknowledge
    input  wire        EDBGRQ,        // External debug request
    output wire        HALTED,        // Core is halted

    // MISCELLANEOUS --------------------------------------------------------
    input  wire        NMI,           // Non-maskable interrupt input
    input  wire [31:0] IRQ,           // 32 interrupt request inputs
    output wire        TXEV,          // Event transmit
    input  wire        RXEV,          // Event receive
    output wire        LOCKUP,        // Core is locked-up
    output wire        SYSRESETREQ,   // System reset request
    input  wire [25:0] STCALIB,       // SysTick calibration register value
    input  wire        STCLKEN,       // SysTick SCLK clock enable
    input  wire [ 7:0] IRQLATENCY,    // Interrupt latency value
    input  wire [19:0] ECOREVNUM,     // Engineering-change-order revision bits

    // CODE SEQUENTIALITY AND SPECULATION -----------------------------------
    output wire        CODENSEQ,      // Code fetch is non-sequential to last
    output wire [ 2:0] CODEHINTDE,    // Code fetch hints
    output wire        SPECHTRANS,    // Speculative HTRANS[1]

    // POWER MANAGEMENT -----------------------------------------------------
    output wire        SLEEPING,      // Core sleeping (HCLK may be gated)
    output wire        SLEEPDEEP,     // Core is in deep-sleep
    input  wire        SLEEPHOLDREQn, // Sleep extension request
    output wire        SLEEPHOLDACKn, // Sleep extension acknowledge
    input  wire        WICDSREQn,     // Operate in WIC based deep-sleep mode
    output wire        WICDSACKn,     // Acknowledge using WIC based deep-sleep
    output wire [31:0] WICMASKISR,    // WIC interrupt sensitivity mask
    output wire        WICMASKNMI,    // WIC NMI sensitivity mask
    output wire        WICMASKRXEV,   // WIC event sensitivity mask
    output wire        WICLOAD,       // WIC should reload using current masks
    output wire        WICCLEAR,      // WIC should clear its mask

    // SCAN/TEST IO ---------------------------------------------------------
    input  wire        SE,            // Scan-enable (not used by RTL)
    output wire [31:0] program_counter);
   // ----------------------------------------------------------------------

    wire [31:0] SLVRDATA;      // Slave read-data
    wire        SLVREADY;      // Slave stall signal
    wire        SLVRESP;       // Slave error response

   CORTEXM0
     cpu0
         (
         .SCLK (clk),
          .HCLK (clk),
          .DCLK (clk),
          .DBGRESETn (DBGRESETn),
          .HRESETn (HRESETn),
          .SE (SE),
          .HADDR (HADDR[31:0]),
          .HBURST (HBURST[2:0]),
          .HMASTLOCK (HMASTLOCK),
          .HPROT (HPROT[3:0]),
          .HSIZE (HSIZE[2:0]),
          .HTRANS (HTRANS[1:0]),
          .HWDATA (HWDATA[31:0]),
          .HWRITE (HWRITE),
          .HRDATA (HRDATA[31:0]),
          .HREADY (HREADY),
          .HRESP (HRESP),
          .HMASTER (HMASTER),
          .SLVADDR (0),
          .SLVSIZE (0),
          .SLVTRANS (0),
          .SLVWDATA (0),
          .SLVWRITE (0),
          .SLVRDATA (SLVRDATA[31:0]),
          .SLVREADY (SLVREADY),
          .SLVRESP (SLVRESP),
          .DBGRESTART (DBGRESTART),
          .DBGRESTARTED (DBGRESTARTED),
          .EDBGRQ (EDBGRQ),
          .HALTED (HALTED),
          .NMI (NMI),
          .IRQ (IRQ[31:0]),
          .TXEV (TXEV),
          .RXEV (RXEV),
          .LOCKUP (LOCKUP),
          .SYSRESETREQ (SYSRESETREQ),
          .STCALIB (STCALIB[25:0]),
          .STCLKEN (STCLKEN),
          .IRQLATENCY (IRQLATENCY[7:0]),
          .ECOREVNUM (ECOREVNUM[19:0]),
          .CODENSEQ (CODENSEQ),
          .CODEHINTDE (CODEHINTDE[2:0]),
          .SPECHTRANS (SPECHTRANS),
          .SLEEPING (SLEEPING),
          .SLEEPDEEP (SLEEPDEEP),
          .SLEEPHOLDREQn (SLEEPHOLDREQn),
          .SLEEPHOLDACKn (SLEEPHOLDACKn),
          .WICDSREQn (0),
          .WICDSACKn (WICDSACKn),
          .WICMASKISR (WICMASKISR[31:0]),
          .WICMASKNMI (WICMASKNMI),
          .WICMASKRXEV (WICMASKRXEV),
          .WICLOAD (WICLOAD),
          .WICCLEAR (WICCLEAR),
          .program_counter(program_counter)
          );

endmodule // CORTEXM0
