The following files are ARM IP blocks, which are not placed in this repository to protect their confidentiallity. Make sure that the following files are compied into this folder from the ARM Cortex M0 package:

* CORTEXM0.v
* cm0_core_ctl.v
* cm0_core_pfu.v
* cm0_dbg_ctl.v
* cm0_matrix.v
* cm0_nvic_reg.v
* cm0_top_dbg.v
* cm0_acg.v
* cm0_core_dec.v
* cm0_core_psr.v
* cm0_dbg_dwt.v
* cm0_matrix_sel.v
* cm0_tarmac.v
* cm0_top_sys.v
* cm0_core.v
* cm0_core_gpr.v
* cm0_core_spu.v
* cm0_dbg_if.v
* cm0_nvic.v
* cm0_top.v
* cm0_core_alu.v
* cm0_core_mul.v
* cm0_dbg_bpu.v
* cm0_dbg_sel.v
* cm0_nvic_main.v
* cm0_top_clk.v
