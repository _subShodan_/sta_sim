module nlsim_wrapper
    (// ----------------------------------------------------------------------
    // CORTEX-M0 PRIMARY INPUTS AND OUTPUTS
    // ----------------------------------------------------------------------

    // CLOCK AND RESETS -----------------------------------------------------
    input  wire        clk_i,          // Global clock
    input  wire        rst_ni,     // Debug logic asynchronous reset

    output wire [31:0] NLSIM_READ_ADDR,
    output reg [31:0] NLSIM_WRITE_ADDR,
    output reg [2:0]  NLSIM_WRITE_HSIZE,
    output reg        NLSIM_WRITE,
    output wire        NLSIM_READ,
    output wire [2:0]  NLSIM_READ_HSIZE,
    output wire [ 3:0] HPROT,                  // AHB protection; priv-data or priv-inst
    input  wire [31:0] NLSIM_READ_DATA,        // AHB read-data
    output wire [31:0] NLSIM_WRITE_DATA,        // AHB read-data
    output wire        test_done_o,        // Core is halted
    input wire NLSIM_READY,

    // MISCELLANEOUS --------------------------------------------------------
    output wire        LOCKUP,        // Core is locked-up

    // SCAN/TEST IO ---------------------------------------------------------
    input  wire        SE,           // Scan-enable (not used by RTL)
    output wire [31:0] program_counter);
   // ----------------------------------------------------------------------

    wire [31:0] SLVRDATA;      // Slave read-data
    wire        SLVREADY;      // Slave stall signal
    wire        SLVRESP;       // Slave error response


    // Unused wires...
    wire SYSRESETREQ;
    wire        SLEEPING;      // Core sleeping (HCLK may be gated)
    wire        SLEEPDEEP;     // Core is in deep-sleep
    wire        SLEEPHOLDACKn; // Sleep extension acknowledge
    wire        WICDSACKn;     // Acknowledge using WIC based deep-sleep
    wire [31:0] WICMASKISR;    // WIC interrupt sensitivity mask
    wire        WICMASKNMI;    // WIC NMI sensitivity mask
    wire        WICMASKRXEV;   // WIC event sensitivity mask
    wire        WICLOAD;       // WIC should reload using current masks
    wire        WICCLEAR;      // WIC should clear its mask

    wire TXEV;
    wire        CODENSEQ;      // Code fetch is non-sequential to last
    wire [ 2:0] CODEHINTDE;    // Code fetch hints
    wire        SPECHTRANS;    // Speculative HTRANS[1]
    wire        DBGRESTARTED;  // CoreSight have-left debug acknowledge

    wire [31:0] HADDR;         // AHB transaction address
    wire [ 2:0] HBURST;        // AHB burst; tied to single
    wire        HMASTLOCK;     // AHB locked transfer (always zero)
    wire [ 2:0] HSIZE;         // AHB size; only byte; half-word or word
    wire [ 1:0] HTRANS;        // AHB transfer; non-sequential only
    wire [31:0] HWDATA;        // AHB write-data
    wire        HWRITE;        // AHB write control

    wire [31:0] HRDATA;        // AHB read-data
    wire        HRESP;         // AHB error response
    wire        HREADY;

    reg write_in_next_active;
    reg [31:0] write_in_next_address;
    reg [ 2:0] write_in_next_hsize;

    assign NLSIM_READ = HTRANS[1];
    assign NLSIM_READ_HSIZE = HSIZE;
    assign NLSIM_READ_ADDR = HADDR;
    assign HRDATA = NLSIM_READ_DATA;

    always @(posedge clk_i) begin
        if (!rst_ni) begin
            write_in_next_address <= 0;
        end

        write_in_next_active <= 0;
        NLSIM_WRITE <= 0;

        if (HWRITE == 1'b1) begin
            //memory[HADDR >> 2][ 31: 0] <= HWDATA[ 31: 0];
            write_in_next_address <= HADDR;
            write_in_next_active <= 1;
            write_in_next_hsize <= HSIZE; // HSIZE is valid in address phase!
        end
        if (write_in_next_active) begin
            //$display("MEMORY WRITE %08x: %08x", write_in_next_address, HWDATA);
            //memory[write_in_next_address][ 31: 0] <= HWDATA[ 31: 0];
            NLSIM_WRITE <= 1;
            NLSIM_WRITE_DATA <= HWDATA; // DATA is present on bus after address cycle...
            NLSIM_WRITE_ADDR <= write_in_next_address;
            NLSIM_WRITE_HSIZE <= write_in_next_hsize;
        end
    end

   nlsim_cm0_top
     cpu0
         (
         .clk (clk_i),
          .DBGRESETn (rst_ni),
          .HRESETn (rst_ni),
          .SE (0),
          .HADDR (HADDR[31:0]),
          .HBURST (HBURST[2:0]),
          .HMASTLOCK (HMASTLOCK),
          .HPROT (HPROT[3:0]),
          .HSIZE (HSIZE[2:0]),
          .HTRANS (HTRANS[1:0]),
          .HWDATA (HWDATA[31:0]),
          .HWRITE (HWRITE),
          .HRDATA (HRDATA[31:0]),
          .HREADY (NLSIM_READY),
          .HRESP (0),
          .HMASTER (HMASTER),
          .DBGRESTART (0),
          .DBGRESTARTED (DBGRESTARTED),
          .EDBGRQ (0),
          .HALTED (test_done_o),
          .NMI (0),
          .IRQ (0),
          .TXEV (TXEV),
          .RXEV (0),
          .LOCKUP (LOCKUP),
          .SYSRESETREQ (SYSRESETREQ),
          .STCALIB (0),
          .STCLKEN (0),
          .IRQLATENCY (0),
          .ECOREVNUM (0),
          .CODENSEQ (CODENSEQ),
          .CODEHINTDE (CODEHINTDE[2:0]),
          .SPECHTRANS (SPECHTRANS),
          .SLEEPING (SLEEPING),
          .SLEEPDEEP (SLEEPDEEP),
          .SLEEPHOLDREQn (0),
          .SLEEPHOLDACKn (SLEEPHOLDACKn),
          .WICDSREQn (0),
          .WICDSACKn (WICDSACKn),
          .WICMASKISR (WICMASKISR[31:0]),
          .WICMASKNMI (WICMASKNMI),
          .WICMASKRXEV (WICMASKRXEV),
          .WICLOAD (WICLOAD),
          .WICCLEAR (WICCLEAR),
          .program_counter(program_counter));

endmodule // CORTEXM0
