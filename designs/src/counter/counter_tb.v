module counter_tb();

reg clk, rst;
wire [3:0] counter;

counter my_counter( .counter_out(counter), .clk(clk), .rst(rst) );

always begin
	#5 clk = ~clk;
end

initial
begin
	$monitor("%b %b %b", clk, counter, rst);
	#5
	clk = 1'b1;
	rst = 1'b1;
	#20
	rst = 1'b0;
	#100
	rst = 1'b1;
	$finish;
end

endmodule
