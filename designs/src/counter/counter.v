module counter (output reg [255:0] counter_out_long, output reg [63:0] counter_out, input clk, input rst);

assign counter_out = counter_out_long[63:0];

always @(posedge clk)
begin
	if (rst == 1'b1) begin
		counter_out <= '0;
	end
	else begin
		counter_out <= counter_out + 1;
	end
end


endmodule
