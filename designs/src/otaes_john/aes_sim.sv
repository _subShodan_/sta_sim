// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0
//
// AES simulation wrapper

module aes_sim import aes_pkg::*;
#(
  parameter bit          AES192Enable         = 1,
  parameter bit          Masking              = 1,
  parameter sbox_impl_e  SBoxImpl             = SBoxImplCanrightMaskedNoreuse,
  parameter int unsigned SecStartTriggerDelay = 40,
  parameter bit          SecAllowForcingMasks = 0
) (
  input                     clk_i,
  input                     rst_ni,

  // Bus Interface
  input  tlul_pkg::tl_h2d_t tl_i,
  output tlul_pkg::tl_d2h_t tl_o
);

  import aes_reg_pkg::*;

  // Instantiate top-level
  aes #(
    .AES192Enable         ( AES192Enable         ),
    .Masking              ( Masking              ),
    .SBoxImpl             ( SBoxImpl             ),
    .SecStartTriggerDelay ( SecStartTriggerDelay ),
    .SecAllowForcingMasks ( SecAllowForcingMasks )
  ) u_aes (
    .clk_i,
    .rst_ni,
    .idle_o     (          ),
    .tl_i,
    .tl_o,
    .alert_rx_i ( alert_rx ),
    .alert_tx_o ( alert_tx )
  );


  // alerts
  prim_alert_pkg::alert_rx_t [NumAlerts-1:0] alert_rx;
  prim_alert_pkg::alert_tx_t [NumAlerts-1:0] alert_tx, unused_alert_tx;

  assign alert_rx[0].ping_p = 1'b0;
  assign alert_rx[0].ping_n = 1'b1;
  assign alert_rx[0].ack_p  = 1'b0;
  assign alert_rx[0].ack_n  = 1'b1;
  assign alert_rx[1].ping_p = 1'b0;
  assign alert_rx[1].ping_n = 1'b1;
  assign alert_rx[1].ack_p  = 1'b0;
  assign alert_rx[1].ack_n  = 1'b1;
  assign unused_alert_tx = alert_tx;

endmodule
