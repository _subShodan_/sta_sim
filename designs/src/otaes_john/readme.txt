Run syn_aes.sh and you should be able to synthesize the OpenTitan AES core to a netlist.
This will output a netlist.v file which has the yosys default standard cell technology.
You will require the latest version of yosys installed and sv2v (get both and compiled from github).

Please read the syn_aes.sh for information on generating the primitive files. This is done by opentitans build system.

After the netlist.v is created, you can map it to what ever technology you so wish using yosys/abc.

-- BIG NOTE --
Parameters in the aes.sv file dictate if AES192Enable, Masking, the type of SBoxImpl, SCA trigger, etc
Once the netlist is synthesized and verilated you cannot change these parameters as it is hardcoded into the design at that point.
So if you would like to test out masking or aes192, then please go in and change these parameters before sv2v and before yosys.

The aes.c and aes.h are taken from the software libs folder, please note that aes_regs.h is auto generated and you will need to run opentitan build system with fusesoc/meson/ninja to get this file.
The aes files are heavily modified because we are only isolating this core in verilator (not on the ibex core), not using the top_earlgrey implementation.


John Fitzgerald
