module prim_buf

#(

) (
  input in_i,
  output logic out_o
);

  prim_generic_buf u_impl_generic (
    .*
  );

endmodule
