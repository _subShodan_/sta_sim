module prim_flop_2sync

#(

  parameter int Width       = 16,
  localparam int WidthSubOne = Width-1, // temp work around #2679
  parameter logic [WidthSubOne:0] ResetValue = '0

) (
  input                    clk_i,       // receive clock
  input                    rst_ni,
  input        [Width-1:0] d_i,
  output logic [Width-1:0] q_o
);

  if (1) begin : gen_generic
    prim_generic_flop_2sync #(
      .ResetValue(ResetValue),
      .Width(Width)
    ) u_impl_generic (
      .*
    );

  end

endmodule
