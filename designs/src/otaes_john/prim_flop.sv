module prim_flop

#(

  parameter int Width      = 1,
  localparam int WidthSubOne = Width-1,
  parameter logic [WidthSubOne:0] ResetValue = 0

) (
  input clk_i,
  input rst_ni,
  input [Width-1:0] d_i,
  output logic [Width-1:0] q_o
);

  prim_generic_flop #(
    .ResetValue(ResetValue),
    .Width(Width)
  ) u_impl_generic (
    .*
  );

endmodule
