#include "Vaes_sim.h"
//#include "aes.h"

#include "aes_tlul_interface.h"
#include "verilated_vcd_c.h"

Vaes_sim *freshcore() {
    Vaes_sim *core = new Vaes_sim;

    // reset
    core->clk_i = 0;
    core->rst_ni = 0;
    memset(core->tl_i, 0, sizeof(core->tl_i));
    memset(core->tl_o, 0, sizeof(core->tl_o));

    for(int i = 0; i < 200; i++) {
        core->eval();
        core->clk_i ^= 1;
    }

    core->rst_ni = 1;

    core->eval();
    core->clk_i ^= 1;

    return core;
}

int main(int argc, char **argv, char **env) {
    Verilated::commandArgs(argc, argv);

    Vaes_sim *core = freshcore();

    // the core is already reset once tracing starts
    Verilated::traceEverOn(true);
    VerilatedVcdC *tfp = new VerilatedVcdC;
    core->trace(tfp, 99); // trace 99 levels of hierarchy
    tfp->open("vlt_dump.vcd");

    printf("got an aes core %X\n", core);

    AESTLULInterface *tlul_interface = new AESTLULInterface(core);

    int i = 0;
    while(!tlul_interface->StatusDone()) {
        tlul_interface->HandleInterface();
        core->eval();
        core->clk_i ^= 1;

        tfp->dump(i++);
    }
    
    //uint32_t *a = (uint32_t *)core->aes__DOT__u_aes_core__DOT__data_out_d;
    //printf("%08X %08X %08X %08X\n", a[0], a[1], a[2], a[3]);

    core->final();
    tfp->close();

    // this just doesnt work, i cry
    /*aes_clear(core);
    while (!aes_idle(core)) {
        ;
    }
    aes_cfg_t aes_cfg;
    aes_cfg.key_len = kAes128;
    aes_cfg.operation = kAesEnc;
    aes_cfg.mode = kAesEcb;
    aes_cfg.manual_operation = 0;
    aes_init(core, aes_cfg);

    for(int i = 0; i < 100; i++) {
        core->eval();
        core->clk_i ^= 1;
        tfp->dump(i);
    }*/

    // these require  /*verilator public_flat*/; to show up in the netlist.v

    //core->aes__DOT__u_aes_core__DOT__aes_op_q
    //core->aes__DOT__u_aes_core__DOT__aes_mode_q

    //core->aes__DOT__u_aes_core__DOT__u_aes_cipher_core__DOT__key_len_i

    /*core->aes__DOT__u_aes_core__DOT__data_in[0] = 0xAABBCCDD;
    core->aes__DOT__u_aes_core__DOT__data_in[1] = 0xEEFF1122;
    core->aes__DOT__u_aes_core__DOT__data_in[2] = 0x33445566;
    core->aes__DOT__u_aes_core__DOT__data_in[3] = 0x77889900;

    for(int i = 0; i < 100; i++) {
        core->eval();
        core->clk_i ^= 1;

        tfp->dump(i);
    }

    //core->aes__DOT__u_aes_core__DOT__data_out

    core->final();
    tfp->close();

    delete core;*/

    return 0;
}
