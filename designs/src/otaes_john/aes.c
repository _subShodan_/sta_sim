// Copyright lowRISC contributors.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

#include "aes.h"
#include "aes_regs.h"  // Generated.

/*
  typedef enum logic [2:0] {
    PutFullData    = 3'h 0,
    PutPartialData = 3'h 1,
    Get            = 3'h 4
  } tl_a_op_e;

  typedef enum logic [2:0] {
    AccessAck     = 3'h 0,
    AccessAckData = 3'h 1
  } tl_d_op_e;

  typedef struct packed {
    logic                         a_valid;
    tl_a_op_e                     a_opcode;
    logic                  [2:0]  a_param;
    logic  [top_pkg::TL_SZW-1:0]  a_size;
    logic  [top_pkg::TL_AIW-1:0]  a_source;
    logic   [top_pkg::TL_AW-1:0]  a_address;
    logic  [top_pkg::TL_DBW-1:0]  a_mask;
    logic   [top_pkg::TL_DW-1:0]  a_data;
    tl_a_user_t                   a_user;

    logic                         d_ready;
  } tl_h2d_t;

  localparam tl_h2d_t TL_H2D_DEFAULT = '{
    d_ready:  1'b1,
    a_opcode: tl_a_op_e'('0),
    a_user:   tl_a_user_t'('0),
    default:  '0
  };

  typedef struct packed {
    logic                         d_valid;
    tl_d_op_e                     d_opcode;
    logic                  [2:0]  d_param;
    logic  [top_pkg::TL_SZW-1:0]  d_size;   // Bouncing back a_size
    logic  [top_pkg::TL_AIW-1:0]  d_source;
    logic  [top_pkg::TL_DIW-1:0]  d_sink;
    logic   [top_pkg::TL_DW-1:0]  d_data;
    logic  [top_pkg::TL_DUW-1:0]  d_user;
    logic                         d_error;

    logic                         a_ready;
  } tl_d2h_t;
*/

// tlul bus in C
// the tl_i is an tl_h2d_t from tlul_pkg.sv
// the tl_o is an tl_d2h_t from tlul_pkg.sv
// ^ look at the structure for how things are organized

/*
VL_INW(tl_i,101,0,4);
VL_OUTW(tl_o,67,0,3);
*/

struct tl_i_s {
    uint8_t a_valid : 1;
    uint8_t a_opcode : 3;
    uint8_t a_param : 3;
    uint8_t a_size : 1; // I THINK SO?
    uint8_t a_source : 8;

    uint32_t a_address : 32;
    uint8_t a_mask : 4;
    uint32_t a_data : 32;

    uint8_t rsvd1 : 7;
    uint8_t parity_en : 1; 
    uint8_t parity : 8; // Use only lower TL_DBW bit

    uint8_t d_ready : 1;
} __attribute__((__packed__));

struct tl_o_s {
    uint8_t d_valid : 1;
    uint8_t d_opcode : 3;
    uint8_t d_param : 3;
    uint8_t d_size : 1; // I THINK SO?
    uint8_t d_source : 8;

    uint8_t d_sink : 1;
    uint32_t d_data : 32;
    uint16_t d_user : 16;
    uint8_t d_error : 1;

    uint8_t a_ready : 1;
} __attribute__((__packed__));

uint32_t tl_write(Vaes *core, uint32_t address, uint32_t data) {
    struct tl_i_s *tl_i = (struct tl_i_s *)core->tl_i;
    struct tl_o_s *tl_o = (struct tl_o_s *)core->tl_o;

    tl_i->a_opcode = 0; // PutFullData
    tl_i->a_address = address;
    tl_i->a_size = 1;
    tl_i->a_mask = 8;
    tl_i->a_data = data;
    tl_i->a_valid = 1;

    tl_o->d_valid = 0;

    do {
        core->eval();
        core->clk_i ^= 1;
    } while(tl_o->d_valid == 0);

    return 0;
}

uint32_t tl_read(Vaes *core, uint32_t address) {
    struct tl_i_s *tl_i = (struct tl_i_s *)core->tl_i;
    struct tl_o_s *tl_o = (struct tl_o_s *)core->tl_o;

    tl_i->a_opcode = 4; // Get
    tl_i->a_address = address;
    tl_i->a_size = 1;
    tl_i->a_mask = 8;
    tl_i->a_data = 0;
    tl_i->a_valid = 1;

    tl_o->d_valid = 0;

    do {
        core->eval();
        core->clk_i ^= 1;
    } while(tl_o->d_valid == 0);

    return tl_o->d_data;
}

#define AES_NUM_REGS_KEY 8
#define AES_NUM_REGS_IV 4
#define AES_NUM_REGS_DATA 4

void aes_init(Vaes *core, aes_cfg_t aes_cfg) {
  uint32_t cfg_val =
      (aes_cfg.operation << AES_CTRL_SHADOWED_OPERATION_BIT) |
      ((aes_cfg.mode & AES_CTRL_SHADOWED_MODE_MASK)
       << AES_CTRL_SHADOWED_MODE_OFFSET) |
      ((aes_cfg.key_len & AES_CTRL_SHADOWED_KEY_LEN_MASK)
       << AES_CTRL_SHADOWED_KEY_LEN_OFFSET) |
      (aes_cfg.manual_operation << AES_CTRL_SHADOWED_MANUAL_OPERATION_BIT);
  tl_write(core, AES_CTRL_SHADOWED_REG_OFFSET, cfg_val);
  tl_write(core, AES_CTRL_SHADOWED_REG_OFFSET, cfg_val);
};

void aes_key_put(Vaes *core, const void *key_share0, const void *key_share1,
                 aes_key_len_t key_len) {
  // Determine how many key registers to use.
  size_t num_regs_key_used;
  if (key_len == kAes256) {
    num_regs_key_used = 8;
  } else if (key_len == kAes192) {
    num_regs_key_used = 6;
  } else {
    num_regs_key_used = 4;
  }

  // Write the used key registers.
  for (int i = 0; i < num_regs_key_used; ++i) {
    tl_write(core, AES_KEY_SHARE0_0_REG_OFFSET + i * sizeof(uint32_t), ((uint32_t *)key_share0)[i]);
    tl_write(core, AES_KEY_SHARE1_0_REG_OFFSET + i * sizeof(uint32_t), ((uint32_t *)key_share1)[i]);
  }
  // Write the unused key registers (the AES unit requires all key registers to
  // be written).
  for (int i = num_regs_key_used; i < AES_NUM_REGS_KEY; ++i) {
    tl_write(core, AES_KEY_SHARE0_0_REG_OFFSET + i * sizeof(uint32_t), 0x0);
    tl_write(core, AES_KEY_SHARE1_0_REG_OFFSET + i * sizeof(uint32_t), 0x0);
  }
}

void aes_iv_put(Vaes *core, const void *iv) {
  // Write the four initialization vector registers.
  for (int i = 0; i < AES_NUM_REGS_IV; ++i) {
    tl_write(core, AES_IV_0_REG_OFFSET + i * sizeof(uint32_t), ((uint32_t *)iv)[i]);
  }
}

void aes_data_put_wait(Vaes *core, const void *data) {
  // Wait for AES unit to be ready for new input data.
  while (!aes_data_ready(core)) {
  }

  // Provide the input data.
  aes_data_put(core, data);
}

void aes_data_put(Vaes *core, const void *data) {
  // Write the four input data registers.
  for (int i = 0; i < AES_NUM_REGS_DATA; ++i) {
    tl_write(core, AES_DATA_IN_0_REG_OFFSET + i * sizeof(uint32_t), ((uint32_t *)data)[i]);
  }
}

void aes_data_get_wait(Vaes *core, void *data) {
  // Wait for AES unit to have valid output data.
  while (!aes_data_valid(core)) {
  }

  // Get the data.
  aes_data_get(core, data);
}

void aes_data_get(Vaes *core, void *data) {
  // Read the four output data registers.
  for (int i = 0; i < AES_NUM_REGS_DATA; ++i) {
    ((uint32_t *)data)[i] = tl_read(core, AES_DATA_OUT_0_REG_OFFSET + i * sizeof(uint32_t));
  }
}

bool aes_data_ready(Vaes *core) {
  return (tl_read(core, AES_STATUS_REG_OFFSET) &
          (0x1u << AES_STATUS_INPUT_READY_BIT));
}

bool aes_data_valid(Vaes *core) {
  return (tl_read(core, AES_STATUS_REG_OFFSET) &
          (0x1u << AES_STATUS_OUTPUT_VALID_BIT));
}

bool aes_idle(Vaes *core) {
  return (tl_read(core, AES_STATUS_REG_OFFSET) &
          (0x1u << AES_STATUS_IDLE_BIT));
}

void aes_manual_trigger(Vaes *core) {
  tl_write(core, AES_TRIGGER_REG_OFFSET, 0x1u << AES_TRIGGER_START_BIT);
}

void aes_clear(Vaes *core) {
  // Wait for AES unit to be idle.
  while (!aes_idle(core)) {
  }

  // Disable autostart
  uint32_t cfg_val = 0x1u << AES_CTRL_SHADOWED_MANUAL_OPERATION_BIT;
  tl_write(core, AES_CTRL_SHADOWED_REG_OFFSET, cfg_val);
  tl_write(core, AES_CTRL_SHADOWED_REG_OFFSET, cfg_val);

  // Clear internal key and output registers
  tl_write(core, AES_TRIGGER_REG_OFFSET,
      (0x1u << AES_TRIGGER_KEY_CLEAR_BIT) | (0x1u << AES_TRIGGER_IV_CLEAR_BIT) |
      (0x1u << AES_TRIGGER_DATA_IN_CLEAR_BIT) |
      (0x1u << AES_TRIGGER_DATA_OUT_CLEAR_BIT));

  // Wait for output not valid, and input ready
  while (!(!aes_data_valid(core) && aes_data_ready(core))) {
  }
}
