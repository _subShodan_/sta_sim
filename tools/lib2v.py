#!/usr/bin/python3

from liberty.parser import parse_liberty
from optparse import OptionParser
import sys

def conv(escstr):
    if not escstr:
        return None
    return str(escstr).strip('"')

def clkedge(clkstr):
    if clkstr.startswith('!'):
        return "negedge %s" % clkstr.strip('!')
    else:
        return "posedge %s" % clkstr

def fixfun(fun):
    return fun.replace('!', '~') # Logical -> bitwise not

def dolib2v(args):
    # Cmd line stuff
    USAGE_STRING = "Usage: lib2v.py [liberty input] [netlist output]"
    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-w", dest="width", type="int", default=64, help="Width of vectors to output")
    (options, args) = optparser.parse_args(args)

    if len(args) != 2:
        print(USAGE_STRING)
        sys.exit(1)


    libertyfile = args[0]
    outputfile = args[1]


    # Parse
    print("Parsing library")
    library = parse_liberty(open(libertyfile).read())

    # Loop over structure and print output

    with open(outputfile, 'w') as f:
        for group in library.groups:
            if group.group_name == "cell":
                # Grab cell name
                cell = conv(group.args[0])

                # Grab pins and function
                pins = []
                ff = []
                ignore = False
                clocked_on = ""
                next_state = None 
                preset = None
                clear = None
                for subgroup in group.groups:
                    if subgroup.group_name == "ff":
                        ff = subgroup
                        clocked_on = conv(ff.attributes.get("clocked_on"))
                        next_state = conv(ff.attributes.get("next_state"))
                        preset = conv(ff.attributes.get("preset"))
                        clear = conv(ff.attributes.get("clear"))

                    if subgroup.group_name == "pin":
                        pin = conv(subgroup.args[0])
                        dirx = conv(subgroup.attributes["direction"])
                        fun = conv(subgroup.attributes.get("function"))
                        if pin == "Q":
                            # Special case for very specific flip flops in sky130
                            if fun != "IQ" or dirx != "output" or not ff or len(ff.args) != 2 or ff.args[0] != "IQ" or ff.args[1] != "IQ_N":
                                ignore = "Ignoring, unknown pin config for cell %s and pin %s" % (cell, pin)
                            elif (len(ff.attributes) == 2 and clocked_on and next_state) or (len(ff.attributes) == 3 and clocked_on and next_state and (clear or "preset")):

                                if next_state == "D":
                                    dirx = dirx + " reg" 
                                    if preset and clear:
                                        ignore = "Don't support preset and clear at the same time for cell %s" % (cell)
                                    else:
                                        if preset:
                                            fun = "  always @(%s) begin\n    %s <= %s | (%s);\n  end" % (clkedge(clocked_on), pin, next_state, preset)
                                        elif clear:
                                            fun = "  always @(%s) begin\n    %s <= %s & ~(%s);\n  end" % (clkedge(clocked_on), pin, next_state, clear)
                                        else:
                                            fun = "  always @(%s) begin\n    %s <= %s;\n  end" % (clkedge(clocked_on), pin, next_state)
                                else:
                                    ignore = "Cell %s has flipflop with nontrivial output, not supported" % (cell)
                            else:
                                ignore = "Ignoring cell %s, unknown ff combi" % (cell)


                        elif pin == "HI" or pin == "LO":
                            if dirx != "output" or not fun:
                                ignore = "Unknown hi/lo pin for cell %s" % (cell)
                            if fun != "1" and fun != "0":
                                ignore = "HI/LO only supports 1 or 0 as function"
                            fun = "  assign %s = %d'b%s;" % (pin, options.width, fun*options.width)

                        else:
                            if fun and (pin != "X" and pin != "Y"):
                                ignore = "Ignoring output function for cell %s because we only consider pins called X or Y. This seems to differentiate logic cells from others in sky130. Yes, it's a hack. Yes, it works for now" % (cell)
                            if dirx != "input" and dirx != "output":
                                ignore = "Ignoring cell %s pin %s because we only consider input and output pins." % (cell, pin)
                            if fun:
                                fun = "  assign %s = %s;" % (pin, fun)
                            is_clk = clocked_on.strip('!') == pin

                        pins.append([ pin, dirx, fun, is_clk ])

                if len(pins) == 0:
                    ignore = "No pins found for cell %s" % (cell)

                if ignore:
                    print(ignore)
                    continue
                else:
                    print("Writing cell", cell)

                # Write verilog
                print("module %s (" % (cell), file=f)

                # Join external pins into pretty string
                pinstr = ["  %s [%d:0] %s" % (direction, (0 if is_clk else options.width-1), pin) for [pin, direction, _, is_clk] in pins if direction]
                print(",\n".join(pinstr), file=f)

                # Module assign body
                print(");", file=f)
                for [pin, direction, function, _] in pins:
                    if function:
                        print(fixfun(function), file=f)
                print("endmodule", file=f)

if __name__ == "__main__":
    dolib2v(sys.argv[1:])
