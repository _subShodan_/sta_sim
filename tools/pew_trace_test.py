#!/usr/bin/python3

import unittest
import pew_trace
import tempfile
from collections import namedtuple
import os

class TestPewTrace(unittest.TestCase):

    def test_pew_trace1(self):
        outf = tempfile.NamedTemporaryFile(suffix=".txt")
        inf = tempfile.NamedTemporaryFile(suffix=".txt")
        gf = tempfile.NamedTemporaryFile(suffix=".dot")
        cmdline = ["-vvvv", "pew_trace_test.v", "-t", "otaes", "-i", "_24558_", "--long", "-o", outf.name, "-f", inf.name, "-g", gf.name]
        # Test file input
        inf.write(b"_24623_\n_24552_\n")
        inf.flush()

        pew_trace.pew_trace(*pew_trace.get_opts(cmdline))
        os.system("cp {} /tmp/bla.txt".format(outf.name))
        os.system("cp {} /tmp/bla.dot".format(gf.name))

        cmp_v = "pew_trace_test_out.txt"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

        cmp_v = "pew_trace_test_out.dot"
        diff = os.system("diff -u " + cmp_v + " " + gf.name)
        self.assertEqual(0, diff, "Difference with expected output")


if __name__ == '__main__':
    unittest.main()
