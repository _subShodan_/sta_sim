#!/usr/bin/python3

import unittest
import harden
import tempfile
from collections import namedtuple
import os

class TestHarden(unittest.TestCase):


    def test_harden1(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        cmdline = ["counter.v", outf.name, "-vvvvv", "-c", "_506_", "-c", "_234_", "-2", "sky130_fd_sc_hs__nand2_1", "-3", "sky130_fd_sc_hs__nand3_1"]

        harden.harden(*harden.get_opts(cmdline))
        os.system("cp {} /tmp/bla.v".format(outf.name))

        cmp_v = "harden_test_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_harden2(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        # Now do it with file input
        cmdline = ["counter.v", outf.name, "-vvvvv", "-2", "sky130_fd_sc_hs__nand2_1", "-3", "sky130_fd_sc_hs__nand3_1", "-r", "harden_test_pins.txt"]
        harden.harden(*harden.get_opts(cmdline))

        cmp_v = "harden_test_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_harden3(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        # And with duplicates
        cmdline = ["counter.v", outf.name, "-vvvvv", "-c", "_506_", "-c", "_234_", "-c", "_506_", "-c", "_234_", "-2", "sky130_fd_sc_hs__nand2_1", "-3", "sky130_fd_sc_hs__nand3_1", "-r", "harden_test_pins.txt"]
        harden.harden(*harden.get_opts(cmdline))

        cmp_v = "harden_test_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

if __name__ == '__main__':
    unittest.main()
