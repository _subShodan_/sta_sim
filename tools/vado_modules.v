module GND(output G);
    assign G = 1'b0;
endmodule

module VCC(output P);
    assign P = 1'b1;
endmodule

/*module RTL_BMERGE(input DATA, input I, output reg O, input S);
    always @* begin
        O = DATA;
        O[S +: 1] = I;
    end
endmodule*/

module RTL_ADD(input I0, input I1, output O);
    assign O = I0 + I1;
endmodule

module RTL_SUB(input I0, input I1, output O);
    assign O = I0 - I1;
endmodule

// XXX I2 is unknown, seems to be always 1
module RTL_LSHIFT(input I0, input I1, input I2, output O);
/*    if (I2 != 1'b1)
    begin
        $display("Don't know what I2 != 1 means");
        $finish;
    end*/
    assign O = I0 << I1;
endmodule

// XXX I2 is unknown, seems to be always 1
module RTL_RSHIFT(input I0, input I1, input I2, output O);
/*    if (I2 != 1'b1)
    begin
        $display("Don't know what I2 != 1 means");
        $finish;
    end*/
    assign O = I0 >> I1;
endmodule

module RTL_REDUCTION_AND(input I0, output O);
    assign O = &I0;
endmodule

module RTL_REDUCTION_OR(input I0, output O);
    assign O = |I0;
endmodule

module RTL_REDUCTION_XOR(input I0, output O);
    assign O = ^I0;
endmodule

module RTL_REDUCTION_NOR(input I0, output O);
    assign O = ~|I0;
endmodule


module RTL_NEQ(input I0, input I1, output O);
    assign O = I0 != I1;
endmodule

module RTL_MULT(input I0, input I1, output O);
    assign O = I0 * I1;
endmodule

module RTL_EQ(input I0, input I1, output O);
    assign O = I0 == I1;
endmodule

module RTL_GT(input I0, input I1, output O);
    assign O = I0 > I1;
endmodule

module RTL_AND(input I0, input I1, output O);
    assign O = I0 & I1;
endmodule

module RTL_OR(input I0, input I1, output O);
    assign O = I0 | I1;
endmodule

module RTL_XOR(input I0, input I1, output O);
    assign O = I0 ^ I1;
endmodule

module RTL_INV(input I0, output O);
    assign O = ~I0;
endmodule
