// antlr4 -Dlanguage=Python3 vcd.g4
grammar fi_diff_vcd;

vcdfile : head body EOF;

head : version date timescale scope enddefinitions;
body : (clock | binnum | onebit)*;

// These 1:1 parser<>lexer rules are to trigger the listener
version: VERSION;
date: DATE;
timescale: TIMESCALE;
scope : SCOPE (var | scope)* upscope;
enddefinitions: ENDDEFINITIONS;
var : VAR;
upscope : UPSCOPE;
clock : CLOCK;
binnum : BINNUM;
onebit : ONEBIT;

VERSION : '$version' .*? '$end';
DATE : '$date' .*? '$end';
TIMESCALE : '$timescale' .*? '$end';
SCOPE : '$scope' .*? '$end';
UPSCOPE : '$upscope' .*? '$end';
VAR : '$var' .*? '$end';
ENDDEFINITIONS : '$enddefinitions' .*? '$end';

//   $var wire 32 ?>" test_done_o [31:0] $end
//   $var wire  1 CD:" clk_i $end
//VAR : '$var' 'wire' [0-9]+ VARNAME VARNAME (VARNAME)? '$end';


//fragment VARNAME : [a-zA-Z0-9+;!/?:()"_[\]>'{}$%<#\-*,.=&@\\^`|~]+;
fragment VARNAME : [!-~]+;
CLOCK : '#'[0-9]+;
BINNUM : 'b'[01]+ WHITESPACE VARNAME;
ONEBIT : [01] VARNAME;

WHITESPACE : [ \t\r\n]+ -> channel(HIDDEN);

