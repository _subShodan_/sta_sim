#!/usr/bin/python3

import subprocess
import re
import sys
import os
import concurrent.futures

MAX=10

def runcmd(cmd):
    print(f"Running {cmd}")
    subprocess.run(cmd)

def domachine(machine, zone, start, end):
    print(f"Doing {machine} in {zone} from {start} to {end}")
    runcmd(["gcloud", "compute", "scp", "--recurse", script, package, f"{machine}:~/", "--zone", zone])
    runcmd(["gcloud", "compute", "ssh", machine, "--zone", zone, f"--command=nohup ./cloudstart.sh {start} {end} >stdout 2>stderr </dev/null &"])
    print(f"Done {machine} in {zone}")

(_, script, package, start, size) = sys.argv
start, size = (int(start), int(size))
print(f"Uploading f{script} and f{package} and starting {start} and increasing with {size}")

with concurrent.futures.ThreadPoolExecutor(max_workers=MAX) as executor:
    futures = []
    result = subprocess.run(["gcloud", "compute", "instances", "list"], stdout=subprocess.PIPE)
    lines = result.stdout.decode('utf-8').splitlines()
    for line in lines[1:]: # skip first
        (machine, zone) = line.split()[0:2]
        end = start + size
        futures.append(executor.submit(domachine, machine, zone, start, end))
        start = end

    concurrent.futures.wait(futures)
