#!/usr/bin/python3

from optparse import OptionParser
import antlr4
from antlr4.InputStream import InputStream
from antlr4.FileStream import FileStream
from fi_diff_vcdLexer import fi_diff_vcdLexer as vcdLexer
from fi_diff_vcdParser import fi_diff_vcdParser as vcdParser
from fi_diff_vcdListener import fi_diff_vcdListener as vcdListener
import IPython
import time
import re
import sys

class BuildVCDInfo(vcdListener):
    def __init__(self, prefix):
        super().__init__()
        self.timescale = None
        self.scopevar = [] # List of tuples (string, shortname or None)
        self.knownvars = set()
        self.clock = None
        self.vcs = []
        self.prefix = prefix # What prefix to add to shortnames when outputting diffs
        self.curvalue = {}
        self.newvalue = {}
    
    def enterTimescale(self, ctx):
        self.timescale = ctx.getText()
        if options.verbose >= 2:
            print(f"Found timescale: {self.timescale}")

    def enterScope(self, ctx):
        # We take the first child, which is the scope definition. The rest of the children will be visited later by the parser
        assert len(ctx.children) >= 1 
        new = ctx.children[0].getText()
        self.scopevar.append((new, None))
        if options.verbose >= 2:
            print(f"Found scope: {new}")
        
    def enterUpscope(self, ctx):
        new = ctx.getText()
        self.scopevar.append((new, None))
        if options.verbose >= 2:
            print(f"Found upscope: {new}")

    def enterEnddefinitions(self, ctx):
        new = ctx.getText()
        self.scopevar.append((new, None))
        if options.verbose >= 2:
            print(f"Found enddefinitions: {new}")
        
    def enterVar(self, ctx):
        new = ctx.getText()

        # Yes, I know I'm adding tokenization to a parser here... I just couldn't figure it out in antlr
        split = new.split()
        assert len(split) == 6 or len(split) == 7
        assert split[0] == '$var'
        assert split[1] == 'wire'
        assert split[-1] == '$end'
        (bits, shortname, longname) = split[2:5]
        if options.verbose >= 2:
            print(f"var: bits {bits} short {shortname} long {longname}")

        # Update lookup
        self.knownvars.add(shortname)

        # Create new var
        new = f"$var wire 1 {self.prefix}{shortname} {longname}_{self.prefix} $end";
        self.scopevar.append((new, shortname))
        if options.verbose >= 2:
            print(f"Found var: {new}")

    def updateValues(self):
        # Any new values that need to be stored?
        if self.newvalue:
            if options.verbose >= 2:
                print(f"Updates for clock {self.clock}: {self.newvalue}")

            # Check all variables whether they have been updated
            # If so, add to updates list
            updates = []
            newvals = []
            for name, val in self.newvalue.items():
                curval = self.curvalue.get(name)
                if curval is None or val != curval:
                    updates.append(name)
                    newvals.append(val)
                self.curvalue[name] = val

            self.vcs.append((self.clock, updates, newvals))
            self.newvalue = {}


    def enterClock(self, ctx):
        clock = ctx.getText()
        assert clock[0] == '#'
        newclock = int(clock[1:])
        assert self.clock is None or newclock > self.clock

        self.updateValues()

        # Update clock
        self.clock = newclock

        if options.verbose >= 2:
            print(f"Found clock: {self.clock}")


    def enterBinnum(self, ctx):
        assert self.clock is not None
        binnum = ctx.getText()
        (num, shortname) = binnum.split()
        assert num.startswith('b')
        assert shortname in self.knownvars, f"{shortname} doesn't exist in our lookup table"
        assert num[1:-1] == "0"*(len(num)-2) or num[1:-1] == "1"*(len(num)-2), f"{num} isn't all 0 or all 1 (except for last character)"

        bit = num[-1]
        assert bit == '0' or bit == '1'
        if options.verbose >= 2:
            print(f"Found binnum: {binnum} -> {shortname} : {bit}")

        assert shortname not in self.newvalue, f"{shortname} mentioned for the second time in this clock"
        self.newvalue[shortname] = bit

    def enterOnebit(self, ctx):
        assert self.clock is not None
        onebit = ctx.getText()
        bit = onebit[0]
        shortname = onebit[1:]
        assert bit == '0' or bit == '1'
        if options.verbose >= 2:
            print(f"Found onebit : {onebit} -> {shortname} : {bit}")
        if shortname not in self.knownvars:
            print(f"Warning, {shortname} not found in var decls")
        assert shortname not in self.newvalue, f"{shortname} mentioned for the second time in this clock"
        self.newvalue[shortname] = bit


def parse(filename, prefix):
    input_stream = FileStream(filename)
    lexer = vcdLexer(input_stream)
    stream = antlr4.CommonTokenStream(lexer)
    parser = vcdParser(stream)
    tree = parser.vcdfile()
    vcdInfo = BuildVCDInfo(prefix)
    walker = antlr4.ParseTreeWalker()
    walker.walk(vcdInfo, tree)
    vcdInfo.updateValues() # finalize any left-over values
    return vcdInfo

def parse_args(args):
    global options

    # Cmd line stuff
    USAGE_STRING = "Usage: fi_diff.py [normal vcd file] [faulted vcd file] [diff vcd file]\n Compiling vcd.g4 is done by: antlr4 -Dlanguage=Python3 vcd.g4"
    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", dest="verbose", default=0, action="count", help="verbosity")
    (options, args) = optparser.parse_args(args)

    if len(args) != 3:
        print(USAGE_STRING)
        sys.exit(1)

    vcdNormalFile = args[0]
    vcdFaultFile = args[1]
    vcdDiffFile = args[2]

    return (vcdNormalFile, vcdFaultFile, vcdDiffFile)

def calc_diff(a, b):
    # Calculate the difference between the two update lists
    # Structure a or b = [(clock, updates, newvals)], where updates = [shortnames] and corresponding new values. We only check updates, not new values, because changes in the latter have already been filtered in the parser.
    # Both a and b are sorted in increasing clocks, so we can linearly progress through them
    # Any updates that are present (irregardless of bit) in a clock are added to the diffs set
    i_a = 0
    i_b = 0
    diffs = set()
    while i_a < len(a) and i_b < len(b):
        clocku_a = a[i_a]
        clocku_b = b[i_b]

        if options.verbose >= 3:
            print(f"a[{i_a}]={clocku_a} and b[{i_b}]={clocku_b}")

        # Shortcut
        if clocku_a == clocku_b:
            if options.verbose >= 2:
                print(f"Clock {clocku_a} is the same")
            i_a+=1
            i_b+=1
            continue
    
        # Unpack
        (clock_a, updates_a, newvals_a) = clocku_a
        (clock_b, updates_b, newvals_b) = clocku_b

        if clock_a > clock_b:
            # clocks b something happened that did not in a. Add b updates to diff.
            if options.verbose >= 2:
                print(f"Clock a {clock_a} is ahead of clock b {clock_b}")
            diffs.update(updates_b)
            i_b += 1
        elif clock_b > clock_a:
            # clocks a something happened that did not in b. Add a updates to diff.
            if options.verbose >= 2:
                print(f"Clock b {clock_b} is ahead of clock a {clock_a}")
            diffs.update(updates_a)
            i_a += 1
        else:
            # diff is in updates, not clock
            if options.verbose >= 2:
                print(f"Clocks equals at {clock_b}=={clock_a} but contents differ")
            vars_a = set(updates_a)
            vars_b = set(updates_b)

            # Calc diff and update
            diffs.update(vars_a.symmetric_difference(vars_b))

            # Step both clocks
            i_a += 1
            i_b += 1

    # End case
    if i_a < len(a):
        if options.verbose >= 2:
            print(f"A has trailing updates we'll add")
        diffs.update([var for up in a[i_a:] for var in up[1]])
    if i_b < len(b):
        if options.verbose >= 2:
            print(f"B has trailing updates we'll add")
        diffs.update([var for up in b[i_b:] for var in up[1]])

    return diffs

def vcd_write(vcdNormal, vcdFault, vcdDiffFile, diff):
    with open(vcdDiffFile, "w") as output:
        # HEader
        output.write("$version Generated by fi_diff $end\n")
        output.write("$date " + time.ctime() + " $end\n")
        output.write(vcdNormal.timescale + "\n")

        # Other scope and vars; filter on whether in diff
        for (normal, faulty) in zip(vcdNormal.scopevar, vcdFault.scopevar):
            (string_n, shortname_n) = normal
            (string_f, shortname_f) = faulty
            assert shortname_n == shortname_f, "Short names should match"

            # Headers
            if shortname_n is None:
                assert string_n == string_f
                output.write(string_n + "\n")

            # Actual variable
            if shortname_n in diff:
                output.write(string_n + "\n")
                output.write(string_f + "\n")

        # Write signals that are in the diff
        output.write("\n")
        (i_n, i_f) = (0, 0)
        while i_n < len(vcdNormal.vcs) or i_f < len(vcdFault.vcs):
            # Unpack
            (clock_n, clock_f) = (None, None)
            if i_n < len(vcdNormal.vcs):
                (clock_n, updates_n, vals_n) = vcdNormal.vcs[i_n]
            if i_f < len(vcdFault.vcs):
                (clock_f, updates_f, vals_f) = vcdFault.vcs[i_f]

            # Let the logic below dump the remainder
            if clock_n is None:
                clock_n = clock_f + 1
            if clock_f is None:
                clock_f = clock_n + 1

            # Write normal 
            updates = []
            clock_out = None
            if clock_n <= clock_f:
                clock_out = clock_n
                updates.extend([f"{val}{vcdNormal.prefix}{name}" for (name, val) in zip(updates_n, vals_n) if name in diff])
                i_n += 1
            # Write faulty
            if clock_f <= clock_n:
                clock_out = clock_f
                updates.extend([f"{val}{vcdFault.prefix}{name}" for (name, val) in zip(updates_f, vals_f) if name in diff])
                i_f += 1
                
            if updates:
                output.write(f"#{clock_out}\n")
                output.write("\n".join(updates) + "\n")

    return


def dofi_diff(args):
    (vcdNormalFile, vcdFaultFile, vcdDiffFile) = parse_args(args)

    # Parse input files
    if options.verbose >= 1:
        print(f"Parsing {vcdNormalFile}")
    vcdNormal = parse(vcdNormalFile, 'n')
    if options.verbose >= 1:
        print(f"Parsing {vcdFaultFile}")
    vcdFault = parse(vcdFaultFile, 'f')

    # Calculate differences
    if options.verbose >= 1:
        print(f"Calculating differences")
    assert vcdNormal.timescale == vcdFault.timescale, "Timescales different"
    diff = calc_diff(vcdNormal.vcs, vcdFault.vcs)
    if options.verbose >= 2:
        print(f"Differences: {diff}")

    # Output new vcd file
    if options.verbose >= 1:
        print(f"Writing output to {vcdDiffFile}")
    vcd_write(vcdNormal, vcdFault, vcdDiffFile, diff)


if __name__ == "__main__":
    dofi_diff(sys.argv[1:])
