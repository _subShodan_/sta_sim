
(* XLNX_LINE_COL = "459520" *)


module add
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2126" *)

  RTL_ADD_0
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module bmerge
(
  DATA,
  I,
  O,
  S
);

  input [7:0] DATA;
  input I;
  output [7:0] O;
  input [2:0] S;
  wire [7:0] DATA;
  wire I;
  wire [7:0] O;
  wire [2:0] S;
  (* XLNX_LINE_COL = "524544" *)
  (* map_to_module = "2134" *)

  RTL_BMERGE_1
  O0_i
  (
    .DATA(DATA),
    .I(I),
    .O(O),
    .S(S)
  );


endmodule


(* XLNX_LINE_COL = "459776" *)


module bsel
(
  I,
  S,
  O
);

  input [3:0] I;
  input [1:0] S;
  output O;
  wire [3:0] I;
  wire O;
  wire [1:0] S;
  (* XLNX_LINE_COL = "983296" *)
  (* map_to_module = "2141" *)

  RTL_BSEL_2
  O_i
  (
    .I(I),
    .O(O),
    .S(S)
  );


endmodule


(* XLNX_LINE_COL = "460544" *)


module d_latch
(
  D,
  G,
  Q
);

  input [7:0] D;
  input [7:0] G;
  output [7:0] Q;
  wire [7:0] D;
  wire [7:0] G;
  wire [7:0] Q;
  (* XLNX_LINE_COL = "786688" *)
  (* map_to_module = "2139" *)

  RTL_LATCH_3
  Q_reg
  (
    .D(D),
    .G(G),
    .Q(Q)
  );


endmodule


(* XLNX_LINE_COL = "459264" *)


module eq
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire O;
  (* XLNX_LINE_COL = "1180160" *)
  (* map_to_module = "2129" *)

  RTL_EQ_4
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "462592" *)


module ff_C_CE_CLR_D_Q
(
  C,
  CE,
  CLR,
  D,
  Q
);

  input C;
  input CE;
  input CLR;
  input D;
  output Q;
  wire C;
  wire CE;
  wire CLR;
  wire D;
  wire Q;
  (* PRIMITIVE_NAME = "RTL_REG" *)
  (* XLNX_LINE_COL = "786688" *)
  (* map_to_module = "2145" *)

  RTL_REG_ASYNC__BREG_4_5
  Q_reg
  (
    .C(C),
    .CE(CE),
    .CLR(CLR),
    .D(D),
    .Q(Q)
  );


endmodule


(* XLNX_LINE_COL = "461568" *)


module ff_C_CE_D_Q
(
  C,
  CE,
  D,
  Q
);

  input C;
  input CE;
  input D;
  output Q;
  wire C;
  wire CE;
  wire D;
  wire Q;
  (* PRIMITIVE_NAME = "RTL_REG" *)
  (* XLNX_LINE_COL = "1048832" *)
  (* map_to_module = "2142" *)

  RTL_REG__BREG_1_6
  Q_reg
  (
    .C(C),
    .CE(CE),
    .D(D),
    .Q(Q)
  );


endmodule


(* XLNX_LINE_COL = "462592" *)


module ff_C_CE_PRE_D_Q
(
  C,
  CE,
  PRE,
  D,
  Q
);

  input C;
  input CE;
  input PRE;
  input D;
  output Q;
  wire C;
  wire CE;
  wire D;
  wire PRE;
  wire Q;
  (* PRIMITIVE_NAME = "RTL_REG" *)
  (* XLNX_LINE_COL = "786688" *)
  (* map_to_module = "2146" *)

  RTL_REG_ASYNC__BREG_5_7
  Q_reg
  (
    .C(C),
    .CE(CE),
    .D(D),
    .PRE(PRE),
    .Q(Q)
  );


endmodule


(* XLNX_LINE_COL = "461824" *)


module ff_C_D_Q_RST
(
  C,
  D,
  Q,
  RST
);

  input C;
  input D;
  output Q;
  input RST;
  wire C;
  wire D;
  wire Q;
  wire RST;
  (* PRIMITIVE_NAME = "RTL_REG" *)
  (* XLNX_LINE_COL = "1048832" *)
  (* map_to_module = "2143" *)

  RTL_REG_SYNC__BREG_2_8
  Q_reg
  (
    .C(C),
    .D(D),
    .Q(Q),
    .RST(RST)
  );


endmodule


(* XLNX_LINE_COL = "461824" *)


module ff_C_D_Q_SET
(
  C,
  D,
  Q,
  SET
);

  input C;
  input D;
  output Q;
  input SET;
  wire C;
  wire D;
  wire Q;
  wire SET;
  (* PRIMITIVE_NAME = "RTL_REG" *)
  (* XLNX_LINE_COL = "1048832" *)
  (* map_to_module = "2144" *)

  RTL_REG_SYNC__BREG_3_9
  Q_reg
  (
    .C(C),
    .D(D),
    .Q(Q),
    .SET(SET)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module lshift
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire \<const1> ;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "983296" *)
  (* map_to_module = "2124" *)

  RTL_LSHIFT_10
  O_i
  (
    .I0(I0),
    .I1(I1),
    .I2(\<const1> ),
    .O(O)
  );


  VCC_11
  VCC
  (
    .P(\<const1> )
  );


endmodule


(* XLNX_LINE_COL = "459776" *)


module mand
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2130" *)

  RTL_AND_12
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module mor
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2132" *)

  RTL_OR_13
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module mul
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2133" *)

  RTL_MULT_14
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module mux
(
  I0,
  I1,
  O,
  S
);

  input I0;
  input I1;
  output O;
  input S;
  wire I0;
  wire I1;
  wire O;
  wire S;
  (* SEL_VAL = "I0:S=1'b0,I1:S=1'b1" *)
  (* XLNX_LINE_COL = "525312" *)
  (* map_to_module = "2140" *)

  RTL_MUX_15
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O),
    .S(S)
  );


endmodule


(* XLNX_LINE_COL = "459776" *)


module mxor
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2131" *)

  RTL_XOR_16
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module neq
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire O;
  (* XLNX_LINE_COL = "1180160" *)
  (* map_to_module = "2128" *)

  RTL_NEQ_17
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module redand
(
  I0,
  O
);

  input [7:0] I0;
  output O;
  wire [7:0] I0;
  wire O;
  (* XLNX_LINE_COL = "983296" *)
  (* map_to_module = "2135" *)

  RTL_REDUCTION_AND_18
  O_i
  (
    .I0(I0),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module rednor
(
  I0,
  O
);

  input [7:0] I0;
  output O;
  wire [7:0] I0;
  wire O;
  (* XLNX_LINE_COL = "983552" *)
  (* map_to_module = "2138" *)

  RTL_REDUCTION_NOR_19
  O_i
  (
    .I0(I0),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "460032" *)


module redor
(
  I0,
  O
);

  input [7:0] I0;
  output O;
  wire [7:0] I0;
  wire O;
  (* XLNX_LINE_COL = "983296" *)
  (* map_to_module = "2137" *)

  RTL_REDUCTION_OR_20
  O_i
  (
    .I0(I0),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module redxor
(
  I0,
  O
);

  input [7:0] I0;
  output O;
  wire [7:0] I0;
  wire O;
  (* XLNX_LINE_COL = "983296" *)
  (* map_to_module = "2136" *)

  RTL_REDUCTION_XOR_21
  O_i
  (
    .I0(I0),
    .O(O)
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module rom
(
  A,
  O
);

  input [5:0] A;
  output [0:0] O;
  wire \<const0> ;
  wire [5:0] A;
  wire [0:0] O;
  wire O_i__0_n_0;
  wire O_i__1_n_0;
  wire O_i__2_n_0;
  wire O_i__3_n_0;
  wire O_i__4_n_0;
  wire O_i_n_0;

  GND_22
  GND
  (
    .G(\<const0> )
  );

  (* INIT_VAL = "INIT_DEFAULT:1'b0,INIT_14:1'b1" *)
  (* XLNX_LINE_COL = "10224128" *)
  (* map_to_module = "2153" *)

  RTL_ROM_23
  O_i
  (
    .A(A),
    .O(O_i_n_0)
  );

  (* SEL_VAL = "I0:S=6'b110111,I1:S=default" *)
  (* XLNX_LINE_COL = "8520192" *)
  (* map_to_module = "2152" *)

  RTL_MUX5_24
  O_i__0
  (
    .I0(\<const0> ),
    .I1(O_i_n_0),
    .O(O_i__0_n_0),
    .S(A)
  );

  (* SEL_VAL = "I0:S=6'b011011,I1:S=default" *)
  (* XLNX_LINE_COL = "6816256" *)
  (* map_to_module = "2151" *)

  RTL_MUX5_25
  O_i__1
  (
    .I0(\<const0> ),
    .I1(O_i__0_n_0),
    .O(O_i__1_n_0),
    .S(A)
  );

  (* SEL_VAL = "I0:S=6'b100010,I1:S=default" *)
  (* XLNX_LINE_COL = "5112320" *)
  (* map_to_module = "2150" *)

  RTL_MUX5_26
  O_i__2
  (
    .I0(\<const0> ),
    .I1(O_i__1_n_0),
    .O(O_i__2_n_0),
    .S(A)
  );

  (* SEL_VAL = "I0:S=6'b010000,I1:S=default" *)
  (* XLNX_LINE_COL = "3408384" *)
  (* map_to_module = "2149" *)

  RTL_MUX5_27
  O_i__3
  (
    .I0(\<const0> ),
    .I1(O_i__2_n_0),
    .O(O_i__3_n_0),
    .S(A)
  );

  (* SEL_VAL = "I0:S=6'b101001,I1:S=default" *)
  (* XLNX_LINE_COL = "1704448" *)
  (* map_to_module = "2148" *)

  RTL_MUX5_28
  O_i__4
  (
    .I0(\<const0> ),
    .I1(O_i__3_n_0),
    .O(O_i__4_n_0),
    .S(A)
  );

  (* SEL_VAL = "I0:S=6'b111100,I1:S=default" *)
  (* XLNX_LINE_COL = "512" *)
  (* map_to_module = "2147" *)

  RTL_MUX5_29
  O_i__5
  (
    .I0(\<const0> ),
    .I1(O_i__4_n_0),
    .O(O),
    .S(A)
  );


endmodule


(* XLNX_LINE_COL = "460288" *)


module rshift
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire \<const1> ;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "918016" *)
  (* map_to_module = "2125" *)

  RTL_RSHIFT_30
  O_i
  (
    .I0(I0),
    .I1(I1),
    .I2(\<const1> ),
    .O(O)
  );


  VCC_31
  VCC
  (
    .P(\<const1> )
  );


endmodule


(* XLNX_LINE_COL = "459520" *)


module sub
(
  I0,
  I1,
  O
);

  input [7:0] I0;
  input [7:0] I1;
  output [7:0] O;
  wire [7:0] I0;
  wire [7:0] I1;
  wire [7:0] O;
  (* XLNX_LINE_COL = "1179904" *)
  (* map_to_module = "2127" *)

  RTL_SUB_32
  O_i
  (
    .I0(I0),
    .I1(I1),
    .O(O)
  );


endmodule


module RTL_ADD_0
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 + I1;

endmodule

module RTL_BMERGE_1(input [7:0] DATA, input [0:0] I, output [7:0] O, input [2:0] S);
    wire [7:0] mask, Iw;
    assign mask = 8'b1 << S;
    assign Iw = I << S;
    assign O = (DATA & ~mask) | Iw;
endmodule

module RTL_BSEL_2(input [3:0] I, output [0:0] O, input [1:0] S);
    assign O[0+:1] = I[S+:1];
endmodule

module RTL_LATCH_3(input [7:0] D, input [7:0] G, output reg [7:0] Q);
    genvar i;
    for (i = 0; i <= 7; i = i + 1) begin
        always @ (G[i] or D[i])  
            if (G[i])  
                Q[i] <= D[i];  
        end
endmodule  

module RTL_EQ_4
(
  input [7:0] I0,
  input [7:0] I1,
  output O
);

  assign O = I0 == I1;

endmodule

module RTL_REG_ASYNC__BREG_4_5(input [0:0] C, input [0:0] CE, input [0:0] CLR, input [0:0] D, output reg [0:0] Q);
    wire CEx, RSTx, SETx, PREx, CLRx;
    assign CEx = CE;
    assign RSTx = 1'b0;
    assign SETx = 1'b0;
    assign PREx = 1'b0;
    assign CLRx = CLR;

    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule

module RTL_REG__BREG_1_6(input [0:0] C, input [0:0] CE, input [0:0] D, output reg [0:0] Q);
    wire CEx, RSTx, SETx, PREx, CLRx;
    assign CEx = CE;
    assign RSTx = 1'b0;
    assign SETx = 1'b0;
    assign PREx = 1'b0;
    assign CLRx = 1'b0;

    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule

module RTL_REG_ASYNC__BREG_5_7(input [0:0] C, input [0:0] CE, input [0:0] D, input [0:0] PRE, output reg [0:0] Q);
    wire CEx, RSTx, SETx, PREx, CLRx;
    assign CEx = CE;
    assign RSTx = 1'b0;
    assign SETx = 1'b0;
    assign PREx = PRE;
    assign CLRx = 1'b0;

    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule

module RTL_REG_SYNC__BREG_2_8(input [0:0] C, input [0:0] D, output reg [0:0] Q, input [0:0] RST);
    wire CEx, RSTx, SETx, PREx, CLRx;
    assign CEx = 1'b1;
    assign RSTx = RST;
    assign SETx = 1'b0;
    assign PREx = 1'b0;
    assign CLRx = 1'b0;

    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule

module RTL_REG_SYNC__BREG_3_9(input [0:0] C, input [0:0] D, output reg [0:0] Q, input [0:0] SET);
    wire CEx, RSTx, SETx, PREx, CLRx;
    assign CEx = 1'b1;
    assign RSTx = 1'b0;
    assign SETx = SET;
    assign PREx = 1'b0;
    assign CLRx = 1'b0;

    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule

module RTL_LSHIFT_10
(
  input [7:0] I0,
  input [7:0] I1,
  input I2,
  output [7:0] O
);

  assign O = I0 << I1;

endmodule

module VCC_11
(
  output P
);

  assign P = 1'b1;

endmodule

module RTL_AND_12
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 & I1;

endmodule

module RTL_OR_13
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 | I1;

endmodule

module RTL_MULT_14
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 * I1;

endmodule

module RTL_MUX_15(input [0:0] I0, input [0:0] I1, input [0:0] S, output [0:0] O);
    assign O = (S==1'b0 ? I0 : (S==1'b1 ? I1 : 1'bx));
endmodule

module RTL_XOR_16
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 ^ I1;

endmodule

module RTL_NEQ_17
(
  input [7:0] I0,
  input [7:0] I1,
  output O
);

  assign O = I0 != I1;

endmodule

module RTL_REDUCTION_AND_18
(
  input [7:0] I0,
  output O
);

  assign O = &I0;

endmodule

module RTL_REDUCTION_NOR_19
(
  input [7:0] I0,
  output O
);

  assign O = ~|I0;

endmodule

module RTL_REDUCTION_OR_20
(
  input [7:0] I0,
  output O
);

  assign O = |I0;

endmodule

module RTL_REDUCTION_XOR_21
(
  input [7:0] I0,
  output O
);

  assign O = ^I0;

endmodule

module GND_22
(
  output G
);

  assign G = 1'b0;

endmodule

module RTL_ROM_23(input [5:0] A, output [0:0] O);
    assign O = (A == 6'd14 ? 1'b1 : 1'bx);
endmodule

module RTL_MUX5_24(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b110111 ? I0 : I1);
endmodule

module RTL_MUX5_25(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b011011 ? I0 : I1);
endmodule

module RTL_MUX5_26(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b100010 ? I0 : I1);
endmodule

module RTL_MUX5_27(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b010000 ? I0 : I1);
endmodule

module RTL_MUX5_28(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b101001 ? I0 : I1);
endmodule

module RTL_MUX5_29(input [0:0] I0, input [0:0] I1, input [5:0] S, output [0:0] O);
    assign O = (S==6'b111100 ? I0 : I1);
endmodule

module RTL_RSHIFT_30
(
  input [7:0] I0,
  input [7:0] I1,
  input I2,
  output [7:0] O
);

  assign O = I0 >> I1;

endmodule

module VCC_31
(
  output P
);

  assign P = 1'b1;

endmodule

module RTL_SUB_32
(
  input [7:0] I0,
  input [7:0] I1,
  output [7:0] O
);

  assign O = I0 - I1;

endmodule
