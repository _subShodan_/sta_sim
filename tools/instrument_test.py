#!/usr/bin/python3

import pickle
import unittest
import instrument
import tempfile
from collections import namedtuple
import os
import subprocess
from instrument import GlitchItem

class TestInstrument(unittest.TestCase):

    def check_gm(self, ff_only):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        mapf = tempfile.NamedTemporaryFile(suffix=".map")
        instrument.instrument(*instrument.get_opts(["/OpenROAD-flow-public/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib", "counter.v", outf.name, mapf.name, "-vv", ff_only]))

        # Check whether the number of glitches in the .v file matches the glitchmap
        process = subprocess.run("grep \"glitches\\[\" {} | wc -l".format(outf.name), shell=True, capture_output=True)
        num_glitches = int(process.stdout.decode("utf-8").strip())

        gm = pickle.load(mapf)
        self.assertEqual(len(gm), num_glitches)
        return gm
        
    def test_gm(self):
        gm = self.check_gm("--ff-only")
        self.assertEqual(gm[0], GlitchItem(index=0, moduleinstance='sky130_fd_sc_hs__dfxtp_1', modulename='_452_', inputport='D', inputwire='_000_', outputport='Q', outputwire='counter_out'))
        self.assertEqual(gm[-1], GlitchItem(index=63, moduleinstance='sky130_fd_sc_hs__dfxtp_1', modulename='_515_', inputport='D', inputwire='_059_', outputport='Q', outputwire='counter_out'))

    def test_gm_all(self):
        gm = self.check_gm("--all")
        self.assertEqual(gm[0], GlitchItem(index=0, moduleinstance='sky130_fd_sc_hs__and4_4', modulename='_229_', inputport='A', inputwire='_066_', outputport='X', outputwire='_067_'))
        self.assertEqual(gm[-1], GlitchItem(index=592, moduleinstance='sky130_fd_sc_hs__dfxtp_1', modulename='_515_', inputport='D', inputwire='_059_', outputport='Q', outputwire='counter_out'))

    def test_clk_exclude(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        mapf = tempfile.NamedTemporaryFile(suffix=".map")
        instrument.instrument(*instrument.get_opts(["/OpenROAD-flow-public/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib", "instrument_test_clk.v", outf.name, mapf.name, "-vv"]))
        #os.system("cp {} /tmp/bla.v".format(outf.name))

        cmp_v = "instrument_test_clk_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_trace_inputs(self):
        outputs = {
                "a": ["b", "c"], "b": ["c", "d"], 
                "e": "f", "f": "e", # Circular
                }

        wires = set()
        inputs = "ef"
        wires_should = set("ef")
        instrument.trace_inputs(wires, inputs, outputs)
        self.assertEqual(wires, wires_should)

        wires = set()
        inputs = ["z"]
        wires_should = set("z")
        instrument.trace_inputs(wires, inputs, outputs)
        self.assertEqual(wires, wires_should)

        wires = set()
        inputs = ["a"]
        wires_should = set("abcd")
        instrument.trace_inputs(wires, inputs, outputs)
        self.assertEqual(wires, wires_should)

    
    def check_diff(self, ff_only, cmp_v, cmp_map):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        mapf = tempfile.NamedTemporaryFile(suffix=".map")
        options = namedtuple("Options", ("verbose", "include", "define", "ff_only", "exclude"))(verbose=2, include=None, define=None, ff_only=ff_only, exclude=[])
        instrument.instrument("/OpenROAD-flow-public/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib", "counter.v", outf.name, mapf.name, options)
        os.system("cp {} /tmp/bla.v".format(outf.name))
        os.system("cp {} /tmp/bla.map".format(mapf.name))

        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

        diff = os.system("diff -u " + cmp_map + " " + mapf.name)
        self.assertEqual(0, diff, "Difference with expected output")

        outf.close()
        mapf.close()
        return

    def test_diffs(self):
        self.check_diff(True, "instrument_test_out.v", "instrument_test_out.map")

    def test_diffs_all(self):
        self.check_diff(False, "instrument_test_out_all.v", "instrument_test_out_all.map")

if __name__ == '__main__':
    unittest.main()
