#!/usr/bin/python3
# John Fitzgerald

import os, sys
import tempfile
from liberty.parser import parse_liberty
import pyverilog.vparser.ast as vast
import pyverilog.vparser.parser as vparser
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
import pickle
from optparse import OptionParser
from collections import namedtuple

import IPython

def get_opts(cmdline):
    USAGE_STRING = "Usage: instrument.py [liberty input] [netlist input] [netlist output] [map output]\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", dest="define", action="append", default=[], help="Macro Definition")
    optparser.add_option("--ff-only", dest="ff_only", action="store_true", default=True, help="Only add glitch signals to flipflop inputs")
    optparser.add_option("--all", dest="ff_only", action="store_false", help="Add glitch signals to all gate inputs")
    optparser.add_option("-E", "--exclude", dest="exclude", action="append", default=[], help="Exclude nets")
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 4:
        print("Wrong number of arguments, should be 4, got: ", args)
        print(USAGE_STRING)
        sys.exit(1)

    return (args[0], args[1], args[2], args[3], options)

def argname(port, add_idx=False):
    name = port.argname
    if type(name) == vast.Pointer:
        if add_idx:
            name = "{}[{}]".format(port.argname.var.name, port.argname.ptr.value)
        else:
            name = port.argname.var.name
    elif type(name) == vast.Identifier:
        name = name.name
    else:
        raise TypeError("Unknown type {}".format(type(name)))
    return name

def conv(escstr):
    if not escstr:
        return None
    return str(escstr).strip('"')


def load_liberty(libertyfile):
    # Load from picklefile if exists, otherwise create
    try:
        create = False
        with open(libertyfile + ".pickle", "rb") as pf:
            library = pickle.load(pf)
    except FileNotFoundError:
        create = True

    if create:
        with open(libertyfile) as lf:
            library = parse_liberty(lf.read())
        with open(libertyfile + ".pickle", "wb") as pf:
            pickle.dump(library, pf)

    # Loop over cells to gather relevant info
    libpinmap = dict()
    ff_clock = dict()
    single_in = set()
    for group in library.groups:
        if group.group_name == "cell":
            # Init
            cellname = conv(group.args[0])
            assert cellname not in libpinmap
            libpinmap[cellname] = dict()
            cellclk = None
            clk = None

            for subgroup in group.groups:
                # Collect flipflop info
                if subgroup.group_name == "ff":
                    clk = conv(subgroup.attributes["clocked_on"])
                    if clk.startswith('!'): # Trim negative clock prefix, we're just interested in the name
                        clk = clk[1:]
                    assert cellname not in ff_clock, "Somehow multiple flip flop blocks for this cell {}".format(cellname)
                    ff_clock[cellname] = clk

                # Add pins to cell pins
                if subgroup.group_name == "pin":
                    pinname = conv(subgroup.args[0])
                    if clk == pinname:
                        direction = "clk"
                    else:
                        direction = conv(subgroup.attributes["direction"] )
                    libpinmap[cellname][pinname] = direction

            if len(libpinmap[cellname]) == 2: # One-in-one-out == length two
                single_in.add(cellname)

    return libpinmap, ff_clock, single_in


def find_pin_direction(cell, pin, libpinmap):
    cell = libpinmap.get(cell)
    if not cell:
        return None
    pin = cell.get(pin)
    if not pin:
        return None

    return pin


def trace_inputs(clock_wires, inputs, outputs):
    if inputs is None:
        return

    for inputw in inputs:
        if inputw not in clock_wires:
            clock_wires.add(inputw)
            next_inputs = outputs.get(inputw)
            trace_inputs(clock_wires, next_inputs, outputs)


def trace_clock(ast, ff_clocks, libpinmap):
    # Loop over AST. Map outputs and clocks to instances for tracing.
    outputs = dict()
    clks = dict()
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue
        for item in definition.items:
            if isinstance(item, vast.InstanceList):
                for instance in item.instances:
                    output = None
                    inputs = []
                    clk = None

                    # Loop over ports
                    for port in instance.portlist:
                        direction = find_pin_direction(instance.module, port.portname, libpinmap)
                        if direction == "output":
                            assert output is None
                            output = argname(port)
                        elif direction == "input":
                            try:
                                inputs.append(argname(port))
                            except TypeError as err:
                                pass
                                # Ignoring type exception in tracing clocks. This is because it's some complex expression, which only happens when we XOR in glitch gates.
                        elif direction == "clk":
                            assert clk is None
                            clk = argname(port)

                    # Add inputs to clk or outputs
                    if clk:
                        clks[clk] = inputs
                    else:
                        outputs[output] = inputs

    # Now, map back from flipflop clocks 
    clock_wires = set()
    trace_inputs(clock_wires, clks, outputs)

    return clock_wires

GlitchItem = namedtuple("GlitchItem", ['index', 'moduleinstance', 'modulename', 'inputport', 'inputwire', 'outputport', 'outputwire'])
def instrument(libertyfile, netlistfile, outputfile, mapfile, options):
    # build pin map
    if options.verbose:
        print("parsing liberty file...")
    libpinmap, ff_clocks, single_in = load_liberty(libertyfile)

    if options.verbose >= 2:
        print("Pinmap: ", libpinmap)
        print("ff_clocks: ", ff_clocks)
        print("single_in: ", single_in)

    # instrument verilog
    if options.verbose:
        print("parsing code...")

    ast = vparser.VerilogCodeParser((netlistfile, ), preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    if options.verbose:
        print("Tracing clock wires...")

    exclude = trace_clock(ast, ff_clocks, libpinmap)
    exclude.update(options.exclude)
    if options.verbose >= 2:
        print("Excluded: ", exclude)


    #ast.show()
    if options.verbose:
        print("instrumenting ast...")

    # Start with empty glitchmap, iterate through modules
    gm = []
    ngwires = 0
    cells = 0
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue

        # add glitch port
        definition.portlist.ports += (vast.Port("glitches", None, None, None), )

        # add gltich signals
        for item in definition.items:
            if isinstance(item, vast.InstanceList):
                for instance in item.instances:
                    glitch_in = []
                    glitch_out = None
                    
                    # Skip non-ffs if requested
                    if options.ff_only and not instance.module in ff_clocks:
                        continue

                    # Skip single in-out 
                    if instance.module in single_in:
                        continue

                    try:
                        for port in instance.portlist:
                            direction = find_pin_direction(instance.module, port.portname, libpinmap)
                            aname = argname(port, add_idx=False)
                            if direction == "input": 
                                glitch_in.append((ngwires, port.portname, aname))
                                port.argname = vast.Xor(port.argname, vast.Pointer(vast.Identifier("glitches"), vast.IntConst(ngwires)))
                                ngwires += 1
                            elif direction == "output":
                                assert glitch_out == None, "Glitch out should be None, but is {}. Multiple outputs for instance {}/{}?".format(glitch_out, instance.name, instance.module)
                                glitch_out = (port.portname, aname)
                    except TypeError as err:
                        raise RuntimeError("Error processing instance {}".format(instance.name)) from err
                        
                    # Update glitchmap
                    if glitch_in:
                        cells += 1
                        for gi in glitch_in:
                            gm.append(GlitchItem(gi[0], instance.module, instance.name, gi[1], gi[2], glitch_out[0], glitch_out[1]))

        # add glitch input
        definition.items = (vast.Decl((vast.Input("glitches", vast.Width(vast.IntConst(ngwires - 1), vast.IntConst(0))), )), ) + definition.items

    if options.verbose:
        print("generating code...")

    codegen = ASTCodeGenerator()
    rslt = codegen.visit(ast)
    with open(outputfile, "w") as of:
        of.write(rslt)

    if options.verbose:
        print("writing glitch map...")

    with open(mapfile, "wb") as mf:
        pickle.dump(gm, mf)

    print("Instrumented {} cells and {} wires".format(cells, ngwires))

if __name__ == '__main__':
    instrument(*get_opts(sys.argv[1:]))
