#!/usr/bin/python3

import unittest
import bitlify
import tempfile
import os

class TestBitlify(unittest.TestCase):

    def test_headerfuncs(self):
        basedir = "/tmp/bitlifytest"
        vf = basedir + ".v" 
        pf = basedir + ".p" 
        hf = basedir + ".h"
        cf = basedir + ".c"
        of = basedir + ""
        COUNTER_LEN = 64
        COUNTER_LONG_LEN = 256
        
        with open(cf, "w") as cfile:
            counter_str = "\n".join(["    char counter_out_bit{}_[INTLEN];".format(i) for i in range(0, COUNTER_LEN)])
            counter_long_str = "\n".join(["    char counter_out_long_bit{}_[INTLEN];".format(i) for i in range(0, COUNTER_LONG_LEN)])

            cfile.write('''
#include "{}"
#include <stdint.h>

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {{log_error(M, ##__VA_ARGS__); assert(A); }}

#define INTLEN ((MACHINES+7)/8)
#define COUNTER_LEN ({}/8)
#define COUNTER_LONG_LEN ({}/8)

struct data {{
    // char counter_out_bitN_[INTLEN];
    {} 
    // char counter_out_long_bitN_[INTLEN];
    {} 

    char rst[INTLEN];
}};

void dump(struct data *y) {{
    for (int vm = 0; vm < MACHINES; vm++)
        printf("VM %02d = %02lx\\n", vm, get_counter_out(y, vm));
}}

void check_value(bitlify_t index, bool value, void* arg) {{
    uint64_t checkvalue = *((int*)arg);
    bool checkbit = (checkvalue >> index) & 1;
    assertf(checkbit == value, "Bit wrong, index %u value %d should be %d", index, value, checkbit);
}}

int main() {{
    bitlify_t word = 0;
    assert(word == 0);
    set_bit_in_word(&word, 0, 1);
    assert(word == 1);
    set_bit_in_word(&word, 0, 0);
    assert(word == 0);
    set_bit_in_word(&word, 1, 1);
    assert(word == 2);
    set_bit_in_word(&word, 0, 0);
    assert(word == 2);
    set_bit_in_word(&word, 0, 1);
    assert(word == 3);
    word = 0;
    const int MAXBIT = sizeof(bitlify_t)*8-1;
    set_bit_in_word(&word, MAXBIT, 1);
    assert(word == 1UL << MAXBIT);
    assert(get_bit_in_word(word, MAXBIT) == 1);
    assert(get_bit_in_word(word, MAXBIT-1) == 0);

    assert(bits_in_counter_out == {});
    assert(bits_in_counter_out_long == {});
    assert(bits_in_rst == 1);

    word = 0;
    set_bit_in_buf(&word, 0, 1);
    assert(word == 1);
    set_bit_in_buf(&word, 0, 0);
    assert(word == 0);
    set_bit_in_buf(&word, 1, 1);
    assert(word == 2);
    set_bit_in_buf(&word, 0, 0);
    assert(word == 2);
    set_bit_in_buf(&word, 0, 1);
    assert(word == 3);
    word = 0;
    set_bit_in_buf(&word, MAXBIT, 1);
    assert(word == 1UL << MAXBIT);
    assert(get_bit_from_buf(&word, MAXBIT) == 1);
    assert(get_bit_from_buf(&word, MAXBIT-1) == 0);

    const int NUM_WORDS = 256;
    bitlify_t words[NUM_WORDS] = {{ 0 }};
    set_bit_in_buf(&words[0], MAXBIT+1, 1);
    assertf(buf_element(&words[0], MAXBIT) == &words[0], "%p != %p, bits in bitlify_t: %lu", buf_element(&words[0], MAXBIT), &words[0], BITS_IN_BITLIFY_T);
    assertf(buf_element(&words[0], MAXBIT+1) == &words[1], "%p != %p, bits in bitlify_t: %lu", buf_element(&words[0], MAXBIT+1), &words[1], BITS_IN_BITLIFY_T);
    assert(words[0] == 0);
    assertf(words[1] == 1, "%u", words[1]);

    for (int w = 0; w < NUM_WORDS; w++)
        words[w] = 0;
    for (int i = 0; i < sizeof(words)*8; i++) {{
        set_bit_in_buf(&words[0], i, 1);
        assert(get_bit_from_buf(&words[0], i) == 1);

        int idx = i/(8*sizeof(bitlify_t));
        for (int w = 0; w < NUM_WORDS; w++) {{
            if (w == idx) {{
                assertf(words[w] == 1ULL << i%(sizeof(bitlify_t)*8), "i=%d, w=%d, words[w]=%x", i, w, words[w]);
            }} else {{
                assertf(words[w] == 0, "i=%d, w=%d, words[w]=%x", i, w, words[w]);
            }}
        }}

        set_bit_in_buf(&words[0], i, 0);
        assert(get_bit_from_buf(&words[0], i) == 0);
        for (int w = 0; w < NUM_WORDS; w++)
            assertf(words[w] == 0, "i=%d, w=%d, words[w]=%x", i, w, words[w]);
    }}

    struct data x = {{0}};
    struct data *y = &x;

    // Check individual bit sets
    assert(bits_in_counter_out == 64);
    for (int vm = 0; vm < MACHINES; vm++) {{
        assert(get_counter_out(y, vm) == 0);

        for (uint64_t value = 0; value < 16; value++) {{
            set_counter_out(y, vm, value);
            assertf(get_counter_out(y, vm) == value, "Should %lu, is %lu", value, get_counter_out(y, vm));
            loop_counter_out(y, vm, check_value, &value);
        }}
        set_counter_out(y, vm, 0);
        assert(get_counter_out(y, vm) == 0);
    }}

    // Check set all
    set_counter_out_all(y, 13);
    for (int vm = 0; vm < MACHINES; vm++) {{
        assertf(get_counter_out(y, vm) == 13, "Counter for vm %d should be 13 but is %lu", vm, get_counter_out(y, vm));
    }}
    set_counter_out_all(y, 0);
    for (int vm = 0; vm < MACHINES; vm++) {{
        assert(get_counter_out(y, vm) == 0);
    }}
    set_counter_out_zero(y);
    for (int vm = 0; vm < MACHINES; vm++) {{
        assert(get_counter_out(y, vm) == 0);
    }}

    // Check set/get from buffer
    unsigned char buf[COUNTER_LONG_LEN] = {{ 0 }};
    unsigned char rdbuf[COUNTER_LONG_LEN];
    buf[0] = 0x0c;
    assert(get_bit_from_buf((bitlify_t*)buf, 0) == 0);
    assert(get_bit_from_buf((bitlify_t*)buf, 1) == 0);
    assert(get_bit_from_buf((bitlify_t*)buf, 2) == 1);
    assert(get_bit_from_buf((bitlify_t*)buf, 3) == 1);

    //dump(y);
    for (int vm = 0; vm < MACHINES; vm++) {{
        for (int v = 0; v < 256; v++) {{
            for (int i = 0; i < COUNTER_LONG_LEN; i++) {{
                buf[i] = v + i; 
                rdbuf[i] = ~buf[i]; 
            }}
            /*printf("2rdbuf: ");
            for (int x = 0; x < COUNTER_LONG_LEN; x++)
                printf("%02x", rdbuf[x]);
            printf("\\n2buf  : ");
            for (int x = 0; x < COUNTER_LONG_LEN; x++)
                printf("%02x", buf[x]);
            printf("\\n");*/


            set_counter_out_long_buf(y, vm, buf);

            char* counter_out_long = (char*)y->counter_out_long_bit0_;
            for (int b = 0; b < COUNTER_LONG_LEN*8; b++) {{
                // Get bit from buf
                bool val = get_bit_from_buf((bitlify_t*)&buf[0], b);
                // Get bit from data struct
                int byteidx = INTLEN*b + vm / 8;
                int bitidx = vm % 8;
                bool val2 = (counter_out_long[byteidx] >> bitidx) & 1;
                assertf(val == val2, "vm=%d, b=%d, val=%d, val2=%d", vm, b, val, val2);
                // Get ground truth
                bool val3 = ((v+(b/8)) >> (b%8)) & 1;
                assertf(val == val3, "v=%d, b=%d, val=%d, val3=%d", v, b, val, val3);
            }}
            get_counter_out_long_buf(y, vm, &rdbuf[0]);
            for (int i = 0; i < COUNTER_LONG_LEN; i++) {{
               assertf(buf[i]==rdbuf[i] && rdbuf[i] == (unsigned char)(v+i), "vm=%d, v=%d, i=%d, buf[i]=%02x, rfbuf[i]=%02x", vm, v, i, buf[i], rdbuf[i]);
            }}
        }}
    }}

    // Check one bit fields
    assert(y->rst[0] == 0);
    set_rst(y, 12, 1);
    assert(y->rst[0] == 0);
    assert(y->rst[1] == 1<<4);
    assert(get_rst(y, 12) == 1);
    assert(get_rst(y, 11) == 0);
    set_rst_all(y, 0);
    assert(get_rst(y, 12) == 0);
    assert(get_rst(y, 11) == 0);
    set_rst_all(y, 1);
    assert(get_rst(y, 12) == 1);
    assert(get_rst(y, 11) == 1);
    set_rst_zero(y);
    assert(get_rst(y, 12) == 0);

    printf("All good!\\n");
    return 0;
}}

'''.format(hf, COUNTER_LEN, COUNTER_LONG_LEN, counter_str, counter_long_str, COUNTER_LEN, COUNTER_LONG_LEN));

        for intlen in (32, 64, 128, 256, 512):
            print("Testing length {}".format(intlen))
            bitlify.bitlify(*bitlify.get_opts(["-vv", "./bitlify_testin.v", vf, hf, "-w", str(intlen), "/OpenROAD-flow-public/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib"]))
            #cmd = "clang -O3 -fno-unroll-loops -g -DMACHINES={} {} -o {} -E".format(intlen, cf, pf);
            #self.assertEqual(os.system(cmd), 0, "Preprocessor failed, command was {}".format(cmd))
            cmd = "clang -O0 -g -DMACHINES={} {} -o {}".format(intlen, cf, of);
            result = os.system(cmd)
            self.assertEqual(result, 0, "Compilation failed, command was {}".format(cmd))


        # Run test
        retcode = os.system(of)

        self.assertEqual(retcode, 0, "Test program didn't return 0")
        return


    def test_equality(self):
        vf = tempfile.NamedTemporaryFile(suffix=".v")
        hf = tempfile.NamedTemporaryFile(suffix=".h")
        in_v = "./bitlify_testin.v"
        out_h = "./bitlify_testout.h"
        out_v = "./bitlify_testout.v"
        bitlify.bitlify(*bitlify.get_opts(["-vv", in_v, vf.name, hf.name, "/OpenROAD-flow-public/flow/platforms/sky130/lib/sky130_fd_sc_hs__tt_025C_1v80.lib"]))
        os.system("cp " + vf.name + " /tmp/bla")

        diff = os.system("diff -u " + out_h + " " + hf.name)
        self.assertEqual(0, diff, "Difference with expected output in .h output")

        diff = os.system("diff -u " + out_v + " " + vf.name)
        self.assertEqual(0, diff, "Difference with expected output in .v output")


        # Close/delete tempfiles
        vf.close()
        hf.close()
        return

if __name__ == '__main__':
    unittest.main()
