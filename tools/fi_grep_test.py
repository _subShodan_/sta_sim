#!/usr/bin/python3

import unittest
import os
import tempfile
import IPython
import numpy as np
import fi_grep
from collections import OrderedDict
from fi_grep import OutputFormat, GlitchParams, GlitchItem # For unpickling

class TestGrepper(unittest.TestCase):

    def test_counts(self):
        data = {}
        data['start'] = np.array([1000, 1000, 1002, 1002, 1003])
        data['code'] = np.array([
                        [True,  True,  True],
                        [False, False, True],
                        [True,  False, False],
                        [False, True,  True],
                        [True,  False, True]])
        maxnum = 10

        start_num, start_pins = fi_grep.common_start_pins(data, maxnum)
        #print(start_num, start_pins)
        self.assertEqual(OrderedDict([(1000, 2), (1002, 2), (1003, 1)]), start_num) # Check numbers
        self.assertEqual(start_num.keys(), start_pins.keys()) # Length an contents
        self.assertEqual(list(start_pins[1000]), [2, 1, 0])
        self.assertEqual(list(start_pins[1002]), [2, 1, 0])
        self.assertEqual(list(start_pins[1003]), [2, 0])

        maxnum = 2
        start_num, start_pins = fi_grep.common_start_pins(data, maxnum)
        self.assertEqual(OrderedDict([(1000, 2), (1002, 2)]), start_num) # Check numbers
        self.assertEqual(list(start_pins[1000]), [2, 1, 0])
        self.assertEqual(list(start_pins[1002]), [2, 1, 0])


        maxnum = 10
        pin_num, pin_starts = fi_grep.common_pins(data, maxnum)
        self.assertEqual(OrderedDict([(2, 4), (0, 3), (1, 2)]), pin_num)
        self.assertEqual(pin_num.keys(), pin_starts.keys())
        self.assertEqual(list(pin_starts[2]), [(1000, 2), (1002, 1), (1003, 1)])
        self.assertEqual(list(pin_starts[1]), [(1000, 1), (1002, 1)])
        self.assertEqual(list(pin_starts[0]), [(1000, 1), (1002, 1), (1003, 1)])
        #print(pin_num, pin_starts)

        minstart, maxstart, minpin, maxpin, hist = fi_grep.histdata(data)
        self.assertEqual(minstart, 1000)
        self.assertEqual(maxstart, 1003)
        self.assertEqual(minpin, 0)
        self.assertEqual(maxpin, 2)
        #print(hist)
        np.testing.assert_array_equal(hist, np.array([ 
            # 1000, 1001, 1002, 1003
            [    1,    0,    1,    1], # pin 0
            [    1,    0,    1,    0], # pin 1
            [    2,    0,    1,    1]])) # pin2

    def test_find_minimal(self):
        data = {}
        data['start'] = np.array([1000, 1000, 1002, 1002, 1003])
        data['code'] = np.array([
                        [True,  True,  True],
                        [False, False, True],
                        [True,  False, False],
                        [False, True,  True],
                        [True,  False, True]])
        (start, pin, should) = (1000, 0, 0)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1001, 0, None)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1002, 0, 2)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1003, 0, 4)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)

        (start, pin, should) = (1000, 1, 0)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1001, 1, None)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1002, 1, 3)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1003, 1, None)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)

        (start, pin, should) = (1000, 2, 1)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1001, 2, None)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1002, 2, 3)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)
        (start, pin, should) = (1003, 2, 4)
        self.assertEqual(fi_grep.find_minimal(pin, start, data), should)

    def test_pinfile(self):
        wf = tempfile.NamedTemporaryFile(suffix=".txt")

        options = type('test', (object,), {})() # Create empty object
        options.verbose = 100
        options.testresult = []
        options.profile = False
        options.figure = None
        options.reproduce = None
        options.glitchmap = "fi_grep_test.map"
        options.topn = 90
        options.write = wf.name
        options.writeoutputname = None
        options.csv = None
        options.reproduce_all = None
        options.reproduce_run = None

        fi_grep.fi_grep("fi_grep_test.res", options)
        os.system("cp {} /tmp/bla2.txt".format(wf.name))

        cmp_f = "fi_grep_test_pins.txt"
        diff = os.system("diff -u " + cmp_f + " " + wf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_pinfile_short(self):
        wf = tempfile.NamedTemporaryFile(suffix=".txt")

        options = type('test', (object,), {})() # Create empty object
        options.verbose = 100
        options.testresult = []
        options.profile = False
        options.figure = None
        options.reproduce = None
        options.glitchmap = "fi_grep_test.map"
        options.topn = 90
        options.write = None
        options.writeoutputname = wf.name
        options.csv = None
        options.reproduce_all = None
        options.reproduce_run = None

        fi_grep.fi_grep("fi_grep_test.res", options)
        os.system("cp {} /tmp/bla.txt".format(wf.name))

        cmp_f = "fi_grep_test_pins_short.txt"
        diff = os.system("diff -u " + cmp_f + " " + wf.name)
        self.assertEqual(0, diff, "Difference with expected output")
        
    def test_csv(self):
        wf = tempfile.NamedTemporaryFile(suffix=".csv")

        options = type('test', (object,), {})() # Create empty object
        options.verbose = 100
        options.testresult = []
        options.profile = False
        options.figure = None
        options.reproduce = None
        options.glitchmap = "fi_grep_test.map"
        options.topn = 90
        options.write = None
        options.writeoutputname = None
        options.csv = wf.name
        options.reproduce_all = None
        options.reproduce_run = None

        fi_grep.fi_grep("fi_grep_test.res", options)
        os.system("cp {} /tmp/bla.txt".format(wf.name))

        cmp_f = "fi_grep_test_csv.txt"
        diff = os.system("diff -u " + cmp_f + " " + wf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_repro(self):
        wf = tempfile.NamedTemporaryFile(suffix=".repro")

        options = type('test', (object,), {})() # Create empty object
        options.verbose = 100
        options.testresult = []
        options.profile = False
        options.figure = None
        options.reproduce = wf.name
        options.glitchmap = "fi_grep_test.map"
        options.topn = 90
        options.write = None
        options.writeoutputname = None
        options.csv = None
        options.reproduce_all = None
        options.reproduce_run = None

        fi_grep.fi_grep("fi_grep_test.res", options)
        #os.system("cp {} /tmp/bla.repro".format(wf.name))

        cmp_f = "fi_grep_test_repro.repro"
        diff = os.system("diff -u " + cmp_f + " " + wf.name)
        self.assertEqual(0, diff, "Difference with expected output")

        # Test all
        wd = tempfile.TemporaryDirectory()
        options.reproduce = None
        options.reproduce_all = wd.name
        options.reproduce_run = "echo"

        fi_grep.fi_grep("fi_grep_test.res", options)
        #os.system("cp -r {} /tmp/xxx".format(wd.name))

        for idx in range(0, 386):
            txtname = f"{wd.name}/{idx}.repro.txt"
            # We can check the contents here
            with open(txtname, "r") as txtf:
                line = txtf.read().strip()
                self.assertTrue(line.startswith('-f ') and line.endswith(f'{idx}.repro'), f"{txtname} doesn't have the right contents: {line}")

            # For this one the contents vary, just check filesize
            reproname = f"{wd.name}/{idx}.repro"
            self.assertEqual(os.path.getsize(reproname), 240, "Incorrect length of repro file")

if __name__ == '__main__':
    unittest.main()


