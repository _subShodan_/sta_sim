module sky130_fd_sc_hs__a2111o_1 (
  input [63:0] A1,
  input [63:0] A2,
  input [63:0] B1,
  input [63:0] C1,
  input [63:0] D1,
  output [63:0] X
);
  assign X = (A1&A2) | (B1) | (C1) | (D1);
endmodule
module sky130_fd_sc_hs__a221oi_1 (
  input [63:0] A1,
  input [63:0] A2,
  input [63:0] B1,
  input [63:0] B2,
  input [63:0] C1,
  output [63:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A2&~B2&~C1);
endmodule
module sky130_fd_sc_hs__bufinv_16 (
  input [63:0] A,
  output [63:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__conb_1 (
  output [63:0] HI,
  output [63:0] LO
);
  assign HI = 64'b1111111111111111111111111111111111111111111111111111111111111111;
  assign LO = 64'b0000000000000000000000000000000000000000000000000000000000000000;
endmodule
module sky130_fd_sc_hs__dfrtn_1 (
  input [0:0] CLK_N,
  input [63:0] D,
  output reg [63:0] Q,
  input [63:0] RESET_B
);
  always @(negedge CLK_N) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfrtp_1 (
  input [0:0] CLK,
  input [63:0] D,
  output reg [63:0] Q,
  input [63:0] RESET_B
);
  always @(posedge CLK) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfstp_1 (
  input [0:0] CLK,
  input [63:0] D,
  output reg [63:0] Q,
  input [63:0] SET_B
);
  always @(posedge CLK) begin
    Q <= D | (~SET_B);
  end
endmodule
module sky130_fd_sc_hs__dfxtp_1 (
  input [0:0] CLK,
  input [63:0] D,
  output reg [63:0] Q
);
  always @(posedge CLK) begin
    Q <= D;
  end
endmodule
