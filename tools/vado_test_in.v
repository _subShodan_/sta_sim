// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Thu Feb 18 13:55:12 2021
// Host        : uslp014 running 64-bit major release  (build 9200)
// Command     : write_verilog -force bla.v
// Design      : test_tb
// Purpose     : This is a Verilog netlist of the current design or from a specific cell of the design. The output is an
//               IEEE 1364-2001 compliant Verilog HDL file that contains netlist information obtained from the input
//               design files.
// Device      : xc7a12ticsg325-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* XLNX_LINE_COL = "459520" *) 
module add
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2126" *) 
  RTL_ADD O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module bmerge
   (DATA,
    I,
    O,
    S);
  input [7:0]DATA;
  input I;
  output [7:0]O;
  input [2:0]S;

  wire [7:0]DATA;
  wire I;
  wire [7:0]O;
  wire [2:0]S;

  (* XLNX_LINE_COL = "524544" *) 
  (* map_to_module = "2134" *) 
  RTL_BMERGE O0_i
       (.DATA(DATA),
        .I(I),
        .O(O),
        .S(S));
endmodule

(* XLNX_LINE_COL = "459776" *) 
module bsel
   (I,
    S,
    O);
  input [3:0]I;
  input [1:0]S;
  output O;

  wire [3:0]I;
  wire O;
  wire [1:0]S;

  (* XLNX_LINE_COL = "983296" *) 
  (* map_to_module = "2141" *) 
  RTL_BSEL O_i
       (.I(I),
        .O(O),
        .S(S));
endmodule

(* XLNX_LINE_COL = "460544" *) 
module d_latch
   (D,
    G,
    Q);
  input [7:0]D;
  input [7:0]G;
  output [7:0]Q;

  wire [7:0]D;
  wire [7:0]G;
  wire [7:0]Q;

  (* XLNX_LINE_COL = "786688" *) 
  (* map_to_module = "2139" *) 
  RTL_LATCH Q_reg
       (.D(D),
        .G(G),
        .Q(Q));
endmodule

(* XLNX_LINE_COL = "459264" *) 
module eq
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire O;

  (* XLNX_LINE_COL = "1180160" *) 
  (* map_to_module = "2129" *) 
  RTL_EQ O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "462592" *) 
module ff_C_CE_CLR_D_Q
   (C,
    CE,
    CLR,
    D,
    Q);
  input C;
  input CE;
  input CLR;
  input D;
  output Q;

  wire C;
  wire CE;
  wire CLR;
  wire D;
  wire Q;

  (* PRIMITIVE_NAME = "RTL_REG" *) 
  (* XLNX_LINE_COL = "786688" *) 
  (* map_to_module = "2145" *) 
  RTL_REG_ASYNC__BREG_4 Q_reg
       (.C(C),
        .CE(CE),
        .CLR(CLR),
        .D(D),
        .Q(Q));
endmodule

(* XLNX_LINE_COL = "461568" *) 
module ff_C_CE_D_Q
   (C,
    CE,
    D,
    Q);
  input C;
  input CE;
  input D;
  output Q;

  wire C;
  wire CE;
  wire D;
  wire Q;

  (* PRIMITIVE_NAME = "RTL_REG" *) 
  (* XLNX_LINE_COL = "1048832" *) 
  (* map_to_module = "2142" *) 
  RTL_REG__BREG_1 Q_reg
       (.C(C),
        .CE(CE),
        .D(D),
        .Q(Q));
endmodule

(* XLNX_LINE_COL = "462592" *) 
module ff_C_CE_PRE_D_Q
   (C,
    CE,
    PRE,
    D,
    Q);
  input C;
  input CE;
  input PRE;
  input D;
  output Q;

  wire C;
  wire CE;
  wire D;
  wire PRE;
  wire Q;

  (* PRIMITIVE_NAME = "RTL_REG" *) 
  (* XLNX_LINE_COL = "786688" *) 
  (* map_to_module = "2146" *) 
  RTL_REG_ASYNC__BREG_5 Q_reg
       (.C(C),
        .CE(CE),
        .D(D),
        .PRE(PRE),
        .Q(Q));
endmodule

(* XLNX_LINE_COL = "461824" *) 
module ff_C_D_Q_RST
   (C,
    D,
    Q,
    RST);
  input C;
  input D;
  output Q;
  input RST;

  wire C;
  wire D;
  wire Q;
  wire RST;

  (* PRIMITIVE_NAME = "RTL_REG" *) 
  (* XLNX_LINE_COL = "1048832" *) 
  (* map_to_module = "2143" *) 
  RTL_REG_SYNC__BREG_2 Q_reg
       (.C(C),
        .D(D),
        .Q(Q),
        .RST(RST));
endmodule

(* XLNX_LINE_COL = "461824" *) 
module ff_C_D_Q_SET
   (C,
    D,
    Q,
    SET);
  input C;
  input D;
  output Q;
  input SET;

  wire C;
  wire D;
  wire Q;
  wire SET;

  (* PRIMITIVE_NAME = "RTL_REG" *) 
  (* XLNX_LINE_COL = "1048832" *) 
  (* map_to_module = "2144" *) 
  RTL_REG_SYNC__BREG_3 Q_reg
       (.C(C),
        .D(D),
        .Q(Q),
        .SET(SET));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module lshift
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire \<const1> ;
  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "983296" *) 
  (* map_to_module = "2124" *) 
  RTL_LSHIFT O_i
       (.I0(I0),
        .I1(I1),
        .I2(\<const1> ),
        .O(O));
  VCC VCC
       (.P(\<const1> ));
endmodule

(* XLNX_LINE_COL = "459776" *) 
module mand
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2130" *) 
  RTL_AND O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module mor
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2132" *) 
  RTL_OR O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module mul
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2133" *) 
  RTL_MULT O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module mux
   (I0,
    I1,
    O,
    S);
  input I0;
  input I1;
  output O;
  input S;

  wire I0;
  wire I1;
  wire O;
  wire S;

  (* SEL_VAL = "I0:S=1'b0,I1:S=1'b1" *) 
  (* XLNX_LINE_COL = "525312" *) 
  (* map_to_module = "2140" *) 
  RTL_MUX O_i
       (.I0(I0),
        .I1(I1),
        .O(O),
        .S(S));
endmodule

(* XLNX_LINE_COL = "459776" *) 
module mxor
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2131" *) 
  RTL_XOR O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module neq
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire O;

  (* XLNX_LINE_COL = "1180160" *) 
  (* map_to_module = "2128" *) 
  RTL_NEQ O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module redand
   (I0,
    O);
  input [7:0]I0;
  output O;

  wire [7:0]I0;
  wire O;

  (* XLNX_LINE_COL = "983296" *) 
  (* map_to_module = "2135" *) 
  RTL_REDUCTION_AND O_i
       (.I0(I0),
        .O(O));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module rednor
   (I0,
    O);
  input [7:0]I0;
  output O;

  wire [7:0]I0;
  wire O;

  (* XLNX_LINE_COL = "983552" *) 
  (* map_to_module = "2138" *) 
  RTL_REDUCTION_NOR O_i
       (.I0(I0),
        .O(O));
endmodule

(* XLNX_LINE_COL = "460032" *) 
module redor
   (I0,
    O);
  input [7:0]I0;
  output O;

  wire [7:0]I0;
  wire O;

  (* XLNX_LINE_COL = "983296" *) 
  (* map_to_module = "2137" *) 
  RTL_REDUCTION_OR O_i
       (.I0(I0),
        .O(O));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module redxor
   (I0,
    O);
  input [7:0]I0;
  output O;

  wire [7:0]I0;
  wire O;

  (* XLNX_LINE_COL = "983296" *) 
  (* map_to_module = "2136" *) 
  RTL_REDUCTION_XOR O_i
       (.I0(I0),
        .O(O));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module rom
   (A,
    O);
  input [5:0]A;
  output [0:0]O;

  wire \<const0> ;
  wire [5:0]A;
  wire [0:0]O;
  wire O_i__0_n_0;
  wire O_i__1_n_0;
  wire O_i__2_n_0;
  wire O_i__3_n_0;
  wire O_i__4_n_0;
  wire O_i_n_0;

  GND GND
       (.G(\<const0> ));
  (* INIT_VAL = "INIT_DEFAULT:1'b0,INIT_14:1'b1" *) 
  (* XLNX_LINE_COL = "10224128" *) 
  (* map_to_module = "2153" *) 
  RTL_ROM O_i
       (.A(A),
        .O(O_i_n_0));
  (* SEL_VAL = "I0:S=6'b110111,I1:S=default" *) 
  (* XLNX_LINE_COL = "8520192" *) 
  (* map_to_module = "2152" *) 
  RTL_MUX5 O_i__0
       (.I0(\<const0> ),
        .I1(O_i_n_0),
        .O(O_i__0_n_0),
        .S(A));
  (* SEL_VAL = "I0:S=6'b011011,I1:S=default" *) 
  (* XLNX_LINE_COL = "6816256" *) 
  (* map_to_module = "2151" *) 
  RTL_MUX5 O_i__1
       (.I0(\<const0> ),
        .I1(O_i__0_n_0),
        .O(O_i__1_n_0),
        .S(A));
  (* SEL_VAL = "I0:S=6'b100010,I1:S=default" *) 
  (* XLNX_LINE_COL = "5112320" *) 
  (* map_to_module = "2150" *) 
  RTL_MUX5 O_i__2
       (.I0(\<const0> ),
        .I1(O_i__1_n_0),
        .O(O_i__2_n_0),
        .S(A));
  (* SEL_VAL = "I0:S=6'b010000,I1:S=default" *) 
  (* XLNX_LINE_COL = "3408384" *) 
  (* map_to_module = "2149" *) 
  RTL_MUX5 O_i__3
       (.I0(\<const0> ),
        .I1(O_i__2_n_0),
        .O(O_i__3_n_0),
        .S(A));
  (* SEL_VAL = "I0:S=6'b101001,I1:S=default" *) 
  (* XLNX_LINE_COL = "1704448" *) 
  (* map_to_module = "2148" *) 
  RTL_MUX5 O_i__4
       (.I0(\<const0> ),
        .I1(O_i__3_n_0),
        .O(O_i__4_n_0),
        .S(A));
  (* SEL_VAL = "I0:S=6'b111100,I1:S=default" *) 
  (* XLNX_LINE_COL = "512" *) 
  (* map_to_module = "2147" *) 
  RTL_MUX5 O_i__5
       (.I0(\<const0> ),
        .I1(O_i__4_n_0),
        .O(O),
        .S(A));
endmodule

(* XLNX_LINE_COL = "460288" *) 
module rshift
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire \<const1> ;
  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "918016" *) 
  (* map_to_module = "2125" *) 
  RTL_RSHIFT O_i
       (.I0(I0),
        .I1(I1),
        .I2(\<const1> ),
        .O(O));
  VCC VCC
       (.P(\<const1> ));
endmodule

(* XLNX_LINE_COL = "459520" *) 
module sub
   (I0,
    I1,
    O);
  input [7:0]I0;
  input [7:0]I1;
  output [7:0]O;

  wire [7:0]I0;
  wire [7:0]I1;
  wire [7:0]O;

  (* XLNX_LINE_COL = "1179904" *) 
  (* map_to_module = "2127" *) 
  RTL_SUB O_i
       (.I0(I0),
        .I1(I1),
        .O(O));
endmodule
