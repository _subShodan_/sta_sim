#!/usr/bin/python3

import tempfile
import sys
import pyverilog.vparser.ast as vast
import pyverilog.vparser.parser as vparser
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
from optparse import OptionParser

import IPython

def get_opts(cmdline):
    USAGE_STRING = "Usage: vado.py netlist_input vivado_input netlist_output\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", dest="define", action="append", default=[], help="Macro Definition")
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 3:
        print("Incorrect number of parameters, is {}, should be 2".format(args))
        print(USAGE_STRING)
        sys.exit(1)

    netlistfile = args[0]
    vivadoin = args[1]
    outputfile = args[2]

    return (netlistfile, vivadoin, outputfile, options)

def calc_width(signal, widths):
    if isinstance(signal, vast.Concat):
        # {x,y,z}
        width = sum([calc_width(element, widths) for element in signal.list])
    elif isinstance(signal, vast.Identifier):
        # x
        width = widths[signal.name]
    elif isinstance(signal, vast.Partselect):
        # x[msb:lsb]
        assert isinstance(signal.var, vast.Identifier)
        width = int(signal.msb.value) - int(signal.lsb.value) + 1
        assert width > 0
    elif isinstance(signal, vast.Pointer):
        # x[num]
        width = 1
    elif isinstance(signal, vast.Unot):
        width = calc_width(signal.right, widths)
    else:
        assert False, "Found {} of type {}".format(signal, type(signal))
    return width

def get_port_width(instance, widths):
    portwidth = dict()
    for port in instance.portlist:
        portwidth[port.portname] = calc_width(port.argname, widths)
    return portwidth


def add_mux(instance, pragmas, widths, options):
    assert len(pragmas) == 1, "Pragmas: {}".format(pragmas)
    # Turn the string "I0:S=1'b1,I1:S=1'b0" into a dict
    assert len(pragmas['SEL_VAL']) == 1
    inputs = str(pragmas['SEL_VAL'][0]).split(',') 
    inputs = [i_s.split(':') for i_s in inputs]
    inputs = {i: s for (i, s) in inputs}

    if options.verbose >= 2:
        print("{} found {} with SEL_VAL={}".format(instance.module, instance.name, inputs))

    # Get widths
    portwidth = get_port_width(instance, widths)

    # Create switch statement
    # Goal: assign O = (S==<val> ? Ix : (S == val2> ? Iy : default))
    switch = ["({} ? {} : ".format(s.replace("=", "=="),i) for (i, s) in inputs.items() if not s.endswith("default")]
    default = ["{}".format(i) for (i, s) in inputs.items() if s.endswith("default")]
    if len(default) == 0:
        default = ["{}'b0".format(portwidth['O'])] # Set as default 0
    assert len(default) < 2 # 0 or 1
    switch.extend(default)
    switch.extend(")"*(len(switch)-1))
    switch = "".join(switch)

    # Switch inputs
    switchinputs = ", ".join(["input [{}:0] {}".format(portwidth[k]-1, k) for k in portwidth.keys() if k.startswith('S')])

    # Generate output
    inputstr = ", ".join(["input [{}:0] {}".format(portwidth[k]-1, k) for k in inputs.keys()])
    mux = """
module {}({}, {}, output [{}:0] O);
    assign O = {};
endmodule
""".format(instance.module, inputstr, switchinputs, portwidth['O']-1, switch);

    return mux

def add_rom(instance, pragmas, widths, options):
    assert len(pragmas) == 1, "Pragmas: {}".format(pragmas)
    # Turn the string "I0:S=1'b1,I1:S=1'b0" into a dict
    assert len(pragmas['INIT_VAL']) == 1
    inputs = str(pragmas['INIT_VAL'][0]).split(',') 
    inputs = [i_s.split(':') for i_s in inputs]
    inputs = {i: s for (i, s) in inputs}

    if options.verbose >= 2:
        print("{} found {} with INIT_VAL={}".format(instance.module, instance.name, inputs))

    # Get widths
    portwidth = get_port_width(instance, widths)

    # Goal: assign O = (S==<val> ? Ix : (S == val2> ? Iy : default))
    switch = ["(A == {}'d{} ? {} : ".format(portwidth['A'], i.replace("INIT_", ""), s) for (i, s) in inputs.items() if not i.endswith("DEFAULT")]
    default = ["{}".format(i) for (i, s) in inputs.items() if s.endswith("DEFAULT")]
    if len(default) == 0:
        default = ["{}'b0".format(portwidth['O'])] # Set as default 0
    assert len(default) < 2 # 0 or 1
    switch.extend(default)
    switch.extend(")"*(len(switch)-1))
    switch = "".join(switch)

    # Generate output
    portstr = ", ".join([("output [{}:0] "+p if p=='O' else "input [{}:0] "+p).format(w-1) for (p, w) in portwidth.items()])

    rom = """
module {}({});
    assign O = {};
endmodule
""".format(instance.module, portstr, switch);

    return rom

def add_bmerge(instance, pragmas, widths, options):
    assert len(pragmas) == 0, "Pragmas: {}".format(pragmas)

    if options.verbose >= 2:
        print("{} found {}".format(instance.module, instance.name))

    # Get widths
    portwidth = get_port_width(instance, widths)

    # Generate output
    portstr = ", ".join([("output [{}:0] "+p if p=='O' else "input [{}:0] "+p).format(w-1) for (p, w) in portwidth.items()])

    bmerge = """
module {}({});
    wire [{}:0] mask, Iw;
    assign mask = {}'b{} << S;
    assign Iw = I << S;
    assign O = (DATA & ~mask) | Iw;
endmodule
""".format(instance.module, portstr, portwidth['O']-1, portwidth['O'], "1"*portwidth['I']);

    return bmerge

def add_bsel(instance, pragmas, widths, options):
    assert len(pragmas) == 0, "Pragmas: {}".format(pragmas)

    if options.verbose >= 2:
        print("{} found {}".format(instance.module, instance.name))

    # Get widths
    portwidth = get_port_width(instance, widths)

    # Generate output
    portstr = ", ".join([("output [{}:0] "+p if p=='O' else "input [{}:0] "+p).format(w-1) for (p, w) in portwidth.items()])

    bsel = """
module {}({});
    assign O[0+:{}] = I[S+:{}];
endmodule
""".format(instance.module, portstr, portwidth['O'], portwidth['O']);
    
    return bsel


def add_reg(instance, pragmas, widths, options):
    assert len(pragmas) == 0, "Pragmas: {}".format(pragmas)
    defaults = {"CE": "1'b1", "RST": "1'b0", "SET": "1'b0", "PRE": "1'b0", "CLR": "1'b0"};

    # Get widths
    portwidth = get_port_width(instance, widths)
    assert sum(portwidth.values()) == len(portwidth), "Only support width 1 ports for register, instead: {}".format(portwidth)

    if options.verbose >= 2:
        print("{} found {} with ports {}".format(instance.module, instance.name, portwidth.keys()))
        
    # Assumption checks
    assert 'D' in portwidth
    assert 'Q' in portwidth
    assert 'C' in portwidth
    assert len(set(portwidth.keys()).difference(set(defaults.keys()))) == 3, "Ports: {}".format(portwidth.keys()) # D and Q left after taking out the stuff we know of
    # D = input
    # Q = output
    # CE = clock enable => if high, clock in input, if low, keep output the same
    # C = clock => on edge, set q=d
    # Async PRE/CLR?
    # PRE=preset => if high, set to 1
    # CLR=clear => if high, set to 0
    # RST / SET => sync versions of PRE/CLR

    portstr = ", ".join([("output reg [{}:0] "+p if p=='Q' else "input [{}:0] "+p).format(w-1) for (p, w) in portwidth.items()])

    defaultstr = "    wire " + ", ".join([d+"x" for d in defaults.keys()]) + ";\n";
    for d, v in defaults.items():
        defaultstr = defaultstr + "    assign {}x = {};\n".format(d, d if d in portwidth else v)

    # NOTE: Only supports 1 bit wide signals
    reg = """
module {}({});
{}
    always @(posedge C or posedge CLRx or posedge PREx)
    begin
        if (CLRx)
            Q <= 1'b0;
        else if (PREx)
            Q <= 1'b1;
        else if (CEx)
            if (RSTx)
                Q <= 1'b0;
            else if (SETx)
                Q <= 1'b1;
            else
                Q <= D;
    end
endmodule
""".format(instance.module, portstr, defaultstr)

    return reg

def add_latch(instance, pragmas, widths, options):
    assert len(pragmas) == 0, "Pragmas: {}".format(pragmas)

    # Get widths
    portwidth = get_port_width(instance, widths)

    if options.verbose >= 2:
        print("{} found {} with ports {}".format(instance.module, instance.name, portwidth.keys()))
        
    # Assumption checks
    assert 'D' in portwidth
    assert 'Q' in portwidth
    assert 'G' in portwidth
    assert len(portwidth) == 3, "Ports: {}".format(portwidth.keys()) 

    latch = """
module {}(input [{}:0] D, input [{}:0] G, output reg [{}:0] Q);
    genvar i;
    for (i = 0; i <= {}; i = i + 1) begin
        always @ (G[i] or D[i])  
            if (G[i])  
                Q[i] <= D[i];  
        end
endmodule  
""".format(instance.module, portwidth['Q']-1, portwidth['Q']-1, portwidth['Q']-1, portwidth['Q']-1) # Note we're taking all Q here, and let verilog parser deal with different widths for D/G

    return latch 

def add_generic(instance, module, widths, options):
    if options.verbose >= 2:
        print("Matched module {} for {}".format(module.name, instance.name))
    
    # Get ports
    portwidth = get_port_width(instance, widths)

    # Patch widths in module
    for port in module.portlist.ports:
        first = port.first
        if first.name not in portwidth or portwidth[first.name] == 1: # Sometimes, Vivado doesn't connect all inputs, in which case the port in the instance has no width, but it exists in the module. Assume width = one wire in that case.
            first.width = None
            if first.name not in portwidth:
                print("Warning: instance {} on line {} does not have port {} connected. Creating module with width 1 port.".format(instance.name, instance.lineno, first.name))
        else:
            first.width = vast.Width(msb=vast.IntConst(portwidth[first.name]-1), lsb=vast.IntConst(0))

    # Patch name
    module.name = instance.module

    # Gen code
    codegen = ASTCodeGenerator()
    rslt = codegen.visit(module)

    if instance.module.startswith("RTL_BMERGE"):
        print("Warning: workaround enabled for RTL_BMERGE bug in PyVerilog codegen")
        rslt = rslt.replace("O[S:S+1]", "O[S+:1]");

    return rslt

def parse_vivado_modules(vivadoin, options):
    ast = vparser.VerilogCodeParser((vivadoin, ), preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    modules = {}
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue
        modules[definition.name] = definition

    return modules
        
def add_module(modules, name, new_module, options):
    if name in modules:
        assert new_module == modules[name], "Oh oh, generated a new modules with different content. \n=== Old ===\n{}\n=== New ===\n{}".format(modules[name], new_module)
        # Do nothing if they are the same
    else:
        modules[name] = new_module
        if options.verbose >= 3:
            print("New module: {}".format(new_module))

def vado(netlistfile, vivadoin, outputfile, options):
    if options.verbose >= 1:
        print("parsing Vivado modules...")

    vado_modules = parse_vivado_modules(vivadoin, options)

    if options.verbose >= 2:
        print("Found modules: {}".format(vado_modules.keys()))

    if options.verbose >= 1:
        print("parsing code...")

    ast = vparser.VerilogCodeParser((netlistfile, ), preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    if options.verbose >= 1:
        print("Processing...")

    pragmas = {}
    modules_needed = set()
    modules_found = set()
    modules_new = dict()
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue
        modules_found.add(definition.name) # Register module 
        widths = dict() # Reinitialize for each new module

        for item in definition.items:
            if isinstance(item, vast.InstanceList):
                assert len(item.instances) == 1, "Assuming only 1 instance, but there are {}".format(len(item.instances))
                instance = item.instances[0]

                if 'INV' in pragmas:
                    # XXX this is purely a guess, don't know how to make vivado make a small test case. It seems to work correct for OTAES rst_ni signal which is active low
                    # XXX one bug is that not all rst_ni signals in OTAES are prefixed with the INV pragma. Not sure how to handle this yet. Requires manual fixing now
                    assert len(pragmas['INV']) == 1
                    invnames = pragmas['INV'][0].value.split(',')

                    # Invert the input
                    inverted = 0
                    for port in instance.portlist:
                        if port.portname in invnames:
                            port.argname = vast.Unot(right=port.argname)
                            inverted = inverted + 1
                    assert inverted == len(invnames), "Couldn't find {} in {}".format(invnames, [p.portname for p in instance.portlist])

                    del pragmas['INV'] # no longer needed

                if instance.module.startswith('RTL_MUX'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions XXX it would be better to have a hash to avoid dups
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_mux(instance, pragmas, widths, options), options)
                elif instance.module.startswith('RTL_BSEL'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions XXX it would be better to have a hash to avoid dups
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_bsel(instance, pragmas, widths, options), options)
                elif instance.module.startswith('RTL_BMERGE'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions XXX it would be better to have a hash to avoid dups
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_bmerge(instance, pragmas, widths, options), options)
                elif instance.module.startswith('RTL_REG'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_reg(instance, pragmas, widths, options), options)
                elif instance.module.startswith('RTL_LATCH'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_latch(instance, pragmas, widths, options), options)
                elif instance.module.startswith('RTL_ROM'):
                    instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions
                    item.module = instance.module
                    add_module(modules_new, instance.module, add_rom(instance, pragmas, widths, options), options)
                else:
                    # Match e.g. RTL_XORnn from vado modules
                    for m in vado_modules.keys():
                        if instance.module.startswith(m): 
                            instance.module = instance.module + "_" + str(len(modules_new)) # Make unique name to avoid collisions
                            item.module = instance.module
                            add_module(modules_new, instance.module, add_generic(instance, vado_modules[m], widths, options), options)

                # Add any renamed modules to needed list
                modules_needed.add(instance.module)

                # Zap previous pragmas
                pragmas = {}
            elif isinstance(item, vast.Pragma):
                entry = item.entry
                if entry.name not in ['KEEP', 'PRIMITIVE_NAME', 'XLNX_LINE_COL', 'map_to_module']: # XXX don't know what KEEP does
                    if entry.name not in pragmas:
                        pragmas[entry.name] = []
                    pragmas[entry.name].append(entry.value)
            elif isinstance(item, vast.Decl):
                # Check preliminaries
                assert len(item.list) == 1
                io = item.list[0]
                assert isinstance(io, vast.Input) or isinstance(io, vast.Output) or isinstance(io, vast.Wire), "Is {} not Input".format(type(io))

                # Get info
                if io.width is None:
                    width = 1
                else:
                    width = int(io.width.msb.value) - int(io.width.lsb.value) + 1
                name = io.name

                # Store it
                widths[name] = width
                if options.verbose >= 2:
                    print("Signal {} has width {}".format(name, width))

                # Zap previous pragmas
                pragmas = {}
            else:
                # Zap previous pragmas
                pragmas = {}

    # Check modules are all defined
    missing = modules_needed.difference(modules_new.keys()).difference(modules_found)
    assert len(missing) == 0, "Modules not defined: {}".format(missing)

    if options.verbose >= 1:
        print("generating code...")

    rslt_file = open(outputfile, "w")
    # Write input netlist
    codegen = ASTCodeGenerator()
    rslt = codegen.visit(ast)
    rslt_file.write(rslt)
    # Write our new modules
    for (name,module) in modules_new.items():
        rslt_file.write(module)
    rslt_file.close()

if __name__ == '__main__':
    vado(*get_opts(sys.argv[1:]))
