

module picorv32
(
  clk,
  glitches
);

  input [1612:0] glitches;
  input clk;

  sky130_fd_sc_hs__dfxtp_4
  _14988_
  (
    .D(_00909_ ^ glitches[0]),
    .Q(\cpuregs[20][0] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14989_
  (
    .D(_00920_ ^ glitches[1]),
    .Q(\cpuregs[20][1] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14990_
  (
    .D(_00931_ ^ glitches[2]),
    .Q(\cpuregs[20][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14991_
  (
    .D(_00934_ ^ glitches[3]),
    .Q(\cpuregs[20][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14992_
  (
    .D(_00935_ ^ glitches[4]),
    .Q(\cpuregs[20][4] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14993_
  (
    .D(_00936_ ^ glitches[5]),
    .Q(\cpuregs[20][5] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14994_
  (
    .D(_00937_ ^ glitches[6]),
    .Q(\cpuregs[20][6] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14995_
  (
    .D(_00938_ ^ glitches[7]),
    .Q(\cpuregs[20][7] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14996_
  (
    .D(_00939_ ^ glitches[8]),
    .Q(\cpuregs[20][8] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14997_
  (
    .D(_00940_ ^ glitches[9]),
    .Q(\cpuregs[20][9] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14998_
  (
    .D(_00910_ ^ glitches[10]),
    .Q(\cpuregs[20][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _14999_
  (
    .D(_00911_ ^ glitches[11]),
    .Q(\cpuregs[20][11] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15000_
  (
    .D(_00912_ ^ glitches[12]),
    .Q(\cpuregs[20][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15001_
  (
    .D(_00913_ ^ glitches[13]),
    .Q(\cpuregs[20][13] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15002_
  (
    .D(_00914_ ^ glitches[14]),
    .Q(\cpuregs[20][14] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15003_
  (
    .D(_00915_ ^ glitches[15]),
    .Q(\cpuregs[20][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15004_
  (
    .D(_00916_ ^ glitches[16]),
    .Q(\cpuregs[20][16] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15005_
  (
    .D(_00917_ ^ glitches[17]),
    .Q(\cpuregs[20][17] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15006_
  (
    .D(_00918_ ^ glitches[18]),
    .Q(\cpuregs[20][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15007_
  (
    .D(_00919_ ^ glitches[19]),
    .Q(\cpuregs[20][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15008_
  (
    .D(_00921_ ^ glitches[20]),
    .Q(\cpuregs[20][20] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15009_
  (
    .D(_00922_ ^ glitches[21]),
    .Q(\cpuregs[20][21] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15010_
  (
    .D(_00923_ ^ glitches[22]),
    .Q(\cpuregs[20][22] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15011_
  (
    .D(_00924_ ^ glitches[23]),
    .Q(\cpuregs[20][23] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15012_
  (
    .D(_00925_ ^ glitches[24]),
    .Q(\cpuregs[20][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15013_
  (
    .D(_00926_ ^ glitches[25]),
    .Q(\cpuregs[20][25] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15014_
  (
    .D(_00927_ ^ glitches[26]),
    .Q(\cpuregs[20][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15015_
  (
    .D(_00928_ ^ glitches[27]),
    .Q(\cpuregs[20][27] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15016_
  (
    .D(_00929_ ^ glitches[28]),
    .Q(\cpuregs[20][28] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15017_
  (
    .D(_00930_ ^ glitches[29]),
    .Q(\cpuregs[20][29] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15018_
  (
    .D(_00932_ ^ glitches[30]),
    .Q(\cpuregs[20][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15019_
  (
    .D(_00933_ ^ glitches[31]),
    .Q(\cpuregs[20][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15020_
  (
    .D(_00877_ ^ glitches[32]),
    .Q(\cpuregs[1][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15021_
  (
    .D(_00888_ ^ glitches[33]),
    .Q(\cpuregs[1][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15022_
  (
    .D(_00899_ ^ glitches[34]),
    .Q(\cpuregs[1][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15023_
  (
    .D(_00902_ ^ glitches[35]),
    .Q(\cpuregs[1][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15024_
  (
    .D(_00903_ ^ glitches[36]),
    .Q(\cpuregs[1][4] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15025_
  (
    .D(_00904_ ^ glitches[37]),
    .Q(\cpuregs[1][5] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15026_
  (
    .D(_00905_ ^ glitches[38]),
    .Q(\cpuregs[1][6] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15027_
  (
    .D(_00906_ ^ glitches[39]),
    .Q(\cpuregs[1][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15028_
  (
    .D(_00907_ ^ glitches[40]),
    .Q(\cpuregs[1][8] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15029_
  (
    .D(_00908_ ^ glitches[41]),
    .Q(\cpuregs[1][9] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15030_
  (
    .D(_00878_ ^ glitches[42]),
    .Q(\cpuregs[1][10] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15031_
  (
    .D(_00879_ ^ glitches[43]),
    .Q(\cpuregs[1][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15032_
  (
    .D(_00880_ ^ glitches[44]),
    .Q(\cpuregs[1][12] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15033_
  (
    .D(_00881_ ^ glitches[45]),
    .Q(\cpuregs[1][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15034_
  (
    .D(_00882_ ^ glitches[46]),
    .Q(\cpuregs[1][14] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15035_
  (
    .D(_00883_ ^ glitches[47]),
    .Q(\cpuregs[1][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15036_
  (
    .D(_00884_ ^ glitches[48]),
    .Q(\cpuregs[1][16] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15037_
  (
    .D(_00885_ ^ glitches[49]),
    .Q(\cpuregs[1][17] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15038_
  (
    .D(_00886_ ^ glitches[50]),
    .Q(\cpuregs[1][18] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15039_
  (
    .D(_00887_ ^ glitches[51]),
    .Q(\cpuregs[1][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15040_
  (
    .D(_00889_ ^ glitches[52]),
    .Q(\cpuregs[1][20] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15041_
  (
    .D(_00890_ ^ glitches[53]),
    .Q(\cpuregs[1][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15042_
  (
    .D(_00891_ ^ glitches[54]),
    .Q(\cpuregs[1][22] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15043_
  (
    .D(_00892_ ^ glitches[55]),
    .Q(\cpuregs[1][23] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15044_
  (
    .D(_00893_ ^ glitches[56]),
    .Q(\cpuregs[1][24] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15045_
  (
    .D(_00894_ ^ glitches[57]),
    .Q(\cpuregs[1][25] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15046_
  (
    .D(_00895_ ^ glitches[58]),
    .Q(\cpuregs[1][26] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15047_
  (
    .D(_00896_ ^ glitches[59]),
    .Q(\cpuregs[1][27] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15048_
  (
    .D(_00897_ ^ glitches[60]),
    .Q(\cpuregs[1][28] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15049_
  (
    .D(_00898_ ^ glitches[61]),
    .Q(\cpuregs[1][29] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15050_
  (
    .D(_00900_ ^ glitches[62]),
    .Q(\cpuregs[1][30] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15051_
  (
    .D(_00901_ ^ glitches[63]),
    .Q(\cpuregs[1][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15052_
  (
    .D(_00525_ ^ glitches[64]),
    .Q(\cpuregs[0][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15053_
  (
    .D(_00536_ ^ glitches[65]),
    .Q(\cpuregs[0][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15054_
  (
    .D(_00547_ ^ glitches[66]),
    .Q(\cpuregs[0][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15055_
  (
    .D(_00550_ ^ glitches[67]),
    .Q(\cpuregs[0][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15056_
  (
    .D(_00551_ ^ glitches[68]),
    .Q(\cpuregs[0][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15057_
  (
    .D(_00552_ ^ glitches[69]),
    .Q(\cpuregs[0][5] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15058_
  (
    .D(_00553_ ^ glitches[70]),
    .Q(\cpuregs[0][6] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15059_
  (
    .D(_00554_ ^ glitches[71]),
    .Q(\cpuregs[0][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15060_
  (
    .D(_00555_ ^ glitches[72]),
    .Q(\cpuregs[0][8] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15061_
  (
    .D(_00556_ ^ glitches[73]),
    .Q(\cpuregs[0][9] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15062_
  (
    .D(_00526_ ^ glitches[74]),
    .Q(\cpuregs[0][10] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15063_
  (
    .D(_00527_ ^ glitches[75]),
    .Q(\cpuregs[0][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15064_
  (
    .D(_00528_ ^ glitches[76]),
    .Q(\cpuregs[0][12] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15065_
  (
    .D(_00529_ ^ glitches[77]),
    .Q(\cpuregs[0][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15066_
  (
    .D(_00530_ ^ glitches[78]),
    .Q(\cpuregs[0][14] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15067_
  (
    .D(_00531_ ^ glitches[79]),
    .Q(\cpuregs[0][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15068_
  (
    .D(_00532_ ^ glitches[80]),
    .Q(\cpuregs[0][16] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15069_
  (
    .D(_00533_ ^ glitches[81]),
    .Q(\cpuregs[0][17] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15070_
  (
    .D(_00534_ ^ glitches[82]),
    .Q(\cpuregs[0][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15071_
  (
    .D(_00535_ ^ glitches[83]),
    .Q(\cpuregs[0][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15072_
  (
    .D(_00537_ ^ glitches[84]),
    .Q(\cpuregs[0][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15073_
  (
    .D(_00538_ ^ glitches[85]),
    .Q(\cpuregs[0][21] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15074_
  (
    .D(_00539_ ^ glitches[86]),
    .Q(\cpuregs[0][22] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15075_
  (
    .D(_00540_ ^ glitches[87]),
    .Q(\cpuregs[0][23] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15076_
  (
    .D(_00541_ ^ glitches[88]),
    .Q(\cpuregs[0][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15077_
  (
    .D(_00542_ ^ glitches[89]),
    .Q(\cpuregs[0][25] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15078_
  (
    .D(_00543_ ^ glitches[90]),
    .Q(\cpuregs[0][26] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15079_
  (
    .D(_00544_ ^ glitches[91]),
    .Q(\cpuregs[0][27] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15080_
  (
    .D(_00545_ ^ glitches[92]),
    .Q(\cpuregs[0][28] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15081_
  (
    .D(_00546_ ^ glitches[93]),
    .Q(\cpuregs[0][29] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15082_
  (
    .D(_00548_ ^ glitches[94]),
    .Q(\cpuregs[0][30] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15083_
  (
    .D(_00549_ ^ glitches[95]),
    .Q(\cpuregs[0][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15084_
  (
    .D(_00621_ ^ glitches[96]),
    .Q(\cpuregs[12][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15085_
  (
    .D(_00632_ ^ glitches[97]),
    .Q(\cpuregs[12][1] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15086_
  (
    .D(_00643_ ^ glitches[98]),
    .Q(\cpuregs[12][2] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15087_
  (
    .D(_00646_ ^ glitches[99]),
    .Q(\cpuregs[12][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15088_
  (
    .D(_00647_ ^ glitches[100]),
    .Q(\cpuregs[12][4] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15089_
  (
    .D(_00648_ ^ glitches[101]),
    .Q(\cpuregs[12][5] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15090_
  (
    .D(_00649_ ^ glitches[102]),
    .Q(\cpuregs[12][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15091_
  (
    .D(_00650_ ^ glitches[103]),
    .Q(\cpuregs[12][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15092_
  (
    .D(_00651_ ^ glitches[104]),
    .Q(\cpuregs[12][8] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15093_
  (
    .D(_00652_ ^ glitches[105]),
    .Q(\cpuregs[12][9] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15094_
  (
    .D(_00622_ ^ glitches[106]),
    .Q(\cpuregs[12][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15095_
  (
    .D(_00623_ ^ glitches[107]),
    .Q(\cpuregs[12][11] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15096_
  (
    .D(_00624_ ^ glitches[108]),
    .Q(\cpuregs[12][12] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15097_
  (
    .D(_00625_ ^ glitches[109]),
    .Q(\cpuregs[12][13] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15098_
  (
    .D(_00626_ ^ glitches[110]),
    .Q(\cpuregs[12][14] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15099_
  (
    .D(_00627_ ^ glitches[111]),
    .Q(\cpuregs[12][15] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15100_
  (
    .D(_00628_ ^ glitches[112]),
    .Q(\cpuregs[12][16] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15101_
  (
    .D(_00629_ ^ glitches[113]),
    .Q(\cpuregs[12][17] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15102_
  (
    .D(_00630_ ^ glitches[114]),
    .Q(\cpuregs[12][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15103_
  (
    .D(_00631_ ^ glitches[115]),
    .Q(\cpuregs[12][19] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15104_
  (
    .D(_00633_ ^ glitches[116]),
    .Q(\cpuregs[12][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15105_
  (
    .D(_00634_ ^ glitches[117]),
    .Q(\cpuregs[12][21] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15106_
  (
    .D(_00635_ ^ glitches[118]),
    .Q(\cpuregs[12][22] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15107_
  (
    .D(_00636_ ^ glitches[119]),
    .Q(\cpuregs[12][23] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15108_
  (
    .D(_00637_ ^ glitches[120]),
    .Q(\cpuregs[12][24] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15109_
  (
    .D(_00638_ ^ glitches[121]),
    .Q(\cpuregs[12][25] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15110_
  (
    .D(_00639_ ^ glitches[122]),
    .Q(\cpuregs[12][26] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15111_
  (
    .D(_00640_ ^ glitches[123]),
    .Q(\cpuregs[12][27] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15112_
  (
    .D(_00641_ ^ glitches[124]),
    .Q(\cpuregs[12][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15113_
  (
    .D(_00642_ ^ glitches[125]),
    .Q(\cpuregs[12][29] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15114_
  (
    .D(_00644_ ^ glitches[126]),
    .Q(\cpuregs[12][30] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15115_
  (
    .D(_00645_ ^ glitches[127]),
    .Q(\cpuregs[12][31] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15116_
  (
    .D(_01165_ ^ glitches[128]),
    .Q(\cpuregs[28][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15117_
  (
    .D(_01176_ ^ glitches[129]),
    .Q(\cpuregs[28][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15118_
  (
    .D(_01187_ ^ glitches[130]),
    .Q(\cpuregs[28][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15119_
  (
    .D(_01190_ ^ glitches[131]),
    .Q(\cpuregs[28][3] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15120_
  (
    .D(_01191_ ^ glitches[132]),
    .Q(\cpuregs[28][4] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15121_
  (
    .D(_01192_ ^ glitches[133]),
    .Q(\cpuregs[28][5] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15122_
  (
    .D(_01193_ ^ glitches[134]),
    .Q(\cpuregs[28][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15123_
  (
    .D(_01194_ ^ glitches[135]),
    .Q(\cpuregs[28][7] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15124_
  (
    .D(_01195_ ^ glitches[136]),
    .Q(\cpuregs[28][8] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15125_
  (
    .D(_01196_ ^ glitches[137]),
    .Q(\cpuregs[28][9] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15126_
  (
    .D(_01166_ ^ glitches[138]),
    .Q(\cpuregs[28][10] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15127_
  (
    .D(_01167_ ^ glitches[139]),
    .Q(\cpuregs[28][11] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15128_
  (
    .D(_01168_ ^ glitches[140]),
    .Q(\cpuregs[28][12] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15129_
  (
    .D(_01169_ ^ glitches[141]),
    .Q(\cpuregs[28][13] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15130_
  (
    .D(_01170_ ^ glitches[142]),
    .Q(\cpuregs[28][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15131_
  (
    .D(_01171_ ^ glitches[143]),
    .Q(\cpuregs[28][15] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15132_
  (
    .D(_01172_ ^ glitches[144]),
    .Q(\cpuregs[28][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15133_
  (
    .D(_01173_ ^ glitches[145]),
    .Q(\cpuregs[28][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15134_
  (
    .D(_01174_ ^ glitches[146]),
    .Q(\cpuregs[28][18] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15135_
  (
    .D(_01175_ ^ glitches[147]),
    .Q(\cpuregs[28][19] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15136_
  (
    .D(_01177_ ^ glitches[148]),
    .Q(\cpuregs[28][20] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15137_
  (
    .D(_01178_ ^ glitches[149]),
    .Q(\cpuregs[28][21] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15138_
  (
    .D(_01179_ ^ glitches[150]),
    .Q(\cpuregs[28][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15139_
  (
    .D(_01180_ ^ glitches[151]),
    .Q(\cpuregs[28][23] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15140_
  (
    .D(_01181_ ^ glitches[152]),
    .Q(\cpuregs[28][24] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15141_
  (
    .D(_01182_ ^ glitches[153]),
    .Q(\cpuregs[28][25] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15142_
  (
    .D(_01183_ ^ glitches[154]),
    .Q(\cpuregs[28][26] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15143_
  (
    .D(_01184_ ^ glitches[155]),
    .Q(\cpuregs[28][27] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15144_
  (
    .D(_01185_ ^ glitches[156]),
    .Q(\cpuregs[28][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15145_
  (
    .D(_01186_ ^ glitches[157]),
    .Q(\cpuregs[28][29] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15146_
  (
    .D(_01188_ ^ glitches[158]),
    .Q(\cpuregs[28][30] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15147_
  (
    .D(_01189_ ^ glitches[159]),
    .Q(\cpuregs[28][31] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15148_
  (
    .D(_00202_ ^ glitches[160]),
    .Q(\decoded_rs2[0] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15149_
  (
    .D(_00203_ ^ glitches[161]),
    .Q(\decoded_rs2[1] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15150_
  (
    .D(_00204_ ^ glitches[162]),
    .Q(\decoded_rs2[2] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15151_
  (
    .D(_00205_ ^ glitches[163]),
    .Q(\decoded_rs2[3] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15152_
  (
    .D(_00206_ ^ glitches[164]),
    .Q(\decoded_rs2[4] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15153_
  (
    .D(_00514_ ^ glitches[165]),
    .Q(trap),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15154_
  (
    .D(_00000_ ^ glitches[166]),
    .Q(\count_cycle[0] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15155_
  (
    .D(_00011_ ^ glitches[167]),
    .Q(\count_cycle[1] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15156_
  (
    .D(_00022_ ^ glitches[168]),
    .Q(\count_cycle[2] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15157_
  (
    .D(_00033_ ^ glitches[169]),
    .Q(\count_cycle[3] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15158_
  (
    .D(_00044_ ^ glitches[170]),
    .Q(\count_cycle[4] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15159_
  (
    .D(_00055_ ^ glitches[171]),
    .Q(\count_cycle[5] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15160_
  (
    .D(_00060_ ^ glitches[172]),
    .Q(\count_cycle[6] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15161_
  (
    .D(_00061_ ^ glitches[173]),
    .Q(\count_cycle[7] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15162_
  (
    .D(_00062_ ^ glitches[174]),
    .Q(\count_cycle[8] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15163_
  (
    .D(_00063_ ^ glitches[175]),
    .Q(\count_cycle[9] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15164_
  (
    .D(_00001_ ^ glitches[176]),
    .Q(\count_cycle[10] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15165_
  (
    .D(_00002_ ^ glitches[177]),
    .Q(\count_cycle[11] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15166_
  (
    .D(_00003_ ^ glitches[178]),
    .Q(\count_cycle[12] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15167_
  (
    .D(_00004_ ^ glitches[179]),
    .Q(\count_cycle[13] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15168_
  (
    .D(_00005_ ^ glitches[180]),
    .Q(\count_cycle[14] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15169_
  (
    .D(_00006_ ^ glitches[181]),
    .Q(\count_cycle[15] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15170_
  (
    .D(_00007_ ^ glitches[182]),
    .Q(\count_cycle[16] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15171_
  (
    .D(_00008_ ^ glitches[183]),
    .Q(\count_cycle[17] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15172_
  (
    .D(_00009_ ^ glitches[184]),
    .Q(\count_cycle[18] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15173_
  (
    .D(_00010_ ^ glitches[185]),
    .Q(\count_cycle[19] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15174_
  (
    .D(_00012_ ^ glitches[186]),
    .Q(\count_cycle[20] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15175_
  (
    .D(_00013_ ^ glitches[187]),
    .Q(\count_cycle[21] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15176_
  (
    .D(_00014_ ^ glitches[188]),
    .Q(\count_cycle[22] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15177_
  (
    .D(_00015_ ^ glitches[189]),
    .Q(\count_cycle[23] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15178_
  (
    .D(_00016_ ^ glitches[190]),
    .Q(\count_cycle[24] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15179_
  (
    .D(_00017_ ^ glitches[191]),
    .Q(\count_cycle[25] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15180_
  (
    .D(_00018_ ^ glitches[192]),
    .Q(\count_cycle[26] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15181_
  (
    .D(_00019_ ^ glitches[193]),
    .Q(\count_cycle[27] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15182_
  (
    .D(_00020_ ^ glitches[194]),
    .Q(\count_cycle[28] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15183_
  (
    .D(_00021_ ^ glitches[195]),
    .Q(\count_cycle[29] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15184_
  (
    .D(_00023_ ^ glitches[196]),
    .Q(\count_cycle[30] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15185_
  (
    .D(_00024_ ^ glitches[197]),
    .Q(\count_cycle[31] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15186_
  (
    .D(_00025_ ^ glitches[198]),
    .Q(\count_cycle[32] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15187_
  (
    .D(_00026_ ^ glitches[199]),
    .Q(\count_cycle[33] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15188_
  (
    .D(_00027_ ^ glitches[200]),
    .Q(\count_cycle[34] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15189_
  (
    .D(_00028_ ^ glitches[201]),
    .Q(\count_cycle[35] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15190_
  (
    .D(_00029_ ^ glitches[202]),
    .Q(\count_cycle[36] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15191_
  (
    .D(_00030_ ^ glitches[203]),
    .Q(\count_cycle[37] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15192_
  (
    .D(_00031_ ^ glitches[204]),
    .Q(\count_cycle[38] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15193_
  (
    .D(_00032_ ^ glitches[205]),
    .Q(\count_cycle[39] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15194_
  (
    .D(_00034_ ^ glitches[206]),
    .Q(\count_cycle[40] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15195_
  (
    .D(_00035_ ^ glitches[207]),
    .Q(\count_cycle[41] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15196_
  (
    .D(_00036_ ^ glitches[208]),
    .Q(\count_cycle[42] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15197_
  (
    .D(_00037_ ^ glitches[209]),
    .Q(\count_cycle[43] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15198_
  (
    .D(_00038_ ^ glitches[210]),
    .Q(\count_cycle[44] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15199_
  (
    .D(_00039_ ^ glitches[211]),
    .Q(\count_cycle[45] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15200_
  (
    .D(_00040_ ^ glitches[212]),
    .Q(\count_cycle[46] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15201_
  (
    .D(_00041_ ^ glitches[213]),
    .Q(\count_cycle[47] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15202_
  (
    .D(_00042_ ^ glitches[214]),
    .Q(\count_cycle[48] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15203_
  (
    .D(_00043_ ^ glitches[215]),
    .Q(\count_cycle[49] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15204_
  (
    .D(_00045_ ^ glitches[216]),
    .Q(\count_cycle[50] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15205_
  (
    .D(_00046_ ^ glitches[217]),
    .Q(\count_cycle[51] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15206_
  (
    .D(_00047_ ^ glitches[218]),
    .Q(\count_cycle[52] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15207_
  (
    .D(_00048_ ^ glitches[219]),
    .Q(\count_cycle[53] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15208_
  (
    .D(_00049_ ^ glitches[220]),
    .Q(\count_cycle[54] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15209_
  (
    .D(_00050_ ^ glitches[221]),
    .Q(\count_cycle[55] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15210_
  (
    .D(_00051_ ^ glitches[222]),
    .Q(\count_cycle[56] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15211_
  (
    .D(_00052_ ^ glitches[223]),
    .Q(\count_cycle[57] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15212_
  (
    .D(_00053_ ^ glitches[224]),
    .Q(\count_cycle[58] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15213_
  (
    .D(_00054_ ^ glitches[225]),
    .Q(\count_cycle[59] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15214_
  (
    .D(_00056_ ^ glitches[226]),
    .Q(\count_cycle[60] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15215_
  (
    .D(_00057_ ^ glitches[227]),
    .Q(\count_cycle[61] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15216_
  (
    .D(_00058_ ^ glitches[228]),
    .Q(\count_cycle[62] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15217_
  (
    .D(_00059_ ^ glitches[229]),
    .Q(\count_cycle[63] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15218_
  (
    .D(_00064_ ^ glitches[230]),
    .Q(\count_instr[0] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15219_
  (
    .D(_00075_ ^ glitches[231]),
    .Q(\count_instr[1] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15220_
  (
    .D(_00086_ ^ glitches[232]),
    .Q(\count_instr[2] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15221_
  (
    .D(_00097_ ^ glitches[233]),
    .Q(\count_instr[3] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15222_
  (
    .D(_00108_ ^ glitches[234]),
    .Q(\count_instr[4] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15223_
  (
    .D(_00119_ ^ glitches[235]),
    .Q(\count_instr[5] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15224_
  (
    .D(_00124_ ^ glitches[236]),
    .Q(\count_instr[6] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15225_
  (
    .D(_00125_ ^ glitches[237]),
    .Q(\count_instr[7] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15226_
  (
    .D(_00126_ ^ glitches[238]),
    .Q(\count_instr[8] ),
    .CLK(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15227_
  (
    .D(_00127_ ^ glitches[239]),
    .Q(\count_instr[9] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15228_
  (
    .D(_00065_ ^ glitches[240]),
    .Q(\count_instr[10] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15229_
  (
    .D(_00066_ ^ glitches[241]),
    .Q(\count_instr[11] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15230_
  (
    .D(_00067_ ^ glitches[242]),
    .Q(\count_instr[12] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15231_
  (
    .D(_00068_ ^ glitches[243]),
    .Q(\count_instr[13] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15232_
  (
    .D(_00069_ ^ glitches[244]),
    .Q(\count_instr[14] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15233_
  (
    .D(_00070_ ^ glitches[245]),
    .Q(\count_instr[15] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15234_
  (
    .D(_00071_ ^ glitches[246]),
    .Q(\count_instr[16] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15235_
  (
    .D(_00072_ ^ glitches[247]),
    .Q(\count_instr[17] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15236_
  (
    .D(_00073_ ^ glitches[248]),
    .Q(\count_instr[18] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15237_
  (
    .D(_00074_ ^ glitches[249]),
    .Q(\count_instr[19] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15238_
  (
    .D(_00076_ ^ glitches[250]),
    .Q(\count_instr[20] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15239_
  (
    .D(_00077_ ^ glitches[251]),
    .Q(\count_instr[21] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15240_
  (
    .D(_00078_ ^ glitches[252]),
    .Q(\count_instr[22] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15241_
  (
    .D(_00079_ ^ glitches[253]),
    .Q(\count_instr[23] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15242_
  (
    .D(_00080_ ^ glitches[254]),
    .Q(\count_instr[24] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15243_
  (
    .D(_00081_ ^ glitches[255]),
    .Q(\count_instr[25] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15244_
  (
    .D(_00082_ ^ glitches[256]),
    .Q(\count_instr[26] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15245_
  (
    .D(_00083_ ^ glitches[257]),
    .Q(\count_instr[27] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15246_
  (
    .D(_00084_ ^ glitches[258]),
    .Q(\count_instr[28] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15247_
  (
    .D(_00085_ ^ glitches[259]),
    .Q(\count_instr[29] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15248_
  (
    .D(_00087_ ^ glitches[260]),
    .Q(\count_instr[30] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15249_
  (
    .D(_00088_ ^ glitches[261]),
    .Q(\count_instr[31] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15250_
  (
    .D(_00089_ ^ glitches[262]),
    .Q(\count_instr[32] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15251_
  (
    .D(_00090_ ^ glitches[263]),
    .Q(\count_instr[33] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15252_
  (
    .D(_00091_ ^ glitches[264]),
    .Q(\count_instr[34] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15253_
  (
    .D(_00092_ ^ glitches[265]),
    .Q(\count_instr[35] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15254_
  (
    .D(_00093_ ^ glitches[266]),
    .Q(\count_instr[36] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15255_
  (
    .D(_00094_ ^ glitches[267]),
    .Q(\count_instr[37] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15256_
  (
    .D(_00095_ ^ glitches[268]),
    .Q(\count_instr[38] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15257_
  (
    .D(_00096_ ^ glitches[269]),
    .Q(\count_instr[39] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15258_
  (
    .D(_00098_ ^ glitches[270]),
    .Q(\count_instr[40] ),
    .CLK(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15259_
  (
    .D(_00099_ ^ glitches[271]),
    .Q(\count_instr[41] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15260_
  (
    .D(_00100_ ^ glitches[272]),
    .Q(\count_instr[42] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15261_
  (
    .D(_00101_ ^ glitches[273]),
    .Q(\count_instr[43] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15262_
  (
    .D(_00102_ ^ glitches[274]),
    .Q(\count_instr[44] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15263_
  (
    .D(_00103_ ^ glitches[275]),
    .Q(\count_instr[45] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15264_
  (
    .D(_00104_ ^ glitches[276]),
    .Q(\count_instr[46] ),
    .CLK(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15265_
  (
    .D(_00105_ ^ glitches[277]),
    .Q(\count_instr[47] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15266_
  (
    .D(_00106_ ^ glitches[278]),
    .Q(\count_instr[48] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15267_
  (
    .D(_00107_ ^ glitches[279]),
    .Q(\count_instr[49] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15268_
  (
    .D(_00109_ ^ glitches[280]),
    .Q(\count_instr[50] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15269_
  (
    .D(_00110_ ^ glitches[281]),
    .Q(\count_instr[51] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15270_
  (
    .D(_00111_ ^ glitches[282]),
    .Q(\count_instr[52] ),
    .CLK(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15271_
  (
    .D(_00112_ ^ glitches[283]),
    .Q(\count_instr[53] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15272_
  (
    .D(_00113_ ^ glitches[284]),
    .Q(\count_instr[54] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15273_
  (
    .D(_00114_ ^ glitches[285]),
    .Q(\count_instr[55] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15274_
  (
    .D(_00115_ ^ glitches[286]),
    .Q(\count_instr[56] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15275_
  (
    .D(_00116_ ^ glitches[287]),
    .Q(\count_instr[57] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15276_
  (
    .D(_00117_ ^ glitches[288]),
    .Q(\count_instr[58] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15277_
  (
    .D(_00118_ ^ glitches[289]),
    .Q(\count_instr[59] ),
    .CLK(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15278_
  (
    .D(_00120_ ^ glitches[290]),
    .Q(\count_instr[60] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15279_
  (
    .D(_00121_ ^ glitches[291]),
    .Q(\count_instr[61] ),
    .CLK(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15280_
  (
    .D(_00122_ ^ glitches[292]),
    .Q(\count_instr[62] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15281_
  (
    .D(_00123_ ^ glitches[293]),
    .Q(\count_instr[63] ),
    .CLK(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15282_
  (
    .D(_00477_ ^ glitches[294]),
    .Q(\reg_pc[0] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15283_
  (
    .D(_00488_ ^ glitches[295]),
    .Q(\reg_pc[1] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15284_
  (
    .D(_00499_ ^ glitches[296]),
    .Q(\reg_pc[2] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15285_
  (
    .D(_00502_ ^ glitches[297]),
    .Q(\reg_pc[3] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15286_
  (
    .D(_00503_ ^ glitches[298]),
    .Q(\reg_pc[4] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15287_
  (
    .D(_00504_ ^ glitches[299]),
    .Q(\reg_pc[5] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15288_
  (
    .D(_00505_ ^ glitches[300]),
    .Q(\reg_pc[6] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15289_
  (
    .D(_00506_ ^ glitches[301]),
    .Q(\reg_pc[7] ),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15290_
  (
    .D(_00507_ ^ glitches[302]),
    .Q(\reg_pc[8] ),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15291_
  (
    .D(_00508_ ^ glitches[303]),
    .Q(\reg_pc[9] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15292_
  (
    .D(_00478_ ^ glitches[304]),
    .Q(\reg_pc[10] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15293_
  (
    .D(_00479_ ^ glitches[305]),
    .Q(\reg_pc[11] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15294_
  (
    .D(_00480_ ^ glitches[306]),
    .Q(\reg_pc[12] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15295_
  (
    .D(_00481_ ^ glitches[307]),
    .Q(\reg_pc[13] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15296_
  (
    .D(_00482_ ^ glitches[308]),
    .Q(\reg_pc[14] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15297_
  (
    .D(_00483_ ^ glitches[309]),
    .Q(\reg_pc[15] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15298_
  (
    .D(_00484_ ^ glitches[310]),
    .Q(\reg_pc[16] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15299_
  (
    .D(_00485_ ^ glitches[311]),
    .Q(\reg_pc[17] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15300_
  (
    .D(_00486_ ^ glitches[312]),
    .Q(\reg_pc[18] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15301_
  (
    .D(_00487_ ^ glitches[313]),
    .Q(\reg_pc[19] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15302_
  (
    .D(_00489_ ^ glitches[314]),
    .Q(\reg_pc[20] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15303_
  (
    .D(_00490_ ^ glitches[315]),
    .Q(\reg_pc[21] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15304_
  (
    .D(_00491_ ^ glitches[316]),
    .Q(\reg_pc[22] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15305_
  (
    .D(_00492_ ^ glitches[317]),
    .Q(\reg_pc[23] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15306_
  (
    .D(_00493_ ^ glitches[318]),
    .Q(\reg_pc[24] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15307_
  (
    .D(_00494_ ^ glitches[319]),
    .Q(\reg_pc[25] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15308_
  (
    .D(_00495_ ^ glitches[320]),
    .Q(\reg_pc[26] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15309_
  (
    .D(_00496_ ^ glitches[321]),
    .Q(\reg_pc[27] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15310_
  (
    .D(_00497_ ^ glitches[322]),
    .Q(\reg_pc[28] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15311_
  (
    .D(_00498_ ^ glitches[323]),
    .Q(\reg_pc[29] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15312_
  (
    .D(_00500_ ^ glitches[324]),
    .Q(\reg_pc[30] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15313_
  (
    .D(_00501_ ^ glitches[325]),
    .Q(\reg_pc[31] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15314_
  (
    .D(_00349_ ^ glitches[326]),
    .Q(\reg_next_pc[0] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15315_
  (
    .D(_00360_ ^ glitches[327]),
    .Q(\reg_next_pc[1] ),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15316_
  (
    .D(_00371_ ^ glitches[328]),
    .Q(\reg_next_pc[2] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15317_
  (
    .D(_00374_ ^ glitches[329]),
    .Q(\reg_next_pc[3] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15318_
  (
    .D(_00375_ ^ glitches[330]),
    .Q(\reg_next_pc[4] ),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15319_
  (
    .D(_00376_ ^ glitches[331]),
    .Q(\reg_next_pc[5] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15320_
  (
    .D(_00377_ ^ glitches[332]),
    .Q(\reg_next_pc[6] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15321_
  (
    .D(_00378_ ^ glitches[333]),
    .Q(\reg_next_pc[7] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15322_
  (
    .D(_00379_ ^ glitches[334]),
    .Q(\reg_next_pc[8] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15323_
  (
    .D(_00380_ ^ glitches[335]),
    .Q(\reg_next_pc[9] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15324_
  (
    .D(_00350_ ^ glitches[336]),
    .Q(\reg_next_pc[10] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15325_
  (
    .D(_00351_ ^ glitches[337]),
    .Q(\reg_next_pc[11] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15326_
  (
    .D(_00352_ ^ glitches[338]),
    .Q(\reg_next_pc[12] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15327_
  (
    .D(_00353_ ^ glitches[339]),
    .Q(\reg_next_pc[13] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15328_
  (
    .D(_00354_ ^ glitches[340]),
    .Q(\reg_next_pc[14] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15329_
  (
    .D(_00355_ ^ glitches[341]),
    .Q(\reg_next_pc[15] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15330_
  (
    .D(_00356_ ^ glitches[342]),
    .Q(\reg_next_pc[16] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15331_
  (
    .D(_00357_ ^ glitches[343]),
    .Q(\reg_next_pc[17] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15332_
  (
    .D(_00358_ ^ glitches[344]),
    .Q(\reg_next_pc[18] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15333_
  (
    .D(_00359_ ^ glitches[345]),
    .Q(\reg_next_pc[19] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15334_
  (
    .D(_00361_ ^ glitches[346]),
    .Q(\reg_next_pc[20] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15335_
  (
    .D(_00362_ ^ glitches[347]),
    .Q(\reg_next_pc[21] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15336_
  (
    .D(_00363_ ^ glitches[348]),
    .Q(\reg_next_pc[22] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15337_
  (
    .D(_00364_ ^ glitches[349]),
    .Q(\reg_next_pc[23] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15338_
  (
    .D(_00365_ ^ glitches[350]),
    .Q(\reg_next_pc[24] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15339_
  (
    .D(_00366_ ^ glitches[351]),
    .Q(\reg_next_pc[25] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15340_
  (
    .D(_00367_ ^ glitches[352]),
    .Q(\reg_next_pc[26] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15341_
  (
    .D(_00368_ ^ glitches[353]),
    .Q(\reg_next_pc[27] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15342_
  (
    .D(_00369_ ^ glitches[354]),
    .Q(\reg_next_pc[28] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15343_
  (
    .D(_00370_ ^ glitches[355]),
    .Q(\reg_next_pc[29] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15344_
  (
    .D(_00372_ ^ glitches[356]),
    .Q(\reg_next_pc[30] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15345_
  (
    .D(_00373_ ^ glitches[357]),
    .Q(\reg_next_pc[31] ),
    .CLK(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15346_
  (
    .D(_00381_ ^ glitches[358]),
    .Q(pcpi_rs1[0]),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15347_
  (
    .D(_00392_ ^ glitches[359]),
    .Q(pcpi_rs1[1]),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15348_
  (
    .D(_00403_ ^ glitches[360]),
    .Q(pcpi_rs1[2]),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15349_
  (
    .D(_00406_ ^ glitches[361]),
    .Q(pcpi_rs1[3]),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15350_
  (
    .D(_00407_ ^ glitches[362]),
    .Q(pcpi_rs1[4]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15351_
  (
    .D(_00408_ ^ glitches[363]),
    .Q(pcpi_rs1[5]),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15352_
  (
    .D(_00409_ ^ glitches[364]),
    .Q(pcpi_rs1[6]),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15353_
  (
    .D(_00410_ ^ glitches[365]),
    .Q(pcpi_rs1[7]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15354_
  (
    .D(_00411_ ^ glitches[366]),
    .Q(pcpi_rs1[8]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15355_
  (
    .D(_00412_ ^ glitches[367]),
    .Q(pcpi_rs1[9]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15356_
  (
    .D(_00382_ ^ glitches[368]),
    .Q(pcpi_rs1[10]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15357_
  (
    .D(_00383_ ^ glitches[369]),
    .Q(pcpi_rs1[11]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15358_
  (
    .D(_00384_ ^ glitches[370]),
    .Q(pcpi_rs1[12]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15359_
  (
    .D(_00385_ ^ glitches[371]),
    .Q(pcpi_rs1[13]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15360_
  (
    .D(_00386_ ^ glitches[372]),
    .Q(pcpi_rs1[14]),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15361_
  (
    .D(_00387_ ^ glitches[373]),
    .Q(pcpi_rs1[15]),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15362_
  (
    .D(_00388_ ^ glitches[374]),
    .Q(pcpi_rs1[16]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15363_
  (
    .D(_00389_ ^ glitches[375]),
    .Q(pcpi_rs1[17]),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15364_
  (
    .D(_00390_ ^ glitches[376]),
    .Q(pcpi_rs1[18]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15365_
  (
    .D(_00391_ ^ glitches[377]),
    .Q(pcpi_rs1[19]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15366_
  (
    .D(_00393_ ^ glitches[378]),
    .Q(pcpi_rs1[20]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15367_
  (
    .D(_00394_ ^ glitches[379]),
    .Q(pcpi_rs1[21]),
    .CLK(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15368_
  (
    .D(_00395_ ^ glitches[380]),
    .Q(pcpi_rs1[22]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15369_
  (
    .D(_00396_ ^ glitches[381]),
    .Q(pcpi_rs1[23]),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15370_
  (
    .D(_00397_ ^ glitches[382]),
    .Q(pcpi_rs1[24]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15371_
  (
    .D(_00398_ ^ glitches[383]),
    .Q(pcpi_rs1[25]),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15372_
  (
    .D(_00399_ ^ glitches[384]),
    .Q(pcpi_rs1[26]),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15373_
  (
    .D(_00400_ ^ glitches[385]),
    .Q(pcpi_rs1[27]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15374_
  (
    .D(_00401_ ^ glitches[386]),
    .Q(pcpi_rs1[28]),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15375_
  (
    .D(_00402_ ^ glitches[387]),
    .Q(pcpi_rs1[29]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15376_
  (
    .D(_00404_ ^ glitches[388]),
    .Q(pcpi_rs1[30]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15377_
  (
    .D(_00405_ ^ glitches[389]),
    .Q(pcpi_rs1[31]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15378_
  (
    .D(_00413_ ^ glitches[390]),
    .Q(mem_la_wdata[0]),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15379_
  (
    .D(_00424_ ^ glitches[391]),
    .Q(mem_la_wdata[1]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15380_
  (
    .D(_00435_ ^ glitches[392]),
    .Q(mem_la_wdata[2]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15381_
  (
    .D(_00438_ ^ glitches[393]),
    .Q(mem_la_wdata[3]),
    .CLK(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15382_
  (
    .D(_00439_ ^ glitches[394]),
    .Q(mem_la_wdata[4]),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15383_
  (
    .D(_00440_ ^ glitches[395]),
    .Q(mem_la_wdata[5]),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15384_
  (
    .D(_00441_ ^ glitches[396]),
    .Q(mem_la_wdata[6]),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15385_
  (
    .D(_00442_ ^ glitches[397]),
    .Q(mem_la_wdata[7]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15386_
  (
    .D(_00443_ ^ glitches[398]),
    .Q(pcpi_rs2[8]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15387_
  (
    .D(_00444_ ^ glitches[399]),
    .Q(pcpi_rs2[9]),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15388_
  (
    .D(_00414_ ^ glitches[400]),
    .Q(pcpi_rs2[10]),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15389_
  (
    .D(_00415_ ^ glitches[401]),
    .Q(pcpi_rs2[11]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15390_
  (
    .D(_00416_ ^ glitches[402]),
    .Q(pcpi_rs2[12]),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15391_
  (
    .D(_00417_ ^ glitches[403]),
    .Q(pcpi_rs2[13]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15392_
  (
    .D(_00418_ ^ glitches[404]),
    .Q(pcpi_rs2[14]),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15393_
  (
    .D(_00419_ ^ glitches[405]),
    .Q(pcpi_rs2[15]),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15394_
  (
    .D(_00420_ ^ glitches[406]),
    .Q(pcpi_rs2[16]),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15395_
  (
    .D(_00421_ ^ glitches[407]),
    .Q(pcpi_rs2[17]),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15396_
  (
    .D(_00422_ ^ glitches[408]),
    .Q(pcpi_rs2[18]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15397_
  (
    .D(_00423_ ^ glitches[409]),
    .Q(pcpi_rs2[19]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15398_
  (
    .D(_00425_ ^ glitches[410]),
    .Q(pcpi_rs2[20]),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15399_
  (
    .D(_00426_ ^ glitches[411]),
    .Q(pcpi_rs2[21]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15400_
  (
    .D(_00427_ ^ glitches[412]),
    .Q(pcpi_rs2[22]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15401_
  (
    .D(_00428_ ^ glitches[413]),
    .Q(pcpi_rs2[23]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15402_
  (
    .D(_00429_ ^ glitches[414]),
    .Q(pcpi_rs2[24]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15403_
  (
    .D(_00430_ ^ glitches[415]),
    .Q(pcpi_rs2[25]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15404_
  (
    .D(_00431_ ^ glitches[416]),
    .Q(pcpi_rs2[26]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15405_
  (
    .D(_00432_ ^ glitches[417]),
    .Q(pcpi_rs2[27]),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15406_
  (
    .D(_00433_ ^ glitches[418]),
    .Q(pcpi_rs2[28]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15407_
  (
    .D(_00434_ ^ glitches[419]),
    .Q(pcpi_rs2[29]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15408_
  (
    .D(_00436_ ^ glitches[420]),
    .Q(pcpi_rs2[30]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15409_
  (
    .D(_00437_ ^ glitches[421]),
    .Q(pcpi_rs2[31]),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15410_
  (
    .D(_00445_ ^ glitches[422]),
    .Q(\reg_out[0] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15411_
  (
    .D(_00456_ ^ glitches[423]),
    .Q(\reg_out[1] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15412_
  (
    .D(_00467_ ^ glitches[424]),
    .Q(\reg_out[2] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15413_
  (
    .D(_00470_ ^ glitches[425]),
    .Q(\reg_out[3] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15414_
  (
    .D(_00471_ ^ glitches[426]),
    .Q(\reg_out[4] ),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15415_
  (
    .D(_00472_ ^ glitches[427]),
    .Q(\reg_out[5] ),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15416_
  (
    .D(_00473_ ^ glitches[428]),
    .Q(\reg_out[6] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15417_
  (
    .D(_00474_ ^ glitches[429]),
    .Q(\reg_out[7] ),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15418_
  (
    .D(_00475_ ^ glitches[430]),
    .Q(\reg_out[8] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15419_
  (
    .D(_00476_ ^ glitches[431]),
    .Q(\reg_out[9] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15420_
  (
    .D(_00446_ ^ glitches[432]),
    .Q(\reg_out[10] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15421_
  (
    .D(_00447_ ^ glitches[433]),
    .Q(\reg_out[11] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15422_
  (
    .D(_00448_ ^ glitches[434]),
    .Q(\reg_out[12] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15423_
  (
    .D(_00449_ ^ glitches[435]),
    .Q(\reg_out[13] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15424_
  (
    .D(_00450_ ^ glitches[436]),
    .Q(\reg_out[14] ),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15425_
  (
    .D(_00451_ ^ glitches[437]),
    .Q(\reg_out[15] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15426_
  (
    .D(_00452_ ^ glitches[438]),
    .Q(\reg_out[16] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15427_
  (
    .D(_00453_ ^ glitches[439]),
    .Q(\reg_out[17] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15428_
  (
    .D(_00454_ ^ glitches[440]),
    .Q(\reg_out[18] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15429_
  (
    .D(_00455_ ^ glitches[441]),
    .Q(\reg_out[19] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15430_
  (
    .D(_00457_ ^ glitches[442]),
    .Q(\reg_out[20] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15431_
  (
    .D(_00458_ ^ glitches[443]),
    .Q(\reg_out[21] ),
    .CLK(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15432_
  (
    .D(_00459_ ^ glitches[444]),
    .Q(\reg_out[22] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15433_
  (
    .D(_00460_ ^ glitches[445]),
    .Q(\reg_out[23] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15434_
  (
    .D(_00461_ ^ glitches[446]),
    .Q(\reg_out[24] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15435_
  (
    .D(_00462_ ^ glitches[447]),
    .Q(\reg_out[25] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15436_
  (
    .D(_00463_ ^ glitches[448]),
    .Q(\reg_out[26] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15437_
  (
    .D(_00464_ ^ glitches[449]),
    .Q(\reg_out[27] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15438_
  (
    .D(_00465_ ^ glitches[450]),
    .Q(\reg_out[28] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15439_
  (
    .D(_00466_ ^ glitches[451]),
    .Q(\reg_out[29] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15440_
  (
    .D(_00468_ ^ glitches[452]),
    .Q(\reg_out[30] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15441_
  (
    .D(_00469_ ^ glitches[453]),
    .Q(\reg_out[31] ),
    .CLK(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15442_
  (
    .D(_00509_ ^ glitches[454]),
    .Q(\reg_sh[0] ),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15443_
  (
    .D(_00510_ ^ glitches[455]),
    .Q(\reg_sh[1] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15444_
  (
    .D(_00511_ ^ glitches[456]),
    .Q(\reg_sh[2] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15445_
  (
    .D(_00512_ ^ glitches[457]),
    .Q(\reg_sh[3] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15446_
  (
    .D(_00513_ ^ glitches[458]),
    .Q(\reg_sh[4] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15447_
  (
    .D(_00305_ ^ glitches[459]),
    .Q(mem_do_prefetch),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15448_
  (
    .D(_00307_ ^ glitches[460]),
    .Q(mem_do_rinst),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15449_
  (
    .D(_00306_ ^ glitches[461]),
    .Q(mem_do_rdata),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15450_
  (
    .D(_00308_ ^ glitches[462]),
    .Q(mem_do_wdata),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15451_
  (
    .D(_00208_ ^ glitches[463]),
    .Q(decoder_trigger),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15452_
  (
    .D(_00207_ ^ glitches[464]),
    .Q(decoder_pseudo_trigger),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15453_
  (
    .D(_00272_ ^ glitches[465]),
    .Q(latched_store),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15454_
  (
    .D(_00271_ ^ glitches[466]),
    .Q(latched_stalu),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15455_
  (
    .D(_00262_ ^ glitches[467]),
    .Q(latched_branch),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15456_
  (
    .D(_00263_ ^ glitches[468]),
    .Q(latched_compr),
    .CLK(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15457_
  (
    .D(_00589_ ^ glitches[469]),
    .Q(\cpuregs[11][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15458_
  (
    .D(_00600_ ^ glitches[470]),
    .Q(\cpuregs[11][1] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15459_
  (
    .D(_00611_ ^ glitches[471]),
    .Q(\cpuregs[11][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15460_
  (
    .D(_00614_ ^ glitches[472]),
    .Q(\cpuregs[11][3] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15461_
  (
    .D(_00615_ ^ glitches[473]),
    .Q(\cpuregs[11][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15462_
  (
    .D(_00616_ ^ glitches[474]),
    .Q(\cpuregs[11][5] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15463_
  (
    .D(_00617_ ^ glitches[475]),
    .Q(\cpuregs[11][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15464_
  (
    .D(_00618_ ^ glitches[476]),
    .Q(\cpuregs[11][7] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15465_
  (
    .D(_00619_ ^ glitches[477]),
    .Q(\cpuregs[11][8] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15466_
  (
    .D(_00620_ ^ glitches[478]),
    .Q(\cpuregs[11][9] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15467_
  (
    .D(_00590_ ^ glitches[479]),
    .Q(\cpuregs[11][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15468_
  (
    .D(_00591_ ^ glitches[480]),
    .Q(\cpuregs[11][11] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15469_
  (
    .D(_00592_ ^ glitches[481]),
    .Q(\cpuregs[11][12] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15470_
  (
    .D(_00593_ ^ glitches[482]),
    .Q(\cpuregs[11][13] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15471_
  (
    .D(_00594_ ^ glitches[483]),
    .Q(\cpuregs[11][14] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15472_
  (
    .D(_00595_ ^ glitches[484]),
    .Q(\cpuregs[11][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15473_
  (
    .D(_00596_ ^ glitches[485]),
    .Q(\cpuregs[11][16] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15474_
  (
    .D(_00597_ ^ glitches[486]),
    .Q(\cpuregs[11][17] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15475_
  (
    .D(_00598_ ^ glitches[487]),
    .Q(\cpuregs[11][18] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15476_
  (
    .D(_00599_ ^ glitches[488]),
    .Q(\cpuregs[11][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15477_
  (
    .D(_00601_ ^ glitches[489]),
    .Q(\cpuregs[11][20] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15478_
  (
    .D(_00602_ ^ glitches[490]),
    .Q(\cpuregs[11][21] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15479_
  (
    .D(_00603_ ^ glitches[491]),
    .Q(\cpuregs[11][22] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15480_
  (
    .D(_00604_ ^ glitches[492]),
    .Q(\cpuregs[11][23] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15481_
  (
    .D(_00605_ ^ glitches[493]),
    .Q(\cpuregs[11][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15482_
  (
    .D(_00606_ ^ glitches[494]),
    .Q(\cpuregs[11][25] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15483_
  (
    .D(_00607_ ^ glitches[495]),
    .Q(\cpuregs[11][26] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15484_
  (
    .D(_00608_ ^ glitches[496]),
    .Q(\cpuregs[11][27] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15485_
  (
    .D(_00609_ ^ glitches[497]),
    .Q(\cpuregs[11][28] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15486_
  (
    .D(_00610_ ^ glitches[498]),
    .Q(\cpuregs[11][29] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15487_
  (
    .D(_00612_ ^ glitches[499]),
    .Q(\cpuregs[11][30] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15488_
  (
    .D(_00613_ ^ glitches[500]),
    .Q(\cpuregs[11][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15489_
  (
    .D(_00265_ ^ glitches[501]),
    .Q(latched_is_lh),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15490_
  (
    .D(_00264_ ^ glitches[502]),
    .Q(latched_is_lb),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15491_
  (
    .D(_00266_ ^ glitches[503]),
    .Q(\latched_rd[0] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15492_
  (
    .D(_00267_ ^ glitches[504]),
    .Q(\latched_rd[1] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15493_
  (
    .D(_00268_ ^ glitches[505]),
    .Q(\latched_rd[2] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15494_
  (
    .D(_00269_ ^ glitches[506]),
    .Q(\latched_rd[3] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15495_
  (
    .D(_00270_ ^ glitches[507]),
    .Q(\latched_rd[4] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15496_
  (
    .D(\alu_out[0]  ^ glitches[508]),
    .Q(\alu_out_q[0] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15497_
  (
    .D(\alu_out[1]  ^ glitches[509]),
    .Q(\alu_out_q[1] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15498_
  (
    .D(\alu_out[2]  ^ glitches[510]),
    .Q(\alu_out_q[2] ),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15499_
  (
    .D(\alu_out[3]  ^ glitches[511]),
    .Q(\alu_out_q[3] ),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15500_
  (
    .D(\alu_out[4]  ^ glitches[512]),
    .Q(\alu_out_q[4] ),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15501_
  (
    .D(\alu_out[5]  ^ glitches[513]),
    .Q(\alu_out_q[5] ),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15502_
  (
    .D(\alu_out[6]  ^ glitches[514]),
    .Q(\alu_out_q[6] ),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15503_
  (
    .D(\alu_out[7]  ^ glitches[515]),
    .Q(\alu_out_q[7] ),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15504_
  (
    .D(\alu_out[8]  ^ glitches[516]),
    .Q(\alu_out_q[8] ),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15505_
  (
    .D(\alu_out[9]  ^ glitches[517]),
    .Q(\alu_out_q[9] ),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15506_
  (
    .D(\alu_out[10]  ^ glitches[518]),
    .Q(\alu_out_q[10] ),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15507_
  (
    .D(\alu_out[11]  ^ glitches[519]),
    .Q(\alu_out_q[11] ),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15508_
  (
    .D(\alu_out[12]  ^ glitches[520]),
    .Q(\alu_out_q[12] ),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15509_
  (
    .D(\alu_out[13]  ^ glitches[521]),
    .Q(\alu_out_q[13] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15510_
  (
    .D(\alu_out[14]  ^ glitches[522]),
    .Q(\alu_out_q[14] ),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15511_
  (
    .D(\alu_out[15]  ^ glitches[523]),
    .Q(\alu_out_q[15] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15512_
  (
    .D(\alu_out[16]  ^ glitches[524]),
    .Q(\alu_out_q[16] ),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15513_
  (
    .D(\alu_out[17]  ^ glitches[525]),
    .Q(\alu_out_q[17] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15514_
  (
    .D(\alu_out[18]  ^ glitches[526]),
    .Q(\alu_out_q[18] ),
    .CLK(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15515_
  (
    .D(\alu_out[19]  ^ glitches[527]),
    .Q(\alu_out_q[19] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15516_
  (
    .D(\alu_out[20]  ^ glitches[528]),
    .Q(\alu_out_q[20] ),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15517_
  (
    .D(\alu_out[21]  ^ glitches[529]),
    .Q(\alu_out_q[21] ),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15518_
  (
    .D(\alu_out[22]  ^ glitches[530]),
    .Q(\alu_out_q[22] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15519_
  (
    .D(\alu_out[23]  ^ glitches[531]),
    .Q(\alu_out_q[23] ),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15520_
  (
    .D(\alu_out[24]  ^ glitches[532]),
    .Q(\alu_out_q[24] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15521_
  (
    .D(\alu_out[25]  ^ glitches[533]),
    .Q(\alu_out_q[25] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15522_
  (
    .D(\alu_out[26]  ^ glitches[534]),
    .Q(\alu_out_q[26] ),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15523_
  (
    .D(\alu_out[27]  ^ glitches[535]),
    .Q(\alu_out_q[27] ),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15524_
  (
    .D(\alu_out[28]  ^ glitches[536]),
    .Q(\alu_out_q[28] ),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15525_
  (
    .D(\alu_out[29]  ^ glitches[537]),
    .Q(\alu_out_q[29] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15526_
  (
    .D(\alu_out[30]  ^ glitches[538]),
    .Q(\alu_out_q[30] ),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15527_
  (
    .D(\alu_out[31]  ^ glitches[539]),
    .Q(\alu_out_q[31] ),
    .CLK(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15528_
  (
    .D(_00226_ ^ glitches[540]),
    .Q(instr_lui),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15529_
  (
    .D(_00213_ ^ glitches[541]),
    .Q(instr_auipc),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15530_
  (
    .D(_00220_ ^ glitches[542]),
    .Q(instr_jal),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15531_
  (
    .D(_00221_ ^ glitches[543]),
    .Q(instr_jalr),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15532_
  (
    .D(_00214_ ^ glitches[544]),
    .Q(instr_beq),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15533_
  (
    .D(_00219_ ^ glitches[545]),
    .Q(instr_bne),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15534_
  (
    .D(_00217_ ^ glitches[546]),
    .Q(instr_blt),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15535_
  (
    .D(_00215_ ^ glitches[547]),
    .Q(instr_bge),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15536_
  (
    .D(_00218_ ^ glitches[548]),
    .Q(instr_bltu),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15537_
  (
    .D(_00216_ ^ glitches[549]),
    .Q(instr_bgeu),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15538_
  (
    .D(_00222_ ^ glitches[550]),
    .Q(instr_lb),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15539_
  (
    .D(_00224_ ^ glitches[551]),
    .Q(instr_lh),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15540_
  (
    .D(_00227_ ^ glitches[552]),
    .Q(instr_lw),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15541_
  (
    .D(_00223_ ^ glitches[553]),
    .Q(instr_lbu),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15542_
  (
    .D(_00225_ ^ glitches[554]),
    .Q(instr_lhu),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15543_
  (
    .D(_00234_ ^ glitches[555]),
    .Q(instr_sb),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15544_
  (
    .D(_00235_ ^ glitches[556]),
    .Q(instr_sh),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15545_
  (
    .D(_00247_ ^ glitches[557]),
    .Q(instr_sw),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15546_
  (
    .D(_00210_ ^ glitches[558]),
    .Q(instr_addi),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15547_
  (
    .D(_00239_ ^ glitches[559]),
    .Q(instr_slti),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15548_
  (
    .D(_00240_ ^ glitches[560]),
    .Q(instr_sltiu),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15549_
  (
    .D(_00249_ ^ glitches[561]),
    .Q(instr_xori),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15550_
  (
    .D(_00229_ ^ glitches[562]),
    .Q(instr_ori),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15551_
  (
    .D(_00212_ ^ glitches[563]),
    .Q(instr_andi),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15552_
  (
    .D(_00237_ ^ glitches[564]),
    .Q(instr_slli),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15553_
  (
    .D(_00245_ ^ glitches[565]),
    .Q(instr_srli),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15554_
  (
    .D(_00243_ ^ glitches[566]),
    .Q(instr_srai),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15555_
  (
    .D(_00209_ ^ glitches[567]),
    .Q(instr_add),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15556_
  (
    .D(_00246_ ^ glitches[568]),
    .Q(instr_sub),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15557_
  (
    .D(_00236_ ^ glitches[569]),
    .Q(instr_sll),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15558_
  (
    .D(_00238_ ^ glitches[570]),
    .Q(instr_slt),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15559_
  (
    .D(_00241_ ^ glitches[571]),
    .Q(instr_sltu),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15560_
  (
    .D(_00248_ ^ glitches[572]),
    .Q(instr_xor),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15561_
  (
    .D(_00244_ ^ glitches[573]),
    .Q(instr_srl),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15562_
  (
    .D(_00242_ ^ glitches[574]),
    .Q(instr_sra),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15563_
  (
    .D(_00228_ ^ glitches[575]),
    .Q(instr_or),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15564_
  (
    .D(_00211_ ^ glitches[576]),
    .Q(instr_and),
    .CLK(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15565_
  (
    .D(_00230_ ^ glitches[577]),
    .Q(instr_rdcycle),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15566_
  (
    .D(_00231_ ^ glitches[578]),
    .Q(instr_rdcycleh),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15567_
  (
    .D(_00232_ ^ glitches[579]),
    .Q(instr_rdinstr),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15568_
  (
    .D(_00233_ ^ glitches[580]),
    .Q(instr_rdinstrh),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15569_
  (
    .D(_00192_ ^ glitches[581]),
    .Q(\decoded_rd[0] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15570_
  (
    .D(_00193_ ^ glitches[582]),
    .Q(\decoded_rd[1] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15571_
  (
    .D(_00194_ ^ glitches[583]),
    .Q(\decoded_rd[2] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15572_
  (
    .D(_00195_ ^ glitches[584]),
    .Q(\decoded_rd[3] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15573_
  (
    .D(_00196_ ^ glitches[585]),
    .Q(\decoded_rd[4] ),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15574_
  (
    .D(_00128_ ^ glitches[586]),
    .Q(\decoded_imm[0] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15575_
  (
    .D(_00139_ ^ glitches[587]),
    .Q(\decoded_imm[1] ),
    .CLK(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15576_
  (
    .D(_00150_ ^ glitches[588]),
    .Q(\decoded_imm[2] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15577_
  (
    .D(_00153_ ^ glitches[589]),
    .Q(\decoded_imm[3] ),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15578_
  (
    .D(_00154_ ^ glitches[590]),
    .Q(\decoded_imm[4] ),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15579_
  (
    .D(_00155_ ^ glitches[591]),
    .Q(\decoded_imm[5] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15580_
  (
    .D(_00156_ ^ glitches[592]),
    .Q(\decoded_imm[6] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15581_
  (
    .D(_00157_ ^ glitches[593]),
    .Q(\decoded_imm[7] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15582_
  (
    .D(_00158_ ^ glitches[594]),
    .Q(\decoded_imm[8] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15583_
  (
    .D(_00159_ ^ glitches[595]),
    .Q(\decoded_imm[9] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15584_
  (
    .D(_00129_ ^ glitches[596]),
    .Q(\decoded_imm[10] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15585_
  (
    .D(_00130_ ^ glitches[597]),
    .Q(\decoded_imm[11] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15586_
  (
    .D(_00131_ ^ glitches[598]),
    .Q(\decoded_imm[12] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15587_
  (
    .D(_00132_ ^ glitches[599]),
    .Q(\decoded_imm[13] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15588_
  (
    .D(_00133_ ^ glitches[600]),
    .Q(\decoded_imm[14] ),
    .CLK(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15589_
  (
    .D(_00134_ ^ glitches[601]),
    .Q(\decoded_imm[15] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15590_
  (
    .D(_00135_ ^ glitches[602]),
    .Q(\decoded_imm[16] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15591_
  (
    .D(_00136_ ^ glitches[603]),
    .Q(\decoded_imm[17] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15592_
  (
    .D(_00137_ ^ glitches[604]),
    .Q(\decoded_imm[18] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15593_
  (
    .D(_00138_ ^ glitches[605]),
    .Q(\decoded_imm[19] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15594_
  (
    .D(_00140_ ^ glitches[606]),
    .Q(\decoded_imm[20] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15595_
  (
    .D(_00141_ ^ glitches[607]),
    .Q(\decoded_imm[21] ),
    .CLK(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15596_
  (
    .D(_00142_ ^ glitches[608]),
    .Q(\decoded_imm[22] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15597_
  (
    .D(_00143_ ^ glitches[609]),
    .Q(\decoded_imm[23] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15598_
  (
    .D(_00144_ ^ glitches[610]),
    .Q(\decoded_imm[24] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15599_
  (
    .D(_00145_ ^ glitches[611]),
    .Q(\decoded_imm[25] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15600_
  (
    .D(_00146_ ^ glitches[612]),
    .Q(\decoded_imm[26] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15601_
  (
    .D(_00147_ ^ glitches[613]),
    .Q(\decoded_imm[27] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15602_
  (
    .D(_00148_ ^ glitches[614]),
    .Q(\decoded_imm[28] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15603_
  (
    .D(_00149_ ^ glitches[615]),
    .Q(\decoded_imm[29] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15604_
  (
    .D(_00151_ ^ glitches[616]),
    .Q(\decoded_imm[30] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15605_
  (
    .D(_00152_ ^ glitches[617]),
    .Q(\decoded_imm[31] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15606_
  (
    .D(_00160_ ^ glitches[618]),
    .Q(\decoded_imm_j[0] ),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15607_
  (
    .D(_00171_ ^ glitches[619]),
    .Q(\decoded_imm_j[1] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15608_
  (
    .D(_00182_ ^ glitches[620]),
    .Q(\decoded_imm_j[2] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15609_
  (
    .D(_00185_ ^ glitches[621]),
    .Q(\decoded_imm_j[3] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15610_
  (
    .D(_00186_ ^ glitches[622]),
    .Q(\decoded_imm_j[4] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15611_
  (
    .D(_00187_ ^ glitches[623]),
    .Q(\decoded_imm_j[5] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15612_
  (
    .D(_00188_ ^ glitches[624]),
    .Q(\decoded_imm_j[6] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15613_
  (
    .D(_00189_ ^ glitches[625]),
    .Q(\decoded_imm_j[7] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15614_
  (
    .D(_00190_ ^ glitches[626]),
    .Q(\decoded_imm_j[8] ),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15615_
  (
    .D(_00191_ ^ glitches[627]),
    .Q(\decoded_imm_j[9] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15616_
  (
    .D(_00161_ ^ glitches[628]),
    .Q(\decoded_imm_j[10] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15617_
  (
    .D(_00162_ ^ glitches[629]),
    .Q(\decoded_imm_j[11] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15618_
  (
    .D(_00163_ ^ glitches[630]),
    .Q(\decoded_imm_j[12] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15619_
  (
    .D(_00164_ ^ glitches[631]),
    .Q(\decoded_imm_j[13] ),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15620_
  (
    .D(_00165_ ^ glitches[632]),
    .Q(\decoded_imm_j[14] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15621_
  (
    .D(_00166_ ^ glitches[633]),
    .Q(\decoded_imm_j[15] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15622_
  (
    .D(_00167_ ^ glitches[634]),
    .Q(\decoded_imm_j[16] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15623_
  (
    .D(_00168_ ^ glitches[635]),
    .Q(\decoded_imm_j[17] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15624_
  (
    .D(_00169_ ^ glitches[636]),
    .Q(\decoded_imm_j[18] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15625_
  (
    .D(_00170_ ^ glitches[637]),
    .Q(\decoded_imm_j[19] ),
    .CLK(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15626_
  (
    .D(_00172_ ^ glitches[638]),
    .Q(\decoded_imm_j[20] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15627_
  (
    .D(_00173_ ^ glitches[639]),
    .Q(\decoded_imm_j[21] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15628_
  (
    .D(_00174_ ^ glitches[640]),
    .Q(\decoded_imm_j[22] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15629_
  (
    .D(_00175_ ^ glitches[641]),
    .Q(\decoded_imm_j[23] ),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15630_
  (
    .D(_00176_ ^ glitches[642]),
    .Q(\decoded_imm_j[24] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15631_
  (
    .D(_00177_ ^ glitches[643]),
    .Q(\decoded_imm_j[25] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15632_
  (
    .D(_00178_ ^ glitches[644]),
    .Q(\decoded_imm_j[26] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15633_
  (
    .D(_00179_ ^ glitches[645]),
    .Q(\decoded_imm_j[27] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15634_
  (
    .D(_00180_ ^ glitches[646]),
    .Q(\decoded_imm_j[28] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15635_
  (
    .D(_00181_ ^ glitches[647]),
    .Q(\decoded_imm_j[29] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15636_
  (
    .D(_00183_ ^ glitches[648]),
    .Q(\decoded_imm_j[30] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15637_
  (
    .D(_00184_ ^ glitches[649]),
    .Q(\decoded_imm_j[31] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15638_
  (
    .D(_00256_ ^ glitches[650]),
    .Q(is_lui_auipc_jal),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15639_
  (
    .D(_00255_ ^ glitches[651]),
    .Q(is_lb_lh_lw_lbu_lhu),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15640_
  (
    .D(_00259_ ^ glitches[652]),
    .Q(is_slli_srli_srai),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15641_
  (
    .D(_00254_ ^ glitches[653]),
    .Q(is_jalr_addi_slti_sltiu_xori_ori_andi),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15642_
  (
    .D(_00257_ ^ glitches[654]),
    .Q(is_sb_sh_sw),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15643_
  (
    .D(_00258_ ^ glitches[655]),
    .Q(is_sll_srl_sra),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15644_
  (
    .D(_00260_ ^ glitches[656]),
    .Q(is_slti_blt_slt),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15645_
  (
    .D(_00261_ ^ glitches[657]),
    .Q(is_sltiu_bltu_sltu),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15646_
  (
    .D(_00252_ ^ glitches[658]),
    .Q(is_beq_bne_blt_bge_bltu_bgeu),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15647_
  (
    .D(_00250_ ^ glitches[659]),
    .Q(is_alu_reg_imm),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15648_
  (
    .D(_00251_ ^ glitches[660]),
    .Q(is_alu_reg_reg),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15649_
  (
    .D(_00253_ ^ glitches[661]),
    .Q(is_compare),
    .CLK(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15650_
  (
    .D(_00312_ ^ glitches[662]),
    .Q(mem_valid),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15651_
  (
    .D(_00309_ ^ glitches[663]),
    .Q(mem_instr),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15652_
  (
    .D(_00273_ ^ glitches[664]),
    .Q(mem_addr[0]),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15653_
  (
    .D(_00284_ ^ glitches[665]),
    .Q(mem_addr[1]),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15654_
  (
    .D(_00295_ ^ glitches[666]),
    .Q(mem_addr[2]),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15655_
  (
    .D(_00298_ ^ glitches[667]),
    .Q(mem_addr[3]),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15656_
  (
    .D(_00299_ ^ glitches[668]),
    .Q(mem_addr[4]),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15657_
  (
    .D(_00300_ ^ glitches[669]),
    .Q(mem_addr[5]),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15658_
  (
    .D(_00301_ ^ glitches[670]),
    .Q(mem_addr[6]),
    .CLK(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15659_
  (
    .D(_00302_ ^ glitches[671]),
    .Q(mem_addr[7]),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15660_
  (
    .D(_00303_ ^ glitches[672]),
    .Q(mem_addr[8]),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15661_
  (
    .D(_00304_ ^ glitches[673]),
    .Q(mem_addr[9]),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15662_
  (
    .D(_00274_ ^ glitches[674]),
    .Q(mem_addr[10]),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15663_
  (
    .D(_00275_ ^ glitches[675]),
    .Q(mem_addr[11]),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15664_
  (
    .D(_00276_ ^ glitches[676]),
    .Q(mem_addr[12]),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15665_
  (
    .D(_00277_ ^ glitches[677]),
    .Q(mem_addr[13]),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15666_
  (
    .D(_00278_ ^ glitches[678]),
    .Q(mem_addr[14]),
    .CLK(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15667_
  (
    .D(_00279_ ^ glitches[679]),
    .Q(mem_addr[15]),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15668_
  (
    .D(_00280_ ^ glitches[680]),
    .Q(mem_addr[16]),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15669_
  (
    .D(_00281_ ^ glitches[681]),
    .Q(mem_addr[17]),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15670_
  (
    .D(_00282_ ^ glitches[682]),
    .Q(mem_addr[18]),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15671_
  (
    .D(_00283_ ^ glitches[683]),
    .Q(mem_addr[19]),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15672_
  (
    .D(_00285_ ^ glitches[684]),
    .Q(mem_addr[20]),
    .CLK(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15673_
  (
    .D(_00286_ ^ glitches[685]),
    .Q(mem_addr[21]),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15674_
  (
    .D(_00287_ ^ glitches[686]),
    .Q(mem_addr[22]),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15675_
  (
    .D(_00288_ ^ glitches[687]),
    .Q(mem_addr[23]),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15676_
  (
    .D(_00289_ ^ glitches[688]),
    .Q(mem_addr[24]),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15677_
  (
    .D(_00290_ ^ glitches[689]),
    .Q(mem_addr[25]),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15678_
  (
    .D(_00291_ ^ glitches[690]),
    .Q(mem_addr[26]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15679_
  (
    .D(_00292_ ^ glitches[691]),
    .Q(mem_addr[27]),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15680_
  (
    .D(_00293_ ^ glitches[692]),
    .Q(mem_addr[28]),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15681_
  (
    .D(_00294_ ^ glitches[693]),
    .Q(mem_addr[29]),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15682_
  (
    .D(_00296_ ^ glitches[694]),
    .Q(mem_addr[30]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15683_
  (
    .D(_00297_ ^ glitches[695]),
    .Q(mem_addr[31]),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15684_
  (
    .D(_00313_ ^ glitches[696]),
    .Q(mem_wdata[0]),
    .CLK(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15685_
  (
    .D(_00324_ ^ glitches[697]),
    .Q(mem_wdata[1]),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15686_
  (
    .D(_00335_ ^ glitches[698]),
    .Q(mem_wdata[2]),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15687_
  (
    .D(_00338_ ^ glitches[699]),
    .Q(mem_wdata[3]),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15688_
  (
    .D(_00339_ ^ glitches[700]),
    .Q(mem_wdata[4]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15689_
  (
    .D(_00340_ ^ glitches[701]),
    .Q(mem_wdata[5]),
    .CLK(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15690_
  (
    .D(_00341_ ^ glitches[702]),
    .Q(mem_wdata[6]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15691_
  (
    .D(_00342_ ^ glitches[703]),
    .Q(mem_wdata[7]),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15692_
  (
    .D(_00343_ ^ glitches[704]),
    .Q(mem_wdata[8]),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15693_
  (
    .D(_00344_ ^ glitches[705]),
    .Q(mem_wdata[9]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15694_
  (
    .D(_00314_ ^ glitches[706]),
    .Q(mem_wdata[10]),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15695_
  (
    .D(_00315_ ^ glitches[707]),
    .Q(mem_wdata[11]),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15696_
  (
    .D(_00316_ ^ glitches[708]),
    .Q(mem_wdata[12]),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15697_
  (
    .D(_00317_ ^ glitches[709]),
    .Q(mem_wdata[13]),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15698_
  (
    .D(_00318_ ^ glitches[710]),
    .Q(mem_wdata[14]),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15699_
  (
    .D(_00319_ ^ glitches[711]),
    .Q(mem_wdata[15]),
    .CLK(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15700_
  (
    .D(_00320_ ^ glitches[712]),
    .Q(mem_wdata[16]),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15701_
  (
    .D(_00321_ ^ glitches[713]),
    .Q(mem_wdata[17]),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15702_
  (
    .D(_00322_ ^ glitches[714]),
    .Q(mem_wdata[18]),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15703_
  (
    .D(_00323_ ^ glitches[715]),
    .Q(mem_wdata[19]),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15704_
  (
    .D(_00325_ ^ glitches[716]),
    .Q(mem_wdata[20]),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15705_
  (
    .D(_00326_ ^ glitches[717]),
    .Q(mem_wdata[21]),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15706_
  (
    .D(_00327_ ^ glitches[718]),
    .Q(mem_wdata[22]),
    .CLK(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15707_
  (
    .D(_00328_ ^ glitches[719]),
    .Q(mem_wdata[23]),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15708_
  (
    .D(_00329_ ^ glitches[720]),
    .Q(mem_wdata[24]),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15709_
  (
    .D(_00330_ ^ glitches[721]),
    .Q(mem_wdata[25]),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15710_
  (
    .D(_00331_ ^ glitches[722]),
    .Q(mem_wdata[26]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15711_
  (
    .D(_00332_ ^ glitches[723]),
    .Q(mem_wdata[27]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15712_
  (
    .D(_00333_ ^ glitches[724]),
    .Q(mem_wdata[28]),
    .CLK(clknet_7_127_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15713_
  (
    .D(_00334_ ^ glitches[725]),
    .Q(mem_wdata[29]),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15714_
  (
    .D(_00336_ ^ glitches[726]),
    .Q(mem_wdata[30]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15715_
  (
    .D(_00337_ ^ glitches[727]),
    .Q(mem_wdata[31]),
    .CLK(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15716_
  (
    .D(_00345_ ^ glitches[728]),
    .Q(mem_wstrb[0]),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15717_
  (
    .D(_00346_ ^ glitches[729]),
    .Q(mem_wstrb[1]),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15718_
  (
    .D(_00347_ ^ glitches[730]),
    .Q(mem_wstrb[2]),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15719_
  (
    .D(_00348_ ^ glitches[731]),
    .Q(mem_wstrb[3]),
    .CLK(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15720_
  (
    .D(_00310_ ^ glitches[732]),
    .Q(\mem_state[0] ),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15721_
  (
    .D(_00311_ ^ glitches[733]),
    .Q(\mem_state[1] ),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15722_
  (
    .D(\mem_rdata_latched[0]  ^ glitches[734]),
    .Q(\mem_rdata_q[0] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15723_
  (
    .D(\mem_rdata_latched[1]  ^ glitches[735]),
    .Q(\mem_rdata_q[1] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15724_
  (
    .D(\mem_rdata_latched[2]  ^ glitches[736]),
    .Q(\mem_rdata_q[2] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15725_
  (
    .D(\mem_rdata_latched[3]  ^ glitches[737]),
    .Q(\mem_rdata_q[3] ),
    .CLK(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15726_
  (
    .D(\mem_rdata_latched[4]  ^ glitches[738]),
    .Q(\mem_rdata_q[4] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15727_
  (
    .D(\mem_rdata_latched[5]  ^ glitches[739]),
    .Q(\mem_rdata_q[5] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15728_
  (
    .D(\mem_rdata_latched[6]  ^ glitches[740]),
    .Q(\mem_rdata_q[6] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15729_
  (
    .D(\mem_rdata_latched[7]  ^ glitches[741]),
    .Q(\mem_rdata_q[7] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15730_
  (
    .D(\mem_rdata_latched[8]  ^ glitches[742]),
    .Q(\mem_rdata_q[8] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15731_
  (
    .D(\mem_rdata_latched[9]  ^ glitches[743]),
    .Q(\mem_rdata_q[9] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15732_
  (
    .D(\mem_rdata_latched[10]  ^ glitches[744]),
    .Q(\mem_rdata_q[10] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15733_
  (
    .D(\mem_rdata_latched[11]  ^ glitches[745]),
    .Q(\mem_rdata_q[11] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15734_
  (
    .D(\mem_rdata_latched[12]  ^ glitches[746]),
    .Q(\mem_rdata_q[12] ),
    .CLK(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15735_
  (
    .D(\mem_rdata_latched[13]  ^ glitches[747]),
    .Q(\mem_rdata_q[13] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15736_
  (
    .D(\mem_rdata_latched[14]  ^ glitches[748]),
    .Q(\mem_rdata_q[14] ),
    .CLK(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15737_
  (
    .D(\mem_rdata_latched[15]  ^ glitches[749]),
    .Q(\mem_rdata_q[15] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15738_
  (
    .D(\mem_rdata_latched[16]  ^ glitches[750]),
    .Q(\mem_rdata_q[16] ),
    .CLK(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15739_
  (
    .D(\mem_rdata_latched[17]  ^ glitches[751]),
    .Q(\mem_rdata_q[17] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15740_
  (
    .D(\mem_rdata_latched[18]  ^ glitches[752]),
    .Q(\mem_rdata_q[18] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15741_
  (
    .D(\mem_rdata_latched[19]  ^ glitches[753]),
    .Q(\mem_rdata_q[19] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15742_
  (
    .D(\mem_rdata_latched[20]  ^ glitches[754]),
    .Q(\mem_rdata_q[20] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15743_
  (
    .D(\mem_rdata_latched[21]  ^ glitches[755]),
    .Q(\mem_rdata_q[21] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15744_
  (
    .D(\mem_rdata_latched[22]  ^ glitches[756]),
    .Q(\mem_rdata_q[22] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15745_
  (
    .D(\mem_rdata_latched[23]  ^ glitches[757]),
    .Q(\mem_rdata_q[23] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15746_
  (
    .D(\mem_rdata_latched[24]  ^ glitches[758]),
    .Q(\mem_rdata_q[24] ),
    .CLK(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15747_
  (
    .D(\mem_rdata_latched[25]  ^ glitches[759]),
    .Q(\mem_rdata_q[25] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15748_
  (
    .D(\mem_rdata_latched[26]  ^ glitches[760]),
    .Q(\mem_rdata_q[26] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15749_
  (
    .D(\mem_rdata_latched[27]  ^ glitches[761]),
    .Q(\mem_rdata_q[27] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15750_
  (
    .D(\mem_rdata_latched[28]  ^ glitches[762]),
    .Q(\mem_rdata_q[28] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15751_
  (
    .D(\mem_rdata_latched[29]  ^ glitches[763]),
    .Q(\mem_rdata_q[29] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15752_
  (
    .D(\mem_rdata_latched[30]  ^ glitches[764]),
    .Q(\mem_rdata_q[30] ),
    .CLK(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15753_
  (
    .D(\mem_rdata_latched[31]  ^ glitches[765]),
    .Q(\mem_rdata_q[31] ),
    .CLK(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15754_
  (
    .D(_00557_ ^ glitches[766]),
    .Q(\cpuregs[10][0] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15755_
  (
    .D(_00568_ ^ glitches[767]),
    .Q(\cpuregs[10][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15756_
  (
    .D(_00579_ ^ glitches[768]),
    .Q(\cpuregs[10][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15757_
  (
    .D(_00582_ ^ glitches[769]),
    .Q(\cpuregs[10][3] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15758_
  (
    .D(_00583_ ^ glitches[770]),
    .Q(\cpuregs[10][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15759_
  (
    .D(_00584_ ^ glitches[771]),
    .Q(\cpuregs[10][5] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15760_
  (
    .D(_00585_ ^ glitches[772]),
    .Q(\cpuregs[10][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15761_
  (
    .D(_00586_ ^ glitches[773]),
    .Q(\cpuregs[10][7] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15762_
  (
    .D(_00587_ ^ glitches[774]),
    .Q(\cpuregs[10][8] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15763_
  (
    .D(_00588_ ^ glitches[775]),
    .Q(\cpuregs[10][9] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15764_
  (
    .D(_00558_ ^ glitches[776]),
    .Q(\cpuregs[10][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15765_
  (
    .D(_00559_ ^ glitches[777]),
    .Q(\cpuregs[10][11] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15766_
  (
    .D(_00560_ ^ glitches[778]),
    .Q(\cpuregs[10][12] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15767_
  (
    .D(_00561_ ^ glitches[779]),
    .Q(\cpuregs[10][13] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15768_
  (
    .D(_00562_ ^ glitches[780]),
    .Q(\cpuregs[10][14] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15769_
  (
    .D(_00563_ ^ glitches[781]),
    .Q(\cpuregs[10][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15770_
  (
    .D(_00564_ ^ glitches[782]),
    .Q(\cpuregs[10][16] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15771_
  (
    .D(_00565_ ^ glitches[783]),
    .Q(\cpuregs[10][17] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15772_
  (
    .D(_00566_ ^ glitches[784]),
    .Q(\cpuregs[10][18] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15773_
  (
    .D(_00567_ ^ glitches[785]),
    .Q(\cpuregs[10][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15774_
  (
    .D(_00569_ ^ glitches[786]),
    .Q(\cpuregs[10][20] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15775_
  (
    .D(_00570_ ^ glitches[787]),
    .Q(\cpuregs[10][21] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15776_
  (
    .D(_00571_ ^ glitches[788]),
    .Q(\cpuregs[10][22] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15777_
  (
    .D(_00572_ ^ glitches[789]),
    .Q(\cpuregs[10][23] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15778_
  (
    .D(_00573_ ^ glitches[790]),
    .Q(\cpuregs[10][24] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15779_
  (
    .D(_00574_ ^ glitches[791]),
    .Q(\cpuregs[10][25] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15780_
  (
    .D(_00575_ ^ glitches[792]),
    .Q(\cpuregs[10][26] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15781_
  (
    .D(_00576_ ^ glitches[793]),
    .Q(\cpuregs[10][27] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15782_
  (
    .D(_00577_ ^ glitches[794]),
    .Q(\cpuregs[10][28] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15783_
  (
    .D(_00578_ ^ glitches[795]),
    .Q(\cpuregs[10][29] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15784_
  (
    .D(_00580_ ^ glitches[796]),
    .Q(\cpuregs[10][30] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15785_
  (
    .D(_00581_ ^ glitches[797]),
    .Q(\cpuregs[10][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15786_
  (
    .D(_01229_ ^ glitches[798]),
    .Q(\cpuregs[2][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15787_
  (
    .D(_01240_ ^ glitches[799]),
    .Q(\cpuregs[2][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15788_
  (
    .D(_01251_ ^ glitches[800]),
    .Q(\cpuregs[2][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15789_
  (
    .D(_01254_ ^ glitches[801]),
    .Q(\cpuregs[2][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15790_
  (
    .D(_01255_ ^ glitches[802]),
    .Q(\cpuregs[2][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15791_
  (
    .D(_01256_ ^ glitches[803]),
    .Q(\cpuregs[2][5] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15792_
  (
    .D(_01257_ ^ glitches[804]),
    .Q(\cpuregs[2][6] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15793_
  (
    .D(_01258_ ^ glitches[805]),
    .Q(\cpuregs[2][7] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15794_
  (
    .D(_01259_ ^ glitches[806]),
    .Q(\cpuregs[2][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15795_
  (
    .D(_01260_ ^ glitches[807]),
    .Q(\cpuregs[2][9] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15796_
  (
    .D(_01230_ ^ glitches[808]),
    .Q(\cpuregs[2][10] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15797_
  (
    .D(_01231_ ^ glitches[809]),
    .Q(\cpuregs[2][11] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15798_
  (
    .D(_01232_ ^ glitches[810]),
    .Q(\cpuregs[2][12] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15799_
  (
    .D(_01233_ ^ glitches[811]),
    .Q(\cpuregs[2][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15800_
  (
    .D(_01234_ ^ glitches[812]),
    .Q(\cpuregs[2][14] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15801_
  (
    .D(_01235_ ^ glitches[813]),
    .Q(\cpuregs[2][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15802_
  (
    .D(_01236_ ^ glitches[814]),
    .Q(\cpuregs[2][16] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15803_
  (
    .D(_01237_ ^ glitches[815]),
    .Q(\cpuregs[2][17] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15804_
  (
    .D(_01238_ ^ glitches[816]),
    .Q(\cpuregs[2][18] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15805_
  (
    .D(_01239_ ^ glitches[817]),
    .Q(\cpuregs[2][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15806_
  (
    .D(_01241_ ^ glitches[818]),
    .Q(\cpuregs[2][20] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15807_
  (
    .D(_01242_ ^ glitches[819]),
    .Q(\cpuregs[2][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15808_
  (
    .D(_01243_ ^ glitches[820]),
    .Q(\cpuregs[2][22] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15809_
  (
    .D(_01244_ ^ glitches[821]),
    .Q(\cpuregs[2][23] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15810_
  (
    .D(_01245_ ^ glitches[822]),
    .Q(\cpuregs[2][24] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15811_
  (
    .D(_01246_ ^ glitches[823]),
    .Q(\cpuregs[2][25] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15812_
  (
    .D(_01247_ ^ glitches[824]),
    .Q(\cpuregs[2][26] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15813_
  (
    .D(_01248_ ^ glitches[825]),
    .Q(\cpuregs[2][27] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15814_
  (
    .D(_01249_ ^ glitches[826]),
    .Q(\cpuregs[2][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15815_
  (
    .D(_01250_ ^ glitches[827]),
    .Q(\cpuregs[2][29] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15816_
  (
    .D(_01252_ ^ glitches[828]),
    .Q(\cpuregs[2][30] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15817_
  (
    .D(_01253_ ^ glitches[829]),
    .Q(\cpuregs[2][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15818_
  (
    .D(_01517_ ^ glitches[830]),
    .Q(\cpuregs[9][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15819_
  (
    .D(_01528_ ^ glitches[831]),
    .Q(\cpuregs[9][1] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15820_
  (
    .D(_01539_ ^ glitches[832]),
    .Q(\cpuregs[9][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15821_
  (
    .D(_01542_ ^ glitches[833]),
    .Q(\cpuregs[9][3] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15822_
  (
    .D(_01543_ ^ glitches[834]),
    .Q(\cpuregs[9][4] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15823_
  (
    .D(_01544_ ^ glitches[835]),
    .Q(\cpuregs[9][5] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15824_
  (
    .D(_01545_ ^ glitches[836]),
    .Q(\cpuregs[9][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15825_
  (
    .D(_01546_ ^ glitches[837]),
    .Q(\cpuregs[9][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15826_
  (
    .D(_01547_ ^ glitches[838]),
    .Q(\cpuregs[9][8] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15827_
  (
    .D(_01548_ ^ glitches[839]),
    .Q(\cpuregs[9][9] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15828_
  (
    .D(_01518_ ^ glitches[840]),
    .Q(\cpuregs[9][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15829_
  (
    .D(_01519_ ^ glitches[841]),
    .Q(\cpuregs[9][11] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15830_
  (
    .D(_01520_ ^ glitches[842]),
    .Q(\cpuregs[9][12] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15831_
  (
    .D(_01521_ ^ glitches[843]),
    .Q(\cpuregs[9][13] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15832_
  (
    .D(_01522_ ^ glitches[844]),
    .Q(\cpuregs[9][14] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15833_
  (
    .D(_01523_ ^ glitches[845]),
    .Q(\cpuregs[9][15] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15834_
  (
    .D(_01524_ ^ glitches[846]),
    .Q(\cpuregs[9][16] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15835_
  (
    .D(_01525_ ^ glitches[847]),
    .Q(\cpuregs[9][17] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15836_
  (
    .D(_01526_ ^ glitches[848]),
    .Q(\cpuregs[9][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15837_
  (
    .D(_01527_ ^ glitches[849]),
    .Q(\cpuregs[9][19] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15838_
  (
    .D(_01529_ ^ glitches[850]),
    .Q(\cpuregs[9][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15839_
  (
    .D(_01530_ ^ glitches[851]),
    .Q(\cpuregs[9][21] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15840_
  (
    .D(_01531_ ^ glitches[852]),
    .Q(\cpuregs[9][22] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15841_
  (
    .D(_01532_ ^ glitches[853]),
    .Q(\cpuregs[9][23] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15842_
  (
    .D(_01533_ ^ glitches[854]),
    .Q(\cpuregs[9][24] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15843_
  (
    .D(_01534_ ^ glitches[855]),
    .Q(\cpuregs[9][25] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15844_
  (
    .D(_01535_ ^ glitches[856]),
    .Q(\cpuregs[9][26] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15845_
  (
    .D(_01536_ ^ glitches[857]),
    .Q(\cpuregs[9][27] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15846_
  (
    .D(_01537_ ^ glitches[858]),
    .Q(\cpuregs[9][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15847_
  (
    .D(_01538_ ^ glitches[859]),
    .Q(\cpuregs[9][29] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15848_
  (
    .D(_01540_ ^ glitches[860]),
    .Q(\cpuregs[9][30] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15849_
  (
    .D(_01541_ ^ glitches[861]),
    .Q(\cpuregs[9][31] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15850_
  (
    .D(_00749_ ^ glitches[862]),
    .Q(\cpuregs[16][0] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15851_
  (
    .D(_00760_ ^ glitches[863]),
    .Q(\cpuregs[16][1] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15852_
  (
    .D(_00771_ ^ glitches[864]),
    .Q(\cpuregs[16][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15853_
  (
    .D(_00774_ ^ glitches[865]),
    .Q(\cpuregs[16][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15854_
  (
    .D(_00775_ ^ glitches[866]),
    .Q(\cpuregs[16][4] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15855_
  (
    .D(_00776_ ^ glitches[867]),
    .Q(\cpuregs[16][5] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15856_
  (
    .D(_00777_ ^ glitches[868]),
    .Q(\cpuregs[16][6] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15857_
  (
    .D(_00778_ ^ glitches[869]),
    .Q(\cpuregs[16][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15858_
  (
    .D(_00779_ ^ glitches[870]),
    .Q(\cpuregs[16][8] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15859_
  (
    .D(_00780_ ^ glitches[871]),
    .Q(\cpuregs[16][9] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15860_
  (
    .D(_00750_ ^ glitches[872]),
    .Q(\cpuregs[16][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15861_
  (
    .D(_00751_ ^ glitches[873]),
    .Q(\cpuregs[16][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15862_
  (
    .D(_00752_ ^ glitches[874]),
    .Q(\cpuregs[16][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15863_
  (
    .D(_00753_ ^ glitches[875]),
    .Q(\cpuregs[16][13] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15864_
  (
    .D(_00754_ ^ glitches[876]),
    .Q(\cpuregs[16][14] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15865_
  (
    .D(_00755_ ^ glitches[877]),
    .Q(\cpuregs[16][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15866_
  (
    .D(_00756_ ^ glitches[878]),
    .Q(\cpuregs[16][16] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15867_
  (
    .D(_00757_ ^ glitches[879]),
    .Q(\cpuregs[16][17] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15868_
  (
    .D(_00758_ ^ glitches[880]),
    .Q(\cpuregs[16][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15869_
  (
    .D(_00759_ ^ glitches[881]),
    .Q(\cpuregs[16][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15870_
  (
    .D(_00761_ ^ glitches[882]),
    .Q(\cpuregs[16][20] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15871_
  (
    .D(_00762_ ^ glitches[883]),
    .Q(\cpuregs[16][21] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15872_
  (
    .D(_00763_ ^ glitches[884]),
    .Q(\cpuregs[16][22] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15873_
  (
    .D(_00764_ ^ glitches[885]),
    .Q(\cpuregs[16][23] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15874_
  (
    .D(_00765_ ^ glitches[886]),
    .Q(\cpuregs[16][24] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15875_
  (
    .D(_00766_ ^ glitches[887]),
    .Q(\cpuregs[16][25] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15876_
  (
    .D(_00767_ ^ glitches[888]),
    .Q(\cpuregs[16][26] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15877_
  (
    .D(_00768_ ^ glitches[889]),
    .Q(\cpuregs[16][27] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15878_
  (
    .D(_00769_ ^ glitches[890]),
    .Q(\cpuregs[16][28] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15879_
  (
    .D(_00770_ ^ glitches[891]),
    .Q(\cpuregs[16][29] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15880_
  (
    .D(_00772_ ^ glitches[892]),
    .Q(\cpuregs[16][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15881_
  (
    .D(_00773_ ^ glitches[893]),
    .Q(\cpuregs[16][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15882_
  (
    .D(_01005_ ^ glitches[894]),
    .Q(\cpuregs[23][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15883_
  (
    .D(_01016_ ^ glitches[895]),
    .Q(\cpuregs[23][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15884_
  (
    .D(_01027_ ^ glitches[896]),
    .Q(\cpuregs[23][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15885_
  (
    .D(_01030_ ^ glitches[897]),
    .Q(\cpuregs[23][3] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15886_
  (
    .D(_01031_ ^ glitches[898]),
    .Q(\cpuregs[23][4] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15887_
  (
    .D(_01032_ ^ glitches[899]),
    .Q(\cpuregs[23][5] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15888_
  (
    .D(_01033_ ^ glitches[900]),
    .Q(\cpuregs[23][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15889_
  (
    .D(_01034_ ^ glitches[901]),
    .Q(\cpuregs[23][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15890_
  (
    .D(_01035_ ^ glitches[902]),
    .Q(\cpuregs[23][8] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15891_
  (
    .D(_01036_ ^ glitches[903]),
    .Q(\cpuregs[23][9] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15892_
  (
    .D(_01006_ ^ glitches[904]),
    .Q(\cpuregs[23][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15893_
  (
    .D(_01007_ ^ glitches[905]),
    .Q(\cpuregs[23][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15894_
  (
    .D(_01008_ ^ glitches[906]),
    .Q(\cpuregs[23][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15895_
  (
    .D(_01009_ ^ glitches[907]),
    .Q(\cpuregs[23][13] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15896_
  (
    .D(_01010_ ^ glitches[908]),
    .Q(\cpuregs[23][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15897_
  (
    .D(_01011_ ^ glitches[909]),
    .Q(\cpuregs[23][15] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15898_
  (
    .D(_01012_ ^ glitches[910]),
    .Q(\cpuregs[23][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15899_
  (
    .D(_01013_ ^ glitches[911]),
    .Q(\cpuregs[23][17] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15900_
  (
    .D(_01014_ ^ glitches[912]),
    .Q(\cpuregs[23][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15901_
  (
    .D(_01015_ ^ glitches[913]),
    .Q(\cpuregs[23][19] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15902_
  (
    .D(_01017_ ^ glitches[914]),
    .Q(\cpuregs[23][20] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15903_
  (
    .D(_01018_ ^ glitches[915]),
    .Q(\cpuregs[23][21] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15904_
  (
    .D(_01019_ ^ glitches[916]),
    .Q(\cpuregs[23][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15905_
  (
    .D(_01020_ ^ glitches[917]),
    .Q(\cpuregs[23][23] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15906_
  (
    .D(_01021_ ^ glitches[918]),
    .Q(\cpuregs[23][24] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15907_
  (
    .D(_01022_ ^ glitches[919]),
    .Q(\cpuregs[23][25] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15908_
  (
    .D(_01023_ ^ glitches[920]),
    .Q(\cpuregs[23][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15909_
  (
    .D(_01024_ ^ glitches[921]),
    .Q(\cpuregs[23][27] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15910_
  (
    .D(_01025_ ^ glitches[922]),
    .Q(\cpuregs[23][28] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15911_
  (
    .D(_01026_ ^ glitches[923]),
    .Q(\cpuregs[23][29] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15912_
  (
    .D(_01028_ ^ glitches[924]),
    .Q(\cpuregs[23][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15913_
  (
    .D(_01029_ ^ glitches[925]),
    .Q(\cpuregs[23][31] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15914_
  (
    .D(_00515_ ^ glitches[926]),
    .Q(\cpu_state[0] ),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15915_
  (
    .D(_00516_ ^ glitches[927]),
    .Q(\cpu_state[1] ),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15916_
  (
    .D(_00517_ ^ glitches[928]),
    .Q(\cpu_state[2] ),
    .CLK(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15917_
  (
    .D(_00518_ ^ glitches[929]),
    .Q(\cpu_state[3] ),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15918_
  (
    .D(_00519_ ^ glitches[930]),
    .Q(\cpu_state[4] ),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15919_
  (
    .D(_00520_ ^ glitches[931]),
    .Q(\cpu_state[5] ),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15920_
  (
    .D(_00521_ ^ glitches[932]),
    .Q(\cpu_state[6] ),
    .CLK(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15921_
  (
    .D(_01037_ ^ glitches[933]),
    .Q(\cpuregs[24][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15922_
  (
    .D(_01048_ ^ glitches[934]),
    .Q(\cpuregs[24][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15923_
  (
    .D(_01059_ ^ glitches[935]),
    .Q(\cpuregs[24][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15924_
  (
    .D(_01062_ ^ glitches[936]),
    .Q(\cpuregs[24][3] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15925_
  (
    .D(_01063_ ^ glitches[937]),
    .Q(\cpuregs[24][4] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15926_
  (
    .D(_01064_ ^ glitches[938]),
    .Q(\cpuregs[24][5] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15927_
  (
    .D(_01065_ ^ glitches[939]),
    .Q(\cpuregs[24][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15928_
  (
    .D(_01066_ ^ glitches[940]),
    .Q(\cpuregs[24][7] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15929_
  (
    .D(_01067_ ^ glitches[941]),
    .Q(\cpuregs[24][8] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15930_
  (
    .D(_01068_ ^ glitches[942]),
    .Q(\cpuregs[24][9] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15931_
  (
    .D(_01038_ ^ glitches[943]),
    .Q(\cpuregs[24][10] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15932_
  (
    .D(_01039_ ^ glitches[944]),
    .Q(\cpuregs[24][11] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15933_
  (
    .D(_01040_ ^ glitches[945]),
    .Q(\cpuregs[24][12] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15934_
  (
    .D(_01041_ ^ glitches[946]),
    .Q(\cpuregs[24][13] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15935_
  (
    .D(_01042_ ^ glitches[947]),
    .Q(\cpuregs[24][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15936_
  (
    .D(_01043_ ^ glitches[948]),
    .Q(\cpuregs[24][15] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15937_
  (
    .D(_01044_ ^ glitches[949]),
    .Q(\cpuregs[24][16] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15938_
  (
    .D(_01045_ ^ glitches[950]),
    .Q(\cpuregs[24][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15939_
  (
    .D(_01046_ ^ glitches[951]),
    .Q(\cpuregs[24][18] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15940_
  (
    .D(_01047_ ^ glitches[952]),
    .Q(\cpuregs[24][19] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15941_
  (
    .D(_01049_ ^ glitches[953]),
    .Q(\cpuregs[24][20] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15942_
  (
    .D(_01050_ ^ glitches[954]),
    .Q(\cpuregs[24][21] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15943_
  (
    .D(_01051_ ^ glitches[955]),
    .Q(\cpuregs[24][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15944_
  (
    .D(_01052_ ^ glitches[956]),
    .Q(\cpuregs[24][23] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15945_
  (
    .D(_01053_ ^ glitches[957]),
    .Q(\cpuregs[24][24] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15946_
  (
    .D(_01054_ ^ glitches[958]),
    .Q(\cpuregs[24][25] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15947_
  (
    .D(_01055_ ^ glitches[959]),
    .Q(\cpuregs[24][26] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15948_
  (
    .D(_01056_ ^ glitches[960]),
    .Q(\cpuregs[24][27] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15949_
  (
    .D(_01057_ ^ glitches[961]),
    .Q(\cpuregs[24][28] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15950_
  (
    .D(_01058_ ^ glitches[962]),
    .Q(\cpuregs[24][29] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15951_
  (
    .D(_01060_ ^ glitches[963]),
    .Q(\cpuregs[24][30] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15952_
  (
    .D(_01061_ ^ glitches[964]),
    .Q(\cpuregs[24][31] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15953_
  (
    .D(_01325_ ^ glitches[965]),
    .Q(\cpuregs[3][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15954_
  (
    .D(_01336_ ^ glitches[966]),
    .Q(\cpuregs[3][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15955_
  (
    .D(_01347_ ^ glitches[967]),
    .Q(\cpuregs[3][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15956_
  (
    .D(_01350_ ^ glitches[968]),
    .Q(\cpuregs[3][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15957_
  (
    .D(_01351_ ^ glitches[969]),
    .Q(\cpuregs[3][4] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15958_
  (
    .D(_01352_ ^ glitches[970]),
    .Q(\cpuregs[3][5] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15959_
  (
    .D(_01353_ ^ glitches[971]),
    .Q(\cpuregs[3][6] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15960_
  (
    .D(_01354_ ^ glitches[972]),
    .Q(\cpuregs[3][7] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15961_
  (
    .D(_01355_ ^ glitches[973]),
    .Q(\cpuregs[3][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15962_
  (
    .D(_01356_ ^ glitches[974]),
    .Q(\cpuregs[3][9] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15963_
  (
    .D(_01326_ ^ glitches[975]),
    .Q(\cpuregs[3][10] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15964_
  (
    .D(_01327_ ^ glitches[976]),
    .Q(\cpuregs[3][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15965_
  (
    .D(_01328_ ^ glitches[977]),
    .Q(\cpuregs[3][12] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15966_
  (
    .D(_01329_ ^ glitches[978]),
    .Q(\cpuregs[3][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15967_
  (
    .D(_01330_ ^ glitches[979]),
    .Q(\cpuregs[3][14] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15968_
  (
    .D(_01331_ ^ glitches[980]),
    .Q(\cpuregs[3][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15969_
  (
    .D(_01332_ ^ glitches[981]),
    .Q(\cpuregs[3][16] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15970_
  (
    .D(_01333_ ^ glitches[982]),
    .Q(\cpuregs[3][17] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15971_
  (
    .D(_01334_ ^ glitches[983]),
    .Q(\cpuregs[3][18] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15972_
  (
    .D(_01335_ ^ glitches[984]),
    .Q(\cpuregs[3][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15973_
  (
    .D(_01337_ ^ glitches[985]),
    .Q(\cpuregs[3][20] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15974_
  (
    .D(_01338_ ^ glitches[986]),
    .Q(\cpuregs[3][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15975_
  (
    .D(_01339_ ^ glitches[987]),
    .Q(\cpuregs[3][22] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15976_
  (
    .D(_01340_ ^ glitches[988]),
    .Q(\cpuregs[3][23] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15977_
  (
    .D(_01341_ ^ glitches[989]),
    .Q(\cpuregs[3][24] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15978_
  (
    .D(_01342_ ^ glitches[990]),
    .Q(\cpuregs[3][25] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15979_
  (
    .D(_01343_ ^ glitches[991]),
    .Q(\cpuregs[3][26] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15980_
  (
    .D(_01344_ ^ glitches[992]),
    .Q(\cpuregs[3][27] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15981_
  (
    .D(_01345_ ^ glitches[993]),
    .Q(\cpuregs[3][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15982_
  (
    .D(_01346_ ^ glitches[994]),
    .Q(\cpuregs[3][29] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15983_
  (
    .D(_01348_ ^ glitches[995]),
    .Q(\cpuregs[3][30] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15984_
  (
    .D(_01349_ ^ glitches[996]),
    .Q(\cpuregs[3][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15985_
  (
    .D(_01421_ ^ glitches[997]),
    .Q(\cpuregs[6][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15986_
  (
    .D(_01432_ ^ glitches[998]),
    .Q(\cpuregs[6][1] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15987_
  (
    .D(_01443_ ^ glitches[999]),
    .Q(\cpuregs[6][2] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15988_
  (
    .D(_01446_ ^ glitches[1000]),
    .Q(\cpuregs[6][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15989_
  (
    .D(_01447_ ^ glitches[1001]),
    .Q(\cpuregs[6][4] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15990_
  (
    .D(_01448_ ^ glitches[1002]),
    .Q(\cpuregs[6][5] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15991_
  (
    .D(_01449_ ^ glitches[1003]),
    .Q(\cpuregs[6][6] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15992_
  (
    .D(_01450_ ^ glitches[1004]),
    .Q(\cpuregs[6][7] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15993_
  (
    .D(_01451_ ^ glitches[1005]),
    .Q(\cpuregs[6][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15994_
  (
    .D(_01452_ ^ glitches[1006]),
    .Q(\cpuregs[6][9] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15995_
  (
    .D(_01422_ ^ glitches[1007]),
    .Q(\cpuregs[6][10] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15996_
  (
    .D(_01423_ ^ glitches[1008]),
    .Q(\cpuregs[6][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15997_
  (
    .D(_01424_ ^ glitches[1009]),
    .Q(\cpuregs[6][12] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15998_
  (
    .D(_01425_ ^ glitches[1010]),
    .Q(\cpuregs[6][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _15999_
  (
    .D(_01426_ ^ glitches[1011]),
    .Q(\cpuregs[6][14] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16000_
  (
    .D(_01427_ ^ glitches[1012]),
    .Q(\cpuregs[6][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16001_
  (
    .D(_01428_ ^ glitches[1013]),
    .Q(\cpuregs[6][16] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16002_
  (
    .D(_01429_ ^ glitches[1014]),
    .Q(\cpuregs[6][17] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16003_
  (
    .D(_01430_ ^ glitches[1015]),
    .Q(\cpuregs[6][18] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16004_
  (
    .D(_01431_ ^ glitches[1016]),
    .Q(\cpuregs[6][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16005_
  (
    .D(_01433_ ^ glitches[1017]),
    .Q(\cpuregs[6][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16006_
  (
    .D(_01434_ ^ glitches[1018]),
    .Q(\cpuregs[6][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16007_
  (
    .D(_01435_ ^ glitches[1019]),
    .Q(\cpuregs[6][22] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16008_
  (
    .D(_01436_ ^ glitches[1020]),
    .Q(\cpuregs[6][23] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16009_
  (
    .D(_01437_ ^ glitches[1021]),
    .Q(\cpuregs[6][24] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16010_
  (
    .D(_01438_ ^ glitches[1022]),
    .Q(\cpuregs[6][25] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16011_
  (
    .D(_01439_ ^ glitches[1023]),
    .Q(\cpuregs[6][26] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16012_
  (
    .D(_01440_ ^ glitches[1024]),
    .Q(\cpuregs[6][27] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16013_
  (
    .D(_01441_ ^ glitches[1025]),
    .Q(\cpuregs[6][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16014_
  (
    .D(_01442_ ^ glitches[1026]),
    .Q(\cpuregs[6][29] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16015_
  (
    .D(_01444_ ^ glitches[1027]),
    .Q(\cpuregs[6][30] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16016_
  (
    .D(_01445_ ^ glitches[1028]),
    .Q(\cpuregs[6][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16017_
  (
    .D(_01069_ ^ glitches[1029]),
    .Q(\cpuregs[25][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16018_
  (
    .D(_01080_ ^ glitches[1030]),
    .Q(\cpuregs[25][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16019_
  (
    .D(_01091_ ^ glitches[1031]),
    .Q(\cpuregs[25][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16020_
  (
    .D(_01094_ ^ glitches[1032]),
    .Q(\cpuregs[25][3] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16021_
  (
    .D(_01095_ ^ glitches[1033]),
    .Q(\cpuregs[25][4] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16022_
  (
    .D(_01096_ ^ glitches[1034]),
    .Q(\cpuregs[25][5] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16023_
  (
    .D(_01097_ ^ glitches[1035]),
    .Q(\cpuregs[25][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16024_
  (
    .D(_01098_ ^ glitches[1036]),
    .Q(\cpuregs[25][7] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16025_
  (
    .D(_01099_ ^ glitches[1037]),
    .Q(\cpuregs[25][8] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16026_
  (
    .D(_01100_ ^ glitches[1038]),
    .Q(\cpuregs[25][9] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16027_
  (
    .D(_01070_ ^ glitches[1039]),
    .Q(\cpuregs[25][10] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16028_
  (
    .D(_01071_ ^ glitches[1040]),
    .Q(\cpuregs[25][11] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16029_
  (
    .D(_01072_ ^ glitches[1041]),
    .Q(\cpuregs[25][12] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16030_
  (
    .D(_01073_ ^ glitches[1042]),
    .Q(\cpuregs[25][13] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16031_
  (
    .D(_01074_ ^ glitches[1043]),
    .Q(\cpuregs[25][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16032_
  (
    .D(_01075_ ^ glitches[1044]),
    .Q(\cpuregs[25][15] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16033_
  (
    .D(_01076_ ^ glitches[1045]),
    .Q(\cpuregs[25][16] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16034_
  (
    .D(_01077_ ^ glitches[1046]),
    .Q(\cpuregs[25][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16035_
  (
    .D(_01078_ ^ glitches[1047]),
    .Q(\cpuregs[25][18] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16036_
  (
    .D(_01079_ ^ glitches[1048]),
    .Q(\cpuregs[25][19] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16037_
  (
    .D(_01081_ ^ glitches[1049]),
    .Q(\cpuregs[25][20] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16038_
  (
    .D(_01082_ ^ glitches[1050]),
    .Q(\cpuregs[25][21] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16039_
  (
    .D(_01083_ ^ glitches[1051]),
    .Q(\cpuregs[25][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16040_
  (
    .D(_01084_ ^ glitches[1052]),
    .Q(\cpuregs[25][23] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16041_
  (
    .D(_01085_ ^ glitches[1053]),
    .Q(\cpuregs[25][24] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16042_
  (
    .D(_01086_ ^ glitches[1054]),
    .Q(\cpuregs[25][25] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16043_
  (
    .D(_01087_ ^ glitches[1055]),
    .Q(\cpuregs[25][26] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16044_
  (
    .D(_01088_ ^ glitches[1056]),
    .Q(\cpuregs[25][27] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16045_
  (
    .D(_01089_ ^ glitches[1057]),
    .Q(\cpuregs[25][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16046_
  (
    .D(_01090_ ^ glitches[1058]),
    .Q(\cpuregs[25][29] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16047_
  (
    .D(_01092_ ^ glitches[1059]),
    .Q(\cpuregs[25][30] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16048_
  (
    .D(_01093_ ^ glitches[1060]),
    .Q(\cpuregs[25][31] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16049_
  (
    .D(_00781_ ^ glitches[1061]),
    .Q(\cpuregs[17][0] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16050_
  (
    .D(_00792_ ^ glitches[1062]),
    .Q(\cpuregs[17][1] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16051_
  (
    .D(_00803_ ^ glitches[1063]),
    .Q(\cpuregs[17][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16052_
  (
    .D(_00806_ ^ glitches[1064]),
    .Q(\cpuregs[17][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16053_
  (
    .D(_00807_ ^ glitches[1065]),
    .Q(\cpuregs[17][4] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16054_
  (
    .D(_00808_ ^ glitches[1066]),
    .Q(\cpuregs[17][5] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16055_
  (
    .D(_00809_ ^ glitches[1067]),
    .Q(\cpuregs[17][6] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16056_
  (
    .D(_00810_ ^ glitches[1068]),
    .Q(\cpuregs[17][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16057_
  (
    .D(_00811_ ^ glitches[1069]),
    .Q(\cpuregs[17][8] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16058_
  (
    .D(_00812_ ^ glitches[1070]),
    .Q(\cpuregs[17][9] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16059_
  (
    .D(_00782_ ^ glitches[1071]),
    .Q(\cpuregs[17][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16060_
  (
    .D(_00783_ ^ glitches[1072]),
    .Q(\cpuregs[17][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16061_
  (
    .D(_00784_ ^ glitches[1073]),
    .Q(\cpuregs[17][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16062_
  (
    .D(_00785_ ^ glitches[1074]),
    .Q(\cpuregs[17][13] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16063_
  (
    .D(_00786_ ^ glitches[1075]),
    .Q(\cpuregs[17][14] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16064_
  (
    .D(_00787_ ^ glitches[1076]),
    .Q(\cpuregs[17][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16065_
  (
    .D(_00788_ ^ glitches[1077]),
    .Q(\cpuregs[17][16] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16066_
  (
    .D(_00789_ ^ glitches[1078]),
    .Q(\cpuregs[17][17] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16067_
  (
    .D(_00790_ ^ glitches[1079]),
    .Q(\cpuregs[17][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16068_
  (
    .D(_00791_ ^ glitches[1080]),
    .Q(\cpuregs[17][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16069_
  (
    .D(_00793_ ^ glitches[1081]),
    .Q(\cpuregs[17][20] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16070_
  (
    .D(_00794_ ^ glitches[1082]),
    .Q(\cpuregs[17][21] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16071_
  (
    .D(_00795_ ^ glitches[1083]),
    .Q(\cpuregs[17][22] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16072_
  (
    .D(_00796_ ^ glitches[1084]),
    .Q(\cpuregs[17][23] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16073_
  (
    .D(_00797_ ^ glitches[1085]),
    .Q(\cpuregs[17][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16074_
  (
    .D(_00798_ ^ glitches[1086]),
    .Q(\cpuregs[17][25] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16075_
  (
    .D(_00799_ ^ glitches[1087]),
    .Q(\cpuregs[17][26] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16076_
  (
    .D(_00800_ ^ glitches[1088]),
    .Q(\cpuregs[17][27] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16077_
  (
    .D(_00801_ ^ glitches[1089]),
    .Q(\cpuregs[17][28] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16078_
  (
    .D(_00802_ ^ glitches[1090]),
    .Q(\cpuregs[17][29] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16079_
  (
    .D(_00804_ ^ glitches[1091]),
    .Q(\cpuregs[17][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16080_
  (
    .D(_00805_ ^ glitches[1092]),
    .Q(\cpuregs[17][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16081_
  (
    .D(_01389_ ^ glitches[1093]),
    .Q(\cpuregs[5][0] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16082_
  (
    .D(_01400_ ^ glitches[1094]),
    .Q(\cpuregs[5][1] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16083_
  (
    .D(_01411_ ^ glitches[1095]),
    .Q(\cpuregs[5][2] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16084_
  (
    .D(_01414_ ^ glitches[1096]),
    .Q(\cpuregs[5][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16085_
  (
    .D(_01415_ ^ glitches[1097]),
    .Q(\cpuregs[5][4] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16086_
  (
    .D(_01416_ ^ glitches[1098]),
    .Q(\cpuregs[5][5] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16087_
  (
    .D(_01417_ ^ glitches[1099]),
    .Q(\cpuregs[5][6] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16088_
  (
    .D(_01418_ ^ glitches[1100]),
    .Q(\cpuregs[5][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16089_
  (
    .D(_01419_ ^ glitches[1101]),
    .Q(\cpuregs[5][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16090_
  (
    .D(_01420_ ^ glitches[1102]),
    .Q(\cpuregs[5][9] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16091_
  (
    .D(_01390_ ^ glitches[1103]),
    .Q(\cpuregs[5][10] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16092_
  (
    .D(_01391_ ^ glitches[1104]),
    .Q(\cpuregs[5][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16093_
  (
    .D(_01392_ ^ glitches[1105]),
    .Q(\cpuregs[5][12] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16094_
  (
    .D(_01393_ ^ glitches[1106]),
    .Q(\cpuregs[5][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16095_
  (
    .D(_01394_ ^ glitches[1107]),
    .Q(\cpuregs[5][14] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16096_
  (
    .D(_01395_ ^ glitches[1108]),
    .Q(\cpuregs[5][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16097_
  (
    .D(_01396_ ^ glitches[1109]),
    .Q(\cpuregs[5][16] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16098_
  (
    .D(_01397_ ^ glitches[1110]),
    .Q(\cpuregs[5][17] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16099_
  (
    .D(_01398_ ^ glitches[1111]),
    .Q(\cpuregs[5][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16100_
  (
    .D(_01399_ ^ glitches[1112]),
    .Q(\cpuregs[5][19] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16101_
  (
    .D(_01401_ ^ glitches[1113]),
    .Q(\cpuregs[5][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16102_
  (
    .D(_01402_ ^ glitches[1114]),
    .Q(\cpuregs[5][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16103_
  (
    .D(_01403_ ^ glitches[1115]),
    .Q(\cpuregs[5][22] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16104_
  (
    .D(_01404_ ^ glitches[1116]),
    .Q(\cpuregs[5][23] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16105_
  (
    .D(_01405_ ^ glitches[1117]),
    .Q(\cpuregs[5][24] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16106_
  (
    .D(_01406_ ^ glitches[1118]),
    .Q(\cpuregs[5][25] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16107_
  (
    .D(_01407_ ^ glitches[1119]),
    .Q(\cpuregs[5][26] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16108_
  (
    .D(_01408_ ^ glitches[1120]),
    .Q(\cpuregs[5][27] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16109_
  (
    .D(_01409_ ^ glitches[1121]),
    .Q(\cpuregs[5][28] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16110_
  (
    .D(_01410_ ^ glitches[1122]),
    .Q(\cpuregs[5][29] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16111_
  (
    .D(_01412_ ^ glitches[1123]),
    .Q(\cpuregs[5][30] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16112_
  (
    .D(_01413_ ^ glitches[1124]),
    .Q(\cpuregs[5][31] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16113_
  (
    .D(_00717_ ^ glitches[1125]),
    .Q(\cpuregs[15][0] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16114_
  (
    .D(_00728_ ^ glitches[1126]),
    .Q(\cpuregs[15][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16115_
  (
    .D(_00739_ ^ glitches[1127]),
    .Q(\cpuregs[15][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16116_
  (
    .D(_00742_ ^ glitches[1128]),
    .Q(\cpuregs[15][3] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16117_
  (
    .D(_00743_ ^ glitches[1129]),
    .Q(\cpuregs[15][4] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16118_
  (
    .D(_00744_ ^ glitches[1130]),
    .Q(\cpuregs[15][5] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16119_
  (
    .D(_00745_ ^ glitches[1131]),
    .Q(\cpuregs[15][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16120_
  (
    .D(_00746_ ^ glitches[1132]),
    .Q(\cpuregs[15][7] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16121_
  (
    .D(_00747_ ^ glitches[1133]),
    .Q(\cpuregs[15][8] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16122_
  (
    .D(_00748_ ^ glitches[1134]),
    .Q(\cpuregs[15][9] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16123_
  (
    .D(_00718_ ^ glitches[1135]),
    .Q(\cpuregs[15][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16124_
  (
    .D(_00719_ ^ glitches[1136]),
    .Q(\cpuregs[15][11] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16125_
  (
    .D(_00720_ ^ glitches[1137]),
    .Q(\cpuregs[15][12] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16126_
  (
    .D(_00721_ ^ glitches[1138]),
    .Q(\cpuregs[15][13] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16127_
  (
    .D(_00722_ ^ glitches[1139]),
    .Q(\cpuregs[15][14] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16128_
  (
    .D(_00723_ ^ glitches[1140]),
    .Q(\cpuregs[15][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16129_
  (
    .D(_00724_ ^ glitches[1141]),
    .Q(\cpuregs[15][16] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16130_
  (
    .D(_00725_ ^ glitches[1142]),
    .Q(\cpuregs[15][17] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16131_
  (
    .D(_00726_ ^ glitches[1143]),
    .Q(\cpuregs[15][18] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16132_
  (
    .D(_00727_ ^ glitches[1144]),
    .Q(\cpuregs[15][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16133_
  (
    .D(_00729_ ^ glitches[1145]),
    .Q(\cpuregs[15][20] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16134_
  (
    .D(_00730_ ^ glitches[1146]),
    .Q(\cpuregs[15][21] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16135_
  (
    .D(_00731_ ^ glitches[1147]),
    .Q(\cpuregs[15][22] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16136_
  (
    .D(_00732_ ^ glitches[1148]),
    .Q(\cpuregs[15][23] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16137_
  (
    .D(_00733_ ^ glitches[1149]),
    .Q(\cpuregs[15][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16138_
  (
    .D(_00734_ ^ glitches[1150]),
    .Q(\cpuregs[15][25] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16139_
  (
    .D(_00735_ ^ glitches[1151]),
    .Q(\cpuregs[15][26] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16140_
  (
    .D(_00736_ ^ glitches[1152]),
    .Q(\cpuregs[15][27] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16141_
  (
    .D(_00737_ ^ glitches[1153]),
    .Q(\cpuregs[15][28] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16142_
  (
    .D(_00738_ ^ glitches[1154]),
    .Q(\cpuregs[15][29] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16143_
  (
    .D(_00740_ ^ glitches[1155]),
    .Q(\cpuregs[15][30] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16144_
  (
    .D(_00741_ ^ glitches[1156]),
    .Q(\cpuregs[15][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16145_
  (
    .D(_01357_ ^ glitches[1157]),
    .Q(\cpuregs[4][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16146_
  (
    .D(_01368_ ^ glitches[1158]),
    .Q(\cpuregs[4][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16147_
  (
    .D(_01379_ ^ glitches[1159]),
    .Q(\cpuregs[4][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16148_
  (
    .D(_01382_ ^ glitches[1160]),
    .Q(\cpuregs[4][3] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16149_
  (
    .D(_01383_ ^ glitches[1161]),
    .Q(\cpuregs[4][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16150_
  (
    .D(_01384_ ^ glitches[1162]),
    .Q(\cpuregs[4][5] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16151_
  (
    .D(_01385_ ^ glitches[1163]),
    .Q(\cpuregs[4][6] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16152_
  (
    .D(_01386_ ^ glitches[1164]),
    .Q(\cpuregs[4][7] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16153_
  (
    .D(_01387_ ^ glitches[1165]),
    .Q(\cpuregs[4][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16154_
  (
    .D(_01388_ ^ glitches[1166]),
    .Q(\cpuregs[4][9] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16155_
  (
    .D(_01358_ ^ glitches[1167]),
    .Q(\cpuregs[4][10] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16156_
  (
    .D(_01359_ ^ glitches[1168]),
    .Q(\cpuregs[4][11] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16157_
  (
    .D(_01360_ ^ glitches[1169]),
    .Q(\cpuregs[4][12] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16158_
  (
    .D(_01361_ ^ glitches[1170]),
    .Q(\cpuregs[4][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16159_
  (
    .D(_01362_ ^ glitches[1171]),
    .Q(\cpuregs[4][14] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16160_
  (
    .D(_01363_ ^ glitches[1172]),
    .Q(\cpuregs[4][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16161_
  (
    .D(_01364_ ^ glitches[1173]),
    .Q(\cpuregs[4][16] ),
    .CLK(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16162_
  (
    .D(_01365_ ^ glitches[1174]),
    .Q(\cpuregs[4][17] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16163_
  (
    .D(_01366_ ^ glitches[1175]),
    .Q(\cpuregs[4][18] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16164_
  (
    .D(_01367_ ^ glitches[1176]),
    .Q(\cpuregs[4][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16165_
  (
    .D(_01369_ ^ glitches[1177]),
    .Q(\cpuregs[4][20] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16166_
  (
    .D(_01370_ ^ glitches[1178]),
    .Q(\cpuregs[4][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16167_
  (
    .D(_01371_ ^ glitches[1179]),
    .Q(\cpuregs[4][22] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16168_
  (
    .D(_01372_ ^ glitches[1180]),
    .Q(\cpuregs[4][23] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16169_
  (
    .D(_01373_ ^ glitches[1181]),
    .Q(\cpuregs[4][24] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16170_
  (
    .D(_01374_ ^ glitches[1182]),
    .Q(\cpuregs[4][25] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16171_
  (
    .D(_01375_ ^ glitches[1183]),
    .Q(\cpuregs[4][26] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16172_
  (
    .D(_01376_ ^ glitches[1184]),
    .Q(\cpuregs[4][27] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16173_
  (
    .D(_01377_ ^ glitches[1185]),
    .Q(\cpuregs[4][28] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16174_
  (
    .D(_01378_ ^ glitches[1186]),
    .Q(\cpuregs[4][29] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16175_
  (
    .D(_01380_ ^ glitches[1187]),
    .Q(\cpuregs[4][30] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16176_
  (
    .D(_01381_ ^ glitches[1188]),
    .Q(\cpuregs[4][31] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16177_
  (
    .D(_00845_ ^ glitches[1189]),
    .Q(\cpuregs[19][0] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16178_
  (
    .D(_00856_ ^ glitches[1190]),
    .Q(\cpuregs[19][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16179_
  (
    .D(_00867_ ^ glitches[1191]),
    .Q(\cpuregs[19][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16180_
  (
    .D(_00870_ ^ glitches[1192]),
    .Q(\cpuregs[19][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16181_
  (
    .D(_00871_ ^ glitches[1193]),
    .Q(\cpuregs[19][4] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16182_
  (
    .D(_00872_ ^ glitches[1194]),
    .Q(\cpuregs[19][5] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16183_
  (
    .D(_00873_ ^ glitches[1195]),
    .Q(\cpuregs[19][6] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16184_
  (
    .D(_00874_ ^ glitches[1196]),
    .Q(\cpuregs[19][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16185_
  (
    .D(_00875_ ^ glitches[1197]),
    .Q(\cpuregs[19][8] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16186_
  (
    .D(_00876_ ^ glitches[1198]),
    .Q(\cpuregs[19][9] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16187_
  (
    .D(_00846_ ^ glitches[1199]),
    .Q(\cpuregs[19][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16188_
  (
    .D(_00847_ ^ glitches[1200]),
    .Q(\cpuregs[19][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16189_
  (
    .D(_00848_ ^ glitches[1201]),
    .Q(\cpuregs[19][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16190_
  (
    .D(_00849_ ^ glitches[1202]),
    .Q(\cpuregs[19][13] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16191_
  (
    .D(_00850_ ^ glitches[1203]),
    .Q(\cpuregs[19][14] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16192_
  (
    .D(_00851_ ^ glitches[1204]),
    .Q(\cpuregs[19][15] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16193_
  (
    .D(_00852_ ^ glitches[1205]),
    .Q(\cpuregs[19][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16194_
  (
    .D(_00853_ ^ glitches[1206]),
    .Q(\cpuregs[19][17] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16195_
  (
    .D(_00854_ ^ glitches[1207]),
    .Q(\cpuregs[19][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16196_
  (
    .D(_00855_ ^ glitches[1208]),
    .Q(\cpuregs[19][19] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16197_
  (
    .D(_00857_ ^ glitches[1209]),
    .Q(\cpuregs[19][20] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16198_
  (
    .D(_00858_ ^ glitches[1210]),
    .Q(\cpuregs[19][21] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16199_
  (
    .D(_00859_ ^ glitches[1211]),
    .Q(\cpuregs[19][22] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16200_
  (
    .D(_00860_ ^ glitches[1212]),
    .Q(\cpuregs[19][23] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16201_
  (
    .D(_00861_ ^ glitches[1213]),
    .Q(\cpuregs[19][24] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16202_
  (
    .D(_00862_ ^ glitches[1214]),
    .Q(\cpuregs[19][25] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16203_
  (
    .D(_00863_ ^ glitches[1215]),
    .Q(\cpuregs[19][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16204_
  (
    .D(_00864_ ^ glitches[1216]),
    .Q(\cpuregs[19][27] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16205_
  (
    .D(_00865_ ^ glitches[1217]),
    .Q(\cpuregs[19][28] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16206_
  (
    .D(_00866_ ^ glitches[1218]),
    .Q(\cpuregs[19][29] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16207_
  (
    .D(_00868_ ^ glitches[1219]),
    .Q(\cpuregs[19][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16208_
  (
    .D(_00869_ ^ glitches[1220]),
    .Q(\cpuregs[19][31] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16209_
  (
    .D(_00685_ ^ glitches[1221]),
    .Q(\cpuregs[14][0] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16210_
  (
    .D(_00696_ ^ glitches[1222]),
    .Q(\cpuregs[14][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16211_
  (
    .D(_00707_ ^ glitches[1223]),
    .Q(\cpuregs[14][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16212_
  (
    .D(_00710_ ^ glitches[1224]),
    .Q(\cpuregs[14][3] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16213_
  (
    .D(_00711_ ^ glitches[1225]),
    .Q(\cpuregs[14][4] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16214_
  (
    .D(_00712_ ^ glitches[1226]),
    .Q(\cpuregs[14][5] ),
    .CLK(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16215_
  (
    .D(_00713_ ^ glitches[1227]),
    .Q(\cpuregs[14][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16216_
  (
    .D(_00714_ ^ glitches[1228]),
    .Q(\cpuregs[14][7] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16217_
  (
    .D(_00715_ ^ glitches[1229]),
    .Q(\cpuregs[14][8] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16218_
  (
    .D(_00716_ ^ glitches[1230]),
    .Q(\cpuregs[14][9] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16219_
  (
    .D(_00686_ ^ glitches[1231]),
    .Q(\cpuregs[14][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16220_
  (
    .D(_00687_ ^ glitches[1232]),
    .Q(\cpuregs[14][11] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16221_
  (
    .D(_00688_ ^ glitches[1233]),
    .Q(\cpuregs[14][12] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16222_
  (
    .D(_00689_ ^ glitches[1234]),
    .Q(\cpuregs[14][13] ),
    .CLK(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16223_
  (
    .D(_00690_ ^ glitches[1235]),
    .Q(\cpuregs[14][14] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16224_
  (
    .D(_00691_ ^ glitches[1236]),
    .Q(\cpuregs[14][15] ),
    .CLK(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16225_
  (
    .D(_00692_ ^ glitches[1237]),
    .Q(\cpuregs[14][16] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16226_
  (
    .D(_00693_ ^ glitches[1238]),
    .Q(\cpuregs[14][17] ),
    .CLK(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16227_
  (
    .D(_00694_ ^ glitches[1239]),
    .Q(\cpuregs[14][18] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16228_
  (
    .D(_00695_ ^ glitches[1240]),
    .Q(\cpuregs[14][19] ),
    .CLK(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16229_
  (
    .D(_00697_ ^ glitches[1241]),
    .Q(\cpuregs[14][20] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16230_
  (
    .D(_00698_ ^ glitches[1242]),
    .Q(\cpuregs[14][21] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16231_
  (
    .D(_00699_ ^ glitches[1243]),
    .Q(\cpuregs[14][22] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16232_
  (
    .D(_00700_ ^ glitches[1244]),
    .Q(\cpuregs[14][23] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16233_
  (
    .D(_00701_ ^ glitches[1245]),
    .Q(\cpuregs[14][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16234_
  (
    .D(_00702_ ^ glitches[1246]),
    .Q(\cpuregs[14][25] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16235_
  (
    .D(_00703_ ^ glitches[1247]),
    .Q(\cpuregs[14][26] ),
    .CLK(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16236_
  (
    .D(_00704_ ^ glitches[1248]),
    .Q(\cpuregs[14][27] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16237_
  (
    .D(_00705_ ^ glitches[1249]),
    .Q(\cpuregs[14][28] ),
    .CLK(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16238_
  (
    .D(_00706_ ^ glitches[1250]),
    .Q(\cpuregs[14][29] ),
    .CLK(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16239_
  (
    .D(_00708_ ^ glitches[1251]),
    .Q(\cpuregs[14][30] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16240_
  (
    .D(_00709_ ^ glitches[1252]),
    .Q(\cpuregs[14][31] ),
    .CLK(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16241_
  (
    .D(_01101_ ^ glitches[1253]),
    .Q(\cpuregs[26][0] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16242_
  (
    .D(_01112_ ^ glitches[1254]),
    .Q(\cpuregs[26][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16243_
  (
    .D(_01123_ ^ glitches[1255]),
    .Q(\cpuregs[26][2] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16244_
  (
    .D(_01126_ ^ glitches[1256]),
    .Q(\cpuregs[26][3] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16245_
  (
    .D(_01127_ ^ glitches[1257]),
    .Q(\cpuregs[26][4] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16246_
  (
    .D(_01128_ ^ glitches[1258]),
    .Q(\cpuregs[26][5] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16247_
  (
    .D(_01129_ ^ glitches[1259]),
    .Q(\cpuregs[26][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16248_
  (
    .D(_01130_ ^ glitches[1260]),
    .Q(\cpuregs[26][7] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16249_
  (
    .D(_01131_ ^ glitches[1261]),
    .Q(\cpuregs[26][8] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16250_
  (
    .D(_01132_ ^ glitches[1262]),
    .Q(\cpuregs[26][9] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16251_
  (
    .D(_01102_ ^ glitches[1263]),
    .Q(\cpuregs[26][10] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16252_
  (
    .D(_01103_ ^ glitches[1264]),
    .Q(\cpuregs[26][11] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16253_
  (
    .D(_01104_ ^ glitches[1265]),
    .Q(\cpuregs[26][12] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16254_
  (
    .D(_01105_ ^ glitches[1266]),
    .Q(\cpuregs[26][13] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16255_
  (
    .D(_01106_ ^ glitches[1267]),
    .Q(\cpuregs[26][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16256_
  (
    .D(_01107_ ^ glitches[1268]),
    .Q(\cpuregs[26][15] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16257_
  (
    .D(_01108_ ^ glitches[1269]),
    .Q(\cpuregs[26][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16258_
  (
    .D(_01109_ ^ glitches[1270]),
    .Q(\cpuregs[26][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16259_
  (
    .D(_01110_ ^ glitches[1271]),
    .Q(\cpuregs[26][18] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16260_
  (
    .D(_01111_ ^ glitches[1272]),
    .Q(\cpuregs[26][19] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16261_
  (
    .D(_01113_ ^ glitches[1273]),
    .Q(\cpuregs[26][20] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16262_
  (
    .D(_01114_ ^ glitches[1274]),
    .Q(\cpuregs[26][21] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16263_
  (
    .D(_01115_ ^ glitches[1275]),
    .Q(\cpuregs[26][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16264_
  (
    .D(_01116_ ^ glitches[1276]),
    .Q(\cpuregs[26][23] ),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16265_
  (
    .D(_01117_ ^ glitches[1277]),
    .Q(\cpuregs[26][24] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16266_
  (
    .D(_01118_ ^ glitches[1278]),
    .Q(\cpuregs[26][25] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16267_
  (
    .D(_01119_ ^ glitches[1279]),
    .Q(\cpuregs[26][26] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16268_
  (
    .D(_01120_ ^ glitches[1280]),
    .Q(\cpuregs[26][27] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16269_
  (
    .D(_01121_ ^ glitches[1281]),
    .Q(\cpuregs[26][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16270_
  (
    .D(_01122_ ^ glitches[1282]),
    .Q(\cpuregs[26][29] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16271_
  (
    .D(_01124_ ^ glitches[1283]),
    .Q(\cpuregs[26][30] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16272_
  (
    .D(_01125_ ^ glitches[1284]),
    .Q(\cpuregs[26][31] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16273_
  (
    .D(_01133_ ^ glitches[1285]),
    .Q(\cpuregs[27][0] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16274_
  (
    .D(_01144_ ^ glitches[1286]),
    .Q(\cpuregs[27][1] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16275_
  (
    .D(_01155_ ^ glitches[1287]),
    .Q(\cpuregs[27][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16276_
  (
    .D(_01158_ ^ glitches[1288]),
    .Q(\cpuregs[27][3] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16277_
  (
    .D(_01159_ ^ glitches[1289]),
    .Q(\cpuregs[27][4] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16278_
  (
    .D(_01160_ ^ glitches[1290]),
    .Q(\cpuregs[27][5] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16279_
  (
    .D(_01161_ ^ glitches[1291]),
    .Q(\cpuregs[27][6] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16280_
  (
    .D(_01162_ ^ glitches[1292]),
    .Q(\cpuregs[27][7] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16281_
  (
    .D(_01163_ ^ glitches[1293]),
    .Q(\cpuregs[27][8] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16282_
  (
    .D(_01164_ ^ glitches[1294]),
    .Q(\cpuregs[27][9] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16283_
  (
    .D(_01134_ ^ glitches[1295]),
    .Q(\cpuregs[27][10] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16284_
  (
    .D(_01135_ ^ glitches[1296]),
    .Q(\cpuregs[27][11] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16285_
  (
    .D(_01136_ ^ glitches[1297]),
    .Q(\cpuregs[27][12] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16286_
  (
    .D(_01137_ ^ glitches[1298]),
    .Q(\cpuregs[27][13] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16287_
  (
    .D(_01138_ ^ glitches[1299]),
    .Q(\cpuregs[27][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16288_
  (
    .D(_01139_ ^ glitches[1300]),
    .Q(\cpuregs[27][15] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16289_
  (
    .D(_01140_ ^ glitches[1301]),
    .Q(\cpuregs[27][16] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16290_
  (
    .D(_01141_ ^ glitches[1302]),
    .Q(\cpuregs[27][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16291_
  (
    .D(_01142_ ^ glitches[1303]),
    .Q(\cpuregs[27][18] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16292_
  (
    .D(_01143_ ^ glitches[1304]),
    .Q(\cpuregs[27][19] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16293_
  (
    .D(_01145_ ^ glitches[1305]),
    .Q(\cpuregs[27][20] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16294_
  (
    .D(_01146_ ^ glitches[1306]),
    .Q(\cpuregs[27][21] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16295_
  (
    .D(_01147_ ^ glitches[1307]),
    .Q(\cpuregs[27][22] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16296_
  (
    .D(_01148_ ^ glitches[1308]),
    .Q(\cpuregs[27][23] ),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16297_
  (
    .D(_01149_ ^ glitches[1309]),
    .Q(\cpuregs[27][24] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16298_
  (
    .D(_01150_ ^ glitches[1310]),
    .Q(\cpuregs[27][25] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16299_
  (
    .D(_01151_ ^ glitches[1311]),
    .Q(\cpuregs[27][26] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16300_
  (
    .D(_01152_ ^ glitches[1312]),
    .Q(\cpuregs[27][27] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16301_
  (
    .D(_01153_ ^ glitches[1313]),
    .Q(\cpuregs[27][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16302_
  (
    .D(_01154_ ^ glitches[1314]),
    .Q(\cpuregs[27][29] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16303_
  (
    .D(_01156_ ^ glitches[1315]),
    .Q(\cpuregs[27][30] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16304_
  (
    .D(_01157_ ^ glitches[1316]),
    .Q(\cpuregs[27][31] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16305_
  (
    .D(_01197_ ^ glitches[1317]),
    .Q(\cpuregs[29][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16306_
  (
    .D(_01208_ ^ glitches[1318]),
    .Q(\cpuregs[29][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16307_
  (
    .D(_01219_ ^ glitches[1319]),
    .Q(\cpuregs[29][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16308_
  (
    .D(_01222_ ^ glitches[1320]),
    .Q(\cpuregs[29][3] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16309_
  (
    .D(_01223_ ^ glitches[1321]),
    .Q(\cpuregs[29][4] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16310_
  (
    .D(_01224_ ^ glitches[1322]),
    .Q(\cpuregs[29][5] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16311_
  (
    .D(_01225_ ^ glitches[1323]),
    .Q(\cpuregs[29][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16312_
  (
    .D(_01226_ ^ glitches[1324]),
    .Q(\cpuregs[29][7] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16313_
  (
    .D(_01227_ ^ glitches[1325]),
    .Q(\cpuregs[29][8] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16314_
  (
    .D(_01228_ ^ glitches[1326]),
    .Q(\cpuregs[29][9] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16315_
  (
    .D(_01198_ ^ glitches[1327]),
    .Q(\cpuregs[29][10] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16316_
  (
    .D(_01199_ ^ glitches[1328]),
    .Q(\cpuregs[29][11] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16317_
  (
    .D(_01200_ ^ glitches[1329]),
    .Q(\cpuregs[29][12] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16318_
  (
    .D(_01201_ ^ glitches[1330]),
    .Q(\cpuregs[29][13] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16319_
  (
    .D(_01202_ ^ glitches[1331]),
    .Q(\cpuregs[29][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16320_
  (
    .D(_01203_ ^ glitches[1332]),
    .Q(\cpuregs[29][15] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16321_
  (
    .D(_01204_ ^ glitches[1333]),
    .Q(\cpuregs[29][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16322_
  (
    .D(_01205_ ^ glitches[1334]),
    .Q(\cpuregs[29][17] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16323_
  (
    .D(_01206_ ^ glitches[1335]),
    .Q(\cpuregs[29][18] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16324_
  (
    .D(_01207_ ^ glitches[1336]),
    .Q(\cpuregs[29][19] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16325_
  (
    .D(_01209_ ^ glitches[1337]),
    .Q(\cpuregs[29][20] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16326_
  (
    .D(_01210_ ^ glitches[1338]),
    .Q(\cpuregs[29][21] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16327_
  (
    .D(_01211_ ^ glitches[1339]),
    .Q(\cpuregs[29][22] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16328_
  (
    .D(_01212_ ^ glitches[1340]),
    .Q(\cpuregs[29][23] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16329_
  (
    .D(_01213_ ^ glitches[1341]),
    .Q(\cpuregs[29][24] ),
    .CLK(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16330_
  (
    .D(_01214_ ^ glitches[1342]),
    .Q(\cpuregs[29][25] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16331_
  (
    .D(_01215_ ^ glitches[1343]),
    .Q(\cpuregs[29][26] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16332_
  (
    .D(_01216_ ^ glitches[1344]),
    .Q(\cpuregs[29][27] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16333_
  (
    .D(_01217_ ^ glitches[1345]),
    .Q(\cpuregs[29][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16334_
  (
    .D(_01218_ ^ glitches[1346]),
    .Q(\cpuregs[29][29] ),
    .CLK(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16335_
  (
    .D(_01220_ ^ glitches[1347]),
    .Q(\cpuregs[29][30] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16336_
  (
    .D(_01221_ ^ glitches[1348]),
    .Q(\cpuregs[29][31] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16337_
  (
    .D(_00973_ ^ glitches[1349]),
    .Q(\cpuregs[22][0] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16338_
  (
    .D(_00984_ ^ glitches[1350]),
    .Q(\cpuregs[22][1] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16339_
  (
    .D(_00995_ ^ glitches[1351]),
    .Q(\cpuregs[22][2] ),
    .CLK(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16340_
  (
    .D(_00998_ ^ glitches[1352]),
    .Q(\cpuregs[22][3] ),
    .CLK(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16341_
  (
    .D(_00999_ ^ glitches[1353]),
    .Q(\cpuregs[22][4] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16342_
  (
    .D(_01000_ ^ glitches[1354]),
    .Q(\cpuregs[22][5] ),
    .CLK(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16343_
  (
    .D(_01001_ ^ glitches[1355]),
    .Q(\cpuregs[22][6] ),
    .CLK(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16344_
  (
    .D(_01002_ ^ glitches[1356]),
    .Q(\cpuregs[22][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16345_
  (
    .D(_01003_ ^ glitches[1357]),
    .Q(\cpuregs[22][8] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16346_
  (
    .D(_01004_ ^ glitches[1358]),
    .Q(\cpuregs[22][9] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16347_
  (
    .D(_00974_ ^ glitches[1359]),
    .Q(\cpuregs[22][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16348_
  (
    .D(_00975_ ^ glitches[1360]),
    .Q(\cpuregs[22][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16349_
  (
    .D(_00976_ ^ glitches[1361]),
    .Q(\cpuregs[22][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16350_
  (
    .D(_00977_ ^ glitches[1362]),
    .Q(\cpuregs[22][13] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16351_
  (
    .D(_00978_ ^ glitches[1363]),
    .Q(\cpuregs[22][14] ),
    .CLK(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16352_
  (
    .D(_00979_ ^ glitches[1364]),
    .Q(\cpuregs[22][15] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16353_
  (
    .D(_00980_ ^ glitches[1365]),
    .Q(\cpuregs[22][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16354_
  (
    .D(_00981_ ^ glitches[1366]),
    .Q(\cpuregs[22][17] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16355_
  (
    .D(_00982_ ^ glitches[1367]),
    .Q(\cpuregs[22][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16356_
  (
    .D(_00983_ ^ glitches[1368]),
    .Q(\cpuregs[22][19] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16357_
  (
    .D(_00985_ ^ glitches[1369]),
    .Q(\cpuregs[22][20] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16358_
  (
    .D(_00986_ ^ glitches[1370]),
    .Q(\cpuregs[22][21] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16359_
  (
    .D(_00987_ ^ glitches[1371]),
    .Q(\cpuregs[22][22] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16360_
  (
    .D(_00988_ ^ glitches[1372]),
    .Q(\cpuregs[22][23] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16361_
  (
    .D(_00989_ ^ glitches[1373]),
    .Q(\cpuregs[22][24] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16362_
  (
    .D(_00990_ ^ glitches[1374]),
    .Q(\cpuregs[22][25] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16363_
  (
    .D(_00991_ ^ glitches[1375]),
    .Q(\cpuregs[22][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16364_
  (
    .D(_00992_ ^ glitches[1376]),
    .Q(\cpuregs[22][27] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16365_
  (
    .D(_00993_ ^ glitches[1377]),
    .Q(\cpuregs[22][28] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16366_
  (
    .D(_00994_ ^ glitches[1378]),
    .Q(\cpuregs[22][29] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16367_
  (
    .D(_00996_ ^ glitches[1379]),
    .Q(\cpuregs[22][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16368_
  (
    .D(_00997_ ^ glitches[1380]),
    .Q(\cpuregs[22][31] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16369_
  (
    .D(_01453_ ^ glitches[1381]),
    .Q(\cpuregs[7][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16370_
  (
    .D(_01464_ ^ glitches[1382]),
    .Q(\cpuregs[7][1] ),
    .CLK(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16371_
  (
    .D(_01475_ ^ glitches[1383]),
    .Q(\cpuregs[7][2] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16372_
  (
    .D(_01478_ ^ glitches[1384]),
    .Q(\cpuregs[7][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16373_
  (
    .D(_01479_ ^ glitches[1385]),
    .Q(\cpuregs[7][4] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16374_
  (
    .D(_01480_ ^ glitches[1386]),
    .Q(\cpuregs[7][5] ),
    .CLK(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16375_
  (
    .D(_01481_ ^ glitches[1387]),
    .Q(\cpuregs[7][6] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16376_
  (
    .D(_01482_ ^ glitches[1388]),
    .Q(\cpuregs[7][7] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16377_
  (
    .D(_01483_ ^ glitches[1389]),
    .Q(\cpuregs[7][8] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16378_
  (
    .D(_01484_ ^ glitches[1390]),
    .Q(\cpuregs[7][9] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16379_
  (
    .D(_01454_ ^ glitches[1391]),
    .Q(\cpuregs[7][10] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16380_
  (
    .D(_01455_ ^ glitches[1392]),
    .Q(\cpuregs[7][11] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16381_
  (
    .D(_01456_ ^ glitches[1393]),
    .Q(\cpuregs[7][12] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16382_
  (
    .D(_01457_ ^ glitches[1394]),
    .Q(\cpuregs[7][13] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16383_
  (
    .D(_01458_ ^ glitches[1395]),
    .Q(\cpuregs[7][14] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16384_
  (
    .D(_01459_ ^ glitches[1396]),
    .Q(\cpuregs[7][15] ),
    .CLK(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16385_
  (
    .D(_01460_ ^ glitches[1397]),
    .Q(\cpuregs[7][16] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16386_
  (
    .D(_01461_ ^ glitches[1398]),
    .Q(\cpuregs[7][17] ),
    .CLK(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16387_
  (
    .D(_01462_ ^ glitches[1399]),
    .Q(\cpuregs[7][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16388_
  (
    .D(_01463_ ^ glitches[1400]),
    .Q(\cpuregs[7][19] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16389_
  (
    .D(_01465_ ^ glitches[1401]),
    .Q(\cpuregs[7][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16390_
  (
    .D(_01466_ ^ glitches[1402]),
    .Q(\cpuregs[7][21] ),
    .CLK(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16391_
  (
    .D(_01467_ ^ glitches[1403]),
    .Q(\cpuregs[7][22] ),
    .CLK(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16392_
  (
    .D(_01468_ ^ glitches[1404]),
    .Q(\cpuregs[7][23] ),
    .CLK(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16393_
  (
    .D(_01469_ ^ glitches[1405]),
    .Q(\cpuregs[7][24] ),
    .CLK(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16394_
  (
    .D(_01470_ ^ glitches[1406]),
    .Q(\cpuregs[7][25] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16395_
  (
    .D(_01471_ ^ glitches[1407]),
    .Q(\cpuregs[7][26] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16396_
  (
    .D(_01472_ ^ glitches[1408]),
    .Q(\cpuregs[7][27] ),
    .CLK(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16397_
  (
    .D(_01473_ ^ glitches[1409]),
    .Q(\cpuregs[7][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16398_
  (
    .D(_01474_ ^ glitches[1410]),
    .Q(\cpuregs[7][29] ),
    .CLK(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16399_
  (
    .D(_01476_ ^ glitches[1411]),
    .Q(\cpuregs[7][30] ),
    .CLK(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16400_
  (
    .D(_01477_ ^ glitches[1412]),
    .Q(\cpuregs[7][31] ),
    .CLK(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16401_
  (
    .D(_00653_ ^ glitches[1413]),
    .Q(\cpuregs[13][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16402_
  (
    .D(_00664_ ^ glitches[1414]),
    .Q(\cpuregs[13][1] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16403_
  (
    .D(_00675_ ^ glitches[1415]),
    .Q(\cpuregs[13][2] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16404_
  (
    .D(_00678_ ^ glitches[1416]),
    .Q(\cpuregs[13][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16405_
  (
    .D(_00679_ ^ glitches[1417]),
    .Q(\cpuregs[13][4] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16406_
  (
    .D(_00680_ ^ glitches[1418]),
    .Q(\cpuregs[13][5] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16407_
  (
    .D(_00681_ ^ glitches[1419]),
    .Q(\cpuregs[13][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16408_
  (
    .D(_00682_ ^ glitches[1420]),
    .Q(\cpuregs[13][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16409_
  (
    .D(_00683_ ^ glitches[1421]),
    .Q(\cpuregs[13][8] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16410_
  (
    .D(_00684_ ^ glitches[1422]),
    .Q(\cpuregs[13][9] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16411_
  (
    .D(_00654_ ^ glitches[1423]),
    .Q(\cpuregs[13][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16412_
  (
    .D(_00655_ ^ glitches[1424]),
    .Q(\cpuregs[13][11] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16413_
  (
    .D(_00656_ ^ glitches[1425]),
    .Q(\cpuregs[13][12] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16414_
  (
    .D(_00657_ ^ glitches[1426]),
    .Q(\cpuregs[13][13] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16415_
  (
    .D(_00658_ ^ glitches[1427]),
    .Q(\cpuregs[13][14] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16416_
  (
    .D(_00659_ ^ glitches[1428]),
    .Q(\cpuregs[13][15] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16417_
  (
    .D(_00660_ ^ glitches[1429]),
    .Q(\cpuregs[13][16] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16418_
  (
    .D(_00661_ ^ glitches[1430]),
    .Q(\cpuregs[13][17] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16419_
  (
    .D(_00662_ ^ glitches[1431]),
    .Q(\cpuregs[13][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16420_
  (
    .D(_00663_ ^ glitches[1432]),
    .Q(\cpuregs[13][19] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16421_
  (
    .D(_00665_ ^ glitches[1433]),
    .Q(\cpuregs[13][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16422_
  (
    .D(_00666_ ^ glitches[1434]),
    .Q(\cpuregs[13][21] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16423_
  (
    .D(_00667_ ^ glitches[1435]),
    .Q(\cpuregs[13][22] ),
    .CLK(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16424_
  (
    .D(_00668_ ^ glitches[1436]),
    .Q(\cpuregs[13][23] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16425_
  (
    .D(_00669_ ^ glitches[1437]),
    .Q(\cpuregs[13][24] ),
    .CLK(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16426_
  (
    .D(_00670_ ^ glitches[1438]),
    .Q(\cpuregs[13][25] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16427_
  (
    .D(_00671_ ^ glitches[1439]),
    .Q(\cpuregs[13][26] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16428_
  (
    .D(_00672_ ^ glitches[1440]),
    .Q(\cpuregs[13][27] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16429_
  (
    .D(_00673_ ^ glitches[1441]),
    .Q(\cpuregs[13][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16430_
  (
    .D(_00674_ ^ glitches[1442]),
    .Q(\cpuregs[13][29] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16431_
  (
    .D(_00676_ ^ glitches[1443]),
    .Q(\cpuregs[13][30] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16432_
  (
    .D(_00677_ ^ glitches[1444]),
    .Q(\cpuregs[13][31] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16433_
  (
    .D(_00522_ ^ glitches[1445]),
    .Q(\mem_wordsize[0] ),
    .CLK(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16434_
  (
    .D(_00523_ ^ glitches[1446]),
    .Q(\mem_wordsize[1] ),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16435_
  (
    .D(_00524_ ^ glitches[1447]),
    .Q(\mem_wordsize[2] ),
    .CLK(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16436_
  (
    .D(_00941_ ^ glitches[1448]),
    .Q(\cpuregs[21][0] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16437_
  (
    .D(_00952_ ^ glitches[1449]),
    .Q(\cpuregs[21][1] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16438_
  (
    .D(_00963_ ^ glitches[1450]),
    .Q(\cpuregs[21][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16439_
  (
    .D(_00966_ ^ glitches[1451]),
    .Q(\cpuregs[21][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16440_
  (
    .D(_00967_ ^ glitches[1452]),
    .Q(\cpuregs[21][4] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16441_
  (
    .D(_00968_ ^ glitches[1453]),
    .Q(\cpuregs[21][5] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16442_
  (
    .D(_00969_ ^ glitches[1454]),
    .Q(\cpuregs[21][6] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16443_
  (
    .D(_00970_ ^ glitches[1455]),
    .Q(\cpuregs[21][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16444_
  (
    .D(_00971_ ^ glitches[1456]),
    .Q(\cpuregs[21][8] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16445_
  (
    .D(_00972_ ^ glitches[1457]),
    .Q(\cpuregs[21][9] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16446_
  (
    .D(_00942_ ^ glitches[1458]),
    .Q(\cpuregs[21][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16447_
  (
    .D(_00943_ ^ glitches[1459]),
    .Q(\cpuregs[21][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16448_
  (
    .D(_00944_ ^ glitches[1460]),
    .Q(\cpuregs[21][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16449_
  (
    .D(_00945_ ^ glitches[1461]),
    .Q(\cpuregs[21][13] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16450_
  (
    .D(_00946_ ^ glitches[1462]),
    .Q(\cpuregs[21][14] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16451_
  (
    .D(_00947_ ^ glitches[1463]),
    .Q(\cpuregs[21][15] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16452_
  (
    .D(_00948_ ^ glitches[1464]),
    .Q(\cpuregs[21][16] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16453_
  (
    .D(_00949_ ^ glitches[1465]),
    .Q(\cpuregs[21][17] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16454_
  (
    .D(_00950_ ^ glitches[1466]),
    .Q(\cpuregs[21][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16455_
  (
    .D(_00951_ ^ glitches[1467]),
    .Q(\cpuregs[21][19] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16456_
  (
    .D(_00953_ ^ glitches[1468]),
    .Q(\cpuregs[21][20] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16457_
  (
    .D(_00954_ ^ glitches[1469]),
    .Q(\cpuregs[21][21] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16458_
  (
    .D(_00955_ ^ glitches[1470]),
    .Q(\cpuregs[21][22] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16459_
  (
    .D(_00956_ ^ glitches[1471]),
    .Q(\cpuregs[21][23] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16460_
  (
    .D(_00957_ ^ glitches[1472]),
    .Q(\cpuregs[21][24] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16461_
  (
    .D(_00958_ ^ glitches[1473]),
    .Q(\cpuregs[21][25] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16462_
  (
    .D(_00959_ ^ glitches[1474]),
    .Q(\cpuregs[21][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16463_
  (
    .D(_00960_ ^ glitches[1475]),
    .Q(\cpuregs[21][27] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16464_
  (
    .D(_00961_ ^ glitches[1476]),
    .Q(\cpuregs[21][28] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16465_
  (
    .D(_00962_ ^ glitches[1477]),
    .Q(\cpuregs[21][29] ),
    .CLK(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16466_
  (
    .D(_00964_ ^ glitches[1478]),
    .Q(\cpuregs[21][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16467_
  (
    .D(_00965_ ^ glitches[1479]),
    .Q(\cpuregs[21][31] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16468_
  (
    .D(_01485_ ^ glitches[1480]),
    .Q(\cpuregs[8][0] ),
    .CLK(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16469_
  (
    .D(_01496_ ^ glitches[1481]),
    .Q(\cpuregs[8][1] ),
    .CLK(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16470_
  (
    .D(_01507_ ^ glitches[1482]),
    .Q(\cpuregs[8][2] ),
    .CLK(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16471_
  (
    .D(_01510_ ^ glitches[1483]),
    .Q(\cpuregs[8][3] ),
    .CLK(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16472_
  (
    .D(_01511_ ^ glitches[1484]),
    .Q(\cpuregs[8][4] ),
    .CLK(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16473_
  (
    .D(_01512_ ^ glitches[1485]),
    .Q(\cpuregs[8][5] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16474_
  (
    .D(_01513_ ^ glitches[1486]),
    .Q(\cpuregs[8][6] ),
    .CLK(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16475_
  (
    .D(_01514_ ^ glitches[1487]),
    .Q(\cpuregs[8][7] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16476_
  (
    .D(_01515_ ^ glitches[1488]),
    .Q(\cpuregs[8][8] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16477_
  (
    .D(_01516_ ^ glitches[1489]),
    .Q(\cpuregs[8][9] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16478_
  (
    .D(_01486_ ^ glitches[1490]),
    .Q(\cpuregs[8][10] ),
    .CLK(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16479_
  (
    .D(_01487_ ^ glitches[1491]),
    .Q(\cpuregs[8][11] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16480_
  (
    .D(_01488_ ^ glitches[1492]),
    .Q(\cpuregs[8][12] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16481_
  (
    .D(_01489_ ^ glitches[1493]),
    .Q(\cpuregs[8][13] ),
    .CLK(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16482_
  (
    .D(_01490_ ^ glitches[1494]),
    .Q(\cpuregs[8][14] ),
    .CLK(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16483_
  (
    .D(_01491_ ^ glitches[1495]),
    .Q(\cpuregs[8][15] ),
    .CLK(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16484_
  (
    .D(_01492_ ^ glitches[1496]),
    .Q(\cpuregs[8][16] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16485_
  (
    .D(_01493_ ^ glitches[1497]),
    .Q(\cpuregs[8][17] ),
    .CLK(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16486_
  (
    .D(_01494_ ^ glitches[1498]),
    .Q(\cpuregs[8][18] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16487_
  (
    .D(_01495_ ^ glitches[1499]),
    .Q(\cpuregs[8][19] ),
    .CLK(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16488_
  (
    .D(_01497_ ^ glitches[1500]),
    .Q(\cpuregs[8][20] ),
    .CLK(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16489_
  (
    .D(_01498_ ^ glitches[1501]),
    .Q(\cpuregs[8][21] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16490_
  (
    .D(_01499_ ^ glitches[1502]),
    .Q(\cpuregs[8][22] ),
    .CLK(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16491_
  (
    .D(_01500_ ^ glitches[1503]),
    .Q(\cpuregs[8][23] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16492_
  (
    .D(_01501_ ^ glitches[1504]),
    .Q(\cpuregs[8][24] ),
    .CLK(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16493_
  (
    .D(_01502_ ^ glitches[1505]),
    .Q(\cpuregs[8][25] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16494_
  (
    .D(_01503_ ^ glitches[1506]),
    .Q(\cpuregs[8][26] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16495_
  (
    .D(_01504_ ^ glitches[1507]),
    .Q(\cpuregs[8][27] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16496_
  (
    .D(_01505_ ^ glitches[1508]),
    .Q(\cpuregs[8][28] ),
    .CLK(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16497_
  (
    .D(_01506_ ^ glitches[1509]),
    .Q(\cpuregs[8][29] ),
    .CLK(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16498_
  (
    .D(_01508_ ^ glitches[1510]),
    .Q(\cpuregs[8][30] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16499_
  (
    .D(_01509_ ^ glitches[1511]),
    .Q(\cpuregs[8][31] ),
    .CLK(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16500_
  (
    .D(_00813_ ^ glitches[1512]),
    .Q(\cpuregs[18][0] ),
    .CLK(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16501_
  (
    .D(_00824_ ^ glitches[1513]),
    .Q(\cpuregs[18][1] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16502_
  (
    .D(_00835_ ^ glitches[1514]),
    .Q(\cpuregs[18][2] ),
    .CLK(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16503_
  (
    .D(_00838_ ^ glitches[1515]),
    .Q(\cpuregs[18][3] ),
    .CLK(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16504_
  (
    .D(_00839_ ^ glitches[1516]),
    .Q(\cpuregs[18][4] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16505_
  (
    .D(_00840_ ^ glitches[1517]),
    .Q(\cpuregs[18][5] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16506_
  (
    .D(_00841_ ^ glitches[1518]),
    .Q(\cpuregs[18][6] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16507_
  (
    .D(_00842_ ^ glitches[1519]),
    .Q(\cpuregs[18][7] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16508_
  (
    .D(_00843_ ^ glitches[1520]),
    .Q(\cpuregs[18][8] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16509_
  (
    .D(_00844_ ^ glitches[1521]),
    .Q(\cpuregs[18][9] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16510_
  (
    .D(_00814_ ^ glitches[1522]),
    .Q(\cpuregs[18][10] ),
    .CLK(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16511_
  (
    .D(_00815_ ^ glitches[1523]),
    .Q(\cpuregs[18][11] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16512_
  (
    .D(_00816_ ^ glitches[1524]),
    .Q(\cpuregs[18][12] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16513_
  (
    .D(_00817_ ^ glitches[1525]),
    .Q(\cpuregs[18][13] ),
    .CLK(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16514_
  (
    .D(_00818_ ^ glitches[1526]),
    .Q(\cpuregs[18][14] ),
    .CLK(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16515_
  (
    .D(_00819_ ^ glitches[1527]),
    .Q(\cpuregs[18][15] ),
    .CLK(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16516_
  (
    .D(_00820_ ^ glitches[1528]),
    .Q(\cpuregs[18][16] ),
    .CLK(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16517_
  (
    .D(_00821_ ^ glitches[1529]),
    .Q(\cpuregs[18][17] ),
    .CLK(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16518_
  (
    .D(_00822_ ^ glitches[1530]),
    .Q(\cpuregs[18][18] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16519_
  (
    .D(_00823_ ^ glitches[1531]),
    .Q(\cpuregs[18][19] ),
    .CLK(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16520_
  (
    .D(_00825_ ^ glitches[1532]),
    .Q(\cpuregs[18][20] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16521_
  (
    .D(_00826_ ^ glitches[1533]),
    .Q(\cpuregs[18][21] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16522_
  (
    .D(_00827_ ^ glitches[1534]),
    .Q(\cpuregs[18][22] ),
    .CLK(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16523_
  (
    .D(_00828_ ^ glitches[1535]),
    .Q(\cpuregs[18][23] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16524_
  (
    .D(_00829_ ^ glitches[1536]),
    .Q(\cpuregs[18][24] ),
    .CLK(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16525_
  (
    .D(_00830_ ^ glitches[1537]),
    .Q(\cpuregs[18][25] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16526_
  (
    .D(_00831_ ^ glitches[1538]),
    .Q(\cpuregs[18][26] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16527_
  (
    .D(_00832_ ^ glitches[1539]),
    .Q(\cpuregs[18][27] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16528_
  (
    .D(_00833_ ^ glitches[1540]),
    .Q(\cpuregs[18][28] ),
    .CLK(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16529_
  (
    .D(_00834_ ^ glitches[1541]),
    .Q(\cpuregs[18][29] ),
    .CLK(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16530_
  (
    .D(_00836_ ^ glitches[1542]),
    .Q(\cpuregs[18][30] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16531_
  (
    .D(_00837_ ^ glitches[1543]),
    .Q(\cpuregs[18][31] ),
    .CLK(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16532_
  (
    .D(_01261_ ^ glitches[1544]),
    .Q(\cpuregs[30][0] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16533_
  (
    .D(_01272_ ^ glitches[1545]),
    .Q(\cpuregs[30][1] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16534_
  (
    .D(_01283_ ^ glitches[1546]),
    .Q(\cpuregs[30][2] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16535_
  (
    .D(_01286_ ^ glitches[1547]),
    .Q(\cpuregs[30][3] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16536_
  (
    .D(_01287_ ^ glitches[1548]),
    .Q(\cpuregs[30][4] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16537_
  (
    .D(_01288_ ^ glitches[1549]),
    .Q(\cpuregs[30][5] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16538_
  (
    .D(_01289_ ^ glitches[1550]),
    .Q(\cpuregs[30][6] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16539_
  (
    .D(_01290_ ^ glitches[1551]),
    .Q(\cpuregs[30][7] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16540_
  (
    .D(_01291_ ^ glitches[1552]),
    .Q(\cpuregs[30][8] ),
    .CLK(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16541_
  (
    .D(_01292_ ^ glitches[1553]),
    .Q(\cpuregs[30][9] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16542_
  (
    .D(_01262_ ^ glitches[1554]),
    .Q(\cpuregs[30][10] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16543_
  (
    .D(_01263_ ^ glitches[1555]),
    .Q(\cpuregs[30][11] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16544_
  (
    .D(_01264_ ^ glitches[1556]),
    .Q(\cpuregs[30][12] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16545_
  (
    .D(_01265_ ^ glitches[1557]),
    .Q(\cpuregs[30][13] ),
    .CLK(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16546_
  (
    .D(_01266_ ^ glitches[1558]),
    .Q(\cpuregs[30][14] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16547_
  (
    .D(_01267_ ^ glitches[1559]),
    .Q(\cpuregs[30][15] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16548_
  (
    .D(_01268_ ^ glitches[1560]),
    .Q(\cpuregs[30][16] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16549_
  (
    .D(_01269_ ^ glitches[1561]),
    .Q(\cpuregs[30][17] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16550_
  (
    .D(_01270_ ^ glitches[1562]),
    .Q(\cpuregs[30][18] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16551_
  (
    .D(_01271_ ^ glitches[1563]),
    .Q(\cpuregs[30][19] ),
    .CLK(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16552_
  (
    .D(_01273_ ^ glitches[1564]),
    .Q(\cpuregs[30][20] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16553_
  (
    .D(_01274_ ^ glitches[1565]),
    .Q(\cpuregs[30][21] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16554_
  (
    .D(_01275_ ^ glitches[1566]),
    .Q(\cpuregs[30][22] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16555_
  (
    .D(_01276_ ^ glitches[1567]),
    .Q(\cpuregs[30][23] ),
    .CLK(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16556_
  (
    .D(_01277_ ^ glitches[1568]),
    .Q(\cpuregs[30][24] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16557_
  (
    .D(_01278_ ^ glitches[1569]),
    .Q(\cpuregs[30][25] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16558_
  (
    .D(_01279_ ^ glitches[1570]),
    .Q(\cpuregs[30][26] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16559_
  (
    .D(_01280_ ^ glitches[1571]),
    .Q(\cpuregs[30][27] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16560_
  (
    .D(_01281_ ^ glitches[1572]),
    .Q(\cpuregs[30][28] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16561_
  (
    .D(_01282_ ^ glitches[1573]),
    .Q(\cpuregs[30][29] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16562_
  (
    .D(_01284_ ^ glitches[1574]),
    .Q(\cpuregs[30][30] ),
    .CLK(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16563_
  (
    .D(_01285_ ^ glitches[1575]),
    .Q(\cpuregs[30][31] ),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16564_
  (
    .D(_01293_ ^ glitches[1576]),
    .Q(\cpuregs[31][0] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16565_
  (
    .D(_01304_ ^ glitches[1577]),
    .Q(\cpuregs[31][1] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16566_
  (
    .D(_01315_ ^ glitches[1578]),
    .Q(\cpuregs[31][2] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16567_
  (
    .D(_01318_ ^ glitches[1579]),
    .Q(\cpuregs[31][3] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16568_
  (
    .D(_01319_ ^ glitches[1580]),
    .Q(\cpuregs[31][4] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16569_
  (
    .D(_01320_ ^ glitches[1581]),
    .Q(\cpuregs[31][5] ),
    .CLK(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16570_
  (
    .D(_01321_ ^ glitches[1582]),
    .Q(\cpuregs[31][6] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16571_
  (
    .D(_01322_ ^ glitches[1583]),
    .Q(\cpuregs[31][7] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16572_
  (
    .D(_01323_ ^ glitches[1584]),
    .Q(\cpuregs[31][8] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16573_
  (
    .D(_01324_ ^ glitches[1585]),
    .Q(\cpuregs[31][9] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16574_
  (
    .D(_01294_ ^ glitches[1586]),
    .Q(\cpuregs[31][10] ),
    .CLK(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16575_
  (
    .D(_01295_ ^ glitches[1587]),
    .Q(\cpuregs[31][11] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16576_
  (
    .D(_01296_ ^ glitches[1588]),
    .Q(\cpuregs[31][12] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16577_
  (
    .D(_01297_ ^ glitches[1589]),
    .Q(\cpuregs[31][13] ),
    .CLK(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16578_
  (
    .D(_01298_ ^ glitches[1590]),
    .Q(\cpuregs[31][14] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16579_
  (
    .D(_01299_ ^ glitches[1591]),
    .Q(\cpuregs[31][15] ),
    .CLK(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16580_
  (
    .D(_01300_ ^ glitches[1592]),
    .Q(\cpuregs[31][16] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16581_
  (
    .D(_01301_ ^ glitches[1593]),
    .Q(\cpuregs[31][17] ),
    .CLK(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16582_
  (
    .D(_01302_ ^ glitches[1594]),
    .Q(\cpuregs[31][18] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16583_
  (
    .D(_01303_ ^ glitches[1595]),
    .Q(\cpuregs[31][19] ),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16584_
  (
    .D(_01305_ ^ glitches[1596]),
    .Q(\cpuregs[31][20] ),
    .CLK(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16585_
  (
    .D(_01306_ ^ glitches[1597]),
    .Q(\cpuregs[31][21] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16586_
  (
    .D(_01307_ ^ glitches[1598]),
    .Q(\cpuregs[31][22] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16587_
  (
    .D(_01308_ ^ glitches[1599]),
    .Q(\cpuregs[31][23] ),
    .CLK(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16588_
  (
    .D(_01309_ ^ glitches[1600]),
    .Q(\cpuregs[31][24] ),
    .CLK(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16589_
  (
    .D(_01310_ ^ glitches[1601]),
    .Q(\cpuregs[31][25] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16590_
  (
    .D(_01311_ ^ glitches[1602]),
    .Q(\cpuregs[31][26] ),
    .CLK(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16591_
  (
    .D(_01312_ ^ glitches[1603]),
    .Q(\cpuregs[31][27] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16592_
  (
    .D(_01313_ ^ glitches[1604]),
    .Q(\cpuregs[31][28] ),
    .CLK(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16593_
  (
    .D(_01314_ ^ glitches[1605]),
    .Q(\cpuregs[31][29] ),
    .CLK(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16594_
  (
    .D(_01316_ ^ glitches[1606]),
    .Q(\cpuregs[31][30] ),
    .CLK(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16595_
  (
    .D(_01317_ ^ glitches[1607]),
    .Q(\cpuregs[31][31] ),
    .CLK(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16596_
  (
    .D(_00197_ ^ glitches[1608]),
    .Q(\decoded_rs1[0] ),
    .CLK(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16597_
  (
    .D(_00198_ ^ glitches[1609]),
    .Q(\decoded_rs1[1] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16598_
  (
    .D(_00199_ ^ glitches[1610]),
    .Q(\decoded_rs1[2] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16599_
  (
    .D(_00200_ ^ glitches[1611]),
    .Q(\decoded_rs1[3] ),
    .CLK(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__dfxtp_4
  _16600_
  (
    .D(_00201_ ^ glitches[1612]),
    .Q(\decoded_rs1[4] ),
    .CLK(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__buf_1
  clkbuf_0_clk
  (
    .A(clk),
    .X(clknet_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_0_0_clk
  (
    .A(clknet_0_clk),
    .X(clknet_1_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_0_1_clk
  (
    .A(clknet_1_0_0_clk),
    .X(clknet_1_0_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_0_2_clk
  (
    .A(clknet_1_0_1_clk),
    .X(clknet_1_0_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_0_3_clk
  (
    .A(clknet_1_0_2_clk),
    .X(clknet_1_0_3_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_1_0_clk
  (
    .A(clknet_0_clk),
    .X(clknet_1_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_1_1_clk
  (
    .A(clknet_1_1_0_clk),
    .X(clknet_1_1_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_1_2_clk
  (
    .A(clknet_1_1_1_clk),
    .X(clknet_1_1_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_1_1_3_clk
  (
    .A(clknet_1_1_2_clk),
    .X(clknet_1_1_3_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_0_0_clk
  (
    .A(clknet_1_0_3_clk),
    .X(clknet_2_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_0_1_clk
  (
    .A(clknet_2_0_0_clk),
    .X(clknet_2_0_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_0_2_clk
  (
    .A(clknet_2_0_1_clk),
    .X(clknet_2_0_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_1_0_clk
  (
    .A(clknet_1_0_3_clk),
    .X(clknet_2_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_1_1_clk
  (
    .A(clknet_2_1_0_clk),
    .X(clknet_2_1_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_1_2_clk
  (
    .A(clknet_2_1_1_clk),
    .X(clknet_2_1_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_2_0_clk
  (
    .A(clknet_1_1_3_clk),
    .X(clknet_2_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_2_1_clk
  (
    .A(clknet_2_2_0_clk),
    .X(clknet_2_2_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_2_2_clk
  (
    .A(clknet_2_2_1_clk),
    .X(clknet_2_2_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_3_0_clk
  (
    .A(clknet_1_1_3_clk),
    .X(clknet_2_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_3_1_clk
  (
    .A(clknet_2_3_0_clk),
    .X(clknet_2_3_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_2_3_2_clk
  (
    .A(clknet_2_3_1_clk),
    .X(clknet_2_3_2_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_0_0_clk
  (
    .A(clknet_2_0_2_clk),
    .X(clknet_3_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_0_1_clk
  (
    .A(clknet_3_0_0_clk),
    .X(clknet_3_0_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_1_0_clk
  (
    .A(clknet_2_0_2_clk),
    .X(clknet_3_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_1_1_clk
  (
    .A(clknet_3_1_0_clk),
    .X(clknet_3_1_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_2_0_clk
  (
    .A(clknet_2_1_2_clk),
    .X(clknet_3_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_2_1_clk
  (
    .A(clknet_3_2_0_clk),
    .X(clknet_3_2_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_3_0_clk
  (
    .A(clknet_2_1_2_clk),
    .X(clknet_3_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_3_1_clk
  (
    .A(clknet_3_3_0_clk),
    .X(clknet_3_3_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_4_0_clk
  (
    .A(clknet_2_2_2_clk),
    .X(clknet_3_4_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_4_1_clk
  (
    .A(clknet_3_4_0_clk),
    .X(clknet_3_4_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_5_0_clk
  (
    .A(clknet_2_2_2_clk),
    .X(clknet_3_5_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_5_1_clk
  (
    .A(clknet_3_5_0_clk),
    .X(clknet_3_5_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_6_0_clk
  (
    .A(clknet_2_3_2_clk),
    .X(clknet_3_6_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_6_1_clk
  (
    .A(clknet_3_6_0_clk),
    .X(clknet_3_6_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_7_0_clk
  (
    .A(clknet_2_3_2_clk),
    .X(clknet_3_7_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_3_7_1_clk
  (
    .A(clknet_3_7_0_clk),
    .X(clknet_3_7_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_0_0_clk
  (
    .A(clknet_3_0_1_clk),
    .X(clknet_4_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_0_1_clk
  (
    .A(clknet_4_0_0_clk),
    .X(clknet_4_0_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_1_0_clk
  (
    .A(clknet_3_0_1_clk),
    .X(clknet_4_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_1_1_clk
  (
    .A(clknet_4_1_0_clk),
    .X(clknet_4_1_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_2_0_clk
  (
    .A(clknet_3_1_1_clk),
    .X(clknet_4_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_2_1_clk
  (
    .A(clknet_4_2_0_clk),
    .X(clknet_4_2_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_3_0_clk
  (
    .A(clknet_3_1_1_clk),
    .X(clknet_4_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_3_1_clk
  (
    .A(clknet_4_3_0_clk),
    .X(clknet_4_3_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_4_0_clk
  (
    .A(clknet_3_2_1_clk),
    .X(clknet_4_4_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_4_1_clk
  (
    .A(clknet_4_4_0_clk),
    .X(clknet_4_4_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_5_0_clk
  (
    .A(clknet_3_2_1_clk),
    .X(clknet_4_5_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_5_1_clk
  (
    .A(clknet_4_5_0_clk),
    .X(clknet_4_5_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_6_0_clk
  (
    .A(clknet_3_3_1_clk),
    .X(clknet_4_6_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_6_1_clk
  (
    .A(clknet_4_6_0_clk),
    .X(clknet_4_6_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_7_0_clk
  (
    .A(clknet_3_3_1_clk),
    .X(clknet_4_7_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_7_1_clk
  (
    .A(clknet_4_7_0_clk),
    .X(clknet_4_7_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_8_0_clk
  (
    .A(clknet_3_4_1_clk),
    .X(clknet_4_8_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_8_1_clk
  (
    .A(clknet_4_8_0_clk),
    .X(clknet_4_8_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_9_0_clk
  (
    .A(clknet_3_4_1_clk),
    .X(clknet_4_9_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_9_1_clk
  (
    .A(clknet_4_9_0_clk),
    .X(clknet_4_9_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_10_0_clk
  (
    .A(clknet_3_5_1_clk),
    .X(clknet_4_10_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_10_1_clk
  (
    .A(clknet_4_10_0_clk),
    .X(clknet_4_10_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_11_0_clk
  (
    .A(clknet_3_5_1_clk),
    .X(clknet_4_11_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_11_1_clk
  (
    .A(clknet_4_11_0_clk),
    .X(clknet_4_11_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_12_0_clk
  (
    .A(clknet_3_6_1_clk),
    .X(clknet_4_12_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_12_1_clk
  (
    .A(clknet_4_12_0_clk),
    .X(clknet_4_12_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_13_0_clk
  (
    .A(clknet_3_6_1_clk),
    .X(clknet_4_13_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_13_1_clk
  (
    .A(clknet_4_13_0_clk),
    .X(clknet_4_13_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_14_0_clk
  (
    .A(clknet_3_7_1_clk),
    .X(clknet_4_14_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_14_1_clk
  (
    .A(clknet_4_14_0_clk),
    .X(clknet_4_14_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_15_0_clk
  (
    .A(clknet_3_7_1_clk),
    .X(clknet_4_15_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_4_15_1_clk
  (
    .A(clknet_4_15_0_clk),
    .X(clknet_4_15_1_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_0_0_clk
  (
    .A(clknet_4_0_1_clk),
    .X(clknet_5_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_1_0_clk
  (
    .A(clknet_4_0_1_clk),
    .X(clknet_5_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_2_0_clk
  (
    .A(clknet_4_1_1_clk),
    .X(clknet_5_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_3_0_clk
  (
    .A(clknet_4_1_1_clk),
    .X(clknet_5_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_4_0_clk
  (
    .A(clknet_4_2_1_clk),
    .X(clknet_5_4_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_5_0_clk
  (
    .A(clknet_4_2_1_clk),
    .X(clknet_5_5_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_6_0_clk
  (
    .A(clknet_4_3_1_clk),
    .X(clknet_5_6_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_7_0_clk
  (
    .A(clknet_4_3_1_clk),
    .X(clknet_5_7_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_8_0_clk
  (
    .A(clknet_4_4_1_clk),
    .X(clknet_5_8_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_9_0_clk
  (
    .A(clknet_4_4_1_clk),
    .X(clknet_5_9_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_10_0_clk
  (
    .A(clknet_4_5_1_clk),
    .X(clknet_5_10_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_11_0_clk
  (
    .A(clknet_4_5_1_clk),
    .X(clknet_5_11_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_12_0_clk
  (
    .A(clknet_4_6_1_clk),
    .X(clknet_5_12_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_13_0_clk
  (
    .A(clknet_4_6_1_clk),
    .X(clknet_5_13_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_14_0_clk
  (
    .A(clknet_4_7_1_clk),
    .X(clknet_5_14_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_15_0_clk
  (
    .A(clknet_4_7_1_clk),
    .X(clknet_5_15_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_16_0_clk
  (
    .A(clknet_4_8_1_clk),
    .X(clknet_5_16_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_17_0_clk
  (
    .A(clknet_4_8_1_clk),
    .X(clknet_5_17_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_18_0_clk
  (
    .A(clknet_4_9_1_clk),
    .X(clknet_5_18_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_19_0_clk
  (
    .A(clknet_4_9_1_clk),
    .X(clknet_5_19_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_20_0_clk
  (
    .A(clknet_4_10_1_clk),
    .X(clknet_5_20_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_21_0_clk
  (
    .A(clknet_4_10_1_clk),
    .X(clknet_5_21_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_22_0_clk
  (
    .A(clknet_4_11_1_clk),
    .X(clknet_5_22_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_23_0_clk
  (
    .A(clknet_4_11_1_clk),
    .X(clknet_5_23_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_24_0_clk
  (
    .A(clknet_4_12_1_clk),
    .X(clknet_5_24_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_25_0_clk
  (
    .A(clknet_4_12_1_clk),
    .X(clknet_5_25_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_26_0_clk
  (
    .A(clknet_4_13_1_clk),
    .X(clknet_5_26_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_27_0_clk
  (
    .A(clknet_4_13_1_clk),
    .X(clknet_5_27_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_28_0_clk
  (
    .A(clknet_4_14_1_clk),
    .X(clknet_5_28_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_29_0_clk
  (
    .A(clknet_4_14_1_clk),
    .X(clknet_5_29_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_30_0_clk
  (
    .A(clknet_4_15_1_clk),
    .X(clknet_5_30_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_5_31_0_clk
  (
    .A(clknet_4_15_1_clk),
    .X(clknet_5_31_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_0_0_clk
  (
    .A(clknet_5_0_0_clk),
    .X(clknet_6_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_1_0_clk
  (
    .A(clknet_5_0_0_clk),
    .X(clknet_6_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_2_0_clk
  (
    .A(clknet_5_1_0_clk),
    .X(clknet_6_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_3_0_clk
  (
    .A(clknet_5_1_0_clk),
    .X(clknet_6_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_4_0_clk
  (
    .A(clknet_5_2_0_clk),
    .X(clknet_6_4_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_5_0_clk
  (
    .A(clknet_5_2_0_clk),
    .X(clknet_6_5_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_6_0_clk
  (
    .A(clknet_5_3_0_clk),
    .X(clknet_6_6_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_7_0_clk
  (
    .A(clknet_5_3_0_clk),
    .X(clknet_6_7_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_8_0_clk
  (
    .A(clknet_5_4_0_clk),
    .X(clknet_6_8_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_9_0_clk
  (
    .A(clknet_5_4_0_clk),
    .X(clknet_6_9_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_10_0_clk
  (
    .A(clknet_5_5_0_clk),
    .X(clknet_6_10_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_11_0_clk
  (
    .A(clknet_5_5_0_clk),
    .X(clknet_6_11_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_12_0_clk
  (
    .A(clknet_5_6_0_clk),
    .X(clknet_6_12_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_13_0_clk
  (
    .A(clknet_5_6_0_clk),
    .X(clknet_6_13_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_14_0_clk
  (
    .A(clknet_5_7_0_clk),
    .X(clknet_6_14_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_15_0_clk
  (
    .A(clknet_5_7_0_clk),
    .X(clknet_6_15_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_16_0_clk
  (
    .A(clknet_5_8_0_clk),
    .X(clknet_6_16_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_17_0_clk
  (
    .A(clknet_5_8_0_clk),
    .X(clknet_6_17_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_18_0_clk
  (
    .A(clknet_5_9_0_clk),
    .X(clknet_6_18_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_19_0_clk
  (
    .A(clknet_5_9_0_clk),
    .X(clknet_6_19_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_20_0_clk
  (
    .A(clknet_5_10_0_clk),
    .X(clknet_6_20_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_21_0_clk
  (
    .A(clknet_5_10_0_clk),
    .X(clknet_6_21_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_22_0_clk
  (
    .A(clknet_5_11_0_clk),
    .X(clknet_6_22_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_23_0_clk
  (
    .A(clknet_5_11_0_clk),
    .X(clknet_6_23_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_24_0_clk
  (
    .A(clknet_5_12_0_clk),
    .X(clknet_6_24_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_25_0_clk
  (
    .A(clknet_5_12_0_clk),
    .X(clknet_6_25_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_26_0_clk
  (
    .A(clknet_5_13_0_clk),
    .X(clknet_6_26_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_27_0_clk
  (
    .A(clknet_5_13_0_clk),
    .X(clknet_6_27_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_28_0_clk
  (
    .A(clknet_5_14_0_clk),
    .X(clknet_6_28_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_29_0_clk
  (
    .A(clknet_5_14_0_clk),
    .X(clknet_6_29_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_30_0_clk
  (
    .A(clknet_5_15_0_clk),
    .X(clknet_6_30_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_31_0_clk
  (
    .A(clknet_5_15_0_clk),
    .X(clknet_6_31_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_32_0_clk
  (
    .A(clknet_5_16_0_clk),
    .X(clknet_6_32_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_33_0_clk
  (
    .A(clknet_5_16_0_clk),
    .X(clknet_6_33_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_34_0_clk
  (
    .A(clknet_5_17_0_clk),
    .X(clknet_6_34_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_35_0_clk
  (
    .A(clknet_5_17_0_clk),
    .X(clknet_6_35_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_36_0_clk
  (
    .A(clknet_5_18_0_clk),
    .X(clknet_6_36_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_37_0_clk
  (
    .A(clknet_5_18_0_clk),
    .X(clknet_6_37_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_38_0_clk
  (
    .A(clknet_5_19_0_clk),
    .X(clknet_6_38_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_39_0_clk
  (
    .A(clknet_5_19_0_clk),
    .X(clknet_6_39_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_40_0_clk
  (
    .A(clknet_5_20_0_clk),
    .X(clknet_6_40_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_41_0_clk
  (
    .A(clknet_5_20_0_clk),
    .X(clknet_6_41_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_42_0_clk
  (
    .A(clknet_5_21_0_clk),
    .X(clknet_6_42_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_43_0_clk
  (
    .A(clknet_5_21_0_clk),
    .X(clknet_6_43_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_44_0_clk
  (
    .A(clknet_5_22_0_clk),
    .X(clknet_6_44_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_45_0_clk
  (
    .A(clknet_5_22_0_clk),
    .X(clknet_6_45_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_46_0_clk
  (
    .A(clknet_5_23_0_clk),
    .X(clknet_6_46_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_47_0_clk
  (
    .A(clknet_5_23_0_clk),
    .X(clknet_6_47_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_48_0_clk
  (
    .A(clknet_5_24_0_clk),
    .X(clknet_6_48_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_49_0_clk
  (
    .A(clknet_5_24_0_clk),
    .X(clknet_6_49_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_50_0_clk
  (
    .A(clknet_5_25_0_clk),
    .X(clknet_6_50_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_51_0_clk
  (
    .A(clknet_5_25_0_clk),
    .X(clknet_6_51_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_52_0_clk
  (
    .A(clknet_5_26_0_clk),
    .X(clknet_6_52_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_53_0_clk
  (
    .A(clknet_5_26_0_clk),
    .X(clknet_6_53_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_54_0_clk
  (
    .A(clknet_5_27_0_clk),
    .X(clknet_6_54_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_55_0_clk
  (
    .A(clknet_5_27_0_clk),
    .X(clknet_6_55_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_56_0_clk
  (
    .A(clknet_5_28_0_clk),
    .X(clknet_6_56_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_57_0_clk
  (
    .A(clknet_5_28_0_clk),
    .X(clknet_6_57_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_58_0_clk
  (
    .A(clknet_5_29_0_clk),
    .X(clknet_6_58_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_59_0_clk
  (
    .A(clknet_5_29_0_clk),
    .X(clknet_6_59_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_60_0_clk
  (
    .A(clknet_5_30_0_clk),
    .X(clknet_6_60_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_61_0_clk
  (
    .A(clknet_5_30_0_clk),
    .X(clknet_6_61_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_62_0_clk
  (
    .A(clknet_5_31_0_clk),
    .X(clknet_6_62_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_6_63_0_clk
  (
    .A(clknet_5_31_0_clk),
    .X(clknet_6_63_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_0_0_clk
  (
    .A(clknet_6_0_0_clk),
    .X(clknet_7_0_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_1_0_clk
  (
    .A(clknet_6_0_0_clk),
    .X(clknet_7_1_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_2_0_clk
  (
    .A(clknet_6_1_0_clk),
    .X(clknet_7_2_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_3_0_clk
  (
    .A(clknet_6_1_0_clk),
    .X(clknet_7_3_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_4_0_clk
  (
    .A(clknet_6_2_0_clk),
    .X(clknet_7_4_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_5_0_clk
  (
    .A(clknet_6_2_0_clk),
    .X(clknet_7_5_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_6_0_clk
  (
    .A(clknet_6_3_0_clk),
    .X(clknet_7_6_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_7_0_clk
  (
    .A(clknet_6_3_0_clk),
    .X(clknet_7_7_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_8_0_clk
  (
    .A(clknet_6_4_0_clk),
    .X(clknet_7_8_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_9_0_clk
  (
    .A(clknet_6_4_0_clk),
    .X(clknet_7_9_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_10_0_clk
  (
    .A(clknet_6_5_0_clk),
    .X(clknet_7_10_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_11_0_clk
  (
    .A(clknet_6_5_0_clk),
    .X(clknet_7_11_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_12_0_clk
  (
    .A(clknet_6_6_0_clk),
    .X(clknet_7_12_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_13_0_clk
  (
    .A(clknet_6_6_0_clk),
    .X(clknet_7_13_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_14_0_clk
  (
    .A(clknet_6_7_0_clk),
    .X(clknet_7_14_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_15_0_clk
  (
    .A(clknet_6_7_0_clk),
    .X(clknet_7_15_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_16_0_clk
  (
    .A(clknet_6_8_0_clk),
    .X(clknet_7_16_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_17_0_clk
  (
    .A(clknet_6_8_0_clk),
    .X(clknet_7_17_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_18_0_clk
  (
    .A(clknet_6_9_0_clk),
    .X(clknet_7_18_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_19_0_clk
  (
    .A(clknet_6_9_0_clk),
    .X(clknet_7_19_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_20_0_clk
  (
    .A(clknet_6_10_0_clk),
    .X(clknet_7_20_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_21_0_clk
  (
    .A(clknet_6_10_0_clk),
    .X(clknet_7_21_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_22_0_clk
  (
    .A(clknet_6_11_0_clk),
    .X(clknet_7_22_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_23_0_clk
  (
    .A(clknet_6_11_0_clk),
    .X(clknet_7_23_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_24_0_clk
  (
    .A(clknet_6_12_0_clk),
    .X(clknet_7_24_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_25_0_clk
  (
    .A(clknet_6_12_0_clk),
    .X(clknet_7_25_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_26_0_clk
  (
    .A(clknet_6_13_0_clk),
    .X(clknet_7_26_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_27_0_clk
  (
    .A(clknet_6_13_0_clk),
    .X(clknet_7_27_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_28_0_clk
  (
    .A(clknet_6_14_0_clk),
    .X(clknet_7_28_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_29_0_clk
  (
    .A(clknet_6_14_0_clk),
    .X(clknet_7_29_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_30_0_clk
  (
    .A(clknet_6_15_0_clk),
    .X(clknet_7_30_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_31_0_clk
  (
    .A(clknet_6_15_0_clk),
    .X(clknet_7_31_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_32_0_clk
  (
    .A(clknet_6_16_0_clk),
    .X(clknet_7_32_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_33_0_clk
  (
    .A(clknet_6_16_0_clk),
    .X(clknet_7_33_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_34_0_clk
  (
    .A(clknet_6_17_0_clk),
    .X(clknet_7_34_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_35_0_clk
  (
    .A(clknet_6_17_0_clk),
    .X(clknet_7_35_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_36_0_clk
  (
    .A(clknet_6_18_0_clk),
    .X(clknet_7_36_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_37_0_clk
  (
    .A(clknet_6_18_0_clk),
    .X(clknet_7_37_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_38_0_clk
  (
    .A(clknet_6_19_0_clk),
    .X(clknet_7_38_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_39_0_clk
  (
    .A(clknet_6_19_0_clk),
    .X(clknet_7_39_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_40_0_clk
  (
    .A(clknet_6_20_0_clk),
    .X(clknet_7_40_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_41_0_clk
  (
    .A(clknet_6_20_0_clk),
    .X(clknet_7_41_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_42_0_clk
  (
    .A(clknet_6_21_0_clk),
    .X(clknet_7_42_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_43_0_clk
  (
    .A(clknet_6_21_0_clk),
    .X(clknet_7_43_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_44_0_clk
  (
    .A(clknet_6_22_0_clk),
    .X(clknet_7_44_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_45_0_clk
  (
    .A(clknet_6_22_0_clk),
    .X(clknet_7_45_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_46_0_clk
  (
    .A(clknet_6_23_0_clk),
    .X(clknet_7_46_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_47_0_clk
  (
    .A(clknet_6_23_0_clk),
    .X(clknet_7_47_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_48_0_clk
  (
    .A(clknet_6_24_0_clk),
    .X(clknet_7_48_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_49_0_clk
  (
    .A(clknet_6_24_0_clk),
    .X(clknet_7_49_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_50_0_clk
  (
    .A(clknet_6_25_0_clk),
    .X(clknet_7_50_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_51_0_clk
  (
    .A(clknet_6_25_0_clk),
    .X(clknet_7_51_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_52_0_clk
  (
    .A(clknet_6_26_0_clk),
    .X(clknet_7_52_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_53_0_clk
  (
    .A(clknet_6_26_0_clk),
    .X(clknet_7_53_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_54_0_clk
  (
    .A(clknet_6_27_0_clk),
    .X(clknet_7_54_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_55_0_clk
  (
    .A(clknet_6_27_0_clk),
    .X(clknet_7_55_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_56_0_clk
  (
    .A(clknet_6_28_0_clk),
    .X(clknet_7_56_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_57_0_clk
  (
    .A(clknet_6_28_0_clk),
    .X(clknet_7_57_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_58_0_clk
  (
    .A(clknet_6_29_0_clk),
    .X(clknet_7_58_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_59_0_clk
  (
    .A(clknet_6_29_0_clk),
    .X(clknet_7_59_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_60_0_clk
  (
    .A(clknet_6_30_0_clk),
    .X(clknet_7_60_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_61_0_clk
  (
    .A(clknet_6_30_0_clk),
    .X(clknet_7_61_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_62_0_clk
  (
    .A(clknet_6_31_0_clk),
    .X(clknet_7_62_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_63_0_clk
  (
    .A(clknet_6_31_0_clk),
    .X(clknet_7_63_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_64_0_clk
  (
    .A(clknet_6_32_0_clk),
    .X(clknet_7_64_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_65_0_clk
  (
    .A(clknet_6_32_0_clk),
    .X(clknet_7_65_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_66_0_clk
  (
    .A(clknet_6_33_0_clk),
    .X(clknet_7_66_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_67_0_clk
  (
    .A(clknet_6_33_0_clk),
    .X(clknet_7_67_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_68_0_clk
  (
    .A(clknet_6_34_0_clk),
    .X(clknet_7_68_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_69_0_clk
  (
    .A(clknet_6_34_0_clk),
    .X(clknet_7_69_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_70_0_clk
  (
    .A(clknet_6_35_0_clk),
    .X(clknet_7_70_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_71_0_clk
  (
    .A(clknet_6_35_0_clk),
    .X(clknet_7_71_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_72_0_clk
  (
    .A(clknet_6_36_0_clk),
    .X(clknet_7_72_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_73_0_clk
  (
    .A(clknet_6_36_0_clk),
    .X(clknet_7_73_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_74_0_clk
  (
    .A(clknet_6_37_0_clk),
    .X(clknet_7_74_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_75_0_clk
  (
    .A(clknet_6_37_0_clk),
    .X(clknet_7_75_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_76_0_clk
  (
    .A(clknet_6_38_0_clk),
    .X(clknet_7_76_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_77_0_clk
  (
    .A(clknet_6_38_0_clk),
    .X(clknet_7_77_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_78_0_clk
  (
    .A(clknet_6_39_0_clk),
    .X(clknet_7_78_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_79_0_clk
  (
    .A(clknet_6_39_0_clk),
    .X(clknet_7_79_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_80_0_clk
  (
    .A(clknet_6_40_0_clk),
    .X(clknet_7_80_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_81_0_clk
  (
    .A(clknet_6_40_0_clk),
    .X(clknet_7_81_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_82_0_clk
  (
    .A(clknet_6_41_0_clk),
    .X(clknet_7_82_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_83_0_clk
  (
    .A(clknet_6_41_0_clk),
    .X(clknet_7_83_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_84_0_clk
  (
    .A(clknet_6_42_0_clk),
    .X(clknet_7_84_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_85_0_clk
  (
    .A(clknet_6_42_0_clk),
    .X(clknet_7_85_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_86_0_clk
  (
    .A(clknet_6_43_0_clk),
    .X(clknet_7_86_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_87_0_clk
  (
    .A(clknet_6_43_0_clk),
    .X(clknet_7_87_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_88_0_clk
  (
    .A(clknet_6_44_0_clk),
    .X(clknet_7_88_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_89_0_clk
  (
    .A(clknet_6_44_0_clk),
    .X(clknet_7_89_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_90_0_clk
  (
    .A(clknet_6_45_0_clk),
    .X(clknet_7_90_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_91_0_clk
  (
    .A(clknet_6_45_0_clk),
    .X(clknet_7_91_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_92_0_clk
  (
    .A(clknet_6_46_0_clk),
    .X(clknet_7_92_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_93_0_clk
  (
    .A(clknet_6_46_0_clk),
    .X(clknet_7_93_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_94_0_clk
  (
    .A(clknet_6_47_0_clk),
    .X(clknet_7_94_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_95_0_clk
  (
    .A(clknet_6_47_0_clk),
    .X(clknet_7_95_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_96_0_clk
  (
    .A(clknet_6_48_0_clk),
    .X(clknet_7_96_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_97_0_clk
  (
    .A(clknet_6_48_0_clk),
    .X(clknet_7_97_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_98_0_clk
  (
    .A(clknet_6_49_0_clk),
    .X(clknet_7_98_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_99_0_clk
  (
    .A(clknet_6_49_0_clk),
    .X(clknet_7_99_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_100_0_clk
  (
    .A(clknet_6_50_0_clk),
    .X(clknet_7_100_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_101_0_clk
  (
    .A(clknet_6_50_0_clk),
    .X(clknet_7_101_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_102_0_clk
  (
    .A(clknet_6_51_0_clk),
    .X(clknet_7_102_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_103_0_clk
  (
    .A(clknet_6_51_0_clk),
    .X(clknet_7_103_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_104_0_clk
  (
    .A(clknet_6_52_0_clk),
    .X(clknet_7_104_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_105_0_clk
  (
    .A(clknet_6_52_0_clk),
    .X(clknet_7_105_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_106_0_clk
  (
    .A(clknet_6_53_0_clk),
    .X(clknet_7_106_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_107_0_clk
  (
    .A(clknet_6_53_0_clk),
    .X(clknet_7_107_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_108_0_clk
  (
    .A(clknet_6_54_0_clk),
    .X(clknet_7_108_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_109_0_clk
  (
    .A(clknet_6_54_0_clk),
    .X(clknet_7_109_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_110_0_clk
  (
    .A(clknet_6_55_0_clk),
    .X(clknet_7_110_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_111_0_clk
  (
    .A(clknet_6_55_0_clk),
    .X(clknet_7_111_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_112_0_clk
  (
    .A(clknet_6_56_0_clk),
    .X(clknet_7_112_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_113_0_clk
  (
    .A(clknet_6_56_0_clk),
    .X(clknet_7_113_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_114_0_clk
  (
    .A(clknet_6_57_0_clk),
    .X(clknet_7_114_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_115_0_clk
  (
    .A(clknet_6_57_0_clk),
    .X(clknet_7_115_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_116_0_clk
  (
    .A(clknet_6_58_0_clk),
    .X(clknet_7_116_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_117_0_clk
  (
    .A(clknet_6_58_0_clk),
    .X(clknet_7_117_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_118_0_clk
  (
    .A(clknet_6_59_0_clk),
    .X(clknet_7_118_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_119_0_clk
  (
    .A(clknet_6_59_0_clk),
    .X(clknet_7_119_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_120_0_clk
  (
    .A(clknet_6_60_0_clk),
    .X(clknet_7_120_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_121_0_clk
  (
    .A(clknet_6_60_0_clk),
    .X(clknet_7_121_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_122_0_clk
  (
    .A(clknet_6_61_0_clk),
    .X(clknet_7_122_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_123_0_clk
  (
    .A(clknet_6_61_0_clk),
    .X(clknet_7_123_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_124_0_clk
  (
    .A(clknet_6_62_0_clk),
    .X(clknet_7_124_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_125_0_clk
  (
    .A(clknet_6_62_0_clk),
    .X(clknet_7_125_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_126_0_clk
  (
    .A(clknet_6_63_0_clk),
    .X(clknet_7_126_0_clk)
  );


  sky130_fd_sc_hs__clkbuf_1
  clkbuf_7_127_0_clk
  (
    .A(clknet_6_63_0_clk),
    .X(clknet_7_127_0_clk)
  );


endmodule

