#!/usr/bin/python3

import unittest
import fi_diff
import tempfile
import os

class TestFiDiff(unittest.TestCase):

    def test_equality(self):
        vf = tempfile.NamedTemporaryFile(suffix=".vcd")
        vf_truth = "./fi_diff_test_diff.vcd"
        nf = "./fi_diff_test_normal.vcd"
        ff = "./fi_diff_test_fault.vcd"
        fi_diff.dofi_diff([nf, ff, vf.name, "-vvvv"])

        # output test
        #os.system("cp " + vf.name + " " + vf_truth)
        diff = os.system("diff -u -I '^\$date' " + vf_truth + " " + vf.name) # Ignore the date line
        self.assertEqual(0, diff, "Difference with expected output")

        # Close/delete tempfiles
        vf.close()

    def test_content(self):
        nf = "./fi_diff_test_normal.vcd"
        ff = "./fi_diff_test_fault.vcd"
        vf = tempfile.NamedTemporaryFile(suffix=".vcd")
        fi_diff.parse_args([nf, ff, vf.name])
        vcdinfo = fi_diff.parse(nf, '')
        self.assertEqual(vcdinfo.timescale, "$timescale   1ns $end")

        # XXX test other info
        #print(vcdinfo.scopevar)
        #print(vcdinfo.longname)
        #print(vcdinfo.vcs)

        # Close/delete tempfiles
        vf.close()

    def test_calcdiff(self):
        fi_diff.parse_args(["x", "x", "x"]) # For options
        # Test simple equality
        a = [(1, ['x', 'y'], None)]
        b = [(1, ['x', 'y'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set(), r, "Same input should be equal")

        a = [(1, ['x', 'y'], None)]
        b = [(1, ['x'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set('y'), r, "y is present in set a and not in set b")

        a = [(1, ['x'], None)]
        b = [(1, ['x', 'y'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set('y'), r, "y is present in set b and not in set a")

        a = [(2, ['x', 'y'], None)]
        b = [(1, ['x', 'y'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set(['x', 'y']), r, "x and y are present in both sets but are updates at a different point in time")

        a = [(1, ['x', 'y'], None)]
        b = [(2, ['x', 'y'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set(['x', 'y']), r, "x and y are present in both sets but are updates at a different point in time")

        a = [(1, ['x', 'y'], None), (2, ['y'], None), (3, ['z'], None)]
        b = [(1, ['x', 'y'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set(['y', 'z']), r, "a has a bunch of trailing update")

        a = [(1, ['x', 'y'])]
        b = [(1, ['x', 'y']), (2, ['y']), (3, ['z'])]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set(['y', 'z']), r, "b has a bunch of trailing updates")

        a = [(1, ['x', 'y'], None), (2, ['y'], None), (4, ['z'], None)]
        b = [(1, ['x', 'y'], None), (3, ['y'], None), (4, ['z'], None)]
        r = fi_diff.calc_diff(a, b)
        self.assertEqual(set('y'), r, "y had another input one clock later")


if __name__ == '__main__':
    unittest.main()
