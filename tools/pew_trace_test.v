module otaes (clk_i,
    rst_ni,
    test_done_o,
    test_passed_o,
    aes_input,
    aes_key,
    aes_output);
 input clk_i;
 input rst_ni;
 output test_done_o;
 output test_passed_o;
 input [127:0] aes_input;
 input [127:0] aes_key;
 output [127:0] aes_output;

 sky130_fd_sc_hs__buf_4 _29960_ (.A(\aes.u_aes_core.u_aes_cipher_core.u_aes_cipher_control.dec_key_gen_q_reg.Q ),
    .X(_24543_));
 sky130_fd_sc_hs__clkdlyinv3sd2_1 _29961_ (.A(_24543_),
    .Y(_24544_));
 sky130_fd_sc_hs__buf_1 _29962_ (.A(_24544_),
    .X(_24545_));
 sky130_fd_sc_hs__buf_1 _29963_ (.A(aes_output[1]),
    .X(_24546_));
 sky130_fd_sc_hs__buf_4 _29964_ (.A(\aes.u_aes_core.u_aes_control.u_state_regs.u_impl_xilinx.q_reg[0].Q ),
    .X(_24547_));
 sky130_fd_sc_hs__nor2_4 _29965_ (.A(_24546_),
    .B(_24547_),
    .Y(_24548_));
 sky130_fd_sc_hs__buf_1 _29966_ (.A(\aes.u_aes_core.u_aes_control.u_state_regs.u_impl_xilinx.q_reg[4].Q ),
    .X(_24549_));
 sky130_fd_sc_hs__buf_4 _29967_ (.A(\aes.u_aes_core.u_aes_control.u_state_regs.u_impl_xilinx.q_reg[5].Q ),
    .X(_24550_));
 sky130_fd_sc_hs__buf_4 _29968_ (.A(\aes.u_aes_core.u_aes_control.u_state_regs.u_impl_xilinx.q_reg[1].Q ),
    .X(_24551_));
 sky130_fd_sc_hs__clkdlyinv3sd1_1 _29969_ (.A(_24551_),
    .Y(_24552_));
 sky130_fd_sc_hs__buf_1 _29970_ (.A(_24552_),
    .X(_24553_));
 sky130_fd_sc_hs__buf_4 _29971_ (.A(\aes.u_aes_core.u_aes_control.u_state_regs.u_impl_xilinx.q_reg[3].Q ),
    .X(_24554_));
 sky130_fd_sc_hs__inv_8 _29972_ (.A(_24554_),
    .Y(_24555_));
 sky130_fd_sc_hs__buf_1 _29973_ (.A(_24555_),
    .X(_24556_));
 sky130_fd_sc_hs__and4_4 _29974_ (.A(_24549_),
    .B(_24550_),
    .C(_24553_),
    .D(_24556_),
    .X(_24557_));
 sky130_fd_sc_hs__and3_4 _29975_ (.A(_24545_),
    .B(_24548_),
    .C(_24557_),
    .X(_24558_));

// Leave no dangling signals
 sky130_fd_sc_hs__buf_1 _123_ (.A(_24558_),
    .X(aes_output[2])); 
 sky130_fd_sc_hs__buf_1 _124_ (.A(_24666_),
    .X(aes_output[3])); 
 sky130_fd_sc_hs__buf_1 _125_ (.A(aes_input[2]),
    .X(_24623_)); 

 sky130_fd_sc_hs__clkdlyinv3sd2_1 _30084_ (.A(_24623_),
    .Y(_24667_));
 sky130_fd_sc_hs__or3_4 _30085_ (.A(_24638_),
    .B(_24667_),
    .C(_24634_),
    .X(_24668_));
 sky130_fd_sc_hs__a2bb2o_4 _30086_ (.A1_N(_24561_),
    .A2_N(_24629_),
    .B1(_24627_),
    .B2(_24559_),
    .X(_24669_));
 sky130_fd_sc_hs__nor2_4 _30087_ (.A(_24596_),
    .B(_24631_),
    .Y(_24670_));
 sky130_fd_sc_hs__nor2_4 _30088_ (.A(_24559_),
    .B(_24627_),
    .Y(_24671_));
 sky130_fd_sc_hs__and2_4 _30089_ (.A(_24596_),
    .B(_24631_),
    .X(_24672_));
 sky130_fd_sc_hs__or4_4 _30090_ (.A(\aes.u_aes_core.ctrl_err_storage_q_reg.Q ),
    .B(_24670_),
    .C(_24671_),
    .D(_24672_),
    .X(_24673_));
 sky130_fd_sc_hs__a2111o_4 _30091_ (.A1(_24561_),
    .A2(_24629_),
    .B1(_24669_),
    .C1(_24641_),
    .D1(_24673_),
    .X(_24674_));
 sky130_fd_sc_hs__a2bb2o_4 _30092_ (.A1_N(_24624_),
    .A2_N(_24625_),
    .B1(_24642_),
    .B2(_24566_),
    .X(_24675_));
 sky130_fd_sc_hs__a2111o_4 _30093_ (.A1(_24624_),
    .A2(_24625_),
    .B1(_24675_),
    .C1(_24635_),
    .D1(_24643_),
    .X(_24676_));
 sky130_fd_sc_hs__or3_4 _30094_ (.A(_24668_),
    .B(_24674_),
    .C(_24676_),
    .X(\aes.u_aes_core.ctrl_err_storage_q_reg.D ));
endmodule
module sky130_fd_sc_hs__a2111o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1) | (D1);
endmodule
module sky130_fd_sc_hs__a2111o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1) | (D1);
endmodule
module sky130_fd_sc_hs__a2111o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1) | (D1);
endmodule
module sky130_fd_sc_hs__a2111oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1&~D1) | (~A2&~B1&~C1&~D1);
endmodule
module sky130_fd_sc_hs__a2111oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1&~D1) | (~A2&~B1&~C1&~D1);
endmodule
module sky130_fd_sc_hs__a2111oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1&~D1) | (~A2&~B1&~C1&~D1);
endmodule
module sky130_fd_sc_hs__a211o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a211o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a211o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a211oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a211oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a211oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a21bo_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&A2) | (~B1_N);
endmodule
module sky130_fd_sc_hs__a21bo_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&A2) | (~B1_N);
endmodule
module sky130_fd_sc_hs__a21bo_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&A2) | (~B1_N);
endmodule
module sky130_fd_sc_hs__a21boi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&B1_N) | (~A2&B1_N);
endmodule
module sky130_fd_sc_hs__a21boi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&B1_N) | (~A2&B1_N);
endmodule
module sky130_fd_sc_hs__a21boi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&B1_N) | (~A2&B1_N);
endmodule
module sky130_fd_sc_hs__a21o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1);
endmodule
module sky130_fd_sc_hs__a21o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1);
endmodule
module sky130_fd_sc_hs__a21o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2) | (B1);
endmodule
module sky130_fd_sc_hs__a21oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1);
endmodule
module sky130_fd_sc_hs__a21oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1);
endmodule
module sky130_fd_sc_hs__a21oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1);
endmodule
module sky130_fd_sc_hs__a221o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2) | (C1);
endmodule
module sky130_fd_sc_hs__a221o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2) | (C1);
endmodule
module sky130_fd_sc_hs__a221o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2) | (C1);
endmodule
module sky130_fd_sc_hs__a221oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A2&~B2&~C1);
endmodule
module sky130_fd_sc_hs__a221oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A2&~B2&~C1);
endmodule
module sky130_fd_sc_hs__a221oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A2&~B2&~C1);
endmodule
module sky130_fd_sc_hs__a222o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  input [255:0] C2,
  output [255:0] X
);
  assign X = (C1&C2) | (B1&B2) | (A1&A2);
endmodule
module sky130_fd_sc_hs__a222o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  input [255:0] C2,
  output [255:0] X
);
  assign X = (C1&C2) | (B1&B2) | (A1&A2);
endmodule
module sky130_fd_sc_hs__a222oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  input [255:0] C2,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B1&~C2) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A1&~B2&~C2) | (~A2&~B1&~C2) | (~A2&~B2&~C1) | (~A2&~B2&~C2);
endmodule
module sky130_fd_sc_hs__a222oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  input [255:0] C2,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A1&~B1&~C2) | (~A1&~B2&~C1) | (~A2&~B1&~C1) | (~A1&~B2&~C2) | (~A2&~B1&~C2) | (~A2&~B2&~C1) | (~A2&~B2&~C2);
endmodule
module sky130_fd_sc_hs__a22o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2);
endmodule
module sky130_fd_sc_hs__a22o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2);
endmodule
module sky130_fd_sc_hs__a22o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (A1&A2);
endmodule
module sky130_fd_sc_hs__a22oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A2&~B2);
endmodule
module sky130_fd_sc_hs__a22oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A2&~B2);
endmodule
module sky130_fd_sc_hs__a22oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A2&~B2);
endmodule
module sky130_fd_sc_hs__a2bb2o_1 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (~A1_N&~A2_N);
endmodule
module sky130_fd_sc_hs__a2bb2o_2 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (~A1_N&~A2_N);
endmodule
module sky130_fd_sc_hs__a2bb2o_4 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (B1&B2) | (~A1_N&~A2_N);
endmodule
module sky130_fd_sc_hs__a2bb2oi_1 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (A1_N&~B1) | (A1_N&~B2) | (A2_N&~B1) | (A2_N&~B2);
endmodule
module sky130_fd_sc_hs__a2bb2oi_2 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (A1_N&~B1) | (A1_N&~B2) | (A2_N&~B1) | (A2_N&~B2);
endmodule
module sky130_fd_sc_hs__a2bb2oi_4 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (A1_N&~B1) | (A1_N&~B2) | (A2_N&~B1) | (A2_N&~B2);
endmodule
module sky130_fd_sc_hs__a311o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a311o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a311o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1) | (C1);
endmodule
module sky130_fd_sc_hs__a311oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1) | (~A3&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a311oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1) | (~A3&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a311oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~B1&~C1) | (~A2&~B1&~C1) | (~A3&~B1&~C1);
endmodule
module sky130_fd_sc_hs__a31o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1);
endmodule
module sky130_fd_sc_hs__a31o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1);
endmodule
module sky130_fd_sc_hs__a31o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1);
endmodule
module sky130_fd_sc_hs__a31oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1);
endmodule
module sky130_fd_sc_hs__a31oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1);
endmodule
module sky130_fd_sc_hs__a31oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1);
endmodule
module sky130_fd_sc_hs__a32o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1&B2);
endmodule
module sky130_fd_sc_hs__a32o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1&B2);
endmodule
module sky130_fd_sc_hs__a32o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&A2&A3) | (B1&B2);
endmodule
module sky130_fd_sc_hs__a32oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A3&~B1) | (~A2&~B2) | (~A3&~B2);
endmodule
module sky130_fd_sc_hs__a32oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A3&~B1) | (~A2&~B2) | (~A3&~B2);
endmodule
module sky130_fd_sc_hs__a32oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A1&~B2) | (~A2&~B1) | (~A3&~B1) | (~A2&~B2) | (~A3&~B2);
endmodule
module sky130_fd_sc_hs__a41o_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3&A4) | (B1);
endmodule
module sky130_fd_sc_hs__a41o_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3&A4) | (B1);
endmodule
module sky130_fd_sc_hs__a41o_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&A2&A3&A4) | (B1);
endmodule
module sky130_fd_sc_hs__a41oi_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1) | (~A4&~B1);
endmodule
module sky130_fd_sc_hs__a41oi_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1) | (~A4&~B1);
endmodule
module sky130_fd_sc_hs__a41oi_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~B1) | (~A2&~B1) | (~A3&~B1) | (~A4&~B1);
endmodule
module sky130_fd_sc_hs__and2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&B);
endmodule
module sky130_fd_sc_hs__and2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&B);
endmodule
module sky130_fd_sc_hs__and2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&B);
endmodule
module sky130_fd_sc_hs__and2b_1 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] X
);
  assign X = (~A_N&B);
endmodule
module sky130_fd_sc_hs__and2b_2 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] X
);
  assign X = (~A_N&B);
endmodule
module sky130_fd_sc_hs__and2b_4 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] X
);
  assign X = (~A_N&B);
endmodule
module sky130_fd_sc_hs__and3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B&C);
endmodule
module sky130_fd_sc_hs__and3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B&C);
endmodule
module sky130_fd_sc_hs__and3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B&C);
endmodule
module sky130_fd_sc_hs__and3b_1 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A_N&B&C);
endmodule
module sky130_fd_sc_hs__and3b_2 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A_N&B&C);
endmodule
module sky130_fd_sc_hs__and3b_4 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A_N&B&C);
endmodule
module sky130_fd_sc_hs__and4_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A&B&C&D);
endmodule
module sky130_fd_sc_hs__and4_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A&B&C&D);
endmodule
module sky130_fd_sc_hs__and4_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A&B&C&D);
endmodule
module sky130_fd_sc_hs__and4b_1 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&B&C&D);
endmodule
module sky130_fd_sc_hs__and4b_2 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&B&C&D);
endmodule
module sky130_fd_sc_hs__and4b_4 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&B&C&D);
endmodule
module sky130_fd_sc_hs__and4bb_1 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&~B_N&C&D);
endmodule
module sky130_fd_sc_hs__and4bb_2 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&~B_N&C&D);
endmodule
module sky130_fd_sc_hs__and4bb_4 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (~A_N&~B_N&C&D);
endmodule
module sky130_fd_sc_hs__buf_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__buf_16 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__buf_2 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__buf_4 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__buf_8 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__bufbuf_16 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__bufbuf_8 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__bufinv_16 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__bufinv_8 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkbuf_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__clkbuf_16 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__clkbuf_2 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__clkbuf_4 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__clkbuf_8 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__clkdlyinv3sd1_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkdlyinv3sd2_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkdlyinv3sd3_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkdlyinv5sd1_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkdlyinv5sd2_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkdlyinv5sd3_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkinv_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkinv_16 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkinv_2 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkinv_4 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__clkinv_8 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__conb_1 (
  output [255:0] HI,
  output [255:0] LO
);
  assign HI = 256'b1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;
  assign LO = 256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
endmodule
module sky130_fd_sc_hs__dfrtn_1 (
  input [0:0] CLK_N,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] RESET_B
);
  always @(negedge CLK_N) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfrtp_1 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] RESET_B
);
  always @(posedge CLK) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfrtp_2 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] RESET_B
);
  always @(posedge CLK) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfrtp_4 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] RESET_B
);
  always @(posedge CLK) begin
    Q <= D & ~(~RESET_B);
  end
endmodule
module sky130_fd_sc_hs__dfstp_1 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] SET_B
);
  always @(posedge CLK) begin
    Q <= D | (~SET_B);
  end
endmodule
module sky130_fd_sc_hs__dfstp_2 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] SET_B
);
  always @(posedge CLK) begin
    Q <= D | (~SET_B);
  end
endmodule
module sky130_fd_sc_hs__dfstp_4 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q,
  input [255:0] SET_B
);
  always @(posedge CLK) begin
    Q <= D | (~SET_B);
  end
endmodule
module sky130_fd_sc_hs__dfxtp_1 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q
);
  always @(posedge CLK) begin
    Q <= D;
  end
endmodule
module sky130_fd_sc_hs__dfxtp_2 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q
);
  always @(posedge CLK) begin
    Q <= D;
  end
endmodule
module sky130_fd_sc_hs__dfxtp_4 (
  input [0:0] CLK,
  input [255:0] D,
  output reg [255:0] Q
);
  always @(posedge CLK) begin
    Q <= D;
  end
endmodule
module sky130_fd_sc_hs__dlygate4sd1_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__dlygate4sd2_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__dlygate4sd3_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__dlymetal6s2s_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__dlymetal6s4s_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__dlymetal6s6s_1 (
  input [255:0] A,
  output [255:0] X
);
  assign X = (A);
endmodule
module sky130_fd_sc_hs__inv_1 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__inv_16 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__inv_2 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__inv_4 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__inv_8 (
  input [255:0] A,
  output [255:0] Y
);
  assign Y = (~A);
endmodule
module sky130_fd_sc_hs__maj3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B) | (A&C) | (B&C);
endmodule
module sky130_fd_sc_hs__maj3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B) | (A&C) | (B&C);
endmodule
module sky130_fd_sc_hs__maj3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&B) | (A&C) | (B&C);
endmodule
module sky130_fd_sc_hs__mux2_1 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] X
);
  assign X = (A0&~S) | (A1&S);
endmodule
module sky130_fd_sc_hs__mux2_2 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] X
);
  assign X = (A0&~S) | (A1&S);
endmodule
module sky130_fd_sc_hs__mux2_4 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] X
);
  assign X = (A0&~S) | (A1&S);
endmodule
module sky130_fd_sc_hs__mux2i_1 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] Y
);
  assign Y = (~A0&~S) | (~A1&S);
endmodule
module sky130_fd_sc_hs__mux2i_2 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] Y
);
  assign Y = (~A0&~S) | (~A1&S);
endmodule
module sky130_fd_sc_hs__mux2i_4 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] S,
  output [255:0] Y
);
  assign Y = (~A0&~S) | (~A1&S);
endmodule
module sky130_fd_sc_hs__mux4_1 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] S0,
  input [255:0] S1,
  output [255:0] X
);
  assign X = (A0&~S0&~S1) | (A1&S0&~S1) | (A2&~S0&S1) | (A3&S0&S1);
endmodule
module sky130_fd_sc_hs__mux4_2 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] S0,
  input [255:0] S1,
  output [255:0] X
);
  assign X = (A0&~S0&~S1) | (A1&S0&~S1) | (A2&~S0&S1) | (A3&S0&S1);
endmodule
module sky130_fd_sc_hs__mux4_4 (
  input [255:0] A0,
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] S0,
  input [255:0] S1,
  output [255:0] X
);
  assign X = (A0&~S0&~S1) | (A1&S0&~S1) | (A2&~S0&S1) | (A3&S0&S1);
endmodule
module sky130_fd_sc_hs__nand2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A) | (~B);
endmodule
module sky130_fd_sc_hs__nand2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A) | (~B);
endmodule
module sky130_fd_sc_hs__nand2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A) | (~B);
endmodule
module sky130_fd_sc_hs__nand2_8 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A) | (~B);
endmodule
module sky130_fd_sc_hs__nand2b_1 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (A_N) | (~B);
endmodule
module sky130_fd_sc_hs__nand2b_2 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (A_N) | (~B);
endmodule
module sky130_fd_sc_hs__nand2b_4 (
  input [255:0] A_N,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (A_N) | (~B);
endmodule
module sky130_fd_sc_hs__nand3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand3b_1 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand3b_2 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand3b_4 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C);
endmodule
module sky130_fd_sc_hs__nand4_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4b_1 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4b_2 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4b_4 (
  input [255:0] A_N,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (~B) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4bb_1 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (B_N) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4bb_2 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (B_N) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nand4bb_4 (
  input [255:0] A_N,
  input [255:0] B_N,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (A_N) | (B_N) | (~C) | (~D);
endmodule
module sky130_fd_sc_hs__nor2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B);
endmodule
module sky130_fd_sc_hs__nor2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B);
endmodule
module sky130_fd_sc_hs__nor2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B);
endmodule
module sky130_fd_sc_hs__nor2_8 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B);
endmodule
module sky130_fd_sc_hs__nor2b_1 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] Y
);
  assign Y = (~A&B_N);
endmodule
module sky130_fd_sc_hs__nor2b_2 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] Y
);
  assign Y = (~A&B_N);
endmodule
module sky130_fd_sc_hs__nor2b_4 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] Y
);
  assign Y = (~A&B_N);
endmodule
module sky130_fd_sc_hs__nor3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A&~B&~C);
endmodule
module sky130_fd_sc_hs__nor3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A&~B&~C);
endmodule
module sky130_fd_sc_hs__nor3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] Y
);
  assign Y = (~A&~B&~C);
endmodule
module sky130_fd_sc_hs__nor3b_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N);
endmodule
module sky130_fd_sc_hs__nor3b_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N);
endmodule
module sky130_fd_sc_hs__nor3b_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N);
endmodule
module sky130_fd_sc_hs__nor4_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&~D);
endmodule
module sky130_fd_sc_hs__nor4_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&~D);
endmodule
module sky130_fd_sc_hs__nor4_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&~D);
endmodule
module sky130_fd_sc_hs__nor4b_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&D_N);
endmodule
module sky130_fd_sc_hs__nor4b_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&D_N);
endmodule
module sky130_fd_sc_hs__nor4b_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&~C&D_N);
endmodule
module sky130_fd_sc_hs__nor4bb_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N&D_N);
endmodule
module sky130_fd_sc_hs__nor4bb_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N&D_N);
endmodule
module sky130_fd_sc_hs__nor4bb_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] Y
);
  assign Y = (~A&~B&C_N&D_N);
endmodule
module sky130_fd_sc_hs__o2111a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&B1&C1&D1) | (A2&B1&C1&D1);
endmodule
module sky130_fd_sc_hs__o2111a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&B1&C1&D1) | (A2&B1&C1&D1);
endmodule
module sky130_fd_sc_hs__o2111a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] X
);
  assign X = (A1&B1&C1&D1) | (A2&B1&C1&D1);
endmodule
module sky130_fd_sc_hs__o2111ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1) | (~D1);
endmodule
module sky130_fd_sc_hs__o2111ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1) | (~D1);
endmodule
module sky130_fd_sc_hs__o2111ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  input [255:0] D1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1) | (~D1);
endmodule
module sky130_fd_sc_hs__o211a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1);
endmodule
module sky130_fd_sc_hs__o211a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1);
endmodule
module sky130_fd_sc_hs__o211a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1);
endmodule
module sky130_fd_sc_hs__o211ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o211ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o211ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o21a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1);
endmodule
module sky130_fd_sc_hs__o21a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1);
endmodule
module sky130_fd_sc_hs__o21a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1);
endmodule
module sky130_fd_sc_hs__o21ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1);
endmodule
module sky130_fd_sc_hs__o21ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1);
endmodule
module sky130_fd_sc_hs__o21ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (~B1);
endmodule
module sky130_fd_sc_hs__o21ba_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&~B1_N) | (A2&~B1_N);
endmodule
module sky130_fd_sc_hs__o21ba_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&~B1_N) | (A2&~B1_N);
endmodule
module sky130_fd_sc_hs__o21ba_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] X
);
  assign X = (A1&~B1_N) | (A2&~B1_N);
endmodule
module sky130_fd_sc_hs__o21bai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (B1_N);
endmodule
module sky130_fd_sc_hs__o21bai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (B1_N);
endmodule
module sky130_fd_sc_hs__o21bai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1_N,
  output [255:0] Y
);
  assign Y = (~A1&~A2) | (B1_N);
endmodule
module sky130_fd_sc_hs__o221a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A1&B2&C1) | (A2&B2&C1);
endmodule
module sky130_fd_sc_hs__o221a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A1&B2&C1) | (A2&B2&C1);
endmodule
module sky130_fd_sc_hs__o221a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A1&B2&C1) | (A2&B2&C1);
endmodule
module sky130_fd_sc_hs__o221ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2) | (~C1);
endmodule
module sky130_fd_sc_hs__o221ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2) | (~C1);
endmodule
module sky130_fd_sc_hs__o221ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2) | (~C1);
endmodule
module sky130_fd_sc_hs__o22a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A1&B2) | (A2&B2);
endmodule
module sky130_fd_sc_hs__o22a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A1&B2) | (A2&B2);
endmodule
module sky130_fd_sc_hs__o22a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A1&B2) | (A2&B2);
endmodule
module sky130_fd_sc_hs__o22ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2);
endmodule
module sky130_fd_sc_hs__o22ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2);
endmodule
module sky130_fd_sc_hs__o22ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (~A1&~A2);
endmodule
module sky130_fd_sc_hs__o2bb2a_1 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (~A1_N&B1) | (~A2_N&B1) | (~A1_N&B2) | (~A2_N&B2);
endmodule
module sky130_fd_sc_hs__o2bb2a_2 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (~A1_N&B1) | (~A2_N&B1) | (~A1_N&B2) | (~A2_N&B2);
endmodule
module sky130_fd_sc_hs__o2bb2a_4 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (~A1_N&B1) | (~A2_N&B1) | (~A1_N&B2) | (~A2_N&B2);
endmodule
module sky130_fd_sc_hs__o2bb2ai_1 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (A1_N&A2_N);
endmodule
module sky130_fd_sc_hs__o2bb2ai_2 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (A1_N&A2_N);
endmodule
module sky130_fd_sc_hs__o2bb2ai_4 (
  input [255:0] A1_N,
  input [255:0] A2_N,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~B1&~B2) | (A1_N&A2_N);
endmodule
module sky130_fd_sc_hs__o311a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A3&B1&C1);
endmodule
module sky130_fd_sc_hs__o311a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A3&B1&C1);
endmodule
module sky130_fd_sc_hs__o311a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] X
);
  assign X = (A1&B1&C1) | (A2&B1&C1) | (A3&B1&C1);
endmodule
module sky130_fd_sc_hs__o311ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o311ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o311ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] C1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1) | (~C1);
endmodule
module sky130_fd_sc_hs__o31a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1);
endmodule
module sky130_fd_sc_hs__o31a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1);
endmodule
module sky130_fd_sc_hs__o31a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1);
endmodule
module sky130_fd_sc_hs__o31ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1);
endmodule
module sky130_fd_sc_hs__o31ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1);
endmodule
module sky130_fd_sc_hs__o31ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1);
endmodule
module sky130_fd_sc_hs__o32a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A1&B2) | (A2&B1) | (A3&B1) | (A2&B2) | (A3&B2);
endmodule
module sky130_fd_sc_hs__o32a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A1&B2) | (A2&B1) | (A3&B1) | (A2&B2) | (A3&B2);
endmodule
module sky130_fd_sc_hs__o32a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] X
);
  assign X = (A1&B1) | (A1&B2) | (A2&B1) | (A3&B1) | (A2&B2) | (A3&B2);
endmodule
module sky130_fd_sc_hs__o32ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1&~B2);
endmodule
module sky130_fd_sc_hs__o32ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1&~B2);
endmodule
module sky130_fd_sc_hs__o32ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] B1,
  input [255:0] B2,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3) | (~B1&~B2);
endmodule
module sky130_fd_sc_hs__o41a_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1) | (A4&B1);
endmodule
module sky130_fd_sc_hs__o41a_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1) | (A4&B1);
endmodule
module sky130_fd_sc_hs__o41a_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] X
);
  assign X = (A1&B1) | (A2&B1) | (A3&B1) | (A4&B1);
endmodule
module sky130_fd_sc_hs__o41ai_1 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3&~A4) | (~B1);
endmodule
module sky130_fd_sc_hs__o41ai_2 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3&~A4) | (~B1);
endmodule
module sky130_fd_sc_hs__o41ai_4 (
  input [255:0] A1,
  input [255:0] A2,
  input [255:0] A3,
  input [255:0] A4,
  input [255:0] B1,
  output [255:0] Y
);
  assign Y = (~A1&~A2&~A3&~A4) | (~B1);
endmodule
module sky130_fd_sc_hs__or2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A) | (B);
endmodule
module sky130_fd_sc_hs__or2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A) | (B);
endmodule
module sky130_fd_sc_hs__or2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A) | (B);
endmodule
module sky130_fd_sc_hs__or2b_1 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] X
);
  assign X = (A) | (~B_N);
endmodule
module sky130_fd_sc_hs__or2b_2 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] X
);
  assign X = (A) | (~B_N);
endmodule
module sky130_fd_sc_hs__or2b_4 (
  input [255:0] A,
  input [255:0] B_N,
  output [255:0] X
);
  assign X = (A) | (~B_N);
endmodule
module sky130_fd_sc_hs__or3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A) | (B) | (C);
endmodule
module sky130_fd_sc_hs__or3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A) | (B) | (C);
endmodule
module sky130_fd_sc_hs__or3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A) | (B) | (C);
endmodule
module sky130_fd_sc_hs__or3b_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N);
endmodule
module sky130_fd_sc_hs__or3b_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N);
endmodule
module sky130_fd_sc_hs__or3b_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N);
endmodule
module sky130_fd_sc_hs__or4_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (D);
endmodule
module sky130_fd_sc_hs__or4_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (D);
endmodule
module sky130_fd_sc_hs__or4_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (D);
endmodule
module sky130_fd_sc_hs__or4b_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (~D_N);
endmodule
module sky130_fd_sc_hs__or4b_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (~D_N);
endmodule
module sky130_fd_sc_hs__or4b_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (C) | (~D_N);
endmodule
module sky130_fd_sc_hs__or4bb_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N) | (~D_N);
endmodule
module sky130_fd_sc_hs__or4bb_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N) | (~D_N);
endmodule
module sky130_fd_sc_hs__or4bb_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C_N,
  input [255:0] D_N,
  output [255:0] X
);
  assign X = (A) | (B) | (~C_N) | (~D_N);
endmodule
module sky130_fd_sc_hs__xnor2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B) | (A&B);
endmodule
module sky130_fd_sc_hs__xnor2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B) | (A&B);
endmodule
module sky130_fd_sc_hs__xnor2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] Y
);
  assign Y = (~A&~B) | (A&B);
endmodule
module sky130_fd_sc_hs__xnor3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A&~B&~C) | (A&B&~C) | (A&~B&C) | (~A&B&C);
endmodule
module sky130_fd_sc_hs__xnor3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A&~B&~C) | (A&B&~C) | (A&~B&C) | (~A&B&C);
endmodule
module sky130_fd_sc_hs__xnor3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (~A&~B&~C) | (A&B&~C) | (A&~B&C) | (~A&B&C);
endmodule
module sky130_fd_sc_hs__xor2_1 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&~B) | (~A&B);
endmodule
module sky130_fd_sc_hs__xor2_2 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&~B) | (~A&B);
endmodule
module sky130_fd_sc_hs__xor2_4 (
  input [255:0] A,
  input [255:0] B,
  output [255:0] X
);
  assign X = (A&~B) | (~A&B);
endmodule
module sky130_fd_sc_hs__xor3_1 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&~B&~C) | (~A&B&~C) | (~A&~B&C) | (A&B&C);
endmodule
module sky130_fd_sc_hs__xor3_2 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&~B&~C) | (~A&B&~C) | (~A&~B&C) | (A&B&C);
endmodule
module sky130_fd_sc_hs__xor3_4 (
  input [255:0] A,
  input [255:0] B,
  input [255:0] C,
  output [255:0] X
);
  assign X = (A&~B&~C) | (~A&B&~C) | (~A&~B&C) | (A&B&C);
endmodule
