#!/usr/bin/python3

import unittest
import tempfile
import vado
from collections import namedtuple
import os

class TestVado(unittest.TestCase):

    def test_large(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        cmdline = ["vado_otaes.v", "vado_modules.v", outf.name]

        vado.vado(*vado.get_opts(cmdline))
        os.system("cp {} /tmp/bla.v".format(outf.name))

        cmp_v = "vado_otaes_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

    def test_something(self):
        outf = tempfile.NamedTemporaryFile(suffix=".v")
        cmdline = ["vado_test_in.v", "vado_modules.v", outf.name]

        vado.vado(*vado.get_opts(cmdline))
        os.system("cp {} /tmp/bla.v".format(outf.name))

        cmp_v = "vado_test_out.v"
        diff = os.system("diff -u " + cmp_v + " " + outf.name)
        self.assertEqual(0, diff, "Difference with expected output")

if __name__ == '__main__':
    unittest.main()
