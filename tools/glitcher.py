#!/usr/bin/python3
# John Fitzgerald

import os, sys, struct, math, time, random, socket, subprocess, defparser, concurrent.futures, itertools, IPython, yappi
import shlex
import threading
import pickle
import numpy as np
import traceback
import datetime
import queue
import tempfile
import zlib
from optparse import OptionParser
from collections import Counter, namedtuple
from instrument import GlitchItem

COMMAND_VERSION = 0xABCD0000
COMMAND_GLITCH = 0xABCD0001
COMMAND_EXIT = 0xABCD0002

MSG_TEST_STATUS = 0x01
MSG_CRASHED = 0x02
MSG_PID = 0x03
MSG_NUM_MACHINES = 0x04
MSG_STATUS = 0x05
MSG_CMD = 0x06
MSG_GLITCHBUF = 0x07
MSG_ARGS = 0x08
MSG_EXTRA_DATA_SIZE = 0x09
MSG_EXTRA_DATA = 0x10

# configuration
SEED_SIZE = 64

QUEUE_MUL = 10 # Queue multiplier: how much memory to allocate in the result queue

def parseoptions(cmdline):
    USAGE_STRING = "Usage: glitcher.py [options] verilator_binary glitchmap outputdb\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=0, help="Verbose output, More vvvv is more verbose")
    optparser.add_option("-n", "--numchild", type="int", dest="numchild", default=1, help="Number of child glitcher processes")
    optparser.add_option("-l", "--location", type="int", dest="location", nargs=2, default=(0, 0), help="Location of fault injection probe in micrometers")
    optparser.add_option("-d", "--diameter", type="int", dest="diameter", default=None, help="Diameter of the fault injection probe in micrometers, default = Entire die area")
    optparser.add_option("-g", "--glitchwidth", type="int", dest="glitchwidth", default="1", help="Number of clock cycles the glitch will last, defaul 1")
    optparser.add_option("-c", "--clocks", type="int", dest="clocks", default="2500", help="Maximum number of clock cycles for full sim")
    optparser.add_option("-s", "--glitchstart", type="int", dest="glitchstart", default="0", help="Starting clock cycle for glitch window, default 0")
    optparser.add_option("-e", "--glitchend", type="int", dest="glitchend", default=None, help="Ending clock cycle for glitch window, default maximum number of clocks")
    optparser.add_option("-t", "--numtests", type="int", dest="numtests", default="10", help="Maximum number of glitch tests to run")
    optparser.add_option("-r", "--rngseed", type="int", dest="rngseed", default="0", help="Seed for all rngs. Default 0")
    optparser.add_option("-b", "--bruteforce", action="store_true", dest="bruteforce", default=False, help="Brute force all pins and clocks. When set, ignores --numtests and --rngseed")
    optparser.add_option("-p", "--profile", action="store_true", dest="profile", default=False, help="Enable profiling")
    optparser.add_option("-P", "--pin", type="int", action="append", dest="pins", default=None, help="Restrict glitching to these pins. Can be specified multiple times. Default all pins. If specified, overrides any diameter/location options.")
    optparser.add_option("-m", "--machines", type="int", dest="machines", default=256, help="Number of VMs per client, default 256")
    optparser.add_option("-a", "--glitchprob", type="float", dest="glitchprob", default=0.1, help="For each glitch run, the probability that a particular input into a cell is faulted")
    optparser.add_option("-D", "--deffile", type="string", dest="deffile", default=None, help="DEF file for localized faulting")

    global options
    global args
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 3:
        print("Need 3 args, got {}".format(args))
        print(USAGE_STRING)
        sys.exit(1)

    if options.glitchend is not None:
        assert options.glitchstart < options.glitchend, f"Glitch start {options.glitchstart} must be before glitch end {options.glitchend}"

#def random_gate():
#    alpha = 2 * math.pi * random.random()
#    r = proberadius * math.sqrt(random.random())
#    return (r * math.cos(alpha) + options.location[0], r * math.sin(alpha) + options.location[1])

GlitchParams = namedtuple("GlitchParams", ['start', 'width', 'clocks', 'code'])
class GlitchGenerator:
    def __init__(self, numpins, pinlist, glitchwidth, clocks, machines, version):
        self.numpins = numpins
        self.pinlist = pinlist
        self.machines = machines
        self.glitchwidth = glitchwidth
        self.clocks = clocks

    def generate(self):
        raise RuntimeError("Abstract method, please override")

    def bytes_per_vm(self):
        return (self.numpins+7) // 8 # Convert to bytes

    def pins2bitmap(self, pinlist):
        pins = np.zeros(self.numpins, dtype=bool)
        pins[pinlist] = True
        pins = np.packbits(pins, bitorder='little')
        return pins

    def decodepins(code):
        npcode = np.frombuffer(code, dtype=np.uint8)
        pinbits = np.unpackbits(npcode, bitorder='little') # XXX we shoudl technically cut off the last bits
        #res = np.where(pinbits == 1)[0].tolist()
        return pinbits == 1



class BruteforcePinGenerator(GlitchGenerator):
    def __init__(self, numpins, pinlist, start, glitchwidth, clocks, machines, version):
        super().__init__(numpins, pinlist, glitchwidth, clocks, machines, version)
        self.start = start
        self.pinidx = 0


    def generate(self):
        if self.pinidx >= len(self.pinlist):
            return None

        codes = b''
        for pin in range(self.pinidx, self.pinidx + self.machines):
            if pin < len(self.pinlist):
                pl = [pin]
            else:
                pl = []
            code = self.pins2bitmap(pl)
            codes = codes + code.tobytes()
        self.pinidx = pin + 1

        gp = GlitchParams(
                width = self.glitchwidth, 
                clocks = self.clocks,
                start = self.start,
                code = codes)

        return gp


class RandomGlitchGenerator(GlitchGenerator):
    def __init__(self, number, seed, numpins, pinlist, glitchstart, glitchend, glitchwidth, clocks, machines, version, probability):
        super().__init__(numpins, pinlist, glitchwidth, clocks, machines, version)
        self.glitchstart = glitchstart
        self.glitchend = glitchend
        self.number = number
        self.rng = np.random.default_rng(seed)
        self.probability = probability

        # Create mask of which pins to retain
        self._mask = self.pins2bitmap(self.pinlist)
        self._mask = np.tile(self._mask, machines)
        assert version == 0, "Unsupported version"

    def generate(self):
        # Update count
        if self.number == 0:
            return None
        self.number -= 1

        # Do rng
        gp = GlitchParams(
                width = self.glitchwidth, 
                clocks = self.clocks,
                start = self.rng.integers(self.glitchstart, self.glitchend),
                code = self._generate_glitches())

        return gp

    def _generate_glitches(self):
        baseidx = 0
        cutoff = int(256 * self.probability)
        assert not (self.probability > 0 and cutoff == 0), "Oops, you've reached the lower end of supported probability. It should be larger than 1/256"

        g = self.rng.integers(0, 256, self.bytes_per_vm() * 8 * self.machines, np.uint8) # Set active pins to random uint8
        g = g < cutoff # Turn into booleans and pack

        # If we have few pins, there is a risk none of them will be selected. Hack this by always setting one
        if self.probability > 0 and self.numpins < 10/self.probability:
            setidx = self.rng.choice(self.pinlist)
            g[setidx] = True

        g = np.packbits(g, bitorder='little') # Index 0 in array should be bit 0 (LSB) of byte 0 -> big endian
        g = g & self._mask # Mask out non-selected pins

        return g.tobytes()


def valid_gate(x, y):
    if x is None or y is None:
        return False
    return True

def fault_gate(x, y):
    proberadius = (options.diameter / 2)
    return (x - options.location[0])**2 + (y - options.location[1])**2 < proberadius**2

def lookup_inputs(name, gm):
    return [item.index for item in gm if item.modulename == name]

def prepare_glitch_pins(df, gm):
    pinlist = []
    invalid = 0
    no_fault = 0
    fault = 0
    numgates = len(df.components.list)
    for gate in df.components.list:
        if valid_gate(gate.origin[0], gate.origin[1]):
            if fault_gate(gate.origin[0], gate.origin[1]):
                fault += 1
                pins = lookup_inputs(gate.name, gm)
                for pin in pins:
                    pinlist.append(pin)
            else:
                no_fault += 1
        else:
            invalid += 1

    if options.verbose > 1:
        print("[*] There are {} gates, of which {} we will fault, {} are not in fault range and {} are invalid".format(numgates, fault, no_fault, invalid))

    return pinlist

def send_or_write(sock, data):
    if type(sock) == socket.socket:
        return sock.send(data)
    else:
        return sock.write(data)

def send(sock, idbyte, data):
    if options.verbose > 2:
        print("[*] sending {} bytes to child with id {}".format(len(data), hex(idbyte)))
    sent = send_or_write(sock, struct.pack("B", idbyte))
    assert sent == 1
    sent = send_or_write(sock, struct.pack("<I", len(data)))
    assert sent == 4
    sent = send_or_write(sock, data) 
    assert sent == len(data)

def send_glitch(sock, gp):
    # send glitch command
    send(sock, MSG_CMD, struct.pack("<I", COMMAND_GLITCH))

    # send glitch arguments
    send(sock, MSG_ARGS, struct.pack("<III", gp.clocks, gp.start, gp.width))

    # send glitch signals
    send(sock, MSG_GLITCHBUF, gp.code)

    if options.verbose > 2:
        print("[*] clocks: {} start: {} width: {}".format(gp.clocks, gp.start, gp.width))

def send_done(sock):
    send(sock, MSG_CMD, struct.pack("<I", COMMAND_EXIT))

def recv(sock, numbytes):
    numleft = numbytes
    resbuf = b''
    while numleft > 0:
        buf = sock.recv(numleft)
        if len(buf) == 0:
            return None
        resbuf += buf
        numleft -= len(buf)
    return resbuf

def recv_bytes(sock, expected_idbyte, numbytes):
    if options.verbose > 2:
        print("[*] Waiting for {} bytes, expected id {}".format(numbytes, hex(expected_idbyte)))

    # Do checks
    idbyte = recv(sock, 1);
    assert idbyte != None
    idbyte = struct.unpack("B", idbyte)[0]
    assert idbyte == expected_idbyte, "Received ID: {}, expected: {}".format(hex(idbyte), hex(expected_idbyte))
    snd_numbytes = recv(sock, 4)
    assert snd_numbytes != None
    snd_numbytes = struct.unpack("<I", snd_numbytes)[0]
    assert snd_numbytes == numbytes, "Expected {} bytes, received {} bytes".format(numbytes, snd_numbytes)

    result = recv(sock, numbytes)

    if options.verbose > 2:
        print("[*] Done receiving {} bytes".format(numbytes))
    return result

def recv_array_uint8(sock, idbyte, arrlen):
    resbuf = recv_bytes(sock, idbyte, arrlen)
    if resbuf is None:
        return None
    return struct.unpack("{}B".format(arrlen), resbuf) # Array of uint8

def recv_array_blob(sock, idbyte, arrlen, elementlen):
    resbuf = recv_bytes(sock, idbyte, arrlen)
    if resbuf is None:
        return None
    return unpack_array_blob(resbuf, elementlen, arrlen)

def unpack_array_blob(resbuf, elementlen, arrlen):
    if elementlen == 0:
        return [b''] * arrlen
    assert len(resbuf) % elementlen == 0, "Received {} bytes, which is not multiple of {}".format(len(resbuf), elementlen)
    return [resbuf[i:i+elementlen] for i in range(0, len(resbuf), elementlen)]

def recv_uint32(sock, idbyte):
    resbuf = recv_bytes(sock, idbyte, 4)
    if resbuf is None:
        return None
    return struct.unpack("<I", resbuf)[0] # 32bit unsigned int

def recv_results(conn, extra_data_size):
    test_status = recv_array_uint8(conn, MSG_TEST_STATUS, options.machines)
    extra_data = recv_array_blob(conn, MSG_EXTRA_DATA, options.machines * extra_data_size, extra_data_size)
    status = recv_uint32(conn, MSG_STATUS)

    # deal with error status above
    if status is None or test_status is None:
        return None

    # gather results
    if options.verbose > 1:
        print("[*] Status: {}, Results: {}".format(status, Counter(test_status)))
    return (test_status, status, extra_data)
 

process_lock = threading.Lock()
def thread_function(sock, process_params, params, result_queue):
    # Open process and do initial communication; lock so we don't mix up subprocesses and threads
    with process_lock:
        if options.verbose > 1:
            print(f"[*] Starting {process_params}")
        process = subprocess.Popen(process_params)
        conn, clientaddr = sock.accept()

        pid = recv_uint32(conn, MSG_PID)
        if pid is None:
            raise RuntimeError("Cannot receive pid")

    # Receive/check no of machines
    machines = recv_uint32(conn, MSG_NUM_MACHINES)
    if machines is None:
        raise RuntimeError("Cannot received number of machines")
    if options.machines != machines:
        raise RuntimeError("Local machines ({}) is different from global machines ({})".format(machines, options.machines))

    # Receive extra data size
    extra_data_size = recv_uint32(conn, MSG_EXTRA_DATA_SIZE)

    if options.verbose > 0:
        print("[*] added a child client pid: {}".format(pid))
        print("[*] number of machines: {}".format(machines))

    while (True):
        # Initiate test
        glitch_params = params.generate()

        if glitch_params is None: # Are we done?
            break

        send_glitch(conn, glitch_params)

        # Receive results and check we're ok
        result = recv_results(conn, extra_data_size)
        if not result or process.poll() is not None:
            print("[*] The child with pid {} died with code {}".format(pid, process.poll()))
            return False
        (test_status, status, extra_data) = result
        if status != 0:
            print("[*] A grandchild returned a nonzero status for test {}, the child with pid {}, status {}".format(i, pid, status))
            return False

        # Store results
        per_vm = params.bytes_per_vm()
        for vm in range(machines):
            vmgp = GlitchParams(
                    start = glitch_params.start, 
                    width = glitch_params.width,
                    clocks = glitch_params.clocks,
                    code = glitch_params.code[per_vm * vm : per_vm * (vm+1)])
            result_queue.put((vmgp, vm, test_status[vm], extra_data[vm])) # Push into result queue

    # Done
    if options.verbose > 0:
        print("[*] done with child {}".format(pid))
    send_done(conn)

    try:
        process.wait(5)
    except subprocess.TimeoutExpired:
        process.kill()
    return True

def start_children(binaryfile, server_address, sock, numpins, pinlist):
    rng = random.Random(options.rngseed) # Our overall rng, seeded from cmd line

    params = []
    # Create parameter generator
    if options.bruteforce:
        params = [BruteforcePinGenerator(numpins, pinlist, start, options.glitchwidth, options.clocks, options.machines, 0) for start in range(options.glitchstart, options.glitchend+1)]
    else:
        tests_assigned = 0 # Keep track of how many tests already farmed out
        for child in range(options.numchild):
            num = (child+1) * options.numtests // options.numchild - tests_assigned # Calculate number of tests for this child. Rounding errors are taken care off by using tests_assigned
            tests_assigned += num
            thread_seed = rng.getrandbits(SEED_SIZE) # Create seed based on overall rng per thread
            params.append(RandomGlitchGenerator(num, thread_seed, numpins, pinlist, options.glitchstart, options.glitchend, options.glitchwidth, options.clocks, options.machines, 0, options.glitchprob))


    # Push all parameters to work items, limited by number of child in concurrency
    result_queue = queue.Queue(options.numchild * options.machines * QUEUE_MUL) # Size it some factor of number of results
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=options.numchild)
    futures = []
    for i, param in enumerate(params):
        process_params = shlex.split(binaryfile)
        process_params.extend([ "-c", server_address ])
        if options.verbose > 0:
            process_params.append("-" + ("v"*options.verbose))
        if options.verbose > 2:
            print(f"[*] Starting thread {i}")
        future = executor.submit(thread_function, sock, process_params, param, result_queue)
        futures.append(future)

    return futures, result_queue

OutputFormat = namedtuple('OutputFormat', ['version', 'date', 'machines', 'numpins', 'glitchstart', 'glitchend', 'glitchwidth', 'clocks', 'pinlist', 'results'])
def dump_output(outfile, result_queue, futures, machines, numpins, pinlist, glitchstart, glitchend, glitchwidth, clocks):
    #version = 1
    #version = 2 # Add zlib compression
    version = 3 # Add extra data
    date = datetime.datetime.now()
    zpinlist = zlib.compress(pickle.dumps(pinlist))

    # Dump output info
    outf = open(outfile, "ab")

    # Loop through results
    block_size = options.numchild * options.machines * QUEUE_MUL # Glass if more than half full -> dump out to disk
    counter = [0,0,0,0]
    done = False
    while not done: #sum([f.done() for f in futures]) < len(futures): # Loop until all threads are done
        out_results = []

        # Loop until we're done or we have enough to fill a block
        while not done and len(out_results) < block_size:
            glitch_params = None
            while not done and glitch_params is None: # Loop until we have results or all threads are gone
                try:
                    (glitch_params, vm, test_status, extra_data) = result_queue.get(block=True, timeout=1)  # Wait for a second if we see no results
                except queue.Empty:
                    # We timed out. No results available. Check if all threads are done. In that case we know there will be no more results coming in later
                    donesum = sum([f.done() for f in futures])
                    if options.verbose > 2:
                        print("[*] Timed out waiting for results. {} done out of total of {} threads".format(donesum, len(futures)))
                    for f in futures:
                        if f.done():
                            e = f.exception()
                            if e:
                                print("[*] Exception in thread: " + ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__)))

                    done = (donesum == len(futures)) # All threads done?

            # Process params if we have them
            if glitch_params is not None:
                if options.verbose > 1:
                    print("[*] vm: {}, test_status: {} extra_data: {}".format(vm, test_status, extra_data.hex()))

                success = test_status >> 6 # 0 = no fault, 1 = fault exploitable, 2 = fault unexploitable, 3 = unknown fault
                extra = test_status & 0x3f # Lower bits, extra info
            
                counter[success] += 1

                # Only output exploitable faulty results
                if success == 1:
                    out_results.append((glitch_params, vm, extra, extra_data))
            
        # Write any data we have
        if len(out_results) > 0:
            if options.verbose > 0:
                print("[*] Writing block of {} results".format(len(out_results)))
            # We compress per block. This way we can keep appending to a result file. Appending to a fully compressed file doesn't work
            zresults = zlib.compress(pickle.dumps(out_results))
            outdata = OutputFormat(version=version, date=date, machines=machines, numpins=numpins, glitchstart=glitchstart, glitchend=glitchend, glitchwidth=glitchwidth, clocks=clocks, pinlist=zpinlist, results=zresults)
            pickle.dump(outdata, outf)

    print("[*] Jobs total: {}, No fault: {}, Exploitable: {}, Unexploitable: {}, Unknown fault: {}".format(sum(counter), counter[0], counter[1], counter[2], counter[3]))

    outf.close()

def determine_glitch_pins(deffile, gm):
    numpins = len(gm)

    # Only do location based if diameter specified, and no pin override specified
    if options.diameter is not None and options.pins is None:
        # Localized fault based on def file
        df = defparser.parse(deffile)

        if options.verbose > 0:
            print("[*] location: {} diameter: {}".format(options.location, options.diameter))
            print("[*] die area: {}".format(df.diearea))
        pinlist = prepare_glitch_pins(df, gm)
    else:
        if options.pins is None:
            pinlist = list(range(numpins))
        else:
            pinlist = options.pins
    return numpins, pinlist

def main(cmdline):
    parseoptions(cmdline)
    print("[*] Command line options: {}".format(options))
    print("[*] Command line args: {}".format(args))

    # arguments
    binaryfile = args[0]
    # Load glitchmap
    with open(args[1], "rb") as gf:
        gm = pickle.load(gf)

    if options.glitchend is None:
        options.glitchend = options.clocks

    # this generates a list of valid gate inputs that are in the circular layout of where glitching is occuring
    if options.verbose > 0:
        print("[*] prepare pin list...")
    numpins, pinlist = determine_glitch_pins(options.deffile, gm)
    if options.verbose > 1:
        print("[*] Selected {} pins out of total {} to glitch".format(len(pinlist), numpins))

    # start the server
    if options.verbose > 0:
        print("[*] starting server...")

    # Get temporary socket name
    with tempfile.NamedTemporaryFile(suffix=".socket") as tf:
        server_address = tf.name
    if options.verbose > 1:
        print(f"[*] Socket at {server_address}")

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(server_address)
    sock.listen(options.numchild)
    sock.settimeout(None) # Blocking mode, wait forevah

    clients = []

    try:
        if options.profile:
            yappi.start()

        # Start and collect all threads
        if options.verbose > 0:
            print("[*] Starting simulations")
        totalstart = time.time()

        futures, result_queue = start_children(binaryfile, server_address, sock, numpins, pinlist)
        # Collect results of all threads as they are going
        dump_output(args[2], result_queue, futures, options.machines, numpins, pinlist, options.glitchstart, options.glitchend, options.glitchwidth, options.clocks)
        print("[*] finished. Time: {}".format(time.time() - totalstart))

        if options.profile:
            yappi.stop()
            stats = yappi.get_func_stats()
            stats.sort('tsub') # Sort by time spent in individual function, excluding subcalls
            stats.print_all()

    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])
