#!/usr/bin/python3


import matplotlib.pyplot as plt
from glitcher import OutputFormat, GlitchParams, GlitchGenerator
from matplotlib import cm
from optparse import OptionParser
import yappi
from collections import namedtuple, Counter, OrderedDict
import pathlib
import os
import multiprocessing, concurrent
import re
import sys
import zlib
import IPython
import glitcher
import pickle
import numpy as np
from instrument import GlitchItem

def get_opts(cmdline):
    USAGE_STRING = "Usage: fi_grep.py result_file\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=0, help="Verbose output, More vvvv is more verbose")
    optparser.add_option("-p", "--profile", action="store_true", dest="profile", default=False, help="Enable profiling")
    optparser.add_option("-f", "--figure", action="store", dest="figure", default=None, type="string", help="Output figure to this file")
    optparser.add_option("-r", "--reproduce", action="store", dest="reproduce", default=None, type="string", help="Output reproducing example to this file")
    optparser.add_option("-a", "--reproduce-all", action="store", dest="reproduce_all", default=None, type="string", help="Reproduce all faults to this directory")
    optparser.add_option("-R", "--reproduce-run", action="store", dest="reproduce_run", default=None, type="string", help="Run the specified test bench and capture output")
    optparser.add_option("-g", "--glitchmap", action="store", dest="glitchmap", default=None, type="string", help="Glitchmap (for pin name resolution")
    optparser.add_option("-n", "--topn", action="store", dest="topn", default=10, type="int", help="Only give top N results, default=10")
    optparser.add_option("-w", "--write", action="store", dest="write", default=None, type="string", help="Write the topn pins to a file (full)")
    optparser.add_option("-o", "--write-outputname", action="store", dest="writeoutputname", default=None, type="string", help="Write the topn pins to a file (output name only)")
    optparser.add_option("-c", "--csv", action="store", dest="csv", default=None, type="string", help="Write the glitch data to csv format")

    global options
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 1:
        print(USAGE_STRING)
        sys.exit(1)

    result_file = args[0]

    return (result_file, options)


def get_data(experiment, data, options):
    assert experiment.version >= 2 and experiment.version <= 3, "Unsupported input version {}".format(experiment.version)
    results = pickle.loads(zlib.decompress(experiment.results)) # Unzip results
    pinlist = pickle.loads(zlib.decompress(experiment.pinlist)) # unzip pinlist
    if options.verbose > 0:
        print("Version: {}, date: {}, machines: {}, numpints: {}, glitchstart: {}, glitchend: {}, glitchwidth: {}, clocks: {}, len(pinlist): {}, len(results): {}".format(experiment.version, experiment.date, experiment.machines, experiment.numpins, experiment.glitchstart, experiment.glitchend, experiment.glitchwidth, experiment.clocks, len(pinlist), len(results)))

    if experiment.version == 2:
        glitchparams = (gp for (gp, vm, result) in results)
    elif experiment.version == 3:
        glitchparams = (gp for (gp, vm, result, extra_data) in results)
    for params in glitchparams:
        if data is None:
            data = {f: [] for f in params._fields} # Create empty lists
        for field, param in params._asdict().items():
            data[field].append(param)
        if experiment.version == 2:
            data['extra_data'] = [b''] * len(data[field])
    return data

def decodepins(codes, numpins):
    decoded = np.zeros((len(codes), numpins), dtype=np.bool)
    for i, code in enumerate(codes):
        dec = GlitchGenerator.decodepins(code)
        assert len(dec) >= numpins
        decoded[:][i] = dec[:numpins]
    return decoded

def common_start_pins(data, maxnum):
    # Some stats
    start_pins = OrderedDict()
    start_num = OrderedDict()
    common_start = Counter(data['start']).most_common(maxnum)
    for start, num in common_start:
        idxs = np.where(data['start'] == start) # Get idxs for this starting time
        codes = data['code'][idxs]
        pins = np.sum(codes, axis=0) # Sum all pin occurrences
        most = np.flip(np.argsort(pins))
        zeros = np.sum(pins == 0)
        if zeros:
            most = most[:-zeros] # Trim off zeros
        start_num[start] = num
        start_pins[start] = most
    return start_num, start_pins


def common_pins(data, maxnum):
    pin_num = OrderedDict()
    pin_starts = OrderedDict()

    common_pins = np.sum(data['code'], axis=0)
    most = np.flip(np.argsort(common_pins))
    for pin in most[:maxnum]:
        idxs = data['code'][:,pin] == True
        starts = data['start'][idxs]

        pin_num[pin] = common_pins[pin]
        pin_starts[pin] = Counter(starts).most_common(maxnum)


    return pin_num, pin_starts


def histdata(data):
    minstart = np.min(data['start'])
    maxstart = np.max(data['start'])
    minpin = 0
    maxpin = len(data['code'][0])-1

    # Count
    hist = np.zeros((maxpin-minpin+1, maxstart-minstart+1), dtype=np.uint64)
    for idx, start in enumerate(data['start']):
        pins = data['code'][idx]
        hist[:,start-minstart] += pins

    return minstart, maxstart, minpin, maxpin, hist

def find_minimal(pin, start, data):
    idxs = (data['start'] == start) & (data['code'][:,pin]) # Right start and pin
    pincount = np.sum(data['code'][idxs,:], axis=1) # Count pins in remaining indexes
    if len(pincount) == 0:
        return None
    amin = np.argmin(pincount)

    return np.where(idxs)[0][amin] # Return original index

def pinname(pin, gm, short=False):
    if not gm:
        return "@{}".format(pin)

    if short:
        return gm[pin].outputwire
    else:
        return "{}({}=>{}) @{}".format(gm[pin].modulename, gm[pin].inputwire, gm[pin].outputwire, pin)

def pinnames(pins, gm):
    return ", ".join((pinname(pin, gm) for pin in pins))


worker_queue = concurrent.futures.ThreadPoolExecutor(max_workers=multiprocessing.cpu_count())
def write_repro(idx, indata, reprofile, options):
    if options.verbose > 0:
        print(f"[*] Writing idx {idx} to repro file {reprofile}")

    gp = GlitchParams(start=indata['start'][idx], width=indata['width'][idx], clocks=indata['clocks'][idx], code=indata['code'][idx])
    # Create single fault output
    #width = experiment.glitchwidth
    #clocks = experiment.clocks

    #gg = GlitchGenerator(experiment.numpins, experiment.pinlist, width, clocks, experiment.machines, experiment.version)
    #code = gg.pins2bitmap([pin]) # Create code for one pin
    #gp = GlitchParams(start=start, width=width, clocks=clocks, code=code)

    with open(reprofile, "wb") as output:
        glitcher.send_glitch(output, gp)
        glitcher.send_done(output)

    if options.reproduce_run:
        cmd = options.reproduce_run + f" -f {reprofile} 2>&1 > {reprofile}.txt"
        if options.verbose > 1:
            print(f"[*] Queueing {cmd}")
        # Start a reproduce task as future
        worker_queue.submit(os.system, cmd)


def fi_grep(results_file, options):
    
    if options.profile:
        yappi.start()

    if options.glitchmap:
        if options.verbose > 0:
            print("[*] Loading glitchmap")
        with open(options.glitchmap, "rb") as gf:
            gm = pickle.load(gf)
    else:
        gm = None

    # Load data
    indata = None
    resf = open(results_file, "rb")
    while (True):
        try:
            experiment = pickle.load(resf)
        except EOFError:
            break

        indata = get_data(experiment, indata, options)
    resf.close()

    if indata is None:
        print("0 faulty datapoints. Quitting")
        return

    numfault = len(indata['start'])
    print("Data points: {}".format(numfault))
    if options.verbose > 0:
        print("[*] Converting to numpy arrays")

    # Bit ugly to convert here... but during loading it somehow got pretty heavy with array copies etc
    data = dict()
    for k in indata.keys():
        if k == 'code':
            data[k] = decodepins(indata['code'], experiment.numpins)
        else:
            data[k] = np.array(indata[k])

    # Write most  popular starting clocks
    if options.verbose >= 1:
        start_num, start_pins = common_start_pins(data, options.topn)
        for start, num in start_num.items():
            if num > 0:
                print("Start: {}, num: {}/{}%, pins: {}".format(start, num, int(num/numfault*100), pinnames(start_pins[start][:options.topn], gm)))

    # Write most popular pins
    pin_num, pin_starts = common_pins(data, options.topn)
    if options.verbose >= 1:
        for pin, num in pin_num.items():
            if num > 0:
                print("Pin {}, num {}/{}%, starts: {}".format(pinname(pin, gm), num, int(num/numfault*100), pin_starts[pin]))
    if options.write:
        with open(options.write, "w") as pinf:
            pinf.write("\n".join((pinname(pin, gm, short=False) for (pin, num) in pin_num.items() if num > 0)))
    if options.writeoutputname:
        with open(options.writeoutputname, "w") as pinf:
            # Uniqueness is not guaranteed here, multiple input pins can affect same output pin. Therefore sorted(set())
            pinf.write("\n".join( sorted(set(pinname(pin, gm, short=True ) for (pin, num) in pin_num.items() if num > 0)) ))

    # Output figure
    if options.figure:
        if options.verbose > 0:
            print("[*] Gathering plot data")
        fig = plt.figure()
        ax = fig.gca(projection='3d')

        minstart, maxstart, minpin, maxpin, hist = histdata(data)

        if options.verbose > 0:
            print("[*] Plotting to {}".format(options.figure))
        X = np.arange(minstart, maxstart+1, 1)
        Y = np.arange(minpin, maxpin+1, 1)
        X, Y = np.meshgrid(X, Y)
        surf = ax.plot_surface(Y, X, hist, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        #ax.bar3d(Y.ravel(), X.ravel(), np.zeros_like(hist).ravel(), 1, 1, hist.ravel(), shade=True)

        ax.set_ylabel("Clock start")
        ax.set_xlabel("Glitch pin")
        ax.set_zlabel("Count")

        fig.savefig(options.figure)

    glitcher.options = options # Hack for verbose
    if options.reproduce_all:
        # Create folder and loop over all repro
        pathlib.Path(options.reproduce_all).mkdir(parents=True, exist_ok=True)
        for i in range(0, len(data['code'])):
            write_repro(i, indata, options.reproduce_all + "/" + str(i) + ".repro", options) 
    elif options.reproduce:
        pin = list(pin_num.keys())[0] # Grab first pin
        start = pin_starts[pin][0][0] # Grab first start
        idx = find_minimal(pin, start, data)
        write_repro(idx, indata, options.reproduce, options)
    if options.reproduce_run:
        worker_queue.shutdown() # Wait for any tasks running

    if options.csv:
        print("CSV is not yet complete. The glitched pins need to represented")
        with open(options.csv, "w") as csvf:
            keys = list(data.keys())
            csvf.write(",".join(f'"{x}"' for x in keys));
            csvf.write("\n")
            num = len(data[keys[0]])
            for i in range(0, num):
                csvf.write(",".join(str(data[x][i]) for x in keys));
                csvf.write("\n")

    if options.profile:
        yappi.stop()
        stats = yappi.get_func_stats()
        stats.sort('tsub') # Sort by time spent in individual function, excluding subcalls
        stats.print_all()

if __name__ == '__main__':
    fi_grep(*get_opts(sys.argv[1:]))
