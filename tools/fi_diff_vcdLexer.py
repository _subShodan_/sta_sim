# Generated from fi_diff_vcd.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\r")
        buf.write("\u00c5\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2&\n\2\f\2")
        buf.write("\16\2)\13\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\7\3\67\n\3\f\3\16\3:\13\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4M\n")
        buf.write("\4\f\4\16\4P\13\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\7\5_\n\5\f\5\16\5b\13\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6s\n")
        buf.write("\6\f\6\16\6v\13\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\7\7\u0083\n\7\f\7\16\7\u0086\13\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\7\b\u009e\n\b\f\b\16\b\u00a1\13")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\t\6\t\u00a9\n\t\r\t\16\t\u00aa")
        buf.write("\3\n\3\n\6\n\u00af\n\n\r\n\16\n\u00b0\3\13\3\13\6\13\u00b5")
        buf.write("\n\13\r\13\16\13\u00b6\3\13\3\13\3\13\3\f\3\f\3\f\3\r")
        buf.write("\6\r\u00c0\n\r\r\r\16\r\u00c1\3\r\3\r\t\'8N`t\u0084\u009f")
        buf.write("\2\16\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\2\23\n\25\13\27")
        buf.write("\f\31\r\3\2\6\3\2#\u0080\3\2\62;\3\2\62\63\5\2\13\f\17")
        buf.write("\17\"\"\2\u00ce\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2")
        buf.write("\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\23")
        buf.write("\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3")
        buf.write("\2\2\2\5/\3\2\2\2\7@\3\2\2\2\tV\3\2\2\2\13h\3\2\2\2\r")
        buf.write("|\3\2\2\2\17\u008c\3\2\2\2\21\u00a8\3\2\2\2\23\u00ac\3")
        buf.write("\2\2\2\25\u00b2\3\2\2\2\27\u00bb\3\2\2\2\31\u00bf\3\2")
        buf.write("\2\2\33\34\7&\2\2\34\35\7x\2\2\35\36\7g\2\2\36\37\7t\2")
        buf.write("\2\37 \7u\2\2 !\7k\2\2!\"\7q\2\2\"#\7p\2\2#\'\3\2\2\2")
        buf.write("$&\13\2\2\2%$\3\2\2\2&)\3\2\2\2\'(\3\2\2\2\'%\3\2\2\2")
        buf.write("(*\3\2\2\2)\'\3\2\2\2*+\7&\2\2+,\7g\2\2,-\7p\2\2-.\7f")
        buf.write("\2\2.\4\3\2\2\2/\60\7&\2\2\60\61\7f\2\2\61\62\7c\2\2\62")
        buf.write("\63\7v\2\2\63\64\7g\2\2\648\3\2\2\2\65\67\13\2\2\2\66")
        buf.write("\65\3\2\2\2\67:\3\2\2\289\3\2\2\28\66\3\2\2\29;\3\2\2")
        buf.write("\2:8\3\2\2\2;<\7&\2\2<=\7g\2\2=>\7p\2\2>?\7f\2\2?\6\3")
        buf.write("\2\2\2@A\7&\2\2AB\7v\2\2BC\7k\2\2CD\7o\2\2DE\7g\2\2EF")
        buf.write("\7u\2\2FG\7e\2\2GH\7c\2\2HI\7n\2\2IJ\7g\2\2JN\3\2\2\2")
        buf.write("KM\13\2\2\2LK\3\2\2\2MP\3\2\2\2NO\3\2\2\2NL\3\2\2\2OQ")
        buf.write("\3\2\2\2PN\3\2\2\2QR\7&\2\2RS\7g\2\2ST\7p\2\2TU\7f\2\2")
        buf.write("U\b\3\2\2\2VW\7&\2\2WX\7u\2\2XY\7e\2\2YZ\7q\2\2Z[\7r\2")
        buf.write("\2[\\\7g\2\2\\`\3\2\2\2]_\13\2\2\2^]\3\2\2\2_b\3\2\2\2")
        buf.write("`a\3\2\2\2`^\3\2\2\2ac\3\2\2\2b`\3\2\2\2cd\7&\2\2de\7")
        buf.write("g\2\2ef\7p\2\2fg\7f\2\2g\n\3\2\2\2hi\7&\2\2ij\7w\2\2j")
        buf.write("k\7r\2\2kl\7u\2\2lm\7e\2\2mn\7q\2\2no\7r\2\2op\7g\2\2")
        buf.write("pt\3\2\2\2qs\13\2\2\2rq\3\2\2\2sv\3\2\2\2tu\3\2\2\2tr")
        buf.write("\3\2\2\2uw\3\2\2\2vt\3\2\2\2wx\7&\2\2xy\7g\2\2yz\7p\2")
        buf.write("\2z{\7f\2\2{\f\3\2\2\2|}\7&\2\2}~\7x\2\2~\177\7c\2\2\177")
        buf.write("\u0080\7t\2\2\u0080\u0084\3\2\2\2\u0081\u0083\13\2\2\2")
        buf.write("\u0082\u0081\3\2\2\2\u0083\u0086\3\2\2\2\u0084\u0085\3")
        buf.write("\2\2\2\u0084\u0082\3\2\2\2\u0085\u0087\3\2\2\2\u0086\u0084")
        buf.write("\3\2\2\2\u0087\u0088\7&\2\2\u0088\u0089\7g\2\2\u0089\u008a")
        buf.write("\7p\2\2\u008a\u008b\7f\2\2\u008b\16\3\2\2\2\u008c\u008d")
        buf.write("\7&\2\2\u008d\u008e\7g\2\2\u008e\u008f\7p\2\2\u008f\u0090")
        buf.write("\7f\2\2\u0090\u0091\7f\2\2\u0091\u0092\7g\2\2\u0092\u0093")
        buf.write("\7h\2\2\u0093\u0094\7k\2\2\u0094\u0095\7p\2\2\u0095\u0096")
        buf.write("\7k\2\2\u0096\u0097\7v\2\2\u0097\u0098\7k\2\2\u0098\u0099")
        buf.write("\7q\2\2\u0099\u009a\7p\2\2\u009a\u009b\7u\2\2\u009b\u009f")
        buf.write("\3\2\2\2\u009c\u009e\13\2\2\2\u009d\u009c\3\2\2\2\u009e")
        buf.write("\u00a1\3\2\2\2\u009f\u00a0\3\2\2\2\u009f\u009d\3\2\2\2")
        buf.write("\u00a0\u00a2\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\7")
        buf.write("&\2\2\u00a3\u00a4\7g\2\2\u00a4\u00a5\7p\2\2\u00a5\u00a6")
        buf.write("\7f\2\2\u00a6\20\3\2\2\2\u00a7\u00a9\t\2\2\2\u00a8\u00a7")
        buf.write("\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa")
        buf.write("\u00ab\3\2\2\2\u00ab\22\3\2\2\2\u00ac\u00ae\7%\2\2\u00ad")
        buf.write("\u00af\t\3\2\2\u00ae\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2")
        buf.write("\u00b0\u00ae\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\24\3\2")
        buf.write("\2\2\u00b2\u00b4\7d\2\2\u00b3\u00b5\t\4\2\2\u00b4\u00b3")
        buf.write("\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6")
        buf.write("\u00b7\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00b9\5\31\r")
        buf.write("\2\u00b9\u00ba\5\21\t\2\u00ba\26\3\2\2\2\u00bb\u00bc\t")
        buf.write("\4\2\2\u00bc\u00bd\5\21\t\2\u00bd\30\3\2\2\2\u00be\u00c0")
        buf.write("\t\5\2\2\u00bf\u00be\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1")
        buf.write("\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c3\3\2\2\2")
        buf.write("\u00c3\u00c4\b\r\2\2\u00c4\32\3\2\2\2\16\2\'8N`t\u0084")
        buf.write("\u009f\u00aa\u00b0\u00b6\u00c1\3\2\3\2")
        return buf.getvalue()


class fi_diff_vcdLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    VERSION = 1
    DATE = 2
    TIMESCALE = 3
    SCOPE = 4
    UPSCOPE = 5
    VAR = 6
    ENDDEFINITIONS = 7
    CLOCK = 8
    BINNUM = 9
    ONEBIT = 10
    WHITESPACE = 11

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
 ]

    symbolicNames = [ "<INVALID>",
            "VERSION", "DATE", "TIMESCALE", "SCOPE", "UPSCOPE", "VAR", "ENDDEFINITIONS", 
            "CLOCK", "BINNUM", "ONEBIT", "WHITESPACE" ]

    ruleNames = [ "VERSION", "DATE", "TIMESCALE", "SCOPE", "UPSCOPE", "VAR", 
                  "ENDDEFINITIONS", "VARNAME", "CLOCK", "BINNUM", "ONEBIT", 
                  "WHITESPACE" ]

    grammarFileName = "fi_diff_vcd.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


