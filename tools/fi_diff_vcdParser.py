# Generated from fi_diff_vcd.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\r")
        buf.write("K\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t")
        buf.write("\16\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3")
        buf.write("\4\7\4*\n\4\f\4\16\4-\13\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b")
        buf.write("\3\b\3\b\7\b8\n\b\f\b\16\b;\13\b\3\b\3\b\3\t\3\t\3\n\3")
        buf.write("\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\2\2\17\2\4")
        buf.write("\6\b\n\f\16\20\22\24\26\30\32\2\2\2B\2\34\3\2\2\2\4 \3")
        buf.write("\2\2\2\6+\3\2\2\2\b.\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2")
        buf.write("\16\64\3\2\2\2\20>\3\2\2\2\22@\3\2\2\2\24B\3\2\2\2\26")
        buf.write("D\3\2\2\2\30F\3\2\2\2\32H\3\2\2\2\34\35\5\4\3\2\35\36")
        buf.write("\5\6\4\2\36\37\7\2\2\3\37\3\3\2\2\2 !\5\b\5\2!\"\5\n\6")
        buf.write("\2\"#\5\f\7\2#$\5\16\b\2$%\5\20\t\2%\5\3\2\2\2&*\5\26")
        buf.write("\f\2\'*\5\30\r\2(*\5\32\16\2)&\3\2\2\2)\'\3\2\2\2)(\3")
        buf.write("\2\2\2*-\3\2\2\2+)\3\2\2\2+,\3\2\2\2,\7\3\2\2\2-+\3\2")
        buf.write("\2\2./\7\3\2\2/\t\3\2\2\2\60\61\7\4\2\2\61\13\3\2\2\2")
        buf.write("\62\63\7\5\2\2\63\r\3\2\2\2\649\7\6\2\2\658\5\22\n\2\66")
        buf.write("8\5\16\b\2\67\65\3\2\2\2\67\66\3\2\2\28;\3\2\2\29\67\3")
        buf.write("\2\2\29:\3\2\2\2:<\3\2\2\2;9\3\2\2\2<=\5\24\13\2=\17\3")
        buf.write("\2\2\2>?\7\t\2\2?\21\3\2\2\2@A\7\b\2\2A\23\3\2\2\2BC\7")
        buf.write("\7\2\2C\25\3\2\2\2DE\7\n\2\2E\27\3\2\2\2FG\7\13\2\2G\31")
        buf.write("\3\2\2\2HI\7\f\2\2I\33\3\2\2\2\6)+\679")
        return buf.getvalue()


class fi_diff_vcdParser ( Parser ):

    grammarFileName = "fi_diff_vcd.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [  ]

    symbolicNames = [ "<INVALID>", "VERSION", "DATE", "TIMESCALE", "SCOPE", 
                      "UPSCOPE", "VAR", "ENDDEFINITIONS", "CLOCK", "BINNUM", 
                      "ONEBIT", "WHITESPACE" ]

    RULE_vcdfile = 0
    RULE_head = 1
    RULE_body = 2
    RULE_version = 3
    RULE_date = 4
    RULE_timescale = 5
    RULE_scope = 6
    RULE_enddefinitions = 7
    RULE_var = 8
    RULE_upscope = 9
    RULE_clock = 10
    RULE_binnum = 11
    RULE_onebit = 12

    ruleNames =  [ "vcdfile", "head", "body", "version", "date", "timescale", 
                   "scope", "enddefinitions", "var", "upscope", "clock", 
                   "binnum", "onebit" ]

    EOF = Token.EOF
    VERSION=1
    DATE=2
    TIMESCALE=3
    SCOPE=4
    UPSCOPE=5
    VAR=6
    ENDDEFINITIONS=7
    CLOCK=8
    BINNUM=9
    ONEBIT=10
    WHITESPACE=11

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class VcdfileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def head(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.HeadContext,0)


        def body(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.BodyContext,0)


        def EOF(self):
            return self.getToken(fi_diff_vcdParser.EOF, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_vcdfile

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVcdfile" ):
                listener.enterVcdfile(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVcdfile" ):
                listener.exitVcdfile(self)




    def vcdfile(self):

        localctx = fi_diff_vcdParser.VcdfileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_vcdfile)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self.head()
            self.state = 27
            self.body()
            self.state = 28
            self.match(fi_diff_vcdParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HeadContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def version(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.VersionContext,0)


        def date(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.DateContext,0)


        def timescale(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.TimescaleContext,0)


        def scope(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.ScopeContext,0)


        def enddefinitions(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.EnddefinitionsContext,0)


        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_head

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterHead" ):
                listener.enterHead(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitHead" ):
                listener.exitHead(self)




    def head(self):

        localctx = fi_diff_vcdParser.HeadContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_head)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 30
            self.version()
            self.state = 31
            self.date()
            self.state = 32
            self.timescale()
            self.state = 33
            self.scope()
            self.state = 34
            self.enddefinitions()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def clock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(fi_diff_vcdParser.ClockContext)
            else:
                return self.getTypedRuleContext(fi_diff_vcdParser.ClockContext,i)


        def binnum(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(fi_diff_vcdParser.BinnumContext)
            else:
                return self.getTypedRuleContext(fi_diff_vcdParser.BinnumContext,i)


        def onebit(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(fi_diff_vcdParser.OnebitContext)
            else:
                return self.getTypedRuleContext(fi_diff_vcdParser.OnebitContext,i)


        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBody" ):
                listener.enterBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBody" ):
                listener.exitBody(self)




    def body(self):

        localctx = fi_diff_vcdParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << fi_diff_vcdParser.CLOCK) | (1 << fi_diff_vcdParser.BINNUM) | (1 << fi_diff_vcdParser.ONEBIT))) != 0):
                self.state = 39
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [fi_diff_vcdParser.CLOCK]:
                    self.state = 36
                    self.clock()
                    pass
                elif token in [fi_diff_vcdParser.BINNUM]:
                    self.state = 37
                    self.binnum()
                    pass
                elif token in [fi_diff_vcdParser.ONEBIT]:
                    self.state = 38
                    self.onebit()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 43
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VersionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VERSION(self):
            return self.getToken(fi_diff_vcdParser.VERSION, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_version

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVersion" ):
                listener.enterVersion(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVersion" ):
                listener.exitVersion(self)




    def version(self):

        localctx = fi_diff_vcdParser.VersionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_version)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.match(fi_diff_vcdParser.VERSION)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DateContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DATE(self):
            return self.getToken(fi_diff_vcdParser.DATE, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_date

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDate" ):
                listener.enterDate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDate" ):
                listener.exitDate(self)




    def date(self):

        localctx = fi_diff_vcdParser.DateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_date)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self.match(fi_diff_vcdParser.DATE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TimescaleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TIMESCALE(self):
            return self.getToken(fi_diff_vcdParser.TIMESCALE, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_timescale

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTimescale" ):
                listener.enterTimescale(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTimescale" ):
                listener.exitTimescale(self)




    def timescale(self):

        localctx = fi_diff_vcdParser.TimescaleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_timescale)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(fi_diff_vcdParser.TIMESCALE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ScopeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SCOPE(self):
            return self.getToken(fi_diff_vcdParser.SCOPE, 0)

        def upscope(self):
            return self.getTypedRuleContext(fi_diff_vcdParser.UpscopeContext,0)


        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(fi_diff_vcdParser.VarContext)
            else:
                return self.getTypedRuleContext(fi_diff_vcdParser.VarContext,i)


        def scope(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(fi_diff_vcdParser.ScopeContext)
            else:
                return self.getTypedRuleContext(fi_diff_vcdParser.ScopeContext,i)


        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_scope

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScope" ):
                listener.enterScope(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScope" ):
                listener.exitScope(self)




    def scope(self):

        localctx = fi_diff_vcdParser.ScopeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_scope)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 50
            self.match(fi_diff_vcdParser.SCOPE)
            self.state = 55
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==fi_diff_vcdParser.SCOPE or _la==fi_diff_vcdParser.VAR:
                self.state = 53
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [fi_diff_vcdParser.VAR]:
                    self.state = 51
                    self.var()
                    pass
                elif token in [fi_diff_vcdParser.SCOPE]:
                    self.state = 52
                    self.scope()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 57
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 58
            self.upscope()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class EnddefinitionsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ENDDEFINITIONS(self):
            return self.getToken(fi_diff_vcdParser.ENDDEFINITIONS, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_enddefinitions

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnddefinitions" ):
                listener.enterEnddefinitions(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnddefinitions" ):
                listener.exitEnddefinitions(self)




    def enddefinitions(self):

        localctx = fi_diff_vcdParser.EnddefinitionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_enddefinitions)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self.match(fi_diff_vcdParser.ENDDEFINITIONS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(fi_diff_vcdParser.VAR, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar" ):
                listener.enterVar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar" ):
                listener.exitVar(self)




    def var(self):

        localctx = fi_diff_vcdParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_var)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self.match(fi_diff_vcdParser.VAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class UpscopeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def UPSCOPE(self):
            return self.getToken(fi_diff_vcdParser.UPSCOPE, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_upscope

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUpscope" ):
                listener.enterUpscope(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUpscope" ):
                listener.exitUpscope(self)




    def upscope(self):

        localctx = fi_diff_vcdParser.UpscopeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_upscope)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.match(fi_diff_vcdParser.UPSCOPE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ClockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CLOCK(self):
            return self.getToken(fi_diff_vcdParser.CLOCK, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_clock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClock" ):
                listener.enterClock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClock" ):
                listener.exitClock(self)




    def clock(self):

        localctx = fi_diff_vcdParser.ClockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_clock)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 66
            self.match(fi_diff_vcdParser.CLOCK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BinnumContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BINNUM(self):
            return self.getToken(fi_diff_vcdParser.BINNUM, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_binnum

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinnum" ):
                listener.enterBinnum(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinnum" ):
                listener.exitBinnum(self)




    def binnum(self):

        localctx = fi_diff_vcdParser.BinnumContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_binnum)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.match(fi_diff_vcdParser.BINNUM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class OnebitContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ONEBIT(self):
            return self.getToken(fi_diff_vcdParser.ONEBIT, 0)

        def getRuleIndex(self):
            return fi_diff_vcdParser.RULE_onebit

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOnebit" ):
                listener.enterOnebit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOnebit" ):
                listener.exitOnebit(self)




    def onebit(self):

        localctx = fi_diff_vcdParser.OnebitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_onebit)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.match(fi_diff_vcdParser.ONEBIT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





