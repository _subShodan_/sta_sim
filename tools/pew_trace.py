#!/usr/bin/python3

from pyverilog.dataflow.dataflow_analyzer import VerilogDataflowAnalyzer

import os, sys, re
from itertools import chain
import tempfile
from liberty.parser import parse_liberty
import pyverilog.vparser.ast as vast
import pyverilog.vparser.parser as vparser
import pyverilog.dataflow.dataflow as vdf
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
import pickle
from optparse import OptionParser
from collections import namedtuple

import IPython

def get_opts(cmdline):
    USAGE_STRING = "Usage: pew_trace.py [options] -t <netlist top> -i <intermediate> -o <outputfile> [netlist inputs]\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", "--define", dest="define", action="append", default=[], help="Macro Definition")
    optparser.add_option("-n", "--nofix", dest="fix", action="store_false", default=True, help="Don't fix up the netlist implicit wire declarations")
    optparser.add_option("-l", "--long", dest="long", action="store_true", default=False, help="Give 'long' output, which includes all vector indices, instead of consolidating them")
    optparser.add_option("-t", "--top", dest="top", action="store", default=None, help="Netlist top")
    optparser.add_option("-i", "--intermediate", dest="intermediates", action="append", default=[], help="Intermediates to trace. May be supplied multiple times")
    optparser.add_option("-f", "--intermediate-file", dest="intermediate_file", action="store", default=None, help="Intermediates to trace. May be supplied multiple times")
    optparser.add_option("-o", "--output", dest="output", action="store", default=None, help="Output file")
    optparser.add_option("-g", "--graph", dest="graph", action="store", default=None, help="Graphviz file.")

    (options, args) = optparser.parse_args(cmdline)

    if len(args) == 0:
        print("No netlist inputs specified. Exiting")
        sys.exit(-1)

    if options.top is None:
        print("No top specified. Try -t <top>. Exiting")
        sys.exit(-1)

    if len(options.intermediates) == 0 and options.intermediate_file is None:
        print("No intermediates or intermediates file specified. Try -i <intermediate> or -f <file>. Exiting")
        sys.exit(-1)

    if options.output is None:
        print("No output file specified. Try -o <outputfile>. Exiting")
        sys.exit(-1)

    return (args, options)

def walk_deps(tree):
    # Walk dependency tree for a certain tree node. Operations require recursion.

    if isinstance(tree, vdf.DFTerminal):
        # Terminal node
        assert len(tree.children()) == 0, "Children: {}".format(tree.children())
        deps = set([str(tree.name)])
    elif isinstance(tree, vdf.DFPartselect):
        assert len(tree.children()) == 3, "Children: {}".format(tree.children())
        deps = set([tree.tocode()]) 
    elif isinstance(tree, vdf.DFOperator):
        assert len(tree.children()) > 0, "Children: {}".format(tree.children())
        # Operators may have multiple operands
        deps = set()
        for dep in tree.children():
            deps |= walk_deps(dep)
    elif isinstance(tree, vdf.DFIntConst):
        # Int consts have no dependencies
        assert len(tree.children()) == 0, "Children: {}".format(tree.children())
        deps = set()
    else:
        print("Unknown tree node type: {} for {}".format(type(tree), tree.tostr()))
        IPython.embed()
        assert False

    return deps

def fix_netlist(netlistfiles, options):
    # Add all missing wire declarations explicitly. The dataflow graph parser needs this...
    if options.verbose:
        print("fixing netlist")

    ast = vparser.VerilogCodeParser(netlistfiles, preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    # Start with empty glitchmap, iterate through modules
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue

        wires = set()
        # Collect wires for each netlist item
        for item in definition.items:
            if isinstance(item, vast.InstanceList):
                for instance in item.instances:
                    for port in instance.portlist:
                        if isinstance(port.argname, vast.Identifier):
                            wires.add(str(port.argname))
                        elif isinstance(port.argname, vast.Pointer):
                            if options.verbose >= 1:
                                print("Ignoring port arg {}[{}]".format(port.argname.var, port.argname.ptr)); # Assuming this is some module i/o and therefore already defined
                        else:
                            assert false, "Port {} is a {}".format(str(port.argname), type(port.argname))

        # Prepend wire definitions
        wiredefs = tuple([vast.Decl( tuple([vast.Wire(name) for name in wires]))])
        definition.items = wiredefs + definition.items

    tmpfile = tempfile.NamedTemporaryFile(mode="w", delete=False)
    if options.verbose:
        print("generating code in {}".format(tmpfile.name))

    codegen = ASTCodeGenerator()
    rslt = codegen.visit(ast)

    tmpfile.write(rslt)
    tmpfile.flush()

    return tmpfile.name

def trace(name, graph, options):
    # This is basically an iterative version that keeps a work stack "todo" and all items processed in "done".
    # I'm using sets() to avoid duplicated work
    # When I wrote this recursive, I ran out of Python stack
    # This can probably be sped up significantly by having a graph with actual pointers instead of having dict lookups

    todo = set([name]) # All items still to process
    done = set()     # All items already processed
    deps = set()     # RTL dependencies


    while len(todo) > 0:
        item = todo.pop() # Grab random item
        done.add(item)    # Add to processed list

        if re.search(r'_\d+_', item): # XXX it's kind of a hack to go by naming scheme. A dev could name a var this way in the code and it would be recognized as internal wire.
            if item in graph:
                # Add all items from dependency
                todo.update(graph[item])
                # Remove all items we've already processed
                todo.difference_update(done)
            else:
                deps.add(f"{item}_dangling")
                if options.verbose > 0:
                    print(f"WARNING: {item} is not in graph. Creating dangling wire as dependency")
        else:
            # Name is like RTL name and not wire name => stop search and add as dep
            deps.add(item)

    return frozenset(deps)

def load_input(netlistfiles, options):
    # pew_trace verilog
    if options.fix:
        netlistfiles = (fix_netlist(netlistfiles, options))

    if options.verbose:
        print("parsing netlist from {}".format(netlistfiles))

    # Hack to work around VerilogDataflowAnalyzer outputting temp files in current dir. Doing this in parallel gives race conditions. XXX
    cwd = os.getcwd() 
    with tempfile.TemporaryDirectory() as td:
        os.chdir(td)

        analyzer = VerilogDataflowAnalyzer(netlistfiles, options.top, preprocess_include=options.include, preprocess_define=options.define)
        analyzer.generate()
        result = analyzer.getBinddict()

        os.chdir(cwd)

    if options.fix:
        if options.verbose:
            print("deleting temp file {}".format(netlistfiles))
        os.unlink(netlistfiles)

    return result


def create_graph(binddict, options):
    if options.verbose:
        print("creating netlist graph...")

    # For each backward edge, add forward edges
    fwdgraph = {}
    # Make simplified graph
    backgraph = {}
    for (binding, backdeps) in binddict.items():
        for backdep in backdeps:
            assert binding == backdep.dest, IPython.embed() # Just to make sure these are the same
            if options.verbose >= 2:
                print("Processing {} => {}".format(binding, backdep.tostr()))

            # Get backward dependencies, and add
            if backdep.lsb is not None or backdep.msb is not None:
                # Hack to deal with top module ports that get assigned to vectors
                if options.verbose >= 2:
                    print("Dealing with vector slice {}".format(backdep.tostr()))
                assert backdep.lsb == backdep.msb # Netlist assumption seems valid
                lsb = backdep.lsb.tostr()
                assert lsb.startswith("'d") # Remove decimal prefix
                lsb = lsb[2:]
                bindtarget = "{}[{}]".format(binding.tocode(), lsb)
            else:
                bindtarget = str(binding)
            assert bindtarget not in backgraph
            deps = walk_deps(backdep.tree)
            backgraph[bindtarget] = deps
            if options.verbose >= 2:
                print("Adding back edge {} <= {}".format(bindtarget, deps))

            # Now add all forward edges
            for dep in deps:
                if dep not in fwdgraph:
                    fwdgraph[dep] = set()
                fwdgraph[dep].add(bindtarget)
                if options.verbose >= 2:
                    print("Adding forward edge {} => {}".format(dep, bindtarget))

    return fwdgraph, backgraph

def trace_intermediates(intermediates, fwdgraph, backgraph, options):
    # Overall results. Using a set as key to de-duplicate.
    # The goal here is to trace back/fwd for each intermediate, and also consolidate intermediates with the same back/fwd dependencies. E.g. _1_ -> _2_ -> A, would result in a fwd dependency of [_1_, _2_] -> [A]
    fwds = dict()
    backs = dict()

    # Trace intermediates
    for intermediate in intermediates:
        intermediate = "{}.{}".format(options.top, intermediate)
        if options.verbose:
            print("tracing {}".format(intermediate))

        # Trace
        fwddeps = trace(intermediate, fwdgraph, options)
        backdeps = trace(intermediate, backgraph, options)

        if not options.long:
            # Remove any trailing [0-9] indices
            fwddeps  = frozenset([re.sub(r'\[\d+\]', '[]', d) for d in fwddeps])
            backdeps = frozenset([re.sub(r'\[\d+\]', '[]', d) for d in backdeps])

        # Add to results
        if fwddeps not in fwds:
            fwds[fwddeps] = set()
        fwds[fwddeps].add(intermediate)

        if backdeps not in backs:
            backs[backdeps] = set()
        backs[backdeps].add(intermediate)

    # Write output file
    with open(options.output, "w") as outf:
        for dep, intermediate in fwds.items():
            outf.write("Intermediate {} forward deps : {}\n".format(sorted(list(intermediate)), sorted(list(dep)) ))
        for dep, intermediate in backs.items():
            outf.write("Intermediate {} backward deps: {}\n".format(sorted(list(intermediate)), sorted(list(dep)) ))

    # Write graph file
    if options.graph:
        with open(options.graph, "w") as gf:
            numcolor = 8
            gf.write("digraph pew_trace {\n");
            gf.write("rankdir = LR;\n");
            gf.write("splines = false;\n");
            gf.write("node [shape=box, fontname=Monospace, colorscheme=accent{}];\n".format(numcolor));
            gf.write("edge [           fontname=Monospace, colorscheme=accent{}];\n".format(numcolor));

            # Write subgraphs
            gf.write("subgraph back_0 {\n");
            gf.write("\n".join(sorted(['"{}";'.format(x) for y in backs.keys() for x in y])));
            gf.write("\n}\n");

            gf.write("subgraph int_1 {\n");
            gf.write("rank=same;\n")
            ints = sorted(set([x for y in chain(backs.values(), fwds.values()) for x in y])) # Sort uniquely
            gf.write("\n".join(sorted(['"{}" [style=filled, fillcolor={}];'.format(x, color%numcolor+1) for color, x in enumerate(ints)])));
            gf.write("\n}\n");

            gf.write("subgraph fwd_2 {\n");
            gf.write("\n".join(sorted(['"{}";'.format(x) for y in fwds.keys() for x in y])));
            gf.write("\n}\n");

            # Wrote fwd edges
            # Use index into list of intermediates "ints" to determine color
            for intermediates, deps in sorted(fwds.items()):
                for intermediate in sorted(intermediates):
                    for dep in sorted(deps):
                        gf.write('"{}" -> "{}" [headport=w, color={}];\n'.format(dep, intermediate, ints.index(dep)%numcolor + 1))

            # Write back edges
            for intermediates, deps in sorted(backs.items()):
                for intermediate in sorted(intermediates):
                    for dep in sorted(deps):
                        gf.write('"{}" -> "{}" [headport=w, color={}];\n'.format(intermediate, dep, ints.index(dep)%numcolor + 1))

            gf.write("}\n")


def get_intermediates(intermediates, intermediate_file):
    result = list()
    if intermediates:
        result = list(intermediates)
    if intermediate_file:
        with open(intermediate_file) as f:
            result.extend(f.read().splitlines())
    return result

def pew_trace(netlistfiles, options):
    binddict = load_input(netlistfiles, options)
    fwdgraph, backgraph = create_graph(binddict, options)
    intermediates = get_intermediates(options.intermediates, options.intermediate_file)
    trace_intermediates(intermediates, fwdgraph, backgraph, options)

if __name__ == '__main__':
    pew_trace(*get_opts(sys.argv[1:]))

