

module counter
(
  clk,
  rst,
  counter_out
);

  input clk;
  input rst;
  output [63:0] counter_out;

  sky130_fd_sc_hs__buf_8
  _226_
  (
    .A(counter_out[46]),
    .X(_064_)
  );


  sky130_fd_sc_hs__buf_4
  _227_
  (
    .A(counter_out[40]),
    .X(_065_)
  );


  sky130_fd_sc_hs__buf_8
  _228_
  (
    .A(counter_out[38]),
    .X(_066_)
  );


  sky130_fd_sc_hs__and4_4
  _229_
  (
    .A(_066_),
    .B(counter_out[39]),
    .C(counter_out[36]),
    .D(counter_out[37]),
    .X(_067_)
  );


  sky130_fd_sc_hs__buf_8
  _230_
  (
    .A(counter_out[34]),
    .X(_068_)
  );


  sky130_fd_sc_hs__and3_4
  _231_
  (
    .A(_068_),
    .B(counter_out[35]),
    .C(counter_out[33]),
    .X(_069_)
  );


  sky130_fd_sc_hs__buf_4
  _232_
  (
    .A(counter_out[0]),
    .X(_070_)
  );


  sky130_fd_sc_hs__and4_4
  _233_
  (
    .A(counter_out[2]),
    .B(counter_out[3]),
    .C(_070_),
    .D(counter_out[1]),
    .X(_071_)
  );


  sky130_fd_sc_hs__and4_4
  _235_
  (
    .A(counter_out[8]),
    .B(counter_out[9]),
    .C(counter_out[7]),
    .D(_072_),
    .X(_073_)
  );


  sky130_fd_sc_hs__buf_8
  _236_
  (
    .A(counter_out[11]),
    .X(_074_)
  );


  sky130_fd_sc_hs__buf_1
  _237_
  (
    .A(counter_out[14]),
    .X(_075_)
  );


  sky130_fd_sc_hs__and2_4
  _238_
  (
    .A(_075_),
    .B(counter_out[13]),
    .X(_076_)
  );


  sky130_fd_sc_hs__and4_4
  _239_
  (
    .A(counter_out[15]),
    .B(counter_out[12]),
    .C(_074_),
    .D(_076_),
    .X(_077_)
  );


  sky130_fd_sc_hs__buf_1
  _240_
  (
    .A(counter_out[22]),
    .X(_078_)
  );


  sky130_fd_sc_hs__buf_1
  _241_
  (
    .A(counter_out[20]),
    .X(_079_)
  );


  sky130_fd_sc_hs__buf_1
  _242_
  (
    .A(counter_out[21]),
    .X(_080_)
  );


  sky130_fd_sc_hs__and4_4
  _243_
  (
    .A(_078_),
    .B(counter_out[23]),
    .C(_079_),
    .D(_080_),
    .X(_081_)
  );


  sky130_fd_sc_hs__buf_1
  _244_
  (
    .A(counter_out[25]),
    .X(_082_)
  );


  sky130_fd_sc_hs__and4_4
  _245_
  (
    .A(counter_out[26]),
    .B(counter_out[27]),
    .C(counter_out[24]),
    .D(_082_),
    .X(_083_)
  );


  sky130_fd_sc_hs__buf_1
  _246_
  (
    .A(counter_out[19]),
    .X(_084_)
  );


  sky130_fd_sc_hs__buf_1
  _247_
  (
    .A(counter_out[17]),
    .X(_085_)
  );


  sky130_fd_sc_hs__and4_4
  _248_
  (
    .A(counter_out[18]),
    .B(_084_),
    .C(counter_out[16]),
    .D(_085_),
    .X(_086_)
  );


  sky130_fd_sc_hs__buf_1
  _249_
  (
    .A(counter_out[30]),
    .X(_087_)
  );


  sky130_fd_sc_hs__and4_4
  _250_
  (
    .A(_087_),
    .B(counter_out[31]),
    .C(counter_out[28]),
    .D(counter_out[29]),
    .X(_088_)
  );


  sky130_fd_sc_hs__and4_4
  _251_
  (
    .A(_081_),
    .B(_083_),
    .C(_086_),
    .D(_088_),
    .X(_089_)
  );


  sky130_fd_sc_hs__and4_4
  _252_
  (
    .A(counter_out[10]),
    .B(_073_),
    .C(_077_),
    .D(_089_),
    .X(_090_)
  );


  sky130_fd_sc_hs__and4_4
  _253_
  (
    .A(counter_out[32]),
    .B(_067_),
    .C(_069_),
    .D(_090_),
    .X(_091_)
  );


  sky130_fd_sc_hs__and4_4
  _254_
  (
    .A(counter_out[42]),
    .B(_065_),
    .C(counter_out[41]),
    .D(_091_),
    .X(_092_)
  );


  sky130_fd_sc_hs__and4_4
  _255_
  (
    .A(counter_out[44]),
    .B(counter_out[45]),
    .C(counter_out[43]),
    .D(_092_),
    .X(_093_)
  );


  sky130_fd_sc_hs__buf_8
  _256_
  (
    .A(_093_),
    .X(_094_)
  );


  sky130_fd_sc_hs__buf_1
  _257_
  (
    .A(rst),
    .X(_095_)
  );


  sky130_fd_sc_hs__clkdlyinv3sd2_1
  _258_
  (
    .A(_095_),
    .Y(_096_)
  );


  sky130_fd_sc_hs__buf_1
  _259_
  (
    .A(_096_),
    .X(_097_)
  );


  sky130_fd_sc_hs__buf_1
  _260_
  (
    .A(_097_),
    .X(_098_)
  );


  sky130_fd_sc_hs__o21ai_4
  _261_
  (
    .A1(_064_),
    .A2(_094_),
    .B1(_098_),
    .Y(_099_)
  );


  sky130_fd_sc_hs__a21oi_4
  _262_
  (
    .A1(_064_),
    .A2(_094_),
    .B1(_099_),
    .Y(_040_)
  );


  sky130_fd_sc_hs__buf_1
  _263_
  (
    .A(_095_),
    .X(_100_)
  );


  sky130_fd_sc_hs__a21oi_4
  _264_
  (
    .A1(_064_),
    .A2(_094_),
    .B1(counter_out[47]),
    .Y(_101_)
  );


  sky130_fd_sc_hs__and3_4
  _265_
  (
    .A(_064_),
    .B(counter_out[47]),
    .C(_094_),
    .X(_102_)
  );


  sky130_fd_sc_hs__nor3_4
  _266_
  (
    .A(_100_),
    .B(_101_),
    .C(_102_),
    .Y(_041_)
  );


  sky130_fd_sc_hs__buf_1
  _267_
  (
    .A(_097_),
    .X(_103_)
  );


  sky130_fd_sc_hs__o21ai_4
  _268_
  (
    .A1(counter_out[48]),
    .A2(_102_),
    .B1(_103_),
    .Y(_104_)
  );


  sky130_fd_sc_hs__and4_4
  _269_
  (
    .A(_064_),
    .B(counter_out[47]),
    .C(counter_out[48]),
    .D(_094_),
    .X(_105_)
  );


  sky130_fd_sc_hs__buf_8
  _270_
  (
    .A(_105_),
    .X(_106_)
  );


  sky130_fd_sc_hs__nor2_4
  _271_
  (
    .A(_104_),
    .B(_106_),
    .Y(_042_)
  );


  sky130_fd_sc_hs__buf_8
  _272_
  (
    .A(counter_out[49]),
    .X(_107_)
  );


  sky130_fd_sc_hs__o21ai_4
  _273_
  (
    .A1(_107_),
    .A2(_106_),
    .B1(_098_),
    .Y(_108_)
  );


  sky130_fd_sc_hs__a21oi_4
  _274_
  (
    .A1(_107_),
    .A2(_106_),
    .B1(_108_),
    .Y(_043_)
  );


  sky130_fd_sc_hs__a21oi_4
  _275_
  (
    .A1(_107_),
    .A2(_106_),
    .B1(counter_out[50]),
    .Y(_109_)
  );


  sky130_fd_sc_hs__and3_4
  _276_
  (
    .A(_107_),
    .B(counter_out[50]),
    .C(_106_),
    .X(_110_)
  );


  sky130_fd_sc_hs__nor3_4
  _277_
  (
    .A(_100_),
    .B(_109_),
    .C(_110_),
    .Y(_045_)
  );


  sky130_fd_sc_hs__o21ai_4
  _278_
  (
    .A1(counter_out[51]),
    .A2(_110_),
    .B1(_103_),
    .Y(_111_)
  );


  sky130_fd_sc_hs__and4_4
  _279_
  (
    .A(_107_),
    .B(counter_out[50]),
    .C(counter_out[51]),
    .D(_106_),
    .X(_112_)
  );


  sky130_fd_sc_hs__buf_8
  _280_
  (
    .A(_112_),
    .X(_113_)
  );


  sky130_fd_sc_hs__nor2_4
  _281_
  (
    .A(_111_),
    .B(_113_),
    .Y(_046_)
  );


  sky130_fd_sc_hs__buf_8
  _282_
  (
    .A(counter_out[52]),
    .X(_114_)
  );


  sky130_fd_sc_hs__o21ai_4
  _283_
  (
    .A1(_114_),
    .A2(_113_),
    .B1(_098_),
    .Y(_115_)
  );


  sky130_fd_sc_hs__a21oi_4
  _284_
  (
    .A1(_114_),
    .A2(_113_),
    .B1(_115_),
    .Y(_047_)
  );


  sky130_fd_sc_hs__a21oi_4
  _285_
  (
    .A1(_114_),
    .A2(_113_),
    .B1(counter_out[53]),
    .Y(_116_)
  );


  sky130_fd_sc_hs__and3_4
  _286_
  (
    .A(_114_),
    .B(counter_out[53]),
    .C(_113_),
    .X(_117_)
  );


  sky130_fd_sc_hs__nor3_4
  _287_
  (
    .A(_100_),
    .B(_116_),
    .C(_117_),
    .Y(_048_)
  );


  sky130_fd_sc_hs__o21ai_4
  _288_
  (
    .A1(counter_out[54]),
    .A2(_117_),
    .B1(_103_),
    .Y(_118_)
  );


  sky130_fd_sc_hs__and4_4
  _289_
  (
    .A(_114_),
    .B(counter_out[53]),
    .C(counter_out[54]),
    .D(_113_),
    .X(_119_)
  );


  sky130_fd_sc_hs__buf_8
  _290_
  (
    .A(_119_),
    .X(_120_)
  );


  sky130_fd_sc_hs__nor2_4
  _291_
  (
    .A(_118_),
    .B(_120_),
    .Y(_049_)
  );


  sky130_fd_sc_hs__buf_8
  _292_
  (
    .A(counter_out[55]),
    .X(_121_)
  );


  sky130_fd_sc_hs__o21ai_4
  _293_
  (
    .A1(_121_),
    .A2(_120_),
    .B1(_098_),
    .Y(_122_)
  );


  sky130_fd_sc_hs__a21oi_4
  _294_
  (
    .A1(_121_),
    .A2(_120_),
    .B1(_122_),
    .Y(_050_)
  );


  sky130_fd_sc_hs__a21oi_4
  _295_
  (
    .A1(_121_),
    .A2(_120_),
    .B1(counter_out[56]),
    .Y(_123_)
  );


  sky130_fd_sc_hs__and3_4
  _296_
  (
    .A(_121_),
    .B(counter_out[56]),
    .C(_120_),
    .X(_124_)
  );


  sky130_fd_sc_hs__nor3_4
  _297_
  (
    .A(_100_),
    .B(_123_),
    .C(_124_),
    .Y(_051_)
  );


  sky130_fd_sc_hs__o21ai_4
  _298_
  (
    .A1(counter_out[57]),
    .A2(_124_),
    .B1(_103_),
    .Y(_125_)
  );


  sky130_fd_sc_hs__and4_4
  _299_
  (
    .A(_121_),
    .B(counter_out[56]),
    .C(counter_out[57]),
    .D(_120_),
    .X(_126_)
  );


  sky130_fd_sc_hs__buf_8
  _300_
  (
    .A(_126_),
    .X(_127_)
  );


  sky130_fd_sc_hs__nor2_4
  _301_
  (
    .A(_125_),
    .B(_127_),
    .Y(_052_)
  );


  sky130_fd_sc_hs__buf_8
  _302_
  (
    .A(counter_out[58]),
    .X(_128_)
  );


  sky130_fd_sc_hs__o21ai_4
  _303_
  (
    .A1(_128_),
    .A2(_127_),
    .B1(_098_),
    .Y(_129_)
  );


  sky130_fd_sc_hs__a21oi_4
  _304_
  (
    .A1(_128_),
    .A2(_127_),
    .B1(_129_),
    .Y(_053_)
  );


  sky130_fd_sc_hs__a21oi_4
  _305_
  (
    .A1(_128_),
    .A2(_127_),
    .B1(counter_out[59]),
    .Y(_130_)
  );


  sky130_fd_sc_hs__and3_4
  _306_
  (
    .A(_128_),
    .B(counter_out[59]),
    .C(_127_),
    .X(_131_)
  );


  sky130_fd_sc_hs__nor3_4
  _307_
  (
    .A(_100_),
    .B(_130_),
    .C(_131_),
    .Y(_054_)
  );


  sky130_fd_sc_hs__o21ai_4
  _308_
  (
    .A1(counter_out[60]),
    .A2(_131_),
    .B1(_103_),
    .Y(_132_)
  );


  sky130_fd_sc_hs__and4_4
  _309_
  (
    .A(_128_),
    .B(counter_out[59]),
    .C(counter_out[60]),
    .D(_127_),
    .X(_133_)
  );


  sky130_fd_sc_hs__buf_8
  _310_
  (
    .A(_133_),
    .X(_134_)
  );


  sky130_fd_sc_hs__nor2_4
  _311_
  (
    .A(_132_),
    .B(_134_),
    .Y(_056_)
  );


  sky130_fd_sc_hs__buf_8
  _312_
  (
    .A(counter_out[61]),
    .X(_135_)
  );


  sky130_fd_sc_hs__o21ai_4
  _313_
  (
    .A1(_135_),
    .A2(_134_),
    .B1(_098_),
    .Y(_136_)
  );


  sky130_fd_sc_hs__a21oi_4
  _314_
  (
    .A1(_135_),
    .A2(_134_),
    .B1(_136_),
    .Y(_057_)
  );


  sky130_fd_sc_hs__buf_1
  _315_
  (
    .A(_097_),
    .X(_137_)
  );


  sky130_fd_sc_hs__buf_4
  _316_
  (
    .A(counter_out[62]),
    .X(_138_)
  );


  sky130_fd_sc_hs__a21o_4
  _317_
  (
    .A1(_135_),
    .A2(_134_),
    .B1(_138_),
    .X(_139_)
  );


  sky130_fd_sc_hs__nand3_4
  _318_
  (
    .A(_135_),
    .B(_138_),
    .C(_134_),
    .Y(_140_)
  );


  sky130_fd_sc_hs__and3_4
  _319_
  (
    .A(_137_),
    .B(_139_),
    .C(_140_),
    .X(_058_)
  );


  sky130_fd_sc_hs__a41o_4
  _320_
  (
    .A1(counter_out[60]),
    .A2(_135_),
    .A3(_138_),
    .A4(_131_),
    .B1(counter_out[63]),
    .X(_141_)
  );


  sky130_fd_sc_hs__nand4_4
  _321_
  (
    .A(_135_),
    .B(_138_),
    .C(counter_out[63]),
    .D(_134_),
    .Y(_142_)
  );


  sky130_fd_sc_hs__and3_4
  _322_
  (
    .A(_137_),
    .B(_141_),
    .C(_142_),
    .X(_059_)
  );


  sky130_fd_sc_hs__nor2_4
  _323_
  (
    .A(_100_),
    .B(_070_),
    .Y(_000_)
  );


  sky130_fd_sc_hs__and2_4
  _324_
  (
    .A(_070_),
    .B(counter_out[1]),
    .X(_143_)
  );


  sky130_fd_sc_hs__nor2_4
  _325_
  (
    .A(_095_),
    .B(_143_),
    .Y(_144_)
  );


  sky130_fd_sc_hs__o21a_4
  _326_
  (
    .A1(_070_),
    .A2(counter_out[1]),
    .B1(_144_),
    .X(_011_)
  );


  sky130_fd_sc_hs__and2_4
  _327_
  (
    .A(counter_out[2]),
    .B(_143_),
    .X(_145_)
  );


  sky130_fd_sc_hs__o21ai_4
  _328_
  (
    .A1(counter_out[2]),
    .A2(_143_),
    .B1(_137_),
    .Y(_146_)
  );


  sky130_fd_sc_hs__nor2_4
  _329_
  (
    .A(_145_),
    .B(_146_),
    .Y(_022_)
  );


  sky130_fd_sc_hs__nor2_4
  _330_
  (
    .A(_095_),
    .B(_071_),
    .Y(_147_)
  );


  sky130_fd_sc_hs__o21a_4
  _331_
  (
    .A1(counter_out[3]),
    .A2(_145_),
    .B1(_147_),
    .X(_033_)
  );


  sky130_fd_sc_hs__and2_4
  _332_
  (
    .A(counter_out[4]),
    .B(_071_),
    .X(_148_)
  );


  sky130_fd_sc_hs__o21ai_4
  _333_
  (
    .A1(counter_out[4]),
    .A2(_071_),
    .B1(_137_),
    .Y(_149_)
  );


  sky130_fd_sc_hs__nor2_4
  _334_
  (
    .A(_148_),
    .B(_149_),
    .Y(_044_)
  );


  sky130_fd_sc_hs__and2_4
  _335_
  (
    .A(counter_out[5]),
    .B(_148_),
    .X(_150_)
  );


  sky130_fd_sc_hs__o21ai_4
  _336_
  (
    .A1(counter_out[5]),
    .A2(_148_),
    .B1(_137_),
    .Y(_151_)
  );


  sky130_fd_sc_hs__nor2_4
  _337_
  (
    .A(_150_),
    .B(_151_),
    .Y(_055_)
  );


  sky130_fd_sc_hs__nor2_4
  _338_
  (
    .A(_095_),
    .B(_072_),
    .Y(_152_)
  );


  sky130_fd_sc_hs__o21a_4
  _339_
  (
    .A1(counter_out[6]),
    .A2(_150_),
    .B1(_152_),
    .X(_060_)
  );


  sky130_fd_sc_hs__and2_4
  _340_
  (
    .A(counter_out[7]),
    .B(_072_),
    .X(_153_)
  );


  sky130_fd_sc_hs__o21ai_4
  _341_
  (
    .A1(counter_out[7]),
    .A2(_072_),
    .B1(_137_),
    .Y(_154_)
  );


  sky130_fd_sc_hs__nor2_4
  _342_
  (
    .A(_153_),
    .B(_154_),
    .Y(_061_)
  );


  sky130_fd_sc_hs__and2_4
  _343_
  (
    .A(counter_out[8]),
    .B(_153_),
    .X(_155_)
  );


  sky130_fd_sc_hs__o21ai_4
  _344_
  (
    .A1(counter_out[8]),
    .A2(_153_),
    .B1(_137_),
    .Y(_156_)
  );


  sky130_fd_sc_hs__nor2_4
  _345_
  (
    .A(_155_),
    .B(_156_),
    .Y(_062_)
  );


  sky130_fd_sc_hs__nor2_4
  _346_
  (
    .A(_095_),
    .B(_073_),
    .Y(_157_)
  );


  sky130_fd_sc_hs__o21a_4
  _347_
  (
    .A1(counter_out[9]),
    .A2(_155_),
    .B1(_157_),
    .X(_063_)
  );


  sky130_fd_sc_hs__and2_4
  _348_
  (
    .A(counter_out[10]),
    .B(_073_),
    .X(_158_)
  );


  sky130_fd_sc_hs__buf_1
  _349_
  (
    .A(_158_),
    .X(_159_)
  );


  sky130_fd_sc_hs__nor2_4
  _350_
  (
    .A(_095_),
    .B(_159_),
    .Y(_160_)
  );


  sky130_fd_sc_hs__o21a_4
  _351_
  (
    .A1(counter_out[10]),
    .A2(_073_),
    .B1(_160_),
    .X(_001_)
  );


  sky130_fd_sc_hs__o21ai_4
  _352_
  (
    .A1(_074_),
    .A2(_159_),
    .B1(_098_),
    .Y(_161_)
  );


  sky130_fd_sc_hs__a21oi_4
  _353_
  (
    .A1(_074_),
    .A2(_159_),
    .B1(_161_),
    .Y(_002_)
  );


  sky130_fd_sc_hs__a21oi_4
  _354_
  (
    .A1(_074_),
    .A2(_159_),
    .B1(counter_out[12]),
    .Y(_162_)
  );


  sky130_fd_sc_hs__and3_4
  _355_
  (
    .A(counter_out[12]),
    .B(_074_),
    .C(_159_),
    .X(_163_)
  );


  sky130_fd_sc_hs__nor3_4
  _356_
  (
    .A(_100_),
    .B(_162_),
    .C(_163_),
    .Y(_003_)
  );


  sky130_fd_sc_hs__o21ai_4
  _357_
  (
    .A1(counter_out[13]),
    .A2(_163_),
    .B1(_103_),
    .Y(_164_)
  );


  sky130_fd_sc_hs__and2_4
  _358_
  (
    .A(counter_out[13]),
    .B(_163_),
    .X(_165_)
  );


  sky130_fd_sc_hs__nor2_4
  _359_
  (
    .A(_164_),
    .B(_165_),
    .Y(_004_)
  );


  sky130_fd_sc_hs__o21ai_4
  _360_
  (
    .A1(_075_),
    .A2(_165_),
    .B1(_098_),
    .Y(_166_)
  );


  sky130_fd_sc_hs__a21oi_4
  _361_
  (
    .A1(_075_),
    .A2(_165_),
    .B1(_166_),
    .Y(_005_)
  );


  sky130_fd_sc_hs__and2_4
  _362_
  (
    .A(_159_),
    .B(_077_),
    .X(_167_)
  );


  sky130_fd_sc_hs__a21oi_4
  _363_
  (
    .A1(_076_),
    .A2(_163_),
    .B1(counter_out[15]),
    .Y(_168_)
  );


  sky130_fd_sc_hs__nor3_4
  _364_
  (
    .A(_100_),
    .B(_167_),
    .C(_168_),
    .Y(_006_)
  );


  sky130_fd_sc_hs__and2_4
  _365_
  (
    .A(counter_out[16]),
    .B(_167_),
    .X(_169_)
  );


  sky130_fd_sc_hs__buf_1
  _366_
  (
    .A(_169_),
    .X(_170_)
  );


  sky130_fd_sc_hs__o21ai_4
  _367_
  (
    .A1(counter_out[16]),
    .A2(_167_),
    .B1(_137_),
    .Y(_171_)
  );


  sky130_fd_sc_hs__nor2_4
  _368_
  (
    .A(_170_),
    .B(_171_),
    .Y(_007_)
  );


  sky130_fd_sc_hs__o21ai_4
  _369_
  (
    .A1(_085_),
    .A2(_170_),
    .B1(_098_),
    .Y(_172_)
  );


  sky130_fd_sc_hs__a21oi_4
  _370_
  (
    .A1(_085_),
    .A2(_170_),
    .B1(_172_),
    .Y(_008_)
  );


  sky130_fd_sc_hs__a21oi_4
  _371_
  (
    .A1(_085_),
    .A2(_170_),
    .B1(counter_out[18]),
    .Y(_173_)
  );


  sky130_fd_sc_hs__and3_4
  _372_
  (
    .A(counter_out[18]),
    .B(_085_),
    .C(_170_),
    .X(_174_)
  );


  sky130_fd_sc_hs__nor3_4
  _373_
  (
    .A(_100_),
    .B(_173_),
    .C(_174_),
    .Y(_009_)
  );


  sky130_fd_sc_hs__o21ai_4
  _374_
  (
    .A1(_084_),
    .A2(_174_),
    .B1(_103_),
    .Y(_175_)
  );


  sky130_fd_sc_hs__and4_4
  _375_
  (
    .A(counter_out[18]),
    .B(_084_),
    .C(_085_),
    .D(_170_),
    .X(_176_)
  );


  sky130_fd_sc_hs__nor2_4
  _376_
  (
    .A(_175_),
    .B(_176_),
    .Y(_010_)
  );


  sky130_fd_sc_hs__buf_1
  _377_
  (
    .A(_097_),
    .X(_177_)
  );


  sky130_fd_sc_hs__o21ai_4
  _378_
  (
    .A1(_079_),
    .A2(_176_),
    .B1(_177_),
    .Y(_178_)
  );


  sky130_fd_sc_hs__and2_4
  _379_
  (
    .A(_079_),
    .B(_176_),
    .X(_179_)
  );


  sky130_fd_sc_hs__nor2_4
  _380_
  (
    .A(_178_),
    .B(_179_),
    .Y(_012_)
  );


  sky130_fd_sc_hs__o21ai_4
  _381_
  (
    .A1(_080_),
    .A2(_179_),
    .B1(_177_),
    .Y(_180_)
  );


  sky130_fd_sc_hs__and4_4
  _382_
  (
    .A(_079_),
    .B(_080_),
    .C(_167_),
    .D(_086_),
    .X(_181_)
  );


  sky130_fd_sc_hs__nor2_4
  _383_
  (
    .A(_180_),
    .B(_181_),
    .Y(_013_)
  );


  sky130_fd_sc_hs__o21ai_4
  _384_
  (
    .A1(_078_),
    .A2(_181_),
    .B1(_177_),
    .Y(_182_)
  );


  sky130_fd_sc_hs__and2_4
  _385_
  (
    .A(_078_),
    .B(_181_),
    .X(_183_)
  );


  sky130_fd_sc_hs__nor2_4
  _386_
  (
    .A(_182_),
    .B(_183_),
    .Y(_014_)
  );


  sky130_fd_sc_hs__buf_1
  _387_
  (
    .A(_095_),
    .X(_184_)
  );


  sky130_fd_sc_hs__nor2_4
  _388_
  (
    .A(counter_out[23]),
    .B(_183_),
    .Y(_185_)
  );


  sky130_fd_sc_hs__and2_4
  _389_
  (
    .A(counter_out[23]),
    .B(_183_),
    .X(_186_)
  );


  sky130_fd_sc_hs__nor3_4
  _390_
  (
    .A(_184_),
    .B(_185_),
    .C(_186_),
    .Y(_015_)
  );


  sky130_fd_sc_hs__o21ai_4
  _391_
  (
    .A1(counter_out[24]),
    .A2(_186_),
    .B1(_177_),
    .Y(_187_)
  );


  sky130_fd_sc_hs__and3_4
  _392_
  (
    .A(_078_),
    .B(counter_out[23]),
    .C(_080_),
    .X(_188_)
  );


  sky130_fd_sc_hs__and2_4
  _393_
  (
    .A(_084_),
    .B(_174_),
    .X(_189_)
  );


  sky130_fd_sc_hs__and4_4
  _394_
  (
    .A(counter_out[24]),
    .B(_079_),
    .C(_188_),
    .D(_189_),
    .X(_190_)
  );


  sky130_fd_sc_hs__nor2_4
  _395_
  (
    .A(_187_),
    .B(_190_),
    .Y(_016_)
  );


  sky130_fd_sc_hs__or2_4
  _396_
  (
    .A(_082_),
    .B(_190_),
    .X(_191_)
  );


  sky130_fd_sc_hs__nand2_4
  _397_
  (
    .A(_082_),
    .B(_190_),
    .Y(_192_)
  );


  sky130_fd_sc_hs__and3_4
  _398_
  (
    .A(_137_),
    .B(_191_),
    .C(_192_),
    .X(_017_)
  );


  sky130_fd_sc_hs__a21oi_4
  _399_
  (
    .A1(_082_),
    .A2(_190_),
    .B1(counter_out[26]),
    .Y(_193_)
  );


  sky130_fd_sc_hs__and3_4
  _400_
  (
    .A(counter_out[26]),
    .B(_082_),
    .C(_190_),
    .X(_194_)
  );


  sky130_fd_sc_hs__nor3_4
  _401_
  (
    .A(_184_),
    .B(_193_),
    .C(_194_),
    .Y(_018_)
  );


  sky130_fd_sc_hs__o21ai_4
  _402_
  (
    .A1(counter_out[27]),
    .A2(_194_),
    .B1(_177_),
    .Y(_195_)
  );


  sky130_fd_sc_hs__and2_4
  _403_
  (
    .A(counter_out[27]),
    .B(_194_),
    .X(_196_)
  );


  sky130_fd_sc_hs__nor2_4
  _404_
  (
    .A(_195_),
    .B(_196_),
    .Y(_019_)
  );


  sky130_fd_sc_hs__o21ai_4
  _405_
  (
    .A1(counter_out[28]),
    .A2(_196_),
    .B1(_177_),
    .Y(_197_)
  );


  sky130_fd_sc_hs__and3_4
  _406_
  (
    .A(counter_out[28]),
    .B(_083_),
    .C(_186_),
    .X(_198_)
  );


  sky130_fd_sc_hs__nor2_4
  _407_
  (
    .A(_197_),
    .B(_198_),
    .Y(_020_)
  );


  sky130_fd_sc_hs__o21ai_4
  _408_
  (
    .A1(counter_out[29]),
    .A2(_198_),
    .B1(_177_),
    .Y(_199_)
  );


  sky130_fd_sc_hs__and2_4
  _409_
  (
    .A(counter_out[29]),
    .B(_198_),
    .X(_200_)
  );


  sky130_fd_sc_hs__nor2_4
  _410_
  (
    .A(_199_),
    .B(_200_),
    .Y(_021_)
  );


  sky130_fd_sc_hs__o21ai_4
  _411_
  (
    .A1(_087_),
    .A2(_200_),
    .B1(_098_),
    .Y(_201_)
  );


  sky130_fd_sc_hs__a21oi_4
  _412_
  (
    .A1(_087_),
    .A2(_200_),
    .B1(_201_),
    .Y(_023_)
  );


  sky130_fd_sc_hs__a21oi_4
  _413_
  (
    .A1(_087_),
    .A2(_200_),
    .B1(counter_out[31]),
    .Y(_202_)
  );


  sky130_fd_sc_hs__nor3_4
  _414_
  (
    .A(_184_),
    .B(_090_),
    .C(_202_),
    .Y(_024_)
  );


  sky130_fd_sc_hs__and2_4
  _415_
  (
    .A(counter_out[32]),
    .B(_090_),
    .X(_203_)
  );


  sky130_fd_sc_hs__nor2_4
  _416_
  (
    .A(counter_out[32]),
    .B(_090_),
    .Y(_204_)
  );


  sky130_fd_sc_hs__nor3_4
  _417_
  (
    .A(_184_),
    .B(_203_),
    .C(_204_),
    .Y(_025_)
  );


  sky130_fd_sc_hs__o21ai_4
  _418_
  (
    .A1(counter_out[33]),
    .A2(_203_),
    .B1(_177_),
    .Y(_205_)
  );


  sky130_fd_sc_hs__and2_4
  _419_
  (
    .A(counter_out[33]),
    .B(_203_),
    .X(_206_)
  );


  sky130_fd_sc_hs__nor2_4
  _420_
  (
    .A(_205_),
    .B(_206_),
    .Y(_026_)
  );


  sky130_fd_sc_hs__o21ai_4
  _421_
  (
    .A1(_068_),
    .A2(_206_),
    .B1(_097_),
    .Y(_207_)
  );


  sky130_fd_sc_hs__a21oi_4
  _422_
  (
    .A1(_068_),
    .A2(_206_),
    .B1(_207_),
    .Y(_027_)
  );


  sky130_fd_sc_hs__and2_4
  _423_
  (
    .A(_069_),
    .B(_203_),
    .X(_208_)
  );


  sky130_fd_sc_hs__a21oi_4
  _424_
  (
    .A1(_068_),
    .A2(_206_),
    .B1(counter_out[35]),
    .Y(_209_)
  );


  sky130_fd_sc_hs__nor3_4
  _425_
  (
    .A(_184_),
    .B(_208_),
    .C(_209_),
    .Y(_028_)
  );


  sky130_fd_sc_hs__o21ai_4
  _426_
  (
    .A1(counter_out[36]),
    .A2(_208_),
    .B1(_177_),
    .Y(_210_)
  );


  sky130_fd_sc_hs__and2_4
  _427_
  (
    .A(counter_out[36]),
    .B(_208_),
    .X(_211_)
  );


  sky130_fd_sc_hs__nor2_4
  _428_
  (
    .A(_210_),
    .B(_211_),
    .Y(_029_)
  );


  sky130_fd_sc_hs__o21ai_4
  _429_
  (
    .A1(counter_out[37]),
    .A2(_211_),
    .B1(_177_),
    .Y(_212_)
  );


  sky130_fd_sc_hs__and2_4
  _430_
  (
    .A(counter_out[37]),
    .B(_211_),
    .X(_213_)
  );


  sky130_fd_sc_hs__nor2_4
  _431_
  (
    .A(_212_),
    .B(_213_),
    .Y(_030_)
  );


  sky130_fd_sc_hs__o21ai_4
  _432_
  (
    .A1(_066_),
    .A2(_213_),
    .B1(_097_),
    .Y(_214_)
  );


  sky130_fd_sc_hs__a21oi_4
  _433_
  (
    .A1(_066_),
    .A2(_213_),
    .B1(_214_),
    .Y(_031_)
  );


  sky130_fd_sc_hs__a21oi_4
  _434_
  (
    .A1(_066_),
    .A2(_213_),
    .B1(counter_out[39]),
    .Y(_215_)
  );


  sky130_fd_sc_hs__nor3_4
  _435_
  (
    .A(_184_),
    .B(_091_),
    .C(_215_),
    .Y(_032_)
  );


  sky130_fd_sc_hs__nand2_4
  _436_
  (
    .A(_065_),
    .B(_091_),
    .Y(_216_)
  );


  sky130_fd_sc_hs__or2_4
  _437_
  (
    .A(_065_),
    .B(_091_),
    .X(_217_)
  );


  sky130_fd_sc_hs__and3_4
  _438_
  (
    .A(_137_),
    .B(_216_),
    .C(_217_),
    .X(_034_)
  );


  sky130_fd_sc_hs__and3_4
  _439_
  (
    .A(_065_),
    .B(counter_out[41]),
    .C(_091_),
    .X(_218_)
  );


  sky130_fd_sc_hs__a21oi_4
  _440_
  (
    .A1(_065_),
    .A2(_091_),
    .B1(counter_out[41]),
    .Y(_219_)
  );


  sky130_fd_sc_hs__nor3_4
  _441_
  (
    .A(_184_),
    .B(_218_),
    .C(_219_),
    .Y(_035_)
  );


  sky130_fd_sc_hs__o21ai_4
  _442_
  (
    .A1(counter_out[42]),
    .A2(_218_),
    .B1(_103_),
    .Y(_220_)
  );


  sky130_fd_sc_hs__nor2_4
  _443_
  (
    .A(_092_),
    .B(_220_),
    .Y(_036_)
  );


  sky130_fd_sc_hs__and2_4
  _444_
  (
    .A(counter_out[43]),
    .B(_092_),
    .X(_221_)
  );


  sky130_fd_sc_hs__o21ai_4
  _445_
  (
    .A1(counter_out[43]),
    .A2(_092_),
    .B1(_103_),
    .Y(_222_)
  );


  sky130_fd_sc_hs__nor2_4
  _446_
  (
    .A(_221_),
    .B(_222_),
    .Y(_037_)
  );


  sky130_fd_sc_hs__and2_4
  _447_
  (
    .A(counter_out[44]),
    .B(_221_),
    .X(_223_)
  );


  sky130_fd_sc_hs__o21ai_4
  _448_
  (
    .A1(counter_out[44]),
    .A2(_221_),
    .B1(_103_),
    .Y(_224_)
  );


  sky130_fd_sc_hs__nor2_4
  _449_
  (
    .A(_223_),
    .B(_224_),
    .Y(_038_)
  );


  sky130_fd_sc_hs__nor2_4
  _450_
  (
    .A(counter_out[45]),
    .B(_223_),
    .Y(_225_)
  );


  sky130_fd_sc_hs__nor3_4
  _451_
  (
    .A(_184_),
    .B(_094_),
    .C(_225_),
    .Y(_039_)
  );


  sky130_fd_sc_hs__dfxtp_1
  _452_
  (
    .D(_000_),
    .Q(counter_out[0]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _453_
  (
    .D(_011_),
    .Q(counter_out[1]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _454_
  (
    .D(_022_),
    .Q(counter_out[2]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _455_
  (
    .D(_033_),
    .Q(counter_out[3]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _456_
  (
    .D(_044_),
    .Q(counter_out[4]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _457_
  (
    .D(_055_),
    .Q(counter_out[5]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _458_
  (
    .D(_060_),
    .Q(counter_out[6]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _459_
  (
    .D(_061_),
    .Q(counter_out[7]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _460_
  (
    .D(_062_),
    .Q(counter_out[8]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _461_
  (
    .D(_063_),
    .Q(counter_out[9]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _462_
  (
    .D(_001_),
    .Q(counter_out[10]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _463_
  (
    .D(_002_),
    .Q(counter_out[11]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _464_
  (
    .D(_003_),
    .Q(counter_out[12]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _465_
  (
    .D(_004_),
    .Q(counter_out[13]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _466_
  (
    .D(_005_),
    .Q(counter_out[14]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _467_
  (
    .D(_006_),
    .Q(counter_out[15]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _468_
  (
    .D(_007_),
    .Q(counter_out[16]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _469_
  (
    .D(_008_),
    .Q(counter_out[17]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _470_
  (
    .D(_009_),
    .Q(counter_out[18]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _471_
  (
    .D(_010_),
    .Q(counter_out[19]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _472_
  (
    .D(_012_),
    .Q(counter_out[20]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _473_
  (
    .D(_013_),
    .Q(counter_out[21]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _474_
  (
    .D(_014_),
    .Q(counter_out[22]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _475_
  (
    .D(_015_),
    .Q(counter_out[23]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _476_
  (
    .D(_016_),
    .Q(counter_out[24]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _477_
  (
    .D(_017_),
    .Q(counter_out[25]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _478_
  (
    .D(_018_),
    .Q(counter_out[26]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _479_
  (
    .D(_019_),
    .Q(counter_out[27]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _480_
  (
    .D(_020_),
    .Q(counter_out[28]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _481_
  (
    .D(_021_),
    .Q(counter_out[29]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _482_
  (
    .D(_023_),
    .Q(counter_out[30]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _483_
  (
    .D(_024_),
    .Q(counter_out[31]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _484_
  (
    .D(_025_),
    .Q(counter_out[32]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _485_
  (
    .D(_026_),
    .Q(counter_out[33]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _486_
  (
    .D(_027_),
    .Q(counter_out[34]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _487_
  (
    .D(_028_),
    .Q(counter_out[35]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _488_
  (
    .D(_029_),
    .Q(counter_out[36]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _489_
  (
    .D(_030_),
    .Q(counter_out[37]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _490_
  (
    .D(_031_),
    .Q(counter_out[38]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _491_
  (
    .D(_032_),
    .Q(counter_out[39]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _492_
  (
    .D(_034_),
    .Q(counter_out[40]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _493_
  (
    .D(_035_),
    .Q(counter_out[41]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _494_
  (
    .D(_036_),
    .Q(counter_out[42]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _495_
  (
    .D(_037_),
    .Q(counter_out[43]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _496_
  (
    .D(_038_),
    .Q(counter_out[44]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _497_
  (
    .D(_039_),
    .Q(counter_out[45]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _498_
  (
    .D(_040_),
    .Q(counter_out[46]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _499_
  (
    .D(_041_),
    .Q(counter_out[47]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _500_
  (
    .D(_042_),
    .Q(counter_out[48]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _501_
  (
    .D(_043_),
    .Q(counter_out[49]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _502_
  (
    .D(_045_),
    .Q(counter_out[50]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _503_
  (
    .D(_046_),
    .Q(counter_out[51]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _504_
  (
    .D(_047_),
    .Q(counter_out[52]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _505_
  (
    .D(_048_),
    .Q(counter_out[53]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _507_
  (
    .D(_050_),
    .Q(counter_out[55]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _508_
  (
    .D(_051_),
    .Q(counter_out[56]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _509_
  (
    .D(_052_),
    .Q(counter_out[57]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _510_
  (
    .D(_053_),
    .Q(counter_out[58]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _511_
  (
    .D(_054_),
    .Q(counter_out[59]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _512_
  (
    .D(_056_),
    .Q(counter_out[60]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _513_
  (
    .D(_057_),
    .Q(counter_out[61]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _514_
  (
    .D(_058_),
    .Q(counter_out[62]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _515_
  (
    .D(_059_),
    .Q(counter_out[63]),
    .CLK(clk)
  );


  sky130_fd_sc_hs__and4_4
  _234__harden0
  (
    .A(counter_out[6]),
    .B(counter_out[4]),
    .C(counter_out[5]),
    .D(_071_),
    .X(_072__harden0)
  );


  sky130_fd_sc_hs__and4_4
  _234__harden1
  (
    .A(counter_out[6]),
    .B(counter_out[4]),
    .C(counter_out[5]),
    .D(_071_),
    .X(_072__harden1)
  );


  sky130_fd_sc_hs__and4_4
  _234__harden2
  (
    .A(counter_out[6]),
    .B(counter_out[4]),
    .C(counter_out[5]),
    .D(_071_),
    .X(_072__harden2)
  );


  sky130_fd_sc_hs__nand2_1
  _072__hardenA_nand
  (
    .A(_072__harden0),
    .B(_072__harden1),
    .Y(_072__hardenA)
  );


  sky130_fd_sc_hs__nand2_1
  _072__hardenB_nand
  (
    .A(_072__harden0),
    .B(_072__harden2),
    .Y(_072__hardenB)
  );


  sky130_fd_sc_hs__nand2_1
  _072__hardenC_nand
  (
    .A(_072__harden1),
    .B(_072__harden2),
    .Y(_072__hardenC)
  );


  sky130_fd_sc_hs__nand3_1
  _072__hardenABC
  (
    .A(_072__hardenA),
    .B(_072__hardenB),
    .C(_072__hardenC),
    .Y(_072_)
  );


  sky130_fd_sc_hs__dfxtp_1
  _506__harden0
  (
    .D(_049_),
    .Q(counter_out_54__harden0),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _506__harden1
  (
    .D(_049_),
    .Q(counter_out_54__harden1),
    .CLK(clk)
  );


  sky130_fd_sc_hs__dfxtp_1
  _506__harden2
  (
    .D(_049_),
    .Q(counter_out_54__harden2),
    .CLK(clk)
  );


  sky130_fd_sc_hs__nand2_1
  counter_out_54__hardenA_nand
  (
    .A(counter_out_54__harden0),
    .B(counter_out_54__harden1),
    .Y(counter_out_54__hardenA)
  );


  sky130_fd_sc_hs__nand2_1
  counter_out_54__hardenB_nand
  (
    .A(counter_out_54__harden0),
    .B(counter_out_54__harden2),
    .Y(counter_out_54__hardenB)
  );


  sky130_fd_sc_hs__nand2_1
  counter_out_54__hardenC_nand
  (
    .A(counter_out_54__harden1),
    .B(counter_out_54__harden2),
    .Y(counter_out_54__hardenC)
  );


  sky130_fd_sc_hs__nand3_1
  counter_out_54__hardenABC
  (
    .A(counter_out_54__hardenA),
    .B(counter_out_54__hardenB),
    .C(counter_out_54__hardenC),
    .Y(counter_out[54])
  );


endmodule

