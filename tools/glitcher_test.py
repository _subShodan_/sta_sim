#!/usr/bin/python3

import unittest
import functools
import IPython
import glitcher
import tempfile
import numpy as np
import os
import yappi
import pickle

class TestGlitcher(unittest.TestCase):

    def test_generate_glitches_perf(self):
        # Setup 
        glitcher.parseoptions(['keep', 'cmdline', 'happy'])

        numpins = 1600 # Roughly like picorv32
        pinlist = [x for x in range(numpins)] # Just add all
        machines = 256
        cpus = 1000
        start = 1900
        width = 1
        clocks = 2500
        seed = 0
        prob = 0.1


        gen = glitcher.RandomGlitchGenerator(10, seed, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        # Profile the function
        yappi.start()
        for _ in range(cpus):
            glitches = gen._generate_glitches()
        yappi.stop()

        # Dump results
        stats = yappi.get_func_stats()
        stats.sort('tsub') # Sort by time spent in individual function, excluding subcalls
        stats.print_all()
        return


    def choice(self, a):
        return a[0]

    def integers(self, low, high, length, nptype):
        return np.zeros(length, dtype=nptype)

    def test_generate_glitches(self):
        # Setup 
        glitcher.parseoptions(['keep', 'cmdline', 'happy'])

        numpins = 1603 # Roughly like picorv32
        pinlist = [8*0+0, 8*1+0, 8*2+1]
        machines = 256
        cpus = 40
        bytes_per_machine = (numpins+7)//8
        start = 1900
        width = 1
        clocks = 2500
        version = 0

        # We override the rng because we want to fix rng.integers, see above. This way all pins are always selected
        prob = 1.0
        gen = glitcher.RandomGlitchGenerator(10, 0, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        #gen.rng = self # Override rng
        glitches = gen._generate_glitches()
        self.assertEqual(len(glitches), machines * bytes_per_machine)

        # Check bit order in all VMs
        for vm in range(machines):
            vmi = vm * bytes_per_machine
            self.assertEqual(glitches[vmi+0], 1, vmi)
            self.assertEqual(glitches[vmi+1], 1, vmi)
            self.assertEqual(glitches[vmi+2], 2, vmi)
            for b in range(3, bytes_per_machine):
                self.assertEqual(glitches[vmi+b], 0, vmi)

        # Since all pins are selected, we should see them in the decode
        decodedpins = glitcher.GlitchGenerator.decodepins(glitches[:bytes_per_machine])
        truth = np.array([False] * (bytes_per_machine*8))
        truth[pinlist] = True
        self.assertEqual(list(decodedpins), list(truth))

        # Test all bits are hit
        prob = 0.1
        pinlist = list(range(numpins))
        gen = glitcher.RandomGlitchGenerator(10, 0, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        glitches = gen._generate_glitches()
        bitmask = functools.reduce((lambda x, y: x | y), glitches)
        self.assertEqual(bitmask, 0xff)

        # Test no bits are hit
        prob = 0
        gen = glitcher.RandomGlitchGenerator(10, 0, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        glitches = gen._generate_glitches()
        self.assertEqual(glitches, b'\00'*len(glitches)) # Should be all 0


    def test_bruteforce_pins(self):
        glitcher.parseoptions(['keep', 'cmdline', 'happy'])

        start = 1900
        width = 1
        clocks = 2500
        numpins = 1600
        machines = 256
        extra = 10
        pinlist = range(machines+extra)
        seed = 0

        gen = glitcher.BruteforcePinGenerator(numpins, pinlist, start, width, clocks, machines, 0)
        gp = gen.generate()
        bytes_per_machine = (numpins+7)//8

        self.assertEqual(len(gp.code), machines * bytes_per_machine)

        # Check bit order in all VMs
        for vm in range(machines):
            vmi = vm * bytes_per_machine
            decodedpins = glitcher.GlitchGenerator.decodepins(gp.code[vmi:(vmi+bytes_per_machine)])
            truth = [False] * (bytes_per_machine*8)
            truth[vm] = True
            np.testing.assert_array_equal(decodedpins, truth)

        # Generate the next 256 bits
        gp = gen.generate()
        self.assertEqual(len(gp.code), machines * bytes_per_machine)
        for vm in range(machines):
            vmi = vm * bytes_per_machine
            decodedpins = glitcher.GlitchGenerator.decodepins(gp.code[vmi:(vmi+bytes_per_machine)])
            truth = [False] * (bytes_per_machine*8)
            if vm < extra:
                truth[vm + machines] = True # We're in the next generate(), so add machine count
            if list(decodedpins) != list(truth):
                IPython.embed()
            np.testing.assert_array_equal(decodedpins, truth)


    def test_rng_stable(self):
        start = 1900
        width = 1
        clocks = 2500
        numpins = 1600
        pinlist = list(range(numpins))
        machines = 256
        prob = 0.1

        seed = 0
        gen = glitcher.RandomGlitchGenerator(10, seed, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        gp0 = gen.generate()

        seed = 1
        gen = glitcher.RandomGlitchGenerator(10, seed, numpins, pinlist, start, start+200, width, clocks, machines, 0, prob)
        gp1 = gen.generate()

        self.assertNotEqual(gp0, gp1)
        self.assertTrue(gp0.code.startswith(b'\x04\x04\x88\x08\x00\x00\x00\x00\x02A\x00\x08\x00'))
    
    def test_terminate(self):
        count = 1
        start = 1900
        end = 1903
        width = 1
        clocks = 2500
        numpins = 1600
        machines = 256
        pinlist = range(machines*2 + 1)
        seed = 0
        prob = 0.1

        # Random
        gen = glitcher.RandomGlitchGenerator(count, seed, numpins, pinlist, start, end, width, clocks, machines, 0, prob)

        gp = gen.generate()
        self.assertIsNotNone(gp)
        gp = gen.generate()
        self.assertIsNone(gp)

        # Bruteforce
        gen = glitcher.BruteforcePinGenerator(numpins, pinlist, start, width, clocks, machines, 0)
        for _ in range(3): # Need 3, as pinlist doesn't fit in 2
            gp = gen.generate()
            self.assertIsNotNone(gp)
        gp = gen.generate()
        self.assertIsNone(gp)

    def test_lookup_inputs(self):
        with open("instrument_test_out_all.map", "rb") as gf:
            gm = pickle.load(gf)
        inputs = glitcher.lookup_inputs("_229_", gm)
        self.assertEqual(inputs, [0, 1, 2, 3]) # From instrument_test_out_all.v
        inputs = glitcher.lookup_inputs("_515_", gm)
        self.assertEqual(inputs, [592]) # From instrument_test_out_all.v

    def test_unpack_array_blob(self):
        x = b'abcd'
        self.assertEqual(glitcher.unpack_array_blob(x, 2, 4), [b'ab', b'cd'])
        self.assertEqual(glitcher.unpack_array_blob(x, 1, 4), [b'a', b'b', b'c', b'd'])
        x = b''
        self.assertEqual(glitcher.unpack_array_blob(x, 0, 4), [b'', b'', b'', b''])
        

if __name__ == '__main__':
    unittest.main()
