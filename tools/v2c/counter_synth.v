module counter (clk,
    rst,
    counter_out);
 input clk;
 input rst;
 output [3:0] counter_out;

 sky130_fd_sc_hs__buf_8 _12_ (.A(counter_out[0]),
    .X(_04_));
 sky130_fd_sc_hs__buf_4 _13_ (.A(rst),
    .X(_05_));
 sky130_fd_sc_hs__nor2_4 _14_ (.A(_04_),
    .B(_05_),
    .Y(_00_));
 sky130_fd_sc_hs__buf_4 _15_ (.A(counter_out[1]),
    .X(_06_));
 sky130_fd_sc_hs__a21oi_4 _16_ (.A1(_04_),
    .A2(_06_),
    .B1(_05_),
    .Y(_07_));
 sky130_fd_sc_hs__o21a_4 _17_ (.A1(_04_),
    .A2(_06_),
    .B1(_07_),
    .X(_01_));
 sky130_fd_sc_hs__buf_4 _18_ (.A(counter_out[2]),
    .X(_08_));
 sky130_fd_sc_hs__and3_4 _19_ (.A(_04_),
    .B(_06_),
    .C(_08_),
    .X(_09_));
 sky130_fd_sc_hs__a21oi_4 _20_ (.A1(_04_),
    .A2(_06_),
    .B1(_08_),
    .Y(_10_));
 sky130_fd_sc_hs__nor3_4 _21_ (.A(_05_),
    .B(_09_),
    .C(_10_),
    .Y(_02_));
 sky130_fd_sc_hs__a41oi_4 _22_ (.A1(_04_),
    .A2(_06_),
    .A3(_08_),
    .A4(counter_out[3]),
    .B1(_05_),
    .Y(_11_));
 sky130_fd_sc_hs__o21a_4 _23_ (.A1(counter_out[3]),
    .A2(_09_),
    .B1(_11_),
    .X(_03_));
 sky130_fd_sc_hs__dfxtp_1 _24_ (.D(_00_),
    .Q(counter_out[0]),
    .CLK(clk));
 sky130_fd_sc_hs__dfxtp_1 _25_ (.D(_01_),
    .Q(counter_out[1]),
    .CLK(clk));
 sky130_fd_sc_hs__dfxtp_1 _26_ (.D(_02_),
    .Q(counter_out[2]),
    .CLK(clk));
 sky130_fd_sc_hs__dfxtp_1 _27_ (.D(_03_),
    .Q(counter_out[3]),
    .CLK(clk));
endmodule
