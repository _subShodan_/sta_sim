#!/usr/bin/python3

"""
This code takes a Verilog netlist and liberty cell library, and coverts it into a C++ class that emulates N virtual
instances of this netlist by using bit parallel execution. N is currently 64, but could later be extended to make use
of vectorization/SIMD.

The goal is to get some performance numbers out of this simulation. There is NO optimization in this code, it relies
fully on the synthesis tool and C compiler to be smart.

This is pre-alpha. It "works" on some simple examples, but is known to fail on more complex ones. There are also
many hacks in this code, hopefully marked by XXX, where it depends on certain sky130 PDK properties.
"""

from liberty.parser import parse_liberty
from optparse import OptionParser
import pickle
import pyverilog.vparser.parser as vparser
import pyverilog
import IPython
import sys
import itertools
import os


BITS = 64  # Number of machines to use in simulation
TYPE = "uint{}_t".format(BITS)  # Int type for C compiler


def conv(escstr):
    """ convert liberty parser strings to Python str objects """
    if not escstr:
        return None
    return str(escstr).strip('"')


def parse_cmdline():
    """ parse command line """
    usage_string = "Usage: v2c.py [liberty input] [netlist input] [c output]"
    optparser = OptionParser(usage=usage_string)
    optparser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", dest="define", action="append", default=[], help="Macro Definition")
    (options, args) = optparser.parse_args()

    if len(args) != 3:
        print(usage_string)
        sys.exit(1)

    libertyfile = args[0]
    netlistfile = args[1]
    outputfile = args[2]

    return libertyfile, netlistfile, outputfile, options


def libertyfunction2c(fun):
    """ turn cell library liberty function into C function """
    return fun.replace('!', '~')


def get_cell_info(library):
    """ Loop over all cells in library, and gather all info needed to convert netlist to C
    Output is dictionary of cell -> (function, ff_clocked_on, output pin name, [input pin names])
    function comes from liberty file, but is converted to C, e.g. "A|B"
    ff_clocked_on indicates the cell is a ff, and which pin is the clock input. None if not a ff.
    output pin name is just that
    input pin names is just that

    there's lot of hardcoding here and assumptions for sky130. Probably doesn't work on other libraries.
    """
    cell_info = dict()

    # Loop over structure and print output
    for group in library.groups:
        if group.group_name == "cell":
            # Grab cell name
            cell = conv(group.args[0])
            info = (None, None, None, [])  # Eq, clocked_on, output pin, input pins
            ignore = False

            # Grab pins and function
            for subgroup in group.groups:
                if subgroup.group_name == "ff":
                    # Grab flip-flop info
                    ff = subgroup
                    info = (info[0], conv(ff.attributes.get("clocked_on")), info[2], info[3])
                    # Basic sanity checks, probably biased to sky130, to avoid assumptions about ffs being uptrue
                    # Main assumption: they are one-wire-in, one-wire-out and just save the state
                    if len(ff.args) != 2 or ff.args[0] != "IQ" or ff.args[1] != "IQ_N" or len(
                            ff.attributes) != 2 or not ff.attributes.get("clocked_on") \
                            or not ff.attributes.get("next_state"):
                        ignore = "Ignoring, unknown ff config for cell %s" % cell

                if subgroup.group_name == "pin":
                    # For each pin, grab
                    pin = conv(subgroup.args[0])
                    fun = conv(subgroup.attributes.get("function"))
                    direction = conv(subgroup.attributes.get("direction"))

                    # Grab function as equation
                    if fun:
                        eq = libertyfunction2c("{}={}".format(pin, fun))
                        if info[0]:
                            ignore = "Multiple outputs in cell {}, ignoring".format(cell)
                        else:
                            # XXX ZOMG BIG HACK to get LO/HI cell working, which has multiple outputs but HI wasn't used
                            if pin != "HI":
                                info = (eq, info[1], info[2], info[3])

                    # Record input and output pins for dependency graph
                    if direction == "input":
                        if pin.lower() != "clk":  # XXX hardcoded!
                            info[3].append(pin)
                    elif direction == "output":
                        if info[2]:
                            ignore = "Multiple outputs in cell {}, ignoring".format(cell)
                        else:
                            # XXX ZOMG BIG HACK to get LO/HI cell working, which has multiple outputs but HI wasn't used
                            if pin != "HI":
                                info = (info[0], info[1], pin, info[3])
                    else:
                        ignore = "Unknown direction {} for cell {}".format(direction, cell)

            if ignore:
                print(ignore)
            else:
                # HACK! XXX
                if info[1]:  # if it's a flipflop, just hardcode the equation
                    info = ("Q=D", info[1], info[2], info[3])
                cell_info[cell] = info
                print("Cell {} does {} and flipflop={}".format(cell, info[0], info[1]))

    return cell_info


def read_cell_lib(libertyfile):
    """ Read cell library either from cache (picklefile), or parse from liberty file """
    picklefile = libertyfile + ".pickle"
    if os.path.isfile(picklefile):
        print("Loading cells from pickled {}".format(picklefile))
        cell_info = pickle.load(open(picklefile, "rb"))
    else:
        print("Parsing library {}".format(libertyfile))
        library = parse_liberty(open(libertyfile).read())
        cell_info = get_cell_info(library)
        pickle.dump(cell_info, open(picklefile, "wb"))

    return cell_info


def fixname(name):
    """ Fix verilog names to be usable in C """
    if name.startswith('\\'):
        name = name[1:]  # Chop of Verilog escapes
    if name.startswith('_'):
        name = "x" + name
    return name.replace('[', '___idx').replace(']', '')  # XXX dangerous, could lead to name collisions


def idx2name(var, idx):
    """ Create new var name for bit index in wire """
    return "{}___idx{}".format(var, idx)


def getportarg(port):
    """ Converts a verilog port to a C-level variable. Does some fixup to make names C-compatible """
    if type(port.argname) == pyverilog.vparser.ast.Identifier:
        return port.portname, port.argname.name
    elif type(port.argname) == pyverilog.vparser.ast.Pointer:
        newname = idx2name(port.argname.var, port.argname.ptr)
        return port.portname, newname
    else:
        port.show()
        print("Warning, unknown type for port.argname")
        assert False


def parse_verilog_decl(item, outputtree, varrange):
    """ Grab the input/output declarations from verilog, and fill the varrange data structure, as well as outputtree
    """
    assert (len(item.list) == 1)
    var = item.list[0]
    name = fixname(var.name)

    # Multiple bits?
    if var.width:
        lohi = (int(var.width.lsb.value), int(var.width.msb.value))
        assert lohi[0] == 0, "We make some assumptions about lo==0 later"
        names = [idx2name(name, i) for i in range(lohi[0], lohi[1] + 1)]
    else:
        lohi = (0, 0)
        names = [name]

    # For IOs, record them
    if type(var) == pyverilog.vparser.ast.Input or type(var) == pyverilog.vparser.ast.Output:
        for n in names:
            outputtree[n] = (
                [], "", type(var) == pyverilog.vparser.ast.Input)  # Assume inputs are externally set
            varrange[n] = (name, lohi[0], lohi[1], type(var) == pyverilog.vparser.ast.Output)
    elif type(var) == pyverilog.vparser.ast.Wire:  # No need for these
        # ignore
        True
    else:
        # Safety
        assert False


def parse_verilog_assign(item, outputtree, ffs, cell_info):
    """ Grab all verilog assign statements, and build the outputtree as well as ffs structure """
    assert len(item.instances) == 1
    instance = item.instances[0]
    cell = instance.module
    info = cell_info[cell]
    (fun, ff_trigger, output, inputs) = info

    # print("Before: {}".format(fun))
    for port in instance.portlist:
        (portname, argname) = getportarg(port)
        argname = fixname(argname)
        # XXX this may be dangerous if one port is substr of another
        output = output.replace(portname, argname)
        inputs = [i.replace(portname, argname) for i in inputs]
        fun = fun.replace(portname, argname)
        if ff_trigger:
            ff_trigger = ff_trigger.replace(portname, argname)
    # print("After: output {}, inputs {}, eq {}".format(output, inputs, fun))

    outputtree[output] = (inputs, fun, False)  # Inputs, function, already output?
    if ff_trigger:
        # XXX HACK! Only posedge clk supported
        assert ff_trigger == fixname('clk'), "FF should only flop on clk, not on {}".format(ff_trigger)
        ffs[output] = True


def parse_verilog(netlistfile, cell_info, options):
    """ Read a verilog file, and creates:
    outputtree, a dict of output C vars -> [C inputs, the C function, False]
    ffs, a set of all C vars that are a flip-flop (implemented as dict)
    varrange,  all C vars that are defined as IOs to the topmodule -> (basename, range_lo, range_hi, is_output):
        for i/os with a range, e.g. reg [31:0] bla,
            basename=bla, lo=0, hi=31, is_output is whether it's declared as input or output.
            in C there will be a bla___idx0 .. bla___idx31
        for i/os without a range, e.g. reg bla,
            basename=bla, lo=0, hi=0, is_output is whether it's declared as input or output.
            in C there will be a bla
    """
    ast, directives = vparser.parse((netlistfile,), preprocess_include=options.include,
                                    preprocess_define=options.define)

    # Safety measure check we only have one module
    assert len(ast.description.definitions) == 1
    items = ast.description.definitions[0].items

    outputtree = dict()  # output var -> ([inputs], function, already output?)
    ffs = dict()
    varrange = dict()  # output var -> (basename, range_lo, range_hi, output?)
    for item in items:
        # Input/output decls
        if type(item) == pyverilog.vparser.ast.Decl:
            parse_verilog_decl(item, outputtree, varrange)

        # Assignments
        if type(item) == pyverilog.vparser.ast.InstanceList:
            parse_verilog_assign(item, outputtree, ffs, cell_info)

    return outputtree, ffs, varrange


def printoutput(output, outputtree, outputf, ffs, varrange):
    """ Print all statements needed to calculate one output. First, recursively write all inputs that
      this output is dependent on. Then write the current statement. """
    (inputs, fun, already) = outputtree[output]
    # Already processed? Exit out, or mark as processed. This avoids duplication and loops
    if already:
        return
    else:
        outputtree[output] = (inputs, fun, True)

    # Loop over all inputs
    for inp in inputs:
        if inp not in ffs and inp not in varrange:  # Don't update flipflops or outputs yet
            printoutput(inp, outputtree, outputf, ffs, varrange)

    # Write current statement. Prefix all outputs with out_ to avoid overwriting them at this point
    outputf.write(TYPE + " ")
    if output in ffs or output in varrange:
        outputf.write("out_")
    outputf.write("{};\n".format(fun))


def write_getset(name, lo, hi, outputf):
    """ Create getter and setter for specific variable
        XXX the setter is currently limited to setting for ALL VMs
        The getter nicely grabs the bits for the specified VM
    """
    outputf.write("%s get_%s(unsigned vmid) {\n" % (TYPE, name))
    outputf.write("    return 0 ")

    # Identify all indices and string them together
    if lo == hi:
        outputf.write("| ((({} >> vmid) & 1) << {})".format(name, 0))
    else:
        for i in range(lo, hi + 1):
            outputf.write("| ((({} >> vmid) & 1) << {})".format(idx2name(name, i), i))
    outputf.write(";\n};\n")

    # Make setter
    outputf.write("void set_%s(%s value) {\n" % (name, TYPE))
    if lo == hi:
        outputf.write("{} = (value & 1)*({})(-1);\n".format(name, TYPE))
    else:
        for i in range(lo, hi + 1):
            outputf.write("{} = (value & 1)*({})(-1); value >>= 1;\n".format(idx2name(name, i), TYPE))
    outputf.write("};\n")


def write_c(outputtree, ffs, varrange, outputfile):
    """ Write the C file """
    # Now that we have everything in a tree, recurse through it to produce output
    outputf = open(outputfile, "w")
    outputf.write("#include <stdint.h>\n")
    outputf.write("#include <cstdio>\n")
    outputf.write("#include <ctime>\n")
    outputf.write("class netlist {\n")
    outputf.write("private:\n")

    # First, declare all IOs
    outputf.write("//inputs/outputs\n")
    for inp in varrange:
        outputf.write("{} {} = 0;\n".format(TYPE, inp))

    # Now, write all flipflops
    outputf.write("//ffs\n")
    for inp in ffs:
        if inp not in varrange:  # Already covered above?
            outputf.write("{} {} = 0;\n".format(TYPE, inp))

    # Write getters and setters
    outputf.write("public:\n")
    outputf.write("//getters / setters\n")
    for (output, (name, lo, hi, is_output)) in varrange.items():
        dim = output.count("___idx")  # XXX bit of a hack, should have better tracking of dimensions
        if dim < 2:
            # We only process index 0 to avoid duplicating getters/setters for each bit
            if dim == 1 and "___idx0" not in output:
                continue

            assert "{}" not in output
            write_getset(name, lo, hi, outputf)

        else:
            print("No getter created for {} of dimension {}".format(output, dim))

    for ff in ffs:
        if ff not in varrange:  # Skip if already written above
            write_getset(ff, 0, 0, outputf)

    # This is where the magic happens
    outputf.write("void eval() {\n")
    outputf.write("if (!clk) return; // Only posedge supported\n")

    # Update all known flipflops and IOs
    for ff in itertools.chain(ffs.keys(), varrange.keys()):
        printoutput(ff, outputtree, outputf, ffs, varrange)
    # Move outputs into final state
    for (output, (_, _, _, is_output)) in varrange.items():
        if is_output:
            outputf.write("{} = out_{};\n".format(output, output))
    for ff in ffs:
        if ff not in varrange:  # Skip if already written above
            outputf.write("{} = out_{};\n".format(ff, ff))
    outputf.write("};\n")

    # Add a print function for the entire state
    outputf.write("void print() {\n")
    outputf.write('printf("===============================\\n");\n')
    for (inp, (name, lo, hi, is_output)) in varrange.items():
        dim = inp.count("___idx")  # XXX bit of a hack, should have better tracking of dimensions
        if dim == 1:
            if "___idx0" in inp:  # Only do for idx0
                outputf.write('printf("{}(vmid0) = %0{}lx\\n", get_{}(0));\n'.format(name, (hi - lo + 7) // 8, name))
        else:
            outputf.write('printf("{} = %lx\\n", {});\n'.format(inp, inp))
    for ff in ffs:
        outputf.write('printf("{} = %lx\\n", {});\n'.format(ff, ff))
    outputf.write("};\n")

    # end of class
    outputf.write("};\n")


# Main!
(libertyfile, netlistfile, outputfile, options) = parse_cmdline()
cell_info = read_cell_lib(libertyfile)
print("Parsing verilog {}".format(netlistfile))
(outputtree, ffs, varrange) = parse_verilog(netlistfile, cell_info, options)
write_c(outputtree, ffs, varrange, outputfile)
