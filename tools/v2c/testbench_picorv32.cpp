#include "picorv32.h"

#define UART0_BASE_ADDRESS          0xA0000000
#define UART0_STATUS                0x00000000
#define UART0_TX_DATA               0x00000004
#define UART0_RX_DATA               0x00000008

unsigned int clock_counter = 0;

uint32_t memory[0x100000 * 32]; // 32 bits per word, 0x100000 words, 64 VMs

int readmemh(const char *filename) {
    FILE *fp;
    unsigned int i;

    fp = fopen(filename, "rb");
    if(!fp) {
        return 1;
    }

    i = 0;

    while(!feof(fp)) {
        fscanf(fp, "%08x\n", &memory[i++]);
    }

    fclose(fp);

    return 0;
}

 // 32 bits per word, 0x100000 words, 64 VMs
int manage_memory(netlist *top) {
    uint64_t vmid = 0; // XXX hardcoded for one VM
    if(top->get_mem_valid(vmid) && !top->get_mem_ready(vmid)) {
	uint64_t mem_addr = top->get_mem_addr(vmid);
	printf("[*] Memory access at 0x%08lx\n", mem_addr);
        if(mem_addr < 0x100000 * 4) {
            top->set_mem_ready(1);
            top->set_mem_rdata(memory[mem_addr >> 2]);
	    printf("[*] Value = 0x%08x\n", memory[mem_addr >> 2]);
            if(top->get_mem_wstrb(vmid) & 1) {
                memory[mem_addr >> 2] = (memory[mem_addr >> 2] & 0x00FFFFFF) | (top->get_mem_wdata(vmid) & 0xFF000000);
            }
            if (top->get_mem_wstrb(vmid) & 2) {
                memory[mem_addr >> 2] = (memory[mem_addr >> 2] & 0xFF00FFFF) | (top->get_mem_wdata(vmid) & 0x00FF0000);
            }
            if (top->get_mem_wstrb(vmid) & 4) {
                memory[mem_addr >> 2] = (memory[mem_addr >> 2] & 0xFFFF00FF) | (top->get_mem_wdata(vmid) & 0x0000FF00);
            }
            if (top->get_mem_wstrb(vmid) & 8) {
                memory[mem_addr >> 2] = (memory[mem_addr >> 2] & 0xFFFFFF00) | (top->get_mem_wdata(vmid) & 0x000000FF);
            }
        } else if(mem_addr == UART0_BASE_ADDRESS + UART0_TX_DATA) {
            // TODO: fix memory mapping and loading in the sample to make data section work and have string printed properly
            top->set_mem_ready(1);
            printf("%c", (char)top->get_mem_wdata(vmid) & 0x000000FF);
        } else {
            // TODO: add if it was read or read/write
            printf("unmapped memory 0x%08lx [strb %i%i%i%i]\n", mem_addr, (top->get_mem_wstrb(vmid) & 1) ? 1 : 0, (top->get_mem_wstrb(vmid) & 2) ? 1 : 0, (top->get_mem_wstrb(vmid) & 4) ? 1 : 0, (top->get_mem_wstrb(vmid) & 8) ? 1 : 0);
            return 1;
        }
    } else {
        top->set_mem_ready(0);
    }

    return 0;
}

void manage_debug(netlist *top) {
    uint64_t vmid = 0; // XXX hardcoded
    if(top->get_mem_valid(vmid) && top->get_mem_ready(vmid)) {
        if(top->get_mem_instr(vmid)) {
            printf("%i | ifetch 0x%08lx: 0x%08lx\n", clock_counter, top->get_mem_addr(vmid), top->get_mem_rdata(vmid));
        } else if(top->get_mem_wstrb(vmid)) {
            printf("%i | write  0x%08lx: 0x%08lx (wstrb=%lx)\n", clock_counter, top->get_mem_addr(vmid), top->get_mem_wdata(vmid), top->get_mem_wstrb(vmid));
        } else {
            printf("%i | read   0x%08lx: 0x%08lx\n", clock_counter, top->get_mem_addr(vmid), top->get_mem_rdata(vmid));
        }
    }
}

netlist *freshcore() {
    netlist *top = new netlist;

    // reset
    top->set_clk(0);
    top->set_resetn(0);
    for(int i = 0; i < 200; i++) {
    	top->set_clk(1);
        top->eval();

    	top->set_clk(0);
        top->eval();
    }

    top->set_resetn(1);
    top->set_clk(1);
    top->eval();
    //top->print();

    return top;
}

void runcore(netlist *top, uint64_t clocks) {
    int vmid = 0;
    // tick for a finite number of cycles
    for(int i = 0; i < clocks * 2; i++) {
        if(top->get_clk(vmid)) 
            clock_counter++;

        if(manage_memory(top)) {
            break;
        }
        manage_debug(top);

        top->eval();
	printf("mem_state=%lx, %lx\n", top->get_mem_state___idx0(vmid),  top->get_mem_state___idx1(vmid));
	printf("mem_rdata=%lx\n", top->get_mem_rdata(vmid));
        top->set_clk(~top->get_clk(vmid));
    }

    printf("\n\n");
}

int main(int argc, char **argv, char **env) {
    if(argc < 2) {
        printf("usage: %s [hex]\n", argv[0]);
        return 1;
    }

    if (readmemh(argv[1])) {
	printf("Error reading %s\n", argv[1]);
	return 1;
    }

    netlist *top = freshcore();

    const int TESTLEN=10;// 00000;
    clock_t t = clock();
    runcore(top, TESTLEN);
    t = clock() - t;

    delete top;

    printf("Clocks per second: %f\n", (double)TESTLEN*(double)CLOCKS_PER_SEC/(double)t);

    return 0;
}

