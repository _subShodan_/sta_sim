#include "counter.h"

unsigned int clock_counter = 0;

netlist *freshcore() {
    netlist *top = new netlist;
    int vmid = 0; // #XXX

    // reset
    top->set_clk(0);
    top->set_rst(1);
    for(int i = 0; i < 10; i++) {
        top->eval();
        top->set_clk(~top->get_clk(vmid));
    }
    top->set_rst(0);

    top->eval();
    top->set_clk(~top->get_clk(vmid));

    return top;
}

void runcore(netlist *top, uint64_t clocks) {
    int vmid = 0; // #XXX
    // tick for a finite number of cycles
    for(uint64_t i = 0; i < clocks * 2; i++) {
	//top->print();
        if(top->get_clk(vmid)) 
            clock_counter++;

        top->eval();
    	top->set_clk(~top->get_clk(vmid));
    }

    printf("\n\n");
}

int main(int argc, char **argv, char **env) {
    netlist *top = freshcore();

    const int TESTLEN=50000000;
    clock_t t = clock();
    runcore(top, TESTLEN);
    t = clock() - t;
    printf("Clocks per second: %f\n", (double)TESTLEN*(double)CLOCKS_PER_SEC/(double)t);

    delete top;

    return 0;
}

