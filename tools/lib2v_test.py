#!/usr/bin/python3

import unittest
import lib2v
import tempfile
import os

class TestLib2v(unittest.TestCase):

    def test_equality(self):
        vf = tempfile.NamedTemporaryFile(suffix=".v")
        src_lib = "./lib2vtest.lib"
        cmp_v = "./lib2vtest.v"
        lib2v.dolib2v([src_lib, vf.name])

        diff = os.system("diff " + cmp_v + " " + vf.name)
        self.assertEqual(0, diff, "Difference with expected output")

        # Close/delete tempfiles
        vf.close()
        return

if __name__ == '__main__':
    unittest.main()
