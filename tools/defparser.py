#!/usr/bin/python3
# John Fitzgerald

import os, sys

class Component:
    def __init__(self, name, module, origin, orientation):
        self.name = name
        self.module = module
        self.origin = origin
        self.orientation = orientation

    def __str__(self):
        return "<Component {} {} {} {}>".format(self.name, self.module, self.origin, self.orientation)

class Components:
    def __init__(self, number):
        self.number = number
        self.list = []
    
    def parse(self, line):
        if line[0] != "-":
            return
        else:
            line = line[1:]
        plus = []
        for a in line.split("+"):
            plus.append(a.replace(";", "").strip())

        space = plus[0].split(" ")
        name = space[0]
        module = space[1]
        origin = (None, None)
        orientation = None
        
        for a in plus[1:]:
            if a.startswith("PLACED"):
                b = a[6:].replace("(", "").replace(")", "").replace("  ", " ").strip().split(" ")
                origin = (int(b[0]), int(b[1]))
                orientation = b[2]
                break

        self.list.append(Component(name, module, origin, orientation))

class DesignExchangeFormat:
    def __init__(self):
        self.parsestack = []
        
        self.diearea = None
        self.components = None

    def parseline(self, line):
        if line.startswith("DIEAREA"):
            decoded = line[7:].replace(";", "").replace("(", "").replace(")", "").replace("  ", " ").replace("  ", " ").strip().split(" ")
            self.diearea = ((int(decoded[0]), int(decoded[1])), (int(decoded[2]), int(decoded[3])))
        if line.startswith("COMPONENTS"):
            number = int(line.split(" ")[1])
            self.components = Components(number)
            self.parsestack.append(self.components)
        elif line.startswith("END COMPONENTS"):
            self.parsestack.pop()
        
        if len(self.parsestack) > 0:
            self.parsestack[-1].parse(line)

    def parse(self, filename):
        fp = open(filename, "r+")
        toparse = ""
        for line in fp:
            toparse += line.strip()
            #print(toparse)
            if (";" in toparse) or ("END" in toparse):
                self.parseline(toparse)
                toparse = ""
def parse(filename):
    df = DesignExchangeFormat()
    df.parse(filename)
    return df
