# Generated from fi_diff_vcd.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .fi_diff_vcdParser import fi_diff_vcdParser
else:
    from fi_diff_vcdParser import fi_diff_vcdParser

# This class defines a complete listener for a parse tree produced by fi_diff_vcdParser.
class fi_diff_vcdListener(ParseTreeListener):

    # Enter a parse tree produced by fi_diff_vcdParser#vcdfile.
    def enterVcdfile(self, ctx:fi_diff_vcdParser.VcdfileContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#vcdfile.
    def exitVcdfile(self, ctx:fi_diff_vcdParser.VcdfileContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#head.
    def enterHead(self, ctx:fi_diff_vcdParser.HeadContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#head.
    def exitHead(self, ctx:fi_diff_vcdParser.HeadContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#body.
    def enterBody(self, ctx:fi_diff_vcdParser.BodyContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#body.
    def exitBody(self, ctx:fi_diff_vcdParser.BodyContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#version.
    def enterVersion(self, ctx:fi_diff_vcdParser.VersionContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#version.
    def exitVersion(self, ctx:fi_diff_vcdParser.VersionContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#date.
    def enterDate(self, ctx:fi_diff_vcdParser.DateContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#date.
    def exitDate(self, ctx:fi_diff_vcdParser.DateContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#timescale.
    def enterTimescale(self, ctx:fi_diff_vcdParser.TimescaleContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#timescale.
    def exitTimescale(self, ctx:fi_diff_vcdParser.TimescaleContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#scope.
    def enterScope(self, ctx:fi_diff_vcdParser.ScopeContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#scope.
    def exitScope(self, ctx:fi_diff_vcdParser.ScopeContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#enddefinitions.
    def enterEnddefinitions(self, ctx:fi_diff_vcdParser.EnddefinitionsContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#enddefinitions.
    def exitEnddefinitions(self, ctx:fi_diff_vcdParser.EnddefinitionsContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#var.
    def enterVar(self, ctx:fi_diff_vcdParser.VarContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#var.
    def exitVar(self, ctx:fi_diff_vcdParser.VarContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#upscope.
    def enterUpscope(self, ctx:fi_diff_vcdParser.UpscopeContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#upscope.
    def exitUpscope(self, ctx:fi_diff_vcdParser.UpscopeContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#clock.
    def enterClock(self, ctx:fi_diff_vcdParser.ClockContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#clock.
    def exitClock(self, ctx:fi_diff_vcdParser.ClockContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#binnum.
    def enterBinnum(self, ctx:fi_diff_vcdParser.BinnumContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#binnum.
    def exitBinnum(self, ctx:fi_diff_vcdParser.BinnumContext):
        pass


    # Enter a parse tree produced by fi_diff_vcdParser#onebit.
    def enterOnebit(self, ctx:fi_diff_vcdParser.OnebitContext):
        pass

    # Exit a parse tree produced by fi_diff_vcdParser#onebit.
    def exitOnebit(self, ctx:fi_diff_vcdParser.OnebitContext):
        pass


