#!/usr/bin/python3

import os, sys
import tempfile
import re
import copy
from liberty.parser import parse_liberty
import pyverilog.vparser.ast as vast
import pyverilog.vparser.parser as vparser
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
from optparse import OptionParser
from itertools import filterfalse

import IPython

def get_opts(cmdline):
    USAGE_STRING = "Usage: harden.py netlist_input netlist_output\nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", dest="define", action="append", default=[], help="Macro Definition")
    optparser.add_option("-c", dest="harden", action="append", default=[], help="Cell to harden")
    optparser.add_option("-r", dest="cellfile", action="append", default=[], help="File to read cells from")
    optparser.add_option("-2", dest="nand2", action="store", default="sky130_fd_sc_hs__nand2_1", help="Name of NAND2 gate in cell lib")
    optparser.add_option("-3", dest="nand3", action="store", default="sky130_fd_sc_hs__nand3_1", help="Name of NAND3 gate in cell lib")
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 2:
        print("Incorrect number of parameters, is {}, should be 2".format(args))
        print(USAGE_STRING)
        sys.exit(1)

    netlistfile = args[0]
    outputfile = args[1]

    return (netlistfile, outputfile, options)

def is_output(port):
    return port.portname == "X" or port.portname == "Y" or port.portname == "Q" # XXX hack!

def portname(port):
    name = port.argname
    if type(name) == vast.Pointer:
        name = "{}[{}]".format(port.argname.var.name, port.argname.ptr.value)
    elif type(name) == vast.Identifier:
        name = name.name
    else:
        assert "Unknown type {}".format(type(name))
    return name
 
def create_nand2(nand2, inputA, inputB, name):
    portA = vast.PortArg("A", inputA.argname) # XXX Portname hack
    portB = vast.PortArg("B", inputB.argname)
    portY = vast.PortArg("Y", vast.Identifier(name))
    return vast.Instance(nand2, name + "_nand", (portA, portB, portY), ())

def create_nand3(nand3, inputs, name, output):  
    ports = [vast.PortArg(p, vast.Identifier(inputs[i])) for (p,i) in (("A",0),("B",1),("C",2))]
    ports.append(vast.PortArg("Y", output.argname))
    return vast.Instance(nand3, name, ports, ())

def harden_instance(instance, nand2, nand3):
    # Create 3 copies of the cell, and tie them into a majority vote
    result = []
    outputs = []
    orgoutput = None # Original output port
    basename = None
    for i in range(3):
        newports = []
        for port in instance.portlist:
            if is_output(port):
                orgoutput = port
                basename = "{}".format(portname(port).replace('[', '_').replace(']', '_')) # Hmmm. bit ugly on the strreplace
                newname = "{}_harden{}".format(basename, i)
                newport = vast.PortArg(port.portname, vast.Identifier(newname))
                outputs.append(newport)
            else:
                newport = port
            newports.append(newport)

        # Create new instance
        newinst = vast.Instance(instance.module, "{}_harden{}".format(instance.name, i), newports, instance.parameterlist)
        result.append(newinst)

    # Create 2 input nands
    nandnames = ["{}_harden{}".format(basename, i) for i in "ABC"]
    result.append(create_nand2(nand2, outputs[0], outputs[1], nandnames[0]))
    result.append(create_nand2(nand2, outputs[0], outputs[2], nandnames[1]))
    result.append(create_nand2(nand2, outputs[1], outputs[2], nandnames[2]))
        
    # Create final 3 input nand
    result.append(create_nand3(nand3, nandnames, "{}_hardenABC".format(basename), orgoutput))

    return result

def harden(netlistfile, outputfile, options):
    # Read cellfile
    if options.cellfile:
        for cellfilename in options.cellfile:
            if options.verbose >= 1:
                print("reading cellfile {}...".format(cellfilename))
            with open(cellfilename, "r") as pf:
                for line in pf.read().splitlines():
                    head, _, _ = line.partition("(") # Remove everything after (
                    options.harden.append(head)

    # Make unique
    options.harden = set(options.harden)
    
    if options.verbose >= 2:
        print("Cells to harden: {}".format(options.harden))

    # instrument verilog
    if options.verbose >= 1:
        print("parsing code...")

    ast = vparser.VerilogCodeParser((netlistfile, ), preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    if options.verbose >= 1:
        print("Hardening...")

    new_modules = []
    new_cells = 0
    old_cells = 0
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue

        assert definition.paramlist.children() == (), "We don't support parameters yet"

        # Find all instances that need hardening
        harden_me = set()
        for item in definition.items:
            harden_items = []
            if type(item) != vast.InstanceList:
                if options.verbose >= 2:
                    print("Ignoring item of type ", type(item))
                continue

            assert len(item.instances) == 1, "Only support 1 instance"

            instance = item.instances[0]
            if instance.name in options.harden:
                harden_me.add(item)

        # Remove instances to be replaced
        if options.verbose >= 2 and harden_me:
            print("Removing {}".format(["{}/{}".format(h.name, h.module) for item in harden_me for h in item.instances]))
        old_cells += len([i for i in definition.items if type(i) == vast.InstanceList])
        definition.items = [x for x in definition.items if x not in harden_me]

        # Add new hardened ones
        for item in harden_me:
            instance = item.instances[0]
            hardened = harden_instance(instance, options.nand2, options.nand3)
            for hard_item in hardened:
                il = vast.InstanceList(hard_item.module, item.parameterlist, (hard_item,))
                if options.verbose >= 2:
                    print("Adding:")
                    il.show()
                definition.items.append(il)
        new_cells += len([i for i in definition.items if type(i) == vast.InstanceList])

    assert len(options.harden) == len(harden_me), "Uhoh, not everything got hardened. Input: {} Actually hardened: {}".format(sorted(options.harden), sorted([item.instances[0].name for item in harden_me]))

    if options.verbose >= 1:
        print("generating code...")

    codegen = ASTCodeGenerator()
    rslt = codegen.visit(ast)
    rslt_file = open(outputfile, "w")
    rslt_file.write(rslt)
    rslt_file.close()

    if options.verbose >= 1:
        print("Old cells: {}, hardened: {}, new cells: {} => {}%".format(old_cells, len(harden_me), new_cells, new_cells*100//old_cells))

if __name__ == '__main__':
    harden(*get_opts(sys.argv[1:]))
