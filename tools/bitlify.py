#!/usr/bin/python3

import os, sys
import re
import copy
import tempfile
from liberty.parser import parse_liberty
import pyverilog.vparser.ast as vast
import pyverilog.vparser.parser as vparser
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator
from optparse import OptionParser
import instrument

import IPython

def get_opts(cmdline):
    USAGE_STRING = "Usage: bitlify.py [options] netlist_input netlist_output C_header_output liberty_file \nUse --help for more information."

    optparser = OptionParser(usage=USAGE_STRING)
    optparser.add_option("-v", "--verbose", action="count", dest="verbose", default=False, help="Verbose output")
    optparser.add_option("-I", "--include", dest="include", action="append", default=[], help="Include path")
    optparser.add_option("-D", dest="define", action="append", default=[], help="Macro Definition")
    optparser.add_option("-C", dest="compat", action="store_true", default=False, help="Create additional module compatible with the original interface")
    optparser.add_option("-w", dest="width", type="int", default=64, help="Width of vectors to output")
    (options, args) = optparser.parse_args(cmdline)

    if len(args) != 4:
        print(USAGE_STRING)
        sys.exit(1)

    netlistfile = args[0]
    outputfile = args[1]
    headerfile = args[2]
    libertyfile = args[3]

    return (netlistfile, outputfile, headerfile, libertyfile, options)


def arrayidx(name):
    return re.sub(r'_bit(\d+)_', r'[\1]', str(name))

def name_bit(identifier, bit):
    return identifier + "_bit{}_".format(bit)

def add_width(port, width):
    if port is None:
        return
    port.width = vast.Width(vast.IntConst(width-1), vast.IntConst(0))

def add_bit(bit, io, width):
    if io is None:
        return 
    io.name = name_bit(io.name, bit)

def port_for_bit(bit, port, width, single):
    first = copy.deepcopy(port.first)
    if not single:
        add_bit(bit, first, width)
    add_width(first, width)

    if hasattr(port, 'second'):
        second = copy.deepcopy(port.second)
        if not single:
            add_bit(bit, second, width)
        add_width(second, width)
    else:
        second = None

    return vast.Ioport(first, second)

def port_excluded(port, exclude):
    return exclude is not None and port in exclude

def expand_port(port, decls, width, exclude):
    if type(port) == vast.Port: # Simple port, need to get info from decl
        decl = decls[port.name]
        port = vast.Ioport(decl)

    if port_excluded(port.first.name, exclude):
        return [port] 

    assert port.second is None or port.second.width == port.first.width, "Don't support different second yet"

    # No expansion needed if port width is unspecified or is 1 bit
    if port.first.width is None: 
        msb = 0
        lsb = 0
    else:
        msb = int(port.first.width.msb.value) 
        lsb = int(port.first.width.lsb.value)

    assert lsb == 0, "We don't support non-0 lsb yet; will fail in writing C header file"

    return [port_for_bit(bit, port, width, (msb-lsb)==0) for bit in range(msb, lsb-1, -1)]

def create_instportlist(oldports, newports):
    instportlist = []
    for old, new in zip(oldports, newports):
        if type(old) == vast.Port:
            name = old.name
        else:
            name = old.first.name

        # Support both port types
        if len(new) == 1: # Not a vector
            instportlist.append(vast.PortArg(str(name), vast.Identifier(name)))
        else:
            # is a vector
            ports = [vast.PortArg(str(p.first.name), vast.Identifier(arrayidx(p.first.name))) for p in new]
            instportlist.extend(ports)

    return instportlist

def parse_port_decls(portdecls):
    decls = dict()
    for portdecl in portdecls:
        (port,) = portdecl.list
        decls[port.name] = port
    return decls


def expand_name(name, namelengths, exclude):
    if isinstance(name, vast.Identifier):
        n = str(name)
        if n.startswith('\\'):
            n = n[1:]
        wd = n not in namelengths and not port_excluded(n, exclude)

        # This is pretty ugly, but works for now for multidim vectors
        while True:
            m = re.match(r'^(.*)\[(\d+)\](.*)$', str(name)) # Match \ something [index] 
            if m:
                name = vast.Identifier(name_bit(m.group(1), int(m.group(2)))+m.group(3))  # group1 = name, group2 = index
            else:
                return (name, (str(name),) if wd else ())

    elif isinstance(name, vast.Pointer):
        wd = str(name.var) not in namelengths and not port_excluded(str(name.var), exclude)
        name = name_bit(str(name.var), name.ptr)
        return (vast.Identifier(name), (name,) if wd else ())
    elif isinstance(name, vast.Xor):  # Support glitch inputs
        (nl, wl) = expand_name(name.left, namelengths, exclude)
        (nr, wr) = expand_name(name.right, namelengths, exclude)
        name = vast.Xor(nl, nr)
        wiredefs = wl + wr
        return (name, wiredefs)
    else:
        assert False, "Warning. Unknown type for name {}".format(name)
    
def create_headers(headerfile, namelengths, width, exclude):
    lengths_done = set()

    headerfile.write('''
    
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

typedef uint32_t bitlify_t;     // Internal bookkeeping
typedef uint64_t bitlify_val_t; // External int size for get/set

#define BITS_IN_BITLIFY_T (sizeof(bitlify_t) * 8)

_Static_assert((MACHINES % BITS_IN_BITLIFY_T) == 0, "Number of machines must be a multiple of BITS_IN_BITLIFY_T");
_Static_assert(MACHINES >= BITS_IN_BITLIFY_T, "Number of machines must be larger than BITS_IN_BITLIFY_T");

bool get_bit_in_word(bitlify_val_t word, bitlify_val_t bit) {{
    return (word >> bit) & 1;
}}

void set_bit_in_word(bitlify_t *word, bitlify_t bit, bool value) {{
    bitlify_t mask = ((bitlify_t)1) << bit;
    *word = (*word & ~mask) | (-((bitlify_t)value) & mask);
}}

#define get_struct_name(base, idx) (base ## _bit ## idx ## _)
#define bytes_in(base) (((bits_in_ ## base) + 7) / 8)

bitlify_t* buf_element(bitlify_t* buffer, bitlify_t bit) {{
    return buffer + (bit / BITS_IN_BITLIFY_T);
}}

bool get_bit_from_buf(bitlify_t* buffer, bitlify_t bit) {{
    return (*buf_element(buffer, bit) >> (bitlify_t)(bit % BITS_IN_BITLIFY_T) ) & 1;
}}

void set_bit_in_buf(bitlify_t* buffer, bitlify_t bit, bool value) {{
    set_bit_in_word(buf_element(buffer, bit), bit % BITS_IN_BITLIFY_T, value);
}}

void set_all_bits_in_buf(bitlify_t* buffer, bool value) {{
    for (int i = 0; i < MACHINES / BITS_IN_BITLIFY_T; i++)
        buffer[i] = -((bitlify_t)value); // all 0 or all 1
}}

void print_if_bit_1(bitlify_t index, bool value, void* arg) {{
    if (value) {{
        printf("%u ", index);
    }}
}}

#define set_bit_all(base, bit, value) {{ set_all_bits_in_buf((bitlify_t*)&get_struct_name(base, bit), value); }}

'''.format());

    CUTOFF = 64
    headerfile.write('_Static_assert(sizeof(bitlify_val_t)*8 == {}, "Unsupported int width");\n'.format(CUTOFF));
    headerfile.write('_Static_assert((sizeof(bitlify_val_t) > sizeof(bitlify_t)) && (sizeof(bitlify_val_t) % sizeof(bitlify_t) == 0), "Unsupported int width");\n'.format(CUTOFF));

    for (name, length) in namelengths.items():
        if name in exclude: # Skip this one
            continue 

        headerfile.write("#define bits_in_{} {}\n".format(name, length))


        if length not in lengths_done:
            lengths_done.add(length)

            headerfile.write("#define loop_len_{}(base, vm, func, arg) {{ ".format(length));
            for i in range(length): # loop over all bits in value
                headerfile.write("{{ func({}, get_bit_from_buf((bitlify_t*)&(get_struct_name(base, {})), (vm)), (arg) ); }} ".format(i, i));
            headerfile.write(" }\n");

            if length == 1: # Different variable names
                headerfile.write('#define get_len_1(base, vm) get_bit_from_buf((bitlify_t*)(&(base)), vm)\n')
                headerfile.write('#define set_len_1(base, vm, value) set_bit_in_buf((bitlify_t*)&(base), vm, value)\n')
                headerfile.write('#define set_all_1(base, value) set_all_bits_in_buf((bitlify_t*)&(base), value);\n')
                headerfile.write('#define set_zero_1(base) set_all_1(base, 0);\n')
            else:
                if length <= CUTOFF:
                    # Base getter for specific length
                    headerfile.write('#define get_len_{}(base, vm) ('.format(length));
                    for i in range(length):
                        headerfile.write("(((bitlify_val_t)get_bit_from_buf((bitlify_t*)&(get_struct_name(base, {})), vm)) << {}ULL) | ".format(i, i));
                    headerfile.write("0)\n"); 

                    # Baser setter for specific length
                    headerfile.write("#define set_len_{}(base, vm, value) ".format(length) + " { ");
                    for i in range(length): # loop over all bits in value
                        headerfile.write("set_bit_in_buf((bitlify_t*)&(get_struct_name(base, {})), vm, 1&((bitlify_val_t)(value)) >> {}ULL ); ".format(i, i));
                    headerfile.write("}\n");

                    # Setter to set all vms to same value
                    headerfile.write("#define set_all_{}(base, value) ".format(length) + " { ")
                    for i in range(length):
                        headerfile.write("set_bit_all(base, {}, get_bit_in_word(value, {})) ".format(i, i));
                    headerfile.write("}\n");

                # Get value from a particular vm into a buffer
                headerfile.write("#define get_buffer_{}(base, vm, buffer) ".format(length) + " { ")
                for i in range(length):
                    headerfile.write("set_bit_in_buf((bitlify_t*)(buffer), {}, get_bit_from_buf((bitlify_t*)&(get_struct_name(base, {})), vm)  ); ".format(i, i));
                headerfile.write("}\n");

                # Set all vms to value in buffer
                headerfile.write("#define set_buffer_{}(base, vm, buffer) ".format(length) + " { ")
                for i in range(length):
                    headerfile.write("set_bit_in_buf((bitlify_t*)&(get_struct_name(base, {})), vm, get_bit_from_buf((bitlify_t*)(buffer), {}) ); ".format(i, i));
                    #headerfile.write("set_bit_all(base, {}, get_bit_from_buf((bitlify_t*)buffer, {})) ".format(i, i))
                headerfile.write("}\n");

                # Set all vms to value 0
                headerfile.write("#define set_zero_{}(base) ".format(length) + " { ")
                for i in range(length):
                    headerfile.write("set_bit_all(base, {}, 0) ".format(i))
                headerfile.write("}\n");


        headerfile.write("#define get_{}(base, vm) get_len_{}(base->{}, vm)\n".format(name, length, name))
        headerfile.write("#define get_{}_buf(base, vm, buffer) get_buffer_{}(base->{}, vm, buffer)\n".format(name, length, name))
        headerfile.write("#define loop_{}(base, vm, func, arg) loop_len_{}(base->{}, vm, func, arg)\n".format(name, length, name))
        headerfile.write("#define set_{}(base, vm, value) set_len_{}(base->{}, vm, value)\n".format(name, length, name))
        headerfile.write("#define set_{}_all(base, value) set_all_{}(base->{}, value)\n".format(name, length, name))
        headerfile.write("#define set_{}_buf(base, vm, buffer) set_buffer_{}(base->{}, vm, buffer)\n".format(name, length, name))
        headerfile.write("#define set_{}_zero(base) set_zero_{}(base->{})\n".format(name, length, name))


def get_name(old):
    if hasattr(old, 'name'): # For some reason... name isn't always there
        return str(old.name)
    else:
        return str(old.first.name)

def create_wire_decls(wires, width):
    return tuple(vast.Decl((vast.Wire(wire, width=vast.Width(vast.IntConst(width-1), vast.IntConst(0))),)) for wire in wires)

def bitlify(netlistfile, outputfile, headerfile, libertyfile, options):
    # instrument verilog
    if options.verbose:
        print("parsing code...")

    ast = vparser.VerilogCodeParser((netlistfile, ), preprocess_include=options.include, preprocess_define=options.define, preprocess_output=tempfile.NamedTemporaryFile(delete=False).name, outputdir=tempfile.TemporaryDirectory().name).parse()

    # build pin map
    if options.verbose:
        print("parsing liberty file...")
    libpinmap, ff_clocks, _ = instrument.load_liberty(libertyfile) # XXX bit ugly to load from other script like this
    exclude = instrument.trace_clock(ast, ff_clocks, libpinmap)
    if options.verbose >= 2:
        print("Excluded: ", exclude)


    if options.verbose:
        print("Adding new modules...")

    new_modules = []
    for definition in ast.description.definitions:
        if not isinstance(definition, vast.ModuleDef):
            continue

        assert definition.paramlist.children() == (), "We don't support parameters yet"

        module_name = definition.name

        # Parse definitions of ports, in case they are inside the module
        decls = parse_port_decls([i for i in definition.items if type(i)==vast.Decl]) # Filter out decls

        # Expand all ports to multibit
        oldports = definition.portlist.children()
        newports = [expand_port(port, decls, options.width, exclude) for port in oldports]

        # update lengths needed for header file
        namelengths = {get_name(old): len(new) for old, new in zip(oldports,newports)}

        # Add new compatible interface module? Copy some stuff now before modifications below
        if options.compat:
            #IPython.embed()

            # create port list for instance
            instportlist = create_instportlist(oldports, newports)
            
            # Create instance in new module refering to base module
            parameterlist = ()
            instance = vast.Instance(module_name, module_name + "_instance", instportlist, parameterlist)
            instances = (instance,)
            parameterlist = ()
            instancelist = vast.InstanceList(module_name, parameterlist, instances)

            # Create module
            newname = module_name + "_compat"
            parameterlist = definition.paramlist
            portlist = definition.portlist
            items = tuple(i for i in definition.items if type(i)==vast.Decl) # Keeps decls only
            items += (instancelist,)
            new_module = vast.ModuleDef(newname, parameterlist, portlist, items)
            new_modules.append(new_module)


        # Fix up module
        definition.portlist = vast.Portlist((p for ports in newports for p in ports)) # Expand inner arrays
        definition.items = tuple(i for i in definition.items if type(i)!=vast.Decl) # Filter decls away, already added to portlist

        # For each port assignment with vector index, expand the name
        implicit_wires = set()
        for item in definition.items:
            if isinstance(item, vast.InstanceList): # Ignore rest
                for instance in item.instances:
                    for port in instance.portlist:
                        # Collect all implicit wires
                        add = False
                        if isinstance(port.argname, vast.Identifier) and str(port.argname) not in namelengths:
                            if not port_excluded(str(port.argname), exclude):
                                add = True

                        # And expand portname
                        (argname, wiredefs) = expand_name(port.argname, namelengths, exclude)
                        port.argname = argname
                        wiredefs = (w for w in wiredefs if w not in namelengths and not port_excluded(w, exclude)) # Filter
                        implicit_wires.update(wiredefs)

        # Add wire definitions
        definition.items = create_wire_decls(sorted(implicit_wires), options.width) + definition.items


    ast.description.definitions += tuple(new_modules)

    if options.verbose:
        print("generating code...")

    codegen = ASTCodeGenerator()
    rslt = codegen.visit(ast)
    rslt_file = open(outputfile, "w")
    rslt_file.write(rslt)
    rslt_file.close()

    if options.verbose:
        print("generating header file...")
    head_file = open(headerfile, "w")
    create_headers(head_file, namelengths, options.width, exclude)
    head_file.close()


if __name__ == '__main__':
    bitlify(*get_opts(sys.argv[1:]))
