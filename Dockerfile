############## begin stolen from openroad ################
FROM ubuntu:20.04 AS base-dependencies
LABEL maintainer="Vitor Bandeira <bandeira@ieee.org>"

ENV CC=/usr/bin/gcc \
    CPP=/usr/bin/cpp \
    CXX=/usr/bin/g++

RUN apt-get update

RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata

RUN apt-get install -y automake autotools-dev bison cmake flex g++ gcc git libpcre3 libpcre3-dev qt5-default tcl tcl-dev wget zlib1g zlib1g-dev

RUN git config --global http.proxy $http_proxy
RUN git config --global https.proxy $https_proxy

# Install CMake
RUN wget https://cmake.org/files/v3.14/cmake-3.14.0-Linux-x86_64.sh && \
    chmod +x cmake-3.14.0-Linux-x86_64.sh  && \
    ./cmake-3.14.0-Linux-x86_64.sh --skip-license --prefix=/usr/local && rm -rf cmake-3.14.0-Linux-x86_64.sh

# Install SWIG
RUN wget https://github.com/swig/swig/archive/rel-4.0.1.tar.gz \
    && tar xfz rel-4.0.1.tar.gz \
    && rm -rf rel-4.0.1.tar.gz \
    && cd swig-rel-4.0.1 \
    && ./autogen.sh && ./configure --prefix=/usr && make -j $(nproc) && make install

# boost 1.68 required for TritonRoute but 1.68 unavailable
RUN wget https://sourceforge.net/projects/boost/files/boost/1.72.0/boost_1_72_0.tar.bz2/download && \
    tar -xf download && \
    cd boost_1_72_0 && \
    ./bootstrap.sh && \
    ./b2 install --with-iostreams -j $(nproc)

# eigen required by replace, TritonMacroPlace
RUN git clone https://gitlab.com/libeigen/eigen.git \
    && cd eigen \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make install

# lemon required by TritonCTS (no package for CentOS!)
#  (On Ubuntu liblemon-dev can be used instead)
RUN wget http://lemon.cs.elte.hu/pub/sources/lemon-1.3.1.tar.gz \
    && tar -xf lemon-1.3.1.tar.gz \
    && cd lemon-1.3.1 \
    && cmake -B build . \
    && cmake --build build -j $(nproc) --target install

# need the "hack" below to run on docker in gcloud instance (MacOS Docker does
# not need)
RUN strip --remove-section=.note.ABI-tag /usr/lib/x86_64-linux-gnu/libQt5Core.so

RUN useradd -ms /bin/bash openroad

# Openroad build
#WORKDIR /
#RUN git clone --recursive https://github.com/The-OpenROAD-Project/OpenROAD.git
#WORKDIR /OpenROAD
#RUN cmake -B build . && cmake --build build -j $(nproc) && ln -s /OpenROAD/build/src/openroad /usr/local/bin

# TritonRoute build
#WORKDIR /
#RUN git clone --recursive https://github.com/The-OpenROAD-Project/TritonRoute.git
#WORKDIR /TritonRoute
#RUN cmake -B build . && cmake --build build -j $(nproc) && ln -s /TritonRoute/build/TritonRoute /usr/local/bin

################# begin openroad hacks Jasper ###############
# Openroad-flow
WORKDIR /
RUN git clone --recursive https://github.com/The-OpenROAD-Project/OpenROAD-flow-public.git
WORKDIR /OpenROAD-flow-public
# Hack to move up to tcl8.6; ubuntu 20 doesn't have tcl8.5
RUN apt-get install -y libreadline-dev libffi-dev
RUN grep -rl tcl8.5 . | tr '\n' '\0' | xargs -0 sed -i -e 's/tcl8.5/tcl8.6/g'
RUN ./build_openroad.sh --local
RUN apt-get install -y time klayout
#RUN /bin/bash -c "source ./setup_env.sh && cd flow && make"

############## begin picorv32 hacks ################
RUN apt-get update && apt-get install -y verilator perl-modules git make g++ sudo bash ftp texinfo libexpat1 libexpat1-dev bison autoconf automake autotools-dev curl libmpc-dev libmpfr-dev  libgmp-dev  gawk  build-essential  bison flex  texinfo  gperf  libtool  patchutils  bc  zlib1g-dev  libexpat-dev git python3
#RUN && rm -rf /var/lib/apt/lists/*

WORKDIR /
RUN git clone https://github.com/cliffordwolf/picorv32

WORKDIR /picorv32
RUN make download-tools
RUN make -j$(nproc) build-riscv32i-tools-bh && make clean && find /opt -executable -type f -exec strip '{}' \;


# lib2v dependencies, instrument.py, glitcher
RUN echo trigger update
RUN apt-get -y update
RUN apt-get -y install python3-pip python3-sympy iverilog vim clang parallel gnuplot-nox
RUN pip3 install liberty-parser pyverilog yappi matplotlib

# Let's add sv2v for OpenTitan
#RUN wget https://github.com/zachjs/sv2v/releases/download/v0.0.6/sv2v-Linux.zip -O /tmp/sv2v.zip
#RUN cd / && unzip /tmp/sv2v.zip
#RUN ln -s /sv2v-Linux/sv2v /usr/local/bin/sv2v
#RUN ln -s /OpenROAD-flow-public/tools/build/yosys/bin/yosys /usr/local/bin/yosys
RUN wget -qO- https://get.haskellstack.org/ | sh
RUN cd / && git clone https://github.com/zachjs/sv2v && cd sv2v && git checkout 5cc4dce01f19433e9434a19967f358d01e6bb6fb && make && ln -s /sv2v/bin/sv2v /usr/local/bin/sv2v

# Hack in new yosys
RUN rm -rf /OpenROAD-flow-public/tools/build/yosys /OpenROAD-flow-public/tools/yosys
RUN cd /OpenROAD-flow-public/tools && git clone https://github.com/YosysHQ/yosys && cd yosys && git checkout 014c7e26b85000e99a27d2287a6967daa64d7a30 && make install -j`nproc` PREFIX=../build/yosys CONFIG=gcc TCL_VERSION=tcl8.6

# Some updates for Nils' OT project 
RUN apt-get -y install binutils-riscv64-unknown-elf
RUN pip3 install bin2coe

# FI-diff
RUN pip3 install antlr4-python3-runtime==4.7.2

# copy working directory
RUN mkdir /nlsim
WORKDIR /nlsim
# Copy some stuff; note we'll mount the host fs over this at runtime 
COPY . /nlsim/

# Let's cache this also
#RUN make single
