#!/bin/sh

START=$1
END=$2

echo `hostname` starting from ${START} to ${END}
tar xvzf package.tgz

sudo apt update -y
sudo apt install -y python3-pip python3-numpy
pip3 install ipython liberty-parser pyverilog yappi matplotlib

killall glitcher.py
killall testbench-veri

cd ~/output/sky130/cortexm0/ffonly/tb && ~/tools/glitcher.py  --glitchprob 0.01 -r `date +%-N` -s $START -e $END -t 100000 -n `nproc` --clocks 350000 -g 1 -m 256 -b "/home/j/output/sky130/cortexm0/ffonly/tb/testbench_veri -i atf-norsa.bin" ~/output/sky130/cortexm0/ffonly/instrumented.v.map ~/output/sky130/cortexm0/ffonly/figrep_bruteforce_${START}_${END}.res 

echo `hostname` done
