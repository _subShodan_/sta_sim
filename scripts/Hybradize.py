import os
import json

# Global definitions
INPUT_FILE  = "input.json"
OUTPUT_FILE = "hybrid.v"
RTL_DIR     = "rtl/Rocket/"
GL_DIR      = "gl/"


def read_module_list(input_file_name):
    """
    This function parse JSON file and
    take the module names into a list.

    """

    input_file = open(input_file_name)
    module_list = json.load(input_file)['ModuleList']
    return ["module " + module + "(" for module in module_list ]


def copy_rtl_modules(dir, module_list, output_file_ptr):
    """
    This function iterates over files in RTL_DIR and 
    copies all the modules except the modules listed in
    module_list.

    """

    for filename in os.listdir(dir):
        trigger = True

        with open(dir + filename) as rtl_file_ptr:
            rtl_file = rtl_file_ptr.readlines()
            
            for line in rtl_file:    
                if (any(module in line for module in module_list)):
                    trigger = False

                if(trigger):
                    output_file_ptr.write(line)

                if("endmodule" in line):
                    trigger = True


def copy_gl_modules(dir, module_list, output_file_ptr):
    """
    This function iterates over files in GL_DIR and 
    copies only the modules listed in module_list.
    
    """

    for filename in os.listdir(dir):
        trigger = False

        with open(dir + filename) as gl_file_ptr:
            gl_file = gl_file_ptr.readlines()
            
            for line in gl_file:    
                if (any(module in line for module in module_list)):
                    trigger = True

                if(trigger):
                    output_file_ptr.write(line)
                
                if("endmodule" in line):
                    trigger = False


if __name__ == '__main__':

    # Input module names from input.json
    module_list = read_module_list(INPUT_FILE)

    # Open output file to copy hybrid modules
    output_file_ptr = open(OUTPUT_FILE, "w")

    # Copy RTL modules (modules - module_list)
    copy_rtl_modules(RTL_DIR, module_list, output_file_ptr)

    # Copy GL modules (module_list)
    copy_gl_modules(GL_DIR, module_list, output_file_ptr)

    # Close output file
    output_file_ptr.close()

   