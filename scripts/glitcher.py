import sys
import subprocess
import sys
import random

def getBinString(p,maxParity):
    binStr = ""
    # Loop to find the string  of desired length
    parity = 0
        
    for i in range(p):
        # randint function to generate
        # 0, 1 randomly and converting 
        # the result into str
        genBit = random.randint(0, 1)
        if(genBit == 1):
            parity = parity + 1
        
        if(parity <=  maxParity):
            temp = str(genBit)
        else:
            temp =str(0)
        # Concatenatin the random 0, 1
        # to the final result
        binStr += temp
          
    return binStr


def driveBinary():
    binstring = getBinString(1197,5)
    result = subprocess.run(['../sim/Rocket/obj_dir/emulator', '../tests/Rocket/hello.riscv', binstring], capture_output=True)
    print(result.stdout)
    if("failed" in str(result.stdout)):
        print("Glitch Unsuccesfull")

    else:
        print("No effect")


if __name__ == "__main__":
    maxNumberOfFFsToFault = sys.argv[1]
    FlipFlopArray = sys.argv[2]
    driveBinary()