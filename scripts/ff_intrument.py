import json
import pyverilog
import os
from pyverilog.vparser.parser import parse
import pyverilog.vparser.ast as vast

from pyverilog.utils.identifiervisitor import getIdentifiers
from pyverilog.ast_code_generator.codegen import ASTCodeGenerator


INPUT_FILE = "input.json"
RTL_DIR     = "rtl/Rocket/"


def copy_rtl_modules(dir, module_list, output_file_ptr):
    """
    This function iterates over files in RTL_DIR and 
    copies all the modules except the modules listed in
    module_list.

    """

    for filename in os.listdir(dir):
        trigger = True

        with open(dir + filename) as rtl_file_ptr:
            rtl_file = rtl_file_ptr.readlines()
            
            for line in rtl_file:    
                if (any(module in line for module in module_list)):
                    trigger = False

                if(trigger):
                    output_file_ptr.write(line)

                if("endmodule" in line):
                    trigger = True


def read_module_list(input_file_name):
    """
    This function parse JSON file and
    take the module names into a list.

    """

    input_file = open(input_file_name)
    module_list = json.load(input_file)['ModuleList']
    return ["module " + module + "(" for module in module_list]





if __name__ == '__main__':
    
    module_list = read_module_list(INPUT_FILE)
    output_file_ptr = open("selected_modules.v", "w")

    # Open output file to copy hybrid modules
    instrumented_file_ptr = open("gl/instrumented.v", "a")

    # Copy RTL modules (modules - module_list)
    copy_rtl_modules(RTL_DIR, module_list, instrumented_file_ptr)


    trigger = False
    with open("hybrid.v") as hybrid_file_ptr:
        hybrid_file = hybrid_file_ptr.readlines()

        for line in hybrid_file:
            if (any(module in line for module in module_list)):
                trigger = True

            if(trigger):
                output_file_ptr.write(line)

            if("endmodule" in line):
                trigger = False

    output_file_ptr.close()



    DFF_count = 0
    replaced_modules = open("selected_modules.v")
    ast, directives = parse(replaced_modules)


    
    for _def in ast.description.definitions:            
        
        item_list = []
        for item in _def.items:
            if(isinstance(item, vast.InstanceList) and str(item.module) == "DFF"):
                
                inst_list = []
                for inst in item.instances:
                
                    port_list = []    
                    for port in inst.portlist:
                        if(str(port.portname) == "D"):
                            xored_port = vast.Xor(port.children()[0], vast.Pointer(vast.Identifier("glitchbus"),vast.IntConst(DFF_count))) 
                            port_list.append(vast.PortArg(port.portname,xored_port))
                        else: 
                            port_list.append(port)

                    modified_inst = vast.Instance(inst.module, inst.name, port_list, inst.parameterlist)
                    inst_list.append(modified_inst)
            
                item_list.append(vast.InstanceList(item.module, item.parameterlist, inst_list))
                DFF_count = DFF_count + 1

            else:
                item_list.append(item)
            
    
        # print(DFF_count)

        modified_ast = vast.ModuleDef(_def.name, _def.paramlist, _def.portlist, item_list)
        codegen = ASTCodeGenerator()
        rslt = codegen.visit(modified_ast) 
        instrumented_file_ptr.write(rslt)

    instrumented_file_ptr.close()       


