import re

def extract_dff_name(line):
    pattern = "_(.*?)_"
    substring = re.search(pattern, line).group(1)
    return substring

if __name__ == "__main__":
    path_file = open("../sta/paths.lst")
    paths = path_file.read().split("Startpoint")
    DFF_lst = open("../sta/dff_lst","w")
    for j,path in enumerate(paths) :
        DFF_lst.write(str(j) + ",")
        for i,line in enumerate(path.split("\n")):
            if(("core" in line or "ibuf" in line or "RCVExpander" in line )): #and "DFF" in line
                dff = extract_dff_name(line)
                DFF_lst.write(dff +",")
        DFF_lst.write("\n")
            